#!/usr/bin/env python3

"""
Description
"""


import argparse
import csv
import os
import sys


def printerr(*messages, **kwargs):
    if 'file' in kwargs:
        del kwargs['file']
    print(*messages, file=sys.stderr, **kwargs)


def to_tuple(strat):
    first = strat.split('--')

    if len(first) == 2:
        bliz = [first[0]]
        rest = first[1]
    else:
        bliz = []
        rest = first[0]

    parts = rest.split('+*')

    return (tuple(sorted(bliz + parts)), strat)


def c_vs_c(strats):
    return [(x.replace('+*', '+'), x.replace('+*', '*'))
            for x in strats
            if 'BLIZZ' not in x]

def comb(ps):
    return [(x[0].replace('+*', '*'), x[1].replace('+*', '*'))
            for x in ps]

def t_other(strats):
    alls = {t[0]: t[1]
            for t in map(to_tuple, [x for x in strats if 'BLIZZ' not in x])}

    res = []

    for t, other_s in alls.items():
        if 'T_OTHER' in t and len(t) > 1:
            no_other_s = alls[tuple(sorted(x for x in t if x != 'T_OTHER'))]

            res.append((no_other_s, other_s))
            
    return comb(res)


def other(strats):
    alls = {t[0]: t[1]
            for t in map(to_tuple, [x for x in strats if 'BLIZZ' not in x])}

    res = []

    for t, other_s in alls.items():
        if 'OTHER' in t and len(t) > 1:
            no_other_s = alls[tuple(sorted(x for x in t if x != 'OTHER'))]

            res.append((no_other_s, other_s))
            
    return comb(res)


def t_all(strats):
    cs = ('T_OB', 'T_EB', 'T_S2R', 'T_TITLE')
    alls = {t[0]: t[1]
            for t in map(to_tuple, [x for x in strats if 'BLIZZ' not in x])}

    res = []

    for c in cs:
        no_task_c = c.replace('T_', '')
        for t, other_s in alls.items():
            if c in t:
                l =list(x for x in t if x != c)
                l.append(no_task_c)
                no_other_s = alls[tuple(sorted(l))]

                res.append((no_other_s, other_s))
            
    return comb(res)

def blizz(strats):
    return [(x.replace('+*', '*'), 'BLIZZARD--' + x.replace('+*', '*'))
            for x in strats
            if 'BLIZZ' not in x]


def main(the_dir):
    with open(os.path.join(the_dir, 'all-strategies.txt')) as infile:
        strats = [x.strip() for x in infile]

    ts = [
        ('1_conj_comb', c_vs_c, 485),
        ('2_other', other, 161),
        ('3_t_other', t_other, 161),
        ('4_t_all', t_all, 648),
        ('5_blizzard', blizz, 485),
    ]

    for n, m, size in ts:
        filt = m(strats)
        fsize = len(filt)
        assert fsize == size, 'Filtered size %d should be %d' % (fsize, size)

        with open(os.path.join(the_dir, n + '.csv'), 'w') as outfile:
            writer = csv.writer(outfile)
            writer.writerow(['strat1', 'strat2'])

            for s1, s2 in filt:
                writer.writerow([s1, s2])


if __name__ == '__main__':
    this_script_dir = os.path.dirname(os.path.abspath(__file__))
    parser = argparse.ArgumentParser()
##    parser.add_argument('number', help='An int value', type=int)
##    parser.add_argument('-v', '--verbose', help='Makes it verbose',
##                        action='store_true')
##    parser.add_argument('-a', '--amount', help='amount', type=int)

    args = parser.parse_args()
##    print(args.number)
##    if args.verbose:
##        pass

    main(this_script_dir)
