BUG REPORT TITLE:
Add utility methods to query state of URL attributes in EmbedConnection

BUG REPORT DESCRIPTION:
Add utility methods to determine whether URL attributes are set or have the value true.
The motivation for the change is improved readability/formatting of the code.
