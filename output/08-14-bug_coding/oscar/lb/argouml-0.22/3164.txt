BUG REPORT TITLE:
Entering an internal transition via PropPanel does not update state

BUG REPORT DESCRIPTION:
When the designer enters an internal transition (do, entry, exit, or "plain" internal transition), the state in the diagram is not updated accordingly.

Editing the fields in the state fig itself behaves correctly and does update the model.
