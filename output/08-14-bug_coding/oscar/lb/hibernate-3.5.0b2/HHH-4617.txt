BUG REPORT TITLE:
Using materialized blobs with Postgresql causes error

BUG REPORT DESCRIPTION:
I have entity with byte[] property annotated as @Lob and lazy fetch type, when table is createad the created column is of type oid, but when the column is read in application, the Hibernate reads the OID value instead of bytes under given oid.
It's behavior like to read / write bytea.

If i remember well, auto-creating table with Hibernate creates oid column.

The proper behavior for dealing in PostgreSQL (and this behavior is in Hibernate 3.4) is to use oids.
