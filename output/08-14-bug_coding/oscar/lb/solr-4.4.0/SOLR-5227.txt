BUG REPORT TITLE:
attempting to configured a dynamicField as required, or using a default value, should fail.

BUG REPORT DESCRIPTION:
In SOLR-5222 Pascal noted that he did not get the behavior expected when using sortMissingLast with a dynamicField using docValues in Solr < 4.5 -- but up to Solr 4.4, docValues required a default value, so he should have gotten a hard error as soon as he tried specifying a default value on a dynamicField.
