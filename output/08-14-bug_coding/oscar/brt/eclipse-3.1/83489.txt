BUG REPORT TITLE:
[select] Code select returns IType instead of ITypeParameter on method parameters types

BUG REPORT DESCRIPTION:
Using HEAD.
Consider following test case:
class Test<T> { void foo(T t) {}
}

When I select "T" in method declaration, selection engine returns an IType
"Test" instead of expected ITypeParameter "T".
