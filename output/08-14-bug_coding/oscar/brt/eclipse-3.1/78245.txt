BUG REPORT TITLE:
Breakpoints in enums not correctly created.

BUG REPORT DESCRIPTION:
We are now able to add breakpoint in enum classes, but they're not correctly created, the associated type is wrong.
It's working OK if the enum is an inner type, but not if it's a top level type.

public enum TestEnum { a;
public static void main(String[] args) {
System.out.println();   // <- add a breakpoint here
}
}

The breakpoint is created, but displayed in the breakpoint view as 'null [line
XX] - main(String[])', and the program doesn't stop on the breakpoint.
