BUG REPORT TITLE:
New toolbar in launch dialog

BUG REPORT DESCRIPTION:
Eclipse I20060124

- I see some new layout in the Launch dialog and thought I would make some comments/suggestions:

1 - the new groupbox for "Configurations" seems to add a lot of spacing above and below the toolbar, it just looks like it is floating out in space
I suggest the following

2 - remove the groupbox
- the setup is quite clear about the three sections, left right and bottom it is not necessary to add a groupbox here
- it also messes up the alignment of the "Configurations" and "Name:" labels
3 - add separators much like the preferences dialog instead
- more space efficient
4 - make the toolbar left aligned.
- I see the similarity to a view here but not sure that is really the right thing to do, I typically expect toolbars to be on the left
5 - I know that you've just added the toolbar, but it seems strange to have a toolbar in a dialog, somehow it is unfamilar.
You might want to consider going back to buttons with text.
