BUG REPORT TITLE:
JavaAttributeWizard shouldn't open with errors when passed an empty string

BUG REPORT DESCRIPTION:
3.3 m2 test candidate

1. On the overview page of the plugin editor, make sure the Activator field is empty.

2. Click the Activator link.
The class wizard opens with an error.
This is a UI faux-pas.

I suspect this is a regression due to fix for bug 101644
