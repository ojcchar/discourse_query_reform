BUG REPORT TITLE:
[Memory View] Provide UI and API to configure the addressable size.

BUG REPORT DESCRIPTION:
Users should be able to configure the addressable size of a memory block manually if it is not provided by the underlying debugger.
