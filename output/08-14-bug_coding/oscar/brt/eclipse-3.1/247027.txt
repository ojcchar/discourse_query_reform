BUG REPORT TITLE:
filteredDependencyCheck missing specified plugin versions

BUG REPORT DESCRIPTION:
When including binary features in the build, requested versions to include in the state have a specific version.

A specific version is incorrectly being excluded from the state.

Fix is to use [0.0.0, 0.0.0) as the bound on the sorted subset, and have ReachablePlugin account for inclusion of the maximum when sorting.
