BUG REPORT TITLE:
[EditorMgmt] history: NavigationHistory won't release references to IEditorParts

BUG REPORT DESCRIPTION:
Through profiling I have discovered that the reason my references are never released is because NavigationHistory is holding them.

NavigationHistory gets references to every IEditorPart opened within a particular WorkbenchPage.
From what I can tell through code examination it never releases any of them until the number held reaches an appearantly arbitrary 50.
It does not seem to be at all related to how much memory a particular editor is using.

If this is some form of cache one wonders if it could have been better done with
SoftReferences.
In any event, I need a way to get the NavigationHistory to empty itself.
Else I will be limited in the number of work that can be done with my plugin since none of the memory upto 50 editors will ever be reclaimed.
