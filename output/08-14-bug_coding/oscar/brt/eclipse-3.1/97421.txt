BUG REPORT TITLE:
New Plug-in Project wizard creates MANIFEST.MF with mixed line delimiters

BUG REPORT DESCRIPTION:
I20050527-1300

The New Plug-in Project wizard creates a MANIFEST.MF with mixed line delimiters.
Create a new Plug-in Project, give a name and step through the wizard accepting default values.

The generated MANIFEST.MF has Windows (\r\n) line delimiters, except for a lonely \n after the comma in this property:

Require-Bundle: org.eclipse.ui, org.eclipse.core.runtime
