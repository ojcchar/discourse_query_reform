BUG REPORT TITLE:
Container attempts to refresh children on project that is not open

BUG REPORT DESCRIPTION:
Take an existing simple project on disk and import the project into the workspace by performing a simple create with code like:

String folder = "/temp";//$NON-NLS-1$
String projName = "project";//$NON-NLS-1$
IProjectDescription description = ResourcesPlugin.getWorkspace
().
loadProjectDescription(projPath);
IProject project = ResourcesPlugin.getWorkspace().
getRoot().
getProject
(description.getName());
project.create(description, new NullProgressMonitor());

Do not open the project with the project.open() API.
This is the key to the issue.

Now create a project either by API or UI and open it.
Or simply switch to the
Java perspective.

A background refresh job has now been started for the closed project, but it never finishes and is stuck in an infinite loop.

I believe the offending code is in the class org.eclipse.core.internal.resources.Container.

The members() method is excuting if (info.isSet(ICoreConstants.M_CHILDREN_UNKNOWN))
workspace.refreshManager.refresh(this);

because the projects members are not known.
Both the AliasManager and the Java
Perspective are calling members on the IProject.

If you override this method in Project and do not refresh for closed projects, the problem goes away.

Our particular use case is that we are loading existing Java projects on disk by performing a create, but never an open.
On the next UI gesture, we get refresh infinite loops, one for each closed project.

We want the projects in the workspace, so we create them but do not open them, as open is very expensive.
The end user will open them by using the open project UI when needed.

This worked fine in Eclipse 3.0.
