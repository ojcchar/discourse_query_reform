BUG REPORT TITLE:
Update involving folder case change failed

BUG REPORT DESCRIPTION:
I got this when updating org.eclipse.core.runtime.
I thought we did all the deletions first so this would work.

Here' the exception

Error 2005-03-29 15:05:29.904 Problem creating folder: /org.eclipse.core.runtime/src/org/eclipse/core/runtime/dynamichelpers.
A resource exists with a different case: /org.eclipse.core.runtime/src/org/eclipse/core/runtime/dynamicHelpers.
org.eclipse.core.internal.resources.ResourceException: A resource exists with a different case: /org.eclipse.core.runtime/src/org/eclipse/core/runtime/dynamicHelpers.
at org.eclipse.core.internal.resources.Resource.checkDoesNotExist
(Resource.java:274)
at org.eclipse.core.internal.resources.Folder.assertCreateRequirements
(Folder.java:27)
at org.eclipse.core.internal.resources.Folder.create(Folder.java:90)
at org.eclipse.core.internal.resources.Folder.create(Folder.java:121)
at org.eclipse.team.internal.ccvs.core.resources.EclipseFolder.mkdir
(EclipseFolder.java:100)
at org.eclipse.team.internal.ccvs.core.CVSSyncInfo.makeInSync
(CVSSyncInfo.java:284)
at org.eclipse.team.internal.ccvs.ui.subscriber.CVSSubscriberOperation.makeInSync
(CVSSubscriberOperation.java:137)
at org.eclipse.team.internal.ccvs.ui.subscriber.CVSSubscriberOperation.makeInSync
(CVSSubscriberOperation.java:111)
at org.eclipse.team.internal.ccvs.ui.subscriber.SafeUpdateOperation.safeUpdate
(SafeUpdateOperation.java:227)
at org.eclipse.team.internal.ccvs.ui.subscriber.SafeUpdateOperation.run
(SafeUpdateOperation.java:94)
at org.eclipse.team.internal.ccvs.ui.subscriber.CVSSubscriberOperation$1.
run
(CVSSubscriberOperation.java:72)
at org.eclipse.team.internal.ccvs.core.resources.EclipseSynchronizer.run
(EclipseSynchronizer.java:1403)
at org.eclipse.team.internal.ccvs.ui.subscriber.CVSSubscriberOperation.run
(CVSSubscriberOperation.java:67)
at org.eclipse.team.internal.ccvs.ui.subscriber.SafeUpdateOperation.run
(SafeUpdateOperation.java:74)
at org.eclipse.team.internal.ui.actions.JobRunnableContext.run
(JobRunnableContext.java:146)
at org.eclipse.team.internal.ui.actions.JobRunnableContext$ResourceJob.runInWorksp ace(JobRunnableContext.java:72)
at org.eclipse.core.internal.resources.InternalWorkspaceJob.run
(InternalWorkspaceJob.java:38)
at org.eclipse.core.internal.jobs.Worker.run(Worker.java:67)
