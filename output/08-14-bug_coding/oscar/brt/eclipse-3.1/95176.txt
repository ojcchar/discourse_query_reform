BUG REPORT TITLE:
Jobs still running after platform shutdown

BUG REPORT DESCRIPTION:
build I20050513-0010

The console output for this build has a couple of entries like:

[java] !
ENTRY org.eclipse.core.runtime 2 2 2005-05-13 06:59:12.343
[java] !
MESSAGE Job found still running after platform shutdown.
Jobs should be canceled by the plugin that scheduled them during shutdown:
org.eclipse.team.internal.core.BackgroundEventHandler$1
