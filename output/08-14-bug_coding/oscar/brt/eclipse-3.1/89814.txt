BUG REPORT TITLE:
Replace With > Another Branch or Version: tree allows multi-selection

BUG REPORT DESCRIPTION:
I20050330-0500

Replace With > Another Branch or Version: The tree allows multi-selection, but the operation only considers one of the selected items.
The tree should be in single-selection mode, as the (filtered) table already is.
