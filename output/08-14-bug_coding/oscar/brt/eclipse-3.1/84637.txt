BUG REPORT TITLE:
Ability for builder to get a hold of its command

BUG REPORT DESCRIPTION:
Build: I20050202

ICommand now has API to configure what triggers a builder responds to (bug
60803).
Various builders (external tools and CDT), already have similar mechanisms to avoid auto-builds.
We need to make sure they have the necessary
API to be able to migrate their builders to the new ICommand API.
Possibilities:

1) Give builders a method to get access to their build command.
Now that we support multiple builders with the same name, this isn't trivial for a client to do on their own.

2) Give builders API to get/set the build triggers in their command.
This is a more limited form of 1) but achieves the same result.
