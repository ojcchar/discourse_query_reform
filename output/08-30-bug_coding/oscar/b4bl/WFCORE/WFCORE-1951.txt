BUG REPORT TITLE:
Expose content repository read-content as a GET operation

BUG REPORT DESCRIPTION:
Allows the use of the GET method to obtain the binary stream to a content in the content repository.
