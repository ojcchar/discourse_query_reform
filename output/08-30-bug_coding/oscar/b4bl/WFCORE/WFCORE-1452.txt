BUG REPORT TITLE:
Socket binding resources do not register a requirement for their interface

BUG REPORT DESCRIPTION:
BindingAddHandler doesn't pass it's attributes into the superclass constructor, so the capability references don't get registered by the default AbstractAddStepHandler.recordCapabilitiesAndRequirements.
And BindingAddHandler also doesn't override recordCapabilitiesAndRequirements to do the registration itself.
