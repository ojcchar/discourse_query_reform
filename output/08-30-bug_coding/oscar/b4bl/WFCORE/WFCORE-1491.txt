BUG REPORT TITLE:
WildFly-Core and its build process cannot rely on default JDK JAXP providers

BUG REPORT DESCRIPTION:
In order to ensure consistent WildFly-Core build and runtime behavior on all JDKs it is necessary to explicitly specify which JAXP providers should be used instead of default ones provided by JDK.
