BUG REPORT TITLE:
WFLYCTL0357 warning upon undeploying any deployment

BUG REPORT DESCRIPTION:
EAP produces following warning upon undeployment:

WARN  [org.jboss.as.controller] (management-handler-thread - 4) WFLYCTL0357: Notification of type deployment-undeployed is not described for the resource at the address []

The warning is produced for both managed and unmanaged deployments.
