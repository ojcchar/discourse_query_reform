BUG REPORT TITLE:
Throw exception in a save operation if the POJO's ID field is null and field type is not String, BigInteger or ObjectId.

BUG REPORT DESCRIPTION:
findById with int type id returns null.
