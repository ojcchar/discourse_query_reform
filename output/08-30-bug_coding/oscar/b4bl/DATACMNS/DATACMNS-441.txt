BUG REPORT TITLE:
Analyzing @Query annotated bridge methods lead to NPEs during QueryMethod processing in Java 8

BUG REPORT DESCRIPTION:
We should skip bridge methods instead considering them for @Query annotation inspection, since we cannot extract the generic type parameter from bridge  methods.
