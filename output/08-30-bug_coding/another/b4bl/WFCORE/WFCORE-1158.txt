BUG REPORT TITLE:
Expose the file system space availability

BUG REPORT DESCRIPTION:
Implements a service to expose the file system space availability, run-time metric for known server locations(data, tmp, logs).
