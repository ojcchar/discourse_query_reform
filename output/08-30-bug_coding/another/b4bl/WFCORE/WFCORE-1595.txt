BUG REPORT TITLE:
CLI doesn't autocomplete for unalias command

BUG REPORT DESCRIPTION:
Tab completion for CLI unalias commend doesn't seem to work.
CLI will suggest available option, but doesn't autocomplete.
actual

[standalone@localhost:9990 /] unalias read_<TAB>

read_undertow

[standalone@localhost:9990 /] unalias read_<TAB>

read_undertow

expected

[standalone@localhost:9990 /] unalias read_<TAB>

[standalone@localhost:9990 /] unalias read_undertow
