BUG REPORT TITLE:
domain mode: remove a path entry from a server instance fails

BUG REPORT DESCRIPTION:
Adding a "path" to a "server-config" in domain mode (works fine) and then remove it again results in a NullPointerException
