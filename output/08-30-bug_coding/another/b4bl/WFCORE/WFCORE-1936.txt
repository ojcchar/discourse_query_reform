BUG REPORT TITLE:
Value of parameters "restart-required" for fixed-*port attributes does not match reality for socket-binding and *-destination-outbound-socket-binding in CLI

BUG REPORT DESCRIPTION:
fixed-port attribute of socket-binding and fixed-source-port attributes of *-destination-outbound-socket-binding define in its description that there is not necessary to do reload or restart for any of them.
But reality is different.
If you tries to change such attributes you are informed that reload is necessary.
The attributes are defined as "restart-required" => "no-services", see /socket-binding-group=standard-sockets:read-resource-description(recursive=true)
