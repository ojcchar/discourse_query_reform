BUG REPORT TITLE:
Whitespaces are not removed from dependencies in module add command

BUG REPORT DESCRIPTION:
Running module add --name=foo.bar --resources=foo.jar --dependencies=[org.a, org.b ] will result in following dependencies in module.xml
{{
...
<dependencies>
<module name="org.a"/>
<module name=" org.b "/>
</dependencies>
...
}}
The module name in dependencies should be stripped of leading and trailing whitespaces.
