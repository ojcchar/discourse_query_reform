BUG REPORT TITLE:
Unable to use path in input-stream-index argument in /core-service=patching:patch

BUG REPORT DESCRIPTION:
File system paths should be allowed as arguments for
/core-service=patching:patch(input-stream-index=)
