BUG REPORT TITLE:
Deployment scanner runtime-failure-causes-rollback attribute is not stored to the model

BUG REPORT DESCRIPTION:
The runtime-failure-causes-rollback is never stored to the model in the add op.
