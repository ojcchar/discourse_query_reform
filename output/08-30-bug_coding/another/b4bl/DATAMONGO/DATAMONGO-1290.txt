BUG REPORT TITLE:
@Query annotation with byte[] parameter does not work

BUG REPORT DESCRIPTION:
In a repository, these finders should be equivalent:

Optional<SampleDomainObject> findBySampleData(byte[] sampleDate);

@Query("{ 'sampleData' : ?
0 }")

Optional<SampleDomainObject> findBySampleDateWithAnnotation(byte[] sampleData);

... but only the first works.
Please find an example project with test attached!
