BUG REPORT TITLE:
ReaderInputStream#read(byte[] b, int off, int len) should check for valid parameters

BUG REPORT DESCRIPTION:
If the buffer is null, the method should throw NPE immediately (rather than letting it occur later)
If the offset or length are < 0 or would overflow the buffer, then throw IndexOutOfBoundsException with details of the values.
