BUG REPORT TITLE:
Fix Race Condition in Test

BUG REPORT DESCRIPTION:
Container is stopped before testing mocki to ensure close() wasn't called on the channel.
