BUG REPORT TITLE:
IMethodBinding#isEqualTo(..) returns true for methods in different anonymous classes

BUG REPORT DESCRIPTION:
I20050323-1615

IMethodBinding#isEqualTo(.
.)
returns true for methods in different anonymous classes.
Here, the two run() methods are said to be equal:

public class Try {
Runnable one= new Runnable(){ public void run() {
}
};
Runnable two= new Runnable(){ public void run() {
}
};
}
