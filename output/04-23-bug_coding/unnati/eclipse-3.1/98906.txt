BUG REPORT TITLE:
No rebuild after upgrade

BUG REPORT DESCRIPTION:
N20050608

1. Start with a workspace running with N20050607 and org.eclipse.jdt.core only.
2. Turn autobuild off
3. Shutdown
4. Restart with N20050608
5. Press build
Observe: no full build occurs
