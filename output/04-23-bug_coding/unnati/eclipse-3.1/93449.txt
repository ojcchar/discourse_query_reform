BUG REPORT TITLE:
CVS action enablement is very slow with large number of items

BUG REPORT DESCRIPTION:
20050426 running self hosted with jface and SWT loaded from HEAD.

Profiled with OptimizeIt (likely faster without profiling)

STEPS
1) load win32.x86 plugin
2) Select all of the classes in org.eclipse.swt.internal.win32
3) Pop up the menu

Of the 3662ms it look to show 2404 was spent in CVSAction#selectionChanged
