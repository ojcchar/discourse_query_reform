BUG REPORT TITLE:
Enable/Disable not on context menu for breakpoint groups

BUG REPORT DESCRIPTION:
I have many breakpoints grouped by File.
If I Select-All breakpoints (including groups) the Disable/Enable options do not appear on the context menu.
They probably should as this is the general mechanism for disabling/enabling a bunch of breakpoints.
