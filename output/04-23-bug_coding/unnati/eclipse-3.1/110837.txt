BUG REPORT TITLE:
javax.crypto.KeyAgreement.getInstance(String) throws exception in IDE

BUG REPORT DESCRIPTION:
A call to KeyAgreement.getInstance("DiffieHellman") will throw a
NoSuchAlgorithmException if run from Eclipse but not if it's run directly from the command line.
(JDK 1.5.0_04 or 1.4.2_09)

Here's the code:
--- import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyAgreement;

public class KeyAgreementProblem
{ public static void main(String[] args) throws NoSuchAlgorithmException
{
KeyAgreement ka = KeyAgreement.getInstance("DiffieHellman");
System.out.println(ka);
}
}
---
Running from the command line goes like this (command line then output below):

C:\...\workspace\sandbox>java -classpath bin KeyAgreementProblem javax.crypto.KeyAgreement@141d683

---

Running in Eclipse with the same JDK and environment produces this exception:

Exception in thread "main" java.security.NoSuchAlgorithmException: Algorithm
DiffieHellman not available at javax.crypto.KeyAgreement.getInstance(DashoA12275)
	at KeyAgreementProblem.main(KeyAgreementProblem.java:8)
