BUG REPORT TITLE:
JavaElement.getURLContents(...) leaves file open

BUG REPORT DESCRIPTION:
I20051123

The AttachedJavadocTests sometimes fail because JavaElement.getURLContents(...) leaves a file open and the test cannot delete the project in the tearDownSuite() method because of that.

More specifcally URLConnection.getContentEncoding() opens a file stream on the doc.zip and never closes it.
So this looks like a bug in the implementation of URLConnection.getContentEncoding().
