BUG REPORT TITLE:
Opening a closed project causes exception in Breakpoints View

BUG REPORT DESCRIPTION:
1. Create multiple projects and files.
2. Add some breakpoints in different projects
3. Show breakpoints with Project Group.
4. Close one of the projects.
5. Reopen the closed project.

Result in the following exception and Breakpoints View can no longer show breakpoint unless the workbench is restarted:
org.eclipse.swt.SWTException: Failed to execute runnable
(java.lang.ArrayIndexOutOfBoundsException)
at java.lang.Throwable.
<init>(Throwable.java)
at java.lang.Throwable.
<init>(Throwable.java)
	at org.eclipse.swt.SWTException.<init>(SWTException.java:84)
	at org.eclipse.swt.SWT.error(SWT.java:2940)
at org.eclipse.swt.SWT.error(SWT.java)
at org.eclipse.swt.widgets.Synchronizer.runAsyncMessages
(Synchronizer.java)
at org.eclipse.swt.widgets.Display.runAsyncMessages(Display.java)
at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java)
	at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1601)
	at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1565)
at org.eclipse.ui.internal.Workbench.createAndRunWorkbench
(Workbench.java:315)
	at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:143)
at org.eclipse.ui.internal.ide.IDEApplication.run
(IDEApplication.java:103)
at org.eclipse.core.internal.runtime.PlatformActivator$1.
run
(PlatformActivator.java:226)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run
(EclipseStarter.java:372)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run
(EclipseStarter.java:161)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:85)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:58)
at sun.reflect.DelegatingMethodAccessorImpl.invoke
(DelegatingMethodAccessorImpl.java:60)
	at java.lang.reflect.Method.invoke(Method.java:391)
	at org.eclipse.core.launcher.Main.invokeFramework(Main.java:330)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:274)
	at org.eclipse.core.launcher.Main.run(Main.java:977)
	at org.eclipse.core.launcher.Main.main(Main.java:952)
Caused by: java.lang.ArrayIndexOutOfBoundsException at java.lang.Throwable.
<init>(Throwable.java)
at java.lang.Throwable.
<init>(Throwable.java)
at java.lang.ArrayIndexOutOfBoundsException.
<init>
(ArrayIndexOutOfBoundsException.java)
at org.eclipse.debug.internal.ui.views.breakpoints.BreakpointsViewEventHandler$1.
ru n(BreakpointsViewEventHandler.java:91)
	at org.eclipse.swt.widgets.RunnableLock.run(RunnableLock.java:35)
at org.eclipse.swt.widgets.Synchronizer.runAsyncMessages
(Synchronizer.java)
at org.eclipse.swt.widgets.Display.runAsyncMessages(Display.java)
at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java)
	at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1601)
	at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1565)
at org.eclipse.ui.internal.Workbench.createAndRunWorkbench
(Workbench.java:315)
	at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:143)
at org.eclipse.ui.internal.ide.IDEApplication.run
(IDEApplication.java:103)
at org.eclipse.core.internal.runtime.PlatformActivator$1.
run
(PlatformActivator.java:226)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run
(EclipseStarter.java:372)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run
(EclipseStarter.java:161)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:85)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:58)
at sun.reflect.DelegatingMethodAccessorImpl.invoke
(DelegatingMethodAccessorImpl.java:60)
	at java.lang.reflect.Method.invoke(Method.java:391)
	at org.eclipse.core.launcher.Main.invokeFramework(Main.java:330)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:274)
	at org.eclipse.core.launcher.Main.run(Main.java:977)
	at org.eclipse.core.launcher.Main.main(Main.java:952)
