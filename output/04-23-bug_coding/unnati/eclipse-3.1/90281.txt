BUG REPORT TITLE:
Replace With Uncommitted changes dialog problems

BUG REPORT DESCRIPTION:
It comes up incorrectly sized, i.e. the dialog's title is cropped.

The upper block should be right aligned with the Cancel button as should be the list and Deselect All button.
