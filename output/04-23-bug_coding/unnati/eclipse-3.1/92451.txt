BUG REPORT TITLE:
code assist failure: new+cast+arrays

BUG REPORT DESCRIPTION:
I20050419
J2SE 5 (but also fails in JDK 1.4)

Code assist fails in the following (self-contained) class (see comments for line of error)

public class Test { public static void main(String[] args) { java.util.List elements = null;
// code assist works on this line new Test(Test.toStrings((Test[])elements.toArray(new Test
[0])));
//code assist fails on this line
} public Test(Object object) {
} public static Object toStrings(Test[] objects) { return null;
}
}
