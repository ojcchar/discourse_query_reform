BUG REPORT TITLE:
[SSH2] Scary & strange looking dialog

BUG REPORT DESCRIPTION:
3.1 M3

I use ssh2 to connect to the cvs server.
My password expired over the weekend.
As a result I get the attached dialog which tells a lot of scary warnings but does not point to the real cause namely that the password has (or might have)
expired.
