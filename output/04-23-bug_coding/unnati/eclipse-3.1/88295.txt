BUG REPORT TITLE:
[1.5][assist] too many completion on enum case label

BUG REPORT DESCRIPTION:
20050315

When completing in enum case label 'R<|>' only enum constants should be offered.

public class Class3 {

enum Color {
BLUE, WHITE, RED;
} void select(Color c) { switch(c){ case BLUE :
case WHITE:
case R<|>
}
}
}
