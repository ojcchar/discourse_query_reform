BUG REPORT TITLE:
extract method trips up with generics and final variables

BUG REPORT DESCRIPTION:
if you extract method where indicated below.
you will see that the extracted method declares its paramater with too many final modifiers:
-------------------------------------

package p;

class Container<T>
{ private final T m_t;

public Container(T t)
{ m_t = t;
}

T get()
{ return m_t;
}
}

class GenericContainer
{ private final Container<?> m_c;

public GenericContainer(Container<?> c)
{ m_c = c;
}

public Container<?> getC()
{ return m_c;
}
}

public class A
{
GenericContainer createContainer()
{ final Container<String> innerContainer = new Container<String>("hello");
final Container<Container<String>> outerContainer = new
Container<Container<String>>(innerContainer);
return new GenericContainer(outerContainer);
} void method()
{ final GenericContainer createContainer = createContainer();
@SuppressWarnings("unchecked")
final Container<Container<String>> c = (Container<Container<String>>)
createContainer.getC();
//extract method from here final Container<String> container = c.get();
final String string = container.get();
//to here
}
}
----------------------------------------------- results in
-----------------------------------------------

package p;

class Container<T>
{ private final T m_t;

public Container(T t)
{ m_t = t;
}

T get()
{ return m_t;
}
}

class GenericContainer
{ private final Container<?> m_c;

public GenericContainer(Container<?> c)
{ m_c = c;
}

public Container<?> getC()
{ return m_c;
}
}

public class A
{
GenericContainer createContainer()
{ final Container<String> innerContainer = new Container<String>("hello");
final Container<Container<String>> outerContainer = new
Container<Container<String>>(innerContainer);
return new GenericContainer(outerContainer);
} void method()
{ final GenericContainer createContainer = createContainer();
@SuppressWarnings("unchecked")
final Container<Container<String>> c = (Container<Container<String>>)
createContainer.getC();
//extract method from here extractedMethod(c);
//to here
}

private void extractedMethod(final final final Container<Container<String>> c)
{ final Container<String> container = c.get();
final String string = container.get();
}
}
-----------------------------------------------------------

notice the 3 final modifiers in the extractedMethod signature.
