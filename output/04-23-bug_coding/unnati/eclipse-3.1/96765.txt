BUG REPORT TITLE:
Code completion does not work in enum constants

BUG REPORT DESCRIPTION:
Code completion does not work in enum constants:
public enum MyEnum {

A(){ public String toString() {
// no code completion here! return null;
}
};

}
