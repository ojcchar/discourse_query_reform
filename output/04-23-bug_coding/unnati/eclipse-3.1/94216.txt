BUG REPORT TITLE:
Open type does not work for generic types

BUG REPORT DESCRIPTION:
interface IGeneric<T> {
} public class Generic<T> implements IGeneric<T> { public static void main(String[] args) {
IGeneric<String> gen= new Generic<String>();
System.out.println();  // <-- breakpoint here
}
}

Try to do 'open declaring type' or 'open concrete type' for 'gen' at the breakpoint, nothing happens.
