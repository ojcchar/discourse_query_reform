BUG REPORT TITLE:
Variables View does not show all children if same instance is expanded twice

BUG REPORT DESCRIPTION:
N20050602-0010

- Debug the class below with the breakpoint where indicated.
- Expand t1 in the Variables view -> expands fine and shows fID and fName.
- Expand t2 -> only child fID is shown

package xy;
public class Try {
String fName;
int fID;
public Try(String name, int id) { fName= name;
fID= id;
} public static void main(String[] args) {
Try t= new Try("Hello", 5);
callee(t, t);
} static void callee(Try t1, Try t2) { boolean same= t1.equals(t2); //breakpoint here
}
}
