BUG REPORT TITLE:
popup menu glitches for Open actions

BUG REPORT DESCRIPTION:
1. The 'Open Type' context menu item is currently the last item in the context menu.
It should be moved up, preferable to the firs position.

2. Open xxx items do not appear in the context menu of packages and bundle IDs in the manifest.mf source page.
F3 works fine though.
