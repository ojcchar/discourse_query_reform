BUG REPORT TITLE:
ConcurrentModificationException on shutdown

BUG REPORT DESCRIPTION:
Build: I20050419

I found the following exception in my log.
It occurred while I was shutting down my workspace last night:

org.osgi.framework.BundleException: Exception in org.eclipse.team.internal.core.TeamPlugin.stop() of bundle org.eclipse.team.core.
at
org.eclipse.osgi.framework.internal.core.BundleContextImpl.stop(BundleContextImpl.java:1061)
at
org.eclipse.osgi.framework.internal.core.BundleHost.stopWorker(BundleHost.java:402)
at
org.eclipse.osgi.framework.internal.core.AbstractBundle.stop(AbstractBundle.java:410)
at
org.eclipse.core.runtime.adaptor.BundleStopper.basicStopBundles(BundleStopper.java:73)
at
org.eclipse.core.runtime.adaptor.BundleStopper.stopBundles(BundleStopper.java:62)
at
org.eclipse.core.runtime.adaptor.EclipseAdaptor.frameworkStopping(EclipseAdaptor.java:695)
	at org.eclipse.osgi.framework.internal.core.Framework.shutdown(Framework.java:502)
at
org.eclipse.osgi.framework.internal.core.SystemBundle$1.run(SystemBundle.java:171)
	at java.lang.Thread.run(Thread.java:813)
Caused by: java.util.ConcurrentModificationException
	at java.util.HashMap$HashIterator.nextEntry(HashMap.java:930)
	at java.util.HashMap$KeyIterator.next(HashMap.java:966)
at
org.eclipse.team.internal.core.ResourceVariantCache.shutdown(ResourceVariantCache.java:102)
	at org.eclipse.team.internal.core.TeamPlugin.stop(TeamPlugin.java:81)
at
org.eclipse.osgi.framework.internal.core.BundleContextImpl$3.run(BundleContextImpl.java:1045)
	at java.security.AccessController.doPrivileged(AccessController.java:202)
at
org.eclipse.osgi.framework.internal.core.BundleContextImpl.stop(BundleContextImpl.java:1041)
at
org.eclipse.osgi.framework.internal.core.BundleHost.stopWorker(BundleHost.java:402)
at
org.eclipse.osgi.framework.internal.core.AbstractBundle.stop(AbstractBundle.java:410)
at
org.eclipse.core.runtime.adaptor.BundleStopper.basicStopBundles(BundleStopper.java:73)
at
org.eclipse.core.runtime.adaptor.BundleStopper.stopBundles(BundleStopper.java:62)
at
org.eclipse.core.runtime.adaptor.EclipseAdaptor.frameworkStopping(EclipseAdaptor.java:695)
	at org.eclipse.osgi.framework.internal.core.Framework.shutdown(Framework.java:502)
at
org.eclipse.osgi.framework.internal.core.SystemBundle$1.run(SystemBundle.java:171)
	at java.lang.Thread.run(Thread.java:813)
