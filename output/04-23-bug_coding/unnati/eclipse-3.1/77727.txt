BUG REPORT TITLE:
OK not enabled on empty input when run on separate VM

BUG REPORT DESCRIPTION:
test case:
<input>Press Enter to continue</input>

If run in the same VM test case works, if run in separate vm OK, the user must enter something in the text field before the OK button is enabled.
