BUG REPORT TITLE:
[ViewMgmt] updating of Search's title line flashes

BUG REPORT DESCRIPTION:
3.1M7 test build, observed on solaris-motif and hpux

(I'm logging this with UI because I think it's the framework doing a lot of work)

- the visibility of this bug depends on the slowness of the machine
- get a few java packages into your workspace
- do a Java Search on Method Declarations of toString
- if you're on a slow machine then it will take a while to populate the Search view with the results
- watch the Search view's "'toString' - xxx declarations in workspace" title line, and note how it flashes on each content update

Each update seems to be doing a lot of work; I've verified that updating a label's content does not flash in a stand-alone example.
