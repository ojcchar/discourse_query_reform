BUG REPORT TITLE:
Externalizing manifest.mf files are one character off

BUG REPORT DESCRIPTION:
using the latest pde.ui and pde.core from head:

1. Check out pde.ui.tests from head.
2. Try to externalize strings from plugin in 1.
3. In the wizard, select the manifest.mf and click on any entry in the
Substitution Key column of the property sheet.

Notice how the text highlighted in the source viewer is one character off.

If you press Finish, the text edit operations result in bogus files.
