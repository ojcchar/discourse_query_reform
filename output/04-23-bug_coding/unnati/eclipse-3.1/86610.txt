BUG REPORT TITLE:
Lots of Worker threads (around 100)

BUG REPORT DESCRIPTION:
3.1 M5a

When running all our tests together (see attached class AllZRZHTests.java) on our buildmachine (Linux Fedora Core using Sun VM 1.4.2_06) we see that more and more Worker threads get created but not terminated.

When the last test cases are reached, tons of "java.lang.StackOverflowError" are written to the console but no stack trace.
The .log is empty.
Note:
- this worked with 3.1 M4 i.e. no errors written to the Console.
- this still works when using another VM but the amount of Workers is huge too

When running in the Debugger the VM often "dies" (or freezes) and the tests never finish.

I put a breakpoint into the Worker constructor and it looked as if the
DecorationScheduler is causing the creation of the Worker in the UI/main thread.
I would expect that there's an upper bound for the Workers and unused Workers get removed again.
