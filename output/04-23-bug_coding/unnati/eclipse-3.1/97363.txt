BUG REPORT TITLE:
Export deployable plug-ins always JARs

BUG REPORT DESCRIPTION:
build I20050527-0900

- org.eclipse.sdk loaded from CVS
- File > Export > Deployable plug-ins and fragments
- Ensure "Package plug-ins as individual JAR archives" is unchecked
- pick a dest dir, e.g. d:\temp\export

It gets exported as a jar to d:\temp\export\plugins\org.eclipse.sdk_3.1.0.jar.

If another plug-in is selected, it gets exported as a separate JAR too.

Should export as unjar'ed plugin directories if "Package plug-ins as individual
JAR archives" is unchecked.
