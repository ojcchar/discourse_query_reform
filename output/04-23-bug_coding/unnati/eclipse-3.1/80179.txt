BUG REPORT TITLE:
[DynamicUI] view-related warning messages in the log

BUG REPORT DESCRIPTION:
See https://bugs.eclipse.org/bugs/show_bug.cgi?id=80174#c2 for details.
