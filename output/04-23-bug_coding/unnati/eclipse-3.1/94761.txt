BUG REPORT TITLE:
Commit still occurs if no changes after a reconnect

BUG REPORT DESCRIPTION:
When reconnecting a project an it's contents match that of the selected branch, the sharing wizard will still try to perform a commit.
The user is prompted that there are no changes to commit, which is good, but the commit does not need to occur at all.
