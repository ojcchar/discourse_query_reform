BUG REPORT TITLE:
Exception when trying to evaluate in Snippet Editor

BUG REPORT DESCRIPTION:
Testcase:

Collection<String> c = new ArrayList<String>();
c.add("a");
c.add("b");
c.add("c");

for (Iterator<String> i = c.iterator(); i.hasNext(); )
if (i.next().
length() == 4)
{
String x = i.next();
System.out.println(x);
} return c;

I added the testcase to the snippet editor.
I then did a "Set Imports..." to include java.util.
* to resolve collection and iterator.
Trying a "Display" or "Inspect" resulted in the following error in the console:

java.lang.VerifyError: arguments are not type compatible (class: CodeSnippet_2 method: run()V) at pc: 57
	at java.lang.Class.verifyImpl(Native Method)
	at java.lang.Class.verify(Class.java:254)
	at java.lang.Class.initialize(Class.java:317)
	at java.lang.Class.forNameImpl(Native Method)
	at java.lang.Class.forName(Class.java:128)
at org.eclipse.jdt.internal.debug.ui.snippeteditor.ScrapbookMain1.eval
(ScrapbookMain1.java:20)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:46)
at sun.reflect.DelegatingMethodAccessorImpl.invoke
(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:611)
at org.eclipse.jdt.internal.debug.ui.snippeteditor.ScrapbookMain.evalLoop
(ScrapbookMain.java:54)
at org.eclipse.jdt.internal.debug.ui.snippeteditor.ScrapbookMain.main
(ScrapbookMain.java:35)
