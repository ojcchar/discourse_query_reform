BUG REPORT TITLE:
Decorator errors during product export.

BUG REPORT DESCRIPTION:
This is on 3.1 M7.

When I run the product export wizard for a multiple windows/mac/linux build from my .
product file there is always an error dialog:

"Problems occurred while refreshing local changes" and a log entry:

!
ENTRY org.eclipse.core.resources 4 1 2005-05-15 13:31:39.765
!
MESSAGE Problems occurred while refreshing local changes
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.765
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/wizards/resources does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.765
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/wizards/learningdesign does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.765
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/views/resources does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.765
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/views/project does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.765
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/views/browser does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/ui/widgets does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/ui/viewers does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/ui/dialogs does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/preferences/general does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/preferences/editors does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/editors/text does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/editors/resources does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/editors/metadata does not exist.
!
SUBENTRY 1 org.eclipse.core.resources 4 368 2005-05-15 13:31:39.812
!
MESSAGE Resource /uk.ac.reload.straker/temp.folder/reload- straker.jar.bin/uk/ac/reload/straker/editors/learningdesign does not exist.

Somewhere a background refresh is happening and complaining that these temp files do not exist.
I have "Refresh Automatically" turned on for my workbench preferences.
If I turn this off, the error does not occur.
