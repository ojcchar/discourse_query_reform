BUG REPORT TITLE:
Error evaluating logical structure value for Map in Java 5.0

BUG REPORT DESCRIPTION:
public class Test { public static void main(String[] args) {
Map<String, Integer> map= new HashMap<String, Integer>();
System.out.println();     // <-- breakpoint here
}
}

I get "Error: The method entrySet() is undefined for the type Map__" when I expand map in the variables view.
