BUG REPORT TITLE:
attempt to show external doc for macrodef call

BUG REPORT DESCRIPTION:
I20050330-0500

In an attempt to show external javadoc for a "prcoess" target in the following file, a web browser is opened to a non-existant URL.
"process" is actually a macrodef call, not a target.

Not sure if the Ant editor can resolve this before attempting to open the browser.

<?xml version="1.0"?>
<!
-- ======================================================================
Feb 4, 2005 2:25:43
PM

EclipseCon
Debug Tutorial Exercises
DWright
URL to check out for simplifying build file:
http://www.oracle.com/technology/pub/articles/bodewig_ant1.6.html
====================================================================== -->
<project name="EclipseCon" default="generateAll">
<description>
Debug Tutorial Exercises
</description>

<taskdef name="preprocess" classname="example.debug.util.tasks.PreProcessor" classpath=".
.
/bin" />
<property name="destBase" location="c:\temp\example" />
<property name="workspace" location="c:\eclipse3.1\dev" />
<property name="coreSource" location="${workspace}
\example.debug.core" />
<property name="uiSource" location="${workspace}\example.debug.ui" />

<!
-- ================================= target: generateAll
================================= -->
<target name="generateAll" description="--> Debug Tutorial Exercises">
<!
-- = = = = = = = = = = = = = = = = = macrodef: process
= = = = = = = = = = = = = = = = = -->
<macrodef name="process">
<attribute name="destdir"/>
<attribute name="symbols"/>
<sequential>
<delete dir="@{destdir}"/>
<mkdir dir="@{destdir}\example.debug.core"/>
<mkdir dir="@{destdir}\example.debug.ui"/>
<preprocess destdir="@{destdir}\example.debug.core" symbols="@
{symbols}">
<fileset dir="${coreSource}">
<exclude name="**/*.class"/>
</fileset>
</preprocess>
<preprocess destdir="@{destdir}\example.debug.ui" symbols="@
{symbols}">
<fileset dir="${uiSource}">
<exclude name="**/*.class"/>
</fileset>
</preprocess>
</sequential>
</macrodef>

<process destdir="${destBase}\exercise1.1" symbols="ex1"/>
<process destdir="${destBase}\exercise2.1" symbols="ex2"/>
<process destdir="${destBase}\exercise3.1" symbols="ex3"/>
<process destdir="${destBase}\exercise4.1" symbols="ex4"/>
<process destdir="${destBase}\exercise5.1" symbols="ex5"/>
<process destdir="${destBase}\exercise6.1" symbols="ex6"/>
<process destdir="${destBase}\exercise7.1" symbols="ex7"/>
</target>

</project>
