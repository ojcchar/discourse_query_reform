BUG REPORT TITLE:
[Markers] Quick fix computation done in UI thread with no progress

BUG REPORT DESCRIPTION:
Build: I20051101-0010

1) Configure the problems view to be showing several warnings of the same type
(say about 100).
2) Ctrl+A to select all problems
3) Right click in problems view, and select "Quick Fix"

-> The UI freezes with no painting, sometimes for ten seconds or more, while the quick fixes are computed.
There should minimally be a busy indicator, and ideally move into a modal context thread to avoid freezing the UI for long periods.
Here is a sample stack trace during one of these freezes:

"main" (TID:0x00119700, sys_thread_t:0x00235A6C, state:CW, native
ID:0x00000720) prio=6
          at java/io/FileInputStream.open(Native Method)
          at java/io/FileInputStream.<init>(FileInputStream.java:129)
at
org/eclipse/core/internal/filesystem/local/LocalFile.openInputStream(LocalFile.java:307)
at
org/eclipse/core/internal/resources/ContentDescriptionManager$LazyFileInputStream.ensureOpened(ContentDescriptionManager.java:154)
at
org/eclipse/core/internal/resources/ContentDescriptionManager$LazyFileInputStream.read(ContentDescriptionManager.java:166)
          at java/io/InputStream.read(InputStream.java:109)
at
org/eclipse/core/internal/content/LazyInputStream.loadBlock(LazyInputStream.java:99)
at
org/eclipse/core/internal/content/LazyInputStream.ensureAvailable(LazyInputStream.java:64)
at
org/eclipse/core/internal/content/LazyInputStream.read(LazyInputStream.java:120)
at
org/eclipse/core/internal/content/TextContentDescriber.getByteOrderMark(TextContentDescriber.java:65)
at
org/eclipse/core/internal/content/TextContentDescriber.describe(TextContentDescriber.java:47)
at
org/eclipse/core/internal/content/ContentType.describe(ContentType.java:161)
at
org/eclipse/core/internal/content/ContentType.internalGetDescriptionFor(ContentType.java:458)
at
org/eclipse/core/internal/content/ContentTypeCatalog.getDescriptionFor(ContentTypeCatalog.java:315)
at
org/eclipse/core/internal/content/ContentTypeCatalog.getDescriptionFor(ContentTypeCatalog.java:319)
at
org/eclipse/core/internal/content/ContentTypeMatcher.getDescriptionFor(ContentTypeMatcher.java:87)
at
org/eclipse/core/internal/resources/ContentDescriptionManager.readDescription(ContentDescriptionManager.java:400)
at
org/eclipse/core/internal/resources/ContentDescriptionManager.getDescriptionFor(ContentDescriptionManager.java:339)
at
org/eclipse/core/internal/resources/File.internalGetCharset(File.java:252)
          at org/eclipse/core/internal/resources/File.getCharset(File.java:213)
          at org/eclipse/core/internal/resources/File.getCharset(File.java:200)
at
org/eclipse/jdt/internal/core/util/Util.getResourceContentsAsCharArray(Util.java:1020)
at
org/eclipse/jdt/internal/core/CompilationUnit.openBuffer(CompilationUnit.java:1003)
at
org/eclipse/jdt/internal/core/CompilationUnit.buildStructure(CompilationUnit.java:104)
          at org/eclipse/jdt/internal/core/Openable.generateInfos(Openable.java:232)
at
org/eclipse/jdt/internal/core/SourceRefElement.generateInfos(SourceRefElement.java:114)
at
org/eclipse/jdt/internal/core/JavaElement.openWhenClosed(JavaElement.java:489)
at
org/eclipse/jdt/internal/core/JavaElement.getElementInfo(JavaElement.java:233)
at
org/eclipse/jdt/internal/core/JavaElement.getElementInfo(JavaElement.java:219)
at
org/eclipse/jdt/internal/core/SearchableEnvironment.find(SearchableEnvironment.java:120)
at
org/eclipse/jdt/internal/core/SearchableEnvironment.findType(SearchableEnvironment.java:188)
at
org/eclipse/jdt/internal/core/CancelableNameEnvironment.findType(CancelableNameEnvironment.java:45)
at
org/eclipse/jdt/internal/compiler/lookup/LookupEnvironment.askForType(LookupEnvironment.java:119)
at
org/eclipse/jdt/internal/compiler/lookup/PackageBinding.getType(PackageBinding.java:126)
at
org/eclipse/jdt/internal/compiler/lookup/Scope.findType(Scope.java:1385)
at
org/eclipse/jdt/internal/compiler/lookup/Scope.getTypeOrPackage(Scope.java:2489)
          at org/eclipse/jdt/internal/compiler/lookup/Scope.getType(Scope.java:2211)
at
org/eclipse/jdt/internal/compiler/ast/SingleTypeReference.getTypeBinding(SingleTypeReference.java:41)
at
org/eclipse/jdt/internal/compiler/ast/TypeReference.resolveType(TypeReference.java:126)
at
org/eclipse/jdt/internal/compiler/ast/LocalDeclaration.resolve(LocalDeclaration.java:148)
          at org/eclipse/jdt/internal/compiler/ast/Block.resolve(Block.java:101)
at
org/eclipse/jdt/internal/compiler/ast/IfStatement.resolve(IfStatement.java:227)
          at org/eclipse/jdt/internal/compiler/ast/Block.resolve(Block.java:101)
at
org/eclipse/jdt/internal/compiler/ast/IfStatement.resolve(IfStatement.java:225)
          at org/eclipse/jdt/internal/compiler/ast/Block.resolve(Block.java:101)
at
org/eclipse/jdt/internal/compiler/ast/IfStatement.resolve(IfStatement.java:225)
at
org/eclipse/jdt/internal/compiler/ast/AbstractMethodDeclaration.resolveStatements(AbstractMethodDeclaration.java:419)
at
org/eclipse/jdt/internal/compiler/ast/MethodDeclaration.resolveStatements(MethodDeclaration.java:178)
at
org/eclipse/jdt/internal/compiler/ast/AbstractMethodDeclaration.resolve(AbstractMethodDeclaration.java:397)
at
org/eclipse/jdt/internal/compiler/ast/TypeDeclaration.resolve(TypeDeclaration.java:1082)
at
org/eclipse/jdt/internal/compiler/ast/TypeDeclaration.resolve(TypeDeclaration.java:1131)
at
org/eclipse/jdt/internal/compiler/ast/CompilationUnitDeclaration.resolve(CompilationUnitDeclaration.java:349)
at
org/eclipse/jdt/core/dom/CompilationUnitResolver.resolve(CompilationUnitResolver.java:856)
at
org/eclipse/jdt/core/dom/CompilationUnitResolver.resolve(CompilationUnitResolver.java:498)
at
org/eclipse/jdt/core/dom/ASTParser.internalCreateAST(ASTParser.java:789)
          at org/eclipse/jdt/core/dom/ASTParser.createAST(ASTParser.java:588)
at
org/eclipse/jdt/internal/ui/javaeditor/ASTProvider$1.run(ASTProvider.java:566)
at
org/eclipse/core/internal/runtime/InternalPlatform.run(InternalPlatform.java:1044)
          at org/eclipse/core/runtime/Platform.run(Platform.java:783)
at
org/eclipse/jdt/internal/ui/javaeditor/ASTProvider.createAST(ASTProvider.java:563)
at
org/eclipse/jdt/internal/ui/javaeditor/ASTProvider.getAST(ASTProvider.java:493)
at
org/eclipse/jdt/internal/ui/text/correction/AssistContext.getASTRoot(AssistContext.java:72)
at
org/eclipse/jdt/internal/ui/text/correction/LocalCorrectionsSubProcessor.addUnnecessaryCastProposal(LocalCorrectionsSubProcessor.java:541)
at
org/eclipse/jdt/internal/ui/text/correction/QuickFixProcessor.process(QuickFixProcessor.java:402)
at
org/eclipse/jdt/internal/ui/text/correction/QuickFixProcessor.getCorrections(QuickFixProcessor.java:215)
at
org/eclipse/jdt/internal/ui/text/correction/JavaCorrectionProcessor$SafeCorrectionCollector.safeRun(JavaCorrectionProcessor.java:310)
at
org/eclipse/jdt/internal/ui/text/correction/JavaCorrectionProcessor$SafeCorrectionProcessorAccess.run(JavaCorrectionProcessor.java:275)
at
org/eclipse/core/internal/runtime/InternalPlatform.run(InternalPlatform.java:1044)
          at org/eclipse/core/runtime/Platform.run(Platform.java:783)
at
org/eclipse/jdt/internal/ui/text/correction/JavaCorrectionProcessor$SafeCorrectionProcessorAccess.process(JavaCorrectionProcessor.java:265)
at
org/eclipse/jdt/internal/ui/text/correction/JavaCorrectionProcessor.collectCorrections(JavaCorrectionProcessor.java:392)
at
org/eclipse/jdt/internal/ui/text/correction/CorrectionMarkerResolutionGenerator.getResolutions(CorrectionMarkerResolutionGenerator.java:157)
at
org/eclipse/ui/internal/ide/registry/MarkerHelpRegistry.getResolutions(MarkerHelpRegistry.java:200)
at
org/eclipse/ui/views/markers/internal/MarkerResolutionWizard.determinePages(MarkerResolutionWizard.java:211)
at
org/eclipse/ui/views/markers/internal/ActionResolveMarker$1.runInUIThread(ActionResolveMarker.java:67)
          at org/eclipse/ui/progress/UIJob$1.run(UIJob.java:93)
          at org/eclipse/swt/widgets/RunnableLock.run(RunnableLock.java:35)
at
org/eclipse/swt/widgets/Synchronizer.runAsyncMessages(Synchronizer.java:118)
          at org/eclipse/swt/widgets/Display.runAsyncMessages(Display.java:3213)
          at org/eclipse/swt/widgets/Display.readAndDispatch(Display.java:2859)
