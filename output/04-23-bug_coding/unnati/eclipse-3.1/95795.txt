BUG REPORT TITLE:
NPE in org.eclipse.osgi.internal.resolver.StateHelperImpl.isSystemExport when starting some of my workspaces

BUG REPORT DESCRIPTION:
N20050518_0010

When starting some of my workspace I get the following NPE in

java.lang.NullPointerException at
org.eclipse.osgi.internal.resolver.StateHelperImpl.isSystemExport(StateHelperImpl.java:190)
at
org.eclipse.osgi.internal.resolver.StateHelperImpl.getPackages(StateHelperImpl.java:176)
at
org.eclipse.osgi.internal.resolver.StateHelperImpl.getVisiblePackages(StateHelperImpl.java:165)
at
org.eclipse.pde.internal.core.RequiredPluginsClasspathContainer.retrieveVisiblePackagesFromState(RequiredPluginsClasspathContainer.java:168)
at
org.eclipse.pde.internal.core.RequiredPluginsClasspathContainer.computePluginEntries(RequiredPluginsClasspathContainer.java:118)
at
org.eclipse.pde.internal.core.RequiredPluginsClasspathContainer.getClasspathEntries(RequiredPluginsClasspathContainer.java:99)
at
org.eclipse.jdt.internal.core.JavaModelManager.containerPutIfInitializingWithSameEntries(JavaModelManager.java:294)
at org.eclipse.jdt.core.JavaCore.setClasspathContainer(JavaCore.java:3754)
at
org.eclipse.pde.internal.core.ModelEntry.updateClasspathContainer(ModelEntry.java:110)
at
org.eclipse.pde.internal.core.RequiredPluginsInitializer.initialize(RequiredPluginsInitializer.java:40)
at
org.eclipse.jdt.internal.core.JavaModelManager.initializeContainer(JavaModelManager.java:1543)
at
org.eclipse.jdt.internal.core.JavaModelManager.getClasspathContainer(JavaModelManager.java:1018)
at org.eclipse.jdt.core.JavaCore.getClasspathContainer(JavaCore.java:1316)
at
org.eclipse.jdt.internal.core.JavaProject.getResolvedClasspath(JavaProject.java:2048)
at
org.eclipse.jdt.internal.core.JavaProject.getResolvedClasspath(JavaProject.java:1956)
at org.eclipse.jdt.internal.core.JavaProject.buildStructure(JavaProject.java:341)
at org.eclipse.jdt.internal.core.Openable.generateInfos(Openable.java:233)
at org.eclipse.jdt.internal.core.JavaElement.openWhenClosed(JavaElement.java:488)
at org.eclipse.jdt.internal.core.JavaElement.getElementInfo(JavaElement.java:232)
at org.eclipse.jdt.internal.core.JavaElement.getElementInfo(JavaElement.java:218)
at
org.eclipse.jdt.internal.core.JavaProject.getJavaProjectElementInfo(JavaProject.java:1522)
at org.eclipse.jdt.internal.core.JavaProject.newNameLookup(JavaProject.java:2407)
at
org.eclipse.jdt.internal.core.SearchableEnvironment.<init>(SearchableEnvironment.java:60)
at
org.eclipse.jdt.internal.core.SearchableEnvironment.<init>(SearchableEnvironment.java:74)
at
org.eclipse.jdt.internal.core.CancelableNameEnvironment.<init>(CancelableNameEnvironment.java:26)
at
org.eclipse.jdt.core.dom.CompilationUnitResolver.resolve(CompilationUnitResolver.java:491)
at org.eclipse.jdt.core.dom.ASTParser.internalCreateAST(ASTParser.java:761)
at org.eclipse.jdt.core.dom.ASTParser.createAST(ASTParser.java:570)
at
org.eclipse.jdt.internal.ui.javaeditor.ASTProvider.createAST(ASTProvider.java:556)
at org.eclipse.jdt.internal.ui.javaeditor.ASTProvider.getAST(ASTProvider.java:487)
at
org.eclipse.jdt.internal.ui.viewsupport.SelectionListenerWithASTManager$PartListenerGroup.calculateASTandInform(SelectionListenerWithASTManager.java:165)
at
org.eclipse.jdt.internal.ui.viewsupport.SelectionListenerWithASTManager$3.run(SelectionListenerWithASTManager.java:142)
at org.eclipse.core.internal.jobs.Worker.run(Worker.java:67)

Saw it from various locations (not only from ASTProvider in a Job).
