BUG REPORT TITLE:
Step over (macrodef) resumes Ant debugger

BUG REPORT DESCRIPTION:
I20050509-2010

* in the attached script, I placed a breakpoint on the "generate all" target
* stepped over, over, and into "<process destdir="${destBase}\exercise1.1" symbols="ex1"/>", and "into" again to get into the marcro def
* stepped thur the macrodef using "step over"
* when I stepped over the last line of the macrodef ("preprocess"), the ant script  simply resumed to completion

<?xml version="1.0"?>
<!
-- ======================================================================
Feb 4, 2005 2:25:43 PM

EclipseCon
Debug Tutorial Exercises
DWright
URL to check out for simplifying build file:
http://www.oracle.com/technology/pub/articles/bodewig_ant1.6.html
====================================================================== -->
<project name="EclipseCon" default="generateAll">
<description>
Debug Tutorial Exercises
</description>

<taskdef name="preprocess" classname="example.debug.util.tasks.PreProcessor" classpath=".
.
/bin" />
<property name="destBase" location="\home\dwright\temp\example" />
<property name="workspace" location="\home\dwright\eclipse3.1\dev" />
<property name="coreSource" location="${workspace}\example.debug.core" />
<property name="uiSource" location="${workspace}\example.debug.ui" />

<!
-- ================================= target: generateAll
================================= -->
<target name="generateAll" description="--> Debug Tutorial Exercises">
<!
-- = = = = = = = = = = = = = = = = = macrodef: process
= = = = = = = = = = = = = = = = = -->
<macrodef name="process">
<attribute name="destdir"/>
<attribute name="symbols"/>
<sequential>
<delete dir="@{destdir}"/>
<mkdir dir="@{destdir}\example.debug.core"/>
<mkdir dir="@{destdir}\example.debug.ui"/>
<preprocess destdir="@{destdir}\example.debug.core" symbols="@{symbols}">
<fileset dir="${coreSource}">
<exclude name="**/*.class"/>
</fileset>
</preprocess>
<preprocess destdir="@{destdir}\example.debug.ui" symbols="@{symbols}">
<fileset dir="${uiSource}">
<exclude name="**/*.class"/>
</fileset>
</preprocess>
</sequential>
</macrodef>

<process destdir="${destBase}\exercise1.1" symbols="ex1"/>
<process destdir="${destBase}\exercise2.1" symbols="ex2"/>
<process destdir="${destBase}\exercise3.1" symbols="ex3"/>
<process destdir="${destBase}\exercise4.1" symbols="ex4"/>
<process destdir="${destBase}\exercise5.1" symbols="ex5"/>
<process destdir="${destBase}\exercise6.1" symbols="ex6"/>
<process destdir="${destBase}\exercise7.1" symbols="ex7"/>
</target>

</project>
