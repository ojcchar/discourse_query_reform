BUG REPORT TITLE:
Not every hit is a class

BUG REPORT DESCRIPTION:
The icon for Java-search-participant hits in the Export-Package and Import-
Package manifest headers should be indicated with a package icon, not a class icon.
