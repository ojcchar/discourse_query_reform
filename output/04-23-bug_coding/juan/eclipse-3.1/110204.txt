BUG REPORT TITLE:
add support for Bundle-RequiredExecutionEnvironment

BUG REPORT DESCRIPTION:
Required-ExecutionEnvironment is an OSGi concept that controls whether or not the plugin can be installed based on the running JRE.

PDE should add support for this manifest header in the plug-in manifest editor.
this would probably take the form of a checkbox table with all the possible values.

TomW, please supply a list of JRE values for this header.

Also, during the creation of a plug-in, do we want to ask the user to specify an execution environment?

If so, I think a combo box in the wizard should be sufficient.
They could later add more in the editor if they want to specify more.
