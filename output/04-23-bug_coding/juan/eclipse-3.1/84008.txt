BUG REPORT TITLE:
[DynamicUI] Keyword extension point needs to be dynamic

BUG REPORT DESCRIPTION:
The keyword extension point needs a registry that respects dynamic plugin changes.
