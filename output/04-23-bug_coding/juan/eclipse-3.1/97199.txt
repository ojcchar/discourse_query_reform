BUG REPORT TITLE:
[formatting] Code formatting activation in comments (using <PRE>) is case sensitive

BUG REPORT DESCRIPTION:
The formatter only recognizes "pre" and not other capatilization like "Pre" or
"PRE".

If...
/**
* <pre>
* public Object[] getChildren(Object parentElement) {
*     if (parentElement instanceof MovingBox) {
*         MovingBox box = (MovingBox) parentElement;
*         return concat(box.getBoxes().
toArray(), box.getBooks().
toArray(), box
*                 .
getGames().
toArray());
*     }
*     return EMPTY_ARRAY;
* }
* </pre>
*/
...is changed into...
/**
* <Pre>
* public Object[] getChildren(Object parentElement) {
*     if (parentElement instanceof MovingBox) {
*         MovingBox box = (MovingBox) parentElement;
*         return concat(box.getBoxes().
toArray(), box.getBooks().
toArray(), box
*                 .
getGames().
toArray());
*     }
*     return EMPTY_ARRAY;
* }
* </pre>
*/
The formatter turns it into:
/**
* <Pre>
*
* public Object[] getChildren(Object parentElement) { if (parentElement
* instanceof MovingBox) { MovingBox box = (MovingBox) parentElement; return
* concat(box.getBoxes().
toArray(), box.getBooks().
toArray(), box
* .
getGames().
toArray()); } return EMPTY_ARRAY; }
*
* </pre>
*/
