BUG REPORT TITLE:
[javadoc][assist] Wrong reference to binary static initializer in javadoc

BUG REPORT DESCRIPTION:
I20050219-1500 + ZRH plugin export

Steps to reproduce:
- Create 1.4 project
- Write javadoc comment

/**
* @see Terminator#<Ctrl+Space>
*/

The first proposal (static initializer) inserts wrong reference

/**
* @see Terminator#<clinit>()
*/

As far as I known, static initializers cannot be referenced within javadoc comments
