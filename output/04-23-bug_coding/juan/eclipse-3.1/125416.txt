BUG REPORT TITLE:
Better diagnostic messages on unresolved bundles

BUG REPORT DESCRIPTION:
Currently, PDE flags imported packages (via Import-Package) that do not have a matching exported package.

This is good, but not sufficient.

There are cases in which an imported package has a corresponding exported package, but the supplier of the exported package is itself unresolved.

This results in the workspace bundle importing that package being unresolved (ie.
empty classpath container), but there are no errors flagged on the manifest.mf to give a clue on to why the bundle is unresolved.
