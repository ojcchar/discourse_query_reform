BUG REPORT TITLE:
[osgi] shutdown did not complete

BUG REPORT DESCRIPTION:
Accidentally shut down Eclipse while exporting a plug-in project as deployable feature was in progress.
The console window stayed open, and responsive (could use console).
No further activity seemed to be happening.

Noticed that the plug-in export failed due to classloaders being closed for business (see below), which is expected.
Took a snapshot of the VM state (see below).
It seems System.exit() was not called.
The console and an event dispatching threads were left behind.

Typing "exit" on the console caused the VM to exit.

Unable to reproduce.

!
SESSION 2005-01-19 11:11:22.132 -----------------------------------------------

eclipse.buildId=I20050118-1015 java.version=1.5.0 java.vendor=Sun Microsystems Inc.
BootLoader constants: OS=win32, ARCH=x86, WS=win32, NL=en_CA
Framework arguments:  -keyring d:\temp\.
keyring -showlocation
Command-line arguments:  -os win32 -ws win32 -arch x86 -keyring d:\temp\.
keyring
-consolelog -console -debug -showlocation

!
ENTRY org.eclipse.pde.build 0 1 2005-01-19 11:11:22.132
!
MESSAGE Some inter-plug-in dependencies have not been satisfied.java.lang.NoClassDefFoundError: org/eclipse/pde/internal/core/ifeature/IFeatureM odel at org.eclipse.pde.internal.ui.wizards.exports.FeatureExportJob.deleteBu ildFiles(FeatureExportJob.java:298)
at org.eclipse.pde.internal.ui.wizards.exports.PluginExportJob.doExports
(PluginExportJob.java:50)
at org.eclipse.pde.internal.ui.wizards.exports.FeatureExportJob.run(Feat ureExportJob.java:95)
        at org.eclipse.core.internal.jobs.Worker.run(Worker.java:66)

osgi>

osgi>

osgi> osgi>

osgi> Full thread dump Java HotSpot(TM) Client VM (1.5.0-b64 mixed mode):

"DestroyJavaVM" prio=5 tid=0x000361e8 nid=0xc84 waiting on condition [0x00000000
.
.0x0007fae8]

"OSGi Console" prio=5 tid=0x19bd0e28 nid=0xa28 in Object.wait() [0x19fcf000.
.0x1
9fcfb68]
        at java.lang.Object.wait(Native Method)
- waiting on <0x042db588> (a java.lang.Object)
at org.eclipse.osgi.framework.internal.core.FrameworkConsole.console(Fra meworkConsole.java:265)
- locked <0x042db588> (a java.lang.Object)
at org.eclipse.osgi.framework.internal.core.FrameworkConsole.console(Fra meworkConsole.java:236)
at org.eclipse.osgi.framework.internal.core.FrameworkConsole.run(Framewo rkConsole.java:207)
        at java.lang.Thread.run(Thread.java:595)

"Framework Event Dispatcher" daemon prio=5 tid=0x199d55a0 nid=0x878 in Object.wa it() [0x19f8f000.
.0x19f8fbe8]
        at java.lang.Object.wait(Native Method)
        at java.lang.Object.wait(Object.java:474)
at org.eclipse.osgi.framework.eventmgr.EventThread.getNextEvent(EventThr ead.java:162)
- locked <0x042db620> (a org.eclipse.osgi.framework.eventmgr.EventThread
)
at org.eclipse.osgi.framework.eventmgr.EventThread.run(EventThread.java:
100)

"Low Memory Detector" daemon prio=5 tid=0x00a912f8 nid=0xe60 runnable [0x0000000
0. .0x00000000]

"CompilerThread0" daemon prio=10 tid=0x00a8fec8 nid=0x2b8 waiting on condition [
0x00000000.
.0x198cf6c0]

"Signal Dispatcher" daemon prio=10 tid=0x00a8f250 nid=0xd4c waiting on condition
[0x00000000.
.0x00000000]

"Finalizer" daemon prio=9 tid=0x00a86670 nid=0xd9c in Object.wait() [0x1984f000.
.0x1984fa68]
        at java.lang.Object.wait(Native Method)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:116)
- locked <0x042db808> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:132)
        at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:159)

"Reference Handler" daemon prio=10 tid=0x00a851e0 nid=0x478 in Object.wait() [0x
1980f000.
.0x1980fae8]
        at java.lang.Object.wait(Native Method)
        at java.lang.Object.wait(Object.java:474)
        at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:116)
- locked <0x042db888> (a java.lang.ref.Reference$Lock)

"VM Thread" prio=10 tid=0x00a82810 nid=0x750 runnable

"VM Periodic Task Thread" prio=10 tid=0x00a924d0 nid=0xd04 waiting on condition
