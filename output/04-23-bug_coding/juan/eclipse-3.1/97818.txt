BUG REPORT TITLE:
entry in .cvsignore is not ignored on update/sync

BUG REPORT DESCRIPTION:
in our projects we have the following structure

project
++dev    (src directories and such)
++docs   (documentation, src libs)
++target (build directory, gets very large due to project deps ~80M)
.
cvsignore
.
project
.
classpath

an entry in .
cvsignore is target.
M7 and prior ignored m7 when syncing/updating, rc1 does not.
this behavior occurs in all projects of this structure.
