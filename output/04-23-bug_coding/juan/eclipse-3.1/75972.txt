BUG REPORT TITLE:
StepIntoSelection action is no longer disabled for non JDT debugger

BUG REPORT DESCRIPTION:
This is actually a follow-up for bug 37288.

The comment back then was:
------- Additional Comment #4 From Darin Wright 2003-05-08 09:47 -------
Fixed in 2.1.1.
The action is disabled unless a java stack frame is selected.

For now, I do not intend to put the same fix into 3.0, as I think there should be a better solution - i.e. for the interaction of debug event filters.

It looks like the fix is not in 3.0.
So StepIntoSelection is no longer disabled, but then it is not working properly neither.
