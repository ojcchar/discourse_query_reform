BUG REPORT TITLE:
[1.5][enum] erroneous strictfp keyword on enum type produces error on constructor

BUG REPORT DESCRIPTION:
I20050215-2300 (M5 test pass)

- have this code:

strictfp enum Natural {
ONE, TWO;
}

expected: strictfp is not allowed on the enum type actual: no error is reported

- alternatively, have this code:

strictfp enum Natural {
ONE, TWO;
private Natural() {
}
}

expected: the wrong modifier is reported with the type name 'Natural' actual: the error is shown for the constructor
