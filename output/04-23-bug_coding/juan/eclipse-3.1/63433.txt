BUG REPORT TITLE:
[Progress] [Dialogs] Poor message during project deletion.

BUG REPORT DESCRIPTION:
1) delete any project from you workspace.

while the operation is running, a dialog titled "Progress information" is opened.
This dialog has the useless caption "Operation in progress...".
This should say something like "Deleting resources...",
"Deleting project..." or similar.
