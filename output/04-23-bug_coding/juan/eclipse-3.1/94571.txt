BUG REPORT TITLE:
Displaying help topics requires double click

BUG REPORT DESCRIPTION:
I20050510. Clicking a topic on All Topics page or Bookmarks page in help view does not show them.
Double click is required.
On search or related topics pages single click is enough.
