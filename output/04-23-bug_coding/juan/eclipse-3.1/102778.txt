BUG REPORT TITLE:
Scrapbook page doesn't work with enhanced for statement

BUG REPORT DESCRIPTION:
Using 3.1, create a new java project.
Add a new scrapbook page that contains this source:
int[] tab = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9 };
int sum = 0;
for (int i : tab) { sum += i;
} sum

You get an error about syntax error.
