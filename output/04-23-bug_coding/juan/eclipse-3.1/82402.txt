BUG REPORT TITLE:
[Memory View] Bad error message format

BUG REPORT DESCRIPTION:
If getBytesFromAddress fails, the following message appears in the Memory view:
Error creating tab:
org.eclipse.debug.core.DebugException: <message>
This happens because the following code is used to generate the output:
DebugUIMessages.getString(ERROR) + e.
Probably "e" should be replaced by "e.getMessage()".
