BUG REPORT TITLE:
[Workbench] ClassNotFoundException trying to eagerly start a plug-in while shutting down

BUG REPORT DESCRIPTION:
build N20050509

Noticed the following in the console output for this build.

[java] !
ENTRY org.eclipse.osgi 2005-05-09 04:31:45.859
[java] !
MESSAGE The class
"org.eclipse.update.internal.scheduler.SchedulerStartup" cannot be loaded because the system is shutting down and the plug-in
"org.eclipse.update.scheduler" has already been stopped.
[java] !
STACK 0
[java] java.lang.ClassNotFoundException: The class
"org.eclipse.update.internal.scheduler.SchedulerStartup" cannot be loaded because the system is shutting down and the plug-in
"org.eclipse.update.scheduler" has already been stopped.
[java] 	at
org.eclipse.core.runtime.adaptor.EclipseClassLoader.shouldActivateFor(EclipseClassLoader.java:148)
[java] 	at
org.eclipse.core.runtime.adaptor.EclipseClassLoader.findLocalClass(EclipseClassLoader.java:66)
[java] 	at
org.eclipse.osgi.framework.internal.core.BundleLoader.findLocalClass(BundleLoader.java:321)
[java] 	at
org.eclipse.osgi.framework.internal.core.BundleLoader.findClass(BundleLoader.java:369)
[java] 	at
org.eclipse.osgi.framework.internal.core.BundleLoader.findClass(BundleLoader.java:334)
[java] 	at
org.eclipse.osgi.framework.adaptor.core.AbstractClassLoader.loadClass(AbstractClassLoader.java:74)
     [java] 	at java.lang.ClassLoader.loadClass(ClassLoader.java:235)
[java] 	at
org.eclipse.osgi.framework.internal.core.BundleLoader.loadClass(BundleLoader.java:259)
[java] 	at
org.eclipse.osgi.framework.internal.core.BundleHost.loadClass(BundleHost.java:227)
[java] 	at
org.eclipse.osgi.framework.internal.core.AbstractBundle.loadClass(AbstractBundle.java:1259)
[java] 	at
org.eclipse.core.internal.registry.ConfigurationElement.createExecutableExtension(ConfigurationElement.java:162)
[java] 	at
org.eclipse.core.internal.registry.ConfigurationElement.createExecutableExtension(ConfigurationElement.java:152)
[java] 	at
org.eclipse.core.internal.registry.ConfigurationElement.createExecutableExtension(ConfigurationElement.java:139)
[java] 	at
org.eclipse.core.internal.registry.ConfigurationElementHandle.createExecutableExtension(ConfigurationElementHandle.java:48)
[java] 	at
org.eclipse.ui.internal.WorkbenchPlugin$1.run(WorkbenchPlugin.java:233)
[java] 	at
org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:51)
[java] 	at
org.eclipse.ui.internal.WorkbenchPlugin.createExtension(WorkbenchPlugin.java:229)
[java] 	at
org.eclipse.ui.internal.EarlyStartupRunnable.getExecutableExtension(EarlyStartupRunnable.java:115)
[java] 	at
org.eclipse.ui.internal.EarlyStartupRunnable.run(EarlyStartupRunnable.java:66)
[java] 	at
org.eclipse.core.internal.runtime.InternalPlatform.run(InternalPlatform.java:1029)
     [java] 	at org.eclipse.core.runtime.Platform.run(Platform.java:775)
     [java] 	at org.eclipse.ui.internal.Workbench$16.run(Workbench.java:1493)
     [java] 	at org.eclipse.core.internal.jobs.Worker.run(Worker.java:67)

The early startup code should check if the workbench is still running before starting each extension.
