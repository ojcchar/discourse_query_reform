BUG REPORT TITLE:
filteredDependencyCheck doesn't work with required features

BUG REPORT DESCRIPTION:
When filteredDependencyCheck is turned on and one of the included feature requires another feature using a match rule other than perfect, then the build fails when trying to add plugins for this requirement.
