BUG REPORT TITLE:
Debugger failure freezing the IDE

BUG REPORT DESCRIPTION:
Build N20050602

While stepping in debugger, I started getting timeout issues, see traces below.
However, instead of properly failing, it ended up freezing the entire IDE leaving me with no better alternative than performing a ctrl-C in the console.

I believe this is severe since I can lose data.
