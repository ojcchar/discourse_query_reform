BUG REPORT TITLE:
[ViewMgmt] Mark the title of a view as dirty

BUG REPORT DESCRIPTION:
Even though view can implement ISaveable, a view can't indicate if it is dirty or not.
That is necessary for RCP application where edition is done in the view itself.
