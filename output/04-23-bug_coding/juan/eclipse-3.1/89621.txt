BUG REPORT TITLE:
[code assist] the caret position is wrong after code assist

BUG REPORT DESCRIPTION:
Using 20030330-0500, I got this weird behavior.

import java.awt.Frame;
import java.awt.event.WindowAdapter;

public class Foo extends Frame {

public void bar() { addWindow<CODE ASSIST HERE>Listener(new WindowAdapter());
}
}

Select addWindowListener in the list of proposal.

The result is:
addWindowListene<POSITION OF THE CARET>rListener

I would expect:
addWindowListener<POSITION OF THE CARET>Listener

This is pretty annoying and seems to occur only for method name proposal.
