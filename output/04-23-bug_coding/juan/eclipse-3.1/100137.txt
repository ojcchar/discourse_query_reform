BUG REPORT TITLE:
Variables view: code assist does not work in details pane

BUG REPORT DESCRIPTION:
3.1 RC2 and N20050615-0010

1. create a fresh Java project
2. delete the JRE System Library
3. add a new User Library which is marked as system library and has the rt.jar as single library (I used JDK 1.5.0_03)
4. add the following class:
public class A {
String dog1 = "Max", dog2 = "Bailey", dog3 = "Harriet";
public static void main(String[] args) { new A().
foo();
} void foo() {
String p= "";
}
}
5. add breakpoint on line 8
6. debug
==> code runs, debugger shows correct values but:
- source is not found
- code assist does not work in Variables view's detail pane
