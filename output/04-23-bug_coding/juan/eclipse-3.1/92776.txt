BUG REPORT TITLE:
Cannot start eclipse

BUG REPORT DESCRIPTION:
I20050426-0849

I am presented with the workspace chooser dialog, but no matter what I choose for my workspace, I get a dialog which says:

Workspace Cannot be Set

Error in runtime; workspace cannot be set.
Exiting.

Although it creates the directory for a new workspace, it contains no .
metadata and no .log.
Using -consoleLog doesn't give anything more either:

!
SESSION 2005-04-26 11:03:08.431
-----------------------------------------------eclipse.buildId=I20050426-0849 java.version=1.4.2_06 java.vendor=Sun Microsystems Inc.
BootLoader constants: OS=linux, ARCH=x86, WS=gtk, NL=en_US
Command-line arguments:  -os linux -ws gtk -arch x86 -clean -debug -consoleLog
!
ENTRY org.eclipse.core.resources 2 10035 2005-04-26 11:03:11.671
!
MESSAGE A workspace crash was detected.
The previous session did not exit normally.
!
ENTRY org.eclipse.osgi 2 1 2005-04-26 11:03:11.836
!
MESSAGE NLS unused message: TeamPlugin_setting_global_ignore_7 in:
org.eclipse.team.internal.core.messages
