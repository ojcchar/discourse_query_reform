BUG REPORT TITLE:
[RC1] Update classpath changes Package Explorer from flat to hierarchical

BUG REPORT DESCRIPTION:
See bug 98602 for initial steps to reproduce.

After doing that, I selected PDE Tools > Update Classpath.
It updated the classpath, all right, but in the process changed the source display in the affected project (only!)
from flat to hierarchical.
The src folder moved from the first displayed item in the project to the fourth (after the META-INF folder.

I have seen this bug once before.
The problem is, I can't find any way to change the Package Explorer options, whatever they are, back for an individual project.
Both times wound up deleting the project, checking out again and starting over on manifest.
