BUG REPORT TITLE:
Change order of fields for extensions

BUG REPORT DESCRIPTION:
When an extension is selected on the Extensions page of the plug-in editor, three fields appear to the right:  ID, Name and Point.

Since Point is the only required attribute, it should be listed first.
