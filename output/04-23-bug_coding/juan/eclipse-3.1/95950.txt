BUG REPORT TITLE:
[KeyBindings] interactions: ALT/COMMAND+ARROW keybindings should be swapped on Mac

BUG REPORT DESCRIPTION:
Safari uses COMMAND+LEFT/RIGHT_ARROW for navigation history.
Eclipse uses ALT.
I don't think this is consistent with other Mac applications.
