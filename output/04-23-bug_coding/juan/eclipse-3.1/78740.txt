BUG REPORT TITLE:
IDOMType.getFlags() fails to represent interface flags correctly.

BUG REPORT DESCRIPTION:
This code demonstrates that calling getComment on an IDOMType will change the flags from correctly encoding the type as being interface to incorrectly encoding it (because during becomeDetailed() that information is lost):

package org.example.jdom;

import org.eclipse.core.runtime.IPlatformRunnable;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.jdom.DOMFactory;
import org.eclipse.jdt.core.jdom.IDOMCompilationUnit;
import org.eclipse.jdt.core.jdom.IDOMType;

public class Test implements IPlatformRunnable
{ public Object run(Object object)
{
DOMFactory factory = new DOMFactory();
IDOMCompilationUnit jCompilationUnit = factory.createCompilationUnit("package x; /** @model */ interface X  {}", "NAME");
IDOMType jType = (IDOMType)jCompilationUnit.getFirstChild().
getNextNode();
System.err.println("" + ((jType.getFlags() & Flags.AccInterface) !
= 0));
jType.getComment();
System.err.println("" + ((jType.getFlags() & Flags.AccInterface) !
= 0));
return new Integer(0);
}
}

This bug completely breaks EMF's JavaEcoreBuilder, which is a blocking problem for our clients and hence we see this as a blocking problem.
