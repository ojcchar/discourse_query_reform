BUG REPORT TITLE:
Antcall and Ant references to targets not marked as occurrences

BUG REPORT DESCRIPTION:
Turn on mark occurrences
Place the on any of the K2 references
The antcall and ant references are not marked

<project>
<target name="1" depends="k2">
<ant target="k2">
</ant>
<antcall target="k2"></antcall>
</target>
<target name="k2">
</target>
</project>
