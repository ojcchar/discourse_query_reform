BUG REPORT TITLE:
errors in log on successful build

BUG REPORT DESCRIPTION:
i0509-2010

I am building a feature that has some plugins being compiled and some already compiled.
The build completes successfully but the log for the biuld is full of the following entries.
It seems that there is one for every target plugin
(which of course do not have build.properties files).
For larger targets there may well be 100s or 1000s of plugins so one entry per plugin per build is going to add up pretty fast.
Can we turn these off?
Perhaps only show them in debug mode or something?

eclipse.buildId=I20050509-2010 java.version=1.4.2_04 java.vendor=Sun Microsystems Inc.
BootLoader constants: OS=win32, ARCH=x86, WS=win32, NL=en_US
Framework arguments:  -application org.eclipse.pde.build.Build
Command-line arguments:  -application org.eclipse.pde.build.Build -data d:\build-ws

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.156
!
MESSAGE File not read: d:\n0509-rcp\eclipse\features\org.eclipse.rcp_3.1.0
\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509-rcp\eclipse\plugins\org.apache.lucene_1.4.3
\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509- rcp\eclipse\plugins\org.eclipse.help.appserver_3.1.0.jar\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509- rcp\eclipse\plugins\org.eclipse.update.configurator_3.1.0.jar\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509- rcp\eclipse\plugins\org.eclipse.update.configurator_3.1.0.jar\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509-rcp\eclipse\plugins\org.eclipse.tomcat_4.1.30.1
\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509- rcp\eclipse\plugins\org.eclipse.update.core_3.1.0.jar\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509- rcp\eclipse\plugins\org.eclipse.help.base_3.1.0.jar\build.properties.

!
ENTRY org.eclipse.pde.build 2 10 2005-05-11 22:46:05.306
!
MESSAGE File not read: d:\n0509- rcp\eclipse\plugins\org.eclipse.help.webapp_3.1.0\build.properties.
