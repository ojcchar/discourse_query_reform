BUG REPORT TITLE:
[MPE] [EditorMgmt] An editor instance is being leaked each time an editor is open and closed

BUG REPORT DESCRIPTION:
Driver: eclipse-SDK-3.1-win32 with eclipse-test-framework-3.1

Every we open and close an editor.
That editor instance is being leaked.
We have a testcase that can demostrate the problem.
The testcase is really simple.
It creates a new simple project and a new file.
It opens up the new file in the editor that comes with the testcase, then close the editor.
Repeat 500 times.

What's interesting is that the editor, upon open, will allocate a 200000 size
String array as a private field.
So this String array can be GC-ed if the editor itself can be GC-ed.

If you run this testcase with -Xmx256M, you will run out of memory.
However, if you explicitly set the String array to null in the dispose() method of the editor, then the same testcase will not run out of memory.
This leads us to believe that the editor instance is being leaked.
