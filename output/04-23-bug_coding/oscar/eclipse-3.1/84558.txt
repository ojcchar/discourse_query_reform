BUG REPORT TITLE:
Strange error message when using keywords "const" and "goto" as variable names

BUG REPORT DESCRIPTION:
I20050202-0800

double const= Math.PI;

The error message is:
'Syntax error on token "Invalid Character", invalid VariableDeclaratorId'

For keywords other than "const" and "goto", the error message includes the offending constant's name in the error message, i.e.
'Syntax error on token "public", invalid VariableDeclaratorId'.
