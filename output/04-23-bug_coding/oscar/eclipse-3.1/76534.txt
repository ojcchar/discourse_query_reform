BUG REPORT TITLE:
Can't perform evaluations inside inner class with constructor parameters

BUG REPORT DESCRIPTION:
We currently disallow evaluations in inner classes that take parameters in the referenced constructor.
For example, see the CheckBoxTreeViewer that's created in BreakpointsView#createViewer(...).

Is there anything we can do to allow evaluations in these kinds of classes?
