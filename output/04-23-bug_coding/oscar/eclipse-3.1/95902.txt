BUG REPORT TITLE:
PDE leaves manifest.mf out of sync after import

BUG REPORT DESCRIPTION:
N20050519-0010

I imported the binary plug-ins from N20050519-0010 into my existing workpspace which already contained older versions for those plug-ins and used 'File' search to find a plug-in ID.
This did not work: search reported a list of all the imported manifest.mf files being out-of-sync with the workspace, see attached picture.

There might be other imported files being out of sync but I only touched the MFs so far today.
