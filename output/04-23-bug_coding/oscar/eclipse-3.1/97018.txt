BUG REPORT TITLE:
PDE generates to strict access restrictions

BUG REPORT DESCRIPTION:
I20050527-0010

- download EMF using the update manager (don't know if this is necessary)
- observe: the install plug-in has a manifest file which only contains one line:
Manifest-Version: 1.0
- it also contains a plugin.xml file which exports the following entries:
<runtime>
<library name="runtime/ecore.jar">
<export name="*"/>
<packages prefixes="org.eclipse.emf.ecore"/>
</library>
</runtime>

This result in the following access restriction for clients using emf.core
==> **/* forbidden reference

As a result all usages to emf core classes even those in API package are flagged as an error.

Since having a one liner manifest file seems to be a legal setup PDE should honor the access restrictions from the plugin.xml file and not generate a ALL forbidden rule only.
