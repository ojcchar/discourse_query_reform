BUG REPORT TITLE:
Key binding for terminate - works 3.0, not 3.1 (debug view issue)

BUG REPORT DESCRIPTION:
The key binding shortcut itself works, but due to a change in the behavior of the debug view, you can not use it, so it's effectively broken.

With a project set up to run, first have the debug view open.
Run the application in your project.

You will see in the debug view:

- <Project> [Java Application]
-<path>/j2re1.4.2/bin/javaw.exe(Date started)

This is the issue- in 3.0 and earlier, the javaw.exe line if the last run application would AUTOMATICALLY get selected.
This in turn would enable the terminate button in this view (and thus the key binding).

In 3.1 however, there is no selection made, so the terminate button is not enabled (since there is no context of what you want to terminate) and thus the key binding won't work.
You would need to open this view and make a selection first, THEN the key binding works as you would expect, which is not much of a shortcut now is it...
