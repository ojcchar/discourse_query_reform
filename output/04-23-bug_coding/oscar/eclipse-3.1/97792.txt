BUG REPORT TITLE:
Misleading message in error log view for optional plugins

BUG REPORT DESCRIPTION:
When a plugin has an optional plugin, (ie see attached sample from o.e.jem.util)
with an optional=true (ie non OSGI manifest), the pde runtime error log view shows a warning but the message says:
Missing required plug-in: ....

It should instead say:
Missing optional plug-in: ....
