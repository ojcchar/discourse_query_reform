BUG REPORT TITLE:
[Eye Candy] Manifest Editor source page should support hover

BUG REPORT DESCRIPTION:
When hovering over attribute/element names, we should show hover information containing the description for that element/attribute (much like we do on the Extensions page).

Hovering over a translated attribute should show the substituted value for that attribute from the properties file.

Hovering over an extension point ID should show the description for that extension.

Hovering over a manifest header should show the javadoc for that header.
