BUG REPORT TITLE:
Console stop button does not stop a debugging session

BUG REPORT DESCRIPTION:
When debugging an ant script, clicking on the stop button of the console does not stop the debug session.
