BUG REPORT TITLE:
ITypeBinding#isEqualTo(..) returns true when comparing ArrayList<Integer> to its erasure

BUG REPORT DESCRIPTION:
I20041117-0800 + jdt.core & ui as for I20041123-0800

ITypeBinding#isEqualTo(.
.)
returns true when comparing ArrayList<Integer> to its erasure.
Expected: false.
