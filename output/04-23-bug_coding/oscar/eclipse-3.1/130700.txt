BUG REPORT TITLE:
Registry view does not auto-refresh when bundle becomes active

BUG REPORT DESCRIPTION:
1. Fresh workspace
2. Open the plugins view and scroll down to the org.eclipse.pde.ui plugin.
It has no green overlay.
Fine.
3. Open the Target Platform preference page, and close it.

The org.eclipse.pde.ui plugin does not refresh to acquire a green overlay, even though it has just become activated.

Refreshing the view manually causes the plugin icon to refresh.
