BUG REPORT TITLE:
[content type] text file recognized as Ant Build Script

BUG REPORT DESCRIPTION:
i20050201

A txt file containing valid XML content having <project> as the root element will be recognized as an Ant Build Script.

This is a regression introduced by recent work on the content type manager code.
