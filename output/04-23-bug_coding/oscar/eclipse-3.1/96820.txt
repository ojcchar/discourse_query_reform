BUG REPORT TITLE:
JME during Source lookup

BUG REPORT DESCRIPTION:
N20050526-0010

I work in a full source workspace (ZRH plugins from HEAD, all other plugins imported as source).
I set a breakpoint in ContentAssistHandler#enable()
(in plugin org.eclipse.ui.workbench.texteditor), and started a run-time workbench.

After opening the Find/Replace dialog and enabling "Regular Expressions", I got a "Source not found." editor and the JME below.
Since I imported all projects as source, a do have a project org.eclipse.core.boot, but it is not a Java project.

Error 2005-05-26 17:50:36.347 Error logged from Debug Core:
Java Model Exception: Java Model Status [org.eclipse.core.boot does not exist] at
org.eclipse.jdt.internal.core.JavaElement.newNotPresentException(JavaElement.java:468)
at
org.eclipse.jdt.internal.core.JavaModelManager.getPerProjectInfoCheckExistence(JavaModelManager.java:1200)
at
org.eclipse.jdt.internal.core.JavaProject.getPerProjectInfo(JavaProject.java:1794)
	at org.eclipse.jdt.internal.core.JavaProject.getRawClasspath(JavaProject.java:1851)
	at org.eclipse.jdt.internal.core.JavaProject.getRawClasspath(JavaProject.java:1837)
at
org.eclipse.jdt.launching.sourcelookup.containers.JavaProjectSourceContainer.createSourceContainers(JavaProjectSourceContainer.java:92)
at
org.eclipse.debug.core.sourcelookup.containers.CompositeSourceContainer.getSourceContainers(CompositeSourceContainer.java:126)
at
org.eclipse.jdt.launching.sourcelookup.containers.JavaProjectSourceContainer.findSourceElements(JavaProjectSourceContainer.java:133)
at
org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant.findSourceElements(AbstractSourceLookupParticipant.java:60)
at
org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector$SourceLookupQuery.run(AbstractSourceLookupDirector.java:126)
at
org.eclipse.core.internal.runtime.InternalPlatform.run(InternalPlatform.java:1038)
	at org.eclipse.core.runtime.Platform.run(Platform.java:775)
at
org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector.doSourceLookup(AbstractSourceLookupDirector.java:465)
at
org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector.getSourceElement(AbstractSourceLookupDirector.java:715)
at
org.eclipse.debug.internal.ui.sourcelookup.SourceLookupFacility.lookup(SourceLookupFacility.java:138)
	at org.eclipse.debug.ui.DebugUITools.lookupSource(DebugUITools.java:658)
at
org.eclipse.debug.internal.ui.views.launch.LaunchView$SourceLookupJob.run(LaunchView.java:176)
	at org.eclipse.core.internal.jobs.Worker.run(Worker.java:67)
