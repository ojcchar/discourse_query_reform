BUG REPORT TITLE:
Ant view does not update project name

BUG REPORT DESCRIPTION:
The Ant view does not update the name of the project node when the user edits the project name attribute in the buildfile.
