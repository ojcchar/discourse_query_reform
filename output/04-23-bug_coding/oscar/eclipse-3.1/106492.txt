BUG REPORT TITLE:
NPE on console during debug session

BUG REPORT DESCRIPTION:
Build: I20050808-2000

While debugging, I noticed the attached stack trace on my Java console (not in the log file).
There was nothing in the log file.
I see from the stack that it occurred during evaluation of a conditional breakpoint.
I have a single breakpoint with this condition:

name.equals("IResourceTest.testDelete") && count==83

After this error occurred, the debug process hung, and "Terminate" and
"Terminate All" had no effect.
I was still able to "Suspend" the process, and it resulted in a debug view showing:

org.eclipse.core.launcher.Main at localhost:1250
Thread [main] (Suspended (breakpoint at line 69 in TestPerformer))
IResourceTest$6(TestPerformer).
performTestRecursiveLoop(Object[][], Object[], int) line: 69
<unknown receiving type>(TestPerformer).
performTestRecursiveLoop(Object[][],
Object[], int) line: 111
<unknown receiving type>(TestPerformer).
performTestRecursiveLoop(Object[][],
Object[], int) line: 111
<unknown receiving type>(TestPerformer).
performTest(Object[][]) line: 55
<unknown receiving type>(<unknown declaring type>).
testDelete() <unknown line number>
<unknown receiving type>(<unknown declaring type>).
invoke0(Method, Object,
Object[]) <unknown line number>
<unknown receiving type>(<unknown declaring type>).
invoke(Object, Object[])
<unknown line number>
<unknown receiving type>(<unknown declaring type>).
invoke(Method, Object,
Object[]) <unknown line number>
<unknown receiving type>(<unknown declaring type>).
invoke(Method, Object,
Object[]) <unknown line number>
<unknown receiving type>(<unknown declaring type>).
invoke(Object, Object[])
<unknown line number>
<unknown receiving type>(<unknown declaring type>).
runTest() <unknown line number>
<unknown receiving type>(<unknown declaring type>).
runBare() <unknown line number>
<unknown receiving type>(<unknown declaring type>).
protect() <unknown line number>
... etc ...

I had to shutdown Eclipse to remove this from the debug view.
