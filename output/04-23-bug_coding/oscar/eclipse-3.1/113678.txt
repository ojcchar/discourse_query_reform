BUG REPORT TITLE:
IFileStore.openOutputStream does gratuitous mkdir on parent

BUG REPORT DESCRIPTION:
Build: I20051025-0800

The local file implementation of IFileStore.openOutputStream blindly calls
IFileStore.mkdir on the parent store.
This is generally unnecessary, and not consistent with mkdir itself which fails if the parent does not exist when
SHALLOW flag is used.
The spec should be clarified and the local file implementation brought in line.
