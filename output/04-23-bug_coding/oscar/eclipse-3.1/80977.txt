BUG REPORT TITLE:
transient test failures

BUG REPORT DESCRIPTION:
In builds of late we have transient test failures such as:

Could not find variable 'sum'

junit.framework.AssertionFailedError: Could not find variable 'sum' at org.eclipse.jdt.debug.tests.core.MethodBreakpointTests.testHitCountExitBreakpoi nt(MethodBreakpointTests.java:215)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke
(DelegatingMethodAccessorImpl.java:25)
at org.eclipse.jdt.debug.tests.DebugSuite$1.run(DebugSuite.java:53)
at java.lang.Thread.run(Thread.java:534)

and

Should have been suspended at linenumber expected:<35> but was:<26>

junit.framework.AssertionFailedError: Should have been suspended at linenumber expected:<35> but was:<26> at org.eclipse.jdt.debug.tests.core.ExceptionBreakpointTests.testHitCountException
(ExceptionBreakpointTests.java:145)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke
(DelegatingMethodAccessorImpl.java:25)
at org.eclipse.jdt.debug.tests.DebugSuite$1.run(DebugSuite.java:53)
at java.lang.Thread.run(Thread.java:534)

We cannot reproduce the failures, but it looks possible that one test is not completely finished before the next test executes (at least in terms of debug events being fired/processed).
A test might receive an event from a previous test that is running, causing the test to fail.

To improve cleanup, we wait for a terminate event on each target.
And then we fire a custom event and wait for it to come back to ensure that the event queue is flusded before the next test runs.
