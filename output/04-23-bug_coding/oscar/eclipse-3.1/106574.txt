BUG REPORT TITLE:
[KeyBindings] registry: General/Keys setting not saved or not restored

BUG REPORT DESCRIPTION:
Using the emacs key settings for editing.
The standard settings has CTRL+E set to "Window/Open Editor Drop Down" which Emacs uses for "End of Line" when editting text.
The default when editing text is the editor drop down action.

I go to Preferences/General/Keys and edit the "Window/Open Editor Drop Down" entry and Remove the editor drop down entry for CTRL+E (leaving the alternate entry of "CTRL+X B" for this action) and hit Apply and OK and everything works as expected.
However, on quiting and restarting, the "Window/Open Editor Drop
Down" binding is back.

I thought that this might be related to bug 86750, but using the -data option gave the same results; so, I'm not sure if it's the case that it's not being saved or it's not being restored.
