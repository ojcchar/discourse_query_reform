BUG REPORT TITLE:
Clean Timestamp on project with large number of false changes

BUG REPORT DESCRIPTION:
After the time change, most projects had a large number of outgoing changes.
Performing a Clean Timestamps on the JDT test projects appeared to freeze the
UI.
A quick inspection of the stacktraces reveal that the UI thread is live and is doing work updating the synchronize view.
However, the hourglass is up and the user cannot do anything.
