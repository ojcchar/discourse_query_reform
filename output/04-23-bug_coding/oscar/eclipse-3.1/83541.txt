BUG REPORT TITLE:
Cannot  find a way to change char value to special character

BUG REPORT DESCRIPTION:
For character array element I open Change Primitive Value dialog and try to change current value to \t or \n. Dialog does not enable OK button either I input \t, or 0x09 or \\u0009 or press tabulation key.
On the other hand, Eclipse
2.1.2 accepted "\t" (or it worked OK if I just pressed tabulation) and \n in this dialog.
