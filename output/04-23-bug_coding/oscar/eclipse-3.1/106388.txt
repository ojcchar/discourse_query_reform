BUG REPORT TITLE:
Directory dialog is unusably slow

BUG REPORT DESCRIPTION:
Trying to browse the file system using a file chooser dialog was not possible.
The file chooser dialog doesn't refresh when I click on the [+].
I am using I20050808-0800 with a patch for bug 106361.
