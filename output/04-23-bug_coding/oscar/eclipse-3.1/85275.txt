BUG REPORT TITLE:
Team>Disconnect should make all resources writtable

BUG REPORT DESCRIPTION:
When the user disconnects a project from CVS control, any read-only resources shoudl be made writtable.
Otherwise, errors occur if the project is renamed.
