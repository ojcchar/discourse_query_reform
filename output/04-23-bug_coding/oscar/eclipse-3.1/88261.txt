BUG REPORT TITLE:
create component XML for debug plugins

BUG REPORT DESCRIPTION:
Create componet.xml for debug plug-ins.

This allows us to police internal cross-component references and enables downstream customers to detect illegal references into our components.
