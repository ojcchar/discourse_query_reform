BUG REPORT TITLE:
Support for search in variables view.

BUG REPORT DESCRIPTION:
Some languages lead to hundreds of variables appearing in the variables view.
It would be nice to have a search mechanism that would help the user find the variable they are looking for.
A couple of suggestions to consider:

1. Incremental Text Search : this would also search the variable value which is not ideal but would be fast
2. Incremental Variable Name Search : base the search on the name returned by
IVariable.getName() - this would be slower than #1 but nicer
