BUG REPORT TITLE:
Run menu: Folder selector shows files, too

BUG REPORT DESCRIPTION:
Select "Run..." from the Run menu and choose "Java Application".
When editing the Classpath ("Classpath" tab) of a configuration by trying to add a folder
(Button "Advanced" -> RadioButton "Add Folders"), a file chooser pops up, showing not only folders, but normal files, too, which is not very nice.

Compare this with the other possible selection with
RadioButton "Add External Folder", where -- correctly -- only folders can be chosen!
So, the "Add Folders" file chooser should become such a folder chooser, too.
