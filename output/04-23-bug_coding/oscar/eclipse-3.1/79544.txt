BUG REPORT TITLE:
ITypeBinding#isEqualTo(..) does not compare type arguments

BUG REPORT DESCRIPTION:
JDT/Core from HEAD (including fix for bug 79271).
ITypeBinding#isEqualTo(.
.)
does not compare type arguments:

public class A<X> {
List<Integer> i;
List<Number> n;
List<? extends Number> en;
List<X> x;
}

Types of i, n, en, and x are not equal, even though ITypeBinding#isEqualTo(.
.)
says they are (in both directions).
