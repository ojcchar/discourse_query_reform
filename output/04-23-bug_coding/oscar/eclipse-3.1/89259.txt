BUG REPORT TITLE:
[WorkbenchParts] assertion failed when opening Compare editor for MANIFEST.MF

BUG REPORT DESCRIPTION:
N20050328-0010

Not sure what the right component to report this against is.
Opening compare editor for MANIFEST.MF files fails with the following AssertionFailedException.
It looks quite critical, but I am leaving severity as normal since it is a nightly build.

Steps:
- select two random MANIFEST.MF files
- compare with each other

The same happens when comparing with CVS resource history.

!
SESSION 2005-03-28 13:41:05.772 ----------------------------------------------- eclipse.buildId=N20050328-0010 java.version=1.5.0 java.vendor=Sun Microsystems Inc.
BootLoader constants: OS=win32, ARCH=x86, WS=win32, NL=en_CA
Framework arguments:  -keyring d:\temp\.
keyring -showlocation
Command-line arguments:  -data d:\dev\head -os win32 -ws win32 -arch x86
-keyring d:\temp\.
keyring -consolelog -console -debug -showlocation

!
ENTRY org.eclipse.ui 4 0 2005-03-28 13:41:05.787
!
MESSAGE Unable to create editor ID org.eclipse.compare.CompareEditor: null argument;
!
STACK 0 org.eclipse.ui.internal.misc.AssertionFailedException: null argument;
	at org.eclipse.ui.internal.misc.Assert.isNotNull(Assert.java:81)
	at org.eclipse.ui.internal.misc.Assert.isNotNull(Assert.java:68)
	at org.eclipse.ui.internal.PartTester.testEditorInput(PartTester.java:51)
	at org.eclipse.ui.internal.PartTester.testEditor(PartTester.java:42)
at
org.eclipse.ui.internal.EditorManager.busyRestoreEditorHelper(EditorManager.java:1222)
	at org.eclipse.ui.internal.EditorManager.busyRestoreEditor(EditorManager.java:1077)
	at org.eclipse.ui.internal.EditorManager$7.run(EditorManager.java:1039)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
	at org.eclipse.ui.internal.EditorManager.restoreEditor(EditorManager.java:1037)
	at org.eclipse.ui.internal.EditorManager$Editor.getEditor(EditorManager.java:1623)
	at org.eclipse.ui.internal.EditorManager$Editor.getPart(EditorManager.java:1614)
	at org.eclipse.ui.internal.PartPane.setVisible(PartPane.java:260)
at
org.eclipse.ui.internal.presentations.PresentablePart.setVisible(PresentablePart.java:126)
at
org.eclipse.ui.internal.presentations.newapi.PresentablePartFolder.select(PresentablePartFolder.java:268)
at
org.eclipse.ui.internal.presentations.newapi.LeftToRightTabOrder.select(LeftToRightTabOrder.java:65)
at
org.eclipse.ui.internal.presentations.newapi.TabbedStackPresentation.selectPart(TabbedStackPresentation.java:391)
at
org.eclipse.ui.internal.PartStack.refreshPresentationSelection(PartStack.java:1070)
	at org.eclipse.ui.internal.PartStack.setSelection(PartStack.java:1019)
	at org.eclipse.ui.internal.PartStack.showPart(PartStack.java:1223)
	at org.eclipse.ui.internal.PartStack.add(PartStack.java:406)
	at org.eclipse.ui.internal.EditorStack.add(EditorStack.java:109)
at
org.eclipse.ui.internal.EditorSashContainer.addEditor(EditorSashContainer.java:63)
	at org.eclipse.ui.internal.EditorAreaHelper.addToLayout(EditorAreaHelper.java:266)
	at org.eclipse.ui.internal.EditorManager$4.run(EditorManager.java:804)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
	at org.eclipse.ui.internal.EditorManager.createEditorTab(EditorManager.java:784)
at
org.eclipse.ui.internal.EditorManager.openEditorFromDescriptor(EditorManager.java:672)
	at org.eclipse.ui.internal.EditorManager.openEditor(EditorManager.java:635)
at
org.eclipse.ui.internal.WorkbenchPage.busyOpenEditorBatched(WorkbenchPage.java:2229)
	at org.eclipse.ui.internal.WorkbenchPage.busyOpenEditor(WorkbenchPage.java:2163)
	at org.eclipse.ui.internal.WorkbenchPage.access$7(WorkbenchPage.java:2155)
	at org.eclipse.ui.internal.WorkbenchPage$9.run(WorkbenchPage.java:2141)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
	at org.eclipse.ui.internal.WorkbenchPage.openEditor(WorkbenchPage.java:2136)
	at org.eclipse.ui.internal.WorkbenchPage.openEditor(WorkbenchPage.java:2121)
at
org.eclipse.compare.internal.CompareUIPlugin.openCompareEditor(CompareUIPlugin.java:444)
	at org.eclipse.compare.CompareUI.openCompareEditorOnPage(CompareUI.java:136)
at
org.eclipse.team.internal.ccvs.ui.actions.CompareRemoteResourcesAction.execute(CompareRemoteResourcesAction.java:42)
	at org.eclipse.team.internal.ccvs.ui.actions.CVSAction.run(CVSAction.java:117)
	at org.eclipse.ui.actions.ActionDelegate.runWithEvent(ActionDelegate.java:70)
	at org.eclipse.ui.internal.PluginAction.runWithEvent(PluginAction.java:231)
at
org.eclipse.jface.action.ActionContributionItem.handleWidgetSelection(ActionContributionItem.java:538)
at
org.eclipse.jface.action.ActionContributionItem.access$2(ActionContributionItem.java:488)
at
org.eclipse.jface.action.ActionContributionItem$5.handleEvent(ActionContributionItem.java:400)
	at org.eclipse.swt.widgets.EventTable.sendEvent(EventTable.java:82)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:842)
	at org.eclipse.swt.widgets.Display.runDeferredEvents(Display.java:2894)
	at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java:2527)
	at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1563)
	at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1527)
	at org.eclipse.ui.internal.Workbench.createAndRunWorkbench(Workbench.java:306)
	at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:143)
	at org.eclipse.ui.internal.ide.IDEApplication.run(IDEApplication.java:103)
at
org.eclipse.core.internal.runtime.PlatformActivator$1.run(PlatformActivator.java:228)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:338)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:151)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.eclipse.core.launcher.Main.invokeFramework(Main.java:315)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:268)
	at org.eclipse.core.launcher.Main.run(Main.java:942)
	at org.eclipse.core.launcher.Main.main(Main.java:926)
