BUG REPORT TITLE:
Breakpoint type group shows up not "enabled"

BUG REPORT DESCRIPTION:
N20050523

Create Java and Ant breakpoints
Set the breakpoints view to "Show breakpoints supported by Selected Target"
Debug a Java program
Toggle off Set the breakpoints view to "Show breakpoints supported by Selected
Target"

The Ant Breakpoints group shows up but displays as disabled (no check mark).
Check mark is displayed once you expand the Ant breakpoints group
