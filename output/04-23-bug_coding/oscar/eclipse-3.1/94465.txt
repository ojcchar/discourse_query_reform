BUG REPORT TITLE:
Java Core Dump where modifying value in the Variables View.

BUG REPORT DESCRIPTION:
Test case:
String [] elms= { "abc", "cde", "xyz" };

I have a string array.
1. In the variables, expand the array, and then expand the first element, [0]
="abc".
2. Select the "value=char[3]" field.
RMC->Change Value.
3. In the Change Primitive Value dialog, type in a new string value.
4. Click ok and it will result in a java dump.

Got the following error in the console:
JVMDG217: Dump Handler is Processing Signal 11 - Please Wait.
JVMDG303: JVM Requesting Java core file
JVMDG304: Java core file written to D:\eclipse3.1\I20050509
\eclipse\workspace\YetAnotherProj\javacore.20050510.142923.2576.txt
JVMDG215: Dump Handler has Processed Exception Signal 11.

Runnign IBM JVM 1.4.2
