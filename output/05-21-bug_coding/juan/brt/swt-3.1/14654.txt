BUG REPORT TITLE:
[typing] Single line selection on triple-click

BUG REPORT DESCRIPTION:
This is a common and convenient behavior for text editors/IDEs which is missing.
Would be nice to have it.
Switching to keyboard and doing "shift+arrow down" seems like a lot more effort for achieving the same effect.
