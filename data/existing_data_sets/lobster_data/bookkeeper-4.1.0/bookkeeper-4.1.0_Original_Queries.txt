1 BOOKKEEPER-524
Bookie journal filesystem gets full after SyncThread is terminated with exception The SyncThread get a NPE while the rest of the bookie is still running. This causes the journal gc to be stopped and the filesystem get full.    Tue Dec 18 17:01:18 2012: Exception in thread "SyncThread" java.lang.NullPointerException  Tue Dec 18 17:01:18 2012:       at org.apache.bookkeeper.bookie.LedgerCacheImpl.getLedgerEntryPage(LedgerCacheImpl.java:153)  Tue Dec 18 17:01:18 2012:       at org.apache.bookkeeper.bookie.LedgerCacheImpl.flushLedger(LedgerCacheImpl.java:421)  Tue Dec 18 17:01:18 2012:       at org.apache.bookkeeper.bookie.LedgerCacheImpl.flushLedger(LedgerCacheImpl.java:363)  Tue Dec 18 17:01:18 2012:       at org.apache.bookkeeper.bookie.InterleavedLedgerStorage.flush(InterleavedLedgerStorage.java:148)  Tue Dec 18 17:01:18 2012:       at org.apache.bookkeeper.bookie.Bookie$SyncThread.run(Bookie.java:221)
3
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.LedgerCacheImpl
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.LedgerCacheTest

2 BOOKKEEPER-514
TestDeadLock hanging sometimes I've attached the logs.    Looks to be something with the new channel manager. Also, the test itself is bad because its doing an assert from a callback.
7
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerHandle
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.PendingAddOp
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.PendingReadOp
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.PerChannelBookieClient
bookkeeper-server.src.test.java.org.apache.bookkeeper.client.BookieThrottleTest
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.BookieReadWriteTest
hedwig-server.src.test.java.org.apache.hedwig.server.persistence.TestDeadlock

3 BOOKKEEPER-498
BookieRecoveryTest.tearDown NPE Jenkins build is complaining among other things about this NPE.
1
bookkeeper-server.src.test.java.org.apache.bookkeeper.client.BookieRecoveryTest

4 BOOKKEEPER-495
Revise BK config doc There are a few missing config parameters. 
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.ServerConfiguration

5 BOOKKEEPER-491
Hedwig doc for configuration Unless I'm not looking properly into it, I can's see any doc describing the configuration parameters of hedwig-server. 
1
hedwig-server.src.main.java.org.apache.hedwig.server.common.ServerConfiguration

6 BOOKKEEPER-472
Provide an option to start Autorecovery along with Bookie Servers We can also have an option to start the Autorecovery along with Bookie servers.  If some users are not having too much load on the servers, they can even start them along the Bookie servers. If they feel, Auditor would disturb Bookie performance, they can anyway start as separate process.    In another case, deployment overhead will reduce a bit as Monitoring process need not monitor one more process in their lifcycles etc.    Thoughts?
6
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.ServerConfiguration
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookieServer
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.LocalBookKeeper
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.BookKeeperClusterTestCase
hedwig-server.src.test.java.org.apache.hedwig.server.persistence.BookKeeperTestBase
hedwig-server.src.test.java.org.apache.hedwig.server.persistence.BookKeeperTestBase.TestBookieServer

7 BOOKKEEPER-465
CreateNewLog may overwrite lastLogId with smaller value  In createNewLog(), only one directory is searched to check for duplicate log id.  Then the id is used to overwrite lastLogId.    It looks like regression from BOOKKEEPER-345.          // It would better not to overwrite existing entry log files      File newLogFile = null;          do {              String logFileName = Long.toHexString(++logId) + ".log";              File dir = ledgerDirsManager.pickRandomWritableDir();              newLogFile = new File(dir, logFileName);              currentDir = dir;              if (newLogFile.exists()) {                  LOG.warn("Found existed entry log " + newLogFile                          + " when trying to create it as a new log.");                  newLogFile = null;                  continue;              }          } while (newLogFile == null);
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.EntryLogger

8 BOOKKEEPER-447
Bookie can fail to recover if index pages flushed before ledger flush acknowledged Bookie index page steal (LedgerCacheImpl::grabCleanPage) can cause index file to reflect unacknowledged entries (due to flushLedger). Suppose ledger and entry fail to flush due to Bookkeeper server crash, it will cause ledger recovery not able to use the bookie afterward, due to InterleavedStorageLedger::getEntry throws IOException.  If the ackSet bookies all experience this problem (DC environment), the ledger will not be able to recover.  The problem here essentially a violation of WAL. One reasonable fix is to track ledger flush progress (either per-ledger entry, or per-topic message). Do not flush index pages which tracks entries whose ledger (log) has not been flushed.
3
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.EntryLogger
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.LedgerCacheTest

9 BOOKKEEPER-436
Journal#rollLog may leak file handler Just seen the peice of code in Jouranl#rollLog   {code}  try {                      FileOutputStream fos = new FileOutputStream(file);                      fos.write(buff);                      fos.getChannel().force(true);                      fos.close();                  } catch (IOException e) {                      LOG.error("Problems writing to " + file, e);                  }  {code}    On exception It is just logging and continuing.  Even though FileOutputStream provides finalize implementation and which will clean streams, I don't think it's a good idea to depend on it as it will not be garanteed.    cleaning with more care would avoid this.
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Journal

10 BOOKKEEPER-424
Bookie start is failing intermittently when zkclient connection delays I'm seeing the following intermittent failure, when there is a delay in establishing zkclient connection with zkserver.   {code}  org.apache.bookkeeper.bookie.BookieException$InvalidCookieException: org.apache.zookeeper.KeeperException$ConnectionLossException: KeeperErrorCode = ConnectionLoss for /ledgers/INSTANCEID  	at org.apache.bookkeeper.bookie.Bookie.checkEnvironment(Bookie.java:329)  	at org.apache.bookkeeper.bookie.Bookie.<init>(Bookie.java:378)  	at org.apache.bookkeeper.bookie.BookieInitializationTest.testStartBookieWithoutZKServer(BookieInitializationTest.java:253)  	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)  	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)  	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)  	at java.lang.reflect.Method.invoke(Unknown Source)  	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:44)  	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:15)  	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:41)  	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:20)  	at org.junit.internal.runners.statements.FailOnTimeout$1.run(FailOnTimeout.java:28)  Caused by: org.apache.zookeeper.KeeperException$ConnectionLossException: KeeperErrorCode = ConnectionLoss for /ledgers/INSTANCEID  	at org.apache.zookeeper.KeeperException.create(KeeperException.java:99)  	at org.apache.zookeeper.KeeperException.create(KeeperException.java:51)  	at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1131)  	at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1160)  	at org.apache.bookkeeper.bookie.Bookie.getInstanceId(Bookie.java:346)  	at org.apache.bookkeeper.bookie.Bookie.checkEnvironment(Bookie.java:280)  	... 11 more  {code}
5
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeper
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.ZkUtils
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.ZooKeeperUtil

11 BOOKKEEPER-409
Integration Test - Perform bookie rereplication cycle by Auditor-RW processes The idea is to perform integration testing of entire re-replication module : Auditor-RW processes
1
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.BookKeeperClusterTestCase

12 BOOKKEEPER-396
Compilation issue in TestClient.java of BenchMark ( showing this in eclipse) {code}  import java.util.concurrent.Future;;  {code}    showing here. Simply we can remove one ';'
1
bookkeeper-benchmark.src.main.java.org.apache.bookkeeper.benchmark.TestClient

13 BOOKKEEPER-387
BookKeeper Upgrade is not working. I am trying to upgrade BK from 4.1.0 to 4.2.0, but it will log as "Directory is current, no need to upgrade” even then it will continue and fail.  and throwing following exception.  {code}  2012-09-03 17:25:12,468 - ERROR - [main:FileSystemUpgrade@229] - Error moving upgraded directories into place /home/BK4.1/bookkeeper1/ledger/upgradeTmp.2433718456734190 -> /home/BK4.1/bookkeeper1/ledger/current  org.apache.commons.io.FileExistsException: Destination '/home/BK4.1/bookkeeper1/ledger/current' already exists          at org.apache.commons.io.FileUtils.moveDirectory(FileUtils.java:2304)          at org.apache.bookkeeper.bookie.FileSystemUpgrade.upgrade(FileSystemUpgrade.java:225)          at org.apache.bookkeeper.bookie.FileSystemUpgrade.main(FileSystemUpgrade.java:367)  {code}  
2
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.FileSystemUpgrade
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.UpgradeTest

14 BOOKKEEPER-383
NPE in BookieJournalTest Running org.apache.bookkeeper.bookie.BookieJournalTest  Exception in thread "GarbageCollectorThread" java.lang.NullPointerException  	at org.apache.bookkeeper.meta.AbstractZkLedgerManager.asyncGetLedgersInSingleNode(AbstractZkLedgerManager.java:191)  	at org.apache.bookkeeper.meta.AbstractZkLedgerManager.getLedgersInSingleNode(AbstractZkLedgerManager.java:268)  	at org.apache.bookkeeper.meta.FlatLedgerManager.garbageCollectLedgers(FlatLedgerManager.java:144)  	at org.apache.bookkeeper.bookie.GarbageCollectorThread.doGcLedgers(GarbageCollectorThread.java:226)    The exception is found in https://builds.apache.org/job/bookkeeper-trunk/671/console
1
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.BookieJournalTest

15 BOOKKEEPER-381
ReadLastConfirmedOp's Logger class name is wrong In ReadLastConfirmedOp class logger name configured LedgerRecoveryOp.class.    {code}  class ReadLastConfirmedOp implements ReadEntryCallback {  static final Logger LOG = LoggerFactory.getLogger(LedgerRecoveryOp.class);  {code}    It should be ReadLastConfirmedOp.class.
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.ReadLastConfirmedOp

16 BOOKKEEPER-376
LedgerManagers should consider 'underreplication' node as a special Znode Saw this while running the RW tests:    {noformat}  2012-08-22 23:59:35,649 - WARN  - [GarbageCollectorThread:HierarchicalLedgerManager@354] - Exception during garbage collecting ledgers for underreplication of /ledgers  java.io.IOException: java.lang.NumberFormatException: For input string: "underreplicationlocks0000"  	at org.apache.bookkeeper.meta.HierarchicalLedgerManager.getLedgerId(HierarchicalLedgerManager.java:236)  	at org.apache.bookkeeper.meta.HierarchicalLedgerManager.getStartLedgerIdByLevel(HierarchicalLedgerManager.java:254)  	at org.apache.bookkeeper.meta.HierarchicalLedgerManager.doGcByLevel(HierarchicalLedgerManager.java:388)  	at org.apache.bookkeeper.meta.HierarchicalLedgerManager.garbageCollectLedgers(HierarchicalLedgerManager.java:351)  	at org.apache.bookkeeper.bookie.GarbageCollectorThread.doGcLedgers(GarbageCollectorThread.java:226)  	at org.apache.bookkeeper.bookie.GarbageCollectorThread.run(GarbageCollectorThread.java:195)  Caused by: java.lang.NumberFormatException: For input string: "underreplicationlocks0000"  	at java.lang.NumberFormatException.forInputString(Unknown Source)  	at java.lang.Long.parseLong(Unknown Source)  	at java.lang.Long.parseLong(Unknown Source)  	at org.apache.bookkeeper.meta.HierarchicalLedgerManager.getLedgerId(HierarchicalLedgerManager.java:234)  	... 5 more  {noformat}  
2
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.AbstractZkLedgerManager
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.LedgerLayout

17 BOOKKEEPER-371
NPE in hedwig hub client causes hedwig hub to shut down. The hedwig client was connected to a remote region hub that restarted resulting in the channel getting disconnected.       2012-08-15 17:47:42,443 - ERROR - [pool-20-thread-1:TerminateJVMExceptionHandler@28] - Uncaught exception in thread pool-20-thread-1  java.lang.NullPointerException          at org.apache.hedwig.client.netty.HedwigClientImpl.getResponseHandlerFromChannel(HedwigClientImpl.java:323)          at org.apache.hedwig.client.handlers.MessageConsumeCallback.operationFinished(MessageConsumeCallback.java:75)          at org.apache.hedwig.client.handlers.MessageConsumeCallback.operationFinished(MessageConsumeCallback.java:41)          at org.apache.hedwig.server.regions.RegionManager$1$1$1.operationFinished(RegionManager.java:208)          at org.apache.hedwig.server.regions.RegionManager$1$1$1.operationFinished(RegionManager.java:202)          at org.apache.hedwig.server.persistence.ReadAheadCache$PersistCallback.operationFinished(ReadAheadCache.java:194)          at org.apache.hedwig.server.persistence.ReadAheadCache$PersistCallback.operationFinished(ReadAheadCache.java:171)          at org.apache.hedwig.server.persistence.BookkeeperPersistenceManager$PersistOp$1.safeAddComplete(BookkeeperPersistenceManager.java:548)          at org.apache.hedwig.zookeeper.SafeAsynBKCallback$AddCallback.addComplete(SafeAsynBKCallback.java:93)          at org.apache.bookkeeper.client.PendingAddOp.submitCallback(PendingAddOp.java:165)          at org.apache.bookkeeper.client.LedgerHandle.sendAddSuccessCallbacks(LedgerHandle.java:643)          at org.apache.bookkeeper.client.PendingAddOp.writeComplete(PendingAddOp.java:159)          at org.apache.bookkeeper.proto.PerChannelBookieClient.handleAddResponse(PerChannelBookieClient.java:577)          at org.apache.bookkeeper.proto.PerChannelBookieClient$7.safeRun(PerChannelBookieClient.java:525)          at org.apache.bookkeeper.util.SafeRunnable.run(SafeRunnable.java:31)          at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:471)          at java.util.concurrent.FutureTask$Sync.innerRun(FutureTask.java:334)          at java.util.concurrent.FutureTask.run(FutureTask.java:166)          at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1110)          at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:603)          at java.lang.Thread.run(Thread.java:722)      At 2012-08-15 17:47:42,443, the channel was disconnected as well.     I believe the following code in the MessageConsumeCallback is causing this problem.      Channel topicSubscriberChannel = client.getSubscriber().getChannelForTopic(topicSubscriber);          HedwigClientImpl.getResponseHandlerFromChannel(topicSubscriberChannel).getSubscribeResponseHandler()          .messageConsumed(messageConsumeData.msg);    The channel was retrieved without checking if it was closed and then getPipeline().getLast() was called which returned a null value resulting in a NPE. Moreover, we need to check if the returned Response handler is not null because there is a race here if channel.close() is called after we retrieve the channel and before we call messageConsumed().     I guess the same applies for other instances where we use this.  Does the above explanation seem right?     
6
hedwig-client.src.main.java.org.apache.hedwig.client.handlers.MessageConsumeCallback
hedwig-client.src.main.java.org.apache.hedwig.client.handlers.SubscribeResponseHandler
hedwig-client.src.main.java.org.apache.hedwig.client.netty.HedwigClientImpl
hedwig-client.src.main.java.org.apache.hedwig.client.netty.HedwigPublisher
hedwig-client.src.main.java.org.apache.hedwig.client.netty.HedwigSubscriber
hedwig-client.src.main.java.org.apache.hedwig.client.netty.WriteCallback

18 BOOKKEEPER-355
Ledger recovery will mark ledger as closed with -1, in case of slow bookie is added to ensemble during  recovery add Scenario:  ------------  1. Ledger is created with ensemble and quorum size as 2, written with one entry  2. Now first bookie is in the ensemble is made down.  3. Another client fence and trying to recover the same ledger  4. During this time ensemble change will happen and new bookie will be added. But this bookie is not able to connect.  5. This recovery will fail.  7. Now previously added bookie came up.  8. Another client trying to recover the same ledger.  9. Since new bookie is first in the ensemble, doRecoveryRead() is reading from that bookie and getting NoSuchLedgerException and closing the ledger with -1    i.e. Marking the ledger as empty, even though first client had successfully written one entry.
3
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerRecoveryOp
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.PendingReadOp
bookkeeper-server.src.test.java.org.apache.bookkeeper.client.BookieRecoveryTest

19 BOOKKEEPER-349
Entry logger should close all the chennels which are there in Map, instead of closing only current channel. I have seen this on restarting the same bookies from same JVM( in one of mey testcase which I was writing for autoRecovery work), i.e, all channels are not getting closed.  When we restart the same bookie, there are 0.log channel will be a older channel. Since the current code cares about only current logChannel. So, we are not releasing the older channels on EntryLogger shutdown. 
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.EntryLogger

20 BOOKKEEPER-347
 Provide mechanism to detect r-o bookie by the bookie clients This jira to discuss, how the bookie client knows about the bookie running in r-o. This would be required by the client to choose writable bookies during add entries. 
5
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BKException
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookieWatcher
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.PerChannelBookieClient

21 BOOKKEEPER-346
Detect IOExceptions in LedgerCache and bookie should look at next ledger dir(if any) This jira to detect IOExceptions in "LedgerCache" to iterate over all the configured ledger(s).
7
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.FileInfo
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.InterleavedLedgerStorage
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.LedgerCache
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.LedgerCacheImpl
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.LedgerStorage
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.LedgerCacheTest

22 BOOKKEEPER-345
Detect IOExceptions on entrylogger and bookie should consider next ledger dir(if any) This jira to detect IOExceptions in "EntryLogger", and will iterate over all the configured ledger(s) on IOException. Finally if no writable dirs available, will move bookie to r-o mode(if this mode is enabled).     By default(r-o mode will be disabled) the bookie will shutdown if no writable disk available.
11
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.BufferedChannel
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.EntryLogger
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.InterleavedLedgerStorage
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Journal
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.LedgerCacheImpl
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.ServerConfiguration
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookieProtocol
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookieServer
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.EntryLogTest
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.LedgerCacheTest

23 BOOKKEEPER-337
Add entry fails with MetadataVersionException when last ensemble has morethan one bookie failures Scenario:  ========  Start Five BK's  Write ledger's with ensemble three and quroum size=2  while write inprogress down two bookies(Bookies should be in ensemble)  
2
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerHandle
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerMetadata

24 BOOKKEEPER-336
bookie readEntries is taking more time if the ensemble has failed bookie(s) Scenario:    1) Start three bookies. Create ledger with ensemblesize=3, quorumsize=2  2) Add 100 entries to this ledger  3) Make first bookie down and read the entries from 0-99    Output: Each entry is going to fetch from the failed bookie and is waiting for the bookie connection timeout, only after failure going to next bookie.  This is affecting the read entry performance.    Impact: Namenode switching time will be affected by adding this failed bookie readTimeOut also.
6
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.DistributionSchedule
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerEntry
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerHandle
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.PendingReadOp
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.RoundRobinDistributionSchedule
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.ClientConfiguration

25 BOOKKEEPER-330
System.currentTimeMillis usage in Hedwig Need same changes in hedwig server as what did in bookkeeper as BOOKKEEPER-327.
16
hedwig-client.src.main.java.org.apache.hedwig.client.benchmark.BenchmarkPublisher
hedwig-client.src.main.java.org.apache.hedwig.client.benchmark.BenchmarkSubscriber
hedwig-client.src.main.java.org.apache.hedwig.client.benchmark.BenchmarkUtils
hedwig-client.src.main.java.org.apache.hedwig.client.netty.HedwigClientImpl
hedwig-client.src.main.java.org.apache.hedwig.client.netty.HedwigPublisher
hedwig-client.src.main.java.org.apache.hedwig.client.netty.HedwigSubscriber
hedwig-server.src.main.java.org.apache.hedwig.admin.console.HedwigConsole
hedwig-server.src.main.java.org.apache.hedwig.server.benchmark.AbstractBenchmark
hedwig-server.src.main.java.org.apache.hedwig.server.benchmark.BookieBenchmark
hedwig-server.src.main.java.org.apache.hedwig.server.benchmark.BookkeeperBenchmark
hedwig-server.src.main.java.org.apache.hedwig.server.delivery.FIFODeliveryManager
hedwig-server.src.main.java.org.apache.hedwig.server.handlers.PublishHandler
hedwig-server.src.main.java.org.apache.hedwig.server.handlers.SubscribeHandler
hedwig-server.src.main.java.org.apache.hedwig.server.handlers.UnsubscribeHandler
hedwig-server.src.main.java.org.apache.hedwig.server.netty.ServerStats
hedwig-server.src.main.java.org.apache.hedwig.server.persistence.ReadAheadCache

26 BOOKKEEPER-327
System.currentTimeMillis usage in BookKeeper The following exception occured in the bookie statistics logic due to the System time changes. In our bookie cluster its running a periodic syncup scripts just to unify the SystemTime in all the machines. This is causing the problem and resulting ArrayIndexOutOfBoundException.  {code}  Exception in thread "BookieJournal-3181" java.lang.ArrayIndexOutOfBoundsException: -423  at org.apache.bookkeeper.proto.BKStats$OpStats.updateLatency(BKStats.java:126)  at org.apache.bookkeeper.proto.BookieServer.writeComplete(BookieServer.java:655)  at org.apache.bookkeeper.bookie.Journal.run(Journal.java:507)  {code}    This jira is raised to discuss whether to use ??System.nanoTime()?? instead of ??System.currentTimeMillis()??
9
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.GarbageCollectorThread
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Journal
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeper
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BKStats
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookieServer
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.PerChannelBookieClient
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.LocalBookKeeper
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.MathUtils

27 BOOKKEEPER-326
DeadLock during ledger recovery  Deadlock found during ledger recovery. please find the attached thread dump.
3
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerHandle
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.PerChannelBookieClient
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.BookieFailureTest

28 BOOKKEEPER-325
Delay the replication of a ledger if RW found that its last fragment is in underReplication. When RW found that ledger's last fragment is in underReplication state, then we should delay that ledger replication for some grace period. optimally we can replicate other fragments.    The idea is, Whenever it finds the last fragement is under replicated, It can add into PendingReplication list.  There will be a small daemon, which will check for the timeouts of this ledgers.     Once it timed out , it will trigger the normal replication process if it is not in last fragment. Otherwise, it will fence the ledger and will trigger the replication nomally.    see the discussion for more info:  http://markmail.org/message/ruhhxxgvuqnjlu2s#query:+page:1+mid:f6ifo4sizulwiaem+state:results
3
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.AbstractConfiguration
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.ServerConfiguration
bookkeeper-server.src.test.java.org.apache.bookkeeper.client.ClientUtil

29 BOOKKEEPER-319
Manage auditing and replication processes This subtask discusses, how we will manage the whole rereplication processes.  
2
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.ZkUtils
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.ZooKeeperUtil

30 BOOKKEEPER-318
Spelling mistake in MultiCallback log message. {code}  @Override          public void processResult(int rc, String path, Object ctx) {              if (rc != successRc) {                  LOG.error("Error in multi callback : " + rc);                  exceptions.add(rc);              }              tick();          }  {code}
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookkeeperInternalCallbacks

31 BOOKKEEPER-304
Prepare bookie vs ledgers cache and will be used by the Auditor This JIRA discusses how to build bookie -> ledgers cache and this will be used by the Auditor to publish the suspected ledgers of failed bookies.  
2
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.AbstractZkLedgerManager
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.LedgerManager

32 BOOKKEEPER-300
Create Bookie format command Provide a bookie format command. Then the admin would just have to run the command on each machine, which will prepare the bookie env    +Zookeeper paths (znodes):+  - ledger's root path  - bookie's available path    +Directories:+  - Journal directories  - Ledger directories  
14
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.BookieException
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.BookieException.InvalidCookieException
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Cookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.AbstractZkLedgerManager
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.LedgerLayout
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.LedgerManager
bookkeeper-server.src.main.java.org.apache.bookkeeper.meta.LedgerManagerFactory
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookieServer
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.IOUtils
bookkeeper-server.src.main.java.org.apache.bookkeeper.util.ZkUtils
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.CookieTest
bookkeeper-server.src.test.java.org.apache.bookkeeper.test.ZooKeeperUtil

33 BOOKKEEPER-299
Provide LedgerFragmentReplicator which should replicate the fragments found from LedgerChecker Replication worker requires LedgerFragmentReplicator for replicating the actula fragments found from Ledger checker.    Most of the fragment replication code available in BookKeeperAdmin. We can refactor it to LedgerFragmentReplicator and use it.
6
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeper
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin.SingleFragmentCallback
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin.SingleFragmentCallback.WriteCb
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.DigestManager
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerHandle

34 BOOKKEEPER-294
Not able to start the bookkeeper before the ZK session timeout. Not able to start the bookkeeper before the ZK session timeout.    Here i killed the bookie and started again.    {noformat}  2012-06-12 20:00:25,220 - INFO  [main:LedgerCache@65] - openFileLimit is 900, pageSize is 8192, pageLimit is 456781  2012-06-12 20:00:25,238 - ERROR [main:Bookie@453] - ZK exception registering ephemeral Znode for Bookie!  org.apache.zookeeper.KeeperException$NodeExistsException: KeeperErrorCode = NodeExists for /ledgers/available/10.18.40.216:3181  	at org.apache.zookeeper.KeeperException.create(KeeperException.java:119)  	at org.apache.zookeeper.KeeperException.create(KeeperException.java:51)  	at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:778)  	at org.apache.bookkeeper.bookie.Bookie.registerBookie(Bookie.java:450)  	at org.apache.bookkeeper.bookie.Bookie.<init>(Bookie.java:348)  	at org.apache.bookkeeper.proto.BookieServer.<init>(BookieServer.java:64)  	at org.apache.bookkeeper.proto.BookieServer.main(BookieServer.java:249)  {noformat}
3
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.proto.BookieServer
bookkeeper-server.src.test.java.org.apache.bookkeeper.bookie.BookieJournalTest

35 BOOKKEEPER-291
BKMBeanRegistry uses log4j directly It should use slf4j.
1
bookkeeper-server.src.main.java.org.apache.bookkeeper.jmx.BKMBeanRegistry

36 BOOKKEEPER-272
Provide automatic mechanism to know bookie failures The idea is to build automatic mechanism to find out the bookie failures. Setup the bookie failure notifications to start the re-replication process.    There are multiple approaches to findout bookie failures. Please refer the documents attached in BookKeeper-237.
6
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie
bookkeeper-server.src.main.java.org.apache.bookkeeper.bookie.Bookie.NoLedgerException
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerCreateOp
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.LedgerHandle
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.AbstractConfiguration
bookkeeper-server.src.main.java.org.apache.bookkeeper.conf.ServerConfiguration

37 BOOKKEEPER-248
Rereplicating of under replicated data This subtask discusses how we will rereplicate underreplicated entries.
4
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeper
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeper.SyncOpenCallback
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.BookKeeperAdmin.SyncObject

38 BOOKKEEPER-247
Detection of under replication This JIRA discusses how the bookkeeper system will detect underreplication of ledger entries.
2
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.DistributionSchedule
bookkeeper-server.src.main.java.org.apache.bookkeeper.client.RoundRobinDistributionSchedule

39 BOOKKEEPER-78
filterable metadata fields in Hedwig's message definition In order to efficiently implement filtering of Hedwig messages, Hedwig should be able to rely on metadata information. (i.e. without needing to deserialize the content of the message)    Filtering could use a subset of SQL (like in the JMS spec), leading to queries such as :   "header1 like 'a' AND header2 IS NOT NULL"       For that purpose, I propose to add customizable metadata to the definition of Hedwig messages, as header fields.    Metadata must be customizable because it may be arbitrary. We should provide "map-like" containers according to the type of the metadata field. Metadata fields would be accessed by name.    There are predefined headers for JMS that could be added as metadata fields such as : destination (~topic), delivery mode (persistent or not), expiration, priority, timestamp, correlation id (link to other message), reply to, type and redelivered. I think only a subset of these should be predefined headers, if any.    Adding metadata fields to Hedwig messages implies modifying the message definition, which does not break backward compatibility when those fields are added as optional in the protocol buffer definition.           
1
hedwig-protocol.src.main.java.org.apache.hedwig.protocol.PubSubProtocol

40 BOOKKEEPER-55
SubscribeReconnectRetryTask might retry subscription endlessly when another subscription is already successfully created previously For channelDisconnected envent, we try to automatically recover the connection and subscription. But when users call HedwigSubscriber.subscribe() at the same time, it might succeed before the auto recovery. Then the auto recovery can never succeed as the server will report topic busy failure. Then the SubscribeReconnectRetryTask will retry again and again endlessly. We found this in our auto test.    Fix is easy, we just need to firstly check if the channel for this topic and subscribe id is null, if not it means some subscription is already created before, we don't need to bother recover.
1
hedwig-client.src.main.java.org.apache.hedwig.client.handlers.SubscribeReconnectCallback

