BUG REPORT TITLE:
deploy --force fails in CLI GUI

BUG REPORT DESCRIPTION:
The --force flag is not working with the deploy command in CLI GUI.
The CLI CommandContextImpl is generating the wrong DMR request.
