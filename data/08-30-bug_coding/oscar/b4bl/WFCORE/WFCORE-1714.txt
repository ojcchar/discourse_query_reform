BUG REPORT TITLE:
Tab completion doesn't offer more options once prefix match some value

BUG REPORT DESCRIPTION:
Tab completion for CLI variables doesn't offer more values once the variable to be completed match some value.
reproduce

set foo=/subsystem=logging

set foobar=/subsystem=logging/console-handler=CONSOLE

actual

$foo<TAB>

/  :

expected

$foo<TAB>

/  :  foo  foobar
