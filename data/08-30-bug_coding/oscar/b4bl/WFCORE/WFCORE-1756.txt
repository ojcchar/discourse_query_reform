BUG REPORT TITLE:
Adding content to an exploded deployment with overwrite to false should fail if file already exists

BUG REPORT DESCRIPTION:
If overwrite is set to false then adding the file should fail.
