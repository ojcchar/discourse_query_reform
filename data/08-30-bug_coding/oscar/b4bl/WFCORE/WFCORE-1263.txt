BUG REPORT TITLE:
MBeanServer.queryMBeans will include the ObjectInstance for the root mbean for the jboss.as[.expr] JMX domains

BUG REPORT DESCRIPTION:
ModelControllerMBeanHelper.queryMBeans is incorrectly checking for the presence of the root mbean by seeing if a Set<ObjectInstance> contains an ObjectName.
Wrong type.
