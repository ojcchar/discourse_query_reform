BUG REPORT TITLE:
Specify which connection factory to use when creating exchanges and queues

BUG REPORT DESCRIPTION:
Add attributes to <rabbit:queue> and <rabbit:exchange> to specify which connection factory to use or admin to use.
This is required systems which 'straddles' two different application environments each of which have their own RabbitMQ brokers.
You can't run both environments on the same broker as there will be name clashing and messages going to the wrong system.
One work around for this is to use the code configuration instead of XML application context configuration.
