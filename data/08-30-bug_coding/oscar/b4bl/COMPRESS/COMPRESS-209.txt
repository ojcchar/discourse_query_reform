BUG REPORT TITLE:
DOC is incorrectly recognized as TAR

BUG REPORT DESCRIPTION:
Empty DOC is autodetected as TAR by ArchiveStreamFactory.createArchiveInputStream(InputStream).
