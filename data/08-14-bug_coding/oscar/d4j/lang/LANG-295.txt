BUG REPORT TITLE:
StrBuilder contains usages of thisBuf.length when they should use size

BUG REPORT DESCRIPTION:
While fixing LANG-294 I noticed that there are two other places in StrBuilder that reference thisBuf.length and unless I'm mistaken they shouldn't.
