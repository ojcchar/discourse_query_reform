BUG REPORT TITLE:
Requesting CompilationUnit.columnNumber() & CompilationUnit.getPosition(int, int)

BUG REPORT DESCRIPTION:
Requesting 2 new API on org.eclipse.jdt.core.dom.Compilation.

/**
* Returns the column number corresponding to the given source character
* position in the original source string.
Column number are zero-based.
* Return zero if it is beyond the valid range.
*
* @param position a 0-based character position, possibly
*   negative or out of range
* @return the 0-based coloumn number, or <code>0</code> if the character
*    position does not correspond to a source line in the original
*    source file or if column number information is not known for this
*    compilation unit
* @see ASTParser
*/ public int columnNumber(final int position);

/**
* Given a line number and column number return the corresponding
* position in the original source string.
* Returns 0 if no line number information is available for this
* compilation unit or the requested line number is less than one.
* Return the total size of the source string if <code>line</code>
* is greater than the actual number lines in the unit.
* Assume 0 for the column number if <code>column</code> is less than 0
* or return the position of the last character of the line if
<code>column</code>
* is beyond the legal range.
*
* @param line the one-based line number
* @param column the zero-based column number.
* @return the 0-based character position in the source string.
* Return <code>0</code> if line/column number information is not known
* for this compilation unit or the input are not valid.
*/ public int getPosition(int line, int column);

These 2 new APIs are required to correctly implement com.sun.mirror.util.SourcePosition.SourcePostion.column() and allow the posting of markers based on SourcePosition.
