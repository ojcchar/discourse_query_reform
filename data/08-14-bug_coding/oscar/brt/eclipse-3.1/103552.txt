BUG REPORT TITLE:
[osgi] PermissionAdmin must override any conditions

BUG REPORT DESCRIPTION:
When using PermissionAdmin to set permissions with a bundle location.
The permissions set used must override any permissions assigned for that bundle in
ConditionalPermissionAdmin.
