BUG REPORT TITLE:
Console View is missing Ctrl-C in context menu item.

BUG REPORT DESCRIPTION:
The Console View is missing Ctrl+C in context menu item.
The menu item "Copy" is present but the Ctrl+C is not there.
Version 3.1.
M3. Thanks,
Gary.
