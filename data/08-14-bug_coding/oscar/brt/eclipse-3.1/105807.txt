BUG REPORT TITLE:
Toolbars are floating in the middle of views

BUG REPORT DESCRIPTION:
20050802

I am not sure how this occured but I ended up with an outline view with 2 toolbars  - one in the correct place and one floating in the middle of my view
(screenshot to come).

Switching perspective back and forth clears this one up.
