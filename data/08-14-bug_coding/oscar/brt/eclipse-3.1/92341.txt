BUG REPORT TITLE:
DCR - Add SWT.VIRTUAL style to Tree widget

BUG REPORT DESCRIPTION:
Please add a possibility to lazy-load the first tree level (like SWT.VIRTUAL for the table widget).
