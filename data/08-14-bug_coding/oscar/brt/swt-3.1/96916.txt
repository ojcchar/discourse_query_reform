BUG REPORT TITLE:
Spinner.setToolTipText() doesn't work

BUG REPORT DESCRIPTION:
No tooltip appears on the spinner widget after Spinner.setToolTipText().
