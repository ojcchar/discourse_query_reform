BUG REPORT TITLE:
Allow to overwrite CrawlDatum's with injected entries

BUG REPORT DESCRIPTION:
Injector's reducer does not permit overwriting existing CrawlDatum entries.
It is, however, useful to optionally overwrite so users can reset metadata manually.
