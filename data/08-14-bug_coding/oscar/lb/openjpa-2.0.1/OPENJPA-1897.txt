BUG REPORT TITLE:
Sybase reserved words may not be used as column names

BUG REPORT DESCRIPTION:
Further investigation on reserved names for Sybase shows that few (if any) of the reserved words may be used as column names.
Erring on the side of caution and disabling them all.
