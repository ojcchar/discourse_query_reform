BUG REPORT TITLE:
CharsRef.subSequence broken

BUG REPORT DESCRIPTION:
Looks like CharsRef.subSequence() is currently broken

It is implemented as:
{code}
@Override public CharSequence subSequence(int start, int end) {
// NOTE: must do a real check here to meet the specs of CharSequence if (start < 0 || end > length || start > end) { throw new IndexOutOfBoundsException();
} return new CharsRef(chars, offset + start, offset + end);
}
{code}

Since CharsRef constructor_ is (char[] chars, int offset, int length),
Should Be:
{code}
@Override public CharSequence subSequence(int start, int end) {
// NOTE: must do a real check here to meet the specs of CharSequence if (start < 0 || end > length || start > end) { throw new IndexOutOfBoundsException();
} return new CharsRef(chars, offset + start, end - start);
}
{code}
