BUG REPORT TITLE:
Untitled buffer is lost after C+n

BUG REPORT DESCRIPTION:
When I'm editing an untitled buffer and call File &gt; New, the edited buffer is lost from the buffer list.

jEdit SVN revision 12783
