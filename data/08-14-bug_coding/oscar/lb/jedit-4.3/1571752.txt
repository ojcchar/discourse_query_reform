BUG REPORT TITLE:
'Add Explicit Fold'  in PHP mode - wrong comments

BUG REPORT DESCRIPTION:
jEdit version: 4.3pre7
Java version: 1.6.0-beta2

Before 'Add Explicit fold' the content of buffer looks like this \('X' means selection boundaries\):

&lt;? php
Xfunction foo\(\) \{

\}X
/\* :folding=explicit:\*/
?&gt;

After:

&lt;? php
&lt;\!
--\{\{\{  --&gt;
function foo\(\) \{

\} //\}\}\}
/\* :folding=explicit:\*/
?&gt;

If is between '&lt;? php' and 'function' empty line, then it works OK.
