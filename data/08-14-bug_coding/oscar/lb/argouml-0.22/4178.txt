BUG REPORT TITLE:
Allow commentedes to be removed from the proppanel

BUG REPORT DESCRIPTION:
Allow commentedes to be removed from the proppanel field "Annotated Elements".
This should be done with a popup menu for this field, containing one entry:
"Remove".
