BUG REPORT TITLE:
CVS should update editor determination for remote files

BUG REPORT DESCRIPTION:
See bug 97946 for the background.
Here's the part that applies to CVS"

In order for this to work you'll need to start using getDefaultEdito(filename, content type) - the new editor bindings can only be resolved when the content type is known.
You'll need to pass the contents of the file to
IContentTypeManager.findContentTypeFor(inputstream, filename) to determine this.

We should do this when opeing files from the repo view and the annotate command.
