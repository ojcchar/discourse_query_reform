BUG REPORT TITLE:
org.eclipes.team.ui plugin's startup code forces compare to be loaded

BUG REPORT DESCRIPTION:
3.1 M3

I wrote tests that ensure plug-ins like Search and Compare aren't loaded when opening a Java editor.

The one for compare fails because org.eclipes.team.ui forces compare to be loaded in its start(BundleContext) method:
Platform.getAdapterManager().
registerAdapters(factory, DiffNode.class);

The direct reference to DiffNode causes the compare plug-in to be loaded even if it is not needed yet.
Only when a compare will be done it needs to be loaded and the adapter being registered.

Test Case:
1. add startup() method to CompareUIPlugin
2. put a breakpoint there
