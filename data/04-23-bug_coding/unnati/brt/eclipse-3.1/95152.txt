BUG REPORT TITLE:
[search] F3 can't find synthetic constructor_

BUG REPORT DESCRIPTION:
Build: I20050513-0010

1) Add org.eclipse.debug.ui to the search path (i.e., by clicking "Add to Java
Search" in the plugins view.
2) Open type on "ProcessConsole" (class file with source attached)
3) Go to line 483:

InputReadJob readJob = new InputReadJob(streamsProxy);

4) Highlight the InputReadJob constructor_ and hit F3.

-> It opens a new class file editor, positioned at the top of the file.

5) The outline view in this editor has the constructor_
InputReadJob(ProcessConsole, IStreamsProxy).
Clicking this entry in the outline view does not jump to the constructor_ in the editor.

The mapping of class file to source is not handling the synthetic addition of the enclosing class by the compiler.
This breaks any kind of navigation to the corresponding constructor_ in the source attachment.
