BUG REPORT TITLE:
Migrate pre-equinox.app launch configs

BUG REPORT DESCRIPTION:
Existing Eclipse app / Plugin Junit launch configurations need to be updated to include the org.eclipse.equinox.app plug-in if they have a hand-crafted list of plug-ins.

Otherwise, they will fail to launch out of the box and this may result in an influx of bug reports.
