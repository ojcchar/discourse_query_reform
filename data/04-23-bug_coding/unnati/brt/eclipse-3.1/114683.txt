BUG REPORT TITLE:
support for deprecating extension points

BUG REPORT DESCRIPTION:
From time to time we need to refactor plugins and move extension points.
Since extension points derive their identity from that of their containing plugin, moving implies a rename.
The old ext pt must be left behind for compatibility and the accessing code updated to read both the old and new ext pts when looking for extensions.

Having done this, it would be nice to be able to deprecate the old extension point definition and give developers a chance to update their extensions to use the new extension point.

The deprecation indication could go in the schema though it is unclear that this is shipped in the runtime builds of plugins.
Alternatively it could be an additional attribute on the extension point markup itself.
In either case,
PDE UI would be updated ot have this notion and add the appropriate error/warning markers in the plugin.xml files.
Of course, quick fixes would be optimal.
:-)
