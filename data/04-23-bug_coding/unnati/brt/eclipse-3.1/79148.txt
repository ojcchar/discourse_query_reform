BUG REPORT TITLE:
Invalid errors reported in CVS console while adding items to repository

BUG REPORT DESCRIPTION:
When while adding items to CVS repository Eclipse reports error messages in CVS console about things which are not errors.
For example:

Error: module: cvs add: scheduling file `filename' for addition
Error: module: cvs add: use `cvs commit' to add these files permanently

3.1M3/Linux GTK
