BUG REPORT TITLE:
[code assist] Code assist does not recommend methods in anonymous enum subclass

BUG REPORT DESCRIPTION:
In RC3.
Consider code:

public enum Foo {
A { tos<<>>
};
}

Where the cursor is at <<>>.
Invoking code assist does not offer to override toString: it only offers class names.
