BUG REPORT TITLE:
Synchronizing a closed project results in "no changes"

BUG REPORT DESCRIPTION:
If I try to synchronize a project, that is closed, I get "no changes", even if there are local and/or CVS differences.
There should be an error message saying, that the project is closed, open project to synchronize.
