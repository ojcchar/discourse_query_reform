BUG REPORT TITLE:
Fully qualified paths in antfile attribute do not open file

BUG REPORT DESCRIPTION:
<project name="internalProject" default="entry">
<target name="entry" depends="default">
<echo>entry</echo>
</target>
<target name="default">
<echo>default</echo>
<ant antfile="C:\apache-ant-1.6.5\bin\antBuildfile.xml"></ant>
</target>
</project>

Ctrl clicking on the antfile attribute should open the external antBuildfile.xml
