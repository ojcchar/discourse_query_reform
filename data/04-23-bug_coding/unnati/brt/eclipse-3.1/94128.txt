BUG REPORT TITLE:
Target if/unless not marked as occurrences

BUG REPORT DESCRIPTION:
If the cursor is positioned in the if or unless attribute of a target, the property reference is not marked as an occurrence.
