BUG REPORT TITLE:
Changes to outgoing change sets forgotten

BUG REPORT DESCRIPTION:
3.1 RC1, Motif, KDE 3.3.2, X.org 6.8.2, Linux 2.6.11

For a while I thought I was just going crazy, but now I'm almost certain.
After creating an initial group of change sets, it appears that any further changes are forgotten after restarting the workbench.

I have tried renaming an existing change set, and creating new ones.

Every time I restart Eclipse I get the following in the log:

!
ENTRY org.eclipse.core.resources 2 10035 2005-06-02 10:34:08.49
!
MESSAGE A workspace crash was detected.
The previous session did not exit normally.
