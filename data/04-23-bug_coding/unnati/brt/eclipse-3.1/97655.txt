BUG REPORT TITLE:
Problem with new Bundle Activation semantics in 3.1M7 (vs. 3.0)

BUG REPORT DESCRIPTION:
In version 3.0 of the platform, a plug-in that
- does not define bundle manifests and
- does not define a custom plug-in lifecycle class is activated whenever any class in the plug-in was loaded.
That is, the apparent default setting of the implied Eclipse-AutoStart header is "true".

Upon updating my Eclipse platform to 3.0 M7, I find that this plug-in's underlying bundle no longer auto-starts when its extensions are loaded and executed.
In consequence, the evaluation of XML enablement expressions that depend on PropertyTesters defined by my plug-in do not work because the property test always results in EvaluationResult.NOT_LOADED.

I can fix the problem in M7 by either:
- providing a plug-in lifecycle class, or
- converting to a bundle manifest and explicitly setting
Eclipse-AutoStart: true

My concerns are:

1) Why the change in bundle activation?
It appears from bug 89261 that this was a deliberate change.
Plug-ins built for Eclipse 3.0 will not work correctly in 3.1 without being migrated, which may not be under the control of the user/developer/integrator.

2) Core APIs such as expression property testers that check bundles for active-ness using Bundle.getState() will cause unexpected results for people migrating from 3.0 to 3.1.
My own product does a lot of this checking for active state.
People need to know that they have to explicitly make their bundles auto-start in order for these APIs to work.

3) What is the real meaning of "bundle activation"?
Why doesn't the bundle state always change to Bundle.ACTIVE whenever *any* of its code is run?
It doesn't matter whether the bundle requires activator hooks or not;
whenever it runs code, it is logically "active".
Currently, the
Bundle.getState() method is not a useful API for determining whether a bundle is engaged in doing stuff in the platform.

4) The Eclipse-AutoStart header does not appear to be documented in the on-line help, and the bundle manifest editor in PDE doesn't provide any
UI support for it.
So, it's not easy to find the work-around.

Thanks in advance for your response!
