BUG REPORT TITLE:
ClassNotLoadedException when trying to change a value

BUG REPORT DESCRIPTION:
public class Test { static class Inner {
} public static void main(String[] args) {
Inner inner= null;
System.out.println(1);  //  <- breakpoint here
}
}

Debug to the breakpoint.
Right-click on the 'inner' variable > change value ...
A ClassNotLoadedException dialog appears.
