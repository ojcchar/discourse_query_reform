BUG REPORT TITLE:
[WorkbenchParts]IPartListener2#partInputChanged is not being sent

BUG REPORT DESCRIPTION:
3.1 M6

We (ASTProvider) receive partActivated(IWorkbenchPartReference ref) where:
ref.getId() -> null ref.getPart(true).
getSite() -> correct id.

Test Case:
0. enable mark occurrences
1. enable search to reuse the editor (see Search preference page)
2. do a Java Search where you have matches in more than one file
3. select the first match
4. step through the matches until the second file gets opened
5. activate the editor by clicking on the tab
==> occurrence marking not working

To see the null value you can put a breakpoint in
ASTProvider.ActivationListener.isJavaEditor().

Might be related to bug 89374.
