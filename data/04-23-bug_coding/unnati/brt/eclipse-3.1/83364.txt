BUG REPORT TITLE:
Buildfile dialog does not recall the last directory selected.

BUG REPORT DESCRIPTION:
Feature request: Buildfile dialog does not recall the last directory selected.
Pick a buildfile and click ok.
Open the dialog again and the dialog could recall where it was used last.

This would make it easier to add more build files in an interactive manner.
