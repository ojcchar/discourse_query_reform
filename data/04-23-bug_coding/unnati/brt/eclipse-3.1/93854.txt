BUG REPORT TITLE:
IAE in  Util.scanTypeSignature when scanning a signature retrieved from a binding key

BUG REPORT DESCRIPTION:
Thread [main] (Suspended (breakpoint at line 2319 in Util))
Util.scanTypeSignature(char[], int) line: 2319
Signature.getTypeParameters(char[]) line: 1382
Signature.getTypeParameters(String) line: 1407
JavaElementLabels.getTypeLabel(IType, long, StringBuffer) line: 821
JavaElementLabels.getElementLabel(IJavaElement, long, StringBuffer) line: 398
JavaElementLabels.getElementLabel(IJavaElement, long) line: 363
JavaElementLabels.getTextLabel(Object, long) line: 345
HierarchyLabelProvider(JavaUILabelProvider).
getText(Object) line: 161
HierarchyLabelProvider.getText(Object) line: 126
DecoratingJavaLabelProvider(DecoratingLabelProvider).
getText(Object) line: 118
DecoratingJavaLabelProvider(DecoratingLabelProvider).
updateLabel(ViewerLabel,
Object) line: 208
TraditionalHierarchyViewer(StructuredViewer).
buildLabel(ViewerLabel, Object,
IViewerLabelProvider) line: 1846
TraditionalHierarchyViewer(TreeViewer).
doUpdateItem(Item, Object) line: 228
AbstractTreeViewer$UpdateItemSafeRunnable.run() line: 85
InternalPlatform.run(ISafeRunnable) line: 1031
Platform.run(ISafeRunnable) line: 757
JFaceUtil$1.
run(ISafeRunnable) line: 44
SafeRunnable.run(ISafeRunnable) line: 148
TraditionalHierarchyViewer(AbstractTreeViewer).
doUpdateItem(Widget, Object, boolean) line: 621
StructuredViewer$UpdateItemSafeRunnable.run() line: 434
InternalPlatform.run(ISafeRunnable) line: 1031
Platform.run(ISafeRunnable) line: 757
JFaceUtil$1.
run(ISafeRunnable) line: 44
SafeRunnable.run(ISafeRunnable) line: 148
TraditionalHierarchyViewer(StructuredViewer).
updateItem(Widget, Object) line: 1754
TraditionalHierarchyViewer(AbstractTreeViewer).
createTreeItem(Widget, Object, int) line: 535
AbstractTreeViewer$1.
run() line: 514
BusyIndicator.showWhile(Display, Runnable) line: 69
TraditionalHierarchyViewer(AbstractTreeViewer).
createChildren(Widget) line: 494
TraditionalHierarchyViewer(AbstractTreeViewer).
internalExpandToLevel(Widget, int) line: 1120
TraditionalHierarchyViewer(AbstractTreeViewer).
internalExpandToLevel(Widget, int) line: 1129
TraditionalHierarchyViewer(AbstractTreeViewer).
expandToLevel(Object, int) line: 658
TraditionalHierarchyViewer(AbstractTreeViewer).
expandToLevel(int) line: 641
TraditionalHierarchyViewer.updateContent(boolean) line: 61
TypeHierarchyViewPart$11.
run() line: 1090
BusyIndicator.showWhile(Display, Runnable) line: 69
TypeHierarchyViewPart.updateHierarchyViewer(boolean) line: 1093
TypeHierarchyViewPart.updateInput(IJavaElement) line: 541
TypeHierarchyViewPart.setInputElement(IJavaElement) line: 486
OpenTypeHierarchyUtil.openInViewPart(IWorkbenchWindow, IJavaElement) line: 98
OpenTypeHierarchyUtil.open(IJavaElement[], IWorkbenchWindow) line: 75
OpenTypeHierarchyAction.run(IJavaElement[]) line: 181
OpenTypeHierarchyAction.run(ITextSelection) line: 143
OpenTypeHierarchyAction(SelectionDispatchAction).
dispatchRun(ISelection) line: 226
OpenTypeHierarchyAction(SelectionDispatchAction).
run() line: 198
OpenTypeHierarchyAction(Action).
runWithEvent(Event) line: 996
ActionHandler.execute(Map) line: 182
LegacyHandlerWrapper.execute(ExecutionEvent) line: 108
Command.execute(ExecutionEvent) line: 312
ParameterizedCommand.execute(Object, Object) line: 396
WorkbenchKeyboard.executeCommand(Binding, Object) line: 452
WorkbenchKeyboard.press(List, Event) line: 722
WorkbenchKeyboard.processKeyEvent(List, Event) line: 766
WorkbenchKeyboard.filterKeySequenceBindings(Event) line: 543
WorkbenchKeyboard.access$3(WorkbenchKeyboard, Event) line: 486
WorkbenchKeyboard$KeyDownFilter.handleEvent(Event) line: 110
EventTable.sendEvent(Event) line: 82
Display.filterEvent(Event) line: 781
StyledText(Widget).
sendEvent(Event) line: 841
StyledText(Widget).
sendEvent(int, Event, boolean) line: 866
StyledText(Widget).
sendEvent(int, Event) line: 851
StyledText(Widget).
sendKeyEvent(int, int, int, int, Event) line: 879
StyledText(Widget).
sendKeyEvent(int, int, int, int) line: 875
StyledText(Widget).
wmKeyDown(int, int, int) line: 1467
StyledText(Control).
WM_KEYDOWN(int, int) line: 3342
StyledText(Control).
windowProc(int, int, int, int) line: 3062
Display.windowProc(int, int, int, int) line: 3493
OS.DispatchMessageW(MSG) line: not available [native method]
OS.DispatchMessage(MSG) line: 1650
Display.readAndDispatch() line: 2532
Workbench.runEventLoop(Window$IExceptionHandler, Display) line: 1592
Workbench.runUI() line: 1556
Workbench.createAndRunWorkbench(Display, WorkbenchAdvisor) line: 314
PlatformUI.createAndRunWorkbench(Display, WorkbenchAdvisor) line: 143
IDEApplication.run(Object) line: 103
PlatformActivator$1.
run(Object) line: 230
EclipseStarter.run(Object) line: 345
EclipseStarter.run(String[], Runnable) line: 158
NativeMethodAccessorImpl.invoke0(Method, Object, Object[]) line: not available
[native method]
NativeMethodAccessorImpl.invoke(Object, Object[]) line: 85
NativeMethodAccessorImpl.invoke(Method, Object, Object[]) line: 58
DelegatingMethodAccessorImpl.invoke(Method, Object, Object[]) line: 60
Method.invoke(Object, Object[]) line: 391
Main.invokeFramework(String[], URL[]) line: 328
Main.basicRun(String[]) line: 272
Main.run(String[]) line: 974
Main.main(String[]) line: 950

Binding key retrieved from Java element:
Ljava/util/Collections$CheckedMap<TK;T,V;>;

Signature retrieved from binding key for which we ask type parameters:
<K:,V:>Ljava.util.Collections$CheckedMap;

char[] in scanTypeSignature
[<, K, :, ,, V, :, >, L, j, a, v, a, .
, u, t, i, l, .
, C, o, l, l, e, c, t, i, o, n, s, $, C, h, e, c, k, e, d, M, a, p, ;]

start in scanTypeSignature: 3

Setting to major since this blocks type hierarchies on generic types.
