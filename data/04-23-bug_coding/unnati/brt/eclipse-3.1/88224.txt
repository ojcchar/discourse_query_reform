BUG REPORT TITLE:
[1.5][DOM] ASTRewriteAnalyzer blows up with local enums

BUG REPORT DESCRIPTION:
Looking at bug 81329, I added support to sort members inside enum declarations.
But on the test case in org.eclipse.jdt.core.tests.model.SortCompilationUnitElementsTests.test024, I end up with:
org.eclipse.text.edits.MalformedTreeException: Range of child edit lies outside of parent edit
	at org.eclipse.text.edits.TextEdit.internalAdd(TextEdit.java:738)
	at org.eclipse.text.edits.TextEdit.addChild(TextEdit.java:332)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.addEdit(ASTRewriteAnalyzer.java:219)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doTextRemove(ASTRewriteAnalyzer.java:259)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doTextRemoveAndVisit(ASTRewriteAnalyzer.java:267)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer$ListRewriter.rewriteList(ASTRewriteAnalyzer.java:566)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer$ListRewriter.rewriteList(ASTRewriteAnalyzer.java:455)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.rewriteNodeList(ASTRewriteAnalyzer.java:968)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:3091)
	at org.eclipse.jdt.core.dom.EnumDeclaration.accept0(EnumDeclaration.java:272)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.voidVisit(ASTRewriteAnalyzer.java:316)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doTextRemoveAndVisit(ASTRewriteAnalyzer.java:270)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer$ListRewriter.rewriteList(ASTRewriteAnalyzer.java:566)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.rewriteParagraphList(ASTRewriteAnalyzer.java:913)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:1441)
	at org.eclipse.jdt.core.dom.TypeDeclaration.accept0(TypeDeclaration.java:466)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.voidVisit(ASTRewriteAnalyzer.java:316)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doVisitUnchangedChildren(ASTRewriteAnalyzer.java:353)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:2694)
at
org.eclipse.jdt.core.dom.TypeDeclarationStatement.accept0(TypeDeclarationStatement.java:187)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doVisit(ASTRewriteAnalyzer.java:278)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.voidVisitList(ASTRewriteAnalyzer.java:341)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doVisitUnchangedChildren(ASTRewriteAnalyzer.java:358)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:1534)
	at org.eclipse.jdt.core.dom.Block.accept0(Block.java:133)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.voidVisit(ASTRewriteAnalyzer.java:316)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doVisitUnchangedChildren(ASTRewriteAnalyzer.java:353)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:1478)
	at org.eclipse.jdt.core.dom.MethodDeclaration.accept0(MethodDeclaration.java:486)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.voidVisit(ASTRewriteAnalyzer.java:316)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doTextRemoveAndVisit(ASTRewriteAnalyzer.java:270)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer$ListRewriter.rewriteList(ASTRewriteAnalyzer.java:566)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.rewriteParagraphList(ASTRewriteAnalyzer.java:913)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:1441)
	at org.eclipse.jdt.core.dom.TypeDeclaration.accept0(TypeDeclaration.java:466)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.voidVisit(ASTRewriteAnalyzer.java:316)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.doTextRemoveAndVisit(ASTRewriteAnalyzer.java:270)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer$ListRewriter.rewriteList(ASTRewriteAnalyzer.java:566)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.rewriteParagraphList(ASTRewriteAnalyzer.java:913)
at
org.eclipse.jdt.internal.core.dom.rewrite.ASTRewriteAnalyzer.visit(ASTRewriteAnalyzer.java:1323)
	at org.eclipse.jdt.core.dom.CompilationUnit.accept0(CompilationUnit.java:294)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.rewrite.ASTRewrite.rewriteAST(ASTRewrite.java:184)
at
org.eclipse.jdt.internal.core.SortElementsOperation.processElement(SortElementsOperation.java:197)
at
org.eclipse.jdt.internal.core.SortElementsOperation.executeOperation(SortElementsOperation.java:86)
at
org.eclipse.jdt.internal.core.JavaModelOperation.run(JavaModelOperation.java:710)
	at org.eclipse.core.internal.resources.Workspace.run(Workspace.java:1702)
at
org.eclipse.jdt.internal.core.JavaModelOperation.runOperation(JavaModelOperation.java:771)
at
org.eclipse.jdt.core.util.CompilationUnitSorter.sort(CompilationUnitSorter.java:174)
at
org.eclipse.jdt.core.tests.model.SortCompilationUnitElementsTests.sortUnit(SortCompilationUnitElementsTests.java:65)
at
org.eclipse.jdt.core.tests.model.SortCompilationUnitElementsTests.sortUnit(SortCompilationUnitElementsTests.java:42)
at
org.eclipse.jdt.core.tests.model.SortCompilationUnitElementsTests.test024(SortCompilationUnitElementsTests.java:1648)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at
sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:324)
	at junit.framework.TestCase.runTest(TestCase.java:154)
	at junit.framework.TestCase.runBare(TestCase.java:127)
	at junit.framework.TestResult$1.protect(TestResult.java:106)
	at junit.framework.TestResult.runProtected(TestResult.java:124)
	at junit.framework.TestResult.run(TestResult.java:109)
	at junit.framework.TestCase.run(TestCase.java:118)
	at junit.framework.TestSuite.runTest(TestSuite.java:208)
at
org.eclipse.jdt.core.tests.model.SuiteOfTestCases$Suite.runTest(SuiteOfTestCases.java:99)
	at junit.framework.TestSuite.run(TestSuite.java:203)
at
org.eclipse.jdt.core.tests.model.SuiteOfTestCases$Suite.superRun(SuiteOfTestCases.java:83)
at
org.eclipse.jdt.core.tests.model.SuiteOfTestCases$1.protect(SuiteOfTestCases.java:71)
	at junit.framework.TestResult.runProtected(TestResult.java:124)
at
org.eclipse.jdt.core.tests.model.SuiteOfTestCases$Suite.run(SuiteOfTestCases.java:80)
at
org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:474)
at
org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:342)
at
org.eclipse.pde.internal.junit.runtime.RemotePluginTestRunner.main(RemotePluginTestRunner.java:36)
at
org.eclipse.pde.internal.junit.runtime.CoreTestApplication.run(CoreTestApplication.java:24)
at
org.eclipse.core.internal.runtime.PlatformActivator$1.run(PlatformActivator.java:228)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:338)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:151)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at
sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:324)
	at org.eclipse.core.launcher.Main.invokeFramework(Main.java:268)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:260)
	at org.eclipse.core.launcher.Main.run(Main.java:887)
	at org.eclipse.core.launcher.Main.main(Main.java:871)

It does work if I change the code a bit.
So I believe this is a bug in the
ASTRewriteAnalyzer and not in the SortElementOperation.
Let me know if you believe the problem comes from the SortElementOperation.
