BUG REPORT TITLE:
[Contributions] interactions: No submenu for "Run As"

BUG REPORT DESCRIPTION:
I20050803-0800

I have org.eclipse.swt, org.eclipse.swt.examples, and org.eclipse.swt.gtk.linux.x86 in my workspace.
When I open the context menu on a .java file in the package explorer for one of the eclipse examples, the option
"Run As" has no submenu at all.
