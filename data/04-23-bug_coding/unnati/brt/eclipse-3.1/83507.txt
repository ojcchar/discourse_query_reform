BUG REPORT TITLE:
run to line test failure

BUG REPORT DESCRIPTION:
This test seems to be failing often on Linux.
The test closes all editors, adds a part listener, launches to a breakpoint and waits for an editor to open before it attempts a "run to line".
There are three tests, and the first of the three seems to fail often.
It does not seem to fail on windows.
The test claims the editor does not open, but I suspect it is open and the test wasn't notified for some reason.

Editor did not open

junit.framework.AssertionFailedError: Editor did not open at org.eclipse.jdt.debug.tests.core.RunToLineTests.runToLine
(RunToLineTests.java:164)
at org.eclipse.jdt.debug.tests.core.RunToLineTests.testRunToLine
(RunToLineTests.java:104)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke
(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke
(DelegatingMethodAccessorImpl.java:25)
at org.eclipse.jdt.debug.tests.DebugSuite$1.run(DebugSuite.java:53)
at java.lang.Thread.run(Thread.java:534)
