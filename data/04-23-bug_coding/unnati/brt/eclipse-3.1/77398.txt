BUG REPORT TITLE:
[1.5] Organize imports does not honor enum types [code manipulation]

BUG REPORT DESCRIPTION:
200410260800: When running organize imports on a cu with references to enum types, existing imports to enum types are incorrectly removed
