BUG REPORT TITLE:
cannot use extension point in same i.e. defining plug-in

BUG REPORT DESCRIPTION:
3.1M3 (200411050810):

I cannot use an extension point in the same plug-in where I defined it.
The plug-in project is created with an OSGi bundle manifest in an empty workspace.

'cannot use' means that it won't appear in the New Extension Wizards 'Extension
Point Selection' unless I disable 'Show only extension points from the required plug-ins'.
Even then the name of the extension point is not prefixed with the bundle name.
After adding the extension point to the plug-in I can't create the defined extension.
In the popup menu only 'generic' is displayed.
