BUG REPORT TITLE:
[Progress] Tooltip in progress dialog is shortened

BUG REPORT DESCRIPTION:
Eclipse 3.1

The tooltip should be the full text while the text itself may be shortened.

See attached pic.
