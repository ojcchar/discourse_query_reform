BUG REPORT TITLE:
Eclipse and JVM Shutdown Hooks

BUG REPORT DESCRIPTION:
We are trying to port legacy runtime environments onto the Eclipse platform.
Some of these environments use JVM shutdown hooks that perform some amount of cleanup.
Unfortunately, in an Eclipse environment the bundles are stopped before the shutdown hooks are executed, which severely restricts what can be done in the hook.
For example, no classes may be loaded.

In general, we do not own the code for the hooks, so we cannot easily change the code to fit some other mechanism (e.g. bundle start levels).

We can, however, live without a standard shutdown of the Eclipse platform.
That is, if bundles stayed active during the JVM shutdown, I believe that would be acceptable.

Here is a very trivial example.
If you run it, you should get the following output:

osgi>
### adding shutdown hook
### in shutdown hook com.acme.Main$ShutdownHook java.lang.NoClassDefFoundError: com/acme/Foo
        at com.acme.Main$ShutdownHook.run(Main.java:16)

-------------- main application ----------- package com.acme;

import org.eclipse.core.runtime.IPlatformRunnable;

public class Main implements IPlatformRunnable {

public Object run(Object arg0) throws Exception {
System.out.println("### adding shutdown hook");
Runtime.getRuntime().
addShutdownHook(new ShutdownHook());
return null;
}

private static class ShutdownHook extends Thread { public void run() {
System.out.println("### in shutdown hook " + this.getClass().
getName());
Foo f = new Foo();
}
}
}

--------------- auxiliary class Foo --------------------

package com.acme;

public class Foo {
}

-------------- plugin.xml -----------------------

<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.0"?>
<plugin>

<extension id="Test" point="org.eclipse.core.runtime.applications">
<application>
<run class="com.acme.Main"/>
</application>
</extension>
</plugin>

------------------ manifest.mf ----------------
Manifest-Version: 1.0
Bundle-Name: Sample Bundle
Bundle-SymbolicName: com.bugsrus
Bundle-Version: 1.0.0
Bundle-ClassPath: testbundle.jar
Bundle-Vendor: Acme Corp.
Bundle-Localization: plugin
Eclipse-AutoStart: true
Import-Package: org.eclipse.core.runtime, org.osgi.framework
