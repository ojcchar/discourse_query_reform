BUG REPORT TITLE:
Up and Down buttons should be disabled when viewer is sorted

BUG REPORT DESCRIPTION:
The Extensions page of the plug-in editor now has a sort toolbar button in the top right.

When this button is selected, the Up and Down buttons in the tree viewer should be disabled.
