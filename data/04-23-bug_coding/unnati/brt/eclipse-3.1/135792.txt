BUG REPORT TITLE:
New flag for incompatible environments

BUG REPORT DESCRIPTION:
To improve the error reporting for manifest.mf files, we should flag the Eclipse-PlatformFilter and Bundle-RequiredExecutionEnvironment headers, if they reference an environment that renders them unresolved.

This flag should be customizable.

Janek, please add a new flag to the Plug-ins tab of the Plug-in Development > Compilers preference page.

The flag name should be "Incompatible environment" and the default severity should be a warning.

Don't forget the mnemonic ;-)  Thanks.
