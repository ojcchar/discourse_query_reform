BUG REPORT TITLE:
[target] Target platform preferences should not be imported

BUG REPORT DESCRIPTION:
The individual project selections in the "Preferences->Plug-in Development-
>Target Platform" preference page are not being stored in exported preferences files.
