BUG REPORT TITLE:
[WorkbenchParts] Cannot reopen closed views

BUG REPORT DESCRIPTION:
N20050725-0010

- new workspace
- close Problems view
- Window > Show View > Problems
-> view is not opened, nothing in the log

Opening new views works, but opening a view that has previously been closed doesn't work.
Already broken in N20050720-0010, but works in 3.1.
