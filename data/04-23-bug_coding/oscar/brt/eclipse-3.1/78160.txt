BUG REPORT TITLE:
Invalid classfile for problem type

BUG REPORT DESCRIPTION:
Build 3.1M3

When compiling the following testcase, the problem classfile issued has an invalid signature in it:

public class X extends Y<Y<?
>> {
} class Y<T> {}

----------
1. ERROR in d:\src\X.java (at line 45)
public class X extends Y<Y<?
>> {
^
The type X cannot extend or implement Y<Y<?
>>.
A supertype may not specify any wildcard
----------
1 problem (1 error)
java.lang.ClassFormatError: Field "has inconsistent hierarchy" in class X has illegal signature "V"
	at java.lang.ClassLoader.defineClass1(Native Method)
	at java.lang.ClassLoader.defineClass(ClassLoader.java:620)
	at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:124)
	at java.net.URLClassLoader.defineClass(URLClassLoader.java:260)
	at java.net.URLClassLoader.access$100(URLClassLoader.java:56)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:195)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:188)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:306)
