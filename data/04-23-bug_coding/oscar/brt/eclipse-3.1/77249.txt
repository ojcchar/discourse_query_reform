BUG REPORT TITLE:
Annotation on class cancels "public" modifier

BUG REPORT DESCRIPTION:
(This is in 3.1 M2.)
The org.eclipse.jdt.core.dom.CompilationUnit instance corresponding to the class Foo below has no "public" modifier, although it should.
Note that if the declaration is simplified to just "@Jpf.
Controller public class Foo {...}", then the instance does have a "public" modifier.

@Jpf.
Controller( catches={
@Jpf.
Catch(type=java.lang.Exception.class, method="handleException"),
@Jpf.
Catch(type=PageFlowException.class, method="handlePageFlowException")
}
)
public class Foo {
...
}
