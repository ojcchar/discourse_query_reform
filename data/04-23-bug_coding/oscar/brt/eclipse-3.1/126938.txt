BUG REPORT TITLE:
Cannot import Installed JREs from preference file

BUG REPORT DESCRIPTION:
Below are the contents of the preferences file I am trying to import.
To reproduce, you may need to change the location of the JRE since you probably don't have it installed in the directory I do.

------------------------------
#Wed Jan 04 09:20:23 PST 2006 file_export_version=3.0

@org.
eclipse.jdt.launching=3.1.0
#Java > Installed JREs
/instance/org.
eclipse.jdt.launching/org.eclipse.jdt.launching.PREF_VM_XML=<?xml version\="1.0" encoding\="UTF-8"?>\r\n<vmSettings defaultVM\="57,org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType13,1136396225475">\r\n<vmType id\="org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType">\r\n<vm id\="1136396225475" javadocURL\="http\://java.sun.com/j2se/1.5.0/docs/api/" name\="jdk1.5.0_04" path\="C\:\\ngp_tools\\jdk1.5.0_04" vmargs\="-ea"/>\r\n</vmType>\r\n</vmSettings>\r\n

\!
/instance=
---------------------------
To reproduce:
1. Create a new workspace.
2. Import a preference file.
3. Open the preference window and view the JRE's

Expected:
To see one JRE "jdk1.5.0_04" and that JRE is configured with the default argument of "-ea".
