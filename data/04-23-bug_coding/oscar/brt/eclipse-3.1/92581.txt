BUG REPORT TITLE:
add exception dialog: catch up to removal of all types cache

BUG REPORT DESCRIPTION:
The all types cache has been removed from JDT and our "add exception breakpoint" dialog needs to catch up to the changes.
We were referencing internal code.
