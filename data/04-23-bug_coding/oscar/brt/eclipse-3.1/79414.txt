BUG REPORT TITLE:
states with same time stamps may appear in the wrong order

BUG REPORT DESCRIPTION:
i20041123

Using the local history UI, sometimes states that have the same apparent timestamp may be in the wrong order.
I could verify this with both new and existing implementation for the history store.
Need to investigate if it is us or Compare.
