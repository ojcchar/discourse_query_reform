BUG REPORT TITLE:
DropToFrameButton communicates with target in UI Thread

BUG REPORT DESCRIPTION:
Build 3.1 20050126

After upgrading to 20050125, I am quite often seeing hangs during debug process
(after hitting a breakpoint, some step action causes a freeze).
There doesn't seem to be a specific pattern.
Usually terminating/restarting would work.

org.eclipse.jdi.TimeoutException: Timeout occurred while waiting for packet 104515 at
org.eclipse.jdi.internal.connect.PacketReceiveManager.getReply(PacketReceiveManager.java:160)
at
org.eclipse.jdi.internal.connect.PacketReceiveManager.getReply(PacketReceiveManager.java:169)
        at org.eclipse.jdi.internal.MirrorImpl.requestVM(MirrorImpl.java:174)
        at org.eclipse.jdi.internal.MirrorImpl.requestVM(MirrorImpl.java:192)
at
org.eclipse.jdi.internal.ThreadReferenceImpl.frames(ThreadReferenceImpl.java:187)
at
org.eclipse.jdi.internal.ThreadReferenceImpl.frame(ThreadReferenceImpl.java:140)
at
org.eclipse.jdt.internal.debug.core.model.JDIThread.getUnderlyingFrame(JDIThread.java:575)
at org.eclipse.jdt.internal.debug.core.model.JDIStackFrame.getUnderlyingStackFrame(JDIStackFrame.java:
1000)
at
org.eclipse.jdt.internal.debug.core.model.JDIStackFrame.getUnderlyingMethod(JDIStackFrame.java:228)
at
org.eclipse.jdt.internal.debug.core.model.JDIStackFrame.isNative(JDIStackFrame.java:793)
at
org.eclipse.jdt.internal.debug.core.model.JDIStackFrame.supportsDropToFrame(JDIStackFrame.java:584)
at org.eclipse.jdt.internal.debug.ui.actions.DropToFrameButton.selectionChanged(DropToFrameButton.jav a:62)
at
org.eclipse.ui.internal.PluginAction.refreshEnablement(PluginAction.java:195)
at
org.eclipse.ui.internal.PluginAction.selectionChanged(PluginAction.java:267)
at
org.eclipse.ui.internal.PluginAction.selectionChanged(PluginAction.java:278)
        at org.eclipse.jface.viewers.Viewer$2.run(Viewer.java:163)
at
org.eclipse.core.internal.runtime.InternalPlatform.run(InternalPlatform.java:1051)
        at org.eclipse.core.runtime.Platform.run(Platform.java:747)
        at org.eclipse.jface.viewers.Viewer.fireSelectionChanged(Viewer.java:161)
at
org.eclipse.jface.viewers.StructuredViewer.updateSelection(StructuredViewer.java:1676)
at
org.eclipse.jface.viewers.StructuredViewer.setSelection(StructuredViewer.java:1358)
at
org.eclipse.debug.internal.ui.views.RemoteTreeViewer.deferSelection(RemoteTreeViewer.java:305)
at org.eclipse.debug.internal.ui.views.launch.LaunchViewEventHandler.doHandleSuspendThreadEvent(Laun chViewEventHandler.java:237)
at org.eclipse.debug.internal.ui.views.launch.LaunchViewEventHandler.doHandleSuspendEvent(LaunchView
EventHandler.java:204)
at org.eclipse.debug.internal.ui.views.launch.LaunchViewEventHandler.doHandleDebugEvents(LaunchView
EventHandler.java:123)
at org.eclipse.debug.internal.ui.views.AbstractDebugEventHandler$EventProcessingJob.runInUIThread(Abst ractDebugEventHandler.java:103)
        at org.eclipse.ui.progress.UIJob$1.run(UIJob.java:96)
        at org.eclipse.swt.widgets.RunnableLock.run(RunnableLock.java:35)
at
org.eclipse.swt.widgets.Synchronizer.runAsyncMessages(Synchronizer.java:118)
        at org.eclipse.swt.widgets.Display.runAsyncMessages(Display.java:2854)
        at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java:2518)
        at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1584)
        at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1550)
at
org.eclipse.ui.internal.Workbench.createAndRunWorkbench(Workbench.java:288)
        at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:144)
        at org.eclipse.ui.internal.ide.IDEApplication.run(IDEApplication.java:102)
at
org.eclipse.core.internal.runtime.PlatformActivator$1.run(PlatformActivator.java:225)
at
org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:274)
at
org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:129)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at
sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at
sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
        at java.lang.reflect.Method.invoke(Method.java:324)
        at org.eclipse.core.launcher.Main.basicRun(Main.java:255)
        at org.eclipse.core.launcher.Main.run(Main.java:811)
        at org.eclipse.core.launcher.Main.main(Main.java:795)
