BUG REPORT TITLE:
field names in org.eclipse.core.commands.operations not consistent

BUG REPORT DESCRIPTION:
(extracted from bug# 37716)

Doug Pollock says:
The "f" or "m_" or "c_" prefixes should not be used in code maintained by the
Platform/UI team.
I apologize that I did not notice this earlier when committing your patches.

Susan: could you please provide a patch changing the member variable names?
I agree it's low priority.
If you're going to be rushed coming into the API freeze (M6), then it can wait until after (M7).
