BUG REPORT TITLE:
Renaming a custom group collapses all of the groups

BUG REPORT DESCRIPTION:
Set to group by custom groups.
Expand all
If I rename one of the groups all of the groups are collapsed.
