BUG REPORT TITLE:
Nested interfaces aren't resolved correctly in import (with "Open Declaration" / F3)

BUG REPORT DESCRIPTION:
Using F3 on "Types" from the import statement "Test.java" leads to java.sql.Types.

Test.java
---------------------------------------- import test.Testable;
import test.Testable.Types; // F3 leads to java.sql.Types

public class Test {

public static void main( String[] args ) {
System.out.println(Testable.Types.TEST);
}
}
----------------------------------------

test/Testable.java
---------------------------------------- package test;

public interface Testable { public interface Types {
String TEST = "test";
}
}
----------------------------------------
