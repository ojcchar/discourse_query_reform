BUG REPORT TITLE:
Infinite "restore viewer state"

BUG REPORT DESCRIPTION:
Using the latest plugins from HEAD I ended up with an infinitely refreshing variables view.
In the progress view I saw the job "Restore Viewer State" running over and over.

I cannot make it happen on demand, but it happens periodically.
Selecting another stack fame made the problem stop.
