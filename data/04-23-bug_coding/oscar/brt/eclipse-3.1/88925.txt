BUG REPORT TITLE:
forced to pass -ws to start SDK

BUG REPORT DESCRIPTION:
i200503230842

With this build, it is not possible to start the SDK by double-clicking startup.jar because SWT classes are not found.
I am forced to run from the command line with -ws win32 to fix the problem.
The reason seems to be that if
-ws is not passed in, the SWT fragment does not get resolved (no missing requirements though).

Note that the error log for a failing startup seems to indicate we are properly computing the appropriate default value for the windowing system:

!
SESSION 2005-03-23 15:19:24.467 ----------------------------------------------- eclipse.buildId=I20050323-0842 java.version=1.5.0 java.vendor=Sun Microsystems Inc.
BootLoader constants: OS=win32, ARCH=x86, WS=win32, NL=en_CA
... dozens of failures
!
ENTRY org.eclipse.osgi 2005-03-23 15:19:24.499
!
MESSAGE Bundle update@/d:/eclipse/plugins/org.eclipse.swt.win32_3.1.0.jar [17] was not resolved.
