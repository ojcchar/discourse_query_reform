BUG REPORT TITLE:
new variables no longer revealed

BUG REPORT DESCRIPTION:
I20050209

When a new variable is added to the variables view (while stepping in a method), and there is no current selection, the new variable is not automatically selected (which was old behavior - see bug 9034).

Must be due to recent background processing changes.
