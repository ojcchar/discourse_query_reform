BUG REPORT TITLE:
[Decorators] [Navigator] (leak) Dangling references to ResourceNavigator

BUG REPORT DESCRIPTION:
To reproduce:

Run Eclipse with a memory profiler
Close all perspectives but the resource perspective
Close the resource navigator
Force a GC

Notice that there are still references to the navigator through the decorator manager.
