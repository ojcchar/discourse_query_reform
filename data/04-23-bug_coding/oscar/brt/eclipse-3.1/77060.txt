BUG REPORT TITLE:
Toggling show qualified names in the Debug view collapses thread

BUG REPORT DESCRIPTION:
I200410260800

Suspend at a breakpoint
Toggle showing qualified names

The thread collapses and I have to re-expand to show the stack frames.
