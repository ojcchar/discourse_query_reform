BUG REPORT TITLE:
Adapt to the "org.eclipse.core.commands" plugin changes

BUG REPORT DESCRIPTION:
IContextManager and friends have all been deprecated.
We need to move to the equivalent components in org.eclipse.core.commands
