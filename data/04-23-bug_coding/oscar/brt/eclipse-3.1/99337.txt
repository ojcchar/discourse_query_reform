BUG REPORT TITLE:
org.eclipse.osgi references itself in 3.0.1

BUG REPORT DESCRIPTION:
If you import the org.eclipse.osgi plugin from a 3.0.x build into your workspace, the plugin will not compile as its classpath container references itself.

This is because the 3.0.x org.eclipse.osgi plugin's Import-Package header lists packages from itself.

PDE does not check for such usage of Import-Package and hence osgi gets put on its own container.
