BUG REPORT TITLE:
Editor save does not write to disk (intermittent)

BUG REPORT DESCRIPTION:
Build: I20050906-1200

Several times while using this build, I have hit save in an editor but the buffer contents have not been flushed to disk.
When this happens, the file on disk has the same contents as the previous save, but the "Save" action is greyed out and the dirty indicator (*) is gone from the editor tab.
There are no errors in the log file or on the Java console.

The first couple of times this happened, I thought it was an error in the debugger because my breakpoints and stepping were not lining up with the correct code.
However, it just happened again, and I opened the same file in Wordpad to discover that the editor contents do not match the contents in Wordpad.
Each time this happened, dirtying the editor again and hitting save again would flush the correct contents to disk.

I have not been able to reproduce it consistently, but here is what I did the most recent time it happened.

1) Did a search and replace in a Java file (Ctrl+F, Replace All)
2) Hit save.
3) Discovered that I replaced with the wrong thing
4) Hit Undo (Ctrl+Z)
5) Did another search and replace with the correct value
6) Hit Save.

The editor now looks correct, but the file on disk still looks like it was in step 2).
I will attach a screen shot in case it helps clarify.
