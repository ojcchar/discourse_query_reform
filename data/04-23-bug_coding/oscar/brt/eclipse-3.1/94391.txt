BUG REPORT TITLE:
[Preferences] preference filter does not like compound names

BUG REPORT DESCRIPTION:
i200505092010

The preference page filter does not like compound names.
For instance, "Code
Assist" will show no hits, although there are two pages with this name.
