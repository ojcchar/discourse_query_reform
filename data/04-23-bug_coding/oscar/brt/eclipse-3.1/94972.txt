BUG REPORT TITLE:
Formatter preference page quickly shows error status on initial display

BUG REPORT DESCRIPTION:
N20050512-0010

Steps to reproduce:
- Make sure that last selected preference page is not the formatter preference page
- Open the preferences dialog
- Show the formatter preference page

-> For a very short time an error status is displayed
