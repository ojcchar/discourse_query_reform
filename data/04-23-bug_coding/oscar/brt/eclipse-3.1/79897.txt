BUG REPORT TITLE:
Debug view flashes after terminating target

BUG REPORT DESCRIPTION:
From bug 79450:

"3) Once, when terminating a debug session that was failing badly, the debug view flashed rapidly and the busy cursor flashed for about thirty seconds.
I took a stack dump (Ctrl+Break) during this period and it looks like
AbstractDebugEventHandler.handleDebugEvents is firing lots of UIJobs (two running at the time of stack dump)."
