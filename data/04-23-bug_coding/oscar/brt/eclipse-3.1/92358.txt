BUG REPORT TITLE:
"errors in project" dialog needs keyboard mnemonics

BUG REPORT DESCRIPTION:
The "Errors in Project" dialog that launches when attempting to run a project with compile-time errors has 2 buttons "OK" and "Cancel".

(Note that you may not be seeing this dialog anymore if you ticked the "Remember my decision" box)

It would be nice if they had keyboard shortcuts (mnemonics).
