BUG REPORT TITLE:
In debug mode, when hoovering over constants, their value should be showed

BUG REPORT DESCRIPTION:
When I'm debugging, I'd really like to see the value of primitive constants
(final  .
.)
when I hoover over its code.
fe.
When the debugger stops at this line :
if (k == MyConstants.TOTAL_COUNT) {

where MyConstants.TOTAL_COUNT is an integer, long, ...

I'd find it really handy if the hoover would simply show its value.
(in case of an object, a toString())
