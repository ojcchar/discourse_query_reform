BUG REPORT TITLE:
[Decorators] Remove DecoratorUpdateListener class from IDE plug-in

BUG REPORT DESCRIPTION:
DecoratorUpdateListener class should be removed as it is just experimental in 3.2

(see org.eclipse.ui.ide plugin)
