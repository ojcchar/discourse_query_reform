BUG REPORT TITLE:
[consistency] Button Selection fires before MouseUp

BUG REPORT DESCRIPTION:
- run the ControlExample, Button tab
- turn on listeners MouseUp and Selection
- click on an example Button
-> on OSX you'll get Selection - MouseUp
-> everywhere else you'll get MouseUp - Selection
