BUG REPORT TITLE:
Remove SWT launcher

BUG REPORT DESCRIPTION:
Based upon the request of the SWT team, the SWT launcher that first appeared in 3.1M5 should be removed from the SDK before the 3.1 release.

Soliciting a +1 from a component lead...
