BUG REPORT TITLE:
[Markers] Some error markers do not appear

BUG REPORT DESCRIPTION:
I20051018-0800, GTK+ 2.6.8, KDE 3.4.1, X.org 6.8.2, Linux 2.6.13
+ When I saw Bug 113454, I started up Eclipse and synchronized with HEAD.
Even with all the code from HEAD, no errors showed up in my Problems view.
+ So, I opened the file referenced in the first compile error
(ResourceMappingMarkersTest), and it had several errors in it.
+ I clicked on one of the methods that was an error
(problemView.getCurrentMarkers()), and hit "F3".
Now one error appears in the
Problems view: "ModelProvider cannot be resolved" in CompositeResourceMapping.
+ I tried cleaning all projects, to see if that would motivate the errors to appear.
The one error that was there previously then disappeared.
I had a hard time deciding whether to make this "blocker" or "major".
The big problem, as I see it, is that this can lead to broken builds (as we've seen).
So I marked it as a blocker.
