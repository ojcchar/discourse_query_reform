BUG REPORT TITLE:
GenerateGetterSetter does not work with trailing , in code style prefix list

BUG REPORT DESCRIPTION:
While editing my code style prefix list regarding generating getters and setters I appended a comma at the end.
The result was the generating getters and setters functionality didn't work at all.
After removing the comma it started working again.
I found this stacktrace, which will help pinpoint the problem.

-Georg

java.lang.ArrayIndexOutOfBoundsException: -1 at org.eclipse.jdt.core.NamingConventions.removePrefixAndSuffix
(NamingConventions.java:245)
at org.eclipse.jdt.core.NamingConventions.removePrefixAndSuffixForFieldName
(NamingConventions.java:364)
at org.eclipse.jdt.core.NamingConventions.suggestAccessorName
(NamingConventions.java:894)
at org.eclipse.jdt.core.NamingConventions.suggestGetterName
(NamingConventions.java:750)
at org.eclipse.jdt.core.NamingConventions.suggestGetterName
(NamingConventions.java:789)
at org.eclipse.jdt.internal.corext.codemanipulation.GetterSetterUtil.getGetterName
(GetterSetterUtil.java:50)
at org.eclipse.jdt.internal.corext.codemanipulation.GetterSetterUtil.getGetterName
(GetterSetterUtil.java:46)
at org.eclipse.jdt.internal.corext.codemanipulation.GetterSetterUtil.getGetter
(GetterSetterUtil.java:65)
at org.eclipse.jdt.ui.actions.AddGetterSetterAction.createGetterSetterMapping
(AddGetterSetterAction.java:716)
at org.eclipse.jdt.ui.actions.AddGetterSetterAction.run
(AddGetterSetterAction.java:259)
at org.eclipse.jdt.ui.actions.AddGetterSetterAction.run
(AddGetterSetterAction.java:513)
at org.eclipse.jdt.ui.actions.SelectionDispatchAction.dispatchRun
(SelectionDispatchAction.java:216)
at org.eclipse.jdt.ui.actions.SelectionDispatchAction.run
(SelectionDispatchAction.java:188)
	at org.eclipse.jface.action.Action.runWithEvent(Action.java:996)
at org.eclipse.ui.actions.RetargetAction.runWithEvent
(RetargetAction.java:216)
at org.eclipse.ui.internal.WWinPluginAction.runWithEvent
(WWinPluginAction.java:225)
at org.eclipse.jface.action.ActionContributionItem.handleWidgetSelection
(ActionContributionItem.java:538)
at org.eclipse.jface.action.ActionContributionItem.access$2
(ActionContributionItem.java:488)
at org.eclipse.jface.action.ActionContributionItem$5.
handleEvent
(ActionContributionItem.java:400)
	at org.eclipse.swt.widgets.EventTable.sendEvent(EventTable.java:82)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:842)
	at org.eclipse.swt.widgets.Display.runDeferredEvents(Display.java:2894)
	at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java:2527)
	at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1570)
	at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1534)
at org.eclipse.ui.internal.Workbench.createAndRunWorkbench
(Workbench.java:306)
	at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:143)
at org.eclipse.ui.internal.ide.IDEApplication.run
(IDEApplication.java:103)
at org.eclipse.core.internal.runtime.PlatformActivator$1.
run
(PlatformActivator.java:228)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run
(EclipseStarter.java:344)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run
(EclipseStarter.java:156)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.eclipse.core.launcher.Main.invokeFramework(Main.java:315)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:268)
	at org.eclipse.core.launcher.Main.run(Main.java:942)
	at org.eclipse.core.launcher.Main.main(Main.java:926)
