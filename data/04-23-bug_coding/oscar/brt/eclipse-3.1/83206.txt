BUG REPORT TITLE:
ICodeAssist#codeSelect(..) on implicit methods should not return a java element

BUG REPORT DESCRIPTION:
I20050118-1015

class User { enum Color {RED, GREEN, BLUE} void x() {
Color.valueOf("RED");
Color.values();
}
}

ICodeAssist#codeSelect(.
.)
on implicit methods 'valueOf(String)' and 'values()' of enum Color should not return a java element.
Currently, the declaring enum is returned.
