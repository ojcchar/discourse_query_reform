BUG REPORT TITLE:
[content assist] Code assist in import statements insert at the end

BUG REPORT DESCRIPTION:
Build: I-20050201

Open any Java file that contains > 1 import statements.

Let's say the first import statement reads:
import org.eclipse.core.runtime.
*;

delete the '*;' from the end and try to use code assist to insert
IRunnableWithProgress for example.

You will see that upon pressing Enter to select, the text gets inserted several lines down under all the import statements.
the cursor is now in a random position also.
