BUG REPORT TITLE:
Installed JRE should indicate if it is 5.0 or not

BUG REPORT DESCRIPTION:
To support changing JRE to a 5.0 JRE as soon as 5.0 features are used in code we need to know which JREs are 5.0.
