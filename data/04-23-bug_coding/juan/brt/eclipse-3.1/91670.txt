BUG REPORT TITLE:
package-info.java is not being correctly compiled

BUG REPORT DESCRIPTION:
When reading annotations from a package, the following error is thrown:

Exception in thread "main" java.lang.ClassFormatError: Illegal class modifiers in class testcase/package-info: 0x1600
	at java.lang.ClassLoader.defineClass1(Native Method)
	at java.lang.ClassLoader.defineClass(ClassLoader.java:620)
	at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:124)
	at java.net.URLClassLoader.defineClass(URLClassLoader.java:260)
	at java.net.URLClassLoader.access$100(URLClassLoader.java:56)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:195)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:188)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:306)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:268)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:251)
	at java.lang.ClassLoader.loadClassInternal(ClassLoader.java:319)
	at java.lang.Class.forName0(Native Method)
	at java.lang.Class.forName(Class.java:242)
	at java.lang.Package.getPackageInfo(Package.java:350)
	at java.lang.Package.getAnnotation(Package.java:361)
	at testcase.Main.main(Main.java:9)

This only happens when eclipse (3.1m6) compiles package-info.java; if I use javac to compile it, reading package annotations works fine.
