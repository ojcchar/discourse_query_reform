BUG REPORT TITLE:
NullPointerException after invoking extract method

BUG REPORT DESCRIPTION:
3.1RC4 (Build id: I20050624-1300)

When trying to extract a method I get the error:
"An unexpected exception occurred during condition checking."

Unfortunately I don't have a reproducible test case; the exception happens when trying to extract a method out of one particular area of my code but not in other similar areas.

from the error log:

java.lang.reflect.InvocationTargetException
	at org.eclipse.jface.operation.ModalContext.run(ModalContext.java:327)
at
org.eclipse.jface.dialogs.ProgressMonitorDialog.run(ProgressMonitorDialog.java:447)
at
org.eclipse.ui.internal.progress.ProgressMonitorJobsDialog.run(ProgressMonitorJobsDialog.java:261)
	at org.eclipse.ui.internal.progress.ProgressManager$3.run(ProgressManager.java:861)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
at
org.eclipse.ui.internal.progress.ProgressManager.busyCursorWhile(ProgressManager.java:895)
at
org.eclipse.ui.internal.progress.ProgressManager.busyCursorWhile(ProgressManager.java:871)
at
org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation.checkInitialConditions(RefactoringWizardOpenOperation.java:150)
at
org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation.access$0(RefactoringWizardOpenOperation.java:146)
at
org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation$1.run(RefactoringWizardOpenOperation.java:116)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
at
org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation.run(RefactoringWizardOpenOperation.java:138)
at
org.eclipse.jdt.internal.ui.refactoring.actions.RefactoringStarter.activate(RefactoringStarter.java:40)
	at org.eclipse.jdt.ui.actions.ExtractMethodAction.run(ExtractMethodAction.java:84)
at
org.eclipse.jdt.ui.actions.SelectionDispatchAction.dispatchRun(SelectionDispatchAction.java:226)
at
org.eclipse.jdt.ui.actions.SelectionDispatchAction.run(SelectionDispatchAction.java:198)
	at org.eclipse.jface.action.Action.runWithEvent(Action.java:996)
	at org.eclipse.ui.commands.ActionHandler.execute(ActionHandler.java:182)
at
org.eclipse.ui.internal.handlers.LegacyHandlerWrapper.execute(LegacyHandlerWrapper.java:108)
	at org.eclipse.core.commands.Command.execute(Command.java:311)
at
org.eclipse.core.commands.ParameterizedCommand.execute(ParameterizedCommand.java:396)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.executeCommand(WorkbenchKeyboard.java:459)
	at org.eclipse.ui.internal.keys.WorkbenchKeyboard.press(WorkbenchKeyboard.java:781)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.processKeyEvent(WorkbenchKeyboard.java:828)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.filterKeySequenceBindings(WorkbenchKeyboard.java:550)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.access$3(WorkbenchKeyboard.java:493)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard$KeyDownFilter.handleEvent(WorkbenchKeyboard.java:117)
	at org.eclipse.swt.widgets.EventTable.sendEvent(EventTable.java:66)
	at org.eclipse.swt.widgets.Display.filterEvent(Display.java:917)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:842)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:867)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:852)
	at org.eclipse.swt.widgets.Widget.sendKeyEvent(Widget.java:880)
	at org.eclipse.swt.widgets.Widget.sendKeyEvent(Widget.java:876)
	at org.eclipse.swt.widgets.Widget.wmKeyDown(Widget.java:1475)
at org.eclipse.swt.widgets.Control.WM_KEYDOWN(Control.java:3348)
	at org.eclipse.swt.widgets.Control.windowProc(Control.java:3067)
	at org.eclipse.swt.widgets.Display.windowProc(Display.java:3706)
	at org.eclipse.swt.internal.win32.OS.DispatchMessageW(Native Method)
	at org.eclipse.swt.internal.win32.OS.DispatchMessage(OS.java:1656)
	at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java:2711)
	at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1699)
	at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1663)
	at org.eclipse.ui.internal.Workbench.createAndRunWorkbench(Workbench.java:367)
	at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:143)
	at org.eclipse.ui.internal.ide.IDEApplication.run(IDEApplication.java:103)
at
org.eclipse.core.internal.runtime.PlatformActivator$1.run(PlatformActivator.java:226)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:376)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:163)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at
sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:585)
	at org.eclipse.core.launcher.Main.invokeFramework(Main.java:334)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:278)
	at org.eclipse.core.launcher.Main.run(Main.java:973)
	at org.eclipse.core.launcher.Main.main(Main.java:948)
Caused by: java.lang.NullPointerException
	at org.eclipse.jdt.internal.corext.dom.Bindings.equals(Bindings.java:88)
at
org.eclipse.jdt.internal.corext.refactoring.code.SnippetFinder$Matcher.match(SnippetFinder.java:107)
	at org.eclipse.jdt.core.dom.SimpleName.subtreeMatch0(SimpleName.java:140)
	at org.eclipse.jdt.core.dom.ASTNode.subtreeMatch(ASTNode.java:2315)
	at org.eclipse.jdt.core.dom.ASTMatcher.safeSubtreeMatch(ASTMatcher.java:138)
	at org.eclipse.jdt.core.dom.ASTMatcher.match(ASTMatcher.java:1386)
at
org.eclipse.jdt.core.dom.MethodInvocation.subtreeMatch0(MethodInvocation.java:229)
	at org.eclipse.jdt.core.dom.ASTNode.subtreeMatch(ASTNode.java:2315)
at
org.eclipse.jdt.internal.corext.refactoring.code.SnippetFinder.matches(SnippetFinder.java:204)
at
org.eclipse.jdt.internal.corext.refactoring.code.SnippetFinder.visitNode(SnippetFinder.java:191)
at
org.eclipse.jdt.internal.corext.dom.GenericVisitor.visit(GenericVisitor.java:142)
	at org.eclipse.jdt.core.dom.MethodInvocation.accept0(MethodInvocation.java:236)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChild(ASTNode.java:2497)
at
org.eclipse.jdt.core.dom.VariableDeclarationFragment.accept0(VariableDeclarationFragment.java:224)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChildren(ASTNode.java:2520)
at
org.eclipse.jdt.core.dom.VariableDeclarationStatement.accept0(VariableDeclarationStatement.java:272)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChildren(ASTNode.java:2520)
	at org.eclipse.jdt.core.dom.Block.accept0(Block.java:135)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChild(ASTNode.java:2497)
	at org.eclipse.jdt.core.dom.IfStatement.accept0(IfStatement.java:189)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChildren(ASTNode.java:2520)
	at org.eclipse.jdt.core.dom.Block.accept0(Block.java:135)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChild(ASTNode.java:2497)
	at org.eclipse.jdt.core.dom.IfStatement.accept0(IfStatement.java:189)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChildren(ASTNode.java:2520)
	at org.eclipse.jdt.core.dom.Block.accept0(Block.java:135)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChild(ASTNode.java:2497)
	at org.eclipse.jdt.core.dom.MethodDeclaration.accept0(MethodDeclaration.java:501)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
	at org.eclipse.jdt.core.dom.ASTNode.acceptChildren(ASTNode.java:2520)
	at org.eclipse.jdt.core.dom.TypeDeclaration.accept0(TypeDeclaration.java:483)
	at org.eclipse.jdt.core.dom.ASTNode.accept(ASTNode.java:2450)
at
org.eclipse.jdt.internal.corext.refactoring.code.SnippetFinder.perform(SnippetFinder.java:138)
at
org.eclipse.jdt.internal.corext.refactoring.code.ExtractMethodRefactoring.initializeDuplicates(ExtractMethodRefactoring.java:581)
at
org.eclipse.jdt.internal.corext.refactoring.code.ExtractMethodRefactoring.checkInitialConditions(ExtractMethodRefactoring.java:247)
at
org.eclipse.ltk.core.refactoring.CheckConditionsOperation.run(CheckConditionsOperation.java:82)
	at org.eclipse.core.internal.resources.Workspace.run(Workspace.java:1719)
at
org.eclipse.ltk.internal.ui.refactoring.WorkbenchRunnableAdapter.run(WorkbenchRunnableAdapter.java:86)
at
org.eclipse.jface.operation.ModalContext$ModalContextThread.run(ModalContext.java:113)
