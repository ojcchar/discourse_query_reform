BUG REPORT TITLE:
[1.5] Add support for package-info.java

BUG REPORT DESCRIPTION:
The compiler should support a file called package-info.java.
This file replaces the package.html file.
If it is present, it should be used by a documentation generation tool.
See for details:
http://www.onjava.com/pub/a/onjava/2004/04/21/declarative.html?page=last
