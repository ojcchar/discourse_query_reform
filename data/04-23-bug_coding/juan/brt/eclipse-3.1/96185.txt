BUG REPORT TITLE:
[implementation] Dangerous Eclipse behavior on file save when out of disk space

BUG REPORT DESCRIPTION:
I ran out of disk space while editing files in Eclipse.
I saved all the files I was working on, and no error was reported.
However I started getting errors that Eclipse couldn't save project metadata.
The error kept popping up, and
(because I still didn't know I was out of disk space) I just assumed Eclipse was in a weird state.
I closed Eclipse and re-opened it.
Some of my files were empty or truncated, and I was still getting the problems with metadata save errors.
I checked the logs and found the "No space left on device" errors.
Unfortunately, by this time I had lost a large amount of work, because the files
I had saved were either empty or truncated.

So the problems are:
- Saving reported no warning or error when there was no space left, but simply truncated the file (sometimes at byte 0).
- The errors in saving metadata gave no information about the underlying exception (No space left on device).

Here are a couple of applicable stack traces:

!
ENTRY org.eclipse.core.resources 4 568 2005-05-20 14:28:30.91
!
MESSAGE Could not write metadata for:
/home/lhutchis/workspace/.
metadata/.
plugins/org.
eclipse.core.resources/.
projects/CORE/.
indexes/b9/7e/17/history.
index.
!
STACK 0 java.io.IOException: No space left on device
        at java.io.FileOutputStream.close0(Native Method)
        at java.io.FileOutputStream.close(FileOutputStream.java:279)
        at java.io.FilterOutputStream.close(FilterOutputStream.java:143)
        at java.io.FilterOutputStream.close(FilterOutputStream.java:143)
        at org.eclipse.core.internal.localstore.Bucket.save(Bucket.java:335)
at
org.eclipse.core.internal.localstore.HistoryStore2.addState(HistoryStore2.java:90)
at
org.eclipse.core.internal.localstore.FileSystemResourceManager.write(FileSystemResourceManager.java:783)
at
org.eclipse.core.internal.resources.File.internalSetContents(File.java:326)
        at org.eclipse.core.internal.resources.File.setContents(File.java:370)
        at org.eclipse.core.internal.resources.File.setContents(File.java:470)
at
org.eclipse.core.internal.filebuffers.ResourceTextFileBuffer.commitFileBufferContent(ResourceTextFileBuffer.java:315)
at
org.eclipse.core.internal.filebuffers.ResourceFileBuffer.commit(ResourceFileBuffer.java:317)
at
org.eclipse.jdt.internal.ui.javaeditor.DocumentAdapter.save(DocumentAdapter.java:356)
at
org.eclipse.jdt.internal.core.CommitWorkingCopyOperation.executeOperation(CommitWorkingCopyOperation.java:113)
at
org.eclipse.jdt.internal.core.JavaModelOperation.run(JavaModelOperation.java:710)
        at org.eclipse.core.internal.resources.Workspace.run(Workspace.java:1714)
at
org.eclipse.jdt.internal.core.JavaModelOperation.runOperation(JavaModelOperation.java:771)
at
org.eclipse.jdt.internal.core.CompilationUnit.commitWorkingCopy(CompilationUnit.java:327)
at
org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitDocumentProvider.commitWorkingCopy(CompilationUnitDocumentProvider.java:953)
at
org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitDocumentProvider$2.execute(CompilationUnitDocumentProvider.java:1012)
at
org.eclipse.ui.editors.text.TextFileDocumentProvider$DocumentProviderOperation.run(TextFileDocumentProvider.java:126)
at
org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation.execute(WorkspaceModifyDelegatingOperation.java:68)
at
org.eclipse.ui.actions.WorkspaceModifyOperation$1.run(WorkspaceModifyOperation.java:98)
        at org.eclipse.core.internal.resources.Workspace.run(Workspace.java:1714)
at
org.eclipse.ui.actions.WorkspaceModifyOperation.run(WorkspaceModifyOperation.java:110)
at
org.eclipse.ui.internal.editors.text.WorkspaceOperationRunner.run(WorkspaceOperationRunner.java:73)
at
org.eclipse.ui.internal.editors.text.WorkspaceOperationRunner.run(WorkspaceOperationRunner.java:63)
at
org.eclipse.ui.editors.text.TextFileDocumentProvider.executeOperation(TextFileDocumentProvider.java:441)
at
org.eclipse.ui.editors.text.TextFileDocumentProvider.saveDocument(TextFileDocumentProvider.java:700)
at
org.eclipse.ui.texteditor.AbstractTextEditor.performSave(AbstractTextEditor.java:3675)
at
org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor.performSave(CompilationUnitEditor.java:1361)
at org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor.doSave(Compilation
!
ENTRY org.eclipse.ui.ide 4 4 2005-05-20 14:48:22.305
!
MESSAGE Problems saving workspace

!
ENTRY org.eclipse.ui.ide 4 1 2005-05-20 14:48:22.346
!
MESSAGE Problems occurred while trying to save the state of the workbench.
!
SUBENTRY 1 org.eclipse.core.resources 4 568 2005-05-20 14:48:22.346
!
MESSAGE Could not write workspace metadata:
/home/lhutchis/workspace/.
metadata/.
plugins/org.
eclipse.core.resources/.
root/52.
tree.
!
STACK 0 java.io.IOException: No space left on device
        at java.io.FileOutputStream.close0(Native Method)
        at java.io.FileOutputStream.close(FileOutputStream.java:279)
        at java.io.FilterOutputStream.close(FilterOutputStream.java:143)
at
org.eclipse.core.internal.localstore.SafeFileOutputStream.close(SafeFileOutputStream.java:58)
        at java.io.FilterOutputStream.close(FilterOutputStream.java:143)
at
org.eclipse.core.internal.resources.SaveManager.saveTree(SaveManager.java:1104)
at
org.eclipse.core.internal.resources.SaveManager.save(SaveManager.java:944)
        at org.eclipse.core.internal.resources.Workspace.save(Workspace.java:1748)
at
org.eclipse.ui.internal.ide.IDEWorkbenchAdvisor$2.run(IDEWorkbenchAdvisor.java:280)
at
org.eclipse.jface.operation.ModalContext$ModalContextThread.run(ModalContext.java:113)
