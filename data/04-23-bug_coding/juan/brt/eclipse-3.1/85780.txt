BUG REPORT TITLE:
SWT support "hidden" widgets

BUG REPORT DESCRIPTION:
As of M5:

Remove widgets from Layout
RowData and GridData have a new exclude attribute for removing the associated widget from the layout management.
Previously, applications had to dispose and recreate widgets to achieve this effect.

http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.swt.snippets/src/org/eclip se/swt/snippets/Snippet175.java? rev=HEAD&content-type=text/vnd.
viewcvs-markup

I know this can help in org.eclipse.jdt.internal.debug.ui.EditLogicalStructureDialog.
Are there others?
