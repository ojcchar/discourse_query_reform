BUG REPORT TITLE:
[5.0][content assist] Content assist popup disappears while completing the statically imported method name

BUG REPORT DESCRIPTION:
I20050513-0010

Steps to reproduce:
- Create a new Class "Foo"
- Type "import static java.lang.Math."
- Press Ctrl+Space
- Type "a"

-> Instead of constraining the proposals to all members with prefix a, the popup closes
