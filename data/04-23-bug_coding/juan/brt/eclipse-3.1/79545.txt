BUG REPORT TITLE:
Eclipse vs Sun JDK: different class files from the same source code

BUG REPORT DESCRIPTION:
Compile the source code below in Eclipse and run it.
Do the same using Sun JDK (1.4.1 or 1.5).
The results are different.
The problem is connected with +' ': Eclipse treats it as ' ' but Sun JDK converts that space into 32 (+' ' => + (int) ' ' => +32 => 32) (which IMHO is correct).

PS.
I'm not sure if I picked the proper product (JDT)...

The source code:

public class CharIntTest
{
/**
* Eclipse value: " "
* JDK value:     "32"
*/ public static String C = "" + +' ';
/**
* Eclipse value: "32"
* JDK value:     "32"
*/ public static String I = "" + +32;

public static void main(String[] args)
{
System.out.println(C);
System.out.println(I);
}
}
