BUG REPORT TITLE:
"Time to load message bundle" printed during start-up

BUG REPORT DESCRIPTION:
N20050228-0010

Even with no debugging options turned on, there are the messages similar to the following printed to stdout on start-up:

Time to load message bundle: org.eclipse.core.internal.utils.messages was 20ms.
Time to load message bundle: org.eclipse.ui.internal.messages was 57ms.
Time to load message bundle: org.eclipse.ui.internal.intro.intro was 15ms.
Time to load message bundle: org.eclipse.ui.internal.progress.messages was 18ms.
