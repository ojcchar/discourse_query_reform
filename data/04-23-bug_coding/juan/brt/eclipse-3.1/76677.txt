BUG REPORT TITLE:
Console Input incorrect

BUG REPORT DESCRIPTION:
1. Run the following program

public class ConsoleTest { public static void main(String[] args) { try { byte[] b = new byte[100];
for(;;) { int read = System.in.read(b);
System.out.write(b, 0, read);
}
} catch (Exception e) { e.printStackTrace();
}
}
}

2. Enter '123' in the console
3. replace '2' with 'x'
4. hit 'enter'

console output is 13x instead of 1x3.
