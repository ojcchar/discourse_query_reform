BUG REPORT TITLE:
Clicking on x on performance page opens details with no errors

BUG REPORT DESCRIPTION:
Take a look at:
http://fullmoon.rtp.raleigh.ibm.com/downloads/drops/M-3.0.2RC2-200502161722/performance/org.eclipse.jdt.text.php?

Scroll down to performance.OpenJavaEditorStressTest#testOpenJavaEditor1()"
Observe: red x for RHEL 3.0 Sun 1.4.2_06.

Click on the red x ==> details show up all green.

Looks like a bug regarding the handling of negative numbers.
