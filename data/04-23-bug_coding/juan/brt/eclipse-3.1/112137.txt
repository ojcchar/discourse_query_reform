BUG REPORT TITLE:
ConcurrentModificationException when CTRL+LeftClick on constructor_ call in Java editor

BUG REPORT DESCRIPTION:
While coding in Java, I CTRL+clicked on an allocation call to InetSocketAddress, in order to open the sources and read a bit.
However, the call failed, and below you'll find what was printed in the log.

After closing the failed window, I did it again and it worked.
There were no background tasks (AFAIK), and I had only a single window open.
The project type
(if relevant) is a regular Java project.

I didn't find another bug about this, and even though I have no idea how to reproduce it, hopefully someone familiar with the sources will be able to get something out of the stack trace below.

eclipse.buildId=I20050627-1435 java.version=1.5.0_05 java.vendor=Sun Microsystems Inc.
BootLoader constants: OS=linux, ARCH=x86, WS=gtk, NL=en_US
Command-line arguments:  -os linux -ws gtk -arch x86

!
ENTRY org.eclipse.ui 4 0 2005-10-10 14:51:33.131
!
MESSAGE Unable to create editor ID org.eclipse.jdt.ui.ClassFileEditor: Editor could not be initialized.
!
STACK 0 java.util.ConcurrentModificationException at java.util.AbstractList$Itr.checkForComodification(AbstractList.java:
449)
        at java.util.AbstractList$Itr.next(AbstractList.java:420)
at org.eclipse.jdt.internal.core.SourceMapper.findSource(SourceMapper.java:830)
at org.eclipse.jdt.internal.core.SourceMapper.findSource(SourceMapper.java:796)
        at org.eclipse.jdt.internal.core.ClassFile.mapSource(ClassFile.java:628)
at org.eclipse.jdt.internal.core.ClassFile.openBuffer(ClassFile.java:
540)
        at org.eclipse.jdt.internal.core.Openable.getBuffer(Openable.java:263)
        at org.eclipse.jdt.internal.core.ClassFile.getBuffer(ClassFile.java:263)
at org.eclipse.jdt.internal.core.ClassFile.getSourceRange(ClassFile.java:428)
at org.eclipse.jdt.internal.ui.javaeditor.ClassFileEditor.
probeInputForSource(ClassFileEditor.java:688)
at org.eclipse.jdt.internal.ui.javaeditor.ClassFileEditor.
doSetInput(ClassFileEditor.java:617)
at org.eclipse.ui.texteditor.AbstractTextEditor$16.
run(AbstractTextEditor.java:2360)
at org.eclipse.jface.operation.ModalContext.
runInCurrentThread(ModalContext.java:346)
        at org.eclipse.jface.operation.ModalContext.run(ModalContext.java:291)
at org.eclipse.jface.window.ApplicationWindow$1.
run(ApplicationWindow.java:624)
        at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
at org.eclipse.jface.window.ApplicationWindow.run(ApplicationWindow.java:621)
at org.eclipse.ui.internal.WorkbenchWindow.run(WorkbenchWindow.java:
2134)
at org.eclipse.ui.texteditor.AbstractTextEditor.
internalInit(AbstractTextEditor.java:2378)
at org.eclipse.ui.texteditor.AbstractTextEditor.init(AbstractTextEditor.java:2405)
at org.eclipse.ui.internal.EditorManager.createSite(EditorManager.java:
773)
at org.eclipse.ui.internal.EditorReference.
createPartHelper(EditorReference.java:572)
at org.eclipse.ui.internal.EditorReference.createPart(EditorReference.java:365)
at org.eclipse.ui.internal.WorkbenchPartReference.
getPart(WorkbenchPartReference.java:552)
at org.eclipse.ui.internal.EditorReference.getEditor(EditorReference.java:214)
at org.eclipse.ui.internal.WorkbenchPage.
busyOpenEditorBatched(WorkbenchPage.java:2325)
at org.eclipse.ui.internal.WorkbenchPage.busyOpenEditor(WorkbenchPage.java:2258)
at org.eclipse.ui.internal.WorkbenchPage.access$9(WorkbenchPage.java:
2250)
        at org.eclipse.ui.internal.WorkbenchPage$9.run(WorkbenchPage.java:2236)
        at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
at org.eclipse.ui.internal.WorkbenchPage.openEditor(WorkbenchPage.java:
2231)
at org.eclipse.ui.internal.WorkbenchPage.openEditor(WorkbenchPage.java:
2212)
at org.eclipse.jdt.internal.ui.javaeditor.EditorUtility.
openInEditor(EditorUtility.java:263)
at org.eclipse.jdt.internal.ui.javaeditor.EditorUtility.
openInEditor(EditorUtility.java:140)
at org.eclipse.jdt.internal.ui.actions.OpenActionUtil.
open(OpenActionUtil.java:49)
        at org.eclipse.jdt.ui.actions.OpenAction.run(OpenAction.java:169)
        at org.eclipse.jdt.ui.actions.OpenAction.run(OpenAction.java:141)
at org.eclipse.jdt.ui.actions.SelectionDispatchAction.
dispatchRun(SelectionDispatchAction.java:226)
at org.eclipse.jdt.ui.actions.SelectionDispatchAction.
run(SelectionDispatchAction.java:198)
at org.eclipse.jdt.internal.ui.javaeditor.JavaElementHyperlink.
open(JavaElementHyperlink.java:55)
at org.eclipse.jface.text.hyperlink.HyperlinkManager.
mouseUp(HyperlinkManager.java:388)
at org.eclipse.swt.widgets.TypedListener.handleEvent(TypedListener.java:
137)
        at org.eclipse.swt.widgets.EventTable.sendEvent(EventTable.java:66)
        at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:1021)
        at org.eclipse.swt.widgets.Display.runDeferredEvents(Display.java:2867)
        at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java:2572)
        at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1699)
        at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1663)
at org.eclipse.ui.internal.Workbench.createAndRunWorkbench(Workbench.java:367)
        at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:143)
at org.eclipse.ui.internal.ide.IDEApplication.run(IDEApplication.java:
103)
at org.eclipse.core.internal.runtime.PlatformActivator$1.
run(PlatformActivator.java:226)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:376)
at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:163)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.
invoke(DelegatingMethodAccessorImpl.java:25)
        at java.lang.reflect.Method.invoke(Method.java:585)
        at org.eclipse.core.launcher.Main.invokeFramework(Main.java:334)
        at org.eclipse.core.launcher.Main.basicRun(Main.java:278)
        at org.eclipse.core.launcher.Main.run(Main.java:973)
        at org.eclipse.core.launcher.Main.main(Main.java:948)
