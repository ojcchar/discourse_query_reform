BUG REPORT TITLE:
[1.5][codeassist] missing proposals for wildcard capture

BUG REPORT DESCRIPTION:
Build N20050525

import java.util.List;
public class X<U, V extends List<U>> {
V v;
void foo(X<String, ?> x1, X<Object, ?> x2) {
x1.v.
|<-----CODEASSIST HERE: should offer List#get(...)
