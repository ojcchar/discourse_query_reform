BUG REPORT TITLE:
Displayed tab width is showing incorrectly

BUG REPORT DESCRIPTION:
Version: 3.1.0
Build id: I20050201-0800

The Text Editor preferences always seem to show the displayed tab width as (x -
1).
For example if i set the width to 4 and restart it will show in the preference page as 3 but tabs will be 4 spaces in the editor
