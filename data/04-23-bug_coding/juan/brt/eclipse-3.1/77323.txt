BUG REPORT TITLE:
[JFace] Deadlock in ImageCache on shutdown

BUG REPORT DESCRIPTION:
I200410260800 and UI from head.
Hyper-threading machine, running the performance test suite.
The reference cleaner thread is trying to syncExec a disposal, but the UI thread is blocked waiting for access to the ImageCache.
A deadlock occurs.
