BUG REPORT TITLE:
[plan] API to extract the Javadoc on org.eclipse.jdt.core.IMember

BUG REPORT DESCRIPTION:
I20050920

A new API to get the Javadoc for a Java element is needed.
This API will extract the Javadoc from the IBuffer of the Java element (underlying compilation unit, or attached source for a class file).
