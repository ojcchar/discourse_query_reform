BUG REPORT TITLE:
DEL works on non-editable editor

BUG REPORT DESCRIPTION:
Build: 3.2 RC5

1. Open Plug-ins View
2. Double-click on any plug-in with a blue/purple icon (e.g. org.eclipse.core.runtime.compatibility.auth.
A plug-in editor will open.
3. On the Overview page, select an item in the Execution Environments table.
Press the DEL key.

Observe how the selected item gets deleted.

This should not happen.
DEL key should do nothing on such a non-editable model.
