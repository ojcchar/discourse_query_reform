BUG REPORT TITLE:
Reuse in Ant Build Loggers

BUG REPORT DESCRIPTION:
Currently there is code duplication in the support provided by the same JRE
Ant loggers and the separate JRE loggers.
