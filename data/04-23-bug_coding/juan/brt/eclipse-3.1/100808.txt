BUG REPORT TITLE:
[assist] Wrong replace range for package proposals if there is no line termination

BUG REPORT DESCRIPTION:
3.1-RC2 test pass

See bug 100795 for details.
- have an empty .java file with the contents below (no line termination)
- invoke code assist at the end of the first line

"package org."

The reported completion proposals specify a replace range of [8, 13), while the document length is only 12.

Note that if there is a trailing space or a newline, the correct replace range is reported: [8, 12)

Since jdt-ui does not check the replace ranges, we run into the IAE of
StyledText eventually (see bug 100795 for the trace).
