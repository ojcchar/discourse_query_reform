BUG REPORT TITLE:
Create Patch references stale information

BUG REPORT DESCRIPTION:
N20050519-0010

When I try to create a patch for jdt.ui I always get a dialog that the resource
AddToClasspathOperation.java doesn't exist and that I should perform a refresh from local.
Doing so that not solve the problem.

It is indeed correct that AddToClasspathOperation.java doesn't exist anymore since  the class got delete a while ago.
If I add it again create patch complains about another class which got delete a while ago.

Martin is seeing the same problem with the serialsupport.jar file.

Setting severity to critical since the behaviour prevents me from creating any patches.
