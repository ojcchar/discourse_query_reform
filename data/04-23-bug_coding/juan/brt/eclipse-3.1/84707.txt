BUG REPORT TITLE:
[Preferences] search doesn't match with spaces.

BUG REPORT DESCRIPTION:
We now search based on word rather than whole phrase for the preference dialog.

But that has broken the case where the word that you type in the search dialog contains a space.
For instance: "code tem" doesn't match "Code Templates".
