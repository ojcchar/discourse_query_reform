BUG REPORT TITLE:
Update the core resources Ant task generation

BUG REPORT DESCRIPTION:
The org.eclipse.core.resources building of the ant task JAR does not make use of the features provided in Eclipse to facilitate generation and self-hosting testing of the tasks created.

And...even breaks some of the rules for contributed ant tasks: ant task class files should not be included in the plugin runtime jar.

The buildfile is also slightly out of date
