BUG REPORT TITLE:
view contents missing when switching perspectives

BUG REPORT DESCRIPTION:
N20050725

- start eclipse on a fresh workspace
-> opens in Java perspective
- create a java project and a class Foo.java
-> everything as expected
- switch to the debug perspective

-> outline view has no contents
-> debug view is empty
-> variables view is empty

Observations:
- closing and reopening fixes outline and debug view
- variables view does not re-appear when closing and reopening
- similar effects when switching to other perspectives (resource ...)
