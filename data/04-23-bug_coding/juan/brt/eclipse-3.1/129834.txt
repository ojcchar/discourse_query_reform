BUG REPORT TITLE:
Glitches in feature import wizard

BUG REPORT DESCRIPTION:
1. Not enough vertical spacing between rows

2. Combo box is not wide enough.
The Reload button should probably be pushed down next to the table viewer.
It would become the first button in that table viewer composite.

3. Progress monitor is needed when parsing/reloading features from a new location

4. If you enter a location in the combo box, check the top checkbox and then uncheck it, you lose the location you had entered.
