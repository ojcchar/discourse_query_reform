BUG REPORT TITLE:
[Markers] Error description column in Problems is empty

BUG REPORT DESCRIPTION:
Version 3.1.0, I20050126-0800 java version "1.5.0_01"
Java(TM) 2 Runtime Environment, Standard Edition (build 1.5.0_01-b08)
Java HotSpot(TM) Client VM (build 1.5.0_01-b08, mixed mode, sharing)

The Description column in the Problems tab is now empty and does not present any error message.
No description is either provided by showing the property of an error.

Workaround: Double-click on the error and put the mouse pointer over the error/warning icon or the underlined text in the editor window will provide the error message.

Eclipse 3.1M4 does not have this problem.
