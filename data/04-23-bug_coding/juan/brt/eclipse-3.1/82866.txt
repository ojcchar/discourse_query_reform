BUG REPORT TITLE:
[Breakpoint Groups] The default groups should be emphasized

BUG REPORT DESCRIPTION:
The default breakpoint working set should be emphasized in the view with a bold font.
