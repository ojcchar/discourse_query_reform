BUG REPORT TITLE:
Field completion failed to propose a field declared into an innerclass

BUG REPORT DESCRIPTION:
A field declared into an innerclass, can not be completed, if the user edit the second method of the innerclass.
(It works fine in the first method !?)

See attachment for more details.
