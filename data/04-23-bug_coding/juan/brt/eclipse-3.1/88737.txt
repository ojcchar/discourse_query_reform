BUG REPORT TITLE:
[GlobalActions] Close All is disabled while an editor is open

BUG REPORT DESCRIPTION:
I20050315-1100

- new workspace, new project, new file "Test.txt"
-> editor for Test.txt is open, but File > Close All is disabled
