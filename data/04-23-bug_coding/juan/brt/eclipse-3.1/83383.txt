BUG REPORT TITLE:
IllegalArgumentException in Signature.getParameterCount

BUG REPORT DESCRIPTION:
I20050118 + unreleased code (not showing in any build)

String signature= "foo(+Ljava.lang.Comparable;)";
Signature.getParameterCount(signature);

Background:
I copied code from CompletionRequestorWrapper to work with the new
CompletionRequestor API.
When completing a METHOD_REF proposal for a method that has a parameter with an open type bound, getParameterPackages I get an IAE:

The reason is the Util.scanTypeSignature does not handle bounded types.

-----------

I get this:

!
ENTRY org.eclipse.ui 4 0 2005-01-21 12:11:32.109
!
MESSAGE The command for the key you pressed failed
!
STACK 0 java.lang.IllegalArgumentException
	at org.eclipse.jdt.internal.core.util.Util.scanTypeSignature(Util.java:2115)
	at org.eclipse.jdt.core.Signature.getParameterCount(Signature.java:944)
	at org.eclipse.jdt.core.Signature.getParameterTypes(Signature.java:1060)
at
org.eclipse.jdt.internal.ui.text.java.ResultCollector.getParameterPackages(ResultCollector.java:732)
at
org.eclipse.jdt.internal.ui.text.java.ResultCollector.internalAcceptMethod(ResultCollector.java:254)
at
org.eclipse.jdt.internal.ui.text.java.ResultCollector.accept(ResultCollector.java:654)
at
org.eclipse.jdt.internal.codeassist.CompletionEngine.findLocalMethods(CompletionEngine.java:2816)
at
org.eclipse.jdt.internal.codeassist.CompletionEngine.findIntefacesMethods(CompletionEngine.java:2551)
at
org.eclipse.jdt.internal.codeassist.CompletionEngine.findMethods(CompletionEngine.java:3270)
at
org.eclipse.jdt.internal.codeassist.CompletionEngine.findFieldsAndMethods(CompletionEngine.java:1903)
at
org.eclipse.jdt.internal.codeassist.CompletionEngine.complete(CompletionEngine.java:627)
at
org.eclipse.jdt.internal.codeassist.CompletionEngine.complete(CompletionEngine.java:1205)
	at org.eclipse.jdt.internal.core.Openable.codeComplete(Openable.java:119)
at
org.eclipse.jdt.internal.core.CompilationUnit.codeComplete(CompilationUnit.java:286)
at
org.eclipse.jdt.internal.core.CompilationUnit.codeComplete(CompilationUnit.java:279)
at
org.eclipse.jdt.internal.ui.text.java.JavaCompletionProcessor.internalComputeCompletionProposals(JavaCompletionProcessor.java:363)
at
org.eclipse.jdt.internal.ui.text.java.JavaCompletionProcessor.computeCompletionProposals(JavaCompletionProcessor.java:334)
at
org.eclipse.jface.text.contentassist.ContentAssistant.computeCompletionProposals(ContentAssistant.java:1470)
at
org.eclipse.jface.text.contentassist.CompletionProposalPopup.computeProposals(CompletionProposalPopup.java:250)
at
org.eclipse.jface.text.contentassist.CompletionProposalPopup.access$7(CompletionProposalPopup.java:247)
at
org.eclipse.jface.text.contentassist.CompletionProposalPopup$9.run(CompletionProposalPopup.java:961)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
at
org.eclipse.jface.text.contentassist.CompletionProposalPopup.incrementalComplete(CompletionProposalPopup.java:956)
at
org.eclipse.jface.text.contentassist.ContentAssistant.showPossibleCompletions(ContentAssistant.java:1318)
at
org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor$AdaptedSourceViewer.doOperation(CompilationUnitEditor.java:180)
	at org.eclipse.ui.texteditor.ContentAssistAction$1.run(ContentAssistAction.java:82)
	at org.eclipse.swt.custom.BusyIndicator.showWhile(BusyIndicator.java:69)
	at org.eclipse.ui.texteditor.ContentAssistAction.run(ContentAssistAction.java:80)
	at org.eclipse.jface.action.Action.runWithEvent(Action.java:989)
	at org.eclipse.ui.commands.ActionHandler.execute(ActionHandler.java:188)
	at org.eclipse.ui.internal.commands.Command.execute(Command.java:130)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.executeCommand(WorkbenchKeyboard.java:445)
	at org.eclipse.ui.internal.keys.WorkbenchKeyboard.press(WorkbenchKeyboard.java:724)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.processKeyEvent(WorkbenchKeyboard.java:767)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.filterKeySequenceBindings(WorkbenchKeyboard.java:536)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard.access$2(WorkbenchKeyboard.java:479)
at
org.eclipse.ui.internal.keys.WorkbenchKeyboard$1.handleEvent(WorkbenchKeyboard.java:221)
	at org.eclipse.swt.widgets.EventTable.sendEvent(EventTable.java:82)
	at org.eclipse.swt.widgets.Display.filterEvent(Display.java:1086)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:1001)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:1026)
	at org.eclipse.swt.widgets.Widget.sendEvent(Widget.java:1011)
	at org.eclipse.swt.widgets.Widget.sendKeyEvent(Widget.java:1038)
at org.eclipse.swt.widgets.Widget.gtk_key_press_event(Widget.java:602)
at org.eclipse.swt.widgets.Control.gtk_key_press_event(Control.java:1889)
at org.eclipse.swt.widgets.Composite.gtk_key_press_event(Composite.java:527)
	at org.eclipse.swt.widgets.Widget.windowProc(Widget.java:1338)
	at org.eclipse.swt.widgets.Display.windowProc(Display.java:3261)
at org.eclipse.swt.internal.gtk.OS.
_gtk_main_do_event(Native Method)
at org.eclipse.swt.internal.gtk.OS.gtk_main_do_event(OS.java:4642)
	at org.eclipse.swt.widgets.Display.eventProc(Display.java:926)
at org.eclipse.swt.internal.gtk.OS.
_g_main_context_iteration(Native Method)
at org.eclipse.swt.internal.gtk.OS.g_main_context_iteration(OS.java:1104)
	at org.eclipse.swt.widgets.Display.readAndDispatch(Display.java:2415)
	at org.eclipse.ui.internal.Workbench.runEventLoop(Workbench.java:1575)
	at org.eclipse.ui.internal.Workbench.runUI(Workbench.java:1541)
	at org.eclipse.ui.internal.Workbench.createAndRunWorkbench(Workbench.java:287)
	at org.eclipse.ui.PlatformUI.createAndRunWorkbench(PlatformUI.java:144)
	at org.eclipse.ui.internal.ide.IDEApplication.run(IDEApplication.java:102)
at
org.eclipse.core.internal.runtime.PlatformActivator$1.run(PlatformActivator.java:220)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:274)
	at org.eclipse.core.runtime.adaptor.EclipseStarter.run(EclipseStarter.java:129)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at
sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:585)
	at org.eclipse.core.launcher.Main.basicRun(Main.java:255)
	at org.eclipse.core.launcher.Main.run(Main.java:811)
	at org.eclipse.core.launcher.Main.main(Main.java:795)
The command for the key you pressed failed
