BUG REPORT TITLE:
provide access to class loader for types in java debugger

BUG REPORT DESCRIPTION:
PDE will require access to a stack frame's declaring type's class loader to perform source lookup that mimics the OSGi runtime class loading model.
