BUG REPORT TITLE:
[Presentations] (regression) Standalone view without title has no border

BUG REPORT DESCRIPTION:
build N20050523

- change the browser example's BrowserPerspectiveFactory to have the following instead of the regular addView layout.addStandaloneView(BrowserApp.BROWSER_VIEW_ID, false,
IPageLayout.RIGHT, .25f, IPageLayout.ID_EDITOR_AREA);

- run the example, and show the history view
- the history view (a regular view) has a border, but the standalone view does not

This is a regression from 3.0.2.

Standalone views should still have their border, just not the title or min/max buttons if showTitle==false.
