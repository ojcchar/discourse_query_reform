BUG REPORT TITLE:
Clicking junit call stack items doesn't open source anymore

BUG REPORT DESCRIPTION:
I'd be really surprised if this isn't either a known issue or something silly I did wrong but I can't find nothing about it.

When doing my ant builds if I get a junit test failure it spits out the call stack as it always did.
For some reason since upgrading from somewhere around 3
M7 to 3 clicking items in the call stack will no longer open the source code.
I'm using the same build.xml and launch config I've been using since forever ago.

The message I get is this:

-----------------------------
Information
-----------------------------
Source not found for com.booksys.atriuum.tests.AdminBarcodeInfoTest
-----------------------------
OK
-----------------------------

com.booksys.atriuum.tests.AdminBarcodeInfoTest is clearly in my project but it can't find it anymore :(
