

set CUR_DIR=%CD%

rem change these env variables (paths to the root folder of the cloned repositories)
set REPOSITORIES_PATH=C:\Users\ojcch\Documents\Repositories\projects

set APPCORE_REPO_PATH=%REPOSITORIES_PATH%\appcore
set TXT_ANALYZER_REPO_PATH=%REPOSITORIES_PATH%\text-analyzer
set BUG_REPORT_COMPLETION_REPO_PATH=%REPOSITORIES_PATH%\bug_report_completion
set IR2SE_REPO_PATH=%REPOSITORIES_PATH%\ir4se-fwk
REM set BUG_ANALYSIS_REPO_PATH=%REPOSITORIES_PATH%\bug-report-analyzer
set BUG_LOCALIZATION_PATH=%REPOSITORIES_PATH%\discourse_query_reform
set QUERY_PROCESSOR_REPO_PATH=%REPOSITORIES_PATH%\query-processor
set CODE_PARSER_PATH=%REPOSITORIES_PATH%\code-parser
set STORMED_CLIENT_PATH=%REPOSITORIES_PATH%\stormed-client
set SEERS_BASE_PATH=%REPOSITORIES_PATH%\seers-base


rem repo update

rem to avoid entering username\pwd everytime, create\modify the .netrc (or .hgrc, see below) file in the home folder
rem with the following content (note the 'replace_me'):
rem
rem machine github.com
rem login replace_me
rem password replace_me
rem
rem machine gitlab.com
rem login replace_me
rem password replace_me
rem
rem for Mercurial, create\modify .hgrc (in the home folder) with:
rem
rem [auth]
rem foo.prefix = bitbucket.org\laumoreno\ir4se-fwk
rem foo.username = replace_me
rem foo.password = replace_me
rem
cd "%APPCORE_REPO_PATH%" && git pull
cd "%TXT_ANALYZER_REPO_PATH%" && git pull
cd "%BUG_REPORT_COMPLETION_REPO_PATH%" && git pull
cd "%IR2SE_REPO_PATH%" && hg pull && hg update
cd "%QUERY_PROCESSOR_REPO_PATH%" && hg pull && hg update
REM cd "%BUG_ANALYSIS_REPO_PATH%" && git pull
cd "%BUG_LOCALIZATION_PATH%" && git pull && git checkout index-gen-project
cd "%SEERS_BASE_PATH%" && git pull


rem project building
cd "%APPCORE_REPO_PATH%\appcore" && git checkout 0.0.1 && call mvn clean install -DskipTests && @echo on
cd "%APPCORE_REPO_PATH%\appcore" && git checkout master && call gradlew clean install && @echo on
cd "%TXT_ANALYZER_REPO_PATH%\text-analyzer" && call gradlew clean install && @echo on
cd "%BUG_REPORT_COMPLETION_REPO_PATH%\code\bug_report_coding" && call gradlew clean install && @echo on
cd "%BUG_REPORT_COMPLETION_REPO_PATH%\code\bug_report_patterns" && call gradlew clean install && @echo on
cd "%BUG_REPORT_COMPLETION_REPO_PATH%\code\bug_report_classifier" && call gradlew clean install && @echo on
cd "%BUG_REPORT_COMPLETION_REPO_PATH%\code\bug_report_parser\bugparser" && call gradlew clean install && @echo on
cd "%IR2SE_REPO_PATH%\ir4se-fwk" && call gradlew clean install && @echo on
cd "%CODE_PARSER_PATH%\code-parser" && call mvn clean install -DskipTests && @echo on
cd "%SEERS_BASE_PATH%" && call gradlew clean publishToMavenLocal
REM cd "%STORMED_CLIENT_PATH%\stormed-client" && call mvn clean package -DskipTests && @echo on
REM call mvn install:install-file "-Dfile=%STORMED_CLIENT_PATH%/stormed-client/target/stormed-client-1.3.jar" "-DgroupId=seers" "-DartifactId=stormed-client" "-Dversion=1.3" "-Dpackaging=jar"
cd "%QUERY_PROCESSOR_REPO_PATH%" && call mvn clean install -DskipTests && @echo on
REM cd "%BUG_ANALYSIS_REPO_PATH%\bug-report-analyzer" && call mvn clean install -DskipTests && @echo on
cd "%BUG_LOCALIZATION_PATH%\code\base-commons" && call mvn clean install -DskipTests && @echo on
cd "%BUG_LOCALIZATION_PATH%\code\corpus-processor" && call mvn clean install -DskipTests && @echo on
cd "%BUG_LOCALIZATION_PATH%\code\index-generator" && call mvn clean install -DskipTests && @echo on
cd "%BUG_LOCALIZATION_PATH%\code\query-generation" && call mvn clean install -DskipTests && @echo on
cd "%BUG_LOCALIZATION_PATH%\code\retrieval-runner\retrieval-runner" && call mvn clean install -DskipTests && @echo on

cd "%CUR_DIR%"
