package seers.disqueryreform.corpus;

import org.junit.Test;

public class D4JVocabExtractorMainTest {

	@Test
	public void testMain() throws Exception {

		String bugsSrcFolder = "test_data/bugs";
		String bugsFile = "test_data/buglist.txt";
		String outputDir = "test_data/output";
		String[] args = { bugsSrcFolder, bugsFile, outputDir };
		D4JVocabExtractorMain.main(args);
	}

}
