package seers.disqueryreform.corpus;

import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.RetrievalUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GoldSetStats {

    public static void main(String[] args) throws IOException {
        String baseFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
                "\\existing_data_sets";
        DataSet[] dataSets = DataSet.values();
        for (DataSet dataSet : dataSets) {

            final List<Query> queries = RetrievalUtils.readAllQueries(baseFolder, dataSet);
            printQueries(dataSet, queries);
        }
    }

    private static void printQueries(DataSet dataSet, List<Query> queries) {

        for (Query query : queries) {
            String project = query.getInfoAttribute("system").toString();
            String bugId = query.getKey();
            String numDocsInGoldSet = String.valueOf(RetrievalUtils.readGoldSet(dataSet, query).size());
            final List<String> values = Arrays.asList(dataSet.toString().toLowerCase(), project, bugId, numDocsInGoldSet);
            final String stringToPrint = values.stream().collect(Collectors.joining(";"));
            System.out.println(stringToPrint);
        }
    }
}
