package seers.disqueryreform.corpus.code;

import org.apache.commons.io.FileUtils;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.utils.FilePathUtils;
import seers.appcore.xml.XMLHelper;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.codeident.CodeTaggedBugReport;
import seers.disqueryreform.base.codeident.CodeTaggedBugReportDescription;
import seers.disqueryreform.base.codeident.CodeTaggedBugReportTitle;
import seers.disqueryreform.base.codeident.TaggedText;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PreprocessCodeInBugsMain {

    static String scriptsPath = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\ir4se-fwk\\ir4se-fwk\\src\\scripts" +
            "\\corpus" +
            "-preprocessing";
    static String tempDir = "C:\\Users\\ojcch\\Documents\\Projects\\Discourse_query_reformulation\\temp" +
            "\\code_preprocessing";
    static String baseDataFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
            "\\existing_data_sets";
    static DataSet[] dataSets = {DataSet.BRT};

    public static void main(String[] args) throws Exception {

        for (DataSet dataSet : dataSets) {
            processDataSet(dataSet);
        }

    }

    private static void processDataSet(DataSet dataSet) throws Exception {

        //get the projectsFolder
        File[] projectsFolder = DataSetUtils.getBRProjectFolders(baseDataFolder, dataSet);

        //for each project
        for (File projectFolder : projectsFolder) {

            //read the bug reports/queries
            List<CodeTaggedBugReport> bugReports = getCodeTaggedBugReports(dataSet, projectFolder);

            //paginate the BR/queries
            final ThreadParameters params = new ThreadParameters();
            params.addParam("dataSet", dataSet);
            params.addParam("projectFolder", projectFolder);
            ThreadExecutor.executePaginated(bugReports, BRPreProcessor.class, params);

        }
    }


    private static List<CodeTaggedBugReport> getCodeTaggedBugReports(DataSet dataSet, File projectFolder)
            throws            Exception {

        final File codeTaggedBugReportsFolder = DataSetUtils.getCodeTaggedBugReportsFolder(baseDataFolder, dataSet,
                projectFolder.getName(), null, false);

        final File[] xmlFiles = codeTaggedBugReportsFolder.listFiles(f -> f.getName().endsWith(".xml"));
        List<CodeTaggedBugReport> brs = new ArrayList<>();
        if (xmlFiles!=null) {
            for (File xmlFile : xmlFiles) {
                final CodeTaggedBugReport br = XMLHelper.readXML(CodeTaggedBugReport.class, xmlFile);
                brs.add(br);
            }
        }

        return brs;
    }

    public static class BRPreProcessor extends ThreadProcessor {

        private final List<CodeTaggedBugReport> bugReports;
        private final DataSet dataSet;
        private final File projectFolder;

        public BRPreProcessor(ThreadParameters params) {
            super(params);
            bugReports = params.getListParam(CodeTaggedBugReport.class, ThreadExecutor.ELEMENTS_PARAM);
            dataSet = params.getParam(DataSet.class, "dataSet");
            projectFolder = params.getParam(File.class, "projectFolder");
        }

        @Override
        public void executeJob() throws Exception {

            for (CodeTaggedBugReport bugReport : bugReports) {
                try {
                    LOGGER.debug("Processing "+ projectFolder.getName() + ";" + bugReport.getBugId());

                    final List<TaggedText> titleTaggedTexts = bugReport.getTitle().getTaggedTexts();
                    final List<TaggedText> descriptionTaggedTexts = bugReport.getDescription().getTaggedTexts();

                    //preprocess
                    final List<TaggedText> prepTitleTaggedTexts = preprocessTexts(bugReport.getBugId(),
                            titleTaggedTexts);
                    final List<TaggedText> prepDescriptionTaggedTexts = preprocessTexts(bugReport.getBugId(),
                            descriptionTaggedTexts);

                    //dump the xml
                    File outFile = DataSetUtils.getBugReportCodeFile(baseDataFolder, dataSet, projectFolder.getName()
                            , bugReport.getBugId(), true);
                    outFile.getParentFile().mkdirs();
                    CodeTaggedBugReport prepBugReport = new CodeTaggedBugReport(bugReport.getBugId(), new
                            CodeTaggedBugReportTitle
                            (prepTitleTaggedTexts), new
                            CodeTaggedBugReportDescription(prepDescriptionTaggedTexts));
                    XMLHelper.writeXML(prepBugReport, outFile);

                } catch (Exception e) {
                    LOGGER.error("Error for bug report: " + projectFolder.getName() + ";" + bugReport.getBugId(), e);
                }
            }
        }

        private List<TaggedText> preprocessTexts(String bugId, List<TaggedText> texts) throws IOException,
                InterruptedException {
            List<TaggedText> prepTexts = new ArrayList<>();

            if (texts==null || texts.isEmpty())
                return  prepTexts;

            final File tempFile = FilePathUtils.getFile(new File(tempDir), bugId + ".txt");
            for (TaggedText text : texts) {

                final String nonPrepTextValue = text.getValue();
                FileUtils.write(tempFile, nonPrepTextValue, Charset.defaultCharset());

                //------------------------------

                String[] cmdarray ={"cmd", "/c", "python", "preprocess-in-text.py", "<", tempFile
                        .getAbsolutePath()};
                Process p = new ProcessBuilder(cmdarray)
                        .directory(new File(scriptsPath))
                        .redirectErrorStream(true)
                        .start();

                //------------------------------

                BufferedReader stdInput = new BufferedReader(new
                        InputStreamReader(p.getInputStream()));

                StringBuilder prepValue = new StringBuilder();
                String inLine;
                // read the output from the command
                while ((inLine = stdInput.readLine()) != null) {
                    prepValue.append(inLine);
                }

                final String value = prepValue.toString();

                //------------------------------

                p.waitFor();
                final int exitValue = p.exitValue();
                if (exitValue != 0) {
                    throw new RuntimeException("Error executing " + Arrays.toString(cmdarray) + " :\n" + value);
                }

                //------------------------------

                prepTexts.add(new TaggedText(text.getType(), value));

            }
            return prepTexts;
        }
    }
}
