package seers.disqueryreform.corpus.bench4bl;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.utdallas.seers.entity.SourceFileText;
import edu.utdallas.seers.ir4se.extraction.TextExtractionMain;
import edu.utdallas.seers.ir4se.index.utils.DateTimeJsonAdapter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.bench4bl.Bench4BLUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CodeCorpusFormatter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodeCorpusFormatter.class);
    private static String outFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
            "\\existing_data_sets\\bench4bl_data";
    private static String taskName = "NewDataAsIs";
    private static String technique = "Locus";

    public static void main(String[] args) throws Exception {

        for (ImmutablePair<String, String> groupProject : Bench4BLUtils.groupProjects) {
            final String group = groupProject.getLeft();
            final String project = groupProject.getRight();

            LOGGER.debug(String.format("%s-%s", group, project));

            File outputDirectory = Paths.get(outFolder, project).toFile();
            if (!outputDirectory.exists())
                FileUtils.forceMkdir(outputDirectory);

            File[] sourceFolders = Bench4BLUtils.getSourceFolders(group, project);
            for (File sourceFolder : sourceFolders) {

                LOGGER.debug(String.format("%s-%s-%s", group, project, sourceFolder.getName()));
                try {
                    File corpusOutFile =null;
//                    File corpusOutFile = TextExtractionMain.processSystem(outputDirectory.toString(), sourceFolder,
//                            false);

                    validateCorpus(group, project, corpusOutFile, sourceFolder);
                } catch (Exception e) {
                    LOGGER.error("Error for " + sourceFolder, e);
                    throw e;
                }
            }

        }


    }

    private static void validateCorpus(String group, String project, File corpusOutFile, File sourceFolder) throws IOException {

        final String systemVersion = sourceFolder.getName();

        List<String> sourceFiles = Bench4BLUtils.sourceFileIndex(group, project, systemVersion, taskName, technique);

        if (sourceFiles == null)
            return;


        final Map<String, List<SourceFileText>> allCorpusFiles = getSourceFilesStream(corpusOutFile)
                .collect(Collectors.groupingBy(SourceFileText::getFilePath));

        for (String sourceFile : sourceFiles) {
            List<SourceFileText> srcFiles = allCorpusFiles.get(sourceFile);
            if (srcFiles == null)
                LOGGER.warn("File not found: " + sourceFile);
            else if (srcFiles.size() != 1)
                LOGGER.warn(String.format("Multiple files for %s: %s", sourceFile, srcFiles));
        }

    }

    public static Stream<SourceFileText> getSourceFilesStream(File corpusOutFile) throws IOException {

        Gson gson = getGson();
        return FileUtils.readLines(corpusOutFile, Charset.defaultCharset())
                .stream().map(line -> {
                    final SourceFileText sourceFileText = gson.fromJson(line, SourceFileText.class);
                    final String filePathFormatted = getFilePathFormatted(sourceFileText);
                    sourceFileText.setFilePath(filePathFormatted);
                    return sourceFileText;
                });
    }

    public static String getFilePathFormatted(SourceFileText sourceFileText) {
        final String filePath = sourceFileText.getFilePath();
        return filePath.substring(1).replace("/", ".");
    }

    public static Gson getGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeJsonAdapter());
        return gsonBuilder.create();
    }

}
