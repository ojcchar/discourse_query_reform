package seers.disqueryreform.corpus;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.disqueryreform.base.D4JUtils;

public class D4JVocabExtractorMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(D4JVocabExtractorMain.class);

	static String bugsSrcFolder = "";
	static String bugsFile = "";
	static String outputDir = "";

	public static void main(String[] args) throws Exception {

		bugsSrcFolder = FilenameUtils.separatorsToSystem(args[0]);
		bugsFile = args[1];
		outputDir = args[2];

		// read all the bugs
		List<List<String>> allBugs = D4JUtils.readLines(new File(bugsFile), ';');

		// paginate the bugs
		List<List<String>> subList = allBugs.subList(1, allBugs.size());

		LOGGER.debug("# of bugs: " + subList.size());

		ThreadExecutor.executePaginated(subList, BugExecutor.class, new ThreadParameters(), 10, 50);

		LOGGER.debug("Done!");
	}



	
}
