package seers.disqueryreform.corpus.querrqualdata;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.codeparser.CodeVocabularyExtractor;
import seers.disqueryreform.base.D4JUtils;
import seers.disqueryreform.corpus.D4JVocabExtractorMain;

public class QQDVocabExtractorMain {

	static String systemsFile;
	static String srcBaseFolder = "";
	static String outputFolder;

	private static final Logger LOGGER = LoggerFactory.getLogger(D4JVocabExtractorMain.class);

	public static void main(String[] args) throws Exception {

		systemsFile= args[0];
		srcBaseFolder = FilenameUtils.separatorsToSystem(args[1]);
		outputFolder= args[2];

		List<List<String>> allSystems = D4JUtils.readLines(new File(systemsFile), ';');

		List<List<String>> subList = allSystems.subList(1, allSystems.size());

		LOGGER.debug("# of bugs: " + subList.size());

		ThreadExecutor.executeOneByOne(subList, SystemProcessor.class, new ThreadParameters(), 15);

		LOGGER.debug("Done!");
	}

	public static class SystemProcessor extends ThreadProcessor {

		private List<String> systemInfo;

		public SystemProcessor(ThreadParameters params) {
			super(params);
			systemInfo = params.getListParam(String.class, ThreadExecutor.ELEMENT_PARAM);
		}

		@Override
		public void executeJob() throws Exception {
			String system = systemInfo.get(0);

			try {
				// get data
				String[] srcDirs = systemInfo.get(1).split(",");

				// read all java files
				String projectBaseDir = srcBaseFolder + File.separator + system;
				File projectDir = new File(projectBaseDir);
				LinkedList<File> allFiles = (LinkedList<File>) FileUtils.listFiles(projectDir, new String[] { "java" },
						true);

				LOGGER.debug("Processing " + system + ": # files -> " + allFiles.size());

				// out file
				File outFile = new File(outputFolder + File.separator + system + ".json");
				if (outFile.exists()) {
					boolean delete = outFile.delete();
					if (!delete) {
						throw new RuntimeException("Output file not deleted");
					}
				}

				// paginate the files
				ThreadParameters params2 = new ThreadParameters();
				params2.addParam("outFile", outFile);
				params2.addParam("baseFolder", projectBaseDir);
				params2.addParam("classPaths", new String[] { projectDir.getAbsolutePath() });
				params2.addParam("sourceFolders", srcDirs);
				params2.addParam("sequence", new AtomicInteger(1));

				ThreadExecutor.executePaginated(allFiles, SystemFileExecutor.class, params2, 50, 20);
			} catch (Exception e) {
				LOGGER.error("Error for: " + system, e);
			}

		}

	}

	public static class SystemFileExecutor extends ThreadProcessor {

		private List<File> files;
		private File outFile;
		private CodeVocabularyExtractor extractor;
		private AtomicInteger sequence;
		private String baseFolder;

		public SystemFileExecutor(ThreadParameters params) {
			super(params);
			files = params.getListParam(File.class, ThreadExecutor.ELEMENTS_PARAM);
			outFile = params.getParam(File.class, "outFile");
			baseFolder = params.getStringParam("baseFolder");
			String[] classPaths = params.getParam(String[].class, "classPaths");
			String[] sourceFolders = params.getParam(String[].class, "sourceFolders");
			extractor = new CodeVocabularyExtractor(baseFolder, classPaths, sourceFolders);
			sequence = params.getParam(AtomicInteger.class, "sequence");
		}

		@Override
		public void executeJob() throws Exception {

			Gson gson = new Gson();

			StringBuffer buffer = new StringBuffer();
			// for each java file
			for (File file : files) {

				try {
					// extract method vocabulary
					HashMap<String, List<String>> methodVocabulary = extractor.extractMethodVocabulary(file);
					Set<Entry<String, List<String>>> entrySet = methodVocabulary.entrySet();
					String filePathString = getFile(file);

					for (Entry<String, List<String>> entry : entrySet) {

						// create json object
						Map<String, Object> json = new LinkedHashMap<>();
						json.put("id", sequence.getAndIncrement());
						json.put("qname", entry.getKey());
						json.put("text", getWholeText(entry.getValue()));
						json.put("file", filePathString);

						// write the vocabulary
						buffer.append(gson.toJson(json));
						buffer.append("\n");
					}
				} catch (Exception e) {
					LOGGER.error("Error for file: " + file, e);
				}

			}

			FileUtils.write(outFile, buffer.toString(), true);
		}

		private String getFile(File file) {

			StringBuffer absolutePath = new StringBuffer(file.getAbsolutePath());

			int indexOf = absolutePath.indexOf(baseFolder);
			if (indexOf != -1) {
				absolutePath.delete(0, indexOf + baseFolder.length());
			}
			return absolutePath.toString();
		}

		private String getWholeText(List<String> textItems) {

			StringBuffer buffer = new StringBuffer();

			for (String txtItem : textItems) {
				buffer.append(txtItem);
				buffer.append(" ");
			}

			return buffer.toString().trim();
		}

	}
}
