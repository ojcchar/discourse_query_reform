package seers.disqueryreform.corpus.code;

import edu.utdallas.seers.entity.BugReport;
import seers.appcore.csv.CSVHelper;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class SourceCodeIdentifierFromSampleMain extends SourceCodeIdentifierMain {


    public static void main(String[] args) throws Exception {

        String baseDataFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform" +
                "\\data" +
                "\\existing_data_sets";
//        String sampleFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\output" +
//                "\\bugs_with_no_code_yet-08-17.csv";
        String sampleFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\output" +
                "\\sampling\\8-30\\sample-8-30.csv";

        //read sample
        List<List<String>> sampleLines = CSVHelper.readCsv(sampleFile, true, CSVHelper.DEFAULT_SEPARATOR);

        //group by data set
        final Map<String, List<List<String>>> dataSetBugSamples =
                sampleLines.stream().collect(Collectors.groupingBy(l -> l.get(0)));

        for (Map.Entry<String, List<List<String>>> dsEntry : dataSetBugSamples.entrySet()) {

            final String strDataSet = dsEntry.getKey();
            final List<List<String>> dsBugSamples = dsEntry.getValue();

            DataSet dataSet = DataSet.valueOf(strDataSet.toUpperCase());

            //group by project
            final Map<String, List<List<String>>> projectBugSample =
                    dsBugSamples.stream().collect(Collectors.groupingBy(l -> l.get(1)));

            //get the projectsFolder
            File[] allProjectsFolder = DataSetUtils.getBRProjectFolders(baseDataFolder, dataSet);

            final List<File> sampleProjectFolders = Arrays.stream(allProjectsFolder)
                    .filter(f -> projectBugSample.containsKey(f.getName()))
                    .collect(Collectors.toList());

            //for each project
            for (File projectFolder : sampleProjectFolders) {

                final List<List<String>> projectBugs = projectBugSample.get(projectFolder.getName());

                //read the bug reports/queries
                List<BugReport> bugReports = getBugReports(dataSet, projectFolder);

                //filter only the ones in the sample
                final List<BugReport> filteredBugReports = bugReports.stream()
                        .filter(b -> projectBugs.stream().anyMatch(l -> l.get(2).equals(b.getKey())))
                        .collect(Collectors.toList());

                //paginate the BR/queries
                final ThreadParameters params = new ThreadParameters();
                params.addParam("dataSet", dataSet);
                params.addParam("projectFolder", projectFolder);
                params.addParam("baseDataFolder", baseDataFolder);
                ThreadExecutor.executePaginated(filteredBugReports, BRTextLabelProcessor.class,
                        params);
            }
        }
    }
}
