package seers.disqueryreform.corpus;

import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import seers.appcore.csv.CSVHelper;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.QueryReformStrategy;
import seers.disqueryreform.base.RetrievalUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SampledQueryStats {

    public static void main(String[] args) throws Exception {
        DataSet[] dataSets = DataSet.values();
        String queriesOutBaseFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation" +
                "/reformulated_queries";
        String sampledQueriesFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
                "\\existing_data_sets\\all-hard-to-retrieve-queries-b10_executed_sampled.csv";
        final List<List<String>> allQueries = CSVHelper.readCsv(sampledQueriesFile, true, CSVHelper.DEFAULT_SEPARATOR);
        QueryReformStrategy[] strategies = {QueryReformStrategy.TITLE, QueryReformStrategy.OB, QueryReformStrategy.EB,
                QueryReformStrategy.S2R, QueryReformStrategy.CODE};

        for (DataSet dataSet : dataSets) {
            final File queriesOutFolder = DataSetUtils.getReformulatedQueriesOutFolder
                    (queriesOutBaseFolder, dataSet, true);
            List<List<String>> dsQueries = allQueries.stream().filter(q -> q.get(0).equalsIgnoreCase(dataSet.toString
                    ())).collect(Collectors.toList());

            List<List<Query>> stratQueries = new ArrayList<>();
            for (QueryReformStrategy strategy : strategies) {
                String strategyFolder = queriesOutFolder.getAbsolutePath() + File.separator +
                        "TITLE_DESCRIPTION" + File.separator + strategy.toString();
//                System.out.println(strategyFolder);
                final List<Query> queries = RetrievalUtils.readReformulatedBugReports(strategyFolder, false);
                stratQueries.add(queries);
            }

            for (List<String> dsQuery : dsQueries) {
                List<String> info = hasInfo(stratQueries, dsQuery);
                List<String> values = new ArrayList<>(dsQuery);
                values.addAll(info);
                final String stringToPrint = values.stream().collect(Collectors.joining(";"));
                System.out.println(stringToPrint);
            }

        }
    }

    private static List<String> hasInfo(List<List<Query>> allStratsQueries, List<String> dsQuery) {
        String project = dsQuery.get(1);
        String bugId = dsQuery.get(2);

        List<String> info = new ArrayList<>();
        for (List<Query> stratQueries : allStratsQueries) {
            final boolean match = stratQueries.stream().anyMatch(q -> q.getKey().equals(bugId) && q.getInfoAttribute
                    ("system").toString()                    .equals(project));
            info.add(match?"x":"");
        }
        return info;
    }
}
