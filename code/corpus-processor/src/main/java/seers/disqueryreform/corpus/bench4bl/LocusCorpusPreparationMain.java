package seers.disqueryreform.corpus.bench4bl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.bench4bl.Bench4BLUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LocusCorpusPreparationMain {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocusCorpusPreparationMain.class);

    public static void main(String[] args) throws Exception {

        String baseDataFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
                "\\existing_data_sets";

        //for each project
        for (ImmutablePair<String, String> groupProject : Bench4BLUtils.groupProjects) {
            final String group = groupProject.getLeft();
            final String project = groupProject.getRight();

            final List<File> subProjects = getSubProjectsInOurRepo(baseDataFolder, project);

            for (File subProject : subProjects) {
                copySources(group, project, subProject);
                copyGitRepo(group, project, subProject);
            }

            LOGGER.debug("Done: " + project);
        }
    }

    private static void copyGitRepo(String group, String project, File subProject) throws IOException,
            InterruptedException {

        //checkout repo
        String tag = Bench4BLUtils.getTag(group, project, subProject.getName());
        final File gitRepoFolder = Bench4BLUtils.getGitRepoFolder(group, project);
        hardResetRepo(tag, gitRepoFolder);

        //copy the checkout
        final File destDir = Paths.get(subProject.getAbsolutePath(), "git-repo").toFile();
        copyDir(gitRepoFolder, destDir);
    }

    private static void hardResetRepo(String tag, File gitRepoFolder) throws IOException, InterruptedException {
        String[] command = {"git", "reset", "--hard", tag};
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.directory(gitRepoFolder);
        Process tool = pb.start();

        tool.waitFor();

        String normalOutput = new BufferedReader(new InputStreamReader(tool.getInputStream())).lines()
                .collect(Collectors.joining("\n\t", "\t", "\n"));
        String errorOutput = new BufferedReader(new InputStreamReader(tool.getErrorStream())).lines()
                .collect(Collectors.joining("\n\t", "\t", ""));

        LOGGER.debug(normalOutput);
        LOGGER.debug(errorOutput);
        if (tool.exitValue() != 0) {
            throw new RuntimeException("Process finished with errors!");
        }
    }


    private static void copySources(String group, String project, File subProject) throws IOException {
        final File sourceFolder = Bench4BLUtils.getSourceFolder(group, project, subProject.getName());
        final File destDir = Paths.get(subProject.getAbsolutePath(), "sources").toFile();
        copyDir(sourceFolder, destDir);
        return;
    }

    private static void copyDir(File sourceFolder, File destDir) throws IOException {
        LOGGER.debug(String.format("Copying %s to %s", sourceFolder, destDir));
        FileUtils.copyDirectory(sourceFolder, destDir);
    }

    private static List<File> getSubProjectsInOurRepo(String baseDataFolder, String project) {
        final File[] projectFolders = DataSetUtils.getBRProjectFolders(baseDataFolder, DataSet.B4BL);
        final File projectFolder =
                Arrays.stream(projectFolders).filter(file -> file.getName().equals(project)).findFirst().get();
        final File[] subProjects = projectFolder.listFiles(File::isDirectory);

        return Arrays.stream(subProjects).filter(subProject -> {
            final File file = Paths.get(subProject.getAbsolutePath(), "bug-reports.json").toFile();
            return file.exists();
        }).collect(Collectors.toList());
    }
}
