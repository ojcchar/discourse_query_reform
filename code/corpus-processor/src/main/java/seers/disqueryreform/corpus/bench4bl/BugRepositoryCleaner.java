package seers.disqueryreform.corpus.bench4bl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.xml.XMLHelper;
import seers.disqueryreform.base.bench4bl.Bench4BLUtils;
import seers.disqueryreform.base.bench4bl.Bug;
import seers.disqueryreform.base.bench4bl.BugRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class BugRepositoryCleaner {

    private static final Logger LOGGER = LoggerFactory.getLogger(BugRepositoryCleaner.class);

    public static void main(String[] args) throws Exception {
        //for each project
        for (ImmutablePair<String, String> groupProject : Bench4BLUtils.groupProjects) {
            final String group = groupProject.getLeft();
            final String project = groupProject.getRight();

            LOGGER.debug(String.format("%s-%s", group, project));

            final File originalFile = backAndReadOriginalFile(group, project);
            final BugRepository bugRepository =XMLHelper.readXML(BugRepository.class, originalFile);
            cleanRepo(bugRepository);

            final File outFile = Bench4BLUtils.getBugRepositoryFile(group, project);
            XMLHelper.writeXML(bugRepository, outFile);

        }
    }

    private static File backAndReadOriginalFile(String group, String project) throws IOException {
        final File file = Bench4BLUtils.getBugRepositoryFile(group, project);
        final File originalFile = Paths.get(file.getParent(), "repository.xml.orig").toFile();
        if (!originalFile.exists())
            FileUtils.copyFile(file, originalFile);
        return originalFile;
    }

    private static void cleanRepo(BugRepository bugRepository) {

        for (Bug bug : bugRepository.getBugs()) {
            final String summary = bug.getSummary();
            final String description = bug.getDescription();

            final String sum1 = StringEscapeUtils.unescapeHtml4(summary);
            final String desc1 = StringEscapeUtils.unescapeHtml4(description);
            bug.updateText(unescape(sum1),
                    unescape(desc1));
        }
    }

    private static String unescape(String text) {
        return text.replace("&apos;", "'");
    }
}
