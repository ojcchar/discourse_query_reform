package seers.disqueryreform.corpus;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.codeparser.CodeVocabularyExtractor;

public class BugFileExecutor extends ThreadProcessor {

	private List<File> files;
	private File outFile;
	private CodeVocabularyExtractor extractor;
	private AtomicInteger sequence;
	private String baseFolder;

	public BugFileExecutor(ThreadParameters params) {
		super(params);
		files = params.getListParam(File.class, ThreadExecutor.ELEMENTS_PARAM);
		outFile = params.getParam(File.class, "outFile");
		baseFolder = params.getStringParam("baseFolder");
		String[] classPaths = params.getParam(String[].class, "classPaths");
		String[] sourceFolders = params.getParam(String[].class, "sourceFolders");
		extractor = new CodeVocabularyExtractor(baseFolder, classPaths, sourceFolders);
		sequence = params.getParam(AtomicInteger.class	, "sequence");
	}

	@Override
	public void executeJob() throws Exception {

		Gson gson = new Gson();

		StringBuffer buffer = new StringBuffer();
		// for each java file
		for (File file : files) {

			try {
				// extract method vocabulary
				HashMap<String, List<String>> methodVocabulary = extractor.extractMethodVocabulary(file);
				Set<Entry<String, List<String>>> entrySet = methodVocabulary.entrySet();
				String filePathString = getFile(file);

				for (Entry<String, List<String>> entry : entrySet) {

					// create json object
					Map<String, Object> json = new LinkedHashMap<>();
					json.put("id", sequence.getAndIncrement());
					json.put("qname", entry.getKey());
					json.put("text", getWholeText(entry.getValue()));
					json.put("file", filePathString);

					// write the vocabulary
					buffer.append(gson.toJson(json));
					buffer.append("\n");
				}
			} catch (Exception e) {
				LOGGER.error("Error for file: " + file, e);
			}

		}

		FileUtils.write(outFile, buffer.toString(), true);
	}

	private String getFile(File file) {
		
		StringBuffer absolutePath = new StringBuffer( file.getAbsolutePath());
		
		int indexOf = absolutePath.indexOf(baseFolder);
		if (indexOf!=-1) {
			absolutePath.delete(0, indexOf + baseFolder.length());
		}
		return absolutePath.toString();
	}

	private String getWholeText(List<String> textItems) {

		StringBuffer buffer = new StringBuffer();

		for (String txtItem : textItems) {
			buffer.append(txtItem);
			buffer.append(" ");
		}

		return buffer.toString().trim();
	}

}
