package seers.disqueryreform.corpus.bench4bl;

import com.google.gson.Gson;
import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.entity.SourceFileText;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.disqueryreform.base.bench4bl.Bench4BLUtils;
import seers.disqueryreform.base.bench4bl.Bug;
import seers.disqueryreform.base.bench4bl.BugRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BugRepositoryFormatter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodeCorpusFormatter.class);
    private static String corpusFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform" +
            "\\data" +
            "\\existing_data_sets\\bench4bl_data";
    private static String outFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
            "\\existing_data_sets\\bench4bl_data2";

    public static void main(String[] args) throws Exception {

        File corpusBaseDir= new File(corpusFolder);
        final Gson gson = CodeCorpusFormatter.getGson();

        //for each project
        for (ImmutablePair<String, String> groupProject : Bench4BLUtils.groupProjects) {
            final String group = groupProject.getLeft();
            final String project = groupProject.getRight();

            //create the output directory
            File outputDirectory = Paths.get(outFolder, project).toFile();
            if (!outputDirectory.exists())
                FileUtils.forceMkdir(outputDirectory);

            //read all the bugs for this project
            BugRepository bugRepo = Bench4BLUtils.getBugRepository(group, project);
            final List<Bug> repoBugs = bugRepo.getBugs();


            //also, read versions that the bugs belong to
            final List<ImmutablePair<String, String>> bugsWithVersions =
                    Bench4BLUtils.getBugsWithVersions(group, project);
            Map<String, List<ImmutablePair<String, String>>> bugsWithVersionsIndexed =
                    bugsWithVersions.stream()
                            .collect(Collectors.groupingBy(ImmutablePair::getRight));

            final Map<String, List<Bug>> bugsPerVersion = repoBugs.stream().collect(Collectors.groupingBy(b -> {
                final List<ImmutablePair<String, String>> bugVersion = bugsWithVersionsIndexed.get(b.getId());
                if (bugVersion == null)
                    return "other";
                return bugVersion.get(0).left;
            }));


            //for each bug
            for (Map.Entry<String, List<Bug>> entry : bugsPerVersion.entrySet()) {

                final String codeVersion = entry.getKey();
                final List<Bug> versionBugs = entry.getValue();

                //get the corpus for the system version for the bug
                List<SourceFileText> corpus = getCorpus(codeVersion, corpusBaseDir);
                List<String> lines = new ArrayList<>();

                for (Bug bug : versionBugs) {

                    try {

                        String bugKey = String.format("%s-%s", project, bug.getId());

                        //convert the bug to our format
                        BugReport bugReport = new BugConverter(project, bug, bugKey, corpus).invoke();
                        if (bugReport == null) continue;
                        final String json = gson.toJson(bugReport);
                        lines.add(json);

                    } catch (Exception e) {
                        LOGGER.debug(String.format("Error for bug %s-%s", project, bug.getId()), e);
//                    throw e;
                    }
                }

                File outFile = Paths.get(outputDirectory.toString(),
                        codeVersion, "original-bug-reports.json").toFile();
                FileUtils.writeLines(outFile, lines);
            }

            //write the bugs and their versions
            File outFileBugsVersions =
                    Paths.get(outputDirectory.getAbsolutePath(), "bug-reports-versions.csv").toFile();
            List<List<String>> bugsWithVersions2 = bugsWithVersionsIndexed.values()
                    .stream()
                    .flatMap(Collection::stream)
                    .map(p -> Arrays.asList(p.left, project + "-" + p.right))
                    .collect(Collectors.toList());
            CSVHelper.writeCsv(outFileBugsVersions, null, bugsWithVersions2, null, Function.identity(),
                    CSVHelper.DEFAULT_SEPARATOR);

        }
    }

    private static List<SourceFileText> getCorpus(String codeVersion,
                                                  File outputDirectory) throws IOException {
        File corpusOutFile = Paths.get(outputDirectory.toString(),
                codeVersion, "original-source-code.json").toFile();

        return CodeCorpusFormatter.getSourceFilesStream(corpusOutFile).collect(Collectors.toList());
    }


    private static class BugConverter {
        private final String project;
        private Bug bug;
        private String bugKey;
        private List<SourceFileText> corpus;

        public BugConverter(String project, Bug bug, String bugKey, List<SourceFileText> corpus) {
            this.project = project;
            this.bug = bug;
            this.bugKey = bugKey;
            this.corpus = corpus;
        }

        private BugReport convertToOurFormat() {

            final List<String> fixedFileList = bug.getFixedFileList();
            final List<String> fixedFilesInCorpus = findFixedFilesInCorpus(fixedFileList, corpus);
            if (fixedFilesInCorpus.isEmpty()) {
                LOGGER.debug(bugKey + ": Fixed files not found in corpus");
                return null;
            }

            BugReport bugReport = new BugReport(project + "-" + bug.getId(), bug.getSummary(), bug.getDescription());
            bugReport.setFixedFiles(fixedFilesInCorpus);

            final String openDate = bug.getOpenDate();
            final String fixDate = bug.getFixDate();

            bugReport.setCreationDate(Bug.DATE_FORMATTER.parseDateTime(openDate));
            bugReport.setResolutionDate(Bug.DATE_FORMATTER.parseDateTime(fixDate));

            return bugReport;
        }

        private List<String> findFixedFilesInCorpus(List<String> fixedFileList, List<SourceFileText> corpus) {

            List<String> fixedFiles = new ArrayList<>();

            //for each file
            for (String fixedFile : fixedFileList) {

                //try find the file
                List<SourceFileText> filesFound = findFixedFiles(fixedFile, corpus);

                //not found
                if (filesFound.isEmpty()) {
                    LOGGER.warn(bugKey + ": Couldn't find file in corpus: " + fixedFile);
//                throw new RuntimeException("Couldn't find file in corpus: " + fixedFile);
                    continue;
                }

                //found!
                if (filesFound.size() > 1) {
                    LOGGER.warn(bugKey + ": Found multiple files in corpus (taking the 1st): " + filesFound);
                }
                fixedFiles.add(filesFound.get(0).getFilePackagePath());
            }
            return fixedFiles;
        }

        private List<SourceFileText> findFixedFiles(String fixedFile, List<SourceFileText> corpus) {
            return corpus.stream()
                    .filter(sf -> CodeCorpusFormatter.getFilePathFormatted(sf).endsWith(fixedFile))
                    .collect(Collectors.toList());
        }


        public BugReport invoke() {
            return convertToOurFormat();
        }
    }
}
