package seers.disqueryreform.corpus;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;

public class BugExecutor extends ThreadProcessor {

	@SuppressWarnings("rawtypes")
	private List<List> bugs;

	public BugExecutor(ThreadParameters params) {
		super(params);
		bugs = params.getListParam(List.class, ThreadExecutor.ELEMENTS_PARAM);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void executeJob() throws Exception {

		// for each bug
		for (List<String> bug : bugs) {

			try {
				// get data
				String system = bug.get(0);
				String d4jId = bug.get(1);
				String bugId = bug.get(2);
				String srcDir = bug.get(3).substring(1, bug.get(3).length() - 1);

				// read all java files
				String projectBaseDir = D4JVocabExtractorMain.bugsSrcFolder + File.separator + system + "_" + d4jId;
				File projectDir = new File(projectBaseDir + File.separator + srcDir);
				LinkedList<File> allFiles = (LinkedList<File>) FileUtils.listFiles(projectDir, new String[] { "java" },
						true);

				LOGGER.debug(
						"Processing " + system + " - " + d4jId + " - " + bugId + ": # files -> " + allFiles.size());

				// out file
				File outFile = new File(
						D4JVocabExtractorMain.outputDir + File.separator + system + "_" + bugId + ".json");
				if (outFile.exists()) {
					boolean delete = outFile.delete();
					if (!delete) {
						throw new RuntimeException("OUtput file not deleted");
					}
				}

				// paginate the files
				ThreadParameters params2 = new ThreadParameters();
				params2.addParam("outFile", outFile);
				params2.addParam("baseFolder", projectBaseDir);
				params2.addParam("classPaths", new String[] { projectDir.getAbsolutePath() });
				params2.addParam("sourceFolders", new String[] { srcDir });
				params2.addParam("sequence", new AtomicInteger(1));

				ThreadExecutor.executePaginated(allFiles, BugFileExecutor.class, params2, 50, 20);
			} catch (Exception e) {
				LOGGER.error("Error for: " + bug, e);
			}

		}
	}

}
