package seers.disqueryreform.results;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumReformulatedQueriesStatsTest {

    @Test
    public void getBugId() {
        String line ="{\"id\":44,\"bug_id\":\"MATH-718\",\"text\":\"Following code will be reproduce the problem.   " +
                "This returns 499525, though it should be 499999.\",\"method_goldset\":[\"org.apache.commons.math3" +
                ".util.ContinuedFraction:evaluate(double, double, int)\"],\"creation_date\":\"2011-12-3 18:40:44\"," +
                "\"resolution_date\":\"2012-5-21 14:56:36\"}";
        String bugId = NumReformulatedQueriesStats.getBugId(line);
        System.out.println(bugId);
    }
}