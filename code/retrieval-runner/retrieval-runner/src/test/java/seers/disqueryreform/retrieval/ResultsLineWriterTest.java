package seers.disqueryreform.retrieval;

import com.google.gson.Gson;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ResultsLineWriterTest {
    private static final Path TESTING_PATH = Paths.get(FileUtils.getTempDirectoryPath()).resolve("ResultsLineWriterTesting");

    @Before
    public void setUp() throws FileExistsException {
        File testingDir = TESTING_PATH.toFile();

        if (testingDir.exists()) {
            throw new FileExistsException("Testing directory already exists: " + testingDir.getAbsolutePath());
        }
    }

    @After
    public void tearDown() throws IOException {
        FileUtils.deleteDirectory(TESTING_PATH.toFile());
    }

    /**
     * Tests that the output directory is created and no already executed keys are loaded.
     *
     * @throws IOException If something fails.
     */
    @Test
    public void testFromScratch() throws IOException {
        ResultsLineWriter writer = createNewTarget();
        writer.close();

        assertThat(TESTING_PATH.toFile().exists())
                .isTrue()
                .describedAs("Builder method failed to create output directory");

        assertThat(writer.getAlreadyExecutedKeys()).isEmpty();

        File[] files = Optional.ofNullable(TESTING_PATH.toFile().listFiles(f -> f.getName().startsWith(ResultsLineWriter.RESULTS_PART_PREFIX)))
                .orElse(new File[0]);

        assertThat(files.length).isEqualTo(1)
                .describedAs("There should be a single part file");

        List<String> lines = FileUtils.readLines(files[0], "utf-8");

        assertThat(lines).isEmpty();
    }

    @Test
    @Parameters
    public void testReadAlreadyExecutedKeys(Set<QueryResultKey> expected) throws IOException {
        writeKeys(TESTING_PATH.resolve(ResultsLineWriter.RESULTS_PART_PREFIX + "1.txt"), expected);

        Set<QueryResultKey> actual = createNewTarget().getAlreadyExecutedKeys();

        if (expected.isEmpty()) {
            assertThat(actual).isEmpty();
        } else {
            assertThat(actual).containsOnly(expected.toArray(new QueryResultKey[0]))
                    .describedAs("Already executed query keys loaded do not correspond to actual keys");
        }
    }

    private void writeKeys(Path filePath, Collection<QueryResultKey> keys) throws IOException {
        Gson gson = RetrievalResults.createGson();

        FileUtils.writeLines(
                filePath.toFile(),
                keys.stream()
                        .map(k -> gson.toJson(new RetrievalResults(k)))
                        .collect(Collectors.toList())
        );
    }

    public Object[] parametersForTestReadAlreadyExecutedKeys() {
        QueryResultKey key1 = QueryResultKey.fromString("BRT__project1;PROJ-1;strategy1;;LUCENE");
        QueryResultKey key2 = QueryResultKey.fromString("LB__project1;PROJ-2;strategy2;config1;LOBSTER");

        return Stream.of(
                new HashSet<>(),
                new HashSet<>(Collections.singletonList(key1)),
                new HashSet<>(Arrays.asList(
                        key1,
                        key2
                )),
                new HashSet<>(Arrays.asList(
                        key1,
                        key2,
                        QueryResultKey.fromString("B4BL__project2;22541258;strategy3;;LOCUS")
                ))
        )
                .map(s -> new Object[]{s})
                .toArray();
    }

    @Test
    @Parameters(method = "createMultipleResultParts")
    public void testLoadMultipleParts(List<List<QueryResultKey>> parts) throws IOException {
        writeResultParts(parts);

        ResultsLineWriter target = createNewTarget();

        assertThat(target.getAlreadyExecutedKeys()).containsOnly(
                parts.stream()
                        .flatMap(Collection::stream)
                        .distinct()
                        .toArray(QueryResultKey[]::new));
    }

    private void writeResultParts(List<List<QueryResultKey>> lists) throws IOException {
        for (int i = 0; i < lists.size(); i++) {
            List<QueryResultKey> part = lists.get(i);
            writeKeys(TESTING_PATH.resolve(ResultsLineWriter.RESULTS_PART_PREFIX + (i + 1) + ".txt"), part);
        }
    }

    public Object[] createMultipleResultParts() {
        List<QueryResultKey> part1 = createKeyList("BRT__project1", "strategy1", "config1", RetrievalTechnique.BUG_LOCATOR);
        List<QueryResultKey> part2 = createKeyList("QQ__project2", "strategy2", "", RetrievalTechnique.LUCENE);
        List<QueryResultKey> part3 = createKeyList("D4J__project1", "strategy1", "config2", RetrievalTechnique.LUCENE);
        List<QueryResultKey> part4 = createKeyList("LB__project3", "strategy3", "", RetrievalTechnique.LOBSTER);

        List<QueryResultKey> parts1And2 = Stream.concat(part1.stream(), part2.stream()).collect(Collectors.toList());
        List<QueryResultKey> parts1And2And3 = Stream.concat(parts1And2.stream(), part3.stream()).collect(Collectors.toList());

        return new Object[]{
                new Object[]{Arrays.asList(part1, part2)},
                new Object[]{Arrays.asList(part1, part2, part3)},
                new Object[]{Arrays.asList(parts1And2, part3)},
                new Object[]{Arrays.asList(part1, part2, part3, part4)},
                new Object[]{Arrays.asList(parts1And2And3, part4)}
        };
    }

    @Test
    @Parameters(method = "createMultipleResultParts")
    public void testStartOnNextPart(List<List<QueryResultKey>> parts) throws IOException {
        writeResultParts(parts);

        ResultsLineWriter target = createNewTarget();

        File nextFile = TESTING_PATH
                .resolve(ResultsLineWriter.RESULTS_PART_PREFIX + (parts.size() + 1) + ".txt")
                .toFile();

        assertThat(nextFile.exists()).isTrue()
                .describedAs("The next file being written should be " + nextFile.getName());

        assertThat(FileUtils.readLines(nextFile, "utf-8")).isEmpty();
    }

    @Test
    @Parameters
    public void testSwitchToNextPart(List<QueryResultKey> keys, int partLength) throws IOException {
        ResultsLineWriter target = ResultsLineWriter.createResultsLineWriter(TESTING_PATH, partLength);

        for (QueryResultKey key : keys) {
            target.write(new RetrievalResults(key));
        }

        target.close();

        File[] parts = Objects.requireNonNull(TESTING_PATH.toFile().listFiles(f -> f.getName().endsWith(".txt")));

        int actualParts = parts.length;

        // Plus 1 because it might switch to the next file upon writing the last result
        int expectedParts = (int) Math.ceil((double) (keys.size() + 1) / partLength);

        assertThat(actualParts).isEqualTo(expectedParts)
                .describedAs("The amount of parts written does not match the expected");

        Gson gson = RetrievalResults.createGson();

        List<QueryResultKey> savedResults = Arrays.stream(parts)
                .flatMap(f -> {
                    try {
                        return FileUtils.readLines(f, "utf-8").stream()
                                .map(l -> gson.fromJson(l, RetrievalResults.class));
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .map(RetrievalResults::getKey)
                .collect(Collectors.toList());

        assertThat(savedResults).containsOnly(keys.toArray(new QueryResultKey[0]));
    }

    public Object[] parametersForTestSwitchToNextPart() {
        List<QueryResultKey> keys = createKeyList("BRT__project", "strategy", "config", RetrievalTechnique.LUCENE);
        return new Object[]{
                new Object[]{keys, 10},
                new Object[]{keys, 11},
                new Object[]{keys, 50},
                new Object[]{keys, 99},
                new Object[]{keys, 100},
                new Object[]{keys, 200}
        };
    }

    private List<QueryResultKey> createKeyList(String project, String strategy, String config, RetrievalTechnique approach) {
        return IntStream.rangeClosed(1, 100)
                .boxed()
                .map(i -> QueryResultKey.fromString(String.format("%s;%d;%s;%s;%s", project, i, strategy, config, approach)))
                .collect(Collectors.toList());
    }

    private ResultsLineWriter createNewTarget() throws IOException {
        return ResultsLineWriter.createResultsLineWriter(TESTING_PATH);
    }
}