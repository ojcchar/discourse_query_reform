package seers.disqueryreform.evaluation;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;
import org.apache.commons.io.FileUtils;
import seers.appcore.csv.CSVHelper;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.stats.EvaluationStatsTable;
import seers.disqueryreform.evaluation.stats.ReformulationAccumulationGroup;
import seers.disqueryreform.evaluation.stats.ReformulationEvaluation;
import seers.disqueryreform.evaluation.stats.output.StatsTableWriter;
import seers.disqueryreform.retrieval.QueryResultKey;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Used to compare with the old implementation of the evaluation.
 */
public class ComparisonThresholdEvaluator extends ThresholdEvaluator {

    private final Path overallStatsFilePath;

    private ComparisonThresholdEvaluator(Path overallStatsFilePath) {
        super(null, null, null, null, StrategyType.COMBINATORIAL);
        this.overallStatsFilePath = overallStatsFilePath;
    }

    public static void main(String[] args) throws IOException {
        Path overallStatsFilePath = Paths.get(args[0]);
        Path outputDirPath = Paths.get(args[1]);

        FileUtils.forceMkdir(outputDirPath.toFile());

        EvaluationStatsTable table =
                EvaluationStatsTable.accumulateAllResults(new ComparisonThresholdEvaluator(overallStatsFilePath));

        ReformulationAccumulationGroup gran = table.groupByGranularity();

        table.allStatsByTechniqueAndStrategy().forEach(c -> {
            RetrievalTechnique technique = c.getTechnique();
            Path outputFilePath = outputDirPath.resolve(technique + "-stats.csv");

            new StatsTableWriter(gran, technique, null, null, false)
                    .writeTable(c.allEntries(), outputFilePath);
        });
    }

    private Integer threshold(List<String> l) {
        return Integer.valueOf(l.get(3));
    }

    private String key(List<String> l) {
        return l.get(6);
    }

    private RetrievalTechnique technique(List<String> l) {
        RetrievalTechnique t;
        switch (l.get(1)) {
            case "locus":
                t = RetrievalTechnique.LOCUS;
                break;
            case "lucene":
                t = RetrievalTechnique.LUCENE;
                break;
            case "lobster":
                t = RetrievalTechnique.LOBSTER;
                break;
            case "buglocator":
                t = RetrievalTechnique.BUG_LOCATOR;
                break;
            case "brtracer":
                t = RetrievalTechnique.BR_TRACER;
                break;
            default:
                throw new Error();
        }
        return t;
    }

    private Project project(List<String> l) {
        return Project.fromString(l.get(0).toUpperCase() + "__" + l.get(4));
    }

    private Map<String, Double> findBaselineAPs() {
        List<List<String>> csvLines;
        try {
            csvLines = CSVHelper.readCsv(overallStatsFilePath.toString(), true);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return csvLines.stream()
                .filter(l -> l.get(7).equals("ALL_TEXT") && !l.get(3).startsWith("no"))
                .collect(Collectors.toMap(
                        this::baselineKey,
                        l -> Double.valueOf(l.get(10)),
                        (l1, l2) -> l1
                ));
    }

    private String baselineKey(List<String> l) {
        return Stream.of(
                technique(l),
                project(l),
                key(l),
                threshold(l)
        )
                .map(String::valueOf)
                .collect(Collectors.joining(";"));
    }

    @Override
    public Stream<ReformulationEvaluation> thresholdEvaluations() {
        List<List<String>> csvLines;
        try {
            csvLines = CSVHelper.readCsv(overallStatsFilePath.toString(), true);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        Map<String, Double> baselineAP = findBaselineAPs();

        return csvLines.stream()
                .filter(l -> !l.get(7).equals("ALL_TEXT") && !l.get(3).startsWith("no"))
                .map(l -> {
                    Project project = project(l);
                    String key = key(l);
                    DiscourseQuery q = new DiscourseQuery(key, l.get(2), null, null, null);
                    RetrievalTechnique t = technique(l);
                    QueryResultKey k = new QueryResultKey(project, q, t);

                    int thresh = threshold(l);
                    int rank = Double.valueOf(l.get(8)).intValue();
                    int rankDiff = Integer.valueOf(l.get(9));
                    double ap = Double.valueOf(l.get(10));
                    RetrievalEvaluation r = new RetrievalEvaluation(thresh, rank, ap);
                    RetrievalEvaluation bl = new RetrievalEvaluation(thresh, rank + rankDiff, baselineAP.get(baselineKey(l)));
                    return new ReformulationEvaluation(k, bl, r, Collections.emptySet());
                });
    }
}