package seers.disqueryreform.retrieval;

import org.junit.Test;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;

import java.nio.file.Path;
import java.util.List;

public class ApproachRetrieverTest {
    /**
     * Tests that only queries that are not already run and have valid text are actually run.
     */
    @Test
    public void testInitialFiltering() {
        // TODO: implement and maybe look into mocking
    }

    private class DummyApproachRetriever extends ApproachRetriever {
        private List<DiscourseQuery> validQueries;

        public DummyApproachRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath, RetrievalTechnique approach, boolean loadCorpusDocs) {
            super(resultsWriter, existingDataSetsPath, approach, loadCorpusDocs);
        }

        @Override
        protected void runApproach(Project project, List<DiscourseQuery> queries) {
            validQueries = queries;
        }

        @Override
        protected void runForCorpus(Corpus corpus) {

        }
    }

//    TODO: test load corpora
}