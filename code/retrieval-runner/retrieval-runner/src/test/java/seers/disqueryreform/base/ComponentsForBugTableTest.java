package seers.disqueryreform.base;

import com.google.common.collect.Sets;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.math3.util.Pair;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ComponentsForBugTableTest {
    // FIXME this is a very bad test, we're testing the object's properties directly instead of through its public interface
    @Test
    @Parameters
    public void testCreation(List<StrategyQueryContainer> containers,
                             Set<String> expectedComponents,
                             Map<Pair<Project, String>, Set<String>> expectedComponentsForQuery) {
        ComponentsForBugTable target = new ComponentsForBugTable(containers, 0);

        System.out.println("Actual allComponents: " + target.getAllComponents());
        System.out.println("Actual compForQuery: " + target.getAllEntries());

        assertThat(target.getAllComponents())
                .containsOnly(expectedComponents.toArray(new String[0]));

        assertThat(target.getAllEntries())
                .isEqualTo(expectedComponentsForQuery);
    }

    public Object[] parametersForTestCreation() {
        return new Object[]{
                createCase(createQuery("1", "OB", null)),
                createCase(
                        createQuery("1", "OB", null),
                        createQuery("2", "OB", null),
                        createQuery("3", "OB", null)
                ),
                createCase(
                        createQuery("1", "OB", null),
                        createQuery("2", "EB", null),
                        createQuery("3", "CODE", null)
                ),
                createCase(
                        createQuery("1", "T_F_OTHER", "true-true"),
                        createQuery("1", "T_F_OTHER", "false-true"),
                        createQuery("1", "T_F_OTHER", "false-false"),
                        createQuery("1", "T_F_OTHER", "true-false")
                ),
                createCase(
                        createQuery("1", "OB--T_F_OTHER", "true-true"),
                        createQuery("2", "OB--T_F_OTHER", "true-true"),
                        createQuery("1", "OB", null),
                        createQuery("1", "T_F_OTHER", "true-true")
                ),
                createCase(
                        createQuery("1", "CODE--T_F_OTHER", "true-true"),
                        createQuery("1", "T_F_OTHER", "true-true"),
                        createQuery("2", "CODE--T_F_OTHER", "true-false")
                ),
                createCase(
                        createQuery("1", "T_O_TITLE--T_V_OB--T_VO_OTHER", "true-true"),
                        createQuery("1", "T_V_OB", "true-true"),
                        createQuery("1", "T_O_TITLE", "true-true"),
                        createQuery("1", "T_VO_OTHER", "true-true"),
                        createQuery("1", "T_O_TITLE--T_V_OB--T_VO_OTHER", "true-false"),
                        createQuery("1", "T_O_TITLE--T_V_OB--T_VO_OTHER", "false-true"),
                        createQuery("1", "T_O_TITLE--T_V_OB--T_VO_OTHER", "false-false")
                ),
                createCase(
                        createQuery("1", "EB", null),
                        createQuery("1", "T_O_TITLE--T_V_OB--T_VO_OTHER", "true-true"),
                        createQuery("1", "T_F_OTHER", "true-false"),
                        createQuery("1", "T_F_OTHER", "true-true"),
                        createQuery("2", "T_F_OTHER", "true-false"),
                        createQuery("3", "T_F_OTHER", "true-false"),
                        createQuery("3", "EB", null)
                )
        };
    }

    private Pair<Project, DiscourseQuery> createQuery(String key, String strategy, String config) {
        Project p = new Project(DataSet.B4BL, "p1");
        return new Pair<>(p, new DiscourseQuery(key, strategy, config, "title", "desc"));
    }

    // FIXME this could be made nicer if project was in discourse query and tasknav config was in strategy
    @SafeVarargs
    private final Object[] createCase(Pair<Project, DiscourseQuery>... queries) {
        Set<String> expected = new HashSet<>();
        Map<Pair<Project, String>, Set<String>> compForQuery = new HashMap<>();

        List<StrategyQueryContainer> cs = Arrays.stream(queries)
                .collect(Collectors.groupingBy(p -> p.getValue().getStrategy()))
                .entrySet().stream()
                .filter(e -> DiscourseTaskStrategy.splitIntoComponentNames(e.getKey()).size() == 1)
                .map(Map.Entry::getValue)
                .map(l -> {
                    String strategyName = l.get(0).getValue().getStrategy();
                    StrategyQueryContainer container = new StrategyQueryContainer(strategyName);
                    String[] comps = strategyName.split("--");
                    for (Pair<Project, DiscourseQuery> p : l) {
                        DiscourseQuery query = p.getValue();
                        container.addQuery(p.getKey().getDataSet(), p.getKey().getProjectName(), query);
                        for (String component : comps) {
                            String fullComp;

                            if (component.startsWith("T_")) {
                                fullComp = component + "--" + query.getTaskNavConfiguration();
                            } else {
                                fullComp = component + "--NONE";
                            }

                            expected.add(fullComp);
                            compForQuery.computeIfAbsent(new Pair<>(p.getKey(), query.getKey()), k -> new HashSet<>())
                                    .add(fullComp);
                        }
                    }
                    return container;
                })
                .collect(Collectors.toList());

        return new Object[]{cs, expected, compForQuery};
    }

    @Test
    @Parameters
    public void testStrategiesForQuery(List<String> otherComponents, List<String> queryComponents, String resultStrategy, String resultConfig) {
        List<DiscourseQuery> queries = queryComponents.stream()
                .map(c -> {
                    String[] split = c.split("--");
                    String config = split.length > 1 ? split[1] : null;

                    return new DiscourseQuery("1", split[0], config, "a", "b");
                })
                .collect(Collectors.toList());

        List<DiscourseQuery> otherQueries = otherComponents.stream()
                .map(c -> {
                    String[] split = c.split("--");
                    String config = split.length > 1 ? split[1] : null;

                    return new DiscourseQuery("2", split[0], config, "a", "b");
                })
                .collect(Collectors.toList());

        List<StrategyQueryContainer> containers = Stream.of(queries, otherQueries).flatMap(Collection::stream)
                .collect(Collectors.groupingBy(DiscourseQuery::getStrategy))
                .values().stream()
                .map(l -> {
                    StrategyQueryContainer c = new StrategyQueryContainer(l.get(0).getStrategy());
                    c.addProject(DataSet.B4BL, "p1", l);
                    return c;
                })
                .collect(Collectors.toList());

        ComponentsForBugTable table = new ComponentsForBugTable(containers, 0);

        Optional<Set<String>> actual = table.findStrategiesForQuery(
                new Project(DataSet.B4BL, "p1"),
                "1",
                resultStrategy,
                resultConfig,
                StrategyType.COMBINATORIAL
        );

        System.out.println("Actual: " + actual.get());


        Set<String> allCs = Stream.of(otherComponents, queryComponents).flatMap(Collection::stream).collect(Collectors.toSet());

        Set<Set<String>> allStrats = ComponentsForBugTable.generateAllComponentCombinations(allCs);

        @SuppressWarnings("unchecked")
        String[] expected = allStrats.stream()
                .filter(s -> {
                    Set<String> sSet = s.stream().map(c -> {
                        String[] split = c.split("--");
                        return split[1].equals("NONE") ? split[0] : c;
                    }).collect(Collectors.toSet());

                    HashSet<String> qSet = new HashSet<>(queryComponents);

                    Set<String> intersection = Sets.intersection(sSet, qSet).stream()
                            .map(n -> n.split("--")[0])
                            .collect(Collectors.toSet());

                    Set<String> rStrat = DiscourseTaskStrategy.splitIntoComponentNames(resultStrategy)
                            .stream()
                            .filter(a -> !a.equals("ALL_TEXT"))
                            .collect(Collectors.toSet());

                    return intersection.equals(rStrat);
                })
                .map(l -> {
                    String conf = null;
                    for (String s : l) {
                        String[] split = s.split("--");
                        if (split.length > 1 && !split[1].equals("NONE")) {
                            conf = split[1];
                        }
                    }

                    List<BugReportComponent> components = l.stream()
                            .map(s -> s.split("--")[0])
                            .map(BugReportComponent::valueOf).collect(Collectors.toList());

                    String strategyName = new DiscourseTaskStrategy(components).toString();

                    return DiscourseQuery.generateFullStrategyName(strategyName, conf);
                })
                .toArray(String[]::new);

        if (expected.length != 0) {
            Assertions.assertThat(actual.get())
                    .containsOnly(expected);
        } else {
            Assertions.assertThat(actual.get())
                    .isEmpty();
        }
    }

    public Object[] parametersForTestStrategiesForQuery() {
        return new Object[]{
                // Only one component and query has it
                new Object[]{Collections.emptyList(), Collections.singletonList("OB"), "OB", null},
                new Object[]{Collections.emptyList(), Collections.singletonList("CODE"), "CODE", null},
                new Object[]{Collections.emptyList(), Collections.singletonList("T_F_OB--true-true"), "T_F_OB", "true-true"},
                new Object[]{Collections.emptyList(), Collections.singletonList("T_F_OTHER--true-true"), "T_F_OTHER", "true-true"},
                // Two components and query only has one
                new Object[]{Collections.singletonList("EB"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Collections.singletonList("T_F_OB--true-true"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Collections.singletonList("OB"), Collections.singletonList("T_O_OB--true-true"), "T_O_OB", "true-true"},
                new Object[]{Collections.singletonList("CODE"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Collections.singletonList("T_F_EB--true-true"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Collections.singletonList("T_F_S2R--true-true"), Collections.singletonList("T_F_OB--true-true"), "T_F_OB", "true-true"},
                new Object[]{Collections.singletonList("T_F_S2R--true-false"), Collections.singletonList("T_F_OB--true-true"), "T_F_OB", "true-true"},
                new Object[]{Collections.singletonList("T_F_OTHER--true-true"), Collections.singletonList("T_F_OB--true-true"), "T_F_OB", "true-true"},
                new Object[]{Collections.singletonList("T_F_OTHER--true-false"), Collections.singletonList("T_F_OB--true-true"), "T_F_OB", "true-true"},
                new Object[]{Collections.singletonList("TITLE"), Collections.singletonList("T_F_OB--true-true"), "T_F_OB", "true-true"},
                // Three components and query only has one
                new Object[]{Arrays.asList("EB", "CODE"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Arrays.asList("T_O_OB--true-true", "CODE"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Arrays.asList("T_O_EB--true-true", "T_V_OTHER--true-true"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Arrays.asList("T_O_EB--false-false", "T_V_OTHER--true-true"), Collections.singletonList("OB"), "OB", null},
                new Object[]{Arrays.asList("TITLE", "CODE"), Collections.singletonList("T_VO_EB--true-true"), "T_VO_EB", "true-true"},
                new Object[]{Arrays.asList("T_F_TITLE--true-true", "CODE"), Collections.singletonList("T_VO_EB--true-true"), "T_VO_EB", "true-true"},
                new Object[]{Arrays.asList("T_F_TITLE--true-true", "T_F_OTHER--false-false"), Collections.singletonList("T_VO_EB--true-true"), "T_VO_EB", "true-true"},
                // Two components and query has both
                new Object[]{Collections.emptyList(), Arrays.asList("OB", "EB"), "OB", null},
                new Object[]{Collections.emptyList(), Arrays.asList("OB", "EB"), "EB", null},
                new Object[]{Collections.emptyList(), Arrays.asList("OB", "EB"), "EB--OB", null},
                new Object[]{
                        Collections.emptyList(),
                        Arrays.asList("T_F_OTHER--true-true", "T_V_OB--true-true"),
                        "T_F_OTHER", "true-true"
                },
                new Object[]{
                        Collections.emptyList(),
                        Arrays.asList("T_F_OTHER--true-true", "T_V_OB--true-true"),
                        "T_V_OB", "true-true"
                },
                new Object[]{
                        Collections.emptyList(),
                        Arrays.asList("T_F_OTHER--true-true", "T_V_OB--true-true"),
                        "T_V_OB--T_F_OTHER", "true-true"
                },
                new Object[]{
                        Collections.emptyList(),
                        Arrays.asList("T_F_OTHER--true-true", "CODE"),
                        "CODE", null
                },
                new Object[]{
                        Collections.emptyList(),
                        Arrays.asList("T_F_OTHER--true-true", "CODE"),
                        "T_F_OTHER", "true-true"
                },
                // All text
                new Object[]{Collections.emptyList(), Collections.singletonList("OB"), "ALL_TEXT", null},
                new Object[]{Collections.singletonList("EB"), Collections.singletonList("OB"), "ALL_TEXT", null},
                new Object[]{Arrays.asList("EB", "CODE"), Collections.singletonList("OB"), "ALL_TEXT", null},
                new Object[]{Arrays.asList("EB", "CODE", "T_VO_OB--false-false"), Collections.singletonList("OB"), "ALL_TEXT", null},
                new Object[]{Arrays.asList("T_F_OTHER--true-true", "CODE", "T_VO_OB--false-false"), Collections.singletonList("T_F_EB--false-true"), "ALL_TEXT", null},
        };
    }
}