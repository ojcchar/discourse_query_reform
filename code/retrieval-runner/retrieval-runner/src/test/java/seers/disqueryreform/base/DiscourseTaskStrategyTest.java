package seers.disqueryreform.base;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Assertions.assertThat;

public class DiscourseTaskStrategyTest {
    @Test
    public void testGenerateDiscourseStrategies() {
        Set<DiscourseTaskStrategy> strategies = DiscourseTaskStrategy.generateDiscourseStrategies();

        assertValid(strategies, 31, BugReportComponent.DISCOURSE_COMPONENTS);
    }

    @Test
    public void testGenerateDiscourseTaskStrategies() {
        Set<DiscourseTaskStrategy> strategies =
                DiscourseTaskStrategy.generateDiscourseTaskStrategies();

        assertValid(strategies, 12_928, Sets.union(
                BugReportComponent.DISCOURSE_COMPONENTS,
                BugReportComponent.TASK_COMPONENTS
        ));
    }

    @Test
    public void testGenerateAllOtherStrategies() {
        Set<DiscourseTaskStrategy> strategies = DiscourseTaskStrategy.generateAllOtherStrategies();

        assertValid(strategies, 162, Sets.newHashSet(
                BugReportComponent.OB, BugReportComponent.T_F_OB,
                BugReportComponent.TITLE, BugReportComponent.T_F_TITLE,
                BugReportComponent.EB, BugReportComponent.T_F_EB,
                BugReportComponent.S2R, BugReportComponent.T_F_S2R,
                BugReportComponent.CODE,
                BugReportComponent.OTHER
        ));
    }

    @Test
    public void testGenerateSimplifiedStrategies() {
        Set<DiscourseTaskStrategy> strategies = DiscourseTaskStrategy.generateSimplifiedStrategies();

        assertValid(strategies, 292, Sets.newHashSet(
                BugReportComponent.OB, BugReportComponent.T_F_OB,
                BugReportComponent.TITLE, BugReportComponent.T_F_TITLE,
                BugReportComponent.EB, BugReportComponent.T_F_EB,
                BugReportComponent.S2R, BugReportComponent.T_F_S2R,
                BugReportComponent.CODE,
                BugReportComponent.T_F_OTHER
        ));
    }

    private void assertValid(Set<DiscourseTaskStrategy> strategies, int expectedSize, Collection<BugReportComponent> expectedComponents) {
        Set<BugReportComponent> actualComponents = strategies.stream()
                .flatMap(s -> s.getComponents().stream())
                .collect(Collectors.toSet());

        assertThat(actualComponents)
                .containsOnly(expectedComponents.toArray(new BugReportComponent[0]));

        assertThat(new HashSet<>(strategies))
                .describedAs("Strategies must be distinct")
                .containsOnly(strategies.toArray(new DiscourseTaskStrategy[0]));

        assertThat(strategies.size())
                .describedAs("Amount of strategies")
                .isEqualTo(expectedSize);
    }
}