package seers.disqueryreform.evaluation.stats.keys;

import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.Objects;

public class GTSKey {

    private final DocumentGranularity granularity;
    private final RetrievalTechnique technique;
    private final String strategy;

    GTSKey(DocumentGranularity granularity, RetrievalTechnique technique, String strategy) {
        this.granularity = granularity;
        this.technique = technique;
        this.strategy = strategy;
    }

    public GranularityTechniquePair removeStrategy() {
        return new GranularityTechniquePair(granularity, technique);
    }

    public GTSKey withStrategy(String strategy) {
        return new GTSKey(granularity, technique, strategy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GTSKey gtsKey = (GTSKey) o;
        return granularity == gtsKey.granularity &&
                technique == gtsKey.technique &&
                strategy.equals(gtsKey.strategy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(granularity, technique, strategy);
    }

    public DocumentGranularity getGranularity() {
        return granularity;
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }

    public String getStrategy() {
        return strategy;
    }
}
