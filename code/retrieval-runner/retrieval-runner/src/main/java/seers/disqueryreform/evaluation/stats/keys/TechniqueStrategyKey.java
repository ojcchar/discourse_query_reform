package seers.disqueryreform.evaluation.stats.keys;

import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.Objects;

public class TechniqueStrategyKey {
    private final RetrievalTechnique technique;
    private final String strategy;

    public TechniqueStrategyKey(RetrievalTechnique technique, String strategy) {
        this.technique = technique;
        this.strategy = strategy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechniqueStrategyKey that = (TechniqueStrategyKey) o;
        return technique == that.technique &&
                strategy.equals(that.strategy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(technique, strategy);
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }

    public String getStrategy() {
        return strategy;
    }
}
