package seers.disqueryreform.evaluation.stats.keys;

import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.Objects;

public class GranularityTechniquePair {

    private final DocumentGranularity granularity;
    private final RetrievalTechnique technique;

    GranularityTechniquePair(DocumentGranularity granularity, RetrievalTechnique technique) {
        this.granularity = granularity;
        this.technique = technique;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", granularity, technique);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GranularityTechniquePair that = (GranularityTechniquePair) o;
        return granularity == that.granularity &&
                technique == that.technique;
    }

    @Override
    public int hashCode() {
        return Objects.hash(granularity, technique);
    }

    public DocumentGranularity getGranularity() {
        return granularity;
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }
}
