package seers.disqueryreform.retrieval.lucene;

import edu.wayne.cs.severe.ir4se.processor.controllers.RetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.RetrievalWriter;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalWriter;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.RAMRetrievalIndexer;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.lucene.LuceneRetrievalSearcher;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalStats;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.RetrievalUtils;

import java.io.File;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
public class LuceneMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuceneMain.class);

    private static ConcurrentHashMap<String, List<RetrievalDoc>> systemCorpus = new ConcurrentHashMap<>();

    private static String statsFile;
    private static String baseDataFolder;
    private static String baseDataFolder2;
    private static String outputDir;
    private static DataSet dataSet;

    private static String corpusBaseFolder;

    public static void main(String[] args) throws Exception {

        baseDataFolder = args[0];
        outputDir = args[1];
        dataSet = DataSet.valueOf(args[2]);
        baseDataFolder2 = args[3];

        statsFile = outputDir + File.separator + dataSet.toString() + "-stats.csv";
        File file = new File(statsFile);
        if (file.exists()) {
            FileUtils.forceDelete(file);
        }

        corpusBaseFolder = DataSetUtils.getCorpusBaseFolder(baseDataFolder, baseDataFolder2, dataSet);

        LOGGER.debug("Reading queries...");

        List<Query> queries = RetrievalUtils.readAllQueries(baseDataFolder, dataSet);
        LOGGER.debug("# of queries: " + queries.size());

        ThreadParameters params = new ThreadParameters();
        List<ThreadProcessor> processors = ThreadExecutor.executePaginated(queries, LuceneProcessor2.class,
                params, 30, 10);

        // ---------------

        writeStats(processors);

        // ---------------

        LOGGER.debug("Done!");

    }

    public static class LuceneProcessor2 extends ThreadProcessor {

        private final List<Query> queries;
        Map<Query, List<Double>> queryEvals = new LinkedHashMap<>();


        public LuceneProcessor2(ThreadParameters params) {
            super(params);
            queries = params.getListParam(Query.class, ThreadExecutor.ELEMENTS_PARAM);
        }

        @Override
        public void executeJob() throws Exception {

            //for each query
            for (Query query : queries) {

                String project = (String) query.getInfoAttribute("system");

                //build the original query
                String projectBugId = project + ";" + query.getKey();
                LOGGER.debug("Processing " + projectBugId);

                //get the specific corpus for the query
                List<RetrievalDoc> queryCorpus = Collections.emptyList();
//                        RetrievalUtils.readCorpus(systemCorpus, corpusBaseFolder, dataSet,
//                        query, project);

                // evaluate the relevance judgments
                RelJudgment queryRelJudgment = RetrievalUtils.getRelevantJudgement(dataSet, query, queryCorpus);

                if (queryRelJudgment.getRelevantDocs().isEmpty()) {
                    LOGGER.error("No rel jud evaluation for query: " + projectBugId);
                    continue;
                }

                List<RetrievalDoc> originalQueryRetrievedDocs;

                try (Directory index = new RAMRetrievalIndexer().buildIndex(queryCorpus, null)) {
                    LuceneRetrievalSearcher searcher = new LuceneRetrievalSearcher(index, null);
                    originalQueryRetrievedDocs = searcher.searchQuery(query);
                }

                List<Double> queryMetrics = new DefaultRetrievalEvaluator().evaluateRelJudgment(queryRelJudgment,
                        originalQueryRetrievedDocs);

                queryEvals.put(query, queryMetrics);

            }

        }

        public Map<Query, List<Double>> getQueryEvals() {
            return queryEvals;
        }
    }

    private static void writeStats(List<ThreadProcessor> processors) throws Exception {
        Map<Query, List<Double>> allQueryEval = new LinkedHashMap<>();

        for (ThreadProcessor proc : processors) {
            LuceneProcessor2 processor = (LuceneProcessor2) proc;

            Map<Query, List<Double>> queryEvals = processor.getQueryEvals();
            allQueryEval.putAll(queryEvals);
        }

        RetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();
        RetrievalStats modelStats = evaluator.evaluateModel(allQueryEval);

        // write the results
        RetrievalWriter writer = new DefaultRetrievalWriter();
        writer.writeStats(modelStats, statsFile);

        LOGGER.debug("Stats written in " + statsFile);
    }


}
