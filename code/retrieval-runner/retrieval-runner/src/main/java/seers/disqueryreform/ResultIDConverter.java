package seers.disqueryreform;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.fest.assertions.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.*;
import seers.disqueryreform.querygen.tasknav.GoldSetAdder;
import seers.disqueryreform.retrieval.QueryResultKey;
import seers.disqueryreform.retrieval.RetrievalResults;
import seers.disqueryreform.retrieval.RetrievalTechnique;
import seers.disqueryreform.retrieval.bench4bl.locus.LocusRetrieverWrapper;
import seers.disqueryreform.retrieval.bl.brt.BLBRTRunner;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The IDs in the result-lines files are different between approaches, e.g. Locus and BugLocator.
 * This class converts the result list so that all IDs are uniform and creates look-up files.
 */
public class ResultIDConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResultIDConverter.class);

    private final Path rawResultsPath;
    private final IDLookupMap lookupMap;
    private final IDConversionMap conversionMap;

    /**
     * For checking of the results
     */
    private final GoldSetAdder goldSetLoader;

    private ResultIDConverter(Path baseRepositoryPath, Path rawResultsPath) throws IOException {
        this.rawResultsPath = rawResultsPath;

        Path existingDataSetsPath = baseRepositoryPath.resolve(Paths.get("data", "existing_data_sets"));
        lookupMap = new IDLookupMap(existingDataSetsPath, rawResultsPath);
        conversionMap = new IDConversionMap(existingDataSetsPath);
        goldSetLoader = new GoldSetAdder(existingDataSetsPath);
    }

    public static void main(String[] args) throws IOException {
        Path baseRepositoryPath = Paths.get(args[0]);
        Path rawResultsPath = Paths.get(args[1]);
        Path destinationPath = Paths.get(args[2]);

        new ResultIDConverter(baseRepositoryPath, rawResultsPath).convertIDs(destinationPath);
    }

    private void convertIDs(Path destinationPath) throws IOException {
        Gson gson = RetrievalResults.createGson();

        FileUtils.forceMkdir(destinationPath.toFile());

        try (Stream<String> convertedResults = Files.walk(rawResultsPath, 1)
                .filter(p -> p.getFileName().toString().startsWith("result-lines-"))
                .flatMap(p -> {
                    LOGGER.info("Loading file for converting: " + p);
                    try {
                        return Files.lines(p);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .map(l -> gson.fromJson(l, RetrievalResults.class))
                .map(this::convertResult)
                .peek(this::checkResults)
                .map(gson::toJson)) {

            Files.write(
                    destinationPath.resolve("results.json.txt"),
                    ((Iterable<String>) convertedResults::iterator)
            );
        }

        JSON.writeJSON(lookupMap.createLookupMap(), destinationPath.resolve("lookup-table.json"));
    }

    /**
     * Checks that the gold set in the converted results matches that of the original files.
     *
     * @param retrievalResults Converted results.
     */
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    private void checkResults(RetrievalResults retrievalResults) {
        QueryResultKey key = retrievalResults.getKey();

        if (key.getTechnique().equals(RetrievalTechnique.LOCUS)) {
            // Since LOCUS uses its own gold set, there is no point in checking. We instead output
            // a LOCUS_ corpus for each locus system in the output lookup table
            return;
        }

        List<String> originalGoldSet = goldSetLoader.getGoldSet(key.getProject(), key.getQueryKey());

        IDLookupMap.ProjectCorpora.LookupCorpus corpus = lookupMap.loadLookupForQuery(key);

        List<String> convertedGoldSet = retrievalResults.getRelevanceJudgment().get().getRelevantDocs().stream()
                .map(d -> corpus.inverseLookup(d.getDocId()))
                .collect(Collectors.toList());

        try {
            Assertions.assertThat(originalGoldSet)
                    .containsAll(convertedGoldSet);
        } catch (AssertionError e) {
            throw new RuntimeException("Error with query: " + key, e);
        }
    }

    private RetrievalResults convertResult(RetrievalResults results) {
        RetrievalTechnique technique = results.getKey().getTechnique();

        IDLookupMap.ProjectCorpora.LookupCorpus corpus = lookupMap.loadLookupForQuery(results.getKey());

        RetrievalResults finalResults;

        switch (technique) {
            case LUCENE:
            case LOBSTER:
            case LOCUS:
                finalResults = results;
                break;
            case BR_TRACER:
            case BUG_LOCATOR:
                finalResults = conversionMap.convert(results, corpus);
                break;
            default:
                throw new RuntimeException("Unknown technique: " + technique);
        }

        return finalResults;
    }

    private static class IDLookupMap {
        private final Path existingDataSetsPath;
        private final Map<String, String> b4blIDToProject;
        private final Map<String, String> eclipseSplits;
        private final Path rawResultsPath;
        Map<Project, ProjectCorpora> map = new HashMap<>();

        @SuppressWarnings("deprecation")
        IDLookupMap(Path existingDataSetsPath, Path rawResultsPath) {
            this.existingDataSetsPath = existingDataSetsPath;
            b4blIDToProject = BLBRTRunner.loadB4BLIDToProjectMap(DataSet.B4BL.resolveCorpusPath(existingDataSetsPath));
            eclipseSplits = LocusRetrieverWrapper.loadEclipseSplits(existingDataSetsPath);
            this.rawResultsPath = rawResultsPath;
        }

        ProjectCorpora.LookupCorpus loadLookupForQuery(QueryResultKey key) {
            ProjectCorpora projectCorpora = map.computeIfAbsent(key.getProject(), k -> new ProjectCorpora());

            return projectCorpora.loadForQuery(key, existingDataSetsPath, b4blIDToProject, eclipseSplits, rawResultsPath);
        }

        Map createLookupMap() {
            return map.entrySet().stream()
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            e -> e.getValue().toMap()
                    ));
        }

        private static class ProjectCorpora {
            Map<String, LookupCorpus> corpora = new HashMap<>();

            LookupCorpus loadForQuery(QueryResultKey key, Path existingDataSetsPath, Map<String, String> b4blIDToProject, Map<String, String> eclipseSplits, Path rawResultsPath) {
                String corpusID = computeCorpusID(key, b4blIDToProject, eclipseSplits);

                LookupCorpus corpus = corpora.computeIfAbsent(corpusID,
                        k -> LookupCorpus.load(corpusID, existingDataSetsPath, key, rawResultsPath));

                corpus.addQuery(key.getQueryKey());

                return corpus;
            }

            private String computeCorpusID(QueryResultKey key, Map<String, String> b4blIDToProject, Map<String, String> eclipseSplits) {
                String prefix;
                if (key.getTechnique().equals(RetrievalTechnique.LOCUS)) {
                    prefix = "LOCUS_";
                } else {
                    prefix = "";
                }

                // FIXME this duplicates logic in Approach Retriever and LocusRetrieverWrapper
                String id;
                switch (key.getProject().getDataSet()) {
                    case D4J:
                        // Corpus for each query
                        id = key.getProject().getProjectName() + "_" + key.getQueryKey();
                        break;
                    case B4BL:
                        // Each project has multiple versions
                        id = b4blIDToProject.get(key.getQueryKey());
                        break;
                    case BRT:
                        if (key.getTechnique().equals(RetrievalTechnique.LOCUS) &&
                                key.getProject().getProjectName().equals("eclipse-3.1")) {
                            id = eclipseSplits.get(key.getQueryKey());
                            break;
                        }
                        // Intentional pass through
                    case QQ:
                    case LB:
                        // one  corpus for everything
                        id = key.getProject().getProjectName();
                        break;
                    default:
                        throw new RuntimeException("Unknown data set: " + key.getProject().getDataSet());
                }

                return prefix + id;
            }

            public Map toMap() {
                return this.corpora.entrySet().stream()
                        .collect(Collectors.toMap(
                                Map.Entry::getKey,
                                e -> e.getValue().toMap()
                        ));
            }

            private static class LookupCorpus {
                private final Map<Integer, String> map;
                private final String corpusID;
                private final Map<String, Integer> inverseMap;
                private Set<String> queries = new HashSet<>();

                LookupCorpus(String corpusID, List<RetrievalDoc> corpusDocs) {
                    this.corpusID = corpusID;
                    map = corpusDocs.stream()
                            .collect(Collectors.toMap(
                                    RetrievalDoc::getDocId,
                                    RetrievalDoc::getDocName
                            ));

                    // Some duplicated method names in eclipse 2.0 were causing problems so they are
                    // just merged. They didn't seem important anyway: $packageName$.$className$:$className$()
                    inverseMap = map.entrySet().stream()
                            .collect(Collectors.toMap(
                                    Map.Entry::getValue,
                                    Map.Entry::getKey,
                                    (n1, n2) -> {
                                        LOGGER.warn(String.format("Duplicated doc name for %s: %s - %s",
                                                corpusID, n1, n2));

                                        return n1;
                                    }
                            ));
                }

                static LookupCorpus load(String corpusID, Path existingDataSetsPath, QueryResultKey key, Path rawResultsPath) {
                    try {
                        LOGGER.info("Loading corpus: " + key.getProject() + " - " + corpusID);

                        if (key.getTechnique().equals(RetrievalTechnique.LOCUS)) {
                            return loadLocus(corpusID, rawResultsPath);
                        }

                        String projectName = key.getProject().getProjectName();

                        List<RetrievalDoc> corpusDocs = RetrievalUtils.readCorpus(
                                existingDataSetsPath,
                                key.getProject().getDataSet(),
                                projectName,
                                new DiscourseQuery(key.getQueryKey(), null, null, null, null)
                        );

                        return new LookupCorpus(corpusID, corpusDocs);

                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                }

                private static LookupCorpus loadLocus(String corpusID, Path rawResultsPath) throws FileNotFoundException {
                    Gson gson = GsonUtils.createDefaultGson();

                    String projectName = corpusID.replace("LOCUS_", "");
                    Path filePath = rawResultsPath.resolve(projectName + "-locus-corpus-IDs.json");

                    List<RetrievalDoc> docs = gson.fromJson(new FileReader(filePath.toFile()), new TypeToken<List<RetrievalDoc>>() {
                    }.getType());

                    return new LookupCorpus(corpusID, docs);
                }

                void addQuery(String queryKey) {
                    queries.add(queryKey);
                }

                String inverseLookup(Integer docId) {
                    return map.get(docId);
                }

                public Object toMap() {
                    HashMap<Object, Object> convertedMap = Maps.newHashMap();
                    convertedMap.put("queries", Lists.newArrayList(queries));
                    convertedMap.put("ids", this.map);

                    return convertedMap;
                }

                String getCorpusID() {
                    return corpusID;
                }
            }
        }
    }

    private static class IDConversionMap {
        private final Path existingDataSetsPath;
        Map<String, Map<Integer, String>> blbrtMap = new HashMap<>();

        IDConversionMap(Path existingDataSetsPath) {
            this.existingDataSetsPath = existingDataSetsPath;
        }

        @SuppressWarnings("OptionalGetWithoutIsPresent")
        RetrievalResults convert(RetrievalResults results, IDLookupMap.ProjectCorpora.LookupCorpus corpus) {
            Project project = results.getKey().getProject();
            String key = project.toString() + "-" + corpus.corpusID;
            Map<Integer, String> projectMap = blbrtMap.computeIfAbsent(key, k -> loadBLBRTMap(project, corpus));

            Function<RetrievalDoc, String> nameGetter = d -> projectMap.get(d.getDocId());

            List<RetrievalDoc> docs = results.getRetrievedDocuments();
            List<RetrievalDoc> relevant = results.getRelevanceJudgment().get().getRelevantDocs();

            Stream<String> retrievedNames = docs.stream().map(nameGetter);
            Stream<String> relevantNames = relevant.stream().map(nameGetter);

            return new RetrievalResults(
                    results.getKey(),
                    retrievedNames.map(corpus.inverseMap::get).collect(Collectors.toList()),
                    relevantNames.map(corpus.inverseMap::get).collect(Collectors.toList())
            );
        }

        private Map<Integer, String> loadBLBRTMap(Project project, IDLookupMap.ProjectCorpora.LookupCorpus corpus) {
            Path dataSetPath = project.getDataSet().resolveCorpusPath(existingDataSetsPath);
            String projectName;
            if (project.getDataSet().equals(DataSet.B4BL)) {
                projectName = corpus.getCorpusID();
            } else {
                projectName = project.getProjectName();
            }

            Path filePath = dataSetPath.resolve(Paths.get(projectName, "source-code.json"));
            int id = 1;
            Map<String, Integer> result = new HashMap<>();
            try (BufferedReader reader = new BufferedReader(new FileReader(filePath.toFile()))) {
                Gson gson = new Gson();
                String line;
                while ((line = reader.readLine()) != null) {
                    Map map = gson.fromJson(line, Map.class);
                    String packagePath = (String) map.get("file_package_path");
                    // Eclipse contains duplicated package paths and they are being indexed first
                    // come first serve
                    if (!result.containsKey(packagePath)) {
                        result.put(packagePath, id++);
                    }
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }

            // Invert the map
            return result.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        }
    }
}
