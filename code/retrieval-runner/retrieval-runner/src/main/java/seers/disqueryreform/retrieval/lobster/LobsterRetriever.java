package seers.disqueryreform.retrieval.lobster;

import edu.uci.ics.jung.graph.Graph;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.RAMRetrievalIndexer;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.callgraph.CallGraphRetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.*;
import org.apache.lucene.store.Directory;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.retrieval.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class LobsterRetriever extends ApproachRetriever {

    private final Path existingDataSetsPath;

    public LobsterRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath) {
        super(resultsWriter, existingDataSetsPath, RetrievalTechnique.LOBSTER, true);

        this.existingDataSetsPath = existingDataSetsPath;
    }

    private Map<String, Set<String>> loadStackTraces(Project project) {

        //----------------------------------------------

        String projectName = project.getProjectName();
        Path projectPath = project.getDataSet()
                .resolveCorpusPath(existingDataSetsPath)
                .resolve(projectName);

        final String queriesFilePath = projectPath.resolve(projectName + "_Queries.txt").toString();
        final String originalQueriesFilePath = projectPath.resolve(projectName + "_Original_Queries.txt")
                .toString();
        final String stackFilePath = projectPath.resolve(projectName + "_Stack.txt").toString();

        //----------------------------------------------

        CallGraphRetrievalParser parser = new CallGraphRetrievalParser();
        Map<Query, Set<String>> queryStack;
        List<Query> queries;

        try {
            queryStack = parser.readQueryStack(stackFilePath);
            queries = parser.readQueriesWithKey(queriesFilePath, originalQueriesFilePath);
        } catch (StackTraceException | QueryException e) {
            throw new Error(e);
        }

        Map<String, Set<String>> queryTraces = new HashMap<>();

        for (Query query : queries) {
            final Set<String> traces = queryStack.get(query);
            String key = query.getKey();
            if (traces == null) {
                throw new Error("No traces found for " + key);
            }
            queryTraces.put(key, traces);
        }

        return queryTraces;
    }

    private Graph<String, String> readCallGraph(Project project) {
        CallGraphRetrievalParser parser = new CallGraphRetrievalParser();
        String projectName = project.getProjectName();

        final String graphFilePath = project.getDataSet()
                .resolveCorpusPath(existingDataSetsPath)
                .resolve(projectName)
                .resolve(projectName + "_Graph.txt")
                .toString();

        try {
            return parser.readCallGraph(graphFilePath, "C");
        } catch (ParseException | GraphException e) {
            throw new Error(e);
        }
    }

    @Override
    protected Stream<Corpus> loadCorpora(Project project, List<DiscourseQuery> queries) {
        // Lobster implementation requires the original name field set
        return super.loadCorpora(project, queries)
                .peek(c -> c.getDocuments()
                        .forEach(d -> d.setOriginalDocName(d.getDocName())));
    }

    @Override
    protected void runForCorpus(Corpus corpus) {
        Map<String, Set<String>> traces = loadStackTraces(corpus.getProject());
        Graph<String, String> callGraph = readCallGraph(corpus.getProject());

        try (Directory index = new RAMRetrievalIndexer().buildIndex(corpus.getDocuments(), null)) {
            Map<String, String> searcherParams = new HashMap<>();
            searcherParams.put("alpha", "0.9");
            searcherParams.put("dt", "3");
            searcherParams.put("level", "C");
            searcherParams.put("sp", "D");

            CallGraphRetrievalSearcher2 searcher =
                    new CallGraphRetrievalSearcher2(index, searcherParams, corpus.getDocuments(), callGraph);

            corpus.getQueries().parallelStream()
                    .forEach(q -> runAndOutputQuery(q, searcher, corpus, traces));
        } catch (IndexerException | IOException | SearchException | ParameterException e) {
            throw new Error(e);
        }
    }

    private void runAndOutputQuery(DiscourseQuery query, CallGraphRetrievalSearcher2 searcher, Corpus corpus, Map<String, Set<String>> traces) {
        try {
            Set<String> queryTraces = traces.get(query.getKey());

            if (queryTraces.isEmpty()) {
                return;
            }

            Query searchQuery = new Query(query.getId(), query.getText());
            searchQuery.addInfoAttribute(CallGraphRetrievalSearcher2.STACK_TRACE_CLASSES_PROPERTY, queryTraces);
            searchQuery.setKey(query.getKey());

            List<RetrievalDoc> results = searcher.searchQuery(searchQuery);
            RelJudgment relJudgment = corpus.createRelevanceJudgment(query);
            writeQueryResults(new RetrievalResults(corpus.getProject(), query,
                    results, relJudgment, RetrievalTechnique.LOBSTER));
        } catch (SearchException e) {
            throw new Error(String.format("Error processing query %s with Lobster", query), e);
        }
    }
}
