package seers.disqueryreform.evaluation.stats;

import seers.disqueryreform.evaluation.stats.keys.GTSKey;

public class StrategySummary extends StatsWithKey<GTSKey, ReformulationSummary> {
    StrategySummary(GTSKey key, ReformulationSummary summary) {
        super(key, summary);
    }
}
