package seers.disqueryreform.evaluation.stats;

import edu.utdallas.seers.ir4se.evaluation.RetrievalAccumulation;
import edu.utdallas.seers.ir4se.evaluation.RetrievalSummary;
import edu.utdallas.seers.ir4se.evaluation.RetrievalSummary.SummarizedMetric;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReformulationSummary extends ReformulationStats<RetrievalSummary, SummarizedMetric> {

    private final int accumulatedAmount;

    /**
     * We need to calculate the average of improvements and not improvement of the average, so
     * an accumulator of the differences for each aggregation must be kept.
     */
    private final Map<SummarizedMetric, Double> differencesAccumulator;
    private final Map<SummarizedMetric, Double> relativeDifferencesAccumulator;

    ReformulationSummary(RetrievalAccumulation baselineAccumulation, RetrievalAccumulation reformAccumulation) {
        this(baselineAccumulation.summarize(), reformAccumulation.summarize());
    }

    private ReformulationSummary(RetrievalSummary baselineSummary, RetrievalSummary reformulationSummary) {
        super(baselineSummary, reformulationSummary);

        Function<SummarizedMetric, Double> calculateImprovement =
                m -> reformulationSummary.getMetric(m) - baselineSummary.getMetric(m);

        differencesAccumulator = collectMetrics(calculateImprovement);

        relativeDifferencesAccumulator = collectMetrics(m -> {
            double improvement = calculateImprovement.apply(m);
            double baselineValue = baselineSummary.getMetric(m);

            if (baselineValue != 0) {
                return improvement / baselineValue;
            } else if (improvement == 0) {
                return 0D;
            } else {
                // Improvement when baseline is 0 is undefined
                return Double.NaN;
            }
        });

        accumulatedAmount = 1;
    }

    private ReformulationSummary(int accumulatedAmount, RetrievalSummary baselineSummary,
                                 RetrievalSummary reformulationSummary,
                                 Map<SummarizedMetric, Double> differencesAccumulator,
                                 Map<SummarizedMetric, Double> relativeDiffs) {
        super(baselineSummary, reformulationSummary);
        this.accumulatedAmount = accumulatedAmount;
        this.differencesAccumulator = differencesAccumulator;
        this.relativeDifferencesAccumulator = relativeDiffs;
    }

    private Map<SummarizedMetric, Double> collectMetrics(Function<SummarizedMetric, Double> valueExtractor) {
        return Arrays.stream(SummarizedMetric.values())
                .collect(Collectors.toMap(m -> m, valueExtractor));
    }

    private Map<SummarizedMetric, Double> addAccumulators(Map<SummarizedMetric, Double> acc1, Map<SummarizedMetric, Double> acc2) {
        return Stream.of(acc1.entrySet(), acc2.entrySet())
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Map.Entry::getKey))
                .entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().stream().mapToDouble(Map.Entry::getValue).sum()
                ));
    }

    @Override
    @SuppressWarnings("unchecked")
    <T extends ReformulationStats> T aggregate(T rawOther) {
        ReformulationSummary other = (ReformulationSummary) rawOther;
        return (T) new ReformulationSummary(
                accumulatedAmount + other.accumulatedAmount,
                baselineAggregation.aggregate(other.baselineAggregation),
                reformulationAggregation.aggregate(other.reformulationAggregation),
                addAccumulators(differencesAccumulator, other.differencesAccumulator),
                addAccumulators(relativeDifferencesAccumulator, other.relativeDifferencesAccumulator)
        );
    }

    @Override
    public double getRelativeImprovement(SummarizedMetric metric) {
        return relativeDifferencesAccumulator.get(metric) / accumulatedAmount;
    }

    @Override
    public double getImprovement(SummarizedMetric metric) {
        return differencesAccumulator.get(metric) / accumulatedAmount;
    }
}
