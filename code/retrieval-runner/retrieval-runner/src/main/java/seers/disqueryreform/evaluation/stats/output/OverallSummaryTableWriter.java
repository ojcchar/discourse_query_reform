package seers.disqueryreform.evaluation.stats.output;

import edu.utdallas.seers.ir4se.evaluation.RetrievalSummary.SummarizedMetric;
import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.stats.OverallStrategySummary;
import seers.disqueryreform.evaluation.stats.OverallStrategySummary.OverallSummaryEntry;
import seers.disqueryreform.evaluation.stats.ReformulationSummary;
import seers.disqueryreform.evaluation.stats.keys.GranularityTechniquePair;

import java.util.List;
import java.util.stream.Stream;

public class OverallSummaryTableWriter extends TableWriter<OverallSummaryEntry> {

    private final List<GranularityTechniquePair> granularityTechniques;
    private final ComponentsForBugTable table;
    private final StrategyType strategyType;

    public OverallSummaryTableWriter(OverallStrategySummary overallSummary, ComponentsForBugTable table, StrategyType strategyType) {
        granularityTechniques = overallSummary.getGranularityTechniques();
        this.table = table;
        this.strategyType = strategyType;
    }

    /**
     * For each metric (%HITS, MRR, MAP) there are N + 1 headers (N = amount of granularity-techniques):
     * * One header for the improvement of each granularity technique
     * * One header for the average improvement across all granularity-techniques
     *
     * @return Headers.
     */
    @SuppressWarnings("Convert2Diamond")
    private Stream<Header<OverallSummaryEntry>> createImprovementHeaders() {
        return Stream.of(SummarizedMetric.AVERAGE_HITS_PERCENTAGE, SummarizedMetric.AVERAGE_MRR, SummarizedMetric.AVERAGE_MAP)
                .flatMap(m -> Stream.concat(
                        granularityTechniques.stream()
                                .map(gt -> {
                                    String label = String.format("%s-%s Imp %s",
                                            gt.getGranularity(), gt.getTechnique(), m.getName());
                                    return new Header<>(
                                            label,
                                            e -> {
                                                ReformulationSummary stats = e.get(gt);

                                                if (stats == null) {
                                                    return "-";
                                                }

                                                return stats.getRelativeImprovement(m);
                                            }
                                    );
                                }),
                        Stream.of(new Header<OverallSummaryEntry>("Average " + m.getName(), e -> e.calculateAverage(m)))
                ));
    }

    @Override
    protected Stream<Stream<Header<OverallSummaryEntry>>> createHeaders() {
        return Stream.of(
                createSimpleHeader("Strategy", e -> table.createNameSortedByApplicability(e.getFullStrategy(), strategyType)),
                createSimpleHeader("Configuration", e -> ComponentsForBugTable.extractConfiguration(e.getFullStrategy()).orElse("")),
                createImprovementHeaders()
        );
    }
}
