package seers.disqueryreform.retrieval.lobster;

import edu.wayne.cs.severe.ir4se.processor.boundary.Main;
import edu.wayne.cs.severe.ir4se.processor.boundary.MassiveProcessor;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.RetrievalStatsAccumulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;

import java.util.List;

@Deprecated
public class MassiveProcessorThreaded extends MassiveProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MassiveProcessorThreaded.class);

    public void runConfigurations(List<String> files, String processor) {

        try {
            RetrievalStatsAccumulator accumulator = new RetrievalStatsAccumulator();

            final ThreadParameters params = new ThreadParameters();
            params.addParam("processor", processor);
            params.addParam("accumulator", accumulator);
            ThreadExecutor.executePaginated(files, FilesProcessor.class, params, 8);

        } catch (Exception ignored) {
            LOGGER.error("Error", ignored);
        }
    }

    public static class FilesProcessor extends ThreadProcessor {

        private final RetrievalStatsAccumulator accumulator;
        private List<String> files;
        private String processor;

        public FilesProcessor(ThreadParameters params) {
            super(params);
            files = params.getListParam(String.class, ThreadExecutor.ELEMENTS_PARAM);
            processor = params.getStringParam("processor");
            accumulator = params.getParam(RetrievalStatsAccumulator.class, "accumulator");
        }

        @Override
        public void executeJob() throws Exception {

            for (String file : files) {
                Main.execute2( accumulator, file, processor);
            }
        }
    }
}
