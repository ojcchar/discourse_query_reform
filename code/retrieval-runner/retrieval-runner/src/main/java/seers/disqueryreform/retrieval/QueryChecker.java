package seers.disqueryreform.retrieval;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import seers.disqueryreform.base.RetrievalUtils;
import seers.disqueryreform.retrieval.lucene.LuceneMain;

public class QueryChecker {

//	static String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/d4j-generated-all-hard-to-retrieve-queries_or/ALL_TEXT";
//	
	static String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/query_quality-generated-all-hard-to-retrieve-queries_or/ALL_TEXT";

	public static void main(String[] args) throws Exception {

		List<Query> queries = RetrievalUtils.readReformulatedQueries(queriesFolder, true);

		int num = 0;

		for (Query query : queries) {

			List<String> methodsGs = (List<String>) query.getInfoAttribute("method_goldset");
			List<Tuple<String, String>> classMethods = getClMeth(methodsGs);

			String txt = query.getTxt();

			boolean containsGolSet = classMethods.stream()
					.anyMatch(cm -> txt.contains(cm.x) && txt.contains(cm.y));
			if (containsGolSet) {
				System.out.println(query.getInfoAttribute("system") + ";" + query.getKey());
				num++;
			}
		}

		System.out.println(queries.size() + ";" + num + ";" + ((double) num / queries.size()));

	}

	private static List<Tuple<String, String>> getClMeth(List<String> methodsGs) {

		List<Tuple<String, String>> tuplex = new ArrayList<>();

		for (String methodgs : methodsGs) {
			try {
				int i = methodgs.indexOf("(");
				String substring = methodgs;
				if (i != -1) {
					substring = methodgs.substring(0, i);

				}
				String[] split = substring.split(":");

				String m = split[1];
				String c = split[0].substring(split[0].lastIndexOf(".") + 1);

//				System.out.println(c + "-" + m);
				tuplex.add(new Tuple<String, String>(c, m));
			} catch (Exception e) {
				System.err.println(methodgs);
				e.printStackTrace();
			}

		}
		return tuplex;
	}

	public static class Tuple<X, Y> {
		public final X x;
		public final Y y;

		public Tuple(X x, Y y) {
			this.x = x;
			this.y = y;
		}
	}

}
