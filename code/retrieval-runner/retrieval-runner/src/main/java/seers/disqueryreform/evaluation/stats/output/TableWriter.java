package seers.disqueryreform.evaluation.stats.output;

import seers.appcore.csv.CSVHelper;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class TableWriter<T> {
    private Stream<Stream<Object>> generateRows(Stream<T> rows) {
        // Create a list of extractors so that we don't have to check the headers to generate each row
        List<Header<T>> headers = createHeaders()
                .flatMap(Function.identity())
                .collect(Collectors.toList());

        List<Function<T, Object>> extractors = headers.stream()
                .map(Header::getExtractor)
                .collect(Collectors.toList());

        Stream<Object> headerRow = headers.stream().map(Header::getLabel);

        return Stream.concat(
                // Header
                Stream.of(headerRow),

                // Other rows
                rows.map(row ->
                        extractors.stream()
                                .map(e -> e.apply(row))
                )
        );
    }

    protected Stream<Header<T>> createSimpleHeader(String label, Function<T, Object> extractor) {
        return Stream.of(new Header<>(label, extractor));
    }

    public void writeTable(Stream<T> rows, Path outputFilePath) {
        Stream<T> finalRows = modifyRows(rows);

        Stream<Stream<Object>> summaryRows = generateRows(finalRows);

        try {
            CSVHelper.writeCSV(outputFilePath, summaryRows);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Can be used to filter or add more rows before they are printed.
     *
     * @param rows The rows that have been initially passed for printing.
     * @return The rows that will actually be printed.
     */
    protected Stream<T> modifyRows(Stream<T> rows) {
        return rows;
    }

    /**
     * Creates the table headers. The funny return type exists so that we can generate each header
     * with a method that produces more than one header at a time with a single call like in
     * {@link AggregatedTableWriter#createHeadersForMetric(String, Object)}
     *
     * @return Nested stream of headers to use for the table.
     */
    protected abstract Stream<Stream<Header<T>>> createHeaders();

    protected static class Header<U> {
        final String label;
        final Function<U, Object> extractor;

        Header(String label, Function<U, Object> extractor) {
            this.label = label;
            this.extractor = extractor;
        }

        String getLabel() {
            return label;
        }

        Function<U, Object> getExtractor() {
            return extractor;
        }
    }
}
