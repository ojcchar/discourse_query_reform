package seers.disqueryreform.retrieval.bench4bl;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.ir4se.index.utils.DateTimeJsonAdapter;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalWriter;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalStats;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import edu.wayne.cs.severe.ir4se.processor.exception.WritingException;
import org.joda.time.DateTime;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Bench4BLToolEvaluator {
    protected final Path dataPath;
    private Bench4BLRetriever bench4BLRetriever;

    public Bench4BLToolEvaluator(Bench4BLRetriever bench4BLRetriever, Path dataPath) {
        this.bench4BLRetriever = bench4BLRetriever;
        this.dataPath = dataPath;
    }

    /**
     * Evaluates the approach.
     *
     * @param outputPath Where to output the evaluation results.
     * @param systemNames
     * @return A list of evaluation results. Query IDs are unique across all of them.
     * @throws IOException
     * @throws JAXBException
     * @throws InterruptedException
     * @throws EvaluationException
     * @throws WritingException
     */
    public List<Bench4BLResults> evaluateApproach(String outputPath, Set<String> systemNames) throws IOException, JAXBException, InterruptedException, EvaluationException, WritingException {
        List<Bench4BLResults> allEvals = runAndLoadResults(dataPath, systemNames);

        Map<Query, List<Double>> allEvalsMap = new HashMap<>();

        for (Bench4BLResults systemEval : allEvals) {
            // We must convert the evaluation to a map so that we can use the evaluator
//            allEvalsMap.putAll(systemEval.getEvaluationsMap());
        }

        DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();
        RetrievalStats retrievalStats = evaluator.evaluateModel(allEvalsMap);

        DefaultRetrievalWriter writer = new DefaultRetrievalWriter();
        writer.writeStats(retrievalStats, outputPath);

        return allEvals;
    }

    private List<Bench4BLResults> runAndLoadResults(Path dataPath, Set<String> systemNames)
            throws IOException, JAXBException, InterruptedException, EvaluationException {

        File[] projectFolders = dataPath.toFile()
                .listFiles(f -> f.isDirectory() && systemNames.contains(f.getName()));

        List<Bench4BLResults> allSystemEvals = new ArrayList<>();

        for (File projectFolder : Objects.requireNonNull(projectFolders)) {
            allSystemEvals.add(runForProject(projectFolder.toPath()));
        }

        return allSystemEvals;
    }

    private Bench4BLResults runForProject(Path projectDirectoryPath) throws IOException, JAXBException, InterruptedException, EvaluationException {
        String jsonBugReportsPath;
        String system = projectDirectoryPath.getFileName().toString();
        String repoDir = projectDirectoryPath.resolve("git-repo").toString();
        String sourceDir = projectDirectoryPath.resolve("sources").toString();
        jsonBugReportsPath = projectDirectoryPath.resolve("original-bug-reports.json").toString();

        return bench4BLRetriever.retrieve(system, repoDir, sourceDir, loadJSONBugReports(jsonBugReportsPath));
    }

    public static List<BugReport> loadJSONBugReports(String jsonBugReportsPath) throws IOException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeJsonAdapter());
        Gson gson = gsonBuilder.create();

        List<BugReport> bugReports;
        try (BufferedReader reader = new BufferedReader(new FileReader(jsonBugReportsPath))) {
            bugReports = reader.lines()
                    .map(l -> {
                        // If bugs have the proj-ID format, keep only the ID
                        BugReport bugReport = gson.fromJson(l, BugReport.class);
                        String key = bugReport.getKey();
                        if (key.contains("-")) {
                            bugReport.setKey(key.split("-")[1]);
                        }

                        // If fixed files don't have the .java, add it
                        bugReport.setFixedFiles(bugReport.getFixedFiles().stream()
                                .map(f -> f.endsWith(".java") ? f : f + ".java")
                                .collect(Collectors.toList()));
                        return bugReport;
                    })
                    .collect(Collectors.toList());
        }

        return bugReports;
    }
}
