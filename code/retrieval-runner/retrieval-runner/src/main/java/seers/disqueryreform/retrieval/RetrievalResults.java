package seers.disqueryreform.retrieval;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class RetrievalResults {
    private final QueryResultKey key;
    private final List<Integer> retrievedDocIDs;
    private final List<Integer> relevantDocIDs;

    public RetrievalResults(Project project, DiscourseQuery discourseQuery, List<RetrievalDoc> retrievedDocs,
                            RelJudgment relJudgment, RetrievalTechnique approach) {
        key = new QueryResultKey(project, discourseQuery, approach);
        retrievedDocIDs = extractIDs(retrievedDocs);
        relevantDocIDs = extractIDs(relJudgment.getRelevantDocs());
    }

    public RetrievalResults(QueryResultKey key, List<Integer> retrievedDocIDs, List<Integer> relevantDocIDs) {
        this.key = key;
        this.retrievedDocIDs = retrievedDocIDs;
        this.relevantDocIDs = relevantDocIDs;
    }

    /**
     * For testing.
     *
     * @param key The key.
     */
    RetrievalResults(QueryResultKey key) {
        this.key = key;
        retrievedDocIDs = Collections.emptyList();
        relevantDocIDs = Collections.emptyList();
    }

    public static Gson createGson() {
        return new GsonBuilder()
                .registerTypeAdapter(QueryResultKey.class, QueryResultKey.getTypeAdapter())
                .create();
    }

    private List<Integer> extractIDs(List<RetrievalDoc> retrievedDocs) {
        return retrievedDocs.stream()
                .map(RetrievalDoc::getDocId)
                .collect(Collectors.toList());
    }

    private List<RetrievalDoc> idsToRetrievalDocs(List<Integer> retrievedDocIDs) {
        return retrievedDocIDs.stream()
                .map(id -> new RetrievalDoc(id, null, null))
                .collect(Collectors.toList());
    }

    public List<RetrievalDoc> getRetrievedDocuments() {
        return idsToRetrievalDocs(this.retrievedDocIDs);
    }

    public Optional<RelJudgment> getRelevanceJudgment() {
        if (relevantDocIDs == null || relevantDocIDs.isEmpty()) {
            return Optional.empty();
        }

        RelJudgment relJudgment = new RelJudgment();
        relJudgment.setRelevantDocs(idsToRetrievalDocs(relevantDocIDs));

        return Optional.of(relJudgment);
    }

    public QueryResultKey getKey() {
        return key;
    }
}
