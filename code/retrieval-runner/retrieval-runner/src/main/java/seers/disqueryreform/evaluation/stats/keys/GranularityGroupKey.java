package seers.disqueryreform.evaluation.stats.keys;

import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.Objects;

public class GranularityGroupKey {
    private final RetrievalTechnique technique;
    private final String strategy;
    private final int threshold;

    GranularityGroupKey(RetrievalTechnique technique, String strategy, int threshold) {
        this.technique = technique;
        this.strategy = strategy;
        this.threshold = threshold;
    }

    public TechniqueStrategyKey fixedThreshold() {
        return new TechniqueStrategyKey(technique, strategy);
    }

    public EvaluationKey toFullKey() {
        return new EvaluationKey(DocumentGranularity.ALL, technique, strategy, threshold);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GranularityGroupKey that = (GranularityGroupKey) o;
        return threshold == that.threshold &&
                technique == that.technique &&
                strategy.equals(that.strategy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(technique, strategy, threshold);
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }
}
