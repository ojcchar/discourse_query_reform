package seers.disqueryreform.retrieval;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.StrategyQueryContainer;
import seers.disqueryreform.querygen.tasknav.DiscourseTasksQueryGenerator;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Manages converting queries from grouped by strategy to grouped by project.
 */
public class QueryGrouper {
    public static final Logger LOGGER = LoggerFactory.getLogger(QueryGrouper.class);

    private final Path groupedQueriesPath;

    private QueryGrouper(Path groupedQueriesPath) {
        this.groupedQueriesPath = groupedQueriesPath;
    }

    /**
     * Takes the specified segment of strategy containers, writes them by project in the specified
     * location, and returns a manager that can be used to read the resulting queries.
     *
     * @param reformulatedQueriesPath Path with strategy containers.
     * @param segmentNumber           Which segment should be picked.
     * @param totalSegments           How many segments should the containers be split in.
     * @param tempPath                Path where queries by project will be stored.
     * @return A new query manager.
     * @throws IOException If reading strategies or writing queries fail.
     */
    public static QueryGrouper newQueryGrouper(Path reformulatedQueriesPath, int segmentNumber,
                                               int totalSegments, Path tempPath)
            throws IOException {

        if (segmentNumber <= 0 || totalSegments <= 0 || segmentNumber > totalSegments) {
            throw new IllegalArgumentException("Invalid segment, x and y must be > 0 and x <= y");
        }

        Path groupedQueriesPath = tempPath.resolve(String.format("grouped-queries-%d-%d", segmentNumber, totalSegments));

        QueryGrouper queryGrouper = new QueryGrouper(groupedQueriesPath);

        File groupedQueriesDir = groupedQueriesPath.toFile();
        if (!groupedQueriesDir.exists()) {
            LOGGER.info(String.format("Calculating query segment %d of %d", segmentNumber, totalSegments));

            List<StrategyQueryContainer> strategies =
                    queryGrouper.readStrategySegment(reformulatedQueriesPath, segmentNumber, totalSegments);

            queryGrouper.writeQueriesByProject(strategies);
        } else {
            LOGGER.info(String.format("Queries for segment %d of %d are already grouped", segmentNumber, totalSegments));
        }

        return queryGrouper;
    }

    /**
     * To run queries as efficiently as possible, we group all queries from all strategies that
     * belong to the same project. This way we only have to load the corpus once.
     *
     * @param strategies The strategy containers with the queries that will be run.
     * @throws IOException If saving to file fails.
     */
    private void writeQueriesByProject(List<StrategyQueryContainer> strategies) throws IOException {
        FileUtils.forceMkdir(groupedQueriesPath.toFile());

        // Collects queries by project across all datasets
        Map<Project, List<DiscourseQuery>> queriesByProject = strategies.stream()
                .flatMap(c -> c.getItems().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (l1, l2) -> Stream.concat(l1.stream(), l2.stream())
                                .collect(Collectors.toList())
                ));

        LOGGER.info(String.format("Writing %d projects to %s", queriesByProject.size(), groupedQueriesPath));

        Gson gson = GsonUtils.createDefaultGson();

        for (Map.Entry<Project, List<DiscourseQuery>> entry : queriesByProject.entrySet()) {
            Project project = entry.getKey();
            File projectOutFile =
                    groupedQueriesPath.resolve(project + ".json").toFile();

            try (FileWriter writer = new FileWriter(projectOutFile)) {
                gson.toJson(entry.getValue(), writer);
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    private List<StrategyQueryContainer> readStrategySegment(Path reformulatedQueriesPath, int segmentNumber, int totalSegments) throws IOException {
        // All strategies
        if (totalSegments == 1) {
            return Arrays.stream(reformulatedQueriesPath.toFile().listFiles(f -> !f.getName().startsWith("component") && f.getName().endsWith(".json")))
                    .map(this::loadContainer)
                    .collect(Collectors.toList());
        }

        // Otherwise, split the strategies
        List<Pair<String, Integer>> sortedStrategySizes = readSortedStrategySizes(reformulatedQueriesPath);

        int totalQueries = sortedStrategySizes.stream()
                .mapToInt(Pair::getValue)
                .sum();

        int idealSegmentSize = totalQueries / totalSegments;

        List<Segment> segments = splitStrategies(sortedStrategySizes, idealSegmentSize, totalSegments);

        Segment segment = segments.get(segmentNumber - 1);

        LOGGER.info(String.format("Segment %d of %d has %d queries out of %d",
                segmentNumber,
                totalSegments,
                segment.size,
                segments.stream().mapToInt(s -> s.size).sum()));

        LOGGER.info(String.format("Loading queries from %d strategy containers...", segment.strategies.size()));

        return segment.strategies.stream()
                .map(n -> reformulatedQueriesPath.resolve(n + ".json").toFile())
                .map(this::loadContainer)
                .collect(Collectors.toList());
    }

    private StrategyQueryContainer loadContainer(File file) {
        LOGGER.info(String.format("Loading strategy container %s", file));

        try {
            return StrategyQueryContainer.fromFile(file);
        } catch (IOException e) {
            throw new UncheckedIOException(String.format("Error loading strategy file: %s", file.getAbsolutePath()), e);
        }
    }

    private List<Segment> splitStrategies(List<Pair<String, Integer>> sortedStrategySizes, int segmentSize, int remainingSegments) {
        if (sortedStrategySizes.isEmpty()) {
            return Collections.emptyList();
        }

        if (remainingSegments == 1) {
            // We cannot split anymore
            return Collections.singletonList(new Segment(sortedStrategySizes));
        }

        /* Since strategies have a random size, we take them out one by one until we have a segment
        of size >= to the ideal segment size, hoping that the split will be more or less even across
        segments. This is a simpler solution to e.g. framing this as a knapsack problem, which would
        probably be more trouble than it's worth */
        int segmentSizeAccum = 0;

        // Assignment in case the loop ends without breaking
        int endIndex = sortedStrategySizes.size() - 1;

        for (int i = 0; i < sortedStrategySizes.size(); i++) {
            segmentSizeAccum += sortedStrategySizes.get(i).getValue();

            if (segmentSizeAccum >= segmentSize) {
                endIndex = i;
                break;
            }
        }

        List<Segment> thisSegment = Collections.singletonList(new Segment(sortedStrategySizes.subList(0, endIndex + 1)));

        // Recursively get the rest of the segments
        List<Segment> otherSegments = splitStrategies(sortedStrategySizes.subList(endIndex + 1, sortedStrategySizes.size()),
                segmentSize, remainingSegments - 1);

        return Stream.concat(thisSegment.stream(), otherSegments.stream())
                .collect(Collectors.toList());
    }

    private List<Pair<String, Integer>> readSortedStrategySizes(Path reformulatedQueriesPath) throws IOException {
        // FIXME: pass repo path instead of reform queries path here
        Path baseRepositoryPath = reformulatedQueriesPath.resolve("..").resolve("..");
        return new DiscourseTasksQueryGenerator(baseRepositoryPath).readStrategyFileSizeSummary()
                // Sort by strategy name to make sure the list has the same ordering on all nodes
                .sorted(Comparator.comparing(Pair::getKey))
                .collect(Collectors.toList());
    }

    /**
     * @return A stream so that queries can be loaded lazily.
     */
    @SuppressWarnings("ConstantConditions")
    Stream<Pair<Project, List<DiscourseQuery>>> loadQueriesByProject() {
        Gson gson = GsonUtils.createDefaultGson();
        Type listType = new TypeToken<List<DiscourseQuery>>() {
        }.getType();

        return Arrays.stream(groupedQueriesPath.toFile().listFiles(f -> f.getName().endsWith(".json")))
                .map(f -> {
                    try (FileReader reader = new FileReader(f)) {
                        String projectString = f.getName().replace(".json", "");
                        return new Pair<>(Project.fromString(projectString), gson.fromJson(reader, listType));
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
    }

    /**
     * Dumb class used to keep track of segment sizes.
     */
    private class Segment {
        private final Set<String> strategies = new HashSet<>();
        private int size = 0;

        Segment(List<Pair<String, Integer>> strategySizes) {
            for (Pair<String, Integer> pair : strategySizes) {
                strategies.add(pair.getKey());
                size += pair.getValue();
            }
        }
    }
}