package seers.disqueryreform;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.action.StoreTrueArgumentAction;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.io.FileUtils;
import seers.disqueryreform.querygen.tasknav.DiscourseTasksQueryGenerator;
import seers.disqueryreform.retrieval.DiscourseReformulationRetriever;
import seers.disqueryreform.retrieval.QueryGrouper;
import seers.disqueryreform.retrieval.bench4bl.locus.LocusRetrieverWrapper;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Runs all reformulation strategies with all approaches. It is made so that it can easily split
 * work across multiple machines, for example, if running on three separate machines, you would
 * run each with 1/3, 2/3 and 3/3 segment parameters.
 */
public class DiscourseReformulationRunner {
    public static void main(String[] args) throws Exception {
        ArgumentParser parser =
                ArgumentParsers.newArgumentParser("DiscourseReformulationRunner", true);

        parser.addArgument("segment")
                .help("Segment of queries that will be executed. Format: x/y. Ex: 1/1, 1/3, 3/5, 7/7")
                .type(String.class);

        parser.addArgument("baseRepositoryPath")
                .help("Base path of the repository")
                .type(String.class);

        parser.addArgument("-o", "--output-path")
                .help("Directory for result output")
                .setDefault("discourse-reformulation-results")
                .type(String.class);

        parser.addArgument("-t", "--locus-threads")
                .help("Amount of threads to use for Locus. Over 10G of memory will be used for each thread")
                .setDefault(LocusRetrieverWrapper.THREAD_POOL_SIZE)
                .type(Integer.class);

        parser.addArgument("-s", "--simplified-generation")
                .help("If provided, only generate queries with configuration true-false and full tasks")
                .action(new StoreTrueArgumentAction());

        parser.addArgument("-O", "--include-other")
                .help("Include strategies using OTHER without tasks")
                .action(new StoreTrueArgumentAction());

        try {
            Namespace parsedArgs = parser.parseArgs(args);
            String segmentString = parsedArgs.getString("segment");
            Matcher segmentMatcher = Pattern.compile("(\\d+)/(\\d+)").matcher(segmentString);

            if (!segmentMatcher.matches()) {
                throw new NumberFormatException();
            }

            int segmentNumber = Integer.valueOf(segmentMatcher.group(1));
            int totalSegments = Integer.valueOf(segmentMatcher.group(2));

            Path baseRepositoryPath = Paths.get(parsedArgs.getString("baseRepositoryPath"));
            Path resultsOutputPath = Paths.get(parsedArgs.getString("output_path"));
            RetrievalSettings.getInstance().setLocusThreadPoolSize(parsedArgs.getInt("locus_threads"));
            boolean simplify = parsedArgs.getBoolean("simplified_generation");
            boolean includeOther = parsedArgs.getBoolean("include_other");

            new DiscourseReformulationRunner()
                    .runQueries(baseRepositoryPath, resultsOutputPath, segmentNumber, totalSegments, simplify, includeOther);

        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        } catch (NumberFormatException e) {
            System.err.println("Segment format is x/y where x and y are integers");
            System.exit(2);
        }
    }

    private void runQueries(Path baseRepositoryPath, Path resultsOutputParentDir,
                            int segmentNumber, int totalSegments, boolean simplified, boolean includeOther)
            throws Exception {
        Path resultsOutputPath = resultsOutputParentDir
                .resolve(String.format("results-%d-%d", segmentNumber, totalSegments));

        new DiscourseTasksQueryGenerator(baseRepositoryPath).generateQueries(true, simplified, includeOther);

        Path tempPath = Paths.get(FileUtils.getTempDirectoryPath()).resolve("temp-discourse-queries");
        Path reformulatedQueriesPath = baseRepositoryPath.resolve(Paths.get("data", "tasknav_reformulated_queries"));
        Path existingDataSetsPath = baseRepositoryPath.resolve(Paths.get("data", "existing_data_sets"));

        QueryGrouper queryGrouper = QueryGrouper.newQueryGrouper(reformulatedQueriesPath, segmentNumber, totalSegments, tempPath);

        new DiscourseReformulationRetriever(resultsOutputPath, existingDataSetsPath).runQueries(queryGrouper);
    }
}
