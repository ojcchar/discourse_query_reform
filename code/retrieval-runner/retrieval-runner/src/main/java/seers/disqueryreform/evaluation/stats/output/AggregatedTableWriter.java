package seers.disqueryreform.evaluation.stats.output;

import seers.disqueryreform.evaluation.stats.ReformulationStats;
import seers.disqueryreform.evaluation.stats.StatsWithKey;

import java.util.stream.Stream;

/**
 * Generic table writer for summaries.
 * FIXME using classes instead of enums for the metrics would probably reduce the amount of type parameters needed
 *
 * @param <T> The type of each input table row.
 * @param <S> The type of reformulation stats that the row type uses.
 * @param <M> The metric that the stats type uses.
 * @param <K> The key that the rows use
 */
abstract class AggregatedTableWriter<T extends StatsWithKey<K, S>, S extends ReformulationStats<?, M>, M, K> extends TableWriter<T> {

    /**
     * We have to create four headers for each metric, so this simplifies the creation.
     *
     * @param metricName Name of the metric
     * @param metric     Metric to create headers for.
     * @return A stream with three headers.
     */
    Stream<Header<T>> createHeadersForMetric(String metricName, M metric) {
        return Stream.concat(Stream.of(
                new Header<>("Base " + metricName, s -> s.getStats().getBaselineMetric(metric)),
                new Header<>("Reform " + metricName, s -> s.getStats().getReformulationMetric(metric))),
                createImprovementHeaders(metricName, metric));
    }

    Stream<Header<T>> createImprovementHeaders(String metricName, M metric) {
        return Stream.of(
                new Header<>("Imp " + metricName, s -> s.getStats().getImprovement(metric)),
                new Header<>("Rel Imp " + metricName, s -> s.getStats().getRelativeImprovement(metric))
        );
    }

}
