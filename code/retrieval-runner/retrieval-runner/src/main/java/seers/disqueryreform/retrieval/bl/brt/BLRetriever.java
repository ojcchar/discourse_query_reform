package seers.disqueryreform.retrieval.bl.brt;

import edu.utdallas.seers.buglocator.BugLocator;
import edu.utdallas.seers.ir4se.retrieval.Retriever;
import seers.disqueryreform.retrieval.ResultsLineWriter;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.io.IOException;
import java.nio.file.Path;

public class BLRetriever extends BLBRTRetriever {
    public BLRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath) {
        super(resultsWriter, existingDataSetsPath, RetrievalTechnique.BUG_LOCATOR);
    }

    @Override
    protected Retriever buildRetriever(String name, Path corpusFolderPath, Path indexPath, float alpha) throws IOException {
        return BugLocator.buildRetriever(name, corpusFolderPath, indexPath, alpha);
    }
}
