package seers.disqueryreform.retrieval.bl.brt;

import edu.utdallas.seers.brtracer.BRTracer;
import edu.utdallas.seers.buglocator.BugLocator;
import edu.wayne.cs.severe.ir4se.processor.exception.SearchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Deprecated
public class BLBRTRunnerBench4BL {

    private static final Logger LOGGER = LoggerFactory.getLogger(BLBRTRunnerBench4BL.class);

    public static void main(String[] args) throws Exception {

        final String indexFolder = "C:\\Users\\ojcch\\Documents\\Temp\\blbrt_indexes";
        String baseFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
                "\\existing_data_sets";
        final String alpha = "0.2";
        final String outFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git" +
                "\\discourse_query_reform\\output\\all" +
                "-queries-run-8-29\\bl_brt";
        String statsFolder = Paths.get(outFolder, "stats").toString();

        //----------------------------

        runTools(indexFolder, baseFolder, alpha, statsFolder);

        //----------------------------

        aggregateRanks(outFolder, statsFolder, "BugLocator");
        aggregateRanks(outFolder, statsFolder, "BRTracer");

    }

    private static void runTools(String indexFolder, String baseFolder, String alpha, String statsFolder) throws IOException, SearchException {
        final File[] brProjectFolders = DataSetUtils.getBRProjectFolders(baseFolder, DataSet.B4BL);
        for (File brProjectFolder : brProjectFolders) {

            LOGGER.debug(brProjectFolder.getName());

            final String blOutFile = String.format("out-%s-%s.csv", brProjectFolder.getName(), "BL");
            final String brtOutFile = String.format("out-%s-%s.csv", brProjectFolder.getName(), "BRT");

            BugLocator.main(new String[]{"-d", brProjectFolder.toString(), "-o",
                    Paths.get(statsFolder, blOutFile).toString(), "-a", alpha, "-i",
                    indexFolder});
            BRTracer.main(new String[]{"-d", brProjectFolder.toString(), "-o",
                    Paths.get(statsFolder, brtOutFile).toString(), "-a", alpha, "-i",
                    indexFolder});
        }
    }

    private static void aggregateRanks(String outFolder, String statsFolder, String tech) throws IOException {
        List<List<String>> allLines = getTechLines(statsFolder, tech);

        File blRanksFile = Paths.get(outFolder, "all_ranks_" + tech + ".csv").toFile();
        List<String> header = getHeader(statsFolder, tech);
        CSVHelper.writeCsv(blRanksFile, header, allLines, null, Function.identity(), CSVHelper.DEFAULT_SEPARATOR);
    }

    private static List<List<String>> getTechLines(String outFolder, String tech) throws IOException {
        final File[] outFiles = getOutFiles(outFolder, tech);

        List<List<String>> allLines = new ArrayList<>();
        for (File outFile : outFiles) {
            final List<List<String>> lines = CSVHelper.readCsv(outFile.getAbsolutePath(), true,
                    CSVHelper.DEFAULT_SEPARATOR);
            final List<List<String>> subLines = lines.subList(1, lines.size());
            subLines.forEach(l -> {
                final String systemVersion = l.get(0);
                String system = systemVersion.substring(0, systemVersion.indexOf("_"));
                l.add(0, system);
            });
            allLines.addAll(subLines);
        }
        return allLines;
    }

    private static File[] getOutFiles(String outFolder, String tech) {
        return new File(outFolder)
                .listFiles(f -> !f.isDirectory() &&
                        f.getName().startsWith(tech) &&
                        f.getName().endsWith(".csv"));
    }

    private static List<String> getHeader(String outFolder, String tech) throws IOException {
        final File[] outFiles = getOutFiles(outFolder, tech);

        final List<List<String>> lines = CSVHelper.readCsv(outFiles[0].getAbsolutePath(), true,
                CSVHelper.DEFAULT_SEPARATOR);
        final List<String> header = lines.get(0);
        header.add(0, "system");
        header.set(1, "system_version");
        return header;
    }
}
