package seers.disqueryreform.evaluation.stats;

import seers.disqueryreform.evaluation.stats.keys.TechniqueStrategyKey;

/**
 * Exists only to combine key and value so that we don't have to use something like a Pair.
 */
public class TechniqueStrategySummary extends StatsWithKey<TechniqueStrategyKey, ReformulationSummary> {

    TechniqueStrategySummary(TechniqueStrategyKey key, ReformulationSummary stats) {
        super(key, stats);
    }

}
