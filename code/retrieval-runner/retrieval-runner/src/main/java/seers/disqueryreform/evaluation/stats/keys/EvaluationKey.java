package seers.disqueryreform.evaluation.stats.keys;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.retrieval.QueryResultKey;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.io.IOException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EvaluationKey {
    private static final String TO_STRING_DELIMITER = ";";

    private final DocumentGranularity granularity;
    private final RetrievalTechnique technique;
    private final String strategy;
    private final int threshold;

    public EvaluationKey(QueryResultKey key, int threshold) {
        granularity = DocumentGranularity.forDataSet(key.getProject().getDataSet());
        technique = key.getTechnique();
        strategy = DiscourseQuery.generateFullStrategyName(key.getStrategy(), key.getTaskNavConfiguration());
        this.threshold = threshold;
    }

    EvaluationKey(DocumentGranularity granularity, RetrievalTechnique technique, String strategy, Integer threshold) {
        this.granularity = granularity;
        this.technique = technique;
        this.strategy = strategy;
        this.threshold = threshold;
    }

    private static EvaluationKey fromString(String string) {
        String[] split = string.split(TO_STRING_DELIMITER);
        return new EvaluationKey(
                DocumentGranularity.valueOf(split[0]),
                RetrievalTechnique.valueOf(split[1]),
                split[2],
                Integer.valueOf(split[3])
        );
    }

    public GranularityGroupKey fixedGranularity() {
        return new GranularityGroupKey(technique, strategy, threshold);
    }

    public EvaluationKey withStrategy(String otherStrategy) {
        return new EvaluationKey(granularity, technique, otherStrategy, threshold);
    }

    public GTSKey removeThreshold() {
        return new GTSKey(granularity, technique, strategy);
    }

    @Override
    public String toString() {
        return Stream.of(
                granularity,
                technique,
                strategy,
                threshold
        )
                .map(String::valueOf)
                .collect(Collectors.joining(TO_STRING_DELIMITER));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvaluationKey that = (EvaluationKey) o;
        return threshold == that.threshold &&
                granularity == that.granularity &&
                technique == that.technique &&
                strategy.equals(that.strategy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(granularity, technique, strategy, threshold);
    }

    public static TypeAdapter<EvaluationKey> getTypeAdapter() {
        return new TypeAdapter<EvaluationKey>() {
            @Override
            public void write(JsonWriter jsonWriter, EvaluationKey evaluationKey) throws IOException {
                jsonWriter.value(evaluationKey.toString());
            }

            @Override
            public EvaluationKey read(JsonReader jsonReader) throws IOException {
                return fromString(jsonReader.nextString());
            }
        };
    }

    public DocumentGranularity getGranularity() {
        return granularity;
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }

    public String getStrategy() {
        return strategy;
    }

    public int getThreshold() {
        return threshold;
    }

    public GTSTKey getGTSTKey() {
        return new GTSTKey(granularity, technique, strategy, threshold);
    }
}
