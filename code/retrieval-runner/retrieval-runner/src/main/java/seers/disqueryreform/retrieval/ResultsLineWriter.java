package seers.disqueryreform.retrieval;

import com.google.gson.Gson;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Writes query results lines into files of a specified length.
 */
public class ResultsLineWriter implements AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultsLineWriter.class);

    /**
     * They are JSON lines files.
     */
    static final String RESULTS_PART_PREFIX = "result-lines-";

    /**
     * Maximum amount of lines to write to each results part.
     */
    private static final int MAX_LINES_PER_PART = 200_000;

    private final Set<QueryResultKey> alreadyExecutedKeys;
    private final Path resultsOutputPath;
    private final Gson gson = RetrievalResults.createGson();
    private final int maxLinesPerPart;
    private int currentPartIndex;
    private PrintWriter writer;
    private int writtenLines = 0;

    private ResultsLineWriter(Path resultsOutputPath, Set<QueryResultKey> alreadyExecutedKeys, int maxLinesPerPart) {
        this.resultsOutputPath = resultsOutputPath;
        this.alreadyExecutedKeys = alreadyExecutedKeys;
        this.maxLinesPerPart = maxLinesPerPart;
    }

    private static Set<QueryResultKey> readAlreadyExecutedKeys(Path resultsOutputDir) {
        if (!Files.exists(resultsOutputDir)) {
            return Collections.emptySet();
        }

        return allResultsLines(resultsOutputDir)
                .map(RetrievalResults::getKey)
                .collect(Collectors.toSet());
    }

    /**
     * Returns a stream over all the lines of all results file. Kept in this class so that both reading
     * and writing logic are in the same place and it's easier to understand and test.
     *
     * @param resultsOutputDir Directory containing the result lines
     * @return A stream of results found in the file lines.
     */
    public static Stream<RetrievalResults> allResultsLines(Path resultsOutputDir) {
        Gson gson = RetrievalResults.createGson();

        // TODO load amount of files to show progress
        // TODO find files up to 2 levels deep
        return findResultParts(resultsOutputDir)
                .flatMap(p -> {
                    try {
                        LOGGER.info("Opening result lines file for reading: " + p);
                        return Files.lines(p);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .map(l -> gson.fromJson(l, RetrievalResults.class));
    }

    private static Stream<Path> findResultParts(Path resultsOutputDir) {
        try {
            return Files.walk(resultsOutputDir)
                    .filter(p -> p.getFileName().toString().startsWith(RESULTS_PART_PREFIX));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    static ResultsLineWriter createResultsLineWriter(Path resultsOutputPath) throws IOException {
        return createResultsLineWriter(resultsOutputPath, MAX_LINES_PER_PART);
    }

    static ResultsLineWriter createResultsLineWriter(Path resultsOutputPath, int maxLinesPerPart) throws IOException {
        Set<QueryResultKey> alreadyExecutedKeys = readAlreadyExecutedKeys(resultsOutputPath);

        ResultsLineWriter resultsLineWriter = new ResultsLineWriter(resultsOutputPath, alreadyExecutedKeys, maxLinesPerPart);

        resultsLineWriter.setUpForWriting();

        return resultsLineWriter;
    }

    private void setUpForWriting() throws IOException {
        FileUtils.forceMkdir(resultsOutputPath.toFile());

        currentPartIndex = findCurrentPartIndex(resultsOutputPath);

        openNextFileForWriting();
    }

    private void openNextFileForWriting() throws IOException {
        close();

        currentPartIndex++;
        writtenLines = 0;

        File newOutFile = resultsOutputPath
                .resolve(ResultsLineWriter.RESULTS_PART_PREFIX + currentPartIndex + ".txt")
                .toFile();

        if (newOutFile.exists()) {
            throw new FileExistsException("Not overwriting part file " + newOutFile.getAbsolutePath());
        }

        writer = new PrintWriter(new FileWriter(newOutFile));
    }

    private int findCurrentPartIndex(Path resultsOutputDir) {
        return findResultParts(resultsOutputDir)
                .mapToInt(p -> {
                    String partNumber = p.getFileName().toString()
                            .replace(RESULTS_PART_PREFIX, "")
                            .replace(".txt", "");

                    return Integer.parseInt(partNumber);
                })
                .max()
                // the maximum index that already exists or 0 if there are no files yet
                .orElse(0);
    }

    /**
     * Writes results to file. Thread-safe.
     *
     * @param retrievalResults Results to write to file.
     */
    void write(RetrievalResults retrievalResults) throws IOException {
        // TODO: keep a concurrent queue and write using a dedicated thread
        synchronized (this) {
            writer.println(gson.toJson(retrievalResults));

            writtenLines++;

            if (writtenLines >= maxLinesPerPart) {
                openNextFileForWriting();
            }
        }
    }

    @Override
    public void close() {
        if (writer != null) {
            writer.close();
        }
    }

    Set<QueryResultKey> getAlreadyExecutedKeys() {
        return alreadyExecutedKeys;
    }
}
