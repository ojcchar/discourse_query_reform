package seers.disqueryreform.retrieval.bench4bl.eval;

import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalWriter;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalStats;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.bench4bl.Bench4BLUtils;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ResultsComputer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResultsComputer.class);

//    private static String taskName = "NewDataAsIs";
    private static String taskName = "NewDataCommitFilter3";
    private static final String TECHNIQUE = "Locus";
    private static String outputFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform" +
            "\\output\\all-queries-run-9-7\\locus";

    public static void main(String[] args) throws Exception {

        int qId = 1;
        Map<Query, List<Double>> allQueryEvals = new LinkedHashMap<>();

        for (ImmutablePair<String, String> groupProject : Bench4BLUtils.groupProjects) {
            final String group = groupProject.getLeft();
            final String project = groupProject.getRight();

            LOGGER.debug(String.format("%s-%s", group, project));

            List<String> bugs = Bench4BLUtils.getBugs(group, project);
            List<List<String>> rawResults = Bench4BLUtils.getRawResults(group, project, taskName, TECHNIQUE);

            final Map<String, List<List<String>>> resultsByBug =
                    rawResults.stream().collect(Collectors.groupingBy(l -> l.get(0)));

            for (String bugId : bugs) {
                try {
                    List<List<String>> bugResults = resultsByBug.get(bugId);
                    double reciprocalRank = 0;
                    double ap = 0;
                    double rankFirst = 0;

                    if (bugResults != null) {
                        List<Integer> ranks = bugResults.stream()
                                .map(l -> Integer.valueOf(l.get(2)))
                                .sorted(Integer::compareTo)
                                .collect(Collectors.toList());
                        reciprocalRank = EvaluationMetric.RR(ranks);
                        ap = EvaluationMetric.AP(ranks);
                        rankFirst = ranks.get(0).doubleValue() + 1;
                    }

                    Query query = new Query(qId++);
                    query.setKey(project + "-" + bugId);
                    query.addInfoAttribute("system", project);
                    query.addInfoAttribute("group", group);
                    allQueryEvals.put(query, Arrays.asList(rankFirst, reciprocalRank,
                            0d, 0d, 0d, 0d, 0d,
                            ap, 0d, 0d, 0d));
                } catch (Exception e) {
                    LOGGER.debug("Error for bug " + bugId, e);
                    throw e;
                }

            }

        }

        //------------------------------------

        DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();
        RetrievalStats retrievalStats = evaluator.evaluateModel(allQueryEvals);

        DefaultRetrievalWriter writer = new DefaultRetrievalWriter();

        File outFold = Paths.get(outputFolder, taskName).toFile();
        if (!outFold.exists())
            FileUtils.forceMkdir(outFold);

        String outFile = Paths.get(outFold.getAbsolutePath(),
                "results-all.csv").toString();
        writer.writeStats(retrievalStats, outFile);

        RetrievalStats retrievalStats2 = evaluator.evaluateModel(allQueryEvals, true);
        outFile = Paths.get(outFold.getAbsolutePath(),
                "results-no_zero_ranks.csv").toString();
        writer.writeStats(retrievalStats2, outFile);
    }


}
