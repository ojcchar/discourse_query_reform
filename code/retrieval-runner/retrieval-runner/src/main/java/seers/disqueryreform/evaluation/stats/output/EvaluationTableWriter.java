package seers.disqueryreform.evaluation.stats.output;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation.Metric;
import seers.disqueryreform.evaluation.stats.ReformulationEvaluation;

import java.nio.file.Path;
import java.util.stream.Stream;

public class EvaluationTableWriter extends ByLineTableWriter<ReformulationEvaluation> {
    public EvaluationTableWriter(Path outputFilePath) {
        super(outputFilePath);
    }

    @Override
    protected Stream<Stream<Header<ReformulationEvaluation>>> createHeaders() {
        return Stream.of(
                createSimpleHeader("Technique", e -> e.getKey().getTechnique()),
                createSimpleHeader("Dataset", e -> e.getQueryResultKey().getProject().getDataSet()),
                createSimpleHeader("Project", e -> e.getQueryResultKey().getProject().getProjectName()),
                createSimpleHeader("Key", e -> e.getQueryResultKey().getQueryKey()),
                createSimpleHeader("Strategy", e -> e.getKey().getStrategy()),
                createSimpleHeader("Applies To", e -> e.getStrategies().size()),
                createSimpleHeader("Threshold", e -> e.getKey().getThreshold()),
                createSimpleHeader("Base Rank", e -> e.getBaselineMetric(Metric.RANK)),
                createSimpleHeader("Reform Rank", e -> e.getReformulationMetric(Metric.RANK)),
                createSimpleHeader("Rank diff",
                        e -> e.getBaselineMetric(Metric.RANK) - e.getReformulationMetric(Metric.RANK)),
                createSimpleHeader("Base AP", e -> e.getBaselineMetric(Metric.AP)),
                createSimpleHeader("Reform AP", e -> e.getReformulationMetric(Metric.AP))
        );
    }
}
