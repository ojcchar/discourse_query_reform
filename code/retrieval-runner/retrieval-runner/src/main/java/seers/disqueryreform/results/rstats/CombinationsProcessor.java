package seers.disqueryreform.results.rstats;

import org.apache.commons.io.FileUtils;
import seers.appcore.csv.CSVHelper;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class CombinationsProcessor extends ThreadProcessor {


    private List<List> bugs;
    private List<List> stats;
    List<List> combinations;
    private String tempFolder;
    private String scriptPath;
    private String resultsFolder;

    public CombinationsProcessor(ThreadParameters params) {
        super(params);
        combinations = params.getListParam(List.class, ThreadExecutor.ELEMENTS_PARAM);
        bugs = params.getListParam(List.class, "bugs");
        stats = params.getListParam(List.class, "stats");
        tempFolder = params.getStringParam("tempFolder");
        scriptPath = params.getStringParam("scriptPath");
        resultsFolder = params.getStringParam("resultsFolder");
    }

    @Override
    public void executeJob() {

        final ArrayList<List<String>> localBugs = new ArrayList(bugs);
        final ArrayList<List<String>> localStats = new ArrayList(stats);

        for (List<String> combination : combinations) {
            try {
                processCombination(combination, localBugs, localStats);
            } catch (Exception e) {
                LOGGER.error("Error for combination: " + combination, e);
            }
        }

    }

    void processCombination(List<String> combination, ArrayList<List<String>> localBugs,
                            ArrayList<List<String>> localStats) throws Exception {

        LOGGER.debug(combination.toString());

        CombinationData combinationData = getCombinationData(combination, localBugs, localStats).readData();
        if (combinationData.isNoData()) return;
        List<List<String>> combinationBugs = combinationData.getCombinationBugs();
        List<List<String>> combinationStats = combinationData.getCombinationStats();

        //---------------------------------------------------

        String approach = combination.get(1);
        String reformStrategy = combination.get(2);
        String rankThreshold = combination.get(3);
        runAnalysis(combination, combinationBugs, combinationStats, tempFolder, resultsFolder,null,
                approach, reformStrategy, rankThreshold);

    }

    public CombinationData getCombinationData(List<String> combination, ArrayList<List<String>> localBugs,
                                               ArrayList<List<String>> localStats) {
        return new CombinationData(combination, localBugs, localStats);
    }

    void runAnalysis(List<String> combination, List<List<String>> combinationBugs,
                     List<List<String>> combinationStats, String tempFolder1,String resultsFolder1,
                     String fileSuffix, String approach, String reformStrategy, String rankThreshold)
            throws Exception {

        if (fileSuffix == null)
            fileSuffix = "";

        //LOGGER.debug(combination.toString() + ": Saving data and running script");

        final ArrayList<String> elementsList = new ArrayList<>(combination);
        elementsList.add(fileSuffix);
        final String[] elements = elementsList.toArray(new String[0]);
        final File combinationTempFolder = Paths.get(tempFolder1, elements).toFile();
        combinationTempFolder.mkdirs();

        File outFolder = Paths.get(resultsFolder1, elements).toFile();
        outFolder.mkdirs();

        //---------------------------------------------------

        //headers
        List<String> bugListHeader = Arrays.asList("dataset", "system", "bug_id");
        List<String> statsHeader = Arrays.asList("dataset", "system",
                "query_id", "bug_id", "strategy", "rank", "rank_diff", "avg_prec");

        File bugsFile =
                Paths.get(combinationTempFolder.getAbsolutePath(), "bug-list" + fileSuffix + ".csv").toFile();
        File statsFile = Paths.get(combinationTempFolder.getAbsolutePath(),
                "overall-stats" + fileSuffix + ".csv").toFile();
        CSVHelper.writeCsv(bugsFile, bugListHeader, combinationBugs, null, Function.identity(), CSVHelper
                .DEFAULT_SEPARATOR);
        CSVHelper.writeCsv(statsFile, statsHeader, combinationStats, null, Function.identity(), CSVHelper
                .DEFAULT_SEPARATOR);

        //---------------------------------------------------

        runRScript(bugsFile, statsFile, outFolder, combination, approach, reformStrategy, rankThreshold);
    }

    private void runRScript(File bugsFile, File statsFile, File outFolder, List<String> combination, String
            approach, String reformStrategy, String rankThreshold) throws Exception {

        if (RStatsRunner.NO_TOPK_REMOVAL.equals(rankThreshold)) {
            rankThreshold = "10";
        }


        //-d $IN_DIR/overall-stats.csv -l $IN_DIR/bug-list.csv -o $OUT_DIR -s $query_strategy
        String[] cmdarray = {"Rscript", scriptPath, "-d", statsFile.getAbsolutePath(), "-l", bugsFile
                .getAbsolutePath(), "-o", outFolder.getAbsolutePath(), "-s", reformStrategy, "-t", approach,
                "-n", rankThreshold};
        //LOGGER.debug(Arrays.toString(cmdarray));
        Process process = new ProcessBuilder(cmdarray)
                .redirectErrorStream(true)
                .start();

        //------------------------------

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(process.getInputStream()));

        StringBuilder processLogBuilder = new StringBuilder();
        String inLine;
        // read the output from the command
        while ((inLine = stdInput.readLine()) != null) {
            processLogBuilder.append(inLine);
            processLogBuilder.append("\n");
        }

        final String processLog = processLogBuilder.toString();
        //------------------------------


        File logFile = Paths.get(outFolder.getAbsolutePath(), "log.txt").toFile();
        final String commandString = String.join(" ", cmdarray);
        FileUtils.write(logFile, commandString, Charset.defaultCharset(), true);
        FileUtils.write(logFile, processLog, Charset.defaultCharset());
        //------------------------------

        process.waitFor();
        final int exitValue = process.exitValue();
        if (exitValue != 0) {
            throw new RuntimeException("Error executing " + commandString);
        }

        //------------------------------

    }

}
