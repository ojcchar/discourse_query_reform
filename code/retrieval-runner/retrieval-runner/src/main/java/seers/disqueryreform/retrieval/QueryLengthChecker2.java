package seers.disqueryreform.retrieval;

import edu.wayne.cs.severe.ir4se.processor.controllers.RetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.CorpusException;
import edu.wayne.cs.severe.ir4se.processor.exception.QueryException;
import edu.wayne.cs.severe.ir4se.processor.exception.RelJudgException;
import edu.wayne.cs.severe.ir4se.processor.utils.ExceptionUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QueryLengthChecker2 {

	// static String queriesFolder =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/buglocator-preprocessed";
	// static String[] systems = { "aspectj-1.5.3", "eclipse-3.1", "swt-3.1" };

	static String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/lobster-queries-preprocessed";
	static String[] systems = { "argouml-0.22", "bookkeeper-4.1.0", "derby-10.7.1.1", "derby-10.9.1.0",
			"hibernate-3.5.0b2", "jabref-2.6", "jedit-4.3", "lucene-4.0", "mahout-0.8", "openjpa-2.0.1",
			"openjpa-2.2.0", "pig-0.11.1", "pig-0.8.0", "solr-4.4.0", "tika-1.3", "zookeeper-3.4.5" };

	public static void main(String[] args) throws Exception {

		RetrievalParser parser = new DefaultRetrievalParser();

		for (String system : systems) {
			String queriesFilePath = queriesFolder + File.separator + system + "_OB" + File.separator + system
					+ "_OB_Queries.txt";
			String originalQueriesFilePath = queriesFolder + File.separator + system + "_OB" + File.separator + system
					+ "_OB_Original_Queries.txt";
			List<Query> queries = parser.readQueriesWithKey(queriesFilePath, originalQueriesFilePath);

			for (Query query : queries) {

				String txt = query.getTxt();
				int length = txt.split(" ").length;

				System.out.println(system + ";" + query.getKey() + ";" + length);
			}
		}

	}

	private static RetrievalParser getParser() {
		return new RetrievalParser() {

			@Override
			public Map<Query, RelJudgment> readRelevantJudgments(String releJudgmentPath, List<RetrievalDoc> corpusDocs)
					throws RelJudgException {
				return null;
			}

			@Override
			public Map<Query, RelJudgment> readReleJudgments(String releJudgmentPath, String mapDocsPath)
					throws RelJudgException {
				return null;
			}

			@Override
			public List<Query> readQueriesWithKey(String queriesPath, String originalQueriesFilePath)
					throws QueryException {

				List<Query> queryList = new ArrayList<Query>();
				File fileQuery = new File(queriesPath);

				if (!fileQuery.isFile() || !fileQuery.exists()) {
					throw new QueryException("Query file (" + queriesPath + ") is not valid!");
				}

				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(fileQuery));

					String line;
					int lineNumber = 0;
					Integer queryId = 0;
					String key = null;

					Query query = new Query();
					while ((line = reader.readLine()) != null) {

						// it is not a blank line
						String lineTrimmed = line.trim();
						if (!lineTrimmed.isEmpty()) {
							lineNumber++;
							switch (lineNumber) {
							case 1:
								if (lineTrimmed.contains(" ")) {
									queryId = Integer.valueOf(lineTrimmed.split(" ")[0]);
									key = lineTrimmed.split(" ")[1];
								} else {
									queryId = Integer.valueOf(lineTrimmed);
								}

								query.setQueryId(queryId);
								query.setKey(key);
								// queryId++;
								break;
							case 2:
								query.setTxt(lineTrimmed);
								queryList.add(query);
								break;
							}
						} else {
							lineNumber = 0;
							query = new Query();
						}
					}

				} catch (Exception e) {
					QueryException e2 = new QueryException(e.getMessage());
					ExceptionUtils.addStackTrace(e, e2);
					throw e2;
				} finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (IOException e) {
							throw new QueryException(e.getMessage());
						}
					}
				}
				return queryList;
			}

			@Override
			public List<Query> readQueries(String queriesFilePath, String queriesExcludedFilePath)
					throws QueryException {
				return null;
			}

			@Override
			public List<Query> readQueries(String queriesPath) throws QueryException {
				return null;
			}

			@Override
			public List<RetrievalDoc> readCorpus(String corpFilePath, String mapDocsPath) throws CorpusException {
				return null;
			}

			@Override
			public List<RetrievalDoc> readCorpus(String corpFilePath) throws CorpusException {
				return null;
			}
		};
	}

}
