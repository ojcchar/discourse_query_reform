package seers.disqueryreform.retrieval.lucene;

import edu.wayne.cs.severe.ir4se.processor.controllers.impl.RAMRetrievalIndexer;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.lucene.LuceneRetrievalSearcher;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.IndexerException;
import edu.wayne.cs.severe.ir4se.processor.exception.SearchException;
import org.apache.lucene.store.Directory;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.retrieval.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class LuceneRetriever extends ApproachRetriever {

    public LuceneRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath) {
        super(resultsWriter, existingDataSetsPath, RetrievalTechnique.LUCENE, true);
    }

    private void runAndOutputQuery(DiscourseQuery discourseQuery, LuceneRetrievalSearcher searcher, Corpus corpus) {
        try {
            Query query = new Query(discourseQuery.getId(), discourseQuery.getText());
            List<RetrievalDoc> retrievedDocs = searcher.searchQuery(query);
            RelJudgment relJudgment = corpus.createRelevanceJudgment(discourseQuery);
            writeQueryResults(new RetrievalResults(corpus.getProject(), discourseQuery,
                    retrievedDocs, relJudgment, RetrievalTechnique.LUCENE));
        } catch (Exception | Error e) {
            throw new Error(String.format("Error processing query %s with Lucene", discourseQuery), e);
        }
    }

    @Override
    protected void runForCorpus(Corpus corpus) {
        try (Directory index = new RAMRetrievalIndexer().buildIndex(corpus.getDocuments(), null)) {

            LuceneRetrievalSearcher searcher = new LuceneRetrievalSearcher(index, null);

            corpus.getQueries().parallelStream()
                    .forEach(q -> runAndOutputQuery(q, searcher, corpus));
        } catch (IndexerException | IOException | SearchException e) {
            throw new Error(e);
        }
    }

}
