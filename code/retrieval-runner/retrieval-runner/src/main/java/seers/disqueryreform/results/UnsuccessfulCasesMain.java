package seers.disqueryreform.results;

import net.quux00.simplecsv.CsvWriter;
import org.apache.commons.io.FileUtils;
import seers.appcore.csv.CSVHelper;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.QueryReformStrategy;
import seers.disqueryreform.results.rstats.RStatsRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UnsuccessfulCasesMain {

    private static Set<String> techniques = JavaUtils.getSet("locus", "lucene", "brtracer", "buglocator", "lobster");
    private static List<String> thresholds = RStatsRunner.getRankThresholds(30, false);
    private static List<QueryReformStrategy> reformStrategies = Arrays.asList(QueryReformStrategy.OB_TITLE,
            QueryReformStrategy.OB_S2R_TITLE);

    public static void main(String[] args) throws IOException {

        File basePath = Paths.get(RStatsRunner.getResultsFolder(RStatsRunner.getDefaultBaseFolder()),
                "all_granularities").toFile();
        File outFile = Paths.get(basePath.getAbsolutePath(), "all_results.csv").toFile();
        File outFileUnique = Paths.get(basePath.getAbsolutePath(), "all_results_unique.csv").toFile();

        if (outFile.exists()) {
            FileUtils.forceDelete(outFile);
        }

        LinkedHashSet<String> uniqueBugs = new LinkedHashSet<>();

        try (CsvWriter writer = CSVHelper.getWriter(outFile, CSVHelper.DEFAULT_SEPARATOR, true)) {

            for (String technique : techniques) {

                for (QueryReformStrategy reformStrategy : reformStrategies) {

                    for (String threshold : thresholds) {

                        File resultsFile = Paths.get(basePath.getAbsolutePath(), technique, reformStrategy.toString(),
                                threshold, "results__" + technique + "_(0, inf]_" + reformStrategy + ".csv").toFile();

                        final List<List<String>> content = CSVHelper.readCsv(resultsFile, true, Function.identity(),
                                CSVHelper.DEFAULT_SEPARATOR, "UTF-8");


                        final List<List<String>> newContent = content.stream().map(l -> {

                            String cat1 = l.get(10);
                            String cat2 = l.get(18);

                            String expCat = "1-" + threshold;

                            if (cat1.equals(expCat) && !cat2.equals(expCat)) {
                                List<String> nL = new LinkedList<>(Arrays.asList(technique, reformStrategy.toString(),
                                        threshold));
                                nL.addAll(l);
                                return nL;
                            }

                            return null;
                        }).filter(Objects::nonNull).collect(Collectors.toList());

                        final Set<String> unBugs = newContent.stream().map(l -> {
                            final String format = String.format("%s;%s;%s", l.get(3),
                                    l.get(4), l.get(5));
                            return format + ";\"" + format + "\"";
                        })
                                .collect(Collectors.toSet());
                        uniqueBugs.addAll(unBugs);

                        writer.writeAll(newContent);

                    }

                }
            }
        }

        final String content = String.join("\n", uniqueBugs);
        FileUtils.write(outFileUnique, content, Charset.defaultCharset());
    }
}
