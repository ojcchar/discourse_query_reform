package seers.disqueryreform.evaluation.stats.output;

import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.stats.ReformulationAccumulation;
import seers.disqueryreform.evaluation.stats.ReformulationAccumulationGroup;
import seers.disqueryreform.evaluation.stats.keys.EvaluationKey;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.stream.Stream;

import static edu.utdallas.seers.ir4se.evaluation.RetrievalAccumulation.AccumulatedMetric;
import static seers.disqueryreform.evaluation.stats.EvaluationStatsTable.TableEntry;

public class StatsTableWriter
        extends AggregatedTableWriter<TableEntry, ReformulationAccumulation, AccumulatedMetric, EvaluationKey> {

    private final ReformulationAccumulationGroup allGranularitiesStats;
    private final RetrievalTechnique technique;
    private final ComponentsForBugTable componentsTable;
    private final StrategyType strategyType;
    private final boolean simplifiedEvaluation;

    public StatsTableWriter(ReformulationAccumulationGroup allGranularitiesStats,
                            RetrievalTechnique technique, ComponentsForBugTable componentsForBugTable,
                            StrategyType strategyType, boolean simplifiedEvaluation) {
        this.allGranularitiesStats = allGranularitiesStats;
        this.technique = technique;
        componentsTable = componentsForBugTable;
        this.strategyType = strategyType;
        this.simplifiedEvaluation = simplifiedEvaluation;
    }

    private Stream<Header<TableEntry>> createHeadersForMetric(AccumulatedMetric metric) {
        return createHeadersForMetric(metric.name(), metric);
    }

    private String formatStrategyName(String strategyName) {
        if (simplifiedEvaluation) {
            return strategyName.replace("T_F_", "T_");
        } else {
            return strategyName;
        }
    }

    /**
     * Adds the rows corresponding to accumulated granularity.
     *
     * @param rows The rows that have been initially passed for printing.
     * @return Many rows.
     */
    @Override
    protected Stream<TableEntry> modifyRows(Stream<TableEntry> rows) {
        if (simplifiedEvaluation) {
            return rows;
        }

        return Stream.concat(
                rows,
                allGranularitiesStats.entries()
                        .filter(e -> e.getKey().getTechnique() == technique)
                        .map(e -> new TableEntry(e.getKey().toFullKey(), e.getValue()))
        );
    }

    @Override
    protected Stream<Stream<Header<TableEntry>>> createHeaders() {
        return Stream.of(
                createSimpleHeader("Technique", e -> e.getKey().getTechnique()),
                createSimpleHeader("Strategy",
                        e -> formatStrategyName(componentsTable.createNameSortedByApplicability(e.getKey().getStrategy(), strategyType))),
                createSimpleHeader("Config", e -> ComponentsForBugTable.extractConfiguration(e.getKey().getStrategy()).orElse("")),
                createSimpleHeader("Granularity", e -> e.getKey().getGranularity()),
                createSimpleHeader("N", e -> e.getKey().getThreshold()),
                createSimpleHeader("q", e -> e.getStats().getIdenticalMetric(AccumulatedMetric.TOTAL_QUERIES)),
                createHeadersForMetric(AccumulatedMetric.HITS_PERCENTAGE),
                createHeadersForMetric(AccumulatedMetric.MAP),
                createHeadersForMetric(AccumulatedMetric.MRR),
                createHeadersForMetric(AccumulatedMetric.TOTAL_HITS)
        );
    }
}
