package seers.disqueryreform.evaluation.stats.keys;

import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.Objects;

public class GTSTKey {

    private final DocumentGranularity granularity;
    private final RetrievalTechnique technique;
    private final int threshold;
    private final String strategy;

    public GTSTKey(DocumentGranularity granularity, RetrievalTechnique technique, String strategy, int threshold) {
        this.granularity = granularity;
        this.technique = technique;
        this.strategy = strategy;
        this.threshold = threshold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GTSTKey gtstKey = (GTSTKey) o;
        return threshold == gtstKey.threshold &&
                granularity == gtstKey.granularity &&
                technique == gtstKey.technique &&
                strategy.equals(gtstKey.strategy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(granularity, technique, threshold, strategy);
    }

    public DocumentGranularity getGranularity() {
        return granularity;
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }

    public String getStrategy() {
        return strategy;
    }

    public int getThreshold() {
        return threshold;
    }


}
