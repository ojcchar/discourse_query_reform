package seers.disqueryreform.retrieval.bench4bl.locus;

import seers.disqueryreform.base.DataSet;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

public class LocusReformulationRunner {
//    private static Path reformulatedQueriesPath = Paths.get("/home/juan/Source/discourse_query_reform/data/tasknav_reformulated_queries/TaskNav-true-true/B4BL");
//    private static Path originalDataPath = Paths.get("/home/juan/Source/discourse_query_reform/data/existing_data_sets/bench4bl_data");
//    private static Path outputPath = Paths.get("/tmp/aaa");

    private static Path reformulatedQueriesPath = Paths.get("/home/juan/Source/discourse_query_reform/data/tasknav_reformulated_queries/TaskNav-true-true/BRT");
    private static Path originalDataPath = Paths.get("/home/juan/Source/discourse_query_reform/data/existing_data_sets/buglocator_data");
    private static Path outputPath = Paths.get("/tmp/bbb");


    public static void main(String[] args) throws Exception {
        if (args.length == 3) {
            reformulatedQueriesPath = Paths.get(args[0]);
            originalDataPath = Paths.get(args[1]);
            outputPath = Paths.get(args[2]);
        }

        new LocusReformulationEvaluator().evaluateApproach(reformulatedQueriesPath, originalDataPath, outputPath,
                Arrays.asList(15, 30), Collections.singletonList("CODE--OB--S2R--TITLE--T_V_OTHER"),
                Paths.get("/home/juan/Source/discourse_query_reform/"), DataSet.BRT);
    }
}
