package seers.disqueryreform.retrieval.lucene;

import com.google.gson.Gson;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalWriter;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalStats;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import edu.wayne.cs.severe.ir4se.processor.exception.WritingException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.utils.FilePathUtils;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.DataSet;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Deprecated
public class LuceneMassiveMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuceneMassiveMain.class);

    private static String corpusFolder = "";
    private static String outputDir = "";

    static List<Integer> rankThresholds;

    public static Map<String, Integer> queriesPerStrategy;


    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void main(String[] args) throws Exception {

        String queriesFolder = args[0];
        corpusFolder = args[1];
        outputDir = args[2];
        String baselineStrategy = args[3];
        List<String> strategies = Arrays.stream(args[4].split(","))
                .collect(Collectors.toList());
        rankThresholds = JavaUtils.getSet(args[5].split(",")).stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        rankThresholds.sort(Comparator.comparing(Integer::intValue));
        DataSet dataSet = DataSet.valueOf(args[6]);
        // -----------------------------------------
        ConcurrentHashMap<String, List<RetrievalDoc>> systemCorpus = new ConcurrentHashMap<>();

        List<Query> baselineQueries = readQueries(queriesFolder, baselineStrategy);
        LOGGER.debug("# of baseline queries: " + baselineQueries.size());
        queriesPerStrategy.put(baselineStrategy,
                queriesPerStrategy.getOrDefault(baselineStrategy, 0) + baselineQueries.size());

        for (String strategy : strategies) {
            LOGGER.debug("Strategy: " + strategy);

            List<Query> reducedQueries = readQueries(queriesFolder, strategy);
            LOGGER.debug("# of reduced queries: " + reducedQueries.size());
            queriesPerStrategy.put(strategy,
                    queriesPerStrategy.getOrDefault(strategy, 0) + reducedQueries.size());

            String strategyOutputPath = outputDir + File.separator + strategy;
            File strategyOutDir = new File(strategyOutputPath);

            /* This exists to support interrupted execution, if the target exists just load stats
             * and continue with the next one, assuming it was already computed */
            if (strategyOutDir.exists()) {
                // We already computed the strategy and the queries per strategy value was read
                LOGGER.info("Results already exist, continuing...");
                continue;
            }

            strategyOutDir.mkdirs();

            //--------------------------------------------------------

            //initialize the map that will store the query metrics for each rank threshold
            //rank threshold ->  query -> <baseline metrics, reformulation metrics>
            Map<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsPerK = new
                    ConcurrentHashMap<>();
            for (Integer rankThreshold : rankThresholds) {
                queryEvalsPerK.put(rankThreshold, new ConcurrentHashMap<>());
            }

            //initialize the map that will store the original query metrics from original rankings (i.e, with no
            // modifications
            //query -> <baseline metrics, reformulation metrics>
            Map<Query, ImmutablePair<List<Double>, List<Double>>> originalQueryEvals = new ConcurrentHashMap<>();

            //--------------------------------------------------------

            ThreadParameters params = new ThreadParameters();
            params.addParam("corpusBaseFolder", corpusFolder);
            params.addParam("baselineQueries", baselineQueries);
            params.addParam("queryEvalsPerK", queryEvalsPerK);
            params.addParam("originalQueryEvals", originalQueryEvals);
            params.addParam("dataSet", dataSet);
            params.addParam("systemCorpus", systemCorpus);

            ThreadExecutor.executePaginated(reducedQueries, LuceneProcessor.class, params, 8);

            writeStats3(strategy, queryEvalsPerK, originalQueryEvals, baselineStrategy,
                    strategyOutputPath);
        }

        LOGGER.debug("Done!");
    }

    @SuppressWarnings("unchecked")
    public static List<Query> readQueries(String queriesFolder, String strategy) throws IOException {
        Gson gson = new Gson();
        return Collections.emptyList();

//        try (FileReader reader = new FileReader(Paths.get(queriesFolder, strategy + ".json").toFile())) {
//            return ((StrategyQueryContainer<Map>) gson.fromJson(reader, StrategyQueryContainer.class)).getItems().stream()
//                    .flatMap(e -> e.getValue().stream()
//                            .map(q -> {
//                                String text = q.containsKey("text") ? ((String) q.get("text")).trim() : null;
//
//                                if (text == null || text.isEmpty()) {
//                                    LOGGER.warn("Voided query: " + e.getKey() + ";" + q.get("bug_id"));
//                                    return null;
//                                }
//
//                                Query query = new Query(((Double) q.get("id")).intValue(), text);
//                                query.setKey(q.get("bug_id").toString());
//                                query.addInfoAttribute("method_goldset", q.get("method_goldset"));
//                                query.addInfoAttribute("class_goldset", q.get("class_goldset"));
//                                query.addInfoAttribute("file_goldset", q.get("file_goldset"));
//                                query.addInfoAttribute("system", e.getKey());
//
//                                return query;
//                            })
//                            .filter(Objects::nonNull))
//                    .collect(Collectors.toList());
//        }
    }

    public static void writeStats3(String strategyName, Map<Integer,
            Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsPerK,
                                   Map<Query, ImmutablePair<List<Double>, List<Double>>> originalQueryEvals, String
                                           baselineStrategy, String outputFolder) throws
            EvaluationException, WritingException {

        DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();
        DefaultRetrievalWriter writer = new DefaultRetrievalWriter();

        //------------------------------------------

        //get the metrics for the baseline and reformulation

        Map<Query, List<Double>> baselineQueryEvals = originalQueryEvals.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().left));
        Map<Query, List<Double>> reformStrategyQueryEvals = originalQueryEvals.entrySet().stream().collect
                (Collectors.toMap(Map.Entry::getKey, e -> e.getValue().right));

        //compute and write stats for the baseline and reformulation
        RetrievalStats baselineStats = evaluator.evaluateModel(baselineQueryEvals);
        writer.writeStats(baselineStats, FilePathUtils.getFile(new File(outputFolder),
                baselineStrategy + "-stats.csv").getAbsolutePath());

        RetrievalStats reformulationStats = evaluator.evaluateModel(reformStrategyQueryEvals);
        writer.writeStats(reformulationStats, FilePathUtils.getFile(new File(outputFolder),
                strategyName + "-stats.csv").getAbsolutePath());

        //------------------------------------------

        //for each threshold
        for (Map.Entry<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsK :
                queryEvalsPerK.entrySet()) {

            Integer threshold = queryEvalsK.getKey();
            Set<Map.Entry<Query, ImmutablePair<List<Double>, List<Double>>>> entries = queryEvalsK.getValue()
                    .entrySet();

            File kOutputFolder = new File(outputFolder + File.separator + "threshold-" + threshold);
            kOutputFolder.mkdirs();

            //--------------------------------------------
            //get the metrics for the baseline and reformulation

            Map<Query, List<Double>> baselineKQueryEvals = entries.stream().collect(Collectors.toMap(Map
                    .Entry::getKey, e -> e.getValue().left));
            Map<Query, List<Double>> reformStrategyKQueryEvals = entries.stream().collect(Collectors.toMap(Map
                    .Entry::getKey, e -> e.getValue().right));

            //--------------------------------------------
            //compute and write stats for the baseline and reformulation

            RetrievalStats baselineKStats = evaluator.evaluateModel(baselineKQueryEvals);
            writer.writeStats(baselineKStats, FilePathUtils.getFile(kOutputFolder,
                    baselineStrategy + "-stats.csv").getAbsolutePath());

            RetrievalStats reformulationKStats = evaluator.evaluateModel(reformStrategyKQueryEvals);
            writer.writeStats(reformulationKStats, FilePathUtils.getFile(kOutputFolder,
                    strategyName + "-stats.csv").getAbsolutePath());
        }
    }

}
