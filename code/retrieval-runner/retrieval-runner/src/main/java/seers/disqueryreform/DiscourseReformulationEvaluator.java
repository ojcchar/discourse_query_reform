package seers.disqueryreform;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.action.StoreTrueArgumentAction;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.DetailedResultsWriter;
import seers.disqueryreform.evaluation.ThresholdEvaluator;
import seers.disqueryreform.evaluation.stats.*;
import seers.disqueryreform.evaluation.stats.keys.GranularityTechniquePair;
import seers.disqueryreform.evaluation.stats.output.OverallSummaryTableWriter;
import seers.disqueryreform.evaluation.stats.output.StatsTableWriter;
import seers.disqueryreform.evaluation.stats.output.SummaryTableWriter;
import seers.disqueryreform.querygen.tasknav.DiscourseTasksQueryGenerator;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DiscourseReformulationEvaluator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiscourseReformulationEvaluator.class);

    private static final List<Integer> THRESHOLDS = Arrays.asList(5, 10, 15, 20, 25, 30);
    private static final String BRP_NAME = "baseRepositoryPath";
    private static final String RSP_NAME = "resultSourcePath";
    private static final String OUTPUT_DIR_PARAM_NAME = "outputDir";

    public static void main(String[] args) {
        ArgumentParser parser =
                ArgumentParsers.newArgumentParser("DiscourseReformulationEvaluator", true);

        parser.addArgument(BRP_NAME);
        parser.addArgument(RSP_NAME)
                .help("Source of results for the evaluation. Can be a directory with result " +
                        "lines files or an evaluation stats file");
        parser.addArgument(OUTPUT_DIR_PARAM_NAME);
        parser.addArgument("-f")
                .help("Recompute even if the evaluation already exists")
                .action(new StoreTrueArgumentAction());

        parser.addArgument("-s")
                .help("Only use true-false configuration and full tasks")
                .action(new StoreTrueArgumentAction());

        List<String> strategyTypesString = Arrays.stream(StrategyType.values())
                .map(String::valueOf)
                .collect(Collectors.toList());

        parser.addArgument("-e")
                .help("Evaluation types to run")
                .nargs("+")
                .choices(strategyTypesString)
                .setDefault(strategyTypesString);

        Namespace namespace = null;

        try {
            namespace = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        // SET UP
        // ARGS
        Path baseRepositoryPath = Paths.get(namespace.getString(BRP_NAME)).toAbsolutePath();
        Path resultSource = Paths.get(namespace.getString(RSP_NAME)).toAbsolutePath();
        Path outputDirPath = Paths.get(namespace.getString(OUTPUT_DIR_PARAM_NAME)).toAbsolutePath();
        boolean forceRecompute = namespace.getBoolean("f");
        boolean simplifiedEvaluation = namespace.getBoolean("s");

        List<StrategyType> strategyTypes = namespace.<String>getList("e").stream()
                .map(StrategyType::valueOf)
                .distinct()
                .collect(Collectors.toList());

        new DiscourseReformulationEvaluator().evaluate(baseRepositoryPath, resultSource,
                outputDirPath, forceRecompute, strategyTypes, simplifiedEvaluation);
    }

    private void evaluate(Path baseRepositoryPath, Path resultSource, Path outputDirPath,
                          boolean forceRecompute, List<StrategyType> strategyTypes, boolean simplifiedEvaluation) {
        for (StrategyType strategyType : strategyTypes) {
            executeEvaluation(baseRepositoryPath, resultSource, outputDirPath, forceRecompute, strategyType, simplifiedEvaluation);
        }

        LOGGER.info("Finished evaluating results");
    }

    private void executeEvaluation(Path baseRepositoryPath, Path resultSource, Path baseOutputDir,
                                   boolean forceRecompute, StrategyType strategyType, boolean simplifiedEvaluation) {

        Path outputDir = baseOutputDir.resolve(strategyType.toString());

        EvaluationStatsTable accum = createAccumulatedStats(baseRepositoryPath, resultSource, forceRecompute, strategyType, simplifiedEvaluation, outputDir);

        ComponentsForBugTable componentsForBugTable =
                new DiscourseTasksQueryGenerator(baseRepositoryPath).readComponentsForBugTable();

        writeTechniqueStats(accum, outputDir, componentsForBugTable, strategyType);
        writeAllStats(accum, outputDir, componentsForBugTable, strategyType);

        writeSummary(accum, outputDir, componentsForBugTable, strategyType);

        LOGGER.info("Finished evaluation");
    }

    private EvaluationStatsTable createAccumulatedStats(Path baseRepositoryPath, Path resultSource,
                                                        boolean forceRecompute, StrategyType strategyType,
                                                        boolean simplifiedEvaluation, Path outputDir) {

        DetailedResultsWriter detailedResultsWriter = DetailedResultsWriter.newDetailedResultsWriter(outputDir);

        EvaluationStatsTable accum;

        if (forceRecompute || !detailedResultsWriter.hasAccumulatedStats()) {
            ThresholdEvaluator thresholdEvaluator =
                    ThresholdEvaluator.create(THRESHOLDS, baseRepositoryPath, resultSource, outputDir, strategyType);

            accum = EvaluationStatsTable.accumulateAllResults(thresholdEvaluator);

            detailedResultsWriter.writeAccumulatedStats(accum);
        } else {
            accum = detailedResultsWriter.loadAccumulatedStats();
        }

        if (simplifiedEvaluation) {
            LOGGER.info("Simplifying accumulated results");
            return accum.simplify();
        } else {
            return accum;
        }
    }

    private void writeAllStats(EvaluationStatsTable accum, Path outputDir, ComponentsForBugTable componentsForBugTable, StrategyType strategyType) {
        Path outFile = outputDir.resolve("all-stats.csv");

        LOGGER.info("Writing simplified stats to " + outFile);

        new StatsTableWriter(null, null, componentsForBugTable, strategyType, true)
                .writeTable(accum.allStatsByTechniqueAndStrategy()
                                .flatMap(TechniqueStrategyCollection::allEntries),
                        outFile
                );
    }

    private void writeTechniqueStats(EvaluationStatsTable accum, Path outputDirPath,
                                     ComponentsForBugTable componentsForBugTable, StrategyType strategyType) {
        ReformulationAccumulationGroup allGranularitiesStats = accum.groupByGranularity();

        accum.allStatsByTechniqueAndStrategy().forEach(c -> {
            RetrievalTechnique technique = c.getTechnique();
            Path outputFilePath = outputDirPath.resolve(technique + "-stats.csv");

            LOGGER.info(String.format("Writing %s stats to %s", technique, outputFilePath));

            new StatsTableWriter(allGranularitiesStats, technique, componentsForBugTable, strategyType, false)
                    .writeTable(c.allEntries(), outputFilePath);
        });
    }

    private void writeSummary(EvaluationStatsTable accum, Path outputDirPath, ComponentsForBugTable table, StrategyType strategyType) {
        ThresholdSummary summary = accum.summarizeThresholds();

        summary.entries()
                .forEach(e -> {
                    GranularityTechniquePair key = e.getKey();

                    Path outputFilePath = outputDirPath.resolve(
                            String.format("%s-%s-summary.csv",
                                    key.getGranularity(), key.getTechnique())
                    );

                    LOGGER.info(String.format("Writing %s summary to %s",
                            key, outputFilePath));

                    new SummaryTableWriter(table, strategyType).writeTable(e.getSummaries().stream(), outputFilePath);
                });

        OverallStrategySummary overallSummary = summary.toOverallStrategySummary();
        Path overallSummaryFile = outputDirPath.resolve("overall-summary.csv");

        LOGGER.info("Writing overall summary to " + overallSummaryFile);

        new OverallSummaryTableWriter(overallSummary, table, strategyType)
                .writeTable(overallSummary.entries(), overallSummaryFile);
    }

}
