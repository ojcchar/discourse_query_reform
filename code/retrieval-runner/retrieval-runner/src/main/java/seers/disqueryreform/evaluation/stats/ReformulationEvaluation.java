package seers.disqueryreform.evaluation.stats;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;
import seers.disqueryreform.evaluation.stats.keys.EvaluationKey;
import seers.disqueryreform.retrieval.QueryResultKey;

import java.util.Set;

import static edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation.Metric;

public class ReformulationEvaluation {
    private final EvaluationKey key;
    private final RetrievalEvaluation baselineMetrics;
    private final RetrievalEvaluation reformMetrics;
    private final QueryResultKey queryResultKey;
    private final Set<String> strategies;

    public ReformulationEvaluation(QueryResultKey queryResultKey, RetrievalEvaluation baselineMetrics, RetrievalEvaluation reformMetrics, Set<String> strategies) {
        if (baselineMetrics.getMetric(Metric.HITS_THRESHOLD) !=
                reformMetrics.getMetric(Metric.HITS_THRESHOLD)) {
            throw new IllegalArgumentException("Both metrics must be of the same threshold");
        }

        key = new EvaluationKey(queryResultKey, ((int) baselineMetrics.getMetric(Metric.HITS_THRESHOLD)));
        this.baselineMetrics = baselineMetrics;
        this.reformMetrics = reformMetrics;
        this.queryResultKey = queryResultKey;
        this.strategies = strategies;
    }

    ReformulationAccumulation toAccumulation() {
        return new ReformulationAccumulation(baselineMetrics, reformMetrics);
    }

    public double getBaselineMetric(Metric metric) {
        return baselineMetrics.getMetric(metric);
    }

    public double getReformulationMetric(Metric metric) {
        return reformMetrics.getMetric(metric);
    }

    RetrievalEvaluation getBaselineMetrics() {
        return baselineMetrics;
    }

    RetrievalEvaluation getReformMetrics() {
        return reformMetrics;
    }

    public EvaluationKey getKey() {
        return key;
    }

    public QueryResultKey getQueryResultKey() {
        return queryResultKey;
    }

    public Set<String> getStrategies() {
        return strategies;
    }
}
