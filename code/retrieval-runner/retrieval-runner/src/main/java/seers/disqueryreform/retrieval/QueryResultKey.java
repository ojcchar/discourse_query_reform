package seers.disqueryreform.retrieval;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class QueryResultKey {
    private static final String JOIN_CHARACTER = ";";

    private final Project project;
    private final String queryKey;
    private final String strategy;
    private final String taskNavConfiguration;
    private final RetrievalTechnique technique;

    public QueryResultKey(Project project, DiscourseQuery query, RetrievalTechnique technique) {
        this(project, query.getKey(), query.getStrategy(), query.getTaskNavConfiguration(), technique);
    }

    public QueryResultKey(Project project, String queryKey, String strategy,
                          String taskNavConfiguration, RetrievalTechnique technique) {
        this.project = project;
        this.queryKey = queryKey;
        this.strategy = strategy;
        this.taskNavConfiguration = taskNavConfiguration;
        this.technique = technique;
    }

    static QueryResultKey fromString(String s) {
        String[] split = s.split(JOIN_CHARACTER);

        return new QueryResultKey(
                Project.fromString(split[0]),
                split[1],
                split[2],
                split[3].isEmpty() ? null : split[3],
                RetrievalTechnique.valueOf(split[4]));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryResultKey that = (QueryResultKey) o;
        return project.equals(that.project) &&
                queryKey.equals(that.queryKey) &&
                strategy.equals(that.strategy) &&
                Objects.equals(taskNavConfiguration, that.taskNavConfiguration) &&
                technique == that.technique;
    }

    @Override
    public int hashCode() {
        return Objects.hash(project, queryKey, strategy, taskNavConfiguration, technique);
    }

    @Override
    public String toString() {
        return String.join(JOIN_CHARACTER, Arrays.asList(
                project.toString(),
                queryKey,
                strategy,
                taskNavConfiguration != null ? taskNavConfiguration : "",
                technique.toString()
        ));
    }

    public String getTaskNavConfiguration() {
        return taskNavConfiguration;
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }

    public String getQueryKey() {
        return queryKey;
    }

    public Project getProject() {
        return project;
    }

    public String getStrategy() {
        return strategy;
    }

    static TypeAdapter<QueryResultKey> getTypeAdapter() {
        return new TypeAdapter<QueryResultKey>() {
            @Override
            public void write(JsonWriter jsonWriter, QueryResultKey queryResultKey) throws IOException {
                jsonWriter.value(queryResultKey.toString());
            }

            @Override
            public QueryResultKey read(JsonReader jsonReader) throws IOException {
                return QueryResultKey.fromString(jsonReader.nextString());
            }
        };
    }
}
