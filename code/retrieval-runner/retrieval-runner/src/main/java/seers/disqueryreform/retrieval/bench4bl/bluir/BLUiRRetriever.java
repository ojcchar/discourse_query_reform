package seers.disqueryreform.retrieval.bench4bl.bluir;

import seers.disqueryreform.retrieval.bench4bl.Bench4BLRetriever;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BLUiRRetriever extends Bench4BLRetriever {
    private static final Path BLUIR_BIN_PATH =
            Paths.get("..", "..", "other-tools", "BLUiR.jar");

    public BLUiRRetriever(Path baseWorkingDirectoryPath, boolean wrapWorkingDir) {
//        super(baseWorkingDirectoryPath, wrapWorkingDir, true);
    }

    @Override
    protected String[] buildCommand(String alias, String repoDir, String sourceDir,
                                    String xmlBugReportsPath, Path workingDirPath) throws IOException {
        return new String[]{String.format("java -Xms512m -Xmx4000m -jar %s -a 0.2 -s %s -b %s -w %s -n %s",
                BLUIR_BIN_PATH.toString(),
                sourceDir,
                xmlBugReportsPath,
                workingDirPath.toString(),
                String.join("_", alias, alias).replace('.', '_'))};
    }

    @Override
    protected String getToolName() {
        return "BLUiR";
    }
}
