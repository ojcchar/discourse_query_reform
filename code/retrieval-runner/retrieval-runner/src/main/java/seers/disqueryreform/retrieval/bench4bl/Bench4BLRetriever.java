package seers.disqueryreform.retrieval.bench4bl;

import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.bench4bl.BugRepository;
import seers.disqueryreform.retrieval.SubProcessOutputCollector;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public abstract class Bench4BLRetriever {
    private static final Logger LOGGER = LoggerFactory.getLogger(Bench4BLRetriever.class);
    private final boolean wrapWorkingDir = true;
    private final Path baseWorkingDirectoryPath = Paths.get(FileUtils.getTempDirectoryPath());
    private boolean cleanUpAfter = false;

    public Bench4BLRetriever() {
    }

    public Bench4BLRetriever(boolean cleanUpAfter) {
        this.cleanUpAfter = cleanUpAfter;
    }

    public Bench4BLResults retrieve(String alias, String repoDir, String sourceDir,
                                    List<BugReport> queries)
            throws IOException, InterruptedException, EvaluationException, JAXBException {
        Path actualWorkingDirPath = baseWorkingDirectoryPath.resolve(
                String.format("%s_%s_%s", getToolName(), alias, alias).replace('.', '_'));

        Path workingDirPath;
        if (wrapWorkingDir) {
            workingDirPath = actualWorkingDirPath;

            FileUtils.forceMkdirParent(actualWorkingDirPath.toFile());
        } else {
            workingDirPath = baseWorkingDirectoryPath;
        }

        String xmlBugReportsPath = Paths.get(FileUtils.getTempDirectoryPath(), String.format("%s-bug-reports.xml", alias)).toString();
        new BugRepository(alias, queries).writeXMLRepository(xmlBugReportsPath);
        String[] command = buildCommand(alias, repoDir, sourceDir, xmlBugReportsPath, workingDirPath);

        Process tool = new ProcessBuilder(command)
                .start();

        // Thanks to java runtime exec API this must be read in threads or it will deadlock
        SubProcessOutputCollector stdOut = new SubProcessOutputCollector(tool.getInputStream());
        SubProcessOutputCollector stdErr = new SubProcessOutputCollector(tool.getErrorStream());

        try {
            stdOut.start();
            stdErr.start();

            int exitValue = tool.waitFor();

            stdOut.join();
            stdErr.join();

            StringBuilder outputBuilder =
                    new StringBuilder(String.format("================Tool output for %s\n", alias))
                            .append(stdOut.getOutput());

            String errorOutput = stdErr.getOutput();

            if (errorOutput != null && errorOutput.length() > 1) {
                outputBuilder.append("\n----------------Errors:\n\n").append(errorOutput);

                if (exitValue != 0) {
                    outputBuilder.append("\nTool exit value was not 0");
                }

                LOGGER.error(outputBuilder.toString());
            } else {
                LOGGER.info(outputBuilder.toString());
            }

            if (exitValue != 0) {
                // Deleting this so that it doesn't interfere with subsequent runs
                FileUtils.deleteDirectory(workingDirPath.toFile());
                throw new EvaluationException("Tool execution failed");
            }

            return Bench4BLResults.loadBench4BLResults(workingDirPath, cleanUpAfter);
        } finally {
            if (cleanUpAfter) {
                cleanUp(xmlBugReportsPath);
            }
        }

    }

    protected void cleanUp(String xmlBugReportsPath) throws IOException {
        FileUtils.forceDelete(new File(xmlBugReportsPath));
    }

    protected abstract String[] buildCommand(String alias, String repoDir,
                                             String sourceDir, String xmlBugReportsPath, Path workingDirPath) throws IOException;

    protected abstract String getToolName();

}