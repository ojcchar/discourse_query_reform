package seers.disqueryreform.retrieval.lobster;

import net.quux00.simplecsv.CsvWriter;
import net.quux00.simplecsv.CsvWriterBuilder;
import seers.disqueryreform.base.D4JUtils;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.stream.Collectors;

@Deprecated
public class NonObProcessor {

	static String resultsFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/brtracer/overall-stats-or.csv";

	public static void main(String[] args) throws Exception {

		File inputFile = new File(resultsFile);
		List<List<String>> lines = D4JUtils.readLines(inputFile, ';');

		List<List<String>> nonObLines = lines.stream().filter(l -> l.get(3).equals("EB_S2R_OTHER"))
				.collect(Collectors.toList());
		List<List<String>> obLines = lines.stream().filter(l -> l.get(3).equals("OB")).collect(Collectors.toList());
		List<List<String>> allTextLines = lines.stream().filter(l -> l.get(3).equals("ALL_TEXT"))
				.collect(Collectors.toList());
		
		List<List<String>> nonObLines2 = nonObLines.stream()
				.filter(l -> obLines.stream().anyMatch(
						nol -> nol.get(0).toLowerCase().equals(l.get(0).toLowerCase()) && nol.get(2).equals(l.get(2))))
				.collect(Collectors.toList());

		List<List<String>> allTextLinesOnlynonOB = allTextLines.stream()
				.filter(l -> nonObLines2.stream().anyMatch(
						nol -> nol.get(0).toLowerCase().equals(l.get(0).toLowerCase()) && nol.get(2).equals(l.get(2))))
				.collect(Collectors.toList());

		List<List<String>> ObLinesOnlynonOB = obLines.stream()
				.filter(l -> nonObLines2.stream().anyMatch(
						nol -> nol.get(0).toLowerCase().equals(l.get(0).toLowerCase()) && nol.get(2).equals(l.get(2))))
				.collect(Collectors.toList());

		String outFile = inputFile.getAbsolutePath() + ".non_ob.csv";
		try (CsvWriter wr = new CsvWriterBuilder(new FileWriter(outFile)).separator(';').build();) {
			wr.writeNext(lines.get(0));
			wr.writeAll(allTextLinesOnlynonOB);
			wr.writeAll(ObLinesOnlynonOB);
			wr.writeAll(nonObLines2);
		}

	}

}
