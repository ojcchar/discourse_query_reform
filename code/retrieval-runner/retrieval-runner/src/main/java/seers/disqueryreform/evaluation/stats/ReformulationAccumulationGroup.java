package seers.disqueryreform.evaluation.stats;

import seers.disqueryreform.evaluation.stats.keys.GranularityGroupKey;

import java.util.Map;
import java.util.stream.Stream;

public class ReformulationAccumulationGroup {

    private final Map<GranularityGroupKey, ReformulationAccumulation> grouping;

    public ReformulationAccumulationGroup(Map<GranularityGroupKey, ReformulationAccumulation> grouping) {
        this.grouping = grouping;
    }

    public Stream<Map.Entry<GranularityGroupKey, ReformulationAccumulation>> entries() {
        return grouping.entrySet().stream();
    }
}
