package seers.disqueryreform.retrieval.bl.brt;

import edu.utdallas.seers.buglocator.BugLocator;
import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.ir4se.retrieval.Retriever;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.SearchException;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.search.ScoreDoc;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.retrieval.*;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class BLBRTRetriever extends ApproachRetriever {
    private static final float ALPHA = 0.2F;
    private final Path existingDataSetsPath;
    private final RetrievalTechnique approach;

    BLBRTRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath, RetrievalTechnique approach) {
        super(resultsWriter, existingDataSetsPath, approach, false);
        this.existingDataSetsPath = existingDataSetsPath;
        this.approach = approach;
    }

    private void retrieveAndWriteQuery(Retriever retriever, BugReport query, SourceFileIDFinder idFinder, Project project, DiscourseQuery discourseQuery) {
        try {
            RelJudgment relJudgment = new RelJudgment();
            // idFinder should always return the same ID for a doc name provided IndexBuilder always indexes the same way
            List<RetrievalDoc> relevantDocs = query.getFixedFiles().stream()
                    .map(n -> {
                        try {
                            return new RetrievalDoc(idFinder.get(n), null, null);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    })
                    .collect(Collectors.toList());
            relJudgment.setRelevantDocs(relevantDocs);

            ScoreDoc[] scoreDocs = retriever.locate(query, true).getScores();
            // idFinder should always return the same ID for a ScoreDoc ID provided IndexBuilder always indexes the same way
            List<RetrievalDoc> retrievedDocs = Arrays.stream(scoreDocs)
                    .map(d -> new RetrievalDoc(idFinder.idForScoreDoc(d.doc), null, null))
                    .collect(Collectors.toList());

            writeQueryResults(new RetrievalResults(
                    project,
                    discourseQuery,
                    retrievedDocs,
                    relJudgment,
                    approach
            ));
        } catch (IOException | SearchException e) {
            throw new Error("Error processing query " + query + " from " + query.getProject(), e);
        }
    }

    protected abstract Retriever buildRetriever(String name, Path corpusFolderPath, Path indexPath, float alpha) throws IOException;

    @Override
    protected void runForCorpus(Corpus corpus) {
        Project project = corpus.getProject();
        String projectName = corpus.getName();

        Path corpusFolderPath = project.getDataSet()
                .resolveCorpusPath(existingDataSetsPath);

        Path indexPath = Paths.get(FileUtils.getTempDirectoryPath(), "BL-BRT-index");
        File indexFolder = indexPath.toFile();

        try {
            Map<String, BugReport> originalBugReports = BLBRTRunner.loadOriginalQueries(projectName, corpusFolderPath);

            FileUtils.forceMkdir(indexFolder);
            new BugLocator.Index(projectName, corpusFolderPath, indexPath).buildIfNotPresent();

            Retriever retriever = buildRetriever(projectName, corpusFolderPath, indexPath, ALPHA);

            SourceFileIDFinder sourceFileIDFinder = new SourceFileIDFinder(indexPath.resolve(Paths.get("source-code",
                    projectName)));

            for (DiscourseQuery query : corpus.getQueries()) {
                BugReport bugReport = query.toBugReport(false, project);
                bugReport.setOriginalText(originalBugReports.get(query.getKey()).getOriginalText());
                retrieveAndWriteQuery(retriever, bugReport, sourceFileIDFinder, project, query);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    protected Stream<Corpus> loadCorpora(Project project, List<DiscourseQuery> queries) {
        // Concurrently over corpora
        return super.loadCorpora(project, queries)
                .collect(Collectors.toList())
                .parallelStream();
    }
}
