package seers.disqueryreform.results.rstats;

import seers.disqueryreform.base.DataSet;

public enum DocumentGranularity {
    //code granularities
    FILE, CLASS, METHOD, ALL,
    //bug report
    BR;

    public static DocumentGranularity forDataSet(DataSet dataSet) {
        switch (dataSet) {
            case LB:
                return CLASS;
            case QQ:
            case D4J:
                return METHOD;
            case BRT:
            case B4BL:
                return FILE;
            default:
                throw new IllegalArgumentException("No granularity for data set " + dataSet);
        }
    }

    public static DocumentGranularity[] getCodeGranularities() {
        return new DocumentGranularity[]{FILE, CLASS, METHOD};
    }

    public static DocumentGranularity[] getBugGranularities() {
        return new DocumentGranularity[]{BR};
    }
}
