package seers.disqueryreform.evaluation.stats;


import edu.utdallas.seers.ir4se.evaluation.AggregatedMetrics;

public abstract class ReformulationStats<A extends AggregatedMetrics<M, ?>, M> {
    final A baselineAggregation;
    final A reformulationAggregation;

    ReformulationStats(A baselineAggregation, A reformulationAggregation) {
        this.baselineAggregation = baselineAggregation;
        this.reformulationAggregation = reformulationAggregation;
    }

    abstract <T extends ReformulationStats> T aggregate(T other);

    public double getBaselineMetric(M metric) {
        return baselineAggregation.getMetric(metric);
    }

    public double getReformulationMetric(M metric) {
        return reformulationAggregation.getMetric(metric);
    }

    public abstract double getImprovement(M metric);

    public abstract double getRelativeImprovement(M metric);

    public double getIdenticalMetric(M metric) {
        double baselineMetric = getBaselineMetric(metric);
        double reformulationMetric = getReformulationMetric(metric);

        if (baselineMetric == reformulationMetric) {
            return baselineMetric;
        } else {
            throw new IllegalStateException(String.format("Metric %s is not same for both: %f vs %f",
                    metric, baselineMetric, reformulationMetric));
        }
    }
}
