package seers.disqueryreform.retrieval.lucene;

import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.RAMRetrievalIndexer;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.lucene.LuceneRetrievalSearcher;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.RetrievalUtils;
import seers.disqueryreform.retrieval.lobster.LobsterMassiveMain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


@Deprecated
public class LuceneProcessor extends ThreadProcessor {

    private static final Logger LOGGER2 = LoggerFactory.getLogger(LobsterMassiveMain.class);

    private final List<Query> baselineQueries;
    private final Map queryEvalsPerK;
    private final Map originalQueryEvals;
    private final DataSet dataSet;
    private final ConcurrentHashMap systemCorpus;
    private List<Query> reducedQueries;
    private String corpusFolder;

    public LuceneProcessor(ThreadParameters params) {
        super(params);
        reducedQueries = params.getListParam(Query.class, ThreadExecutor.ELEMENTS_PARAM);
        corpusFolder = params.getStringParam("corpusBaseFolder");
        baselineQueries = params.getListParam(Query.class, "baselineQueries");
        queryEvalsPerK = params.getParam(Map.class, "queryEvalsPerK");
        originalQueryEvals = params.getParam(Map.class, "originalQueryEvals");
        dataSet = params.getParam(DataSet.class, "dataSet");
        systemCorpus = params.getParam(ConcurrentHashMap.class, "systemCorpus");
    }

    @Override
    public void executeJob() throws Exception {

        DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();

        //for each query
        for (Query reducedQuery : reducedQueries) {
            String project = (String) reducedQuery.getInfoAttribute("system");

            //build the original query
            String projectBugId = project + ";" + reducedQuery.getKey();
            Query originalQuery = getOriginalQuery(baselineQueries, project, reducedQuery.getKey());
            if (originalQuery == null) continue;
            List<RetrievalDoc> queryCorpus = Collections.emptyList();
//                    RetrievalUtils.readCorpus(systemCorpus, corpusFolder, dataSet,
//                    reducedQuery, project);

            // evaluate the relevance judgments
            RelJudgment queryRelJudment = RetrievalUtils.getRelevantJudgement(dataSet, reducedQuery, queryCorpus);

            if (queryRelJudment.getRelevantDocs().isEmpty()) {
                LOGGER.error("No rel jud evaluation for query: " + projectBugId);
                continue;
            }

            LOGGER2.debug("Processing reduced query " + projectBugId);

            List<RetrievalDoc> originalQueryRetrievedDocs = null;
            List<RetrievalDoc> reducedQueryRetrievedDocs = null;

            //index the corpus and run the original and reduced reducedQueries
            try (Directory index = new RAMRetrievalIndexer().buildIndex(queryCorpus, null)) {

                LuceneRetrievalSearcher searcher = new LuceneRetrievalSearcher(index, null);

                //query the corpus with the original query
                originalQueryRetrievedDocs = searcher.searchQuery(originalQuery);

                //query the corpus with the reduced query
                reducedQueryRetrievedDocs = searcher.searchQuery(reducedQuery);
            }

            //--------------------------------------------
            //compute the performance for the reducedQueries

            //assess performance for the original query
            List<Double> originalQueryMetrics = evaluator.evaluateRelJudgment(queryRelJudment,
                    originalQueryRetrievedDocs);

            //assess performance for the reformulated query
            List<Double> reducedQueryMetrics = evaluator.evaluateRelJudgment(queryRelJudment,
                    reducedQueryRetrievedDocs);

            //save these metrics (for debugging purposes)
            originalQueryEvals.put(reducedQuery, new ImmutablePair<>(originalQueryMetrics,
                    reducedQueryMetrics));

            //--------------------------------------------

            //get the rank of the original query
            final int originalQueryRank = originalQueryMetrics.get(0).intValue();
            processThresholds(reducedQuery, projectBugId, queryRelJudment, originalQueryRetrievedDocs,
                    reducedQueryRetrievedDocs, originalQueryRank, LuceneMassiveMain.rankThresholds,
                    queryEvalsPerK);


        }
    }


    public static Query getOriginalQuery(List<Query> baselineQueries, String project, String key) {
        return baselineQueries.stream().filter(q -> project.equals(q.getInfoAttribute("system").toString()) &&
                key.equals(q.getKey())).findFirst().get();
    }

    public static void processThresholds(Query reducedQuery, String projectBugId, RelJudgment queryRelJudment,
                                         List<RetrievalDoc> originalQueryRetrievedDocs, List<RetrievalDoc>
                                                 reducedQueryRetrievedDocs, int originalQueryRank, List<Integer>
                                                 rankThresholds,
                                         Map<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsPerK
    ) throws EvaluationException {


        DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();

        //for each rank threshold
        for (Integer rankThreshold : rankThresholds) {

            //does the original query needs reformulation?
            if (originalQueryRank <= rankThreshold) {
                //nope!
                continue;
            }

            //at this point the query needs reformulation!!

            //--------------------------------------------

            //make a copy of the retrieved doc list for the original query to be able to modify (for
            // computing metrics such as MAP, Precision, etc.
            List<RetrievalDoc> originalQueryRetrievedDocsCopy = getCopy(originalQueryRetrievedDocs);

            //same thing for the doc list of the reduced query
            List<RetrievalDoc> reducedQueryRetrievedDocsCopy = getCopy(reducedQueryRetrievedDocs);

            //-------------------------------------------------------------
            //from now on only work with the copies of doc lists

            List<RetrievalDoc> origQueryRetrievedDocsAtK = getRetrievedDocsAtK
                    (originalQueryRetrievedDocsCopy, rankThreshold);


            //remove the retrieved docs at K for the original query from the retrieved docs for
            // the reformulated query
            int numRemoved = removeDocsFromRetrievedDocs(reducedQueryRetrievedDocsCopy,
                    origQueryRetrievedDocsAtK);
/*            LOGGER2.debug("Num of docs removed at k=" + rankThreshold + " for reduced query "
                    + projectBugId + ": " + numRemoved + "/" + origQueryRetrievedDocsAtK.size());*/

            //re-rank the retrieved docs for the reduced query
            reRankDocs(reducedQueryRetrievedDocsCopy);

            //-------------------------------------------------------------

            //remove the retrieved docs at K for the original query
            removeDocsFromRetrievedDocs(originalQueryRetrievedDocsCopy, origQueryRetrievedDocsAtK);


            //re-rank the retrieved docs for the original query
            reRankDocs(originalQueryRetrievedDocsCopy);

            //--------------------------------------------
            //re-compute the performance for the reducedQueries

            //assess performance for the original query
            List<Double> newOriginalQueryMetrics = evaluator.evaluateRelJudgment(queryRelJudment,
                    originalQueryRetrievedDocsCopy);

            //assess performance for the reformulated query
            List<Double> newReducedQueryMetrics = evaluator.evaluateRelJudgment(queryRelJudment,
                    reducedQueryRetrievedDocsCopy);

            //--------------------------------------------

            //save the performance
            Map<Query, ImmutablePair<List<Double>, List<Double>>> queryEvalsK = queryEvalsPerK
                    .get(rankThreshold);
            queryEvalsK.put(reducedQuery, new ImmutablePair<>(newOriginalQueryMetrics,
                    newReducedQueryMetrics));

        }


    }

    public static List<RetrievalDoc> getCopy(List<RetrievalDoc> retrievedDocs) {
        return retrievedDocs.stream()
                .map(doc -> {
                    RetrievalDoc newDoc = new RetrievalDoc();
                    newDoc.setDocRank(doc.getDocRank());
                    newDoc.setScore(doc.getDocScore());
                    newDoc.setDocId(doc.getDocId());
                    newDoc.setDocName(doc.getDocName());
                    return newDoc;
                }).collect(Collectors.toList());
    }

    public static void reRankDocs(List<RetrievalDoc> reducedQueryRetrievedDocs) {
        int rank = 1;
        for (RetrievalDoc doc : reducedQueryRetrievedDocs) {
            doc.setDocRank(rank++);
        }
    }

    public static int removeDocsFromRetrievedDocs(List<RetrievalDoc> retrievedDocs, List<RetrievalDoc> docsToRemove) {
        int numRemoved = 0;
        for (RetrievalDoc doc : docsToRemove) {
            boolean removed = retrievedDocs.remove(doc);
            if (removed)
                numRemoved++;
            //throw new RuntimeException("Could not remove doc from the retrieved list: " + doc.getDocName());
        }
        return numRemoved;
    }

    public static List<RetrievalDoc> getRetrievedDocsAtK(List<RetrievalDoc> retrievedDocs, Integer rankThreshold) {
        if (retrievedDocs == null)
            return null;
        if (retrievedDocs.isEmpty())
            return new ArrayList<>();
        if (retrievedDocs.size() <= rankThreshold)
            return new ArrayList<>(retrievedDocs);
        return new ArrayList<>(retrievedDocs.subList(0, rankThreshold));
    }
}
