package seers.disqueryreform.evaluation.stats.output;

import edu.utdallas.seers.ir4se.evaluation.RetrievalSummary.SummarizedMetric;
import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.stats.ReformulationSummary;
import seers.disqueryreform.evaluation.stats.StrategySummary;
import seers.disqueryreform.evaluation.stats.keys.GTSKey;

import java.util.stream.Stream;

public class SummaryTableWriter
        extends AggregatedTableWriter<StrategySummary, ReformulationSummary, SummarizedMetric, GTSKey> {

    private final ComponentsForBugTable componentsTable;
    private final StrategyType strategyType;

    public SummaryTableWriter(ComponentsForBugTable componentsTable, StrategyType strategyType) {
        this.componentsTable = componentsTable;
        this.strategyType = strategyType;
    }

    // TODO: turning metrics into full classes instead of enums, we'd be able to put the getName method in the base class and avoid this method
    private Stream<Header<StrategySummary>> createHeadersForMetric(SummarizedMetric metric) {
        return createHeadersForMetric(metric.getName(), metric);
    }

    private String extractConfiguration(String strategyName) {
        return ComponentsForBugTable.extractConfiguration(strategyName).orElse("");
    }

    private String readableStrategyName(String strategyName) {
        return componentsTable.createNameSortedByApplicability(strategyName, strategyType);
    }

    @Override
    protected Stream<Stream<Header<StrategySummary>>> createHeaders() {
        return Stream.of(
                createSimpleHeader("Strategy", s -> readableStrategyName(s.getKey().getStrategy())),
                createSimpleHeader("Config", s -> extractConfiguration(s.getKey().getStrategy())),
                createHeadersForMetric(SummarizedMetric.AVERAGE_HITS_PERCENTAGE),
                createImprovementHeaders("Avg. MAP", SummarizedMetric.AVERAGE_MAP),
                createImprovementHeaders("Avg. MRR", SummarizedMetric.AVERAGE_MRR),
                createSimpleHeader(SummarizedMetric.AVERAGE_QUERIES.getName(),
                        s -> s.getStats().getIdenticalMetric(SummarizedMetric.AVERAGE_QUERIES))
        );
    }
}