package seers.disqueryreform.evaluation;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.JSON;
import seers.disqueryreform.evaluation.stats.EvaluationStatsTable;

import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class DetailedResultsWriter {
    private static final Logger LOGGER = LoggerFactory.getLogger(DetailedResultsWriter.class);

    public static DetailedResultsWriter newDetailedResultsWriter(Path detailedResultsDir) {
        if (detailedResultsDir != null) {
            return new ActualResultsWriter(detailedResultsDir);
        } else {
            return new NullResultsWriter();
        }
    }

    public abstract boolean hasAccumulatedStats();

    public abstract EvaluationStatsTable loadAccumulatedStats();

    public abstract void writeAccumulatedStats(EvaluationStatsTable accumulator);

    private static class ActualResultsWriter extends DetailedResultsWriter {

        private static final String ACCUMULATED_STATS_FILE_NAME = "accumulated-stats.json";
        private final Path accumulatedStatsPath;

        ActualResultsWriter(Path detailedResultsDir) {
            try {
                FileUtils.forceMkdir(detailedResultsDir.toFile());
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }

            accumulatedStatsPath = detailedResultsDir.resolve(ACCUMULATED_STATS_FILE_NAME);
        }

        @Override
        public boolean hasAccumulatedStats() {
            return Files.exists(accumulatedStatsPath);
        }

        @Override
        public EvaluationStatsTable loadAccumulatedStats() {
            LOGGER.info("Loading accumulated stats from file " + accumulatedStatsPath);

            try (FileReader reader = new FileReader(accumulatedStatsPath.toFile())) {
                return EvaluationStatsTable.createGson().fromJson(reader, EvaluationStatsTable.class);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

        @Override
        public void writeAccumulatedStats(EvaluationStatsTable accumulator) {
            LOGGER.info("Writing accumulated stats to " + accumulatedStatsPath);

            try {
                JSON.writeJSON(accumulator, accumulatedStatsPath);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    /**
     * Exists so that we can seamlessly manage the detailed results dir without having to ask
     * "should we write/read this" in every class that uses the directory.
     */
    private static class NullResultsWriter extends DetailedResultsWriter {
        private void fail() {
            throw new UnsupportedOperationException("Null writer cannot perform this action");
        }

        @Override
        public boolean hasAccumulatedStats() {
            return false;
        }

        @Override
        public EvaluationStatsTable loadAccumulatedStats() {
            fail();
            return null;
        }

        @Override
        public void writeAccumulatedStats(EvaluationStatsTable accumulator) {
            LOGGER.debug("Not writing accumulated stats to analysis folder");
        }
    }
}
