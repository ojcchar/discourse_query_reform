package seers.disqueryreform.results;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.GeneralUtils;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AllRetrievalResultsToAnalysisFormat {

    private static Logger LOGGER = LoggerFactory.getLogger(AllRetrievalResultsToAnalysisFormat.class);

    private String inputFolder = "C:\\Users\\ojcch\\Documents\\Projects\\Discourse_query_reformulation" +
            "\\results" +
            "\\1_retrieval-results";
    //     DataSet[] dataSets = {DataSet.BRT, DataSet.B4BL};
    private Set<String> dataSets = new LinkedHashSet<>(Arrays.asList(DataSet.BRT.toString(),
            DataSet.B4BL.toString()));
    private Set<String> techniques = JavaUtils.getSet(
            //"lucene", "brtracer", "buglocator", "lobster",
            "locus"
    );
    private Set<String> reformStrategies = QueryReformStrategy.getAllStrategiesExceptBaseline().stream()
            .map(Object::toString).collect(Collectors.toCollection(LinkedHashSet::new));
    private List<String> thresholds = GeneralUtils.getRankThresholdList(30);
    private String outputFolder = "C:\\Users\\ojcch\\Documents\\Projects\\Discourse_query_reformulation" +
            "\\results\\2_analysis-format2";

    public AllRetrievalResultsToAnalysisFormat(String inputFolder, Set<String> dataSets, Set<String> techniques,
                                               Set<String> reformStrategies, List<String> thresholds,
                                               String outputFolder) {
        this.inputFolder = inputFolder;
        this.dataSets = dataSets;
        this.techniques = techniques;
        this.reformStrategies = reformStrategies;
        this.thresholds = thresholds;
        this.outputFolder = outputFolder;
    }

    public AllRetrievalResultsToAnalysisFormat() {
    }

    public static void main(String[] args) throws IOException {


        AllRetrievalResultsToAnalysisFormat instance = new AllRetrievalResultsToAnalysisFormat();

        if (args.length > 0) {
            String inputFolder = args[0];
            Set<String> dataSets = JavaUtils.getSet(args[1].split(","));
            Set<String> techniques = JavaUtils.getSet(args[2].split(","));
            Set<String> reformStrategies = JavaUtils.getSet(args[3].split(","));
            List<String> thresholds = Arrays.asList(args[4].split(","));
            String outputFolder = args[5];
            instance = new AllRetrievalResultsToAnalysisFormat
                    (inputFolder, dataSets, techniques, reformStrategies, thresholds, outputFolder);
        }

        //---------------------------

        instance.runAnalysis();

    }

    public void runAnalysis() throws IOException {
        final File outFolder = new File(outputFolder);
        outFolder.mkdirs();

        List<List<String>> buglist = new ArrayList<>();
        List<List<String>> stats = new ArrayList<>();

        //-----------------

        //headers
        List<String> bugListHeader = Arrays.asList("dataset", "technique", "reform_strategy", "threshold", "system",
                "bug_id");
        List<String> statsHeader = Arrays.asList("dataset", "technique", "reform_strategy", "threshold", "system",
                "query_id", "bug_id", "strategy", "rank", "rank_diff", "avg_prec");

        //-------------------

        for (String dataSet : dataSets) {
            final File resultsFolder = getResultsFolder(inputFolder, dataSet);
            for (String technique : techniques) {
                final File techniqueFolder = Paths.get(resultsFolder.getAbsolutePath(), technique).toFile();
                if (!techniqueFolder.exists()) {
                    LOGGER.warn("Folder does not exist: " + techniqueFolder);
                    continue;
                }
                for (String reformStrategy : reformStrategies) {
                    final File strategyFolder = Paths.get(techniqueFolder.getAbsolutePath(), "and",
                            "TITLE_DESCRIPTION", reformStrategy).toFile();

                    File baselineFile = Paths.get(strategyFolder.getAbsolutePath(), "ALL_TEXT-stats.csv").toFile();
                    File strategyFile =
                            Paths.get(strategyFolder.getAbsolutePath(), reformStrategy + "-stats.csv").toFile();

                    List<List<String>> baselineStats = CSVHelper.readCsv(baselineFile.toString(), true, CSVHelper
                            .DEFAULT_SEPARATOR);
                    List<List<String>> reformStategyStats = CSVHelper.readCsv(strategyFile.toString(), true,
                            CSVHelper.DEFAULT_SEPARATOR);

                    List<List<String>> finalStats = computeFinalStats(baselineStats, reformStategyStats,
                            reformStrategy);
                    addStats(buglist, stats, dataSet, technique, reformStrategy, "no-topk-removal", finalStats);

                    for (String threshold : thresholds) {

                        File thresholdFolder =
                                Paths.get(strategyFolder.getAbsolutePath(), "threshold-" + threshold).toFile();

                        baselineFile = Paths.get(thresholdFolder.getAbsolutePath(), "ALL_TEXT-stats.csv").toFile();
                        strategyFile =
                                Paths.get(thresholdFolder.getAbsolutePath(), reformStrategy + "-stats.csv").toFile();

                        baselineStats = CSVHelper.readCsv(baselineFile.toString(), true, CSVHelper
                                .DEFAULT_SEPARATOR);
                        reformStategyStats = CSVHelper.readCsv(strategyFile.toString(), true,
                                CSVHelper.DEFAULT_SEPARATOR);

                        finalStats = computeFinalStats(baselineStats, reformStategyStats, reformStrategy);
                        addStats(buglist, stats, dataSet, technique, reformStrategy, threshold, finalStats);
                    }
                }
            }
        }

        //-------------------------

        File bugsFile = Paths.get(outFolder.getAbsolutePath(), "bug-list.csv").toFile();
        File statsFile = Paths.get(outFolder.getAbsolutePath(), "overall-stats.csv").toFile();
        CSVHelper.writeCsv(bugsFile, bugListHeader, buglist, null, Function.identity(), CSVHelper.DEFAULT_SEPARATOR);
        CSVHelper.writeCsv(statsFile, statsHeader, stats, null, Function.identity(), CSVHelper.DEFAULT_SEPARATOR);
    }

    protected File getResultsFolder(String baseFold, String dataSet) {
        return DataSetUtils.getResultsFolder(baseFold, DataSet.valueOf(dataSet));
    }

    private void addStats(List<List<String>> buglist, List<List<String>> stats, String dataSet, String
            technique, String reformStrategy, String threshold, List<List<String>> finalStats) {

        final List<String> preColumns = Arrays.asList(dataSet.toLowerCase(), technique,
                reformStrategy, threshold);
        LOGGER.debug(preColumns.toString());

        finalStats.stream().filter(bugStats -> reformStrategy.equals(bugStats.get(3))).map(bugStats ->
        {
            final ArrayList<String> allStats = new ArrayList<>(preColumns);
            allStats.add(bugStats.get(0));
            allStats.add(bugStats.get(2));
            return allStats;
        }).forEach(buglist::add);

        finalStats.stream().map(bugStats -> {
            final ArrayList<String> allStats = new ArrayList<>(preColumns);
            allStats.addAll(bugStats);
            return allStats;
        }).forEach(stats::add);

    }

    private List<List<String>> computeFinalStats(List<List<String>> baselineStats, List<List<String>>
            reformStategyStats, String reformStrategy) {
        //"system", "query_id", "bug_id", "strategy", "rank", "rank_diff", "avg_prec"
        List<List<String>> finalStats = baselineStats.subList(1, baselineStats.size()).stream().map
                (bugStats -> Arrays.asList(bugStats.get(0), bugStats.get(1), bugStats.get(2), "ALL_TEXT", bugStats
                        .get(3), "0", bugStats.get(10))).collect(Collectors.toCollection(ArrayList::new));

        Map<String, String> baselineRanks = baselineStats.subList(1, baselineStats.size()).stream().collect
                (Collectors.toMap(b -> b.get(0) + ";" + b.get(2), b -> b.get(3)));

        final List<List<String>> strategyStats = reformStategyStats.subList(1, reformStategyStats.size()).stream().map
                (bugStats -> {
                    Integer baselineRank = Double.valueOf(baselineRanks.get(bugStats.get(0) + ";" + bugStats.get(2)))
                            .intValue();
                    Integer strategyRank = Double.valueOf(bugStats.get(3)).intValue();
                    Integer rankDifference = baselineRank - strategyRank;
                    return Arrays.asList(bugStats.get(0), bugStats.get(1), bugStats.get(2), reformStrategy
                            , bugStats
                                    .get(3), rankDifference.toString(), bugStats.get(10));
                }).collect(Collectors.toList());

        finalStats.addAll(strategyStats);

        return finalStats;
    }
}
