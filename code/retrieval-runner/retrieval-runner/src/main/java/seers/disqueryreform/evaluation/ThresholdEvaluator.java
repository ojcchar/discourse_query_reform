package seers.disqueryreform.evaluation;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.base.DiscourseTaskStrategy;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.stats.ReformulationEvaluation;
import seers.disqueryreform.evaluation.stats.output.EvaluationTableWriter;
import seers.disqueryreform.querygen.tasknav.DiscourseTasksQueryGenerator;
import seers.disqueryreform.retrieval.QueryResultKey;
import seers.disqueryreform.retrieval.RetrievalResults;
import seers.disqueryreform.retrieval.lucene.LuceneProcessor;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static seers.disqueryreform.retrieval.lucene.LuceneProcessor.reRankDocs;
import static seers.disqueryreform.retrieval.lucene.LuceneProcessor.removeDocsFromRetrievedDocs;

public class ThresholdEvaluator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThresholdEvaluator.class);

    private final List<Integer> thresholds;
    private final Path resultListsPath;
    private final Path outputDirPath;
    private final Path baseRepositoryPath;
    private final StrategyType strategyType;

    ThresholdEvaluator(List<Integer> thresholds, Path resultListsPath, Path baseRepositoryPath, Path outputDirPath, StrategyType strategyType) {
        this.thresholds = thresholds;
        this.resultListsPath = resultListsPath;
        this.outputDirPath = outputDirPath;
        this.baseRepositoryPath = baseRepositoryPath;
        this.strategyType = strategyType;
    }

    public static ThresholdEvaluator create(List<Integer> thresholds, Path baseRepositoryPath, Path resultSource, Path outputFile, StrategyType strategyType) {
        if (Files.isDirectory(resultSource)) {
            LOGGER.info("Creating threshold evaluator for result lines from " + resultSource);
            return new ThresholdEvaluator(thresholds, resultSource, baseRepositoryPath, outputFile, strategyType);
        } else {
            LOGGER.info("Creating threshold evaluator for stats file from " + resultSource);
            return new ThresholdEvaluatorFromStats(baseRepositoryPath, resultSource, strategyType);
        }
    }

    /**
     * WARNING: Always close this stream, otherwise evaluation stats data will not be properly
     * written.
     *
     * @return Stream of evaluations for results.
     */
    public Stream<ReformulationEvaluation> thresholdEvaluations() {
        LOGGER.info(String.format("Evaluating results with thresholds %s reading from %s",
                thresholds, resultListsPath));

        ComponentsForBugTable componentsForBugTable;
        Map<String, RetrievalResults> baselineResultLists;
        ResultListReader resultListReader = new ResultListReader(resultListsPath);

        try {
            LOGGER.info("Loading table of components for bug...");

            componentsForBugTable = new DiscourseTasksQueryGenerator(baseRepositoryPath)
                    .readComponentsForBugTable();

            LOGGER.info("Reading baseline result lists...");

            baselineResultLists = resultListReader.loadBaselineResultLists();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        LOGGER.info("Processing thresholds...");

        Path statsFilePath = outputDirPath.resolve("evaluation-stats.csv");

        EvaluationTableWriter evaluationTableWriter = new EvaluationTableWriter(statsFilePath);

        LOGGER.info("Writing evaluation stats to " + statsFilePath);

        // Use all results because we will use ALL_TEXT results for some strategies
        return resultListReader.allResults()
                .flatMap(r -> {
                    String baselineKey = resultListReader.createBaselineKey(r.getKey());
                    RetrievalResults baselineResults =
                            baselineResultLists.get(baselineKey);

                    Optional<Set<String>> maybeStrategies = findStrategiesForResult(r.getKey(), componentsForBugTable);

                    Set<String> strategies = maybeStrategies.orElse(Collections.emptySet());

                    if (strategies.isEmpty()) {
                        return Stream.empty();
                    }

                    return processResultLists(baselineResults, r, strategies, evaluationTableWriter).stream();
                })
                .onClose(() -> {
                    try {
                        evaluationTableWriter.close();
                    } catch (Exception e) {
                        throw new Error(e);
                    }
                });
    }

    Optional<Set<String>> findStrategiesForResult(QueryResultKey resultsKey, ComponentsForBugTable componentsForBugTable) {
        // getting rid of data clumps would require moving QRK to base commons
        Project project = resultsKey.getProject();
        String queryKey = resultsKey.getQueryKey();
        String originalStrategy = resultsKey.getStrategy();
        String taskNavConfiguration = resultsKey.getTaskNavConfiguration();

        boolean isBlizzard = originalStrategy.startsWith("BLIZZARD");
        String strategy = isBlizzard
                ? originalStrategy.replace("BLIZZARD--", "")
                : originalStrategy;

        Optional<Set<String>> strategies =
                componentsForBugTable.findStrategiesForQuery(project, queryKey, strategy, taskNavConfiguration, strategyType);

        if (!isBlizzard) {
            return strategies;
        } else {
            // If the strategy was BLIZZARD--s, strategies will be just the s's. Add the BLIZZARD
            Optional<Set<String>> updatedStrategies = strategies.map(ss ->
                    ss.stream()
                            .map(s -> "BLIZZARD--" + s)
                            .collect(Collectors.toSet())
            );

            // Note that for a strategy BLIZZARD+s, if s does not apply, we use BLIZZARD on the
            // original text as opposed to just the original query
            if (strategy.equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME)) {
                // BLIZZARD--ALL_TEXT means plain blizzard
                // TODO call this strategy just "BLIZZARD" in query generation
                HashSet<String> ss = new HashSet<>(updatedStrategies.orElse(Collections.emptySet()));
                ss.add("BLIZZARD");
                return Optional.of(ss);
            }

            return updatedStrategies;
        }
    }

    private List<ReformulationEvaluation> processResultLists(
            RetrievalResults baselineResults, RetrievalResults reformulatedResults, Set<String> strategies, EvaluationTableWriter evaluationTableWriter) {

        RelJudgment relJudgment = baselineResults.getRelevanceJudgment()
                .orElseThrow(() -> new IllegalArgumentException("Query " + baselineResults.getKey() + " has no gold set"));

        List<RetrievalDoc> baselineDocs = baselineResults.getRetrievedDocuments();
        List<RetrievalDoc> reformulatedDocs = reformulatedResults.getRetrievedDocuments();

        DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();

        int originalQueryRank;

        try {
            originalQueryRank = evaluator.evaluateRelJudgment(relJudgment, baselineDocs).get(0).intValue();
        } catch (EvaluationException e) {
            throw new Error(e);
        }

        List<ReformulationEvaluation> evals = new ArrayList<>();

        //for each rank threshold
        for (int rankThreshold : thresholds) {

            //does the original query needs reformulation?
            if (originalQueryRank <= rankThreshold) {
                //nope!
                continue;
            }

            //at this point the query needs reformulation!!

            //--------------------------------------------

            //make a copy of the retrieved doc list for the original query to be able to modify (for
            // computing metrics such as MAP, Precision, etc.
            List<RetrievalDoc> originalQueryRetrievedDocsCopy = LuceneProcessor.getCopy(baselineDocs);

            //same thing for the doc list of the reduced query
            List<RetrievalDoc> reducedQueryRetrievedDocsCopy = LuceneProcessor.getCopy(reformulatedDocs);

            //-------------------------------------------------------------
            //from now on only work with the copies of doc lists

            List<RetrievalDoc> origQueryRetrievedDocsAtK = LuceneProcessor.getRetrievedDocsAtK
                    (originalQueryRetrievedDocsCopy, rankThreshold);


            //remove the retrieved docs at K for the original query from the retrieved docs for
            // the reformulated query
            removeDocsFromRetrievedDocs(reducedQueryRetrievedDocsCopy,
                    origQueryRetrievedDocsAtK);
/*            LOGGER2.debug("Num of docs removed at k=" + rankThreshold + " for reduced query "
                    + projectBugId + ": " + numRemoved + "/" + origQueryRetrievedDocsAtK.size());*/

            //re-rank the retrieved docs for the reduced query
            reRankDocs(reducedQueryRetrievedDocsCopy);

            //-------------------------------------------------------------

            //remove the retrieved docs at K for the original query
            removeDocsFromRetrievedDocs(originalQueryRetrievedDocsCopy, origQueryRetrievedDocsAtK);


            //re-rank the retrieved docs for the original query
            reRankDocs(originalQueryRetrievedDocsCopy);

            //--------------------------------------------
            //re-compute the performance for the reducedQueries

            RetrievalEvaluation newOriginalQueryMetrics;
            RetrievalEvaluation newReducedQueryMetrics;

            try {
                // TODO can cache ALL_TEXT query for each threshold
                //assess performance for the original query
                newOriginalQueryMetrics = evaluator.evaluateRelevanceJudgment(rankThreshold, relJudgment,
                        originalQueryRetrievedDocsCopy);
                //assess performance for the reformulated query
                newReducedQueryMetrics = evaluator.evaluateRelevanceJudgment(rankThreshold, relJudgment,
                        reducedQueryRetrievedDocsCopy);
            } catch (EvaluationException e) {
                throw new Error(e);
            }

            QueryResultKey actualResultKey = reformulatedResults.getKey();

            ReformulationEvaluation evaluation = new ReformulationEvaluation(actualResultKey,
                    newOriginalQueryMetrics, newReducedQueryMetrics, strategies);

            /*
            Will write for only one strategy but this is fine because results are the same for all
            strategies anyway
            */
            evaluationTableWriter.writeRow(evaluation);

            evals.add(evaluation);
        }

        return evals;
    }
}
