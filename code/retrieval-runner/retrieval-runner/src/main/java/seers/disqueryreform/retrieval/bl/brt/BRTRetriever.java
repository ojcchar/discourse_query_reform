package seers.disqueryreform.retrieval.bl.brt;

import edu.utdallas.seers.brtracer.BRTracer;
import edu.utdallas.seers.ir4se.retrieval.Retriever;
import seers.disqueryreform.retrieval.ResultsLineWriter;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.io.IOException;
import java.nio.file.Path;

public class BRTRetriever extends BLBRTRetriever {
    public BRTRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath) {
        super(resultsWriter, existingDataSetsPath, RetrievalTechnique.BR_TRACER);
    }

    @Override
    protected Retriever buildRetriever(String name, Path corpusFolderPath, Path indexPath, float alpha) throws IOException {
        return BRTracer.buildRetriever(name, corpusFolderPath, indexPath, alpha);
    }
}
