package seers.disqueryreform.retrieval.bench4bl;

import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.bench4bl.Bug;

import javax.annotation.Nullable;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Loads results produced by the Bench4BL tools into our format.
 * Contains the relevance judgments, result lists and evaluations for each query.
 * Also contains the corpus.
 */
public class Bench4BLResults implements Closeable {
    private final Path innerDirectoryPath;
    private final Map<String, Optional<RetrievalDoc>> fixedFileCache = new HashMap<>();
    private final boolean cleanUpAfter;
    private final Path workingDirectoryPath;
    private Logger LOGGER = LoggerFactory.getLogger(Bench4BLResults.class);
    private Map<String, RetrievalDoc> corpus;

    private Bench4BLResults(Path workingDirectoryPath, boolean cleanUpAfter) {
        this.workingDirectoryPath = workingDirectoryPath;

        // There should be only one directory inside the working directory
        File[] workingDirDirs = workingDirectoryPath.toFile()
                .listFiles(File::isDirectory);

        if (Objects.requireNonNull(workingDirDirs).length != 1) {
            throw new IllegalStateException("The working directory contains more than one directory");
        }

        this.innerDirectoryPath = workingDirDirs[0].toPath();
        this.cleanUpAfter = cleanUpAfter;
    }

    static Bench4BLResults loadBench4BLResults(Path workingDirectoryPath, boolean cleanUpAfter)
            throws IOException {
        Bench4BLResults bench4BLResults = new Bench4BLResults(workingDirectoryPath, cleanUpAfter);

        bench4BLResults.loadCorpus();

        return bench4BLResults;
    }

    /**
     * @param queries The queries whose results will be read.
     * @return Lazily loaded query results.
     */
    public Stream<QueryResults> loadAllQueryResults(List<BugReport> queries) {
        return queries.stream()
                .map(b -> {
                    RelJudgment relJudgment = createRelevanceJudgment(b);

                    if (relJudgment == null) {
                        LOGGER.error(String.format("Invalid bug report: No gold set documents for %s from %s",
                                b.getKey(), b.getProject()));

                        return null;
                    }

                    try {
                        List<RetrievalDoc> resultList = readResultList(b.getKey());

                        return new QueryResults(b.getKey(), relJudgment, resultList, null);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .filter(Objects::nonNull);
    }

    private List<RetrievalDoc> readResultList(String bugKey) throws IOException {
        File recommendedFile =
                innerDirectoryPath.resolve(Paths.get("recommended", bugKey + ".txt")).toFile();
        try (BufferedReader reader = new BufferedReader(new FileReader(recommendedFile))) {
            return reader.lines()
                    .map(l -> {
                        String rawFileName = l.trim().split("\t")[2];

                        return corpus.get(convertFileName(rawFileName));
                    })
                    .collect(Collectors.toList());
        }
    }

    private @Nullable
    RelJudgment createRelevanceJudgment(BugReport bugReport) {
        List<RetrievalDoc> relevantList = new ArrayList<>();

        for (String fixedFileName : bugReport.getFixedFiles()) {
            Optional<RetrievalDoc> maybeFixedFile;

            /* Since this is a long process, we cache using a map

               The reason we're using a map of optionals is so that we can perform this contains
               check reasonably. If we weren't using optionals, there would be no clean way of
               registering when the map should return a null. In this case the cache would say
               the key does not exist and so the result would have to be computed every time, even
               if we know it is going to return nothing */
            if (!fixedFileCache.containsKey(fixedFileName)) {

                /* The file name in the corpus file is the full path, but the gold set is only the
                package. This is how the dataset was built by the authors.
                That means we need to go through the whole thing to find it. */
                List<RetrievalDoc> corpusDocsForFile = corpus.values().stream()
                        .filter(d -> d.getDocName().endsWith(fixedFileName))
                        .collect(Collectors.toList());

                if (corpusDocsForFile.isEmpty()) {
                    LOGGER.warn(String.format("(%s) File %s from bug %s not found in corpus",
                            bugReport.getProject(),
                            fixedFileName,
                            bugReport.getKey()));

                    // This is the case in which we need to store null in the map
                    maybeFixedFile = Optional.empty();
                } else {
                    maybeFixedFile = Optional.of(corpusDocsForFile.get(0));
                }

                /* This is completely arbitrary, but including all documents that match would
                 * probably boost the performance unfairly. This is a limitation of how the original
                 * authors built the data set */
                if (corpusDocsForFile.size() > 1) {
                    LOGGER.warn(String.format("(%s) File %s from bug %s has multiple matches in corpus: %s. Using first match as gold set document",
                            bugReport.getProject(),
                            fixedFileName,
                            bugReport.getKey(),
                            corpusDocsForFile));
                }

                fixedFileCache.put(fixedFileName, maybeFixedFile);
            } else {
                maybeFixedFile = fixedFileCache.get(fixedFileName);
            }

            maybeFixedFile.ifPresent(relevantList::add);
        }

        if (relevantList.isEmpty()) {
            return null;
        }

        RelJudgment relJudgment = new RelJudgment();
        relJudgment.setRelevantDocs(relevantList);

        return relJudgment;
    }

    private void loadCorpus() throws IOException {
        File corpusFile = innerDirectoryPath.resolve("sourceFileIndex.txt").toFile();
        corpus = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(corpusFile))) {
            int docId = 1;

            // Sorting them so that they are always loaded the same way and have the same ID
            List<String> sortedDocNames = reader.lines()
                    .map(this::convertFileName)
                    .sorted()
                    .collect(Collectors.toList());

            for (String docName : sortedDocNames) {
                corpus.put(docName, new RetrievalDoc(docId++, null, docName));
            }
        }
    }

    /**
     * B4BL tools append ".java" to the "file" names (actually class fully qualified names), so we
     * must strip it if present because our code does not expect that. See also FixedFile
     * constructor in class {@link Bug} for the code that adds ".java" to the name
     * so that their tool works.
     *
     * @param rawLine A line containing a "file" name from B4BL tool.
     * @return The name trimmed and without ".java" at the end if present or just trimmed otherwise.
     */
    private String convertFileName(String rawLine) {
        String rawName = rawLine.trim();

        String docName;
        if (!rawName.endsWith(".java")) {
            docName = rawName;
        } else {
            // Do it using substring and not replace to make sure it only takes off the end
            docName = rawName.substring(0, rawName.length() - 5);
        }

        return docName;
    }

    @Override
    public void close() throws IOException {
        if (cleanUpAfter) {
            FileUtils.deleteDirectory(workingDirectoryPath.toFile());
        }
    }

    public Map<String, RetrievalDoc> getCorpus() {
        return corpus;
    }

    public static class QueryResults {
        private final RelJudgment judgment;
        private final List<RetrievalDoc> resultList;
        private final List<Double> evaluation;
        private final String id;

        QueryResults(String id, RelJudgment judgment, List<RetrievalDoc> resultList, List<Double> evaluation) {
            this.id = id;
            this.judgment = judgment;
            this.resultList = resultList;
            this.evaluation = evaluation;
        }

        public RelJudgment getJudgment() {
            return judgment;
        }

        public List<RetrievalDoc> getResultList() {
            return resultList;
        }

        public List<Double> getEvaluation() {
            return evaluation;
        }

        public String getId() {
            return id;
        }
    }
}
