package seers.disqueryreform.results;

import net.quux00.simplecsv.CsvWriter;
import net.quux00.simplecsv.CsvWriterBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.DiscourseTaskStrategy;
import seers.disqueryreform.base.QueryReformStrategy;
import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.results.rstats.RStatsRunner;
import seers.disqueryreform.retrieval.lucene.LuceneMassiveMain;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ResultsSummarizer {

    private static Logger LOGGER = LoggerFactory.getLogger(ResultsSummarizer.class);

    public static void main(String[] args) throws IOException {

        final DocumentGranularity[] granularities = DocumentGranularity.getCodeGranularities();
        Set<String> techniques = JavaUtils.getSet(
                "lucene", "lobster", "brtracer", "buglocator",
                "locus");
        List<String> thresholds = RStatsRunner.getRankThresholds(30, false);
        List<String> reformStrategies = QueryReformStrategy.getAllStrategiesExceptBaseline().stream()
                .map(Object::toString).collect(Collectors.toList());
        String resultsSummaryFolder = RStatsRunner.getResultsSummaryFolder(RStatsRunner.getDefaultBaseFolder());

        runSummary(granularities, techniques, thresholds, reformStrategies, resultsSummaryFolder, null, LuceneMassiveMain.queriesPerStrategy);

    }

    public static void runSummary(DocumentGranularity[] granularities, Set<String> techniques, List<String> thresholds,
                                  List<String> reformStrategies, String resultsSummaryFolder, File overallSummaryOutFile,
                                  Map<String, Integer> queriesPerStrategy) throws IOException {
        //----------------------------

        File outFile = Paths.get(resultsSummaryFolder, "all" +
                "-results.csv").toFile();

        FileUtils.forceMkdirParent(outFile);

        List<List<String>> finalLines = new ArrayList<>();
        final List<String> header = new ArrayList<>();

        final int numRecords = (granularities.length + 1) * thresholds.size();
        int HITS_IDX = 2;
        int MAP_IDX = HITS_IDX + numRecords + 3;

        //--------------------------------------------

        for (String technique : techniques) {
            Map<String, List<List<String>>> strategyLines = readResults(technique,
                    resultsSummaryFolder,
                    reformStrategies);

            for (Map.Entry<String, List<List<String>>> entry :
                    strategyLines.entrySet()) {
                final String strategy = entry.getKey();
                final List<List<String>> lines = entry.getValue();

                LOGGER.debug(String.format("Processing %s-%s", technique, strategy));

                List<List<String>> hitsLines = lines.subList(HITS_IDX, HITS_IDX + numRecords);
                List<List<String>> mapsLines = lines.subList(MAP_IDX, MAP_IDX + numRecords);

                //-----------------------------------

                if (header.isEmpty()) {
                    header.add("Strategy");
                    header.add("Technique");
                    header.addAll(lines.subList(HITS_IDX - 1, HITS_IDX).get(0));
                    final List<String> mapsHeader = lines.subList(MAP_IDX - 1, MAP_IDX).get(0);
                    header.addAll(mapsHeader.subList(3, mapsHeader.size()));
                }

                //---------------------------------------

                List<List<String>> allLines = new ArrayList<>();

                for (int i = 0; i < hitsLines.size(); i++) {
                    final List<String> hitsLine = hitsLines.get(i);
                    final List<String> mapsLine = mapsLines.get(i);

                    if (!hitsLine.subList(0, 2).equals(mapsLine.subList(0, 2))) {
                        throw new RuntimeException(hitsLine.subList(0, 2).toString() + " vs " + mapsLine.subList(0,
                                2).toString());
                    }

                    //------------------------------------

                    final ArrayList<String> newLine = new ArrayList<>();
                    newLine.add(strategy);
                    newLine.add(technique);
                    newLine.addAll(hitsLine);
                    if (mapsLine.size() >= 3)
                        newLine.addAll(mapsLine.subList(3, mapsLine.size()));

                    allLines.add(newLine);
                }

                finalLines.addAll(allLines);
            }

        }

        header.set(2, "DocumentGranularity");

        //-------------------------------------------------------
        // Output overall strategy summary

        writeOverallStrategySummary(finalLines, overallSummaryOutFile, queriesPerStrategy);

        //-------------------------------------------------------

        if (outFile.exists())
            FileUtils.forceDelete(outFile);

        try (CsvWriter cswWriter = new CsvWriterBuilder(new FileWriter(outFile, true))
                .separator(CSVHelper.DEFAULT_SEPARATOR)
                .escapeChar(CsvWriter.NO_ESCAPE_CHARACTER).build()) {

            CSVHelper.writeCsv(cswWriter, header, finalLines, null, Function.identity());
        }
    }

    private static void writeOverallStrategySummary(List<List<String>> lines,
                                                    File overallSummaryOutFile, Map<String, Integer> queriesPerStrategy)
            throws IOException {
        Map<String, List<List<String>>> rawLinesByStrategy = lines.stream()
                // We are interested in overall results
                // TODO: output other granularities in overall summary
                .filter(l -> l.get(2).equals("all_granularities"))
                .collect(Collectors.toMap(
                        // Group by strategy and technique
                        l -> l.get(0) + ";" + l.get(1),
                        Collections::singletonList,
                        (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList()))
                );

        List<List<String>> summaryLines = new ArrayList<>();
        final double totalQueries = queriesPerStrategy.get(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME);

        for (Map.Entry<String, List<List<String>>> e : rawLinesByStrategy.entrySet()) {
            List<List<String>> rawLines = e.getValue();

            double averageQueries = computeAverage(rawLines, 4);
            double noReformHitsAverage = computeAverage(rawLines, 7);
            double reformHitsAverage = computeAverage(rawLines, 8);
            double averageImprovement = computeAverage(rawLines, 10);

            // 5 is HITS baseline and 6 is HITS strategy
            Function<List<String>, Pair<Integer, Integer>> extractor =
                    l -> new Pair<>(Integer.valueOf(l.get(5)), Integer.valueOf(l.get(6)));

            int totalEffective = countCondition(rawLines, l -> {
                Pair<Integer, Integer> pair = extractor.apply(l);

                return pair.getValue() > pair.getKey();
            });

            int totalNeutral = countCondition(rawLines, l -> {
                Pair<Integer, Integer> p = extractor.apply(l);
                return Objects.equals(p.getKey(), p.getValue());
            });

            int totalIneffective = countCondition(rawLines, l -> {
                Pair<Integer, Integer> p = extractor.apply(l);
                return p.getValue() < p.getKey();
            });

            String[] split = e.getKey().split(";");
            String strategy = split[0];
            String technique = split[1];
            summaryLines.add(Arrays.asList(
                    technique,
                    strategy.replace("--", "+"),
                    String.valueOf(averageQueries),
                    String.valueOf(noReformHitsAverage),
                    String.valueOf(reformHitsAverage),
                    String.valueOf(averageImprovement),
                    String.valueOf(totalEffective),
                    String.valueOf(totalNeutral),
                    String.valueOf(totalIneffective),
                    String.valueOf(queriesPerStrategy.get(strategy) / totalQueries)
            ));
        }

        List<String> header = Arrays.asList(
                "Technique",
                "Strategy Name",
                "Average Queries",
                "% Avg. HITS@n no reform",
                "% Avg. HITS@n reform",
                "% Avg. Improvement",
                "Total thresholds effective",
                "Total thresholds neutral",
                "Total thresholds ineffective",
                "% Initial Applicability"
        );

        try (CsvWriter cswWriter = new CsvWriterBuilder(new FileWriter(overallSummaryOutFile))
                .separator(CSVHelper.DEFAULT_SEPARATOR)
                .escapeChar(CsvWriter.NO_ESCAPE_CHARACTER).build()) {
            CSVHelper.writeCsv(cswWriter, header, summaryLines, null, Function.identity());
        }
    }

    private static int countCondition(List<List<String>> rawLines, Predicate<List<String>> condition) {
        return (int) rawLines.stream()
                .filter(condition)
                .count();
    }

    private static double computeAverage(List<List<String>> rawLines, int index) {
        /* If baseline HITS are 0, improvement will be NaN. Keep it like that for now */
        return rawLines.stream()
                .mapToDouble(l -> {
                    String n = l.get(index);

                    if (n.equals("NA")) {
                        return Double.NaN;
                    }

                    return Double.parseDouble(n);
                })
                .average()
                .orElse(0);
    }

    private static Map<String, List<List<String>>> readResults(String technique,
                                                               String resultsSummaryFolder,
                                                               List<String> reformStrategies) throws IOException {
        final Collection<File> strategyResultFiles =
                FileUtils.listFiles(Paths.get(resultsSummaryFolder, technique).toFile(), new String[]{
                        "csv"}, false);

        Map<String, List<List<String>>> strategyLines = new LinkedHashMap<>();

        for (File resultFile : strategyResultFiles) {

            final String name = resultFile.getName();
            final String reformStrategy = getReformStrategy(name);
            if (!reformStrategies.contains(reformStrategy)) {
                continue;
            }

            final List<List<String>> lines = CSVHelper.readCsv(resultFile.getAbsolutePath(), false);
            strategyLines.put(reformStrategy, lines);

        }
        return strategyLines;
    }

    private static String getReformStrategy(String name) {
        return name.substring(16).replace(".csv", "");
    }
}
