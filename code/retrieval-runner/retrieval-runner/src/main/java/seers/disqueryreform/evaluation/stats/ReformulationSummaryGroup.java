package seers.disqueryreform.evaluation.stats;

import seers.disqueryreform.evaluation.stats.keys.TechniqueStrategyKey;

import java.util.Map;
import java.util.stream.Stream;

public class ReformulationSummaryGroup {
    private final Map<TechniqueStrategyKey, ReformulationSummary> grouping;

    ReformulationSummaryGroup(Map<TechniqueStrategyKey, ReformulationSummary> grouping) {
        this.grouping = grouping;
    }

    public Stream<TechniqueStrategySummary> summaries() {
        return grouping
                .entrySet().stream()
                .map(e -> new TechniqueStrategySummary(e.getKey(), e.getValue()));
    }
}
