package seers.disqueryreform;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.*;
import seers.disqueryreform.evaluation.ThresholdEvaluatorFromStats;
import seers.disqueryreform.evaluation.stats.ReformulationEvaluation;
import seers.disqueryreform.evaluation.stats.keys.EvaluationKey;
import seers.disqueryreform.evaluation.stats.keys.GTSKey;
import seers.disqueryreform.evaluation.stats.output.TableWriter;
import seers.disqueryreform.retrieval.QueryResultKey;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static edu.utdallas.seers.collections.Collections.*;

public class ResultSampler {
    public static final int RANDOM_SEED = 42;
    public static final int TOP_BOTTOM_SAMPLES = 200;
    private final Logger logger = LoggerFactory.getLogger(ResultSampler.class);

    public static void main(String[] args) throws IOException {
        Path baseRepoPath = Paths.get(args[0]);
        // evaluation-stats.csv
        Path statsPath = Paths.get(args[1]);
        Path outputDir = Paths.get(args[2]);
        int sampleSize = Integer.parseInt(args[3]);

        new ResultSampler().sampleResults(baseRepoPath, statsPath, outputDir, sampleSize);
    }

    private static String removeConfig(String strategy) {
        List<String> componentNames =
                new ArrayList<>(DiscourseTaskStrategy.splitIntoComponentNames(strategy));

        String lastComponent = componentNames.get(componentNames.size() - 1);

        if (lastComponent.contains("true") || lastComponent.contains("false")) {
            assert lastComponent.equals("true-false");
            componentNames.remove(lastComponent);
        }

        return String.join(DiscourseTaskStrategy.COMPONENT_SEPARATOR, componentNames);
    }

    private void sampleResults(Path baseRepoPath, Path statsPath, Path outputDir, int sampleSize) throws IOException {
        Map<EvaluationQueryKey, ReformulationEvaluation> evaluations = loadEvaluations(baseRepoPath, statsPath);

        SampleTableWriter sampleTableWriter = new SampleTableWriter(loadReformulatedTexts(baseRepoPath));
        FileUtils.forceMkdir(outputDir.toFile());

        Random random = new Random(RANDOM_SEED);

        for (String component : Arrays.asList("TITLE", "EB", "OB", "S2R", "OTHER")) {
            Stream<EvaluationPair> sampledPairs = samplePairs(sampleSize, evaluations, component, random);

            Path outputFile = outputDir.resolve(component + ".csv");

            logger.info(String.format("Outputting %s sample to %s", component, outputFile));

            sampleTableWriter.writeTable(sampledPairs, outputFile);
        }
    }

    private Stream<EvaluationPair> samplePairs(int sampleSize, Map<EvaluationQueryKey, ReformulationEvaluation> evaluations, String component, Random random) {
        logger.info(String.format("Pairing results: %s vs T_F_%s", component, component));

        AtomicInteger counter = new AtomicInteger(1);

        Map<Boolean, List<EvaluationPair>> allPairs = evaluations.entrySet().stream()
                .parallel()
                .map(e -> {
                    GTSKey thisStrategyKey = e.getKey().gtsKey;
                    List<String> componentNames =
                            new ArrayList<>(DiscourseTaskStrategy.splitIntoComponentNames(thisStrategyKey.getStrategy()));

                    // Only strategies that include the component
                    if (!componentNames.contains(component)) {
                        return null;
                    }

                    // Find strategy that uses all same components + T_F_<component>
                    componentNames.remove(component);
                    componentNames.add("T_F_" + component);

                    String otherStrategyName = componentNames.stream()
                            .sorted()
                            .collect(Collectors.joining(DiscourseTaskStrategy.COMPONENT_SEPARATOR));

                    GTSKey otherEvalKey = thisStrategyKey.withStrategy(otherStrategyName);

                    EvaluationQueryKey otherEvalQueryKey = new EvaluationQueryKey(
                            otherEvalKey,
                            e.getKey().project,
                            e.getKey().queryID
                    );

                    ReformulationEvaluation otherEval = evaluations.get(otherEvalQueryKey);

                    return new EvaluationPair(e.getValue(), otherEval, e.getKey());
                })
                .filter(p -> {
                    boolean nonNull = Objects.nonNull(p);

                    if (nonNull) {
                        int count = counter.getAndIncrement();
                        if (count % 200_000 == 0) {
                            logger.info(String.format("Processed %d pairs", count));
                        }

                        assert p.baselineRank != 0;
                    }


                    return nonNull;
                })
                // If it didn't retrieve with no task, it won't retrieve with task
                .filter(p -> p.noTaskRank != 0 &&
                        // Select only the ones that made a difference
                        p.improvementDifference() != 0)
                .distinct()
                .sorted(Comparator.comparing(EvaluationPair::improvementDifference).reversed())
                .collect(Collectors.groupingBy(p -> p.withTaskRank != 0));

        List<EvaluationPair> retrievedPairs = allPairs.get(true);

        Stream<EvaluationPair> sampledPairs = extractListSample(sampleSize, retrievedPairs, random);
        logger.info(String.format("Finished. Total %d pairs", counter.get() - 1));

        List<EvaluationPair> notRetrievedPairs = allPairs.get(false);
        Stream<EvaluationPair> notRetrievedSample = toJava(randomSample(
                toScala(notRetrievedPairs),
                Math.min(20, notRetrievedPairs.size()),
                scala.util.Random.javaRandomToRandom(random)
        ))
                .stream();

        return Stream.concat(sampledPairs, notRetrievedSample);

    }

    private Map<EvaluationQueryKey, ReformulationEvaluation> loadEvaluations(Path baseRepoPath, Path statsPath) {
        ThresholdEvaluatorFromStats loader =
                new ThresholdEvaluatorFromStats(baseRepoPath, statsPath, StrategyType.COMBINATORIAL);

        Map<EvaluationQueryKey, ReformulationEvaluation> evaluations;

        logger.info("Loading evaluations");

        try (Stream<ReformulationEvaluation> evaluationStream = loader.thresholdEvaluations()) {
            // Each result may apply to multiple strategies, so unravel them
            evaluations = evaluationStream
                    .parallel()
                    .flatMap(e -> e.getStrategies().stream().map(s -> new Pair<>(s, e)))
                    .collect(Collectors.toConcurrentMap(
                            p -> {
                                ReformulationEvaluation evaluation = p.getValue();

                                EvaluationKey key = evaluation.getKey();
                                QueryResultKey queryResultKey = evaluation.getQueryResultKey();
                                String strategy = removeConfig(p.getKey());
                                return new EvaluationQueryKey(
                                        key.withStrategy(strategy).removeThreshold(),
                                        queryResultKey.getProject(),
                                        queryResultKey.getQueryKey()
                                );
                            },
                            Pair::getValue,
                            (e1, e2) -> {
                                assert e1.getReformulationMetric(RetrievalEvaluation.Metric.RANK) ==
                                        e2.getReformulationMetric(RetrievalEvaluation.Metric.RANK);

                                return e1;
                            }
                    ));
        }

        return evaluations;
    }

    private Stream<EvaluationPair> extractListSample(int sampleSize, List<EvaluationPair> pairs, Random random) {
        int topSamples, bottomSamples;
        // todo use fewer top and bottom
        topSamples = bottomSamples = TOP_BOTTOM_SAMPLES;
        int middleSamples = sampleSize - (topSamples + bottomSamples);

        List<EvaluationPair> top = pairs.subList(0, topSamples);
        List<EvaluationPair> bottom = pairs.subList(pairs.size() - bottomSamples, pairs.size());

        List<EvaluationPair> allMiddle = pairs.subList(topSamples, pairs.size() - bottomSamples);

        Collection<EvaluationPair> sampledMiddle = toJava(randomSample(
                toScala(allMiddle),
                middleSamples,
                scala.util.Random.javaRandomToRandom(random)
        ));

        return Stream.of(top, sampledMiddle, bottom)
                .flatMap(Collection::stream);
    }

    private Map<String, String> loadReformulatedTexts(Path baseRepoPath) throws IOException {
        logger.info("Loading reformulated texts");

        Path queriesPath = baseRepoPath.resolve(Paths.get("data", "tasknav_reformulated_queries"));

        return Files.walk(queriesPath)
                .filter(p -> Files.isRegularFile(p) && p.toString().endsWith(".json") &&
                        // This avoids loading component-table.json
                        Character.isUpperCase(p.getFileName().toString().charAt(0)))
                .flatMap(p -> {
                    StrategyQueryContainer container;
                    try {
                        container = JSON.readJSON(p, StrategyQueryContainer.class);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }

                    String strategy = removeConfig(container.getStrategyName());

                    return container.getItems().stream()
                            .flatMap(e -> {
                                Project project = e.getKey();

                                return e.getValue().stream()
                                        .map(q -> new Pair<>(
                                                String.join(";", strategy, project.toString(), q.getKey()),
                                                q.getText()
                                        ));
                            });
                })
                .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));
    }

    static class EvaluationPair {
        private final double baselineRank;
        private final double noTaskRank;
        private final double withTaskRank;
        private final String granularityTechnique;
        private final String noTaskStrategy;
        private final Project project;
        private final String queryID;
        private final String withTaskStrategy;

        EvaluationPair(ReformulationEvaluation noTask, ReformulationEvaluation withTask, EvaluationQueryKey key) {
            baselineRank = noTask.getBaselineMetric(RetrievalEvaluation.Metric.RANK);
            noTaskRank = noTask.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
            withTaskRank = withTask.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
            granularityTechnique = key.gtsKey.getGranularity() + "-" + key.gtsKey.getTechnique();
            noTaskStrategy = removeConfig(noTask.getKey().getStrategy());
            withTaskStrategy = removeConfig(withTask.getKey().getStrategy());
            project = key.project;
            queryID = key.queryID;
        }

        int noTaskImprovement() {
            return getImprovement(this.noTaskRank);
        }

        private int getImprovement(double reformulationRank) {
            return (int) (baselineRank - reformulationRank);
        }

        int withTaskImprovement() {
            return getImprovement(withTaskRank);
        }

        int improvementDifference() {
            return withTaskImprovement() - noTaskImprovement();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EvaluationPair that = (EvaluationPair) o;
            return Double.compare(that.noTaskRank, noTaskRank) == 0 &&
                    Double.compare(that.withTaskRank, withTaskRank) == 0 &&
                    granularityTechnique.equals(that.granularityTechnique) &&
                    project.equals(that.project) &&
                    queryID.equals(that.queryID);
        }

        @Override
        public int hashCode() {
            return Objects.hash(noTaskRank, withTaskRank, granularityTechnique, project, queryID);
        }
    }

    static class EvaluationQueryKey {
        private final GTSKey gtsKey;
        Project project;
        String queryID;

        EvaluationQueryKey(GTSKey gtsKey, Project project, String queryID) {
            // Threshold will be the same for all results of the same query
            this.gtsKey = gtsKey;
            this.project = project;
            this.queryID = queryID;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EvaluationQueryKey that = (EvaluationQueryKey) o;
            return gtsKey.equals(that.gtsKey) &&
                    project.equals(that.project) &&
                    queryID.equals(that.queryID);
        }

        @Override
        public int hashCode() {
            return Objects.hash(gtsKey, project, queryID);
        }
    }

    static class SampleTableWriter extends TableWriter<EvaluationPair> {

        private final Map<String, String> reformulatedTexts;

        public SampleTableWriter(Map<String, String> reformulatedTexts) {
            this.reformulatedTexts = reformulatedTexts;
        }

        @Override
        protected Stream<Stream<Header<EvaluationPair>>> createHeaders() {
            return Stream.of(
                    createSimpleHeader("Granularity - Technique", p -> p.granularityTechnique),
                    createSimpleHeader("Strategy", p -> p.noTaskStrategy),
                    createSimpleHeader("Project", p -> p.project),
                    createSimpleHeader("Query ID", p -> p.queryID),
                    createSimpleHeader("Baseline Rank", p -> p.baselineRank),
                    createSimpleHeader("No Task Rank", p -> p.noTaskRank),
                    createSimpleHeader("With Task Rank", p -> p.withTaskRank),
                    createSimpleHeader("No Task Rank Imp.", EvaluationPair::noTaskImprovement),
                    createSimpleHeader("With Task Rank Imp.", EvaluationPair::withTaskImprovement),
                    createSimpleHeader("Improvement Diff", p -> p.withTaskRank != 0 ? p.improvementDifference() : "NR"),
                    createSimpleHeader("No Task Text",
                            p -> reformulatedTexts.get(String.join(";", p.noTaskStrategy, p.project.toString(), p.queryID))),
                    createSimpleHeader("With Task Text",
                            p -> reformulatedTexts.get(String.join(";", p.withTaskStrategy, p.project.toString(), p.queryID)))
            );
        }
    }
}
