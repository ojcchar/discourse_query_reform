package seers.disqueryreform.evaluation.stats;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.evaluation.ThresholdEvaluator;
import seers.disqueryreform.evaluation.stats.keys.EvaluationKey;
import seers.disqueryreform.evaluation.stats.keys.GTSKey;
import seers.disqueryreform.evaluation.stats.keys.GranularityTechniquePair;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * It's basically an Excel table that accumulates stats for each combination of technique, strategy,
 * granularity and threshold
 */
public class EvaluationStatsTable {
    private static final Logger LOGGER = LoggerFactory.getLogger(EvaluationStatsTable.class);

    private final Map<EvaluationKey, ReformulationAccumulation> accumulated;

    private EvaluationStatsTable(Map<EvaluationKey, ReformulationAccumulation> accumulated) {
        this.accumulated = accumulated;
    }

    public static Gson createGson() {
        return new GsonBuilder()
                .registerTypeAdapter(EvaluationKey.class, EvaluationKey.getTypeAdapter())
                .create();
    }

    /**
     * Accumulates all retrieval results from the files in the parameter and returns them in an object.
     *
     * @param thresholdEvaluator Evaluations that will be accumulated.
     * @return An accumulator that contains all the evaluation stats.
     */
    public static EvaluationStatsTable accumulateAllResults(ThresholdEvaluator thresholdEvaluator) {

        Map<EvaluationKey, ReformulationAccumulation> accumulated = new HashMap<>(2_000_000, 0.9F);

        // The stream must be closed so that evaluation stats are written
        try (Stream<ReformulationEvaluation> evaluationStream = thresholdEvaluator.thresholdEvaluations()) {
            // This used to be all functional and pretty but the GC could not keep up
            evaluationStream.forEach(e -> {
                for (String strategy : e.getStrategies()) {
                    EvaluationKey key = e.getKey().withStrategy(strategy);
                    if (!accumulated.containsKey(key)) {
                        accumulated.put(key, e.toAccumulation());
                    } else {
                        ReformulationAccumulation currentStats = accumulated.get(key);

                        currentStats.accumulate(e);
                    }
                }
            });
        }

        LOGGER.info(String.format("Finished accumulating %d total thresholds", accumulated.size()));

        return new EvaluationStatsTable(accumulated);
    }

    public ThresholdSummary summarizeThresholds() {
        LOGGER.info("Summarizing thresholds...");

        // TODO call this "summarize" and extract to method
        Map<GTSKey, ReformulationSummary> rawSummary = accumulated.entrySet().stream()
                .collect(Collectors.toMap(
                        // By removing threshold they'll be summarized by gran, tech and strat
                        e -> e.getKey().removeThreshold(),
                        e -> e.getValue().toSummary(),
                        ReformulationSummary::aggregate
                ));

        LOGGER.info("Finished summarizing thresholds");
        LOGGER.info("Grouping by granularity and technique...");

        Map<GranularityTechniquePair, List<StrategySummary>> groupedSummary = rawSummary.entrySet().parallelStream()
                .collect(Collectors.toConcurrentMap(
                        e -> e.getKey().removeStrategy(),
                        e -> Collections.singletonList(new StrategySummary(e.getKey(), e.getValue())),
                        (l1, l2) -> Stream.of(l1, l2).flatMap(Collection::stream).collect(Collectors.toList())
                ));

        LOGGER.info("Finished grouping");

        return new ThresholdSummary(groupedSummary);
    }

    public ReformulationAccumulationGroup groupByGranularity() {
        return new ReformulationAccumulationGroup(group(
                e1 -> e1.getKey().fixedGranularity(),
                Map.Entry::getValue
        ));
    }

    private <K> Map<K, ReformulationAccumulation> group(
            Function<Map.Entry<EvaluationKey, ReformulationAccumulation>, K> keyExtractor,
            Function<Map.Entry<EvaluationKey, ReformulationAccumulation>, ReformulationAccumulation> valueExtractor) {

        return accumulated
                .entrySet().parallelStream()
                .collect(Collectors.toConcurrentMap(
                        keyExtractor,
                        valueExtractor,
                        ReformulationAccumulation::aggregate
                ));
    }

    public Stream<TechniqueStrategyCollection> allStatsByTechniqueAndStrategy() {
        return accumulated.entrySet().stream()
                .collect(Collectors.groupingBy(e -> e.getKey().getTechnique()))
                .entrySet().stream()
                .map(e -> new TechniqueStrategyCollection(
                                e.getKey(),
                                e.getValue().stream()
                                        .map(TableEntry::new)
                                        .collect(Collectors.toList())
                        )
                );
    }

    /**
     * Return a new table that uses only true-false configuration and full tasks.
     *
     * @return A new table.
     */
    public EvaluationStatsTable simplify() {
        return new EvaluationStatsTable(accumulated.entrySet().parallelStream()
                .filter(e -> {
                    String strategy = e.getKey().getStrategy();
                    String config = ComponentsForBugTable.extractConfiguration(strategy).orElse(null);

                    // If there are no tasks we accept it
                    if (config == null) {
                        return true;
                    }

                    // Only interested in true-false
                    if (!config.equals("true-false")) {
                        return false;
                    }

                    List<String> otherTasks = Arrays.asList("T_O", "T_V", "T_VO");

                    return otherTasks.stream().noneMatch(strategy::contains);
                })
                .collect(Collectors.toConcurrentMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                )));
    }

    public static class TableEntry extends StatsWithKey<EvaluationKey, ReformulationAccumulation> {
        TableEntry(Map.Entry<EvaluationKey, ReformulationAccumulation> entry) {
            super(entry.getKey(), entry.getValue());
        }

        public TableEntry(EvaluationKey key, ReformulationAccumulation value) {
            super(key, value);
        }
    }
}
