package seers.disqueryreform.evaluation.stats;

import edu.utdallas.seers.ir4se.evaluation.RetrievalSummary.SummarizedMetric;
import seers.disqueryreform.evaluation.stats.ThresholdSummary.ThresholdSummaryEntry;
import seers.disqueryreform.evaluation.stats.keys.GranularityTechniquePair;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OverallStrategySummary {

    private final List<GranularityTechniquePair> granularityTechniques;
    private final Map<String, List<StrategySummary>> summary;

    OverallStrategySummary(List<ThresholdSummaryEntry> groups) {
        granularityTechniques = groups.stream()
                .map(ThresholdSummaryEntry::getKey)
                .collect(Collectors.toList());

        summary = groups.stream()
                .flatMap(g -> g.getSummaries().stream())
                .collect(Collectors.toMap(
                        s -> s.getKey().getStrategy(),
                        Collections::singletonList,
                        (l1, l2) -> Stream.of(l1, l2).flatMap(Collection::stream).collect(Collectors.toList())
                ));
    }

    public Stream<OverallSummaryEntry> entries() {
        return summary.entrySet().stream()
                .map(OverallSummaryEntry::new);
    }

    public List<GranularityTechniquePair> getGranularityTechniques() {
        return granularityTechniques;
    }

    public class OverallSummaryEntry {

        private final String fullStrategy;
        private final Map<GranularityTechniquePair, ReformulationSummary> values;

        OverallSummaryEntry(Map.Entry<String, List<StrategySummary>> entry) {
            fullStrategy = entry.getKey();

            values = entry.getValue().stream()
                    .collect(Collectors.toMap(
                            s -> s.getKey().removeStrategy(),
                            StatsWithKey::getStats
                    ));
        }

        public ReformulationSummary get(GranularityTechniquePair key) {
            return values.get(key);
        }

        public double calculateAverage(SummarizedMetric metric) {
            return values.values().stream()
                    .mapToDouble(s -> s.getRelativeImprovement(metric))
                    .average()
                    .orElse(0D);
        }

        public String getFullStrategy() {
            return fullStrategy;
        }
    }
}
