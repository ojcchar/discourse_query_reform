package seers.disqueryreform.retrieval.bench4bl.locus;

import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import edu.wayne.cs.severe.ir4se.processor.exception.WritingException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DiscourseTaskStrategy;
import seers.disqueryreform.retrieval.bench4bl.Bench4BLResults;
import seers.disqueryreform.retrieval.bench4bl.Bench4BLToolEvaluator;
import seers.disqueryreform.retrieval.bl.brt.BLBRTRunner;
import seers.disqueryreform.retrieval.lucene.LuceneMassiveMain;
import seers.disqueryreform.retrieval.lucene.LuceneProcessor;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocusReformulationEvaluator {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocusReformulationEvaluator.class);

    private static List<Integer> rankThresholds;

    private static Map<String, Map<String, BugReport>> allOriginalQueries;
    private static Map<String, String> eclipseProjectSplits;
    private static Map<String, Bench4BLResults> originalResultsByProject;
    private static Path originalDataPath;
    private static Path outputPath;
    private static Map<Query, ImmutablePair<List<Double>, List<Double>>> originalQueryEvalsMap;
    private static Map<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsPerK;
    private static Map<String, List<Query>> queriesBySystem;
    private static Path baseRepositoryPath;

    @SuppressWarnings("ConstantConditions")
    public void evaluateApproach(Path reformulatedQueriesPath, Path originalDataPath,
                                 Path outputPath, List<Integer> thresholds, List<String> strategies,
                                 Path baseRepositoryPath, DataSet dataSet)
            throws Exception {
        LocusReformulationEvaluator.baseRepositoryPath = baseRepositoryPath;
        rankThresholds = thresholds;
        FileUtils.forceMkdir(outputPath.toFile());

        LocusReformulationEvaluator.originalDataPath = originalDataPath;
        LocusReformulationEvaluator.outputPath = outputPath;
        Set<String> systemNames;

        if (dataSet == DataSet.B4BL) {
            /* WARNING: this method uses a static field in the BLBRTRunner class
             * Should not be a problem unless they are both run concurrently somehow */
            systemNames = BLBRTRunner.loadSystemNames(dataSet, reformulatedQueriesPath, strategies, originalDataPath);
        } else {
            systemNames = Stream.of(originalDataPath.toFile().listFiles(f -> f.isDirectory() &&
                    !f.getName().contains("eclipse") && !f.getName().contains("aspect")))
                    .map(File::getName)
                    .collect(Collectors.toSet());
        }

        originalResultsByProject = runOriginalQueries(originalDataPath, systemNames);
        allOriginalQueries = loadOriginalQueries(originalDataPath, systemNames);
        eclipseProjectSplits = extractEclipseSplits(originalResultsByProject);

        LOGGER.info(String.format("Computing reformulated query results for %d strategies", strategies.size()));

        executeStrategies(dataSet, strategies, reformulatedQueriesPath);

        // UGLY: clearing out these to save memory
        allOriginalQueries = null;
        eclipseProjectSplits = null;
        originalResultsByProject = null;
        queriesBySystem = null;
        originalQueryEvalsMap = null;
        queryEvalsPerK = null;
    }

    private void executeStrategies(DataSet dataSet, List<String> strategies, Path reformulatedQueriesPath) throws Exception {
        for (String strategy : strategies) {
            queryEvalsPerK = new ConcurrentHashMap<>();
            for (Integer rankThreshold : rankThresholds) {
                queryEvalsPerK.put(rankThreshold, new ConcurrentHashMap<>());
            }

            originalQueryEvalsMap = new ConcurrentHashMap<>();

            queriesBySystem = loadQueriesBySystem(dataSet, reformulatedQueriesPath, strategy, eclipseProjectSplits);
            LOGGER.info(String.format("Processing strategy %s", strategy));

            ThreadParameters params = new ThreadParameters();
            params.addParam("st", strategy);
            // Threading across systems
            ThreadExecutor.executePaginated(new ArrayList<>(queriesBySystem.keySet()),
                    LocusThreadProcessor.class, params, 8);

            File outputFolder = outputPath.resolve(strategy).toFile();
            FileUtils.forceMkdir(outputFolder);
            LuceneMassiveMain.writeStats3(strategy, queryEvalsPerK, originalQueryEvalsMap,
                    DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME, outputFolder.getAbsolutePath());

            LOGGER.info(String.format("Finished processing strategy %s", strategy));
        }
    }

    public static class LocusThreadProcessor extends ThreadProcessor {
        private final List<String> systems;
        private final String strategy;

        public LocusThreadProcessor(ThreadParameters params) {
            super(params);
            systems = params.getListParam(String.class, ThreadExecutor.ELEMENTS_PARAM);
            strategy = params.getParam(String.class, "st");
        }

        @Override
        public void executeJob() throws Exception {
            for (String system : systems) {
                LOGGER.info(String.format("[%s] Processing system %s", strategy, system));

                Bench4BLResults originalResults = originalResultsByProject.get(system);
                List<Query> reformulatedQueries = queriesBySystem.get(system);

                if (reformulatedQueries == null) {
                    continue;
                }

                Bench4BLResults reformulationResults;
                try {
                    reformulationResults = runSystem(system, reformulatedQueries);
                } catch (IOException | InterruptedException | JAXBException | EvaluationException e) {
                    throw new Exception(String.format("[%s] Error running system with Locus: %s", strategy, system), e);
                }

                for (Query reformulatedQuery : reformulatedQueries) {
                    // Since the tools can't handle full IDs, we are only gonna get the number
                    String key = reformulatedQuery.getKey();
                    if (key.contains("-")) {
                        key = key.split("-")[1];
                    }

                    Bench4BLResults.QueryResults originalQueryResults = null;

                    Bench4BLResults.QueryResults reformulatedQueryResults = null;



                    // Put values in the map
                    //noinspection ConstantConditions
                    originalQueryEvalsMap.put(reformulatedQuery,
                            new ImmutablePair<>(originalQueryResults.getEvaluation(), reformulatedQueryResults.getEvaluation()));

                    LuceneProcessor.processThresholds(reformulatedQuery, system + ";" + reformulatedQuery.getKey(),
                            reformulatedQueryResults.getJudgment(), originalQueryResults.getResultList(),
                            reformulatedQueryResults.getResultList(), originalQueryResults.getEvaluation().get(0).intValue(),
                            rankThresholds, queryEvalsPerK);
                }

                LOGGER.info(String.format("[%s] Finished processing system %s", strategy, system));
            }
        }

        private Bench4BLResults runSystem(String system, List<Query> reformulatedQueries)
                throws IOException, InterruptedException, EvaluationException, JAXBException {

            Path sysPath = originalDataPath.resolve(system);
            String repoDir = sysPath.resolve("git-repo").toString();
            String sourceDir = sysPath.resolve("sources").toString();
            Map<String, BugReport> originalQueriesForSystem = allOriginalQueries.get(system);
            List<BugReport> reformBugs = reformulatedQueries.stream()
                    .map(q -> {
                        String key = q.getKey();
                        if (key.contains("-")) {
                            key = key.split("-")[1];
                        }

                        return BLBRTRunner.createReformulatedBugReport(q, originalQueriesForSystem.get(key));
                    })
                    .collect(Collectors.toList());

            return new LocusRetriever(true, baseRepositoryPath)
                    .retrieve(system + strategy, repoDir, sourceDir, reformBugs);
        }

    }

    private Map<String, List<Query>> loadQueriesBySystem(DataSet dataSet, Path reformulatedQueriesPath, String strategy, Map<String, String> eclipseProjectSplits)
            throws IOException {

        /* WARNING: this uses static fields in the BLBRTProcessor class */
        Map<String, List<Query>> allQueries = BLBRTRunner.BLBRTProcessor.loadQueriesBySystem(dataSet, reformulatedQueriesPath, strategy);

        if (dataSet != DataSet.BRT) {
            // Only BRT requires reading the project splits
            return allQueries;
        }

        Map<String, List<Query>> queriesBySystem = new HashMap<>();

        for (Map.Entry<String, List<Query>> e : allQueries.entrySet()) {
            String highLevelSystem = e.getKey();

            // This exists because of the Eclipse project splits
            for (Query query : e.getValue()) {

                if (query.getKey().contains("-")) {
                    query.setKey(query.getKey().split("-")[1]);
                }

                String actualSystem;
                if (highLevelSystem.startsWith("eclipse")) {
                    actualSystem = eclipseProjectSplits.get(query.getKey());
                    if (actualSystem == null || actualSystem.startsWith("eclipse")) {
                        LOGGER.warn(String.format("Query %s does not belong to any valid eclipse subsystem", query.getKey()));
                        continue;
                    }
                    if (actualSystem.startsWith("swt")) {
                        LOGGER.warn(String.format("Skipping SWT query in eclipse subsystems: %s", query.getKey()));
                        continue;
                    }
                    query.addInfoAttribute("system", actualSystem);
                } else {
                    actualSystem = highLevelSystem;
                }

                queriesBySystem.computeIfAbsent(actualSystem, k -> new ArrayList<>())
                        .add(query);
            }
        }

        return queriesBySystem;
    }


    /**
     * Creates a map used to split the Eclipse project into subprojects. Must be used because in the
     * reformulated systems eclipse is a single project.
     *
     * @param originalResultsByProject
     * @return Map from query key to project
     */
    private Map<String, String> extractEclipseSplits(Map<String, Bench4BLResults> originalResultsByProject) {
        Map<String, String> splitsMap = new HashMap<>();

        for (Map.Entry<String, Bench4BLResults> entry : originalResultsByProject.entrySet()) {
            String project = entry.getKey();
            Bench4BLResults projectEvals = entry.getValue();

            for (Query query : Collections.singletonList(new Query())) {
                splitsMap.put(query.getKey(), project);
            }
        }

        return splitsMap;
    }

    private Map<String, Bench4BLResults> runOriginalQueries(Path originalDataPath, Set<String> systemNames)
            throws InterruptedException, JAXBException, WritingException, EvaluationException, IOException {
        LOGGER.info("Running original systems");
        LOGGER.info("=====================================");

        Map<String, Bench4BLResults> resultsByProject = new HashMap<>();

        // We must change query keys if they are in the proj-ID format
        for (Bench4BLResults resultsForSystem : resultsByProject.values()) {
            for (Query query : Collections.singletonList(new Query())) {
                String key = query.getKey();
                if (key.contains("-")) {
                    query.setKey(key.split("-")[1]);
                }
            }
        }

        return resultsByProject;
    }

    @SuppressWarnings("ConstantConditions")
    private Map<String, Map<String, BugReport>> loadOriginalQueries(Path originalDataPath, Set<String> systemNames) throws IOException {
        File[] projectFolders = originalDataPath.toFile()
                .listFiles(f -> f.isDirectory() && systemNames.contains(f.getName()));

        LOGGER.info(String.format("Loading all original query texts for %d projects", projectFolders.length));
        LOGGER.info("=====================================");

        Map<String, Map<String, BugReport>> bySystem = new HashMap<>();

        for (File projectFolder : projectFolders) {
            String jsonBugReportsPath = projectFolder.toPath().resolve("original-bug-reports.json").toString();

            List<BugReport> bugReports = Bench4BLToolEvaluator.loadJSONBugReports(jsonBugReportsPath);
            // Bug reports will be sorted by creation date
            bugReports.sort(Comparator.comparing(BugReport::getCreationDate));

            bySystem.put(projectFolder.getName(), bugReports.stream().collect(Collectors.toMap(BugReport::getKey, b -> b)));
        }

        return bySystem;
    }
}
