package seers.disqueryreform.results.querystats;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;
import net.quux00.simplecsv.CsvWriter;
import org.apache.commons.io.FileUtils;
import seers.appcore.csv.CSVHelper;
import seers.disqueryreform.base.DiscourseTaskStrategy;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.ThresholdEvaluatorFromStats;
import seers.disqueryreform.evaluation.stats.ReformulationEvaluation;
import seers.disqueryreform.evaluation.stats.keys.GTSTKey;
import seers.disqueryreform.results.rstats.DocumentGranularity;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.io.File;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainQueryStats {

    public static void main(String[] args) throws Exception {

        //change the paths
        Path baseRepoPath =
                new File("C:\\Users\\ojcch\\Documents\\Repositories\\projects\\discourse_query_reform").toPath();
        Path statsFilePath = new File("C:\\Users\\ojcch\\Documents\\Projects\\Task-based query " +
                "reformulation\\discourse-blizzard-eval-04-30\\COMBINATORIAL\\evaluation-stats.csv").toPath();
        File queryStatsFile = new File("C:\\Users\\ojcch\\Documents\\Projects\\Task-based query " +
                "reformulation\\discourse-blizzard-eval-04-30\\COMBINATORIAL\\query-stats.csv");

        //delete the output file
        if (queryStatsFile.exists()) FileUtils.forceDelete(queryStatsFile);

        //load the results
        ThresholdEvaluatorFromStats loader =
                new ThresholdEvaluatorFromStats(baseRepoPath, statsFilePath, StrategyType.COMBINATORIAL);

        //group the results by threshold, granularity, technique, and strategy
        LinkedHashMap<GTSTKey, List<ReformulationEvaluation>> map = new LinkedHashMap<>();
        try (Stream<ReformulationEvaluation> evaluationStream = loader.thresholdEvaluations()) {

            evaluationStream.forEach(e -> {
                Set<String> strategies = e.getStrategies();
                DocumentGranularity granularity = e.getKey().getGranularity();
                RetrievalTechnique technique = e.getKey().getTechnique();
                int threshold = e.getKey().getThreshold();
                for (String strategy : strategies) {
                    GTSTKey key = new GTSTKey(granularity, technique, strategy, threshold);
                    List<ReformulationEvaluation> evals = map.computeIfAbsent(key, k -> new LinkedList<>());
                    evals.add(e);
                }

            });
        }

        //write stats
        try (CsvWriter writer = CSVHelper.getWriter(queryStatsFile, CSVHelper.DEFAULT_SEPARATOR, true)) {

            //write header
            List<String> header = Arrays.asList("Threshold", "Granularity", "Technique", "Gran-Techique", "Strategy",
                    "Blizzard?",
                    "#queries", "hits_diff",
                    "%improv", "%deter", "%deter/non-retr", "%equal", "%retrieved", "%not_retrieved",
                    "%highq", "#improv", "#deter", "#deter/non-retr", "#equal", "#retrieved"
                    , "#not_retrieved", "#highq",
                    "%top_base", "%nontop_base", "%top->top", "%top->nontop",
                    "%nontop->top",
                    "%nontop->nontop", "%total_queries", "%top->nontop(2t)",
                    "%nontop(+2t)->top",
                    //------------------
                    "#top_base", "#nontop_base", "#top->top", "#top->nontop",
                    "#nontop->top",
                    "#nontop->nontop", "total_queries", "#top->nontop(2t)",
                    "#nontop(+2t)->top"
            );
            writer.writeNext(header);

            //for each entry in the map
            map.forEach((key, evaluations) -> {


                List<ReformulationEvaluation> retrieved_data =
                        evaluations.stream()
                                .filter(ev -> ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK) != 0)
                                .collect(Collectors.toList());
                List<ReformulationEvaluation> non_retrieved_data = evaluations.stream()
                        .filter(ev -> ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK) == 0)
                        .collect(Collectors.toList());

                //--------------------------------------------------------------------------------------------------
                //compute # of queries stats:
                int num_queries = evaluations.size();
                long num_improv =
                        retrieved_data.stream()
                                .filter(ev -> ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK) <
                                        ev.getBaselineMetric(RetrievalEvaluation.Metric.RANK)).count();
                long num_deter =
                        retrieved_data.stream()
                                .filter(ev -> ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK) >
                                        ev.getBaselineMetric(RetrievalEvaluation.Metric.RANK)).count();
                long num_equal =
                        retrieved_data.stream()
                                .filter(ev -> ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK) ==
                                        ev.getBaselineMetric(RetrievalEvaluation.Metric.RANK)).count();


                int num_retrieved = retrieved_data.size();
                int num_not_retrieved = non_retrieved_data.size();

                final int threshold = key.getThreshold();
                long num_highq =
                        retrieved_data.stream().filter(ev -> ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK) <= threshold).count();

                long num_deter_non_retr = num_deter + num_not_retrieved;

                double perc_improv = (double) num_improv / num_queries;
                double perc_deter = (double) num_deter / num_queries;
                double perc_deter_non_retr = (double) num_deter_non_retr / num_queries;
                double perc_equal = (double) num_equal / num_queries;
                double perc_retrieved = (double) num_retrieved / num_queries;
                double perc_not_retrieved = (double) num_not_retrieved / num_queries;

                double perc_highq = (double) num_highq / num_queries;

                //-----------------------------------------------------

                List<ReformulationEvaluation> topBaseLineQueries =
                        evaluations.stream()
                                .filter(ev -> ev.getBaselineMetric(RetrievalEvaluation.Metric.RANK) <= threshold)
                                .collect(Collectors.toList());
                List<ReformulationEvaluation> nonTopBaseLineQueries =
                        evaluations.stream()
                                .filter(ev -> ev.getBaselineMetric(RetrievalEvaluation.Metric.RANK) > threshold)
                                .collect(Collectors.toList());

                int numTopBase = topBaseLineQueries.size();
                int numNonTopBase = nonTopBaseLineQueries.size();

                long numTopRemainedTop = topBaseLineQueries.stream()
                        .filter(ev -> {
                            double reformulationMetric =
                                    ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
                            return reformulationMetric <= threshold && reformulationMetric > 0;
                        }).count();

                long numTopBecameNonTop = topBaseLineQueries.stream()
                        .filter(ev -> {
                            double reformulationMetric =
                                    ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
                            return reformulationMetric > threshold || reformulationMetric == 0;
                        }).count();

                long numNonTopBecameTop = nonTopBaseLineQueries.stream()
                        .filter(ev -> {
                            double reformulationMetric =
                                    ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
                            return reformulationMetric <= threshold && reformulationMetric > 0;
                        }).count();

                long numNonTopRemainedNonTop = nonTopBaseLineQueries.stream()
                        .filter(ev -> {
                            double reformulationMetric =
                                    ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
                            return reformulationMetric > threshold || reformulationMetric == 0;
                        }).count();

                long totalQueries =
                        numTopRemainedTop + numTopBecameNonTop + numNonTopBecameTop + numNonTopRemainedNonTop;


                long numTopBecameNonTopWithin2threshold = topBaseLineQueries.stream()
                        .filter(ev -> {
                            double reformulationMetric =
                                    ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
                            return reformulationMetric > threshold && reformulationMetric <= 2 * threshold;
                        }).count();


                long numNonTopBeyond2thresholdBecameTop = nonTopBaseLineQueries.stream()
                        .filter(ev -> {
                            double baselineMetric = ev.getBaselineMetric(RetrievalEvaluation.Metric.RANK);
                            double reformulationMetric =
                                    ev.getReformulationMetric(RetrievalEvaluation.Metric.RANK);
                            return baselineMetric > 2 * threshold && reformulationMetric <= threshold && reformulationMetric > 0;
                        }).count();


                double percTopBase = (double) numTopBase / num_queries;
                double percNonTopBase = (double) numNonTopBase / num_queries;

                double percTopRemainedTop = (double) numTopRemainedTop / numTopBase;
                double percTopBecameNonTop = (double) numTopBecameNonTop / numTopBase;
                double percNonTopBecameTop = (double) numNonTopBecameTop / numNonTopBase;
                double percNonTopRemainedNonTop = (double) numNonTopRemainedNonTop / numNonTopBase;
                double percTotalQueries = (double) totalQueries / num_queries;

                double percTopBecameNonTopWithin2threshold = (double) numTopBecameNonTopWithin2threshold / numTopBase;
                double percNonTopBeyond2thresholdBecameTop =
                        (double) numNonTopBeyond2thresholdBecameTop / numNonTopBase;

                double diffHits = perc_highq - percTopBase;


                //--------------------------------------------------------------------------------------------------

                //write stats
                List<Object> stats = Arrays.asList(key.getThreshold(), key.getGranularity(),
                        key.getTechnique(), key.getGranularity() + "-" + key.getTechnique(),
                        removeConfig(key.getStrategy()), key.getStrategy().contains("BLIZZARD") ? 1 : 0, num_queries,
                        diffHits,
                        //----------------
                        perc_improv, perc_deter, perc_deter_non_retr, perc_equal, perc_retrieved, perc_not_retrieved,
                        perc_highq,
                        num_improv, num_deter, num_deter_non_retr, num_equal, num_retrieved, num_not_retrieved,
                        num_highq,
                        //-------------
                        percTopBase, percNonTopBase, percTopRemainedTop, percTopBecameNonTop, percNonTopBecameTop,
                        percNonTopRemainedNonTop, percTotalQueries, percTopBecameNonTopWithin2threshold,
                        percNonTopBeyond2thresholdBecameTop,
                        //------------------
                        numTopBase, numNonTopBase, numTopRemainedTop, numTopBecameNonTop, numNonTopBecameTop,
                        numNonTopRemainedNonTop, totalQueries, numTopBecameNonTopWithin2threshold,
                        numNonTopBeyond2thresholdBecameTop
                );
                writer.writeNext(stats.stream().map(Object::toString).collect(Collectors.toList()));
            });
        }

       /*
    Map<EvaluationQueryKey, ReformulationEvaluation> evaluations =
                loadEvaluations(baseRepoPath, statsFilePath);
        Set<Map.Entry<EvaluationQueryKey, ReformulationEvaluation>> entries = evaluations.entrySet();

        LinkedHashMap<GTSTKey, List<ReformulationEvaluation>> map = new LinkedHashMap<>();
        evaluations.entrySet().stream().forEach(e -> {
                    GTSTKey key = e.getKey().gtsKey;
                    List<ReformulationEvaluation> evals = map.computeIfAbsent(key, k -> new LinkedList<>());
                    evals.add(e.getValue());
                }

        );*/

      /* LinkedList<Map.Entry<GTSTKey, List<ReformulationEvaluation>>> entries1 = new LinkedList<>(map.entrySet());
        List<Map.Entry<GTSTKey, List<ReformulationEvaluation>>> sorte = entries1.stream().sorted((e1, e2) -> {
            GTSTKey key1 = e1.getKey();
            GTSTKey key2 = e2.getKey();

            int compare = Integer.compare(key1.getThreshold(), key2.getThreshold());
            if (compare == 0) {
                compare = key1.getGranularity().compareTo(key2.getGranularity());
                if (compare == 0) {
                    compare = key1.getTechnique().compareTo(key2.getTechnique());
                }
            }
            return compare;

        }).collect(Collectors.toList());*/

        System.out.println("done");
    }

    private static String removeConfig(String strategy) {
        List<String> componentNames =
                new ArrayList<>(DiscourseTaskStrategy.splitIntoComponentNames(strategy));

        String lastComponent = componentNames.get(componentNames.size() - 1);

        if (lastComponent.contains("true") || lastComponent.contains("false")) {
            assert lastComponent.equals("true-false");
            componentNames.remove(lastComponent);
        }

        return String.join(DiscourseTaskStrategy.COMPONENT_SEPARATOR, componentNames);
    }

    static class EvaluationQueryKey {
        private final GTSTKey gtsKey;
        Project project;
        String queryID;

        EvaluationQueryKey(GTSTKey gtsKey, Project project, String queryID) {
            // Threshold will be the same for all results of the same query
            this.gtsKey = gtsKey;
            this.project = project;
            this.queryID = queryID;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EvaluationQueryKey that = (EvaluationQueryKey) o;
            return gtsKey.equals(that.gtsKey) &&
                    project.equals(that.project) &&
                    queryID.equals(that.queryID);
        }

        @Override
        public int hashCode() {
            return Objects.hash(gtsKey, project, queryID);
        }
    }
}
