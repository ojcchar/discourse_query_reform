package seers.disqueryreform.evaluation.stats;

import seers.disqueryreform.evaluation.stats.keys.GranularityTechniquePair;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ThresholdSummary {

    private final List<ThresholdSummaryEntry> groups;

    ThresholdSummary(Map<GranularityTechniquePair, List<StrategySummary>> summaries) {
        groups = summaries.entrySet().stream()
                .map(ThresholdSummaryEntry::new)
                .collect(Collectors.toList());
    }

    public Stream<ThresholdSummaryEntry> entries() {
        return groups.stream();
    }

    public class ThresholdSummaryEntry {

        private final GranularityTechniquePair key;
        private final List<StrategySummary> summaries;

        ThresholdSummaryEntry(Map.Entry<GranularityTechniquePair, List<StrategySummary>> entry) {
            key = entry.getKey();
            summaries = entry.getValue();
        }

        public GranularityTechniquePair getKey() {
            return key;
        }

        public List<StrategySummary> getSummaries() {
            return summaries;
        }
    }

    public OverallStrategySummary toOverallStrategySummary() {
        return new OverallStrategySummary(groups);
    }
}
