package seers.disqueryreform.retrieval.lobster;

import edu.wayne.cs.severe.ir4se.processor.boundary.MainMassive;
import edu.wayne.cs.severe.ir4se.processor.utils.GenericConstants;

@Deprecated
public class LobsterRunner {
    public static void main(String[] args) throws Exception {
        String[] args2 = {"lobster/param_file.csv", GenericConstants.CG};
        MainMassive.execute(args2, new MassiveProcessorThreaded());
//        MainMassive.execute(args2, new MassiveProcessor());
    }
}
