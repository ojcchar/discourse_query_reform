package seers.disqueryreform.results.rstats;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.QueryReformStrategy;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RStatsRunner {

    private static Logger LOGGER = LoggerFactory.getLogger(RStatsRunner.class);

    public static final String ALL_DATASETS = "all_datasets";
    public static final String ALL_GRANULARITIES = "all_granularities";
    public static final String NO_TOPK_REMOVAL = "no-topk-removal";


    public static List<String> getRankThresholds(int limit, boolean addNoTopRemoval) {
        final List<String> ranks = IntStream.range(1, limit + 1)
                .boxed()
                .map(String::valueOf)
                .collect(Collectors.toList());
        if (addNoTopRemoval)
            ranks.add(NO_TOPK_REMOVAL);
        return ranks;
    }

    public static void main(String[] args) throws Exception {

        final Class<CombinationsProcessor> combClass = CombinationsProcessor.class;
        final Class<StrategySummarizer> summarizerClass = StrategySummarizer.class;

        DocumentGranularity[] granularities = DocumentGranularity.getCodeGranularities();
        Set<String> techniques = JavaUtils.getSet("locus", "lucene", "brtracer", "buglocator", "lobster");
        List<String> thresholds = getRankThresholds(30, true);
        List<String> reformStrategies = QueryReformStrategy.getAllStrategiesExceptBaseline().stream()
                .map(Object::toString).collect(Collectors.toList());

        String scriptPath = Paths.get("C:", "Users", "ojcch", "Documents", "Repositories", "Git",
                "discourse_query_reform", "code", "scripts", "new_scripts", "results_analysis_updated.r").toString();
        //---------------------------------------

        String baseFolder = getDefaultBaseFolder();

        runAnalysis(combClass, summarizerClass, granularities, techniques, thresholds,
                reformStrategies, baseFolder, scriptPath);

    }

    public static String getDefaultBaseFolder() {
        return Paths.get("C:", "Users", "ojcch", "Documents", "Projects",
                "Discourse_query_reformulation", "results").toString();
    }

    public static void runAnalysis(Class<? extends CombinationsProcessor> combClass,
                                   Class<? extends StrategySummarizer> summarizerClass,
                                   DocumentGranularity[] granularities,
                                   Set<String> techniques, List<String> thresholds,
                                   List<String> reformStrategies, String baseFolder, String scriptPath) throws Exception {

        LOGGER.debug(baseFolder);

        String analysisFormatFolder = Paths.get(baseFolder, "2_analysis-format").toString();
        String tempFolder = Paths.get(baseFolder, "temp").toString();
        String tempFolder2 = Paths.get(baseFolder, "temp2").toString();
        String resultsFolder = getResultsFolder(baseFolder);
        String resultsFolderPairWise = Paths.get(baseFolder, "5_results-analysis_pairwise").toString();
        String resultsSummaryFolder = getResultsSummaryFolder(baseFolder);
        String resultsSummaryFolderPairWise = Paths.get(baseFolder, "6_results-summary-pairswise").toString();

        //-----------------

        List<List<String>> bugs = CSVHelper.readCsv(Paths.get(analysisFormatFolder,
                "bug-list.csv").toFile(), true, Function.identity(),
                CSVHelper.DEFAULT_SEPARATOR, "UTF-8");
        List<List<String>> stats = CSVHelper.readCsv(Paths.get(analysisFormatFolder,
                "overall-stats.csv").toFile(), true, Function.identity(),
                CSVHelper.DEFAULT_SEPARATOR, "UTF-8");

        //---------------

        List<List<String>> resultsCombinations = getCombinations(granularities, reformStrategies,
                techniques, thresholds);
        final ThreadParameters params = new ThreadParameters();
        params.addParam("bugs", bugs);
        params.addParam("stats", stats);
        params.addParam("tempFolder", tempFolder);
        params.addParam("scriptPath", scriptPath);
        params.addParam("techniques", techniques);
        params.addParam("granularities", granularities);
        params.addParam("thresholds", thresholds);
        params.addParam("tempFolder2", tempFolder2);
        params.addParam("resultsFolder", resultsFolder);
        params.addParam("resultsFolderPairWise", resultsFolderPairWise);
        params.addParam("resultsSummaryFolder", resultsSummaryFolder);
        params.addParam("resultsSummaryFolderPairWise", resultsSummaryFolderPairWise);

        ThreadExecutor.executePaginated(resultsCombinations, combClass, params, 8);
        ThreadExecutor.executePaginated(reformStrategies, summarizerClass, params, 8);

        //---------------

        /*List<List<String>> resultsPairWiseCombinations = getPairWiseCombinations();
        ThreadExecutor.executePaginated(resultsPairWiseCombinations, PairWiseCombinationsProcessor.class,
                new ThreadParameters());

        List<ImmutablePair<QueryReformStrategy, List<QueryReformStrategy>>> pairWiseReformStrategies =
                getPairWiseStrategies();
        //FIXME: PairWiseStrategySummarizer would not work for other thresholds??
        //FIXME: Check PairWiseStrategySummarizer.addNewLines
        PairWiseStrategySummarizer summarizer = new PairWiseStrategySummarizer(pairWiseReformStrategies);
        summarizer.executeJob();*/
    }

    public static String getResultsFolder(String baseFolder) {
        return Paths.get(baseFolder, "3_results-analysis2").toString();
    }

    public static String getResultsSummaryFolder(String baseFolder) {
        return Paths.get(baseFolder, "4_results-summary2").toString();
    }

    private static List<ImmutablePair<String, List<String>>> getPairWiseStrategies(List<String> reformStrategies) {
        List<ImmutablePair<String, List<String>>> strategies = new ArrayList<>();
        for (int i = 0; i < reformStrategies.size() - 1; i++) {
            final String reformStrategy1 = reformStrategies.get(i);
            strategies.add(
                    new ImmutablePair<>(reformStrategy1, reformStrategies));
        }
        return strategies;
    }

    private static List<List<String>> getPairWiseCombinations(
            List<String> reformStrategies, Set<String> techniques,
            List<String> thresholds) {
        List<List<String>> pairWiseCombinations = new ArrayList<>();

        addPairWiseCombinations(pairWiseCombinations, ALL_GRANULARITIES, reformStrategies, techniques, thresholds);

        return pairWiseCombinations;
    }

    private static void addPairWiseCombinations(List<List<String>> resultsCombinations, String granularity,
                                                List<String> reformStrategies, Set<String> techniques,
                                                List<String> thresholds) {
        for (String technique : techniques) {
            for (int i = 0; i < reformStrategies.size(); i++) {
                for (int j = i + 1; j < reformStrategies.size(); j++) {

                    final String reformStrategy1 = reformStrategies.get(i);
                    final String reformStrategy2 = reformStrategies.get(j);

                    for (String threshold : thresholds) {

                        if (NO_TOPK_REMOVAL.equals(threshold))
                            continue;

                        List<String> combination = Arrays.asList(granularity,
                                technique, reformStrategy1.toString(), reformStrategy2.toString(), threshold);
                        resultsCombinations.add(combination);
                    }
                }
            }
        }
    }

    public static List<List<String>> getCombinations(DocumentGranularity[] granularities,
                                                     List<String> reformStrategies, Set<String> techniques,
                                                     List<String> thresholds) {
        List<List<String>> resultsCombinations = new ArrayList<>();

        for (DocumentGranularity granularity : granularities) {
            addCombinations(resultsCombinations, granularity.toString().toLowerCase(), reformStrategies, techniques,
                    thresholds);
        }

        addCombinations(resultsCombinations, ALL_GRANULARITIES, reformStrategies, techniques,
                thresholds);

        return resultsCombinations;
    }

    private static void addCombinations(List<List<String>> resultsCombinations, String granularity,
                                        List<String> reformStrategies, Set<String> techniques,
                                        List<String> thresholds) {
        for (String technique : techniques) {
            for (String reformStrategy : reformStrategies) {
                for (String threshold : thresholds) {
                    List<String> combination = Arrays.asList(granularity,
                            technique, reformStrategy, threshold);
                    resultsCombinations.add(combination);
                }
            }
        }
    }


    //-----------------------------------

}
