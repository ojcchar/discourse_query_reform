package seers.disqueryreform.retrieval.bl.brt;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.utdallas.seers.brtracer.BRTracer;
import edu.utdallas.seers.buglocator.BugLocator;
import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.ir4se.index.utils.DateTimeJsonAdapter;
import edu.utdallas.seers.ir4se.retrieval.Retriever;
import edu.wayne.cs.severe.ir4se.processor.controllers.RetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.lucene.search.ScoreDoc;
import org.joda.time.DateTime;
import seers.appcore.csv.CSVHelper;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.retrieval.lucene.LuceneMassiveMain;
import seers.disqueryreform.retrieval.lucene.LuceneProcessor;
import utils.SourceFileIDFinder;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("ALL")
@Deprecated
public class BLBRTRunner {
    private static final String BASELINE_STRATEGY = "ALL_TEXT";

    private static Path corpusFolderPath;
    private static Path outputFolderPath;
    private static Float alpha;

    private static Path indexPath;
    private static Map<String, Map<String, BugReport>> originalQueriesForSystem;
    private static List<Integer> rankThresholds;
    private static Path reformulatedQueriesPath;
    private static DataSet dataSet;
    private static Map<String, String> b4blIDToSystem;

    public static void run(DataSet dataSet, Path corpusFolderPath, Path refQueriesPath, float alpha, Path outputDirPath,
                           List<String> strategies, List<Integer> thresholds) throws Exception {
        BLBRTRunner.dataSet = dataSet;
        BLBRTRunner.corpusFolderPath = corpusFolderPath;
        BLBRTRunner.reformulatedQueriesPath = refQueriesPath;
        BLBRTRunner.alpha = alpha;
        BLBRTRunner.outputFolderPath = outputDirPath;
        BLBRTRunner.rankThresholds = thresholds;
        originalQueriesForSystem = new ConcurrentHashMap<>();

        Set<String> systemNames = loadSystemNames(dataSet, reformulatedQueriesPath, strategies, corpusFolderPath);

        /* Build indexes for the systems beforehand to avoid threading issues
        Also makes sure the index is built with all bug reports instead of building
        it with only the reformulated queries */
        indexPath = Paths.get(FileUtils.getTempDirectoryPath(), "BL-BRT-index");
        File indexFolder = indexPath.toFile();

        FileUtils.forceMkdir(indexFolder);
        FileUtils.forceMkdir(outputFolderPath.toFile());

        for (String systemName : systemNames) {
            new BugLocator.Index(systemName, corpusFolderPath, indexPath).buildIfNotPresent();
            originalQueriesForSystem.put(systemName, loadOriginalQueries(systemName, corpusFolderPath));
        }

        /* Multithreading over strategies and not over queries because the retrievers are already
        optimized across queries */
        ThreadExecutor.executePaginated(strategies, BLBRTProcessor.class, new ThreadParameters(), 8);

        // UGLY: since all these have to be static, we clear them
        originalQueriesForSystem = null;
        b4blIDToSystem = null;
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public static Set<String> loadSystemNames(DataSet dataSet, Path reformulatedQueriesPath,
                                              List<String> strategies, Path corpusFolderPath) {
        /* We have to load all strategies to find all systems for sure, could be changed by
         * outputting a summary when the queries are created, or just creating the index in-memory
         * when it's needed and then sharing it, but this would require synchronization */

        // B4BL has a different structure
        if (dataSet == DataSet.B4BL) {
            return loadB4BLSystemNames(reformulatedQueriesPath, strategies, corpusFolderPath);
        }

        Gson gson = new Gson();
        File[] strategyFiles = reformulatedQueriesPath.toFile().listFiles(
                f -> strategies.contains(f.getName().replace(".json", "")));
        return Collections.emptySet();
//        return Arrays.stream(strategyFiles)
//                .flatMap(f -> {
//                    try {
//                        StrategyQueryContainer<Map> container =
//                                gson.fromJson(new FileReader(f), StrategyQueryContainer.class);
//
//                        return container.getItems().stream()
//                                .map(Map.Entry::getKey);
//                    } catch (FileNotFoundException e) {
//                        throw new UncheckedIOException(e);
//                    }
//                })
//                .collect(Collectors.toSet());
    }

    @SuppressWarnings({"ConstantConditions", "unchecked", "SuspiciousMethodCalls"})
    private static Set<String> loadB4BLSystemNames(Path reformulatedQueriesPath, List<String> strategies, Path corpusFolderPath) {
        b4blIDToSystem = loadB4BLIDToProjectMap(corpusFolderPath);

        Gson gson = new Gson();
        File[] strategyFiles = reformulatedQueriesPath.toFile().listFiles(
                f -> strategies.contains(f.getName().replace(".json", "")));

        return Collections.emptySet();
//        return Arrays.stream(strategyFiles)
//                .flatMap(f -> {
//                    try {
//                        StrategyQueryContainer<Map> container =
//                                gson.fromJson(new FileReader(f), StrategyQueryContainer.class);
//
//                        /* Convert each bug ID for the strategy to its real system */
//                        return container.getItems().stream()
//                                .flatMap(e -> e.getValue().stream()
//                                        .map(m -> b4blIDToSystem.get(m.get("bug_id"))));
//                    } catch (FileNotFoundException e) {
//                        throw new UncheckedIOException(e);
//                    }
//                })
//                .collect(Collectors.toSet());
    }

    @SuppressWarnings("ConstantConditions")
    public static Map<String, String> loadB4BLIDToProjectMap(Path corpusFolderPath) {
        /* Here we must load every query and get the full system for it (with version) */
        return Arrays.stream(corpusFolderPath.toFile().listFiles(f -> f.getName().startsWith("bug-reports-versions-")))
                .flatMap(f -> {
                    try {
                        return CSVHelper.readCsv(f.getAbsolutePath(), false, ';').stream();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .collect(Collectors.toMap(l -> l.get(1), l -> l.get(0)));
    }

    public static Map<String, BugReport> loadOriginalQueries(String systemName, Path corpusFolderPath) throws
            IOException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeJsonAdapter());
        Gson gson = gsonBuilder.create();

        File bugReportsFile = corpusFolderPath.resolve(Paths.get(systemName, "bug-reports.json")).toFile();
        try (BufferedReader r = new BufferedReader(new FileReader(bugReportsFile))) {
            return r.lines()
                    .map(l -> gson.fromJson(l, BugReport.class))
                    .collect(Collectors.toMap(BugReport::getKey, b -> b,
                            (b1, b2) -> b1, ConcurrentHashMap::new));
        }
    }

    public static class BLBRTProcessor extends ThreadProcessor {
        private final List<String> strategies;

        public BLBRTProcessor(ThreadParameters params) {
            super(params);
            strategies = params.getListParam(String.class, ThreadExecutor.ELEMENTS_PARAM);
        }

        @Override
        public void executeJob() throws Exception {
            for (String strategy : strategies) {
                Map<String, List<Query>> allQueriesBySystem = loadQueriesBySystem(dataSet, reformulatedQueriesPath, strategy);

                runApproach(strategy, "buglocator", allQueriesBySystem);
                runApproach(strategy, "brtracer", allQueriesBySystem);

            }
        }

        public static Map<String, List<Query>> loadQueriesBySystem(DataSet dataSet, Path reformulatedQueriesPath, String strategy)
                throws IOException {

            return LuceneMassiveMain.readQueries(reformulatedQueriesPath.toString(), strategy).stream()
                    .collect(Collectors.toMap(
                            q -> {
                                // If B4BL we must set the system name with version for each query
                                if (dataSet == DataSet.B4BL) {
                                    return b4blIDToSystem.get(q.getKey());
                                }

                                return (String) q.getInfoAttribute("system");
                            },
                            Collections::singletonList,
                            (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList())
                    ));
        }

        private void runApproach(String strategy, String approachName, Map<String, List<Query>> allReformulatedQueries)
                throws Exception {
            Path outPath = outputFolderPath.resolve(
                    Paths.get(approachName, "and", "TITLE_DESCRIPTION", strategy));

            // For interrupted execution
            if (outPath.toFile().exists()) {
                LOGGER.info(String.format("Results for %s with %s already exist, skipping...",
                        strategy, approachName));
                return;
            }

            Map<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsPerK = new
                    ConcurrentHashMap<>();
            for (Integer rankThreshold : rankThresholds) {
                queryEvalsPerK.put(rankThreshold, new ConcurrentHashMap<>());
            }

            Map<Query, ImmutablePair<List<Double>, List<Double>>> originalQueryEvals = new ConcurrentHashMap<>();

            //------------------------------

            for (String systemName : allReformulatedQueries.keySet()) {
                Retriever retriever = null;
                if ("buglocator".equals(approachName)) {
                    retriever = BugLocator.buildRetriever(systemName, corpusFolderPath, indexPath, alpha);
                } else if ("brtracer".equals(approachName)) {
                    retriever = BRTracer.buildRetriever(systemName, corpusFolderPath, indexPath, alpha);
                }

                processSystem(strategy, systemName, approachName, retriever, queryEvalsPerK,
                        originalQueryEvals, allReformulatedQueries.get(systemName));
            }

            //------------------------------------

            FileUtils.forceMkdir(outPath.toFile());
            LuceneMassiveMain.writeStats3(strategy, queryEvalsPerK,
                    originalQueryEvals, BASELINE_STRATEGY, outPath.toString());
        }

        private void processSystem(String strategy, String systemName, String approachName, Retriever retriever,
                                   Map<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>>
                                           queryEvalsPerK, Map<Query, ImmutablePair<List<Double>, List<Double>>>
                                           originalQueryEvals, List<Query> reformulatedQueries)
                throws Exception {
            LOGGER.info(String.format("Processing system %s with %s (%s)", systemName, approachName, strategy));

            RetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();

            Map<String, BugReport> originalQueries = originalQueriesForSystem.get(systemName);

            SourceFileIDFinder sourceFileIDFinder = new SourceFileIDFinder(indexPath.resolve(Paths.get("source-code",
                    systemName)));

            for (Query reformulatedQuery : reformulatedQueries) {
                BugReport originalBugReport = originalQueries.get(reformulatedQuery.getKey());
                BugReport reformulatedBugReport = createReformulatedBugReport(reformulatedQuery, originalBugReport);

                /* Build relevance judgment */
                RelJudgment goldSet = new RelJudgment();
                List<String> fixedFiles = originalBugReport.getFixedFiles();
                List<RetrievalDoc> relevantDocs = fixedFiles.stream()
                        .map(n -> {
                            try {
                                return new RetrievalDoc(sourceFileIDFinder.get(n), null, null);
                            } catch (IOException e) {
                                throw new UncheckedIOException(e);
                            }
                        })
                        .collect(Collectors.toList());
                goldSet.setRelevantDocs(relevantDocs);

                /* Get retrieval results */
                List<RetrievalDoc> originalQueryResults = convertToRetrievalDocs(
                        retriever.locate(originalBugReport, true).getScores());
                List<RetrievalDoc> reformulatedQueryResults = convertToRetrievalDocs(
                        retriever.locate(reformulatedBugReport, true).getScores());

                List<Double> originalQueryMetrics = evaluator.evaluateRelJudgment(goldSet, originalQueryResults);
                List<Double> reformulatedQueryMetrics = evaluator.evaluateRelJudgment(goldSet,
                        reformulatedQueryResults);

                originalQueryEvals.put(reformulatedQuery, new ImmutablePair<>(originalQueryMetrics,
                        reformulatedQueryMetrics));

                int originalQueryRank = originalQueryMetrics.get(0).intValue();

                LuceneProcessor.processThresholds(reformulatedQuery, systemName + ';' + reformulatedQuery.getKey(),
                        goldSet, originalQueryResults, reformulatedQueryResults, originalQueryRank, rankThresholds,
                        queryEvalsPerK);
            }

        }

        private List<RetrievalDoc> convertToRetrievalDocs(ScoreDoc[] docs) {
            return Arrays.stream(docs)
                    .map(d -> new RetrievalDoc(d.doc, null, null))
                    .collect(Collectors.toList());
        }
    }

    public static BugReport createReformulatedBugReport(Query reformulatedQuery, BugReport originalBugReport) {
        BugReport bugReport = new BugReport(originalBugReport.getKey(), null,
                reformulatedQuery.getTxt(), originalBugReport.getCreationDate(),
                originalBugReport.getResolutionDate(), originalBugReport.getFixedFiles());
        bugReport.setOriginalText(originalBugReport.getOriginalText());
        return bugReport;
    }
}
