package seers.disqueryreform.results.rstats;

import seers.disqueryreform.base.DataSet;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CombinationData {

    private boolean noData;
    private List<String> combination;
    private ArrayList<List<String>> localBugs;
    private ArrayList<List<String>> localStats;
    private List<List<String>> combinationBugs;
    private List<List<String>> combinationStats;

    public CombinationData(List<String> combination, ArrayList<List<String>> localBugs,
                           ArrayList<List<String>> localStats) {
        this.combination = combination;
        this.localBugs = localBugs;
        this.localStats = localStats;
    }

    boolean isNoData() {
        return noData;
    }

    public List<List<String>> getCombinationBugs() {
        return combinationBugs;
    }

    public List<List<String>> getCombinationStats() {
        return combinationStats;
    }

    public CombinationData readData() {
        final Predicate<List<String>> filterPredicate = data -> {

            final String granularity = combination.get(0);

            List<String> dataSets = getDataSets(granularity);

            for (int i = 1; i < combination.size(); i++) {
                if (!combination.get(i).equals(data.get(i)))
                    return false;
            }
            if (RStatsRunner.ALL_GRANULARITIES.equals(granularity))
                return true;
            else
                return dataSets.contains(data.get(0));
        };
        combinationBugs = localBugs.stream().filter(filterPredicate)
                .map(bug -> Arrays.asList(bug.get(0), bug.get(4), bug.get(5)))
                .collect(Collectors.toList());

        if (combinationBugs.isEmpty()) {
            noData = true;
            return this;
        }

        combinationStats = localStats.stream().filter(filterPredicate)
                .map(stats -> {
                    final List<String> stats2 = new LinkedList<>(stats.subList(combination.size(),
                            stats.size()));
                    stats2.add(0, stats.get(0));
                    return stats2;
                }).collect(Collectors.toList());

        if (combinationStats.isEmpty()) {
            noData = true;
            return this;
        }
        noData = false;
        return this;
    }

    public List<String> getDataSets(String granularity) {

        if (RStatsRunner.ALL_GRANULARITIES.equals(granularity))
            return Arrays.stream(DataSet.values())
                    .map(d -> d.toString().toLowerCase())
                    .collect(Collectors.toList());

        final DocumentGranularity gran = DocumentGranularity.valueOf(granularity.toUpperCase());
        switch (gran) {
            case FILE:
                return Arrays.asList(DataSet.BRT.toString().toLowerCase(),
                        DataSet.B4BL.toString().toLowerCase());
            case CLASS:
                return Collections.singletonList(DataSet.LB.toString().toLowerCase());
            case METHOD:
                return Arrays.asList(DataSet.D4J.toString().toLowerCase(),
                        DataSet.QQ.toString().toLowerCase());
            default:
                throw new RuntimeException();
        }
    }
}
