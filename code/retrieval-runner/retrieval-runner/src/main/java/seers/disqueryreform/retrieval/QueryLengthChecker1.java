package seers.disqueryreform.retrieval;

import java.io.File;
import java.util.List;

import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import seers.disqueryreform.base.RetrievalUtils;
import seers.disqueryreform.retrieval.lucene.LuceneMain;

public class QueryLengthChecker1 {

	static String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/d4j-generated-all-hard-to-retrieve-queries_or_prep";
	//
	// static String queriesFolder =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/query_quality-generated-all-hard-to-retrieve-queries_or_prep";

	public static void main(String[] args) throws Exception {

		List<Query> queries = RetrievalUtils.readReformulatedQueries(queriesFolder + File.separator + "OB", true);

		for (Query query : queries) {

			String txt = query.getTxt();
			int length = txt.split(" ").length;

			String system = (String) query.getInfoAttribute("system");

			System.out.println(system + ";" + query.getKey() + ";" + length);
		}

	}

}
