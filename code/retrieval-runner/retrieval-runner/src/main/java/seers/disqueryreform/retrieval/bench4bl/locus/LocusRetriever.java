package seers.disqueryreform.retrieval.bench4bl.locus;

import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import org.apache.commons.io.FileUtils;
import seers.disqueryreform.retrieval.bench4bl.Bench4BLResults;
import seers.disqueryreform.retrieval.bench4bl.Bench4BLRetriever;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class LocusRetriever extends Bench4BLRetriever {
    private final Path locusBinPath;
    private Path configFilePath;
    private final Path locusEnvPath;

    public LocusRetriever(boolean cleanUpAfter, Path baseRepositoryPath) {
        super(cleanUpAfter);
        locusBinPath = baseRepositoryPath.resolve(Paths.get("code", "other-tools", "Locus.jar"));
        locusEnvPath = baseRepositoryPath.resolve(Paths.get("code", "other-tools", "Locus-env")).toAbsolutePath();
    }

    private void writeConfigFile(Path configFilePath, String alias,
                                 String repoDirPath, String sourceDir, String xmlBugReportsPath,
                                 Path workingDirectoryPath) throws IOException {
        String workingLoc = workingDirectoryPath.toString();
        // Must have a trailing separator
        if (!workingLoc.endsWith(File.separator)) {
            workingLoc = workingLoc + File.separator;
        }

        String versionUnderscore = alias.replace('.', '_');

        FileUtils.writeLines(configFilePath.toFile(), Arrays.asList(
                "task=fileall",
                String.format("Project=%s", alias),
                // Requires it to have the system name here apparently
                String.format("Version=%s_%s", alias, versionUnderscore),
                // repoDir must not have a trailing separator
                String.format("repoDir=%s", repoDirPath.replaceAll(File.separator + "+$", "")),
                // sourceDir must contain no trailing separator either
                String.format("sourceDir=%s", sourceDir.replaceAll(File.separator + "+$", "")),
                String.format("workingLoc=%s", workingLoc),
                String.format("bugReport=%s", xmlBugReportsPath),
                String.format("changeOracle=%s", workingLoc)
        ));
    }

    @Override
    protected String[] buildCommand(String alias, String repoDir, String sourceDir, String xmlBugReportsPath, Path workingDirPath) throws IOException {
        configFilePath = Paths.get(FileUtils.getTempDirectoryPath(), String.format("Locus_%s_%s_config.txt", alias, alias));
        writeConfigFile(configFilePath, alias, repoDir, sourceDir, xmlBugReportsPath, workingDirPath);

        // Locus cannot use a more recent version of git, so we set a path with an older version
        return new String[]{
                "/bin/bash", "-c",
                String.format("PATH=%s java -Xms512m -Xmx10g -jar %s %s",
                        locusEnvPath, locusBinPath, configFilePath.toString())
        };
    }

    @Override
    protected void cleanUp(String xmlBugReportsPath) throws IOException {
        try {
            super.cleanUp(xmlBugReportsPath);
        } finally {
            File file = configFilePath.toFile();
            if (file.exists()) {
                FileUtils.forceDelete(file);
            }
        }
    }

    @Override
    public Bench4BLResults retrieve(String alias, String repoDir, String sourceDir, List<BugReport> queries) throws IOException {
        try {
            return super.retrieve(alias, repoDir, sourceDir, queries);
        } catch (IOException | InterruptedException | EvaluationException | JAXBException e) {
            throw new IOException("Locus execution failed. Make sure these are set up:\n" +
                    "- Git 2.7.4 code/other-tools/build-git\n" +
                    "- java is available at /usr/bin/java (or modify the link code/other-tools/Locus-env/java",
                    e);
        }
    }

    @Override
    protected String getToolName() {
        return "Locus";
    }
}
