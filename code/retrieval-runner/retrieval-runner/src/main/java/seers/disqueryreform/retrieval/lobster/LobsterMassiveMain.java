package seers.disqueryreform.retrieval.lobster;

import edu.uci.ics.jung.graph.Graph;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.RAMRetrievalIndexer;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.callgraph.CallGraphRetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.GraphException;
import edu.wayne.cs.severe.ir4se.processor.exception.ParseException;
import edu.wayne.cs.severe.ir4se.processor.exception.QueryException;
import edu.wayne.cs.severe.ir4se.processor.exception.StackTraceException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.utils.FilePathUtils;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DiscourseTaskStrategy;
import seers.disqueryreform.base.RetrievalUtils;
import seers.disqueryreform.retrieval.lucene.LuceneMassiveMain;
import seers.disqueryreform.retrieval.lucene.LuceneProcessor;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Deprecated
public class LobsterMassiveMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(LobsterMassiveMain.class);
    static List<Integer> rankThresholds;
    private static String graphNTracesFolder;

    public static void main(String[] args) throws Exception {

        String queriesFolder = args[0];
        graphNTracesFolder = args[1];
        String outputDir = args[2];
        String baselineSrategy = DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME;
        List<String> strategies = Arrays.stream(args[4].split(",")).collect(Collectors.toList());
        rankThresholds = JavaUtils.getSet(args[5].split(",")).stream().map(Integer::valueOf).collect
                (Collectors.toList());
        rankThresholds.sort(Comparator.comparing(Integer::intValue));
        DataSet dataSet = DataSet.valueOf(args[6]);
        String baseDataFolder = args[7];

        // -----------------------------------------

        ConcurrentHashMap<String, List<RetrievalDoc>> systemCorpus = new ConcurrentHashMap<>();

        List<Query> baselineQueries = LuceneMassiveMain.readQueries(queriesFolder, DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME);
        setTraces(baselineQueries, graphNTracesFolder);


        LOGGER.debug("# of baseline queries: " + baselineQueries.size());

        ConcurrentMap<String, Graph<String, String>> callGraphsPerSystem = new ConcurrentHashMap<>();
        for (String strategy : strategies) {

            LOGGER.debug("Strategy: " + strategy);

            String strategyOutputDir = outputDir + File.separator + strategy;

            new File(strategyOutputDir).mkdirs();

            List<Query> reducedQueries = LuceneMassiveMain.readQueries(queriesFolder, strategy);
            LOGGER.debug("# of reduced queries: " + reducedQueries.size());


            //--------------------------------------------------------

            //initialize the map that will store the query metrics for each rank threshold
            //rank threshold ->  query -> <baseline metrics, reformulation metrics>
            Map<Integer, Map<Query, ImmutablePair<List<Double>, List<Double>>>> queryEvalsPerK = new
                    ConcurrentHashMap<>();
            for (Integer rankThreshold : rankThresholds) {
                queryEvalsPerK.put(rankThreshold, new ConcurrentHashMap<>());
            }

            //initialize the map that will store the original query metrics from original rankings (i.e, with no
            // modifications
            //query -> <baseline metrics, reformulation metrics>
            Map<Query, ImmutablePair<List<Double>, List<Double>>> originalQueryEvals = new ConcurrentHashMap<>();

            //--------------------------------------------------------

            ThreadParameters params = new ThreadParameters();
            params.addParam("corpusBaseFolder", baseDataFolder);
            params.addParam("baselineQueries", baselineQueries);
            params.addParam("queryEvalsPerK", queryEvalsPerK);
            params.addParam("originalQueryEvals", originalQueryEvals);
            params.addParam("dataSet", dataSet);
            params.addParam("callGraphsPerSystem", callGraphsPerSystem);
            params.addParam("systemCorpus", systemCorpus);

            ThreadExecutor.executePaginated(reducedQueries, LobsterRetrievalProcessor.class, params, 8);

            LuceneMassiveMain.writeStats3(strategy, queryEvalsPerK, originalQueryEvals,
                    baselineSrategy, strategyOutputDir);


        }

        LOGGER.debug("Done!");

    }

    public static class LobsterRetrievalProcessor extends ThreadProcessor {

        private final List<Query> baselineQueries;
        private final Map queryEvalsPerK;
        private final Map originalQueryEvals;
        private final DataSet dataSet;
        private final ConcurrentMap<String, Graph<String, String>> callGraphsPerSystem;
        private final ConcurrentHashMap<String, List<RetrievalDoc>> systemCorpus;
        private List<Query> reducedQueries;
        private String corpusFolder;

        @SuppressWarnings("unchecked")
        public LobsterRetrievalProcessor(ThreadParameters params) {
            super(params);
            reducedQueries = params.getListParam(Query.class, ThreadExecutor.ELEMENTS_PARAM);
            corpusFolder = params.getStringParam("corpusBaseFolder");
            baselineQueries = params.getListParam(Query.class, "baselineQueries");
            queryEvalsPerK = params.getParam(Map.class, "queryEvalsPerK");
            originalQueryEvals = params.getParam(Map.class, "originalQueryEvals");
            dataSet = params.getParam(DataSet.class, "dataSet");
            callGraphsPerSystem = params.getParam(ConcurrentMap.class, "callGraphsPerSystem");
            systemCorpus = params.getParam(ConcurrentHashMap.class, "systemCorpus");

        }

        @SuppressWarnings("unchecked")
        @Override
        public void executeJob() throws Exception {

            DefaultRetrievalEvaluator evaluator = new DefaultRetrievalEvaluator();

            //for each query
            for (Query reducedQuery : reducedQueries) {

                /*if (!reducedQuery.getKey().equals("PIG-1850"))
                    continue;*/

                String project = (String) reducedQuery.getInfoAttribute("system");

                //build the original query
                String projectBugId = project + ";" + reducedQuery.getKey();
                Query originalQuery = LuceneProcessor.getOriginalQuery(baselineQueries, project, reducedQuery.getKey());

                Set<String> traces = (Set<String>) originalQuery.getInfoAttribute("traces");
                if (traces == null || traces.isEmpty()) {
                    // We are only interested in bug reports with stack traces
                    continue;
                }

                LOGGER.debug("Processing reduced query " + projectBugId);

                List<RetrievalDoc> queryCorpus = Collections.emptyList();
//                        RetrievalUtils.readCorpus(systemCorpus, corpusFolder, dataSet,
//                        reducedQuery, project);
                queryCorpus.forEach(doc -> doc.setOriginalDocName(doc.getDocName()));

                // evaluate the relevance judgments
                RelJudgment relJudgment = RetrievalUtils.getRelevantJudgement(dataSet, reducedQuery, queryCorpus);

                if (relJudgment.getRelevantDocs().isEmpty()) {
                    LOGGER.error("No rel jud evaluation for query: " + projectBugId);
                    continue;
                }

                List<RetrievalDoc> originalQueryRetrievedDocs;
                List<RetrievalDoc> reducedQueryRetrievedDocs;

                //index the corpus and run the original and reduced reducedQueries
                try (Directory index = new RAMRetrievalIndexer().buildIndex(queryCorpus, null)) {

                    Map<Query, Set<String>> queryStack = new HashMap<>();
                    queryStack.put(originalQuery, traces);
                    Graph<String, String> callGraph = readCallgraph(callGraphsPerSystem, project);

                    Map<String, String> searcherParams = new HashMap<>();
                    searcherParams.put("alpha", "0.9");
                    searcherParams.put("dt", "3");
                    searcherParams.put("level", "C");
                    searcherParams.put("sp", "D");

                    Map<Integer, RetrievalDoc> idxedCorpus = queryCorpus.stream()
                            .collect(Collectors.toMap(RetrievalDoc::getDocId, d -> d));

                    CallGraphRetrievalSearcher2 searcher = new CallGraphRetrievalSearcher2(index, searcherParams, null, null);
                    searcher.setQueryStack(queryStack);
                    searcher.setCallGraph(callGraph);

                    //query the corpus with the original query
                    originalQueryRetrievedDocs = searcher.searchQuery(originalQuery);

                    searcher = new CallGraphRetrievalSearcher2(index, searcherParams, null, null);
                    searcher.setQueryStack(queryStack);
                    searcher.setCallGraph(callGraph);

                    //query the corpus with the reduced query
                    reducedQueryRetrievedDocs = searcher.searchQuery(reducedQuery);
                }

                //--------------------------------------------
                //compute the performance for the reducedQueries

                //assess performance for the original query
                List<Double> originalQueryMetrics = evaluator.evaluateRelJudgment(relJudgment,
                        originalQueryRetrievedDocs);

                //assess performance for the reformulated query
                List<Double> reducedQueryMetrics = evaluator.evaluateRelJudgment(relJudgment,
                        reducedQueryRetrievedDocs);

                //save these metrics (for debugging purposes)
                originalQueryEvals.put(reducedQuery, new ImmutablePair<>(originalQueryMetrics,
                        reducedQueryMetrics));

                //--------------------------------------------

                //get the rank of the original query
                final int originalQueryRank = originalQueryMetrics.get(0).intValue();
                LuceneProcessor.processThresholds(reducedQuery, projectBugId, relJudgment,
                        originalQueryRetrievedDocs, reducedQueryRetrievedDocs, originalQueryRank, rankThresholds,
                        queryEvalsPerK);


            }
        }

        private Graph<String, String> readCallgraph(ConcurrentMap<String, Graph<String, String>> callGraphsPerSystem,
                                                    String project) throws ParseException, GraphException {
            synchronized (callGraphsPerSystem) {
                Graph<String, String> callGraph = callGraphsPerSystem.get(project);
                if (callGraph == null) {
                    CallGraphRetrievalParser parser = new CallGraphRetrievalParser();
                    final File baseDir = new File(graphNTracesFolder);
                    final String graphFilePath = FilePathUtils.getFile(baseDir, project, project + "_Graph.txt")
                            .getAbsolutePath();
                    callGraph = parser.readCallGraph(graphFilePath, "C");
                    callGraphsPerSystem.put(project, callGraph);
                }
                return callGraph;
            }
        }

    }


    private static void setTraces(List<Query> baselineQueries, String graphNTracesFolder) throws Exception {

        for (Query query : baselineQueries) {
            String system = (String) query.getInfoAttribute("system");
            Set<String> traces = getQueryStackTraces(system, graphNTracesFolder, query);
            if (traces == null)
                throw new RuntimeException("No traces found for " + system + ";" + query.getKey());
            query.addInfoAttribute("traces", traces);
        }


    }

    static Map<String, Map<String, Set<String>>> tracesPerSystem = new HashMap<>();

    private static Set<String> getQueryStackTraces(String system, String graphNTracesFolder, Query query) throws
            Exception {

        Map<String, Set<String>> queryTraces = tracesPerSystem.get(system);
        if (queryTraces == null) {
            readAndIndexTraces(system, graphNTracesFolder);
            queryTraces = tracesPerSystem.get(system);
        }

        return queryTraces.get(system + ";" + query.getKey());
    }

    private static void readAndIndexTraces(String system, String graphNTracesFolder) throws StackTraceException,
            QueryException {

        //----------------------------------------------

        final File systemDir = FilePathUtils.getFile(new File(graphNTracesFolder), system);
        final String queriesFilePath = FilePathUtils.getFile(systemDir, system + "_Queries.txt").getAbsolutePath();
        final String originalQueriesFilePath = FilePathUtils.getFile(systemDir, system + "_Original_Queries.txt")
                .getAbsolutePath();
        final String stackFilePath = FilePathUtils.getFile(systemDir, system + "_Stack.txt").getAbsolutePath();

        //----------------------------------------------

        CallGraphRetrievalParser parser = new CallGraphRetrievalParser();
        Map<Query, Set<String>> queryStack = parser.readQueryStack(stackFilePath);
        List<Query> queries = parser.readQueriesWithKey(queriesFilePath, originalQueriesFilePath);

        Map<String, Set<String>> queryTraces = new HashMap<>();
        for (Query query : queries) {
            final String key = system + ";" + query.getKey();
            final Set<String> traces = queryStack.get(query);
            if (traces == null)
                throw new RuntimeException("No traces found for " + key);
            queryTraces.put(key, traces);
        }
        tracesPerSystem.put(system, queryTraces);
    }
}
