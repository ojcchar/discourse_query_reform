package seers.disqueryreform.evaluation.stats;

import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.util.List;
import java.util.stream.Stream;

public class TechniqueStrategyCollection {

    private final RetrievalTechnique technique;
    private final List<EvaluationStatsTable.TableEntry> entries;

    public TechniqueStrategyCollection(RetrievalTechnique key, List<EvaluationStatsTable.TableEntry> value) {
        technique = key;
        entries = value;
    }

    public Stream<EvaluationStatsTable.TableEntry> allEntries() {
        return entries.stream();
    }

    public RetrievalTechnique getTechnique() {
        return technique;
    }
}
