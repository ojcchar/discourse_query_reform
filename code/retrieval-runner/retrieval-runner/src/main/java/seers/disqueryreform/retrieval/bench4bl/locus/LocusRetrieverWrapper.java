package seers.disqueryreform.retrieval.bench4bl.locus;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.ir4se.index.utils.DateTimeJsonAdapter;
import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import org.joda.time.DateTime;
import seers.disqueryreform.RetrievalSettings;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.GeneralUtils;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.retrieval.*;
import seers.disqueryreform.retrieval.bench4bl.Bench4BLResults;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocusRetrieverWrapper extends ApproachRetriever {
    public static final int THREAD_POOL_SIZE = 3;

    /**
     * Maximum amount of queries that will be run at a time.
     */
    private static final int QUERY_LIST_PARTITION_SIZE = 500;

    private static final List<String> ECLIPSE_SUB_PROJECTS =
            Arrays.asList("jdt.core-3.1", "jdt.debug-3.1", "jdt.ui-3.1", "pde.build-3.1",
                    "pde.ui-3.1", "eclipse.platform.debug-3.1", "eclipse.platform.ui-3.1");

    private static final String CORPUS_PARTITION_DELIMITER = "__";

    private final Path existingDataSetsPath;
    private final Path resultsOutputPath;

    public LocusRetrieverWrapper(ResultsLineWriter resultsWriter, Path existingDataSetsPath, Path resultsOutputPath) {
        super(resultsWriter, existingDataSetsPath, RetrievalTechnique.LOCUS, false);
        this.existingDataSetsPath = existingDataSetsPath;
        this.resultsOutputPath = resultsOutputPath;
    }

    public static Map<String, String> loadEclipseSplits(Path existingDataSetsPath) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(DateTime.class, new DateTimeJsonAdapter())
                .create();

        Project project = new Project(DataSet.BRT, "eclipse-3.1");

        Map<String, String> keyToSystem = new HashMap<>();

        for (String subProject : ECLIPSE_SUB_PROJECTS) {
            File subProjectBugReportsFile = project.getDataSet()
                    .resolveCorpusPath(existingDataSetsPath)
                    .resolve(subProject)
                    .resolve("original-bug-reports.json").toFile();

            try (BufferedReader reader = new BufferedReader(new FileReader(subProjectBugReportsFile))) {
                Map<String, String> mapping = reader.lines()
                        .map(l -> gson.fromJson(l, BugReport.class))
                        .collect(Collectors.toMap(
                                BugReport::getKey,
                                b -> subProject
                        ));

                keyToSystem.putAll(mapping);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

        return keyToSystem;
    }

    /**
     * Since Locus cannot run the entire eclipse dataset, we must split it into its subsystems.
     *
     * @param queries The queries
     * @return Split corpora. Will most likely not contain all queries passed.
     */
    private Stream<Corpus> splitEclipseQueries(List<DiscourseQuery> queries) {
        Path existingDataSetsPath = this.existingDataSetsPath;

        Map<String, String> keyToSystem = loadEclipseSplits(existingDataSetsPath);

        Project project = new Project(DataSet.BRT, "eclipse-3.1");

        return queries.stream()
                .filter(q -> keyToSystem.containsKey(q.getKey()))
                .collect(Collectors.groupingBy(q -> keyToSystem.get(q.getKey())))
                .entrySet().stream()
                .map(corpusGrouper(project));
    }

    private void saveCorpusIDs(String projectName, Bench4BLResults results) {
        synchronized (this) {
            File outputFile = resultsOutputPath.resolve(projectName + "-locus-corpus-IDs.json").toFile();

            // Each project is split into potentially many parts, so another thread might have written it already
            if (outputFile.exists()) {
                return;
            }

            Gson gson = new Gson();

            try (FileWriter writer = new FileWriter(outputFile)) {
                gson.toJson(results.getCorpus().values(), writer);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    /**
     * Overridden so that we can control the number of threads that will be used. It will use a lot
     * of memory if too many threads are used.
     *
     * @param project Project.
     * @param queries Queries.
     */
    @Override
    protected void runApproach(Project project, List<DiscourseQuery> queries) {
        try {
            new ForkJoinPool(RetrievalSettings.getInstance().getLocusThreadPoolSize())
                    .submit(() -> super.runApproach(project, queries))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            throw new Error(e);
        }
    }

    @Override
    protected void runForCorpus(Corpus corpus) {
        Bench4BLResults results;

        Project project = corpus.getProject();

        List<BugReport> queryBugReports = corpus.getQueries().stream()
                .map(q -> q.toBugReport(true, project))
                .collect(Collectors.toList());

        String corpusName = corpus.getName();
        String realProjectName = corpusName.split(CORPUS_PARTITION_DELIMITER)[0];

        try {
            Path baseRepositoryPath = existingDataSetsPath.resolve(Paths.get("..", ".."));
            Path corpusPath = project.getDataSet().resolveCorpusPath(existingDataSetsPath).resolve(realProjectName);

            results = new LocusRetriever(true, baseRepositoryPath)
                    .retrieve(corpusName, corpusPath.resolve("git-repo").toString(),
                            corpusPath.resolve("sources").toString(),
                            queryBugReports);
        } catch (IOException e) {
            throw new Error(String.format("Error when preprocessing %s with Locus", corpus), e);
        }

        Map<String, DiscourseQuery> idToQuery = corpus.getQueries().stream()
                .collect(Collectors.toMap(
                        q -> String.valueOf(q.getId()),
                        Function.identity()
                ));

        results.loadAllQueryResults(queryBugReports)
                .map(r -> new RetrievalResults(
                        project,
                        idToQuery.get(r.getId()),
                        r.getResultList(),
                        r.getJudgment(),
                        RetrievalTechnique.LOCUS
                ))
                .forEach(this::writeQueryResults);

        // Save corpus IDs just in case
        saveCorpusIDs(realProjectName, results);

        // Closing removes the working directory
        try {
            results.close();
        } catch (IOException e) {
            throw new Error("Error closing Locus results", e);
        }
    }

    @Override
    protected Stream<Corpus> loadCorpora(Project project, List<DiscourseQuery> queries) {
        return super.loadCorpora(project, queries)
                .flatMap(c -> {
                    if (c.getProject().equals(new Project(DataSet.BRT, "eclipse-3.1"))) {
                        return splitEclipseQueries(queries);
                    } else {
                        return Stream.of(c);
                    }
                })
                // The Locus implementation is memory-hungry, so split if there are too many queries
                .flatMap(c -> {
                    // Sort of ugly but it's necessary so that they all have different names and don't clash
                    AtomicInteger sequence = new AtomicInteger(1);

                    /* partitionList is smart enough to return a single list if corpus is smaller
                    than partition size */
                    return GeneralUtils.partitionList(c.getQueries(), QUERY_LIST_PARTITION_SIZE)
                            .map(qs -> new Corpus(
                                    c.getName() + CORPUS_PARTITION_DELIMITER + sequence.getAndIncrement(),
                                    c.getProject(),
                                    Collections.emptyList(),
                                    qs
                            ));
                })
                /* Collect so that it actually does multiple threads. Since it is not loading any
                 * corpora, this should not cause major memory overhead */
                .collect(Collectors.toList())
                .parallelStream();
    }
}
