package seers.disqueryreform.retrieval;

import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.retrieval.bench4bl.locus.LocusRetrieverWrapper;
import seers.disqueryreform.retrieval.bl.brt.BLRetriever;
import seers.disqueryreform.retrieval.bl.brt.BRTRetriever;
import seers.disqueryreform.retrieval.lobster.LobsterRetriever;
import seers.disqueryreform.retrieval.lucene.LuceneRetriever;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiscourseReformulationRetriever {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiscourseReformulationRetriever.class);

    private final Path resultsOutputPath;
    private final Path existingDataSetsPath;

    public DiscourseReformulationRetriever(Path resultsOutputPath, Path existingDataSetsPath) {
        this.resultsOutputPath = resultsOutputPath;
        this.existingDataSetsPath = existingDataSetsPath;
    }

    public void runQueries(QueryGrouper queryGrouper) throws IOException {
        try (ResultsLineWriter writer = ResultsLineWriter.createResultsLineWriter(resultsOutputPath)) {
            LOGGER.info(String.format("%d queries are already executed",
                    writer.getAlreadyExecutedKeys().size()));

            queryGrouper.loadQueriesByProject()
                    .forEach(data -> processProject(data, writer));

            LOGGER.info("Finished retrieving queries");
        }
    }

    private void processProject(Pair<Project, List<DiscourseQuery>> data, ResultsLineWriter resultsWriter) {
        LOGGER.info("Processing project " + data.getKey());

        for (ApproachRetriever retriever : getRetrieversForProject(data.getKey(), resultsWriter)) {
            retriever.runQueries(data.getKey(), data.getValue());
        }
    }

    private List<ApproachRetriever> getRetrieversForProject(Project project, ResultsLineWriter resultsWriter) {
        DataSet projectDataSet = project.getDataSet();

        List<ApproachRetriever> retrievers = new ArrayList<>(Collections.singletonList(
                new LuceneRetriever(resultsWriter, existingDataSetsPath)
        ));

        if (projectDataSet.equals(DataSet.B4BL) ||
                projectDataSet.equals(DataSet.BRT)) {
            retrievers.add(new LocusRetrieverWrapper(resultsWriter, existingDataSetsPath, resultsOutputPath));
            retrievers.add(new BLRetriever(resultsWriter, existingDataSetsPath));
            retrievers.add(new BRTRetriever(resultsWriter, existingDataSetsPath));
        }

        if (projectDataSet.equals(DataSet.LB)) {
            retrievers.add(new LobsterRetriever(resultsWriter, existingDataSetsPath));
        }

        return retrievers;
    }

}
