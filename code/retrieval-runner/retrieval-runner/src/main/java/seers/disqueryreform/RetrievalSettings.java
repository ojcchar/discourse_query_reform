package seers.disqueryreform;

public class RetrievalSettings {

    private static RetrievalSettings instance = new RetrievalSettings();
    private int locusThreadPoolSize = 2;

    private RetrievalSettings() {
    }

    public int getLocusThreadPoolSize() {
        return locusThreadPoolSize;
    }

    void setLocusThreadPoolSize(int threadPoolSize) {
        locusThreadPoolSize = threadPoolSize;
    }

    public static RetrievalSettings getInstance() {
        return instance;
    }
}
