package seers.disqueryreform.retrieval.bl.brt;

import edu.utdallas.seers.ir4se.index.building.IndexBuilder;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import utils.CachedIndexReader;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SourceFileIDFinder extends CachedIndexReader<String, Integer> {
    private final IndexSearcher indexSearcher;
    private final Map<Integer, Integer> scoreDocToID = new HashMap<>();

    SourceFileIDFinder(Path indexPath) throws IOException {
        super(indexPath);
        this.indexSearcher = new IndexSearcher(this.indexReader);
    }

    int idForScoreDoc(int scoreDocID) {
        return scoreDocToID.computeIfAbsent(scoreDocID, sd -> {
            try {
                return Integer.valueOf(indexReader.document(sd).getField(IndexBuilder.DOC_ID_FIELD_NAME).stringValue());
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
    }

    @Override
    protected Integer getNewValue(String docName) throws IOException {
        TermQuery query = new TermQuery(new Term("path", docName));
        TopDocs results = this.indexSearcher.search(query, 1);
        if (results.totalHits == 0) {
            throw new IllegalArgumentException(String.format("The source file with path %s does not exist in the index", docName));
        } else {
            Document document = indexReader.document(
                    results.scoreDocs[0].doc,
                    Collections.singleton(IndexBuilder.DOC_ID_FIELD_NAME)
            );

            return Integer.valueOf(document.getField(IndexBuilder.DOC_ID_FIELD_NAME).stringValue());
        }
    }
}
