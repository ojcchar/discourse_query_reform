package seers.disqueryreform.retrieval;

public enum RetrievalTechnique {
    LOCUS, LOBSTER, BUG_LOCATOR, BR_TRACER, LUCENE;

    public String getOldName() {
        return toString().replace("_", "").toLowerCase();
    }
}
