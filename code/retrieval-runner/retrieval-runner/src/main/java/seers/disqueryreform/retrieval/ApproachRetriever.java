package seers.disqueryreform.retrieval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.RetrievalUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ApproachRetriever {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApproachRetriever.class);
    private final RetrievalTechnique approach;
    private final ResultsLineWriter resultsWriter;
    private final CorpusLoader corpusLoader;

    // FIXME: load corpus docs should not exist, all approaches should be run the same way. That way it's easy to guarantee that the corpus IDs are equal on all nodes
    public ApproachRetriever(ResultsLineWriter resultsWriter, Path existingDataSetsPath, RetrievalTechnique approach, boolean loadCorpusDocs) {
        this.resultsWriter = resultsWriter;
        this.approach = approach;
        corpusLoader = new CorpusLoader(existingDataSetsPath, loadCorpusDocs);
    }

    void runQueries(Project project, List<DiscourseQuery> queries) {
        List<DiscourseQuery> pendingQueries;

        // TODO: could load only keys for a project each time. Would take longer but would save memory
        Set<QueryResultKey> alreadyExecutedKeys = resultsWriter.getAlreadyExecutedKeys();

        if (!alreadyExecutedKeys.isEmpty()) {
            pendingQueries = queries.stream()
                    .filter(q -> !alreadyExecutedKeys.contains(new QueryResultKey(project, q, approach)))
                    .collect(Collectors.toList());
        } else {
            pendingQueries = queries;
        }

        List<DiscourseQuery> validQueries = pendingQueries.stream()
                .filter(q -> {
                    boolean isVoided = q.getText() == null || q.getText().trim().isEmpty();

                    if (isVoided) {
                        LOGGER.info(String.format("Voided query: %s", q));
                    }

                    return !isVoided;
                })
                .collect(Collectors.toList());

        if (pendingQueries.isEmpty()) {
            LOGGER.info(String.format("All %s queries for project %s have already been run", approach, project));
        } else if (validQueries.isEmpty()) {
            LOGGER.info(String.format("No valid %s queries left to run for project %s", approach, project));
        } else {
            LOGGER.info(String.format("Running %s with project %s", approach, project));
            runApproach(project, validQueries);
        }
    }

    protected void runApproach(Project project, List<DiscourseQuery> queries) {
        loadCorpora(project, queries).forEach(corpus -> {
            LOGGER.info(String.format("Running %d %s queries for %s...",
                    corpus.getQueries().size(), approach, corpus));

            runForCorpus(corpus);
        });
    }

    protected void writeQueryResults(RetrievalResults retrievalResults) {
        try {
            resultsWriter.write(retrievalResults);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Project will have more than one corpus sometimes, e.g. D4J has a corpus for each query and
     * B4BL has multiple versions for each project. The queries are grouped and returned with their
     * respective corpus (lazily loaded).
     * <p>
     * NOTE: It must be ensured that {@link RetrievalUtils#readCorpus(Path, DataSet, String, DiscourseQuery)}
     * always loads documents with the same ID.
     *
     * @param project Whose corpora will be loaded.
     * @param queries Queries to be grouped.
     * @return The corpora.
     */
    protected Stream<Corpus> loadCorpora(Project project, List<DiscourseQuery> queries) {
        return corpusLoader.loadCorpora(project, queries);
    }

    protected Function<Map.Entry<String, List<DiscourseQuery>>, Corpus> corpusGrouper(Project project) {
        return corpusLoader.corpusGrouper(project);
    }

    protected abstract void runForCorpus(Corpus corpus);
}
