package seers.disqueryreform.retrieval.lobster;

import edu.uci.ics.jung.graph.Graph;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.callgraph.CallGraphRetrievalSearcher;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.ParameterException;
import edu.wayne.cs.severe.ir4se.processor.exception.SearchException;
import edu.wayne.cs.severe.ir4se.processor.utils.ExceptionUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CallGraphRetrievalSearcher2 extends CallGraphRetrievalSearcher {

    static final String STACK_TRACE_CLASSES_PROPERTY = "stackTraceClasses";
    private final Map<Integer, RetrievalDoc> indexedCorpus;
    private final ConcurrentMap<String, Map<Integer, Float>> structuralSimilarityCache =
            new ConcurrentHashMap<>();

    CallGraphRetrievalSearcher2(Directory indexDir, Map<String, String> params, List<RetrievalDoc> corpusDocuments, Graph<String, String> callGraph) throws IOException,
            SearchException, ParameterException {
        super(indexDir, params);
        org.apache.lucene.search.BooleanQuery.setMaxClauseCount(1000000);

        indexedCorpus = corpusDocuments.stream()
                .collect(Collectors.toMap(RetrievalDoc::getDocId, d -> d));
        similarityCalculator.setCallGraph(callGraph);
    }

    private Map<Integer, Float> calculateLuceneScores(Query query) throws ParseException, IOException {
        String txtQuery = query.getTxt();
        // Create a new parser every time for thread safety
        org.apache.lucene.search.Query luceneQuery = new QueryParser("text", new StandardAnalyzer()).parse(txtQuery);

        int resultsNumber = reader.numDocs();

        TopScoreDocCollector collector = TopScoreDocCollector.create(resultsNumber);
        searcher.search(luceneQuery, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        return Arrays.stream(hits)
                .collect(Collectors.toMap(
                        doc -> {
                            try {
                                return Integer.valueOf(reader.document(doc.doc).getField("docNo").stringValue());
                            } catch (IOException e) {
                                throw new UncheckedIOException(e);
                            }
                        },
                        doc -> doc.score
                ));
    }

    @SuppressWarnings("unchecked")
    @Override
    protected float computeStructuralSimilarity(Query query, String corpusDoc) {
        Set<String> stackDocs = (Set<String>) query.getInfoAttribute(STACK_TRACE_CLASSES_PROPERTY);

        if (stackDocs.isEmpty()) {
            return 0F;
        }

        return similarityCalculator.computeStructuralSimilarity(stackDocs, corpusDoc, true);
    }

    @Override
    public List<RetrievalDoc> searchQuery(Query query) throws SearchException {
        if (indexedCorpus == null) {
            throw new SearchException("Missing objects to run the search");
        }

        try {
            Map<Integer, Float> luceneScores = calculateLuceneScores(query);

            /*
            Cache structural similarity scores by key, since these never change and we will be running
            the same query many times, just with different text
            */
            Map<Integer, Float> structuralScores = structuralSimilarityCache.computeIfAbsent(
                    query.getKey(),
                    k -> computeStructuralScores(query)
            );

            // TODO: can implement utility method to enumerate
            AtomicInteger rank = new AtomicInteger(1);

            return Stream.concat(luceneScores.keySet().stream(), structuralScores.keySet().stream())
                    // All IDs of documents with any score, without repetitions
                    .collect(Collectors.toSet()).stream()
                    .map(id -> {
                        float luceneScore = luceneScores.getOrDefault(id, 0F);
                        float structuralScore = structuralScores.getOrDefault(id, 0F);

                        float totalSimilarity = computeTotalSimilarity(structuralScore, luceneScore);

                        if (totalSimilarity > 0F) {
                            RetrievalDoc corpusDoc = indexedCorpus.get(id);
                            RetrievalDoc doc = new RetrievalDoc(corpusDoc.getDocId(), null, corpusDoc.getDocName());
                            doc.setScore(totalSimilarity);
                            return doc;
                        } else {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .sorted(Comparator.comparing(RetrievalDoc::getDocScore).reversed())
                    .peek(d -> d.setDocRank(rank.getAndIncrement()))
                    .collect(Collectors.toList());

        } catch (IOException | ParseException | NullPointerException e) {
            SearchException e2 = new SearchException(e.getMessage());
            ExceptionUtils.addStackTrace(e, e2);
            throw e2;
        }
    }

    private Map<Integer, Float> computeStructuralScores(Query query) {
        return indexedCorpus.values().stream()
                .collect(Collectors.toMap(
                        RetrievalDoc::getDocId,
                        d -> computeStructuralSimilarity(query, d.getOriginalDocName().toLowerCase())
                ));
    }
}
