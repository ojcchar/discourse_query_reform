package seers.disqueryreform.results.rstats;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import seers.appcore.threads.processor.ThreadParameters;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class PairWiseStrategySummarizer extends StrategySummarizer {

    private List<ImmutablePair<QueryReformStrategy, List<QueryReformStrategy>>> pwStrategies;
    private boolean addCurrentStrategy;
    private String resultsFolderPairWise;
    private String resultsSummaryFolderPairWise;

    private Set<String> techniques;
    private List<String> thresholds;

    public PairWiseStrategySummarizer(ThreadParameters params) {
        super(params);
    }

    public PairWiseStrategySummarizer(List<ImmutablePair<QueryReformStrategy, List<QueryReformStrategy>>>
                                              pairWiseRformStrategies) {
        super(new ThreadParameters());
        this.pwStrategies = pairWiseRformStrategies;
        techniques = params.getParam(Set.class, "techniques");
        thresholds = params.getListParam(String.class, "thresholds");
        resultsFolderPairWise = params.getStringParam("resultsFolderPairWise");
        resultsSummaryFolderPairWise = params.getStringParam("resultsSummaryFolderPairWise");
    }

    @Override
    public void executeJob() throws Exception {

        for (String technique : techniques) {

            LOGGER.debug("Summarizing results for " + technique);

            List<List<String>> newLinesImprovedQueries = new LinkedList<>();
            List<List<String>> newLinesDeterioratedQueries = new LinkedList<>();
            List<List<String>> newLinesUnchangedQueries = new LinkedList<>();
            List<List<String>> newLinesNonRetrievalQueries = new LinkedList<>();
            List<List<String>> newLinesImprovDeterQueries = new LinkedList<>();
            List<List<String>> newLinesMrrMap = new LinkedList<>();
            List<List<String>> newLinesHits = new LinkedList<>();

            for (int i = 0; i < pwStrategies.size(); i++) {
                final ImmutablePair<QueryReformStrategy, List<QueryReformStrategy>>
                        pwStrategy = pwStrategies.get(i);
                final QueryReformStrategy strategy1 = pwStrategy.left;

                for (int j = 0; j < pwStrategy.right.size(); j++) {
                    final QueryReformStrategy strategy2 = pwStrategy.right.get(j);

                    ImmutablePair<QueryReformStrategy, QueryReformStrategy> pair = new ImmutablePair<>(strategy1,
                            strategy2);
                    addCurrentStrategy = false;
                    processPairWiseGranularity(strategy2, pair, technique, newLinesImprovedQueries,
                            newLinesDeterioratedQueries,
                            newLinesUnchangedQueries, newLinesNonRetrievalQueries, newLinesImprovDeterQueries,
                            newLinesMrrMap, newLinesHits, RStatsRunner.ALL_GRANULARITIES, resultsFolderPairWise);
                    addCurrentStrategy = true;
                    processPairWiseGranularity(strategy1, pair, technique, newLinesImprovedQueries,
                            newLinesDeterioratedQueries,
                            newLinesUnchangedQueries, newLinesNonRetrievalQueries, newLinesImprovDeterQueries,
                            newLinesMrrMap, newLinesHits, RStatsRunner.ALL_GRANULARITIES, resultsFolderPairWise);

                }
            }


            File outFolder = Paths.get(resultsSummaryFolderPairWise, technique).toFile();
            outFolder.mkdirs();
            File outputFile = Paths.get(outFolder.getAbsolutePath(), "results_summary.csv").toFile();
            if (outputFile.exists())
                FileUtils.forceDelete(outputFile);

            writeLines(newLinesImprovedQueries, newLinesDeterioratedQueries, newLinesUnchangedQueries,
                    newLinesNonRetrievalQueries, newLinesImprovDeterQueries, newLinesMrrMap, newLinesHits,
                    outputFile);
        }
    }


    void processPairWiseGranularity(QueryReformStrategy strategy, ImmutablePair pair, String technique,
                                    List<List<String>>
                                            newLinesImprovedQueries, List<List<String>> newLinesDeterioratedQueries,
                                    List<List<String>> newLinesUnchangedQueries, List<List<String>>
                                            newLinesNonRetrievalQueries,
                                    List<List<String>> newLinesImprovDeterQueries,
                                    List<List<String>> newLinesMrrMap,
                                    List<List<String>> newLinesHits,
                                    String granularity, String resultsFolder3) throws IOException {
        for (String threshold : thresholds) {

            if (RStatsRunner.NO_TOPK_REMOVAL.equals(threshold))
                continue;

            File resultsFile = Paths.get(resultsFolder3, granularity
                            .toLowerCase(), technique, pair.left.toString(), pair.right.toString(), threshold,
                    strategy.toString(), "analysis__" + technique + "_(0, inf].csv").toFile();

            addResults(strategy.toString(), newLinesImprovedQueries,
                    newLinesDeterioratedQueries,
                    newLinesUnchangedQueries,
                    newLinesNonRetrievalQueries, newLinesImprovDeterQueries, newLinesMrrMap, newLinesHits,
                    granularity, threshold,
                    resultsFile);

        }
    }

    @Override
    void addNewLines(String strategy, String granularity, String threshold, List<List<String>> newLines,
                     List<List<String>>
                             lines, int idx) {

        //set the header
        if (newLines.isEmpty() || !newLines.get(0).get(0).equals("preheader")) {
            if (lines != null) {
                List<String> header = lines.get(idx - 1);
                header = header.subList(3, header.size());

                header.add(0, "N");
                header.add(0, "Strategy");
                header.addAll(header);


                newLines.add(0, header);
                newLines.add(0, Collections.singletonList("preheader"));
            }
        }

        //set the content
        List<String> content;
        if (lines != null) {
            content = lines.get(idx);
            content = content.subList(3, content.size());
        } else {
            content = new ArrayList<>();
        }
        content.add(0, threshold);
        content.add(0, strategy);
        if (!addCurrentStrategy) {
            newLines.add(content);
        } else {

            if (lines != null) {
                //FIXME: this would not work for other thresholds??
                int offSet = 3;
                if ("20".equals(threshold))
                    offSet = 2;
                else if ("30".equals(threshold))
                    offSet = 1;
                List<String> lastRow = newLines.get(newLines.size() - offSet);
                lastRow.addAll(content);
            }
        }
    }
}
