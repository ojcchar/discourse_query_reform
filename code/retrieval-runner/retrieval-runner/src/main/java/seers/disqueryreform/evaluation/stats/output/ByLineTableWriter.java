package seers.disqueryreform.evaluation.stats.output;

import net.quux00.simplecsv.CsvWriter;
import seers.appcore.csv.CSVHelper;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class ByLineTableWriter<T> extends TableWriter<T> implements AutoCloseable {

    private final CsvWriter writer;
    private final List<Function<T, Object>> extractors;

    ByLineTableWriter(Path outputFilePath) {
        List<Header<T>> headers = createHeaders().flatMap(Function.identity()).collect(Collectors.toList());
        extractors = headers.stream().map(Header::getExtractor).collect(Collectors.toList());

        try {
            writer = CSVHelper.getWriter(outputFilePath.toFile(), CSVHelper.DEFAULT_SEPARATOR, false);
            writer.writeNext(headers.stream().map(Header::getLabel).map(String::valueOf).collect(Collectors.toList()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void writeRow(T row) {
        writer.writeNext(extractors.stream()
                .map(e -> e.apply(row))
                .map(String::valueOf)
                .collect(Collectors.toList())
        );
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }
}
