package seers.disqueryreform.retrieval.bench4bl.amalgam;

import edu.wayne.cs.severe.ir4se.processor.exception.EvaluationException;
import edu.wayne.cs.severe.ir4se.processor.exception.WritingException;
import seers.disqueryreform.retrieval.bench4bl.Bench4BLToolEvaluator;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AmaLgamRunner {
    public static void main(String[] args) throws InterruptedException, JAXBException, WritingException, EvaluationException, IOException {
        Path dataPath = Paths.get(args[0]);
        String outputPath = args[1];

        // THIS WILL FAIL
//        new Bench4BLToolEvaluator(new AmaLgamRetriever(),dataPath).evaluateApproach(outputPath, null);
    }
}
