package seers.disqueryreform;

import org.apache.commons.io.FileUtils;
import seers.disqueryreform.querygen.blizzard.BlizzardQueryGenerator;
import seers.disqueryreform.retrieval.DiscourseReformulationRetriever;
import seers.disqueryreform.retrieval.QueryGrouper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BlizzardReformulationRunner {
    public static void main(String[] args) throws IOException {
        new BlizzardReformulationRunner().runQueries(Paths.get(args[0]), Paths.get(args[1]), Integer.parseInt(args[2]));
    }

    private void runQueries(Path baseRepoPath, Path outputPath, int locusThreads) throws IOException {
        RetrievalSettings.getInstance().setLocusThreadPoolSize(locusThreads);

        // Lucene conflict fixed by shading Lucene version needed for BLIZZARD index building
        BlizzardQueryGenerator.createBlizzardQueryGenerator(baseRepoPath).generateQueries();

        Path tempPath = Paths.get(FileUtils.getTempDirectoryPath()).resolve("temp-blizzard-queries");
        FileUtils.deleteQuietly(tempPath.toFile());
        Path reformulatedQueriesPath = baseRepoPath.resolve(Paths.get("data", "blizzard-queries"));
        Path existingDataSetsPath = baseRepoPath.resolve(Paths.get("data", "existing_data_sets"));

        QueryGrouper queryGrouper = QueryGrouper.newQueryGrouper(reformulatedQueriesPath, 1, 1, tempPath);

        new DiscourseReformulationRetriever(outputPath, existingDataSetsPath)
                .runQueries(queryGrouper);
    }
}
