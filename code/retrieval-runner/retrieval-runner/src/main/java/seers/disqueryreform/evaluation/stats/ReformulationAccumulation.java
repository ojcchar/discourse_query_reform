package seers.disqueryreform.evaluation.stats;


import edu.utdallas.seers.ir4se.evaluation.RetrievalAccumulation;
import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;

public class ReformulationAccumulation extends ReformulationStats<RetrievalAccumulation, RetrievalAccumulation.AccumulatedMetric> {
    private ReformulationAccumulation(RetrievalAccumulation baselineAggregation, RetrievalAccumulation reformulationAggregation) {
        super(baselineAggregation, reformulationAggregation);
    }

    ReformulationAccumulation(RetrievalEvaluation baselineMetrics, RetrievalEvaluation reformMetrics) {
        super(baselineMetrics.toAccumulation(), reformMetrics.toAccumulation());
    }

    ReformulationSummary toSummary() {
        return new ReformulationSummary(baselineAggregation, reformulationAggregation);
    }

    void accumulate(ReformulationEvaluation evaluation) {
        baselineAggregation.accumulate(evaluation.getBaselineMetrics());
        reformulationAggregation.accumulate(evaluation.getReformMetrics());
    }

    @Override
    @SuppressWarnings("unchecked")
    <T extends ReformulationStats> T aggregate(T other) {
        return (T) new ReformulationAccumulation(
                (RetrievalAccumulation) baselineAggregation.aggregate(other.baselineAggregation),
                (RetrievalAccumulation) reformulationAggregation.aggregate(other.reformulationAggregation)
        );
    }

    @Override
    public double getRelativeImprovement(RetrievalAccumulation.AccumulatedMetric metric) {
        double improvement = getImprovement(metric);

        if (improvement == 0) {
            // So that it doesn't output NaN if baseline result is 0
            return 0;
        }

        /*
        Allowing NaN if improvement is non-zero because relative improvement when baseline is 0
        is undefined
        */
        return improvement / getBaselineMetric(metric);
    }

    @Override
    public double getImprovement(RetrievalAccumulation.AccumulatedMetric metric) {
        return getReformulationMetric(metric) - getBaselineMetric(metric);
    }

}
