package seers.disqueryreform.retrieval.lobster;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import seers.disqueryreform.base.D4JUtils;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Deprecated
public class ResultsSummarizerMain {

	private static final String operator = "or";
	private static String baselineStrategy = "ALL_TEXT";

	// buglocator data - brtracer results
	// static String statsFolder =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/brtracer/bl-brt-"
	// + operator;
	// static String outFile =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/brtracer/overall-stats-"
	// + operator + ".csv";
	// static String prefix = "BRTracer-";
	// static String suffix = "";

	// buglocator data - buglocator results
//	static String statsFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/buglocator/bl-bl-"
//			+ operator;
//	static String outFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/buglocator/overall-stats-"
//			+ operator + ".csv";
//	static String prefix = "BugLocator-";
//	static String suffix = "";

	// buglocator data - lucene results
	 static String statsFolder =
	 "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/lucene/bl-lucene-"
	 + operator;
	 static String outFile =
	 "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/lucene/overall-stats-"
	 + operator + ".csv";
	 static String prefix = "LUCENE-LUCENE-";
	 static String suffix = "";

	// lobster data - lobster results
	// static String statsFolder =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/lobster-all-hard-to-retrieve/lobster/lobster-lobster-"
	// + operator;
	// static String outFile =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/lobster-all-hard-to-retrieve/lobster/overall-stats-"
	// + operator + ".csv";
	// static String prefix = "CG-CG-";
	// static String suffix = "-0.9-3-C-D";

	// lobster data - lucene results
	// static String statsFolder =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/lobster-all-hard-to-retrieve/lucene/lobster-lucene-"
	// + operator;
	// static String outFile =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/lobster-all-hard-to-retrieve/lucene/overall-stats-"
	// + operator + ".csv";
	// static String prefix = "LUCENE-LUCENE-";
	// static String suffix = "";

	static HashMap<String, Double> baselineRanks = new HashMap<>();

	public static void main(String[] args) throws Exception {

		File stFold = new File(statsFolder);
		File outputFile = new File(outFile);

		if (outputFile.exists()) {
			FileUtils.forceDelete(outputFile);
		}

		StringBuffer header = new StringBuffer("system;query_id;bug_id;strategy;rank;rank_diff;avg_prec");
		header.append("\n");

		FileUtils.write(outputFile, header, true);

		List<String> values = Arrays.asList(QueryReformStrategy.values()).stream().map(str -> str.toString())
				.collect(Collectors.toList());

		if (!values.get(0).equals(baselineStrategy)) {
			values.add(0, baselineStrategy);
		}

		boolean baseline = true;

		for (String strategy : values) {

			LinkedList<File> files = getFiles(stFold, strategy);

			System.out.println("strat: " + strategy);
			System.out.println("# files: " + files.size());

			for (File file : files) {
				String name = file.getName();
				int endIndex = name.indexOf("_");
				String system = name.substring(0, endIndex).replace(prefix, "");

				List<List<String>> readLines = D4JUtils.readLines(file, ';');
				List<List<String>> subList = readLines.subList(2, readLines.size());

				System.out.println("# queries for " + system + ": " + subList.size());

				for (List<String> list : subList) {
					try {
						// -----------------------------
						StringBuffer data2 = new StringBuffer();
						data2.append(system);
						data2.append(";");
						data2.append(list.get(0));
						data2.append(";");
						data2.append(list.get(1));
						data2.append(";");
						data2.append(strategy);
						data2.append(";");
						data2.append(list.get(2));
						data2.append(";");

						Double rankThisStrategy = Double.valueOf(list.get(2));
						if (baseline) {
							baselineRanks.put(system + list.get(1), rankThisStrategy);
							data2.append("0");
						} else {
							Double rankBaseline = baselineRanks.get(system + list.get(1));
							data2.append(rankBaseline == 0 || rankThisStrategy == 0 ? ""
									: (rankBaseline - rankThisStrategy));
						}
						data2.append(";");
						data2.append(list.get(9));
						data2.append("\n");

						FileUtils.write(outputFile, data2, true);
					} catch (Exception e) {
						System.err.println("error for file: " + file);
						System.err.println("error for entry: " + list);
						e.printStackTrace();
					}
				}

			}

			baseline = false;

		}
	}

	private static LinkedList<File> getFiles(File stFold, String strategy) {
		LinkedList<File> baseLineFiles = (LinkedList<File>) FileUtils.listFiles(stFold, new IOFileFilter() {

			@Override
			public boolean accept(File dir, String name) {
				int endIndex = name.indexOf("_");
				String system = name.substring(0, endIndex).replace(prefix, "");
				return name.endsWith(system + "_" + strategy + suffix + "-stats.csv");
			}

			@Override
			public boolean accept(File file) {
				return accept(file.getParentFile(), file.getName());
			}
		}, null);
		return baseLineFiles;
	}

}
