package seers.disqueryreform.results.rstats;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PairWiseCombinationsProcessor extends CombinationsProcessor {

    private String tempFolder2;
    private String resultsFolderPairWise;

    public PairWiseCombinationsProcessor(ThreadParameters params) {
        super(params);
        combinations = params.getListParam(List.class, ThreadExecutor.ELEMENTS_PARAM);
        tempFolder2 = params.getStringParam("tempFolder2");
        resultsFolderPairWise = params.getStringParam("resultsFolderPairWise");
    }

    @Override
    void processCombination(List<String> combination, ArrayList<List<String>> localBugs,
                            ArrayList<List<String>> localStats) throws Exception {

        List<String> subCombination1 = new LinkedList<>(combination);
        subCombination1.remove(3);

        CombinationData combinationData1 = new CombinationData(subCombination1, localBugs, localStats).readData();
        if (combinationData1.isNoData()) return;
        List<List<String>> combinationBugs1 = combinationData1.getCombinationBugs();
        List<List<String>> combinationStats1 = combinationData1.getCombinationStats();

        //-----------

        List<String> subCombination2 = new LinkedList<>(combination);
        subCombination2.remove(2);

        CombinationData combinationData2 = new CombinationData(subCombination2, localBugs, localStats).readData();
        if (combinationData2.isNoData()) return;
        List<List<String>> combinationBugs2 = combinationData2.getCombinationBugs();
        List<List<String>> combinationStats2 = combinationData2.getCombinationStats();

        //-----------

        List<List<String>> commonBugs = new LinkedList<>(combinationBugs1);
        commonBugs.retainAll(combinationBugs2);

        String approach = combination.get(1);
        String rankThreshold = combination.get(3);
        runAnalysis(combination, commonBugs, combinationStats1, tempFolder2, resultsFolderPairWise, combination
                .get(2), approach, combination.get(2), rankThreshold);
        runAnalysis(combination, commonBugs, combinationStats2, tempFolder2, resultsFolderPairWise, combination
                .get(3), approach, combination.get(3), rankThreshold);

    }

}
