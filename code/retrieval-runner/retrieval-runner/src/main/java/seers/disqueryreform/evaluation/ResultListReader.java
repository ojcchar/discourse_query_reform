package seers.disqueryreform.evaluation;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.DiscourseTaskStrategy;
import seers.disqueryreform.retrieval.QueryResultKey;
import seers.disqueryreform.retrieval.ResultsLineWriter;
import seers.disqueryreform.retrieval.RetrievalResults;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ResultListReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultListReader.class);

    private static final String BASELINE_RESULTS_FILE_NAME = "baseline-results.json";

    private final Predicate<RetrievalResults> isBaselineResult =
            r -> r.getKey().getStrategy().equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME);

    private final Path resultListsPath;

    ResultListReader(Path resultListsPath) {
        this.resultListsPath = resultListsPath;
    }

    Map<String, RetrievalResults> loadBaselineResultLists() throws IOException {
        Path baselineResultsPath = resultListsPath.resolve(BASELINE_RESULTS_FILE_NAME);
        Gson gson = RetrievalResults.createGson();

        if (Files.exists(baselineResultsPath)) {
            LOGGER.info(String.format("Baseline results are already found in %s. Loading...",
                    baselineResultsPath));

            Type typeOfMap = new TypeToken<Map<String, RetrievalResults>>() {
            }.getType();

            return gson.fromJson(
                    FileUtils.readFileToString(baselineResultsPath.toFile(), "utf-8"),
                    typeOfMap
            );
        } else {
            LOGGER.info("Baseline results not found, reading all results...");

            Map<String, RetrievalResults> baselineResults = ResultsLineWriter.allResultsLines(resultListsPath)
                    .filter(isBaselineResult)
                    .collect(Collectors.toMap(
                            r -> createBaselineKey(r.getKey()),
                            r -> r
                    ));

            LOGGER.info("Writing all baseline results to " + baselineResultsPath);

            FileUtils.write(baselineResultsPath.toFile(), gson.toJson(baselineResults), "utf-8");

            return baselineResults;
        }
    }

    Stream<RetrievalResults> allResults() {
        return ResultsLineWriter.allResultsLines(resultListsPath);
    }

    String createBaselineKey(QueryResultKey resultKey) {
        return resultKey.getTechnique() + ";" + resultKey.getProject() + ";" + resultKey.getQueryKey();
    }
}
