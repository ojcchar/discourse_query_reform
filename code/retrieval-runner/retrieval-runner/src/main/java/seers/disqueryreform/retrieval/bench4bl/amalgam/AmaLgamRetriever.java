package seers.disqueryreform.retrieval.bench4bl.amalgam;

import seers.disqueryreform.retrieval.bench4bl.Bench4BLRetriever;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AmaLgamRetriever extends Bench4BLRetriever {
    private static final Path AMALGAM_BIN_PATH =
            Paths.get("..", "..", "other-tools", "AmaLgam.jar");

    public AmaLgamRetriever() {
    }

    @Override
    protected String[] buildCommand(String alias, String repoDir, String sourceDir, String xmlBugReportsPath, Path workingDirPath) throws IOException {
        // TODO: move this execution to a method that executes commands before buildCommand is executed in superclass
        // FIXME: we need datapath and workingdirectorypath here in order to run BLUiR
//        try {
//            new Bench4BLToolEvaluator(dataPath, workingDirectoryPath).runApproach();
//        } catch (InterruptedException | JAXBException | EvaluationException | IOException e) {
//            throw new RuntimeException("BLUiR execution failed");
//        }

        return new String[]{String.format("java -Xms512m -Xmx4000m -jar %s -a 0.2 -b %s -g %s -n %s -s %s -w %s",
                AMALGAM_BIN_PATH.toString(),
                xmlBugReportsPath,
                repoDir,
                String.join("_", alias, alias).replace('.', '_'),
                sourceDir,
                workingDirPath)};
    }

    @Override
    protected String getToolName() {
        return "AmaLgam";
    }
}
