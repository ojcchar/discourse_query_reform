package seers.disqueryreform.results.rstats;

import net.quux00.simplecsv.CsvWriter;
import org.apache.commons.io.FileUtils;
import seers.appcore.csv.CSVHelper;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;

public class StrategySummarizer extends ThreadProcessor {

    private final List<String> strategies;
    private Set<String> techniques;
    private DocumentGranularity[] granularities;
    private List<String> thresholds;
    private String resultsFolder;
    private String resultsSummaryFolder;

    public StrategySummarizer(ThreadParameters params) {
        super(params);
        strategies = params.getListParam(String.class, ThreadExecutor.ELEMENTS_PARAM);
        techniques = params.getParam(Set.class, "techniques");
        granularities = params.getParam(DocumentGranularity[].class, "granularities");
        thresholds = params.getListParam(String.class, "thresholds");
        resultsFolder = params.getStringParam("resultsFolder");
        resultsSummaryFolder = params.getStringParam("resultsSummaryFolder");
    }

    @Override
    public void executeJob() throws Exception {
        for (String strategy : strategies) {
            processStrategy(strategy);
        }
    }

    private void processStrategy(String strategy) throws IOException {

        LOGGER.debug("Summarizing results for " + strategy);


        for (String technique : techniques) {

            List<List<String>> newLinesImprovedQueries = new LinkedList<>();
            List<List<String>> newLinesDeterioratedQueries = new LinkedList<>();
            List<List<String>> newLinesUnchangedQueries = new LinkedList<>();
            List<List<String>> newLinesNonRetrievalQueries = new LinkedList<>();
            List<List<String>> newLinesImprovDeterQueries = new LinkedList<>();
            List<List<String>> newLinesMrrMap = new LinkedList<>();
            List<List<String>> newLinesHits = new LinkedList<>();

            for (DocumentGranularity granularity : granularities) {
                processGranularity(strategy, technique, newLinesImprovedQueries, newLinesDeterioratedQueries,
                        newLinesUnchangedQueries, newLinesNonRetrievalQueries, newLinesImprovDeterQueries,
                        newLinesMrrMap, newLinesHits, granularity.toString(), resultsFolder);
            }

            processGranularity(strategy, technique, newLinesImprovedQueries, newLinesDeterioratedQueries,
                    newLinesUnchangedQueries, newLinesNonRetrievalQueries, newLinesImprovDeterQueries,
                    newLinesMrrMap, newLinesHits, RStatsRunner.ALL_GRANULARITIES, resultsFolder);

            File outFolder = Paths.get(resultsSummaryFolder, technique).toFile();
            outFolder.mkdirs();
            File outputFile =
                    Paths.get(outFolder.getAbsolutePath(), "results_summary#" + strategy + ".csv").toFile();
            if (outputFile.exists())
                FileUtils.forceDelete(outputFile);

            writeLines(newLinesImprovedQueries, newLinesDeterioratedQueries, newLinesUnchangedQueries,
                    newLinesNonRetrievalQueries, newLinesImprovDeterQueries, newLinesMrrMap, newLinesHits,
                    outputFile);

        }
    }

    void writeLines(List<List<String>> newLinesImprovedQueries, List<List<String>>
            newLinesDeterioratedQueries, List<List<String>> newLinesUnchangedQueries, List<List<String>>
                            newLinesNonRetrievalQueries, List<List<String>> newLinesImprovDeterQueries,
                    List<List<String>> newLinesMrrMap, List<List<String>> newLinesHits, File outputFile) throws IOException {
        try (final CsvWriter writer = CSVHelper.getWriter(outputFile, CSVHelper.DEFAULT_SEPARATOR, true);) {

            writeLines("HITS stats", newLinesHits, writer);
            writeLines("MRR/MAP", newLinesMrrMap, writer);
            writeLines("# of queries and improv/deter size", newLinesImprovDeterQueries, writer);
            writeLines("# of queries improved", newLinesImprovedQueries, writer);
            writeLines("# of queries deteriorated", newLinesDeterioratedQueries, writer);
            writeLines("# of queries unchanged", newLinesUnchangedQueries, writer);
            writeLines("# of queries with non-retrieved", newLinesNonRetrievalQueries, writer);
        }
    }

    private void processGranularity(String strategy, String technique, List<List<String>>
            newLinesImprovedQueries, List<List<String>> newLinesDeterioratedQueries,
                                    List<List<String>> newLinesUnchangedQueries, List<List<String>>
                                            newLinesNonRetrievalQueries,
                                    List<List<String>> newLinesImprovDeterQueries, List<List<String>> newLinesMrrMap,
                                    List<List<String>> newLinesHits, String granularity, String resultsFolder3) throws IOException {
        for (String threshold : thresholds) {

            if (RStatsRunner.NO_TOPK_REMOVAL.equals(threshold))
                continue;

            File resultsFile = Paths.get(resultsFolder3, granularity
                    .toLowerCase(), technique, strategy, threshold, "analysis__" + technique +
                    "_(0, inf].csv").toFile();

            addResults(strategy, newLinesImprovedQueries, newLinesDeterioratedQueries,
                    newLinesUnchangedQueries,
                    newLinesNonRetrievalQueries, newLinesImprovDeterQueries, newLinesMrrMap, newLinesHits,
                    granularity, threshold,
                    resultsFile);

        }
    }

    void addResults(String strategy, List<List<String>> newLinesImprovedQueries, List<List<String>>
            newLinesDeterioratedQueries, List<List<String>> newLinesUnchangedQueries, List<List<String>>
                            newLinesNonRetrievalQueries, List<List<String>> newLinesImprovDeterQueries,
                    List<List<String>> newLinesMrrMap, List<List<String>> newLinesHits,
                    String granularity, String threshold, File resultsFile)
            throws IOException {
        try {
            //----------------------------------

            List<List<String>> lines = null;
            if (resultsFile.exists())
                lines = CSVHelper.readCsv(resultsFile, false, Function.identity(),
                        CSVHelper.DEFAULT_SEPARATOR, "UTF-8");

            //----------------------------------

            final int IMPROVED_QUERIES_IDX = 29;
            final int DETER_QUERIES_IDX = 54;
            final int UNCHANGED_QUERIES_IDX = 19;
            final int NON_RETRIEVAL_QUERIES_IDX = 24;
            final int IMPROVED_DETER_QUERIES_IDX = 14;
            final int MRR_MAP_IDX = 10;
            final int HITS_IDX = 78;

            addNewLines(strategy, granularity, threshold, newLinesImprovedQueries, lines, IMPROVED_QUERIES_IDX);
            addNewLines(strategy, granularity, threshold, newLinesDeterioratedQueries, lines, DETER_QUERIES_IDX);
            addNewLines(strategy, granularity, threshold, newLinesUnchangedQueries, lines, UNCHANGED_QUERIES_IDX);
            addNewLines(strategy, granularity, threshold, newLinesNonRetrievalQueries, lines,
                    NON_RETRIEVAL_QUERIES_IDX);
            addNewLines(strategy, granularity, threshold, newLinesImprovDeterQueries, lines,
                    IMPROVED_DETER_QUERIES_IDX);
            addNewLines(strategy, granularity, threshold, newLinesMrrMap, lines, MRR_MAP_IDX);
            addNewLines(strategy, granularity, threshold, newLinesHits, lines, HITS_IDX);


        } catch (Exception e) {
            LOGGER.error("Error processing " + resultsFile, e);
            throw e;
        }
    }

    void writeLines(String title, List<List<String>> lines, CsvWriter writer) {
        if (lines.get(0).get(0).equals("preheader")) {
            lines.remove(0);
        }
        lines.add(0, Collections.singletonList(title));
        lines.add(new ArrayList<>());
        CSVHelper.writeCsv(writer, null, lines, null, Function.identity());
    }

    void addNewLines(String strategy, String granularity, String threshold, List<List<String>> newLines,
                     List<List<String>>
                             lines, int idx) {

        //set the header
        if (newLines.isEmpty() || !newLines.get(0).get(0).equals("preheader")) {
            if (lines != null) {
                List<String> header = lines.get(idx - 1);
                header = header.subList(3, header.size());
                header.add(0, "N");
                header.add(0, "Data set");

                newLines.add(0, header);
                newLines.add(0, Collections.singletonList("preheader"));
            }
        }

        //set the content
        List<String> content;
        if (lines != null) {
            content = lines.get(idx);
            content = content.subList(3, content.size());
        } else {
            content = new ArrayList<>();
        }
        content.add(0, threshold);
        content.add(0, granularity);
        newLines.add(content);
    }
}
