package seers.disqueryreform.results;

import seers.appcore.csv.CSVHelper;
import seers.disqueryreform.base.DataSet;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FixSystemInResultsFileB4BL {

    public static void main(String[] args) throws IOException {
        Path resultsDir;
        if (args.length != 1) {
            resultsDir = Paths.get("..", "..", "..", "results","2_analysis-format");
        } else {
            resultsDir = Paths.get(args[0]);
        }

        String bugListFile = resultsDir.resolve("bug-list.csv").toString();
        String statsFile = resultsDir.resolve("overall-stats.csv").toString();
        List<List<String>> lines1 = CSVHelper.readCsv(bugListFile, false);
        List<List<String>> lines2 = CSVHelper.readCsv(statsFile, false);

        final List<List<String>> newList = fixSystem(lines1, 5);
        CSVHelper.writeCsv(bugListFile + ".new", null, newList, null, Function.identity(), CSVHelper.DEFAULT_SEPARATOR);

        final List<List<String>> newList2 = fixSystem(lines2, 6);
        CSVHelper.writeCsv(statsFile + ".new", null, newList2, null, Function.identity(), CSVHelper.DEFAULT_SEPARATOR);

    }

    private static List<List<String>> fixSystem(List<List<String>> lines1, int bugIdIndex) {
        return lines1.stream().map(line -> {

            final String dataSet = line.get(0);
            final String technique = line.get(1);
            if (dataSet.equals(DataSet.B4BL.toString().toLowerCase()) && "locus".equals(technique)) {
                String sysName = line.get(4);
                final int i = sysName.indexOf("_");
                if (i != -1) {
                    sysName = sysName.substring(0, i);
                    line.set(4, sysName);
                }

                //------------------------------
                final String bugId = line.get(bugIdIndex);
                final int j = sysName.indexOf("-");
                if (j == -1) {
                    line.set(bugIdIndex, sysName + "-" + bugId);
                }
            }
            if (dataSet.equals(DataSet.BRT.toString().toLowerCase()) && "locus".equals(technique)) {
                String sysName = line.get(4);
                if ((sysName.startsWith("pde") || sysName.startsWith("jdt")) && sysName.endsWith("-3.1")) {
                    line.set(4, "eclipse-3.1");
                }
            }

            //------------------------------

            return line;
        }).collect(Collectors.toList());
    }
}
