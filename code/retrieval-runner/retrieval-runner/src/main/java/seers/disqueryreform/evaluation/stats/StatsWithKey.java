package seers.disqueryreform.evaluation.stats;

public class StatsWithKey<K, S extends ReformulationStats> {
    protected final K key;
    protected final S stats;

    public StatsWithKey(K key, S stats) {
        this.key = key;
        this.stats = stats;
    }

    public K getKey() {
        return key;
    }

    public S getStats() {
        return stats;
    }
}
