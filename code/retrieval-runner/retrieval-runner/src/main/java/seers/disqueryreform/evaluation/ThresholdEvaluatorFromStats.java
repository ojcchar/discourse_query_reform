package seers.disqueryreform.evaluation;

import edu.utdallas.seers.ir4se.evaluation.RetrievalEvaluation;
import net.quux00.simplecsv.CsvParser;
import net.quux00.simplecsv.CsvParserBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.ComponentsForBugTable;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.StrategyType;
import seers.disqueryreform.evaluation.stats.ReformulationEvaluation;
import seers.disqueryreform.querygen.tasknav.DiscourseTasksQueryGenerator;
import seers.disqueryreform.retrieval.QueryResultKey;
import seers.disqueryreform.retrieval.RetrievalTechnique;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Evaluates thresholds from a previously evaluated stats file output by {@link ThresholdEvaluator}.
 * Useful for fixing problems or recomputing stats without having to use all retrieval results
 * (where possible).
 */
public class ThresholdEvaluatorFromStats extends ThresholdEvaluator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThresholdEvaluatorFromStats.class);

    private final Path baseRepositoryPath;
    private final Path statsFilePath;

    private final CsvParser csvParser = new CsvParserBuilder().multiLine(true).separator(';').build();

    public ThresholdEvaluatorFromStats(Path baseRepositoryPath, Path statsFilePath, StrategyType strategyType) {
        super(null, null, null, null, strategyType);
        this.baseRepositoryPath = baseRepositoryPath;
        this.statsFilePath = statsFilePath;
    }

    private Optional<ReformulationEvaluation> processLine(String line, ComponentsForBugTable table) {
        List<String> elements = csvParser.parse(line);
        QueryResultKey resultKey = extractQueryResultKey(elements);

        Optional<Set<String>> maybeStrategies = findStrategiesForResult(resultKey, table);

        if (!maybeStrategies.isPresent() || maybeStrategies.get().isEmpty()) {
            return Optional.empty();
        } else {
            int threshold = Integer.parseInt(elements.get(6));
            int br = Double.valueOf(elements.get(7)).intValue();
            int rr = Double.valueOf(elements.get(8)).intValue();
            double bap = Double.parseDouble(elements.get(10));
            double rap = Double.parseDouble(elements.get(11));

            RetrievalEvaluation bm = new RetrievalEvaluation(threshold, br, bap);
            RetrievalEvaluation rm = new RetrievalEvaluation(threshold, rr, rap);

            ReformulationEvaluation eval = new ReformulationEvaluation(resultKey, bm, rm, maybeStrategies.get());

            return Optional.of(eval);
        }
    }

    private QueryResultKey extractQueryResultKey(List<String> elements) {
        RetrievalTechnique technique = RetrievalTechnique.valueOf(elements.get(0));
        DataSet dataSet = DataSet.valueOf(elements.get(1));
        String projectName = elements.get(2);
        String queryKey = elements.get(3);
        String strategy = elements.get(4);

        Project project = new Project(dataSet, projectName);

        String[] comps = strategy.split("--");
        String lastComp = comps[comps.length - 1];

        String taskNavConfiguration;
        String bareStrategy;
        if (lastComp.startsWith("true") || lastComp.startsWith("false")) {
            taskNavConfiguration = lastComp;
            bareStrategy = String.join("--", Arrays.copyOfRange(comps, 0, comps.length - 1));
        } else {
            taskNavConfiguration = null;
            bareStrategy = String.join("--", comps);
        }

        return new QueryResultKey(project, queryKey, bareStrategy, taskNavConfiguration, technique);
    }

    @Override
    public Stream<ReformulationEvaluation> thresholdEvaluations() {
        try {
            ComponentsForBugTable table = new DiscourseTasksQueryGenerator(baseRepositoryPath)
                    .readComponentsForBugTable();

            AtomicInteger counter = new AtomicInteger(0);
            return Files.lines(statsFilePath)
                    // Skip header
                    .skip(1)
                    .map(l -> {
                        int count = counter.incrementAndGet();
                        if (count % 100_000 == 0) {
                            LOGGER.info(count + " loaded");
                        }

                        return processLine(l, table);
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
