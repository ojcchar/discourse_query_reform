package seers.disqueryreform.results;

import org.apache.commons.io.FileUtils;
import seers.disqueryreform.base.DataSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class NumReformulatedQueriesStats {

    private static DataSet[] dataSets = DataSet.values();
    private static File b5BugsFile = new File("C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform" +
            "\\data\\all_bug_coding\\all_coded_non_bugs_top-5.csv");
    private static String reformQueriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation" +
            "/reformulated_queries";

    public static void main(String[] args) throws Exception {

        Map<String, Integer> allB5Bugs = readB5Bugs();

        //-----------------------

        for (DataSet dataSet : dataSets) {

            File queriesFolder = getQueriesFolder(dataSet);

            final File[] strategyFolders = queriesFolder.listFiles(File::isDirectory);
            for (File strategyFolder : strategyFolders) {

                final String strategy = strategyFolder.getName();
                final File[] files = strategyFolder.listFiles(f -> f.getName().endsWith(".json"));

                for (File file : files) {

                    final String system = file.getName().replace(".json", "");

                    try (BufferedReader r = new BufferedReader(new FileReader(file))) {
                        final List<String> bugIds = r.lines()
                                .map(l -> getBugId(l))
                                .collect(Collectors.toList());
                        for (String bugId : bugIds) {

                            String isB5 = getIsBelowRank5(allB5Bugs, dataSet, system, bugId);
                            final List<String> list =
                                    Arrays.asList(dataSet.toString().toLowerCase(), strategy, system, bugId, isB5);
                            System.out.println(String.join(";", list));
                        }
                    }

                }

            }
        }
    }

    private static Map<String, Integer> readB5Bugs() throws IOException {
        final List<String> lines = FileUtils.readLines(b5BugsFile, Charset.defaultCharset());
        return lines.stream().collect(Collectors.toMap(Function.identity(), l -> 1));
    }

    private static String getIsBelowRank5(Map<String, Integer> allB5Bugs, DataSet dataSet, String system,
                                          String bugId) {
        return allB5Bugs.containsKey(String.join(";",
                Arrays.asList(dataSet.toString().toLowerCase(), system, bugId))) ? "1" : "";
    }

    static String regex = ".+\"bug_id\":\"(.+)\",\"text\".+";
    static Pattern pattern = Pattern.compile(regex);

    public static String getBugId(String line) {
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches())
            return matcher.group(1);
        throw new RuntimeException("Couldn't get the bug id: " + line);
    }



    private static File getQueriesFolder(DataSet dataSet) {
        File queriesFolder = null;
        switch (dataSet) {
            case BRT:
                queriesFolder = Paths.get(reformQueriesFolder,
                        "buglocator-generated-sample-hard-to-retrieve_and", "TITLE_DESCRIPTION").toFile();
                break;
            case D4J:
                queriesFolder = Paths.get(reformQueriesFolder,
                        "d4j-generated-all-hard-to-retrieve-queries_and", "TITLE_DESCRIPTION").toFile();
                break;
            case LB:
                queriesFolder = Paths.get(reformQueriesFolder,
                        "lobster-generated-all-hard-to-retrieve_and", "TITLE_DESCRIPTION").toFile();
                break;
            case QQ:
                queriesFolder = Paths.get(reformQueriesFolder,
                        "query_quality-generated-all-hard-to-retrieve-queries_and", "TITLE_DESCRIPTION").toFile();
                break;
            case B4BL:
                queriesFolder = Paths.get(reformQueriesFolder, "b4bl_and_generated", "TITLE_DESCRIPTION").toFile();
                break;
        }
        return queriesFolder;
    }
}
