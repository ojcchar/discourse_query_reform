package seers.disqueryreform.base;

public enum StrategyType {
    /**
     * Strategy should only apply if query contains ALL components.
     */
    CONJUNCTIVE("+"),

    /**
     * Strategy applies if the query contains ANY of the components. The biggest subset of
     * components is used as the query text.
     */
    COMBINATORIAL("*");

    private final String componentJoiner;

    StrategyType(String componentJoiner) {
        this.componentJoiner = componentJoiner;
    }

    public String getComponentJoiner() {
        return componentJoiner;
    }
}
