package seers.disqueryreform.base;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class GeneralUtils {

    public static String getRankThresholds(int limit) {
        return IntStream.range(1, limit + 1)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(","));
    }

    public static List<String> getRankThresholdList(int limit) {
        return IntStream.range(1, limit + 1)
                .mapToObj(String::valueOf)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unused")
    public static <T> Stream<List<T>> partitionList(List<T> list, int partitionSize) {
        if (partitionSize <= 0) {
            throw new IllegalArgumentException("Partition size must be >= 0");
        }

        int partitionAmount = (int) Math.ceil((double) list.size() / partitionSize);

        return IntStream.range(0, partitionAmount)
                .boxed()
                // Get chunks of the size 'partitionSize'
                .map(i -> {
                    int startIndex = i * partitionSize;
                    return list.subList(startIndex, Math.min(startIndex + partitionSize, list.size()));
                });
    }
}
