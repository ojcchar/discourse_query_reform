package seers.disqueryreform.base;

import com.google.common.collect.Sets;
import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stores which bug report contains which components.
 */
public class ComponentsForBugTable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComponentsForBugTable.class);
    private static final String NO_CONFIG_TOKEN = "NONE";
    private static final String COMP_CONFIG_SEP = "--";

    private final Map<String, Set<String>> table = new HashMap<>();
    private final Set<String> allComponents = new HashSet<>();
    private final Map<String, Integer> applicability = new HashMap<>();
    private final int totalQueries;
    private transient Set<Set<String>> allCombinations;

    public ComponentsForBugTable(List<StrategyQueryContainer> containers, int totalQueries) {
        this.totalQueries = totalQueries;

        // FIXME this is all made ugly because tasknav configuration is not part of the strategy as it should be
        for (StrategyQueryContainer container : containers) {
            if (container.getStrategyName().equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME) ||
                    DiscourseTaskStrategy.splitIntoComponentNames(container.getStrategyName()).size() != 1) {
                /*
                Using only the containers for strategies with a single component because it could
                happen that a bug has A and B but A is lost in preprocessing, so if we look in
                A+B, we would mistakenly assign it to having A
                */
                continue;
            }

            for (Map.Entry<Project, List<DiscourseQuery>> entry : container.getItems()) {
                Project project = entry.getKey();

                for (DiscourseQuery query : entry.getValue()) {
                    /*
                    If a query ended up with no text after preprocessing, it doesn't count for
                    this strategy
                    */
                    if (query.getText() == null || query.getText().trim().isEmpty()) {
                        LOGGER.info(String.format("Query %s from %s has no text",
                                query, project));
                        continue;
                    }

                    String tableKey = createTableKey(project, query.getKey());

                    Set<String> componentSet = extractComponentSet(
                            query.getStrategy(),
                            query.getTaskNavConfiguration()
                    );

                    table.computeIfAbsent(tableKey, k -> new HashSet<>())
                            .addAll(componentSet);

                    allComponents.addAll(componentSet);

                    for (String component : componentSet) {
                        applicability.put(component,
                                applicability.getOrDefault(component, 0) + 1);
                    }
                }
            }
        }
    }

    static Set<Set<String>> generateAllComponentCombinations(Set<String> allComponents) {
        Map<String, List<String>> augmentedComponents = allComponents.stream()
                .collect(Collectors.toMap(
                        s -> {
                            String[] split = s.split("--");
                            return split.length > 1 ? split[1] : "NONE";
                        },
                        s -> Collections.singletonList(s.split("--")[0]),
                        (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList())
                ));

        return augmentedComponents.entrySet().stream()
                .flatMap(e -> {
                    Set<String> allComp = Stream.of(e.getValue(), augmentedComponents.getOrDefault("NONE", Collections.emptyList()))
                            .flatMap(Collection::stream).collect(Collectors.toSet());

                    List<List<String>> combs = DiscourseTaskStrategy.generateAllStringCombinations(allComp).stream()
                            .filter(s -> !s.isEmpty())
                            .collect(Collectors.toList());

                    return combs.stream().map(c -> {
                        List<BugReportComponent> components = c.stream().map(BugReportComponent::valueOf).collect(Collectors.toList());

                        return components.stream()
                                .map(cc -> BugReportComponent.isTaskComponent(cc) ? cc + "--" + e.getKey() : cc + "--NONE")
                                .collect(Collectors.toSet());
                    });
                })
                .collect(Collectors.toSet());
    }

    private Set<String> extractComponentSet(String strategyName, String taskNavConfiguration) {
        String config;

        if (taskNavConfiguration == null) {
            config = NO_CONFIG_TOKEN;
        } else {
            config = taskNavConfiguration;
        }

        List<String> componentNames = DiscourseTaskStrategy.splitIntoComponentNames(strategyName);

        return componentNames.stream()
                .map(cn -> {
                    /* We must augment it with the config where the
                    tasks came from or a token if it is not a task component*/
                    if (BugReportComponent.isTaskComponent(BugReportComponent.valueOf(cn))) {
                        return cn + COMP_CONFIG_SEP + config;
                    } else {
                        return cn + COMP_CONFIG_SEP + NO_CONFIG_TOKEN;
                    }
                })
                .collect(Collectors.toSet());
    }

    private String createTableKey(Project project, String queryKey) {
        return project.toString() + ";" + queryKey;
    }

    public boolean isPartiallyVoided(Project project, String queryKey, String strategy, String configuration) {
        if (strategy.equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME)) {
            return false;
        }

        Set<String> strategySet = extractComponentSet(strategy, configuration);
        Set<String> querySet = table.get(createTableKey(project, queryKey));

        return !Sets.intersection(strategySet, querySet).equals(strategySet);
    }

    /**
     * Finds the strategies that a result must apply to by finding all valid combinations that
     * contain ALL the components that the result has plus all the components that this bug is missing.
     * This is much faster than iterating through all strategies and choosing whether or not this
     * result should apply. That algorithm is implemented for checking in the test.
     *
     * @param project  Of the result.
     * @param queryKey Of the result.
     * @param strategy Of the result.
     * @param config   Of the result.
     * @return Set of strategies that this result applies to.
     */
    private Set<String> findCombinatorialStrategiesForQuery(Project project, String queryKey, String strategy, String config) {
        Set<String> fixedComponents;

        if (strategy.equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME)) {
            fixedComponents = Collections.emptySet();
        } else {
            fixedComponents = new HashSet<>(DiscourseTaskStrategy.splitIntoComponentNames(strategy));
        }

        return findCombinationsOfMissingComponents(project, queryKey, config, fixedComponents);
    }

    private Set<String> findCombinationsOfMissingComponents(Project project, String queryKey, String config, Set<String> fixedComponents) {
        Map<String, List<String>> missingComponentsByConfig = findMissingComponents(project, queryKey)
                .stream()
                /*
                For example, if one of the fixed components is "OB", there won't be a strategy that
                also contains T_F_OB even if this bug is missing that component
                */
                .filter(s -> {
                    String c = s.split(COMP_CONFIG_SEP)[0];
                    BugReportComponent missingComponent = BugReportComponent.valueOf(c);
                    return fixedComponents.stream().noneMatch(missingComponent::hasSameGroup);
                })
                .collect(Collectors.toMap(
                        s -> s.split(COMP_CONFIG_SEP)[1],
                        s -> Collections.singletonList(s.split(COMP_CONFIG_SEP)[0]),
                        (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList())
                ));

        List<String> validConfigs;
        if (config == null) {
            validConfigs = new ArrayList<>(missingComponentsByConfig.keySet());
        } else {
            validConfigs = Collections.singletonList(config);
        }

        List<String> nonTaskComponents = missingComponentsByConfig.getOrDefault(NO_CONFIG_TOKEN, Collections.emptyList());

        return Stream.of(validConfigs, Collections.singletonList(NO_CONFIG_TOKEN))
                .flatMap(Collection::stream)
                .flatMap(vc -> {
                    Set<String> allComponentsToCombine = Stream.of(
                            missingComponentsByConfig.getOrDefault(vc, Collections.emptyList()),
                            nonTaskComponents
                    )
                            .flatMap(Collection::stream)
                            .collect(Collectors.toSet());

                    return DiscourseTaskStrategy.generateAllStringCombinations(allComponentsToCombine)
                            .stream()
                            .map(l -> {
                                Set<String> set = new HashSet<>(l);
                                Set<String> finalComponents = new HashSet<>(Sets.union(set, fixedComponents));
                                return generateFullStrategyName(finalComponents, vc.equals(NO_CONFIG_TOKEN) ? config : vc);
                            });
                })
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toSet());
    }

    private String generateFullStrategyName(Set<String> stringComponents, String configuration) {
        List<BugReportComponent> components = stringComponents.stream().map(BugReportComponent::valueOf).collect(Collectors.toList());

        String strategyName = new DiscourseTaskStrategy(components).toString();

        String config = null;
        if (components.stream().anyMatch(BugReportComponent::isTaskComponent)) {
            config = configuration;
        }

        return DiscourseQuery.generateFullStrategyName(strategyName, config);
    }

    private Set<String> findMissingComponents(Project project, String queryKey) {
        Set<String> queryComponents = table.get(createTableKey(project, queryKey));

        Sets.SetView<String> missingComponents = Sets.difference(allComponents, queryComponents);

        return new HashSet<>(missingComponents);
    }

    // FIXME return shouldn't be an optional set, instead just return empty set

    /**
     * Finds the strategies that a component combination of a query should apply to according to the
     * strategy type.
     *
     * @param project              Query data.
     * @param queryKey             Query data.
     * @param combination          Query data.
     * @param taskNavConfiguration Query data.
     * @return Set of strategy strings or empty optional if the query should not apply.
     */
    public Optional<Set<String>> findStrategiesForQuery(Project project, String queryKey,
                                                        String combination, String taskNavConfiguration,
                                                        StrategyType strategyType) {
        /*
        Make sure that this query does not contain voided components
        It can be the case that query contains A and B but B is lost in preprocessing, so
        the query will show up in A+B but it should not be valid there
        */
        if (isPartiallyVoided(
                project,
                queryKey,
                combination,
                taskNavConfiguration
        )) {
            LOGGER.warn("Found results of partially voided query " +
                    String.join(";", project.toString(), queryKey, combination, taskNavConfiguration) +
                    ". Outputting empty strategy set...");

            return Optional.empty();
        }

        if (strategyType == StrategyType.COMBINATORIAL) {
            return Optional.of(findCombinatorialStrategiesForQuery(project, queryKey, combination, taskNavConfiguration));
        } else {
            return Optional.of(findConjunctiveStrategiesForQuery(project, queryKey, combination, taskNavConfiguration));
        }
    }

    private Set<String> findConjunctiveStrategiesForQuery(Project project, String queryKey, String combination, String taskNavConfiguration) {
        // Since there are not that many ALL_TEXT queries we can just iterate over every possible strategy
        // It will apply to every strategy for which the query doesn't have ALL components
        if (combination.equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME)) {
            Set<Set<String>> allCombinations = generateAllComponentCombinations();
            Set<String> queryComponents = table.get(createTableKey(project, queryKey));
            return allCombinations.stream()
                    // Query does NOT contain all components in strategy
                    .filter(s -> !Sets.intersection(s, queryComponents).equals(s))
                    .map(s -> {
                        String config = s.stream()
                                .map(st -> st.split("--"))
                                .filter(a -> !a[1].equals("NONE"))
                                .map(a -> a[1])
                                .findFirst()
                                .orElse(null);

                        Set<String> bareCs = s.stream().map(st -> st.split("--")[0]).collect(Collectors.toSet());

                        return generateFullStrategyName(bareCs, config);
                    })
                    .collect(Collectors.toSet());
        }

        // Each non-ALL_TEXT query applies to the combination of components that it has and nothing else
        return Collections.singleton(generateFullStrategyName(
                Arrays.stream(combination.split("--")).collect(Collectors.toSet()),
                taskNavConfiguration)
        );
    }

    private Set<Set<String>> generateAllComponentCombinations() {
        if (allCombinations == null) {
            allCombinations = generateAllComponentCombinations(allComponents);
        }

        return allCombinations;
    }

    public String createNameSortedByApplicability(String strategyName, StrategyType strategyType) {
        if (strategyName.equals("BLIZZARD")) {
            return strategyName;
        }

        // TODO this code shouldn't need to be here, turn strategy into a class
        String blizzardPrefix = "BLIZZARD" + DiscourseTaskStrategy.COMPONENT_SEPARATOR;
        boolean isBlizzard = strategyName.startsWith(blizzardPrefix);
        if (isBlizzard) {
            strategyName =
                    strategyName.replace(blizzardPrefix, "");
        }

        Pair<String, String> p = findStrategyAndConfig(strategyName);

        Set<String> componentSet = extractComponentSet(p.getKey(), p.getValue());

        return (isBlizzard ? blizzardPrefix : "") +
                componentSet.stream()
                        .sorted(Comparator.comparing(applicability::get, (i2, i1) -> Integer.compare(i1, i2)))
                        .map(s -> s.split("--")[0])
                        .collect(Collectors.joining(strategyType.getComponentJoiner()));
    }

    private static Pair<String, String> findStrategyAndConfig(String strategyName) {
        String[] comps = strategyName.split("--");
        String lastComp = comps[comps.length - 1];

        String config;
        String realStrat;
        if (lastComp.startsWith("true") || lastComp.startsWith("false")) {
            config = lastComp;
            realStrat = String.join("--", Arrays.copyOfRange(comps, 0, comps.length - 1));
        } else {
            config = null;
            realStrat = String.join("--", comps);
        }

        return new Pair<>(realStrat, config);
    }

    public static Optional<String> extractConfiguration(String strategyName) {
        return Optional.ofNullable(findStrategyAndConfig(strategyName).getValue());
    }

    Map<Pair<Project, String>, Set<String>> getAllEntries() {
        return table.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> {
                            String[] split = e.getKey().split(";");
                            return new Pair<>(Project.fromString(split[0]), split[1]);
                        },
                        Map.Entry::getValue
                ));
    }

    Set<String> getAllComponents() {
        return allComponents;
    }
}
