package seers.disqueryreform.base;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Used to combine text extracted from TaskNav sentences.
 */
public class TextFragment {
    static final TextFragment NULL_FRAGMENT = new TextFragment(null, TextFragmentType.NULL);
    private String text;
    private TextFragmentType type;

    private TextFragment(String text, TextFragmentType type) {
        this.text = text;
        this.type = type;
    }

    /**
     * Merges two segments extracted from the same sentence.
     * Since there are cases in which multiple components apply to a single sentence,
     * we must define which text to keep. For example, if the sentence is marked as both
     * OB and EB and we have the strategies [OB, T_F_EB], then we get the full text
     * because it contains everything, while still satisfying both strategies. A more
     * complicated case is having both T_V and T_O strategies, in which case we must
     * combine the text and turn them into T_VO.
     *
     * @param other The segment to merge to this one.
     * @return A segment that represents the combination of both segments.
     */
    public TextFragment merge(TextFragment other) {
        if (type == TextFragmentType.NULL) {
            return other;
        }

        // Builds a map of both fragments where the key is the type
        Map<TextFragmentType, TextFragment> byType = Stream.of(this, other)
                .collect(Collectors.toMap(f -> f.type, f -> f, (f1, f2) -> {
                    if (!f1.text.equals(f2.text)) {
                        List<String> sortedF1 = Arrays.stream(f1.text.split(" "))
                                .sorted()
                                .collect(Collectors.toList());

                        if (!sortedF1.equals(Arrays.stream(f2.text.split(" "))
                                .sorted()
                                .collect(Collectors.toList()))) {
                            throw new IllegalArgumentException("Segments with same type have different text");
                        }
                    }

                    return f1;
                }));

        /*
        Bigger text types subsume smaller ones, e.g. if we are getting the full text there is
        no need to get the tasks because they are already included in the full text.
        */
        if (byType.containsKey(TextFragmentType.FULL_TEXT)) {
            return byType.get(TextFragmentType.FULL_TEXT);
        }

        if (byType.containsKey(TextFragmentType.FULL_TASKS)) {
            return byType.get(TextFragmentType.FULL_TASKS);
        }

        if (byType.containsKey(TextFragmentType.TASK_VERBS_AND_OBJECTS)) {
            return byType.get(TextFragmentType.TASK_VERBS_AND_OBJECTS);
        }

        // If we have both verbs and objects, we must combine them and apply the correct type
        if (byType.containsKey(TextFragmentType.TASK_VERBS) &&
                byType.containsKey(TextFragmentType.TASK_OBJECTS)) {
            return new TextFragment(this.text + " " + other.text, TextFragmentType.TASK_VERBS_AND_OBJECTS);
        }

        // They're both only verbs or only objects and they both have the same text
        return this;
    }

    public @Nullable
    String getText() {
        return text;
    }

    public enum TextFragmentType {
        FULL_TEXT(CodedBugReportWithTasks.Sentence::getText),
        FULL_TASKS(CodedBugReportWithTasks.Sentence::getFullTasksText),
        TASK_VERBS_AND_OBJECTS(CodedBugReportWithTasks.Sentence::getTaskVerbsAndObjects),
        TASK_VERBS(CodedBugReportWithTasks.Sentence::getTaskVerbs),
        TASK_OBJECTS(CodedBugReportWithTasks.Sentence::getTaskObjects),
        NULL(null);

        private final Function<CodedBugReportWithTasks.Sentence, Optional<String>> extractor;

        TextFragmentType(Function<CodedBugReportWithTasks.Sentence, Optional<String>> extractor) {
            this.extractor = extractor;
        }

        public TextFragment extractFrom(CodedBugReportWithTasks.Sentence sentence) {
            return extractor.apply(sentence)
                    .map(t -> new TextFragment(t, this))
                    .orElse(NULL_FRAGMENT);
        }
    }
}
