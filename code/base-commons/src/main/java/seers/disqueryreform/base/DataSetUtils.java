package seers.disqueryreform.base;

import seers.appcore.csv.CSVHelper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DataSetUtils {


    public static File[] getBRProjectFolders(String baseDataFolder, DataSet dataSet) {
        File baseFolder = getBaseDataSetFolder(baseDataFolder, dataSet);
        File brsFolder = null;
        switch (dataSet) {
            case BRT:
            case LB:
            case B4BL:
                brsFolder = baseFolder;
                break;
            case D4J:
            case QQ:
                brsFolder = Paths.get(baseFolder.getAbsolutePath(), "bug_reports").toFile();
                break;
        }
        return brsFolder.listFiles(File::isDirectory);
    }

    public static File getBaseDataSetFolder(String baseDataFolder, DataSet dataSet) {
        File baseFolder = null;
        switch (dataSet) {
            case BRT:
                baseFolder = Paths.get(baseDataFolder, "buglocator_data").toFile();
                break;
            case D4J:
                baseFolder = Paths.get(baseDataFolder, "defects4j_data").toFile();
                break;
            case LB:
                baseFolder = Paths.get(baseDataFolder, "lobster_data").toFile();
                break;
            case QQ:
                baseFolder = Paths.get(baseDataFolder, "query_quality_data").toFile();
                break;
            case B4BL:
                baseFolder = Paths.get(baseDataFolder, "bench4bl_data").toFile();
                break;
        }
        return baseFolder;
    }

    public static File getBugReportCodeFile(String baseDataFolder, DataSet dataSet, String projectName, String
            bugReportId, boolean preprocessed) throws IOException {
        File codeFolder = getCodeTaggedBugReportsFolder(baseDataFolder, dataSet, projectName, bugReportId,
                preprocessed);

        return Paths.get(codeFolder.getAbsolutePath(), bugReportId + ".xml").toFile();
    }

    public static String getProjectVersion(String baseDataFolder, DataSet dataSet, String projectName,
                                           String bugReportId) throws IOException {
        final List<List<String>> lines = readProjectVersions(baseDataFolder, dataSet, projectName);
        final Optional<List<String>> first = lines.stream().filter(l -> bugReportId.equals(l.get(1))).findFirst();
        return first.map(strings -> strings.get(0)).orElse(null);
    }

    public static List<List<String>> readProjectVersions(String baseDataFolder, DataSet dataSet, String projectName) throws IOException {
        final File baseDataSetFolder = getBaseDataSetFolder(baseDataFolder, dataSet);
        String filePath =
                Paths.get(baseDataSetFolder.getAbsolutePath(), "bug-reports-versions-" + projectName + ".csv").toString();
        return CSVHelper.readCsv(filePath, false, CSVHelper.DEFAULT_SEPARATOR);
    }

    public static File getCodeTaggedBugReportsFolder(String baseDataFolder, DataSet dataSet, String projectName,
                                                     String bugReportId, boolean preprocessed) throws IOException {
        File baseFolder = getBaseDataSetFolder(baseDataFolder, dataSet);
        String suffix = preprocessed ? "_prep" : "";
        File codeFolder = null;
        switch (dataSet) {
            case BRT:
            case LB:
                codeFolder = Paths.get(baseFolder.getAbsolutePath(), projectName, "identified_code" + suffix).toFile();
                break;
            case B4BL: {
                final String projectVersion = getProjectVersion(baseDataFolder, dataSet, projectName, bugReportId);
                codeFolder = Paths.get(baseFolder.getAbsolutePath(),  projectVersion,
                        "identified_code" + suffix).toFile();
                break;
            }
            case D4J:
            case QQ:
                codeFolder = Paths.get(baseFolder.getAbsolutePath(), "identified_code" + suffix,
                        projectName).toFile();
                break;
        }
        return codeFolder;
    }

    public static ConcurrentMap<BugKey, Integer> getSampledBugReportsIndex(String baseDataFolder, DataSet dataSet)
            throws IOException {
        final List<List<String>> sampledBugs = getSampledBugs(baseDataFolder);
        return sampledBugs.stream()
                .filter(l -> l.get(0).equals(dataSet.toString().toLowerCase()))
                .collect(Collectors.toConcurrentMap(l -> new BugKey(l.get(1).toLowerCase(), l.get(2)), l -> 1));
    }

    public static List<BugKey> readSampledBugReports(String baseDataFolder, DataSet dataSet) throws IOException {
        final List<List<String>> sampledBugs = getSampledBugs(baseDataFolder);
        return sampledBugs.stream()
                .filter(l -> l.get(0).equals(dataSet.toString().toLowerCase()))
                .map(l -> new BugKey(l.get(1), l.get(2)))
                .collect(Collectors.toList());
    }

    private static List<List<String>> getSampledBugs(String baseDataFolder) throws IOException {
        File baseFolder = new File(baseDataFolder);
        final File parentFolder = baseFolder.getParentFile();
        File sampleFile = Paths.get(parentFolder.getAbsolutePath(), "all_bug_coding", "all_coded.csv").toFile();
        final List<List<String>> lines = CSVHelper.readCsv(sampleFile, true, Function.identity(), CSVHelper
                .DEFAULT_SEPARATOR, "UTF-8");
        //filter out non-bugs and bugs from aspectj-1.5.3
        return lines.stream()
                .filter(l -> !l.get(5).equals("1") && !l.get(1).equals("aspectj-1.5.3"))
                .collect(Collectors.toList());
    }

    public static List<BugKey> readSampledBugReports(String bugRepSampleFile) throws IOException {
        final List<List<String>> lines = CSVHelper.readCsv(new File(bugRepSampleFile), false, Function.identity(),
                CSVHelper.DEFAULT_SEPARATOR, "UTF-8");
        return lines.stream().map(l -> new BugKey(l.get(0).toLowerCase(), l.get(1))).collect(Collectors.toList());
    }

    public static File getReformulatedQueriesOutFolder(String queriesOutBaseFolder, DataSet dataSet, boolean
            andOperator) {
        String suffix = (andOperator ? "and" : "or");
        File baseFolder = new File(queriesOutBaseFolder);
        File queriesFolder = null;
        switch (dataSet) {
            case BRT:
                queriesFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "buglocator-sample-hard-to-retrieve_" + suffix).toFile();
                break;
            case D4J:
                queriesFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "d4j-all-hard-to-retrieve-queries_" + suffix).toFile();
                break;
            case LB:
                queriesFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "lobster-all-hard-to-retrieve_" + suffix).toFile();
                break;
            case QQ:
                queriesFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "query_quality-all-hard-to-retrieve-queries_" + suffix).toFile();
                break;
            case B4BL:
                queriesFolder = Paths.get(baseFolder.getAbsolutePath(), "b4bl_" + suffix).toFile();
                break;
        }
        return queriesFolder;
    }

    @SuppressWarnings("unused")
    public static File getCodedBugReportsFolder(String baseDataFolder, DataSet dataSet) {
        File baseFolder = new File(baseDataFolder);
        final File parentFolder = baseFolder.getParentFile();
        File codedBugsFolder = null;
        switch (dataSet) {
            case BRT:
                codedBugsFolder = Paths.get(parentFolder.getAbsolutePath(), "all_bug_coding",
                        "buglocator-sample-hard-to-retrieve").toFile();
                break;
            case D4J:
                codedBugsFolder = Paths.get(parentFolder.getAbsolutePath(), "all_bug_coding",
                        "d4j-all-hard-to-retrieve").toFile();
                break;
            case LB:
                codedBugsFolder = Paths.get(parentFolder.getAbsolutePath(), "all_bug_coding",
                        "lobster-all-hard-to-retrieve").toFile();
                break;
            case QQ:
                codedBugsFolder = Paths.get(parentFolder.getAbsolutePath(), "all_bug_coding",
                        "query_quality-all-hard-to-retrieve").toFile();
                break;
            case B4BL:
                codedBugsFolder = Paths.get(parentFolder.getAbsolutePath(), "all_bug_coding",
                        "b4bl").toFile();
                break;
        }

        return codedBugsFolder;
    }


    public static File getResultsFolder(String baseDataFolder, DataSet dataSet) {
        File baseFolder = new File(baseDataFolder);
        File codedBugsFolder = null;
        switch (dataSet) {
            case BRT:
                codedBugsFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "buglocator-sample-hard-to-retrieve").toFile();
                break;
            case D4J:
                codedBugsFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "d4j-all-hard-to-retrieve").toFile();
                break;
            case LB:
                codedBugsFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "lobster-all-hard-to-retrieve").toFile();
                break;
            case QQ:
                codedBugsFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "query_quality-all-hard-to-retrieve").toFile();
                break;
            case B4BL:
                codedBugsFolder = Paths.get(baseFolder.getAbsolutePath(), "b4bl").toFile();
                break;
        }
        return codedBugsFolder;
    }

    public static String getCorpusBaseFolder(String baseDataFolder, String baseDataFolder2, DataSet dataSet) {
        File baseFolder = getBaseDataSetFolder(baseDataFolder, dataSet);
        File corpusFolder = null;
        switch (dataSet) {
            case BRT:
            case LB:
            case B4BL:
                corpusFolder = baseFolder;
                break;
            case QQ:
                corpusFolder = Paths.get(baseFolder.getAbsolutePath(),
                        "query_quality_code_corpus_prep").toFile();
                break;
            case D4J:
                File baseFolder2 = getBaseDataSetFolder(baseDataFolder2, dataSet);
                corpusFolder = Paths.get(baseFolder2.getAbsolutePath(),
                        "dj4_src_code_corpus_prep").toFile();
                break;

        }
        return corpusFolder.getAbsolutePath();
    }


    public static class BugKey {

        public String system;
        public String bugId;

        public BugKey(String system, String bugId) {
            super();
            this.system = system;
            this.bugId = bugId;
        }

        @Override
        public String toString() {
            return "[sys=" + system + ", bug=" + bugId + "]";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BugKey bugKey = (BugKey) o;
            return Objects.equals(system, bugKey.system) &&
                    Objects.equals(bugId, bugKey.bugId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(system, bugId);
        }
    }
}
