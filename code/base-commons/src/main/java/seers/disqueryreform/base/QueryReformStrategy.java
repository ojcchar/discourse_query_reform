package seers.disqueryreform.base;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public enum QueryReformStrategy {
    ALL_TEXT, //first ALL_TEXT!!
    CODE,
    EB,
    EB_CODE,
    EB_S2R,
    EB_S2R_CODE,
    EB_S2R_TITLE,
    EB_S2R_TITLE_CODE,
    EB_TITLE,
    EB_TITLE_CODE,
    OB,
    OB_CODE,
    OB_EB,
    OB_EB_CODE,
    OB_EB_S2R,
    OB_EB_S2R_CODE,
    OB_EB_S2R_TITLE,
    OB_EB_S2R_TITLE_CODE,
    OB_EB_TITLE,
    OB_EB_TITLE_CODE,
    OB_S2R,
    OB_S2R_CODE,
    OB_S2R_TITLE,
    OB_S2R_TITLE_CODE,
    OB_TITLE,
    OB_TITLE_CODE,
    S2R,
    S2R_CODE,
    S2R_TITLE,
    S2R_TITLE_CODE,
    TITLE,
    TITLE_CODE;


    public static List<QueryReformStrategy> getAllStrategiesExceptBaseline() {
        final List<QueryReformStrategy> queryReformStrategies = new LinkedList<>(Arrays.asList(QueryReformStrategy
                .values()));
        queryReformStrategies.remove(0);
        return queryReformStrategies;
    }

}
