package seers.disqueryreform.base.bench4bl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.xml.XMLHelper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Bench4BLUtils {


    private static final Logger LOGGER = LoggerFactory.getLogger(Bench4BLUtils.class);

    private static String dataFolder = "C:\\Bench4BL\\data";
    private static String resultsFolder = "C:\\Bench4BL\\expresults";

    public static List<ImmutablePair<String, String>> groupProjects = new ArrayList<>();

    static {
        groupProjects.add(new ImmutablePair<>("Wildfly", "SWARM"));
        groupProjects.add(new ImmutablePair<>("Commons", "CONFIGURATION"));
        groupProjects.add(new ImmutablePair<>("Commons", "COMPRESS"));
        groupProjects.add(new ImmutablePair<>("Spring", "DATAMONGO"));
        /*groupProjects.add(new ImmutablePair<>("Commons", "IO"));
        groupProjects.add(new ImmutablePair<>("Wildfly", "WFCORE"));
        groupProjects.add(new ImmutablePair<>("Jboss", "ENTESB"));
        groupProjects.add(new ImmutablePair<>("Spring", "AMQP"));
        groupProjects.add(new ImmutablePair<>("Spring", "DATACMNS"));*/
    }

    public static List<List<String>> getRawResults(String group, String project, String taskName, String technique) {

        File projectResultsFolder = Paths.get(resultsFolder, taskName, group, project).toFile();
        if (!projectResultsFolder.exists()) {
            throw new RuntimeException("The folder does not exist: " + projectResultsFolder);
        }
        File[] resultsFiles = Objects.requireNonNull(
                projectResultsFolder.
                        listFiles(n -> {
                            return n.getName().startsWith(technique) && n.getName().endsWith("_output.txt");
                        }));

        return Arrays.stream(resultsFiles)
                .map(f -> {
                    try {
                        return CSVHelper.readCsv(f.getAbsolutePath(), false, '\t');
                    } catch (IOException e) {
                        LOGGER.error("Error for file " + f, e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public static List<String> getBugs(String group, String project) throws IOException {

        File bugsFile = Paths.get(dataFolder, group, project, "bugs.txt").toFile();
        final String content = FileUtils.readFileToString(bugsFile, Charset.defaultCharset());
        final Gson gson = GsonUtils.createDefaultGson();
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        Map<String, Object> bugInfo = gson.fromJson(content, type);
        final List<Double> allBugs = (List) ((Map) bugInfo.get(project)).get("all");
        return allBugs.stream()
                .map(b -> String.valueOf(b.intValue()))
                .collect(Collectors.toList());
    }

    public static File[] getSourceFolders(String group, String project) {
        File srcFolder = getSourceFolder(group, project);
        return srcFolder.listFiles(File::isDirectory);
    }

    public static File getSourceFolder(String group, String project) {
        return Paths.get(dataFolder, group, project, "sources").toFile();
    }

    public static File getSourceFolder(String group, String project, String subProject) {
        return Paths.get(dataFolder, group, project, "sources", subProject).toFile();
    }

    public static File getGitRepoFolder(String group, String project) {
        return Paths.get(dataFolder, group, project, "gitrepo").toFile();
    }

    public static String getTag(String group, String project, String subProject) throws IOException {

        final File ver1File = Paths.get(dataFolder, group, project, "versions.txt").toFile();
        final File ver2File = Paths.get(dataFolder, group, project, "versions2.txt").toFile();

        final Gson gson = GsonUtils.createDefaultGson();
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();

        final String content1 = FileUtils.readFileToString(ver1File, Charset.defaultCharset());
        Map<String, Object> json1 = gson.fromJson(content1, type);

        final String content2 = FileUtils.readFileToString(ver2File, Charset.defaultCharset());
        Map<String, Object> json2 = gson.fromJson(content2, type);

        final String version = ((Map) json2.get(project)).get(subProject).toString();

        return ((Map) json1.get(project)).get(version).toString();
    }


    public static List<String> sourceFileIndex(String group, String project, String systemVersion, String taskName,
                                               String technique) throws IOException {
        File projectResultsFolder = Paths.get(resultsFolder, taskName, group, project).toFile();
        final File[] files =
                projectResultsFolder.listFiles(f -> f.isDirectory() && f.getName().startsWith(technique)
                        && f.getName().endsWith(systemVersion));

        if (files == null || files.length == 0) {
            LOGGER.warn("Couldn't find system version: " + systemVersion);
            return null;
        } else if (files.length > 1)
            LOGGER.warn("Found multiple system versions: " + Arrays.toString(files));

        return FileUtils.readLines(Paths.get(files[0].getAbsolutePath(), "sourceFileIndex.txt").toFile(),
                Charset.defaultCharset());
    }

    public static BugRepository getBugRepository(String group, String project) throws Exception {
        File file = getBugRepositoryFile(group, project);
        return XMLHelper.readXML(BugRepository.class, file);
    }

    public static File getBugRepositoryFile(String group, String project) {
        return Paths.get(dataFolder, group, project, "bugrepo", "repository.xml").toFile();
    }

    public static List<ImmutablePair<String, String>> getBugsWithVersions(String group, String project) throws IOException {

        //read content
        File bugsFile = Paths.get(dataFolder, group, project, "bugs.txt").toFile();
        final String content = FileUtils.readFileToString(bugsFile, Charset.defaultCharset());

        //parse the content
        final Gson gson = GsonUtils.createDefaultGson();
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        Map<String, Object> bugInfo = gson.fromJson(content, type);

        final Set<Map.Entry<String, List<Double>>> entries = ((Map) bugInfo.get(project)).entrySet();

        List<ImmutablePair<String, String>> collection2 = new ArrayList<>();
        entries.stream()
                //filter out these
                .filter(e -> !e.getKey().equals("all") && !e.getKey().equals("miss"))
                //map the bugs to version-bug pairs and add them to the list
                .forEach(e -> {
                    for (Double aDouble : e.getValue()) {
                        collection2.add(new ImmutablePair<>(e.getKey(), String.valueOf(aDouble.intValue())));
                    }
                });
        return collection2;
    }

    public static void main(String[] args) throws Exception {
     /*   String file = "C:\\Users\\ojcch\\Documents\\Projects\\Discourse_query_reformulation\\Bench4BL\\data\\Spring" +
                "\\AMQP\\bugrepo\\repository.xml";
        BugRepository repo = XMLHelper.readXML(BugRepository.class, file);

        final Bug bug = repo.getBugs().stream().filter(b -> b.getId().equals("190")).findFirst().get();
        System.out.println(bug.getDescription());

        String d = StringEscapeUtils.unescapeHtml4(bug.getDescription());
        System.out.println(d);

        Bug newB = new Bug();
        Bug.BugInformation info = new Bug.BugInformation();
        info.setDescription(d);
        newB.setBugInformation(info);
        XMLHelper.writeXML(newB, "C:\\Users\\ojcch\\Documents\\Projects\\Discourse_query_reformulation\\Bench4BL
        \\data\\Spring" +
                "\\AMQP\\bugrepo\\bug.xml");*/

        System.out.println(StringEscapeUtils.unescapeHtml3("&apos;"));
    }

}
