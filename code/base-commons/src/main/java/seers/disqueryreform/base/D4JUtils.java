package seers.disqueryreform.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import net.quux00.simplecsv.CsvParser;
import net.quux00.simplecsv.CsvParserBuilder;
import net.quux00.simplecsv.CsvReader;

public class D4JUtils {
	
	public static List<List<String>> readLines(File inputFile, char separator)
			throws IOException, FileNotFoundException {
		CsvParser csvParser = new CsvParserBuilder().separator(separator).build();
		try (CsvReader csvReader = new CsvReader(new FileReader(inputFile), csvParser)) {
			return csvReader.readAll();
		}
	}


}
