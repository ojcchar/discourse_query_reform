package seers.disqueryreform.base;

import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.entity.Identifiable;
import org.joda.time.DateTime;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

@SuppressWarnings({"FieldCanBeLocal"})
public class DiscourseQuery implements Identifiable {

    private final String key;
    // TODO: taskNav config and strategy should be merged
    private final String taskNavConfiguration;
    private final String strategy;
    @Nullable
    private String title;
    @Nullable
    private String description;
    /**
     * These are also set in the preprocessing python script:
     * code/scripts/new_scripts/preprocess-queries-containers.py
     */
    private String rawTitle;
    private String rawDescription;
    private int id;
    private DateTime creationDate;
    private DateTime resolutionDate;
    private Set<String> goldSet;

    public DiscourseQuery(String key, String strategy, String taskNavConfiguration, String title, String description) {
        this.key = key;
        setText(title, description);
        this.taskNavConfiguration = taskNavConfiguration;
        this.strategy = strategy;
    }

    // TODO: remove this method after config and strategy are merged
    public static String generateFullStrategyName(String strategy, String taskNavConfiguration) {
        if (strategy.equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME)
                || taskNavConfiguration == null || taskNavConfiguration.isEmpty()) {
            return strategy;
        } else {
            return strategy + "--" + taskNavConfiguration;
        }
    }

    // Make it so that the query is created with all its info
    @Deprecated
    public void update(int id, DateTime creationDate, DateTime resolutionDate, Set<String> goldSet) {
        this.id = id;
        this.creationDate = creationDate;
        this.resolutionDate = resolutionDate;
        this.goldSet = goldSet;
    }

    // TODO: add project to DiscourseQuery
    public BugReport toBugReport(boolean useRawText, Project project) {
        String queryText = useRawText ? getRawText() : getText();

        List<String> newGoldSet = new ArrayList<>(goldSet);

        BugReport bugReport = new BugReport(
                String.valueOf(id),
                /* Title is kept null for compatibility clients using this method don't care
                 * about all the text being in the description */
                null,
                queryText,
                creationDate,
                resolutionDate,
                newGoldSet
        );

        bugReport.setProject(key + ";" + project + ";" + strategy + ";" + taskNavConfiguration);

        return bugReport;
    }

    // FIXME this setter should not exist
    public void setRawText(@Nullable String rawTitle, @Nullable String rawDescription) {
        this.rawTitle = rawTitle == null || rawTitle.trim().isEmpty() ? null : rawTitle.trim();
        this.rawDescription = rawDescription == null || rawDescription.trim().isEmpty() ? null : rawDescription.trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscourseQuery that = (DiscourseQuery) o;
        return key.equals(that.key) &&
                Objects.equals(taskNavConfiguration, that.taskNavConfiguration) &&
                strategy.equals(that.strategy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, taskNavConfiguration, strategy);
    }

    @Override
    public String toString() {
        return "DiscourseQuery{" +
                "key='" + key + '\'' +
                ", taskNavConfiguration='" + taskNavConfiguration + '\'' +
                ", strategy='" + strategy + '\'' +
                '}';
    }

    @Override
    public String getKey() {
        return key;
    }

    public Optional<String> getRawTitle() {
        return Optional.ofNullable(rawTitle);
    }

    public Optional<String> getRawDescription() {
        return Optional.ofNullable(rawDescription);
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public DateTime getResolutionDate() {
        return resolutionDate;
    }

    @Nonnull
    private String getRawText() {
        String theTitle = Optional.ofNullable(rawTitle).orElse("");
        String theDescription = Optional.ofNullable(rawDescription).orElse("");

        return (theTitle + " " + theDescription).trim();
    }

    // TODO use instead an ID including the query key, strategy, etc. Do this by parameterizing Identifiable interface
    @Deprecated
    public int getId() {
        return id;
    }

    @Deprecated
    public void setId(int id) {
        this.id = id;
    }

    public String getTaskNavConfiguration() {
        return taskNavConfiguration;
    }

    public String getStrategy() {
        return strategy;
    }

    @Nonnull
    public String getText() {
        String theTitle = Optional.ofNullable(title).orElse("");
        String theDescription = Optional.ofNullable(description).orElse("");

        return (theTitle + " " + theDescription).trim();
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public void setText(String title, String description) {
        this.title = title == null || title.trim().isEmpty() ? null : title.trim();
        this.description = description == null || description.trim().isEmpty() ? null : description.trim();
    }

    public Set<String> getGoldSet() {
        return goldSet;
    }
}
