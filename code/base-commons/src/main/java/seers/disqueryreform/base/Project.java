package seers.disqueryreform.base;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Objects;

public class Project {

    private final DataSet dataSet;
    private final String projectName;

    public Project(DataSet dataSet, String projectName) {
        this.dataSet = dataSet;
        this.projectName = projectName;
    }

    @SuppressWarnings("WeakerAccess")
    public static Project fromString(String s) {
        String[] split = s.split("__");

        return new Project(DataSet.valueOf(split[0]), split[1]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return dataSet == project.dataSet &&
                projectName.equals(project.projectName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataSet, projectName);
    }

    @Override
    public String toString() {
        return String.format("%s__%s", dataSet, projectName);
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public String getProjectName() {
        return projectName;
    }

    protected static TypeAdapter<Project> getTypeAdapter() {
        return new TypeAdapter<Project>() {
            @Override
            public void write(JsonWriter jsonWriter, Project project) throws IOException {
                jsonWriter.value(project.toString());
            }

            @Override
            public Project read(JsonReader jsonReader) throws IOException {
                return fromString(jsonReader.nextString());
            }
        };
    }
}
