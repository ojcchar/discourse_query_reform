package seers.disqueryreform.base;

import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionSentence;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

public class CodedBugReportWithTasks {
    /**
     * Used to join the bug report components in the query text.
     */
    private static final String COMPONENT_JOINER = "\n";

    private final Sentence title;
    private final String key;
    private final List<Sentence> sentences = new ArrayList<>();
    private String taskNavConfiguration = null;

    public CodedBugReportWithTasks(DataSet dataSet, String system, PatternLabeledBugReport labeledBugReport,
                                   @Nullable TaskNavContainer container, Set<String> codeTypesToDiscard) {
        key = labeledBugReport.getId();

        TaskNavContainer.TaskNavBugReport tasksBugReport;
        if (container != null) {
            tasksBugReport = container.findBugReport(dataSet, system, key);
            taskNavConfiguration = container.getConfiguration();
        } else {
            tasksBugReport = null;
        }

        title = Sentence.createTitle(labeledBugReport, tasksBugReport);

        Sentence.createCodeSentence(labeledBugReport, codeTypesToDiscard)
                .ifPresent(sentences::add);

        Optional.ofNullable(labeledBugReport.getDescription())
                .ifPresent(d ->
                        sentences.addAll(d.getAllSentences().stream()
                                .map(ls -> Sentence.createSentence(ls, tasksBugReport))
                                .collect(Collectors.toList())
                        )
                );
    }

    public Optional<DiscourseQuery> extractTextForStrategy(DiscourseTaskStrategy strategy) {
        DiscourseTaskStrategy.StrategyApplier applier = strategy.createApplier();

        Optional<String> titleOption = applier.apply(title);

        String descriptionText = sentences.stream()
                .map(sentence -> applier.apply(sentence).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.joining(COMPONENT_JOINER));

        String combinedText = titleOption
                .map(t -> t + COMPONENT_JOINER + descriptionText)
                .orElse(descriptionText)
                .trim();

        if (applier.isSatisfied() && !combinedText.isEmpty()) {
            return Optional.of(
                    new DiscourseQuery(key, strategy.toString(), taskNavConfiguration,
                            titleOption.orElse(null), descriptionText)
            );
        } else {
            return Optional.empty();
        }
    }

    static class Sentence {
        private static final String TITLE_ID = "title";
        private static final String CODE_ID = "code";

        /**
         * Is never null.
         */
        private final List<TaskNavContainer.TaskNavTask> tasks;

        /**
         * Can be "title" or "code".
         */
        private final String id;

        private final String text;
        private final boolean hasOB;
        private final boolean hasEB;
        private final boolean hasS2R;

        private Sentence(String id, @Nullable String text, boolean hasOB, boolean hasEB,
                         boolean hasS2R, @Nullable List<TaskNavContainer.TaskNavTask> tasks) {
            this.id = id;
            this.text = text != null && !text.trim().isEmpty() ? text.trim() : null;
            this.hasOB = hasOB;
            this.hasEB = hasEB;
            this.hasS2R = hasS2R;
            this.tasks = tasks != null ? tasks : Collections.emptyList();
        }

        private static Sentence createSentence(PatternLabeledDescriptionSentence labeledSentence, TaskNavContainer.TaskNavBugReport tasksReport) {
            List<TaskNavContainer.TaskNavTask> tasks = null;
            if (tasksReport != null) {
                tasks = tasksReport.getSentence(labeledSentence.getId())
                        .map(TaskNavContainer.TaskNavSentence::getTasks)
                        .orElse(null);
            }

            return new Sentence(labeledSentence.getId(), labeledSentence.getValue(), labeledSentence.hasOB(),
                    labeledSentence.hasEB(), labeledSentence.hasS2R(), tasks);
        }

        private static Sentence createTitle(PatternLabeledBugReport labeledBugReport, TaskNavContainer.TaskNavBugReport tasksReport) {
            PatternLabeledBugReportTitle codedTitle = labeledBugReport.getTitle();

            List<TaskNavContainer.TaskNavTask> titleTasks = null;

            if (tasksReport != null) {
                titleTasks = tasksReport.getTitle()
                        .map(TaskNavContainer.TaskNavSentence::getTasks)
                        .orElse(null);
            }

            return new Sentence(Sentence.TITLE_ID, codedTitle.getValue(),
                    codedTitle.hasOB(), codedTitle.hasEB(), codedTitle.hasS2R(),
                    titleTasks);
        }

        private static Optional<Sentence> createCodeSentence(PatternLabeledBugReport labeledBugReport, Set<String> codeTypesToDiscard) {
            String codeText = labeledBugReport.getCode().stream()
                    .map(t -> {
                        String value = t.getValue();

                        if (codeTypesToDiscard.contains(t.getType())) {
                            /* If it is a type we need to discard, we ignore the text but keep a newline if
                             * it is there. This is because BLIZZARD hangs when there is a lot of
                             * code in one line */
                            return value.contains("\n") ? "\n" : "";
                        } else {
                            return value;
                        }
                    })
                    // We cannot simply join with \n because this might break up expressions
                    .collect(Collectors.joining(" "))
                    .trim();

            if (!codeText.isEmpty()) {
                return Optional.of(new Sentence(Sentence.CODE_ID, codeText, false,
                        false, false, null));
            }

            return Optional.empty();
        }

        boolean hasOB() {
            return hasOB;
        }

        boolean hasEB() {
            return hasEB;
        }

        boolean hasS2R() {
            return hasS2R;
        }

        /**
         * Return an empty {@link Optional<String>} if the input string is null or empty. Note that
         * the input string should be trimmed already (we are using it only on strings that
         * result from joining Collector)
         *
         * @param string A trimmed string
         * @return Optional with a non-empty string or empty optional.
         */
        private Optional<String> nullIfEmptyString(String string) {
            String result = null;

            if (string != null && !string.isEmpty()) {
                // Input string should always be trimmed
                result = string;
            }

            return Optional.ofNullable(result);
        }

        Optional<String> getTaskVerbsAndObjects() {
            return nullIfEmptyString(
                    tasks.stream()
                            .map(TaskNavContainer.TaskNavTask::getVerbAndObject)
                            .collect(Collectors.joining(" "))
            );
        }

        Optional<String> getTaskVerbs() {
            return nullIfEmptyString(
                    tasks.stream()
                            .map(TaskNavContainer.TaskNavTask::getVerb)
                            .collect(Collectors.joining(" "))
            );
        }

        Optional<String> getTaskObjects() {
            return nullIfEmptyString(
                    tasks.stream()
                            .map(taskNavTask -> taskNavTask.getObject().orElse(null))
                            .filter(Objects::nonNull)
                            .collect(Collectors.joining(" "))
            );
        }

        Optional<String> getFullTasksText() {
            return nullIfEmptyString(
                    tasks.stream()
                            .map(TaskNavContainer.TaskNavTask::getAllText)
                            .collect(Collectors.joining(" "))
            );
        }

        Optional<String> getText() {
            return Optional.ofNullable(text);
        }

        boolean isOther() {
            return !hasOB && !hasEB && !hasS2R && !isTitle() && !isCode();
        }

        boolean isTitle() {
            return id.equals(TITLE_ID);
        }

        boolean isCode() {
            return id.equals(CODE_ID);
        }
    }

}
