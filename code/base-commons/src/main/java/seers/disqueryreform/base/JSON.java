package seers.disqueryreform.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import edu.utdallas.seers.ir4se.index.utils.DateTimeJsonAdapter;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

// TODO move to SEERS base
public class JSON {
    public static <T> void writeJSON(T object, Path filePath) throws IOException {
        writeJSON(object, filePath, false);
    }

    public static <T> void writeJSONPretty(T object, Path filePath) throws IOException {
        writeJSON(object, filePath, true);
    }

    private static <T> void writeJSON(T object, Path filePath, boolean pretty) throws IOException {
        FileUtils.forceMkdirParent(filePath.toFile());

        Gson gson = createDefaultGson(pretty);

        try (FileWriter writer = new FileWriter(filePath.toFile())) {
            gson.toJson(object, writer);
        }
    }

    // TODO rename to tryRead
    public static <T> T readJSON(Path filePath, Class<T> type) throws IOException {
        Gson gson = createDefaultGson(false);

        try {
            return gson.fromJson(Files.newBufferedReader(filePath), type);
        } catch (JsonSyntaxException e) {
            throw new IllegalStateException("Error when decoding JSON. Did you forget to register a type adapter?", e);
        }
    }

    // TODO rename to just read as opposed to tryRead

    /**
     * Reads JSON without throwing checked exceptions.
     *
     * @param filePath Path of JSON file.
     * @param type     Type to read as.
     * @param <T>      Type to read as.
     * @return Deserialized object.
     */
    public static <T> T readJSONSilent(Path filePath, Class<T> type) {
        try {
            return readJSON(filePath, type);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static Gson createDefaultGson(boolean prettyPrinting) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeJsonAdapter());
        gsonBuilder.registerTypeAdapter(Project.class, Project.getTypeAdapter());

        if (prettyPrinting) {
            gsonBuilder.setPrettyPrinting();
        }

        return gsonBuilder.create();
    }
}
