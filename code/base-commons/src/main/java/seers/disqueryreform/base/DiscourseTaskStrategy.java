package seers.disqueryreform.base;

import com.google.common.collect.Sets;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DiscourseTaskStrategy {
    @SuppressWarnings("unused")
    public static final String ALL_TEXT_STRATEGY_NAME = "ALL_TEXT";
    public static final String COMPONENT_SEPARATOR = "--";

    private final Set<BugReportComponent> components;

    public DiscourseTaskStrategy(List<BugReportComponent> components) {
        this.components = new HashSet<>(components);
    }

    @SuppressWarnings({"unused"})
    public static Set<DiscourseTaskStrategy> generateDiscourseTaskStrategies() {
        Set<BugReportComponent> components = Sets.union(BugReportComponent.DISCOURSE_COMPONENTS, BugReportComponent.TASK_COMPONENTS);
        Set<DiscourseTaskStrategy> allStrategies = generateAllStrategies(components);
        Set<DiscourseTaskStrategy> discourseStrategies = new HashSet<>(generateDiscourseStrategies());

        // We do not want the original 31 in there
        return Sets.difference(allStrategies, discourseStrategies);
    }

    private static Set<DiscourseTaskStrategy> generateAllStrategies(Set<BugReportComponent> components) {
        return generateAllCombinations(components)
                .stream()
                // There will be an empty combination, which would select no components. We don't want it
                .filter(l -> !l.isEmpty())
                .map(DiscourseTaskStrategy::new)
                .collect(Collectors.toSet());
    }

    @SuppressWarnings("WeakerAccess")
    public static Set<DiscourseTaskStrategy> generateAllOtherStrategies() {
        // Sets  {C, C+Tasks}
        Stream<Set<BugReportComponent>> withTasks = Stream.of(
                BugReportComponent.TITLE,
                BugReportComponent.OB,
                BugReportComponent.EB,
                BugReportComponent.S2R
        )
                .map(c -> Sets.newHashSet(c, BugReportComponent.valueOf("T_F_" + c.toString())));

        // Only {C}
        Stream<Set<BugReportComponent>> noTasks = Stream.of(Collections.singleton(BugReportComponent.CODE));

        // Sets {C, ... , null}
        Stream<Set<Optional<BugReportComponent>>> withOption = Stream.of(withTasks, noTasks)
                .flatMap(st -> st.map(s -> s.stream().map(Optional::of).collect(Collectors.toSet())))
                // Add empty optional to each
                .peek(s -> s.add(Optional.empty()));

        // Only {C}
        Set<Optional<BugReportComponent>> nonOptional = Collections.singleton(Optional.of(BugReportComponent.OTHER));

        List<Set<Optional<BugReportComponent>>> sets = Stream.concat(withOption, Stream.of(nonOptional))
                .collect(Collectors.toList());

        return Sets.cartesianProduct(sets)
                .stream()
                .map(l -> l.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()))
                .filter(s -> !s.isEmpty())
                .map(DiscourseTaskStrategy::new)
                .collect(Collectors.toSet());
    }

    /**
     * Returns all combinations of set `D` (the set of discourse components defined in
     * {@link BugReportComponent#DISCOURSE_COMPONENTS}).
     *
     * @return List of strategies corresponding to all combinations of discourse components.
     */
    @SuppressWarnings("unused")
    public static Set<DiscourseTaskStrategy> generateDiscourseStrategies() {
        return generateAllStrategies(BugReportComponent.DISCOURSE_COMPONENTS);
    }

    /**
     * We calculate this as the cartesian product of N sets where N is the amount of groups of
     * strategies (e.g. OB and tasks from OB are in the same group) and each one of the sets
     * contains the components that belong to it and null
     * (actually we use optionals to represent the null because cartesianProduct does not allow nulls)
     * Selecting the empty optional from a set means that component will not be used.
     *
     * @param components Components to combine.
     * @return All possible valid combinations (not including redundant strategies like OB and tasks
     * from OB). Includes the empty strategy.
     */
    private static Set<List<BugReportComponent>> generateAllCombinations(Set<BugReportComponent> components) {
        /*
        Components must be grouped, e.g. all the ones that use OB info are in the same group
        we must also add an "empty component" (empty optional) to signify the choice of not using that component
        */
        List<Set<Optional<BugReportComponent>>> componentSets =
                BugReportComponent.groupByBaseComponent(components).stream()
                        // We use optionals to indicate that we either use the component or not use it
                        .map(g -> Stream.concat(
                                g.stream().map(Optional::of),
                                Stream.<Optional<BugReportComponent>>of(Optional.empty())
                        )
                                .collect(Collectors.toSet()))
                        .collect(Collectors.toList());

        /*
        Each list is a tuple (d1, d2, d3, ...) where each d is either a component or null,
        meaning that component is not part of that strategy
        */
        Stream<List<BugReportComponent>> cartesianProduct = Sets.cartesianProduct(componentSets)
                .stream()
                // Lists will contain the empty optional
                .map(l -> l.stream()
                        // Only keep the non null ones
                        .filter(Optional::isPresent)
                        // Get the actual components
                        .map(Optional::get)
                        .collect(Collectors.toList()));

        return cartesianProduct
                .collect(Collectors.toSet());
    }

    public static List<String> splitIntoComponentNames(String strategyName) {
        return Arrays.asList(strategyName.split(COMPONENT_SEPARATOR));
    }

    static Set<List<String>> generateAllStringCombinations(Set<String> componentStrings) {
        if (componentStrings.isEmpty()) {
            return Collections.singleton(Collections.emptyList());
        }

        Set<List<BugReportComponent>> combinationObjects = generateAllCombinations(componentStrings.stream()
                .map(BugReportComponent::valueOf)
                .collect(Collectors.toSet()));

        return combinationObjects
                .stream()
                .map(l -> l.stream().map(Enum::toString).collect(Collectors.toList()))
                .collect(Collectors.toSet());
    }

    /**
     * Generates only strategies with full tasks.
     *
     * @return A set of strategies.
     */
    public static Set<DiscourseTaskStrategy> generateSimplifiedStrategies() {
        Set<DiscourseTaskStrategy> allStrategies = generateDiscourseTaskStrategies();

        return allStrategies.stream()
                // Either full tasks or no tasks at all
                .filter(s -> s.getComponents().stream()
                        .allMatch(c -> c.toString().contains("T_F") || !c.toString().contains("T_")))
                .collect(Collectors.toSet());
    }

    StrategyApplier createApplier() {
        return new StrategyApplier();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscourseTaskStrategy that = (DiscourseTaskStrategy) o;
        return components.equals(that.components);
    }

    @Override
    public int hashCode() {
        return Objects.hash(components);
    }

    @Override
    public String toString() {
        return components.stream()
                .map(Enum::toString)
                .sorted()
                .collect(Collectors.joining(COMPONENT_SEPARATOR));
    }

    Set<BugReportComponent> getComponents() {
        return components;
    }

    class StrategyApplier {
        /**
         * A strategy only applies to a bug report if *each* component matches at least once.
         * We must iterate through all components to make sure the strategy applies to a bug report,
         * otherwise if we shortcut, we could miss cases in which multiple components apply to a
         * single sentence and mark the strategy as not satisfied when it actually is. The
         * applier keeps track of which components have been satisfied so far.
         */
        private final Set<BugReportComponent> satisfiedComponents = new HashSet<>();

        private StrategyApplier() {
        }

        Optional<String> apply(CodedBugReportWithTasks.Sentence sentence) {
            TextFragment textResult = TextFragment.NULL_FRAGMENT;

            for (BugReportComponent component : DiscourseTaskStrategy.this.components) {
                TextFragment extractedFragment = component.extractFrom(sentence);

                if (extractedFragment != TextFragment.NULL_FRAGMENT) {
                    satisfiedComponents.add(component);
                    textResult = textResult.merge(extractedFragment);
                }
            }

            return Optional.ofNullable(textResult.getText());
        }

        boolean isSatisfied() {
            return satisfiedComponents.containsAll(DiscourseTaskStrategy.this.components);
        }
    }
}
