package seers.disqueryreform.base;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class StrategyQueryContainer {
    private final String strategyName;
    private int totalQueries = 0;
    private Map<Project, List<DiscourseQuery>> projects = new HashMap<>();

    public StrategyQueryContainer(String strategyName) {
        this.strategyName = strategyName;
    }

    public static StrategyQueryContainer fromFile(File file) throws IOException {
        return JSON.readJSON(file.toPath(), StrategyQueryContainer.class);
    }

    public void addProject(DataSet dataSet, String projectName, List<DiscourseQuery> queries) {
        if (queries.isEmpty()) {
            return;
        }

        totalQueries += queries.size();
        projects.put(new Project(dataSet, projectName), queries);
    }

    public void addQuery(DataSet dataSet, String projectName, DiscourseQuery query) {
        totalQueries++;

        projects.computeIfAbsent(new Project(dataSet, projectName), s -> new ArrayList<>())
                .add(query);
    }

    public Set<Map.Entry<Project, List<DiscourseQuery>>> getItems() {
        return projects.entrySet();
    }

    public int getTotalQueries() {
        return totalQueries;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public Stream<DiscourseQuery> allQueries() {
        return projects.values().stream().flatMap(Collection::stream);
    }
}
