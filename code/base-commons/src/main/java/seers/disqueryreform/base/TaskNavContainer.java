package seers.disqueryreform.base;

import com.google.gson.Gson;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskNavContainer {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskNavContainer.class);

    private final Map<DataSet, Map<String, Map<String, TaskNavBugReport>>> dataSets = new HashMap<>();
    private String configuration;

    private TaskNavContainer(File originFile) {
        this.configuration = originFile.getName()
                .replace("TaskNav-", "").replace(".json", "");
    }

    @SuppressWarnings({"unchecked", "unused"})
    public static TaskNavContainer fromFile(File jsonFile) throws IOException {
        Map jsonMap;

        try (FileReader fileReader = new FileReader(jsonFile)) {
            Gson gson = GsonUtils.createDefaultGson();
            jsonMap = gson.fromJson(fileReader, Map.class);
        }

        List<Map> files = (List<Map>) jsonMap.get("files");
        TaskNavContainer container = new TaskNavContainer(jsonFile);

        for (Map file : files) {
            String name = (String) file.get("filename");

            DataSet dataSet;
            if (name.contains("CDS_reports")) {
                dataSet = DataSet.LB;
            } else if (name.contains("D4J_reports")) {
                dataSet = DataSet.D4J;
            } else if (name.contains("QQ_reports")) {
                dataSet = DataSet.QQ;
            } else if (name.contains("B4BL_reports")) {
                dataSet = DataSet.B4BL;
            } else {
                dataSet = DataSet.BRT;
            }

            String[] components = name.split("\\\\");
            String key = components[components.length - 1].replace(".xml", "");
            String system = components[components.length - 2];

            TaskNavBugReport taskNavBugReport = convertMapToBugReport(file);

            // Container will contain only bugs with tasks
            if (taskNavBugReport.isEmpty()) {
                continue;
            }

            Map<String, Map<String, TaskNavBugReport>> dataSetMap =
                    container.dataSets.computeIfAbsent(dataSet, s -> new HashMap<>());

            dataSetMap.computeIfAbsent(system, s -> new HashMap<>())
                    .put(key, taskNavBugReport);
        }

        return container;
    }

    @SuppressWarnings("unchecked")
    private static TaskNavBugReport convertMapToBugReport(Map file) {
        List<Map> sentenceMaps = new ArrayList<>(Collections.singletonList((Map) file.get("title")));
        sentenceMaps.addAll((List) file.get("description"));

        return new TaskNavBugReport(sentenceMaps);
    }

    @SuppressWarnings("WeakerAccess")
    public @Nullable
    TaskNavBugReport findBugReport(DataSet dataSet, String system, String key) {
        return dataSets.get(dataSet).get(system).get(key);
    }

    @Override
    public String toString() {
        return "TaskNavContainer{" +
                "configuration=" + configuration +
                '}';
    }

    @SuppressWarnings({"unused"})
    String getConfiguration() {
        return configuration;
    }

    @SuppressWarnings("WeakerAccess")
    public static class TaskNavBugReport {
        private final Map<String, TaskNavSentence> sentences;

        private TaskNavBugReport(List<Map> sentenceMaps) {
            sentences = sentenceMaps.stream()
                    .map(TaskNavSentence::buildSentence)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toMap(s -> s.id, Function.identity()));
        }

        public Optional<TaskNavSentence> getSentence(String sentenceID) {
            return Optional.ofNullable(sentences.get(sentenceID));
        }

        @Override
        public String toString() {
            return "TaskNavBugReport{" +
                    "#sentences=" + sentences.size() +
                    '}';
        }

        public Optional<TaskNavSentence> getTitle() {
            return getSentence(TaskNavSentence.TITLE_ID);
        }

        public boolean isEmpty() {
            return sentences.isEmpty();
        }
    }

    public static class TaskNavSentence {
        private static final String TITLE_ID = "title";

        private final String id;
        private final List<TaskNavTask> tasks = new ArrayList<>();

        private TaskNavSentence(String id, List<Map> taskWrappers) {
            this.id = id;

            for (Map taskWrapper : taskWrappers) {
                tasks.add(new TaskNavTask((Map) taskWrapper.get("task")));
            }
        }

        /**
         * Returns a sentence if the map has tasks, otherwise returns null.
         *
         * @param sentenceMap A map representing a sentence that can be a title.
         * @return A sentence object or null.
         */
        @SuppressWarnings("unchecked")
        private static TaskNavSentence buildSentence(Map sentenceMap) {
            List<Map> taskWrappers = (List<Map>) sentenceMap.get("tasks");

            if (!taskWrappers.isEmpty()) {
                return new TaskNavSentence(
                        // If the sentence map does not contain id, it means it is the title
                        (String) sentenceMap.getOrDefault("id", TaskNavSentence.TITLE_ID),
                        taskWrappers
                );
            } else {
                return null;
            }
        }

        @Override
        public String toString() {
            return "TaskNavSentence{" +
                    "id='" + id + '\'' +
                    ", #tasks=" + tasks.size() +
                    '}';
        }

        List<TaskNavTask> getTasks() {
            return tasks;
        }

    }

    public static class TaskNavTask {
        /**
         * Must be non-null.
         */
        private final String verb;

        private final String object;
        private final String preposition;

        private TaskNavTask(Map taskMap) {
            String verb = (String) taskMap.get("verb");

            if (verb == null || verb.trim().isEmpty()) {
                throw new IllegalArgumentException("Verb must be provided");
            } else {
                this.verb = verb.trim();
            }

            String object = (String) taskMap.get("accusative");
            if (object == null || object.trim().isEmpty()) {
                this.object = null;
            } else {
                this.object = object.trim();
            }

            String preposition = (String) taskMap.get("preposition");
            if (preposition == null || preposition.trim().isEmpty()) {
                this.preposition = null;
            } else {
                this.preposition = preposition.trim();
            }
        }

        @Override
        public String toString() {
            return "TaskNavTask{" +
                    "verb='" + verb + '\'' +
                    ", object='" + object + '\'' +
                    ", preposition='" + preposition + '\'' +
                    '}';
        }

        String getVerbAndObject() {
            return object == null ? verb : verb + " " + object;
        }

        String getAllText() {
            // Verb is guaranteed to be not null
            return Stream.of(verb, object, preposition)
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining(" "));
        }

        String getVerb() {
            return verb;
        }

        Optional<String> getObject() {
            return Optional.ofNullable(object);
        }
    }
}
