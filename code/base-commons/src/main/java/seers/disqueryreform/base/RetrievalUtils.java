package seers.disqueryreform.base;

import com.google.gson.Gson;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class RetrievalUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(RetrievalUtils.class);

    public static List<Query> readAllQueries(String baseFolder, DataSet dataSet) throws IOException {
        AtomicInteger queryId = new AtomicInteger(1);

        List<Query> allQueries = new ArrayList<>();
        if (DataSet.BRT.equals(dataSet) || DataSet.LB.equals(dataSet) || DataSet.B4BL.equals(dataSet)) {
            final File[] projectFolders = DataSetUtils.getBRProjectFolders(baseFolder, dataSet);
            for (File projectFolder : projectFolders) {
                final String projectName = projectFolder.getName();
                List<Query> projectQueries = readAllQueriesFromProject(projectName, projectFolder, queryId);
                allQueries.addAll(projectQueries);
            }
        }
        if (DataSet.B4BL.equals(dataSet)) {
            final File[] projectFolders = DataSetUtils.getBRProjectFolders(baseFolder, dataSet);
            for (File projectFolder : projectFolders) {
                final String projectName = projectFolder.getName();
                final File[] projectVersions = projectFolder.listFiles(File::isDirectory);
                if (projectVersions != null) {
                    for (File projectVersionFolder : projectVersions) {
                        List<Query> projectQueries = readAllQueriesFromProject(projectName,
                                projectVersionFolder, queryId);
                        allQueries.addAll(projectQueries);
                    }
                }
            }
        } else if (DataSet.D4J.equals(dataSet) || DataSet.QQ.equals(dataSet)) {
            final File baseDataSetFolder = DataSetUtils.getBaseDataSetFolder(baseFolder, dataSet);
            File queriesFolder = Paths.get(baseDataSetFolder.getAbsolutePath(), "queries_prep").toFile();
            LinkedList<File> allFiles = (LinkedList<File>) FileUtils.listFiles(queriesFolder, new String[]{"json" +
                    ".prep"}, false);

            for (File file : allFiles) {
                String fileName = file.getName();
                String system = fileName.replace(".json.prep", "");
                allQueries.addAll(readQueriesFromJsonFile(system, file));
            }

            allQueries.forEach(query -> query.setQueryId(queryId.getAndIncrement()));

        }
        return allQueries;
    }

    private static List<Query> readAllQueriesFromProject(String projectName, File projectFolder,
                                                         AtomicInteger queryId) throws IOException {
        final File queriesFile = Paths.get(projectFolder.getAbsolutePath(), "bug-reports.json").toFile();
        if (!queriesFile.exists())
            return Collections.emptyList();
        return readBugReportsAsQueriesFromJsonFile(projectName, queriesFile, queryId);
    }

    @SuppressWarnings("unchecked")
    private static List<RetrievalDoc> readCorpusFromPath(DataSet dataSet, String corpFilePath) throws IOException {
        Gson gson = new Gson();

        List<RetrievalDoc> docList = new ArrayList<>();

        try (BufferedReader inCorpus = new BufferedReader(new FileReader(corpFilePath))) {
            // add every document into the list
            String lineCorpus;
            while ((lineCorpus = inCorpus.readLine()) != null) {

                if (lineCorpus.isEmpty()) {
                    continue;
                }

                Map<String, Object> jsonVals = gson.fromJson(lineCorpus, Map.class);

                RetrievalDoc doc;
                if (DataSet.D4J.equals(dataSet) || DataSet.QQ.equals(dataSet)) {
                    doc = new RetrievalDoc(((Double) jsonVals.get("id")).intValue(),
                            jsonVals.get("text").toString(), jsonVals.get("qname").toString());
                } else {
                    doc = new RetrievalDoc(0, jsonVals.get("text").toString(),
                            jsonVals.get("file_package_path").toString());
                }

                //normalize
                doc.setDocName(normalizeText(doc.getDocName()));

                docList.add(doc);
            }
        }

        // To ensure docs are always loaded with the same ID, sort and assign IDs
        List<RetrievalDoc> sortedDocs = docList.stream()
                .sorted(Comparator.comparing(RetrievalDoc::getDocName))
                .collect(Collectors.toList());

        int docID = 1;
        for (RetrievalDoc doc : sortedDocs) {
            doc.setDocId(docID++);
        }

        return sortedDocs;
    }

    @SuppressWarnings("unchecked")
    public static List<Query> readReformulatedQueries(String strategyFolder, boolean preprocessed) throws IOException {

        File queriesDir = new File(strategyFolder);
        LinkedList<File> allFiles = (LinkedList<File>) FileUtils.listFiles(queriesDir, new String[]{preprocessed ?
                "json.prep" : "json"}, false);

        List<Query> queries = new ArrayList<>();

        for (File file : allFiles) {

            String fileName = file.getName();
            String system = fileName.replace(".json.prep", "").replace(".json", "");

            queries.addAll(readQueriesFromJsonFile(system, file));
        }

        return queries;
    }

    @SuppressWarnings("unchecked")
    public static List<Query> readReformulatedBugReports(String strategyFolder, boolean preprocessed) throws
            IOException {

        File queriesDir = new File(strategyFolder);
        LinkedList<File> allFiles = (LinkedList<File>) FileUtils.listFiles(queriesDir, new String[]{preprocessed ?
                "json.prep" : "json"}, false);

        List<Query> queries = new ArrayList<>();

        for (File file : allFiles) {

            String fileName = file.getName();
            String system = fileName.replace(".json.prep", "").replace(".json", "");

            queries.addAll(readBugReportsAsQueriesFromJsonFile(system, file));
        }

        return queries;
    }

    private static List<Query> readBugReportsAsQueriesFromJsonFile(String system, File queriesFile) throws IOException {
        List<String> lines = FileUtils.readLines(queriesFile, Charset.defaultCharset());

        List<Query> queries = new ArrayList<>();
        Gson gson = GsonUtils.createDefaultGson();
        for (String line : lines) {

            if (line.isEmpty()) {
                continue;
            }

            Map<String, Object> jsonVals = gson.fromJson(line, Map.class);
            String title = jsonVals.get("title").toString();
            String description = jsonVals.get("description").toString();

            String text = title + " " + description;

            if (text.trim().isEmpty()) {
                LOGGER.warn("Voided query: " + system + ";" + jsonVals.get("key"));
                continue;
            }

            Query query = new Query(-1, text);
            query.setKey(jsonVals.get("key").toString());
            query.addInfoAttribute("method_goldset", jsonVals.get("method_goldset"));
            query.addInfoAttribute("class_goldset", jsonVals.get("fixed_classes"));
            query.addInfoAttribute("file_goldset", jsonVals.get("fixed_files"));
            query.addInfoAttribute("system", system);

            queries.add(query);
        }

        return queries;
    }

    private static List<Query> readBugReportsAsQueriesFromJsonFile(String system, File queriesFile, AtomicInteger
            queryId) throws
            IOException {
        List<String> lines = FileUtils.readLines(queriesFile, Charset.defaultCharset());

        List<Query> queries = new ArrayList<>();
        Gson gson = GsonUtils.createDefaultGson();
        for (String line : lines) {

            if (line.isEmpty()) {
                continue;
            }

            Map<String, Object> jsonVals = gson.fromJson(line, Map.class);
            String title = jsonVals.get("title").toString();
            String description = jsonVals.get("description").toString();

            String text = title + " " + description;

            if (text.trim().isEmpty()) {
                LOGGER.warn("Voided query: " + system + ";" + jsonVals.get("key"));
                continue;
            }

            Query query = new Query(queryId.getAndIncrement(), text);
            query.setKey(jsonVals.get("key").toString());
            query.addInfoAttribute("method_goldset", jsonVals.get("method_goldset"));
            query.addInfoAttribute("class_goldset", jsonVals.get("fixed_classes"));
            query.addInfoAttribute("file_goldset", jsonVals.get("fixed_files"));
            query.addInfoAttribute("system", system);

            queries.add(query);
        }

        return queries;
    }

    private static List<Query> readQueriesFromJsonFile(String system, File queriesFile) throws IOException {
        List<String> lines = FileUtils.readLines(queriesFile, Charset.defaultCharset());

        List<Query> queries = new ArrayList<>();
        Gson gson = GsonUtils.createDefaultGson();
        for (String line : lines) {

            if (line.isEmpty()) {
                continue;
            }

            Map<String, Object> jsonVals = gson.fromJson(line, Map.class);

            Object object = jsonVals.get("id");
            Double qId = object instanceof Double ? (Double) object : Double.valueOf(object.toString());

            String text = jsonVals.get("text").toString();

            if (text.trim().isEmpty()) {
                LOGGER.warn("Voided query: " + system + ";" + jsonVals.get("bug_id"));
                continue;
            }

            Query query = new Query(qId.intValue(), text);
            query.setKey(jsonVals.get("bug_id").toString());
            query.addInfoAttribute("method_goldset", jsonVals.get("method_goldset"));
            query.addInfoAttribute("class_goldset", jsonVals.get("class_goldset"));
            query.addInfoAttribute("file_goldset", jsonVals.get("file_goldset"));
            query.addInfoAttribute("system", system);

            queries.add(query);
        }

        return queries;
    }

    private static String getCorpusFilePath(String corpusFolder, DataSet dataSet, String project) {

        if (DataSet.D4J.equals(dataSet) || DataSet.QQ.equals(dataSet))
            return corpusFolder + File.separator + project + ".json.prep";

        if (DataSet.LB.equals(dataSet) || DataSet.BRT.equals(dataSet))
            return corpusFolder + File.separator + project + File.separator + "source-code.json";

        return null;
    }

    /**
     * Documents are always loaded with the same ID.
     *
     * @param existingDataSetsPath Path.
     * @param dataSet DataSet.
     * @param project Project.
     * @param query Only for D4J and B4BL.
     * @return The corpus.
     * @throws IOException If reading fails.
     */
    @SuppressWarnings("unused")
    public static List<RetrievalDoc> readCorpus(Path existingDataSetsPath, DataSet dataSet,
                                                String project, DiscourseQuery query)
            throws IOException {

        List<RetrievalDoc> queryCorpus;
        String corpusBaseFolder = dataSet.resolveCorpusPath(existingDataSetsPath).toString();

        //d4j: corpus per query
        if (DataSet.D4J.equals(dataSet)) {
            queryCorpus = RetrievalUtils.readCorpusFromPath(dataSet,
                    corpusBaseFolder + File.separator + project + "_" + query.getKey() + ".json.prep");
        } else if (DataSet.B4BL.equals(dataSet)) {

            final String bugId = query.getKey();
            String projectVersion = getProjectVersion(corpusBaseFolder, bugId, project);
            String corpFilePath = Paths.get(corpusBaseFolder,
                    projectVersion, "source-code.json").toString();
            queryCorpus = RetrievalUtils.readCorpusFromPath(dataSet, corpFilePath);
        } else {
            //other data sets: corpus per system
            LOGGER.info("Reading corpus for system: " + project);
            String corpFilePath = RetrievalUtils.getCorpusFilePath(corpusBaseFolder, dataSet, project);
            queryCorpus = RetrievalUtils.readCorpusFromPath(dataSet, corpFilePath);
        }

        return queryCorpus;
    }

    private static String getProjectVersion(String corpusBaseFolder, String bugId, String project) throws IOException {
        String filePath = Paths.get(corpusBaseFolder, "bug-reports-versions-" + project + ".csv").toString();
        final List<List<String>> lines = CSVHelper.readCsv(filePath, false, CSVHelper.DEFAULT_SEPARATOR);
        final Optional<List<String>> first = lines.stream().filter(l -> bugId.equals(l.get(1))).findFirst();
        return first.map(strings -> strings.get(0)).orElse(null);
    }


    public static RelJudgment getRelevantJudgement(DataSet dataSet, Query query, List<RetrievalDoc> queryCorpus) {

        String project = (String) query.getInfoAttribute("system");
        String projectBugId = project + ";" + query.getKey();

        List<String> goldSet = readGoldSet(dataSet, query);

        List<RetrievalDoc> relevantDocs = queryCorpus.stream().filter(d -> goldSet.contains(d.getDocName()))
                .collect(Collectors.toList());

        if (relevantDocs.size() != goldSet.size()) {
            LOGGER.warn("The # of gold set docs is different than the # of matching docs from the " +
                    "corpus: <" + projectBugId + "> (" + goldSet.size() + " vs " + relevantDocs.size() + ") -> " +
                    relevantDocs);
        }

        RelJudgment relJudg = new RelJudgment();
        relJudg.setRelevantDocs(relevantDocs);
        return relJudg;
    }

    public static List<String> readGoldSet(DataSet dataSet, Query query) {
        List<String> goldSet;
        if (DataSet.D4J.equals(dataSet) || DataSet.QQ.equals(dataSet)) {
            goldSet = (List<String>) query.getInfoAttribute("method_goldset");
        } else if (DataSet.LB.equals(dataSet)) {
            goldSet = (List<String>) query.getInfoAttribute("class_goldset");
        } else {
            goldSet = (List<String>) query.getInfoAttribute("file_goldset");
        }

        //normalize
        for (int i = 0; i < goldSet.size(); i++) {
            final String gs = goldSet.get(i);
            goldSet.set(i, normalizeText(gs));
        }

        return goldSet;
    }

    private static String normalizeText(String text) {
        return text.replace("$", ".");
    }

}
