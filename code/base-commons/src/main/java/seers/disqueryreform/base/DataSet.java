package seers.disqueryreform.base;

import seers.appcore.csv.CSVHelper;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum DataSet {
    BRT(Paths.get("buglocator_data"), Paths.get("buglocator_data")),
    D4J(Paths.get("defects4j_data", "src_code_corpus_prep"), Paths.get("defects4j_data", "sources")),
    LB(Paths.get("lobster_data"), Paths.get("lobster_data")),
    QQ(Paths.get("query_quality_data", "query_quality_code_corpus_prep"), Paths.get("query_quality_data", "sources")),
    B4BL(Paths.get("bench4bl_data"), Paths.get("bench4bl_data"));

    /**
     * Corpus path relative to the "existing_data_sets" directory.
     */
    private final Path corpusPath;
    private final Path sourcesPath;

    DataSet(Path corpusPath, Path sourcesPath) {
        this.corpusPath = corpusPath;
        this.sourcesPath = sourcesPath;
    }

    public static Map<String, String> loadB4BLIDToProjectMap(Path existingDataSetsPath) {
        Path corpusFolderPath = B4BL.resolveCorpusPath(existingDataSetsPath);

        /* Here we must load every query and get the full system for it (with version) */
        return Arrays.stream(corpusFolderPath.toFile().listFiles(f -> f.getName().startsWith("bug-reports-versions-")))
                .flatMap(f -> {
                    try {
                        return CSVHelper.readCsv(f.getAbsolutePath(), false, ';').stream();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .collect(Collectors.toMap(l -> l.get(1), l -> l.get(0)));
    }

    public Path resolveCorpusPath(Path existingDataSetsPath) {
        return existingDataSetsPath.resolve(corpusPath);
    }

    public Path resolveProjectSourcesPath(Path dataSetsDir, String project, String corpus) {
        Path second;
        switch (this) {
            case QQ:
                second = sourcesPath.resolve(corpus);
                break;
            case B4BL:
            case BRT:
            case LB:
                second = sourcesPath.resolve(Paths.get(corpus, "sources"));
                break;
            case D4J:
                second = sourcesPath.resolve(Paths.get(project, corpus));
                break;
            default:
                throw new RuntimeException("Unknown data set " + this);
        }

        return dataSetsDir.resolve(second);
    }
}
