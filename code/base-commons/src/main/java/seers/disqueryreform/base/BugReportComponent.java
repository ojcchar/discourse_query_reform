package seers.disqueryreform.base;

import seers.disqueryreform.base.TextFragment.TextFragmentType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public enum BugReportComponent {
    // Normal components
    TITLE(CC.IS_TITLE, TextFragmentType.FULL_TEXT),
    OB(CC.HAS_OB, TextFragmentType.FULL_TEXT),
    EB(CC.HAS_EB, TextFragmentType.FULL_TEXT),
    S2R(CC.HAS_S2R, TextFragmentType.FULL_TEXT),
    CODE(CodedBugReportWithTasks.Sentence::isCode, TextFragmentType.FULL_TEXT),
    OTHER(CC.IS_OTHER, TextFragmentType.FULL_TEXT),

    // Full tasks (verb, object and preposition)
    T_F_TITLE(CC.IS_TITLE, TextFragmentType.FULL_TASKS),
    T_F_OB(CC.HAS_OB, TextFragmentType.FULL_TASKS),
    T_F_EB(CC.HAS_EB, TextFragmentType.FULL_TASKS),
    T_F_S2R(CC.HAS_S2R, TextFragmentType.FULL_TASKS),
    T_F_OTHER(CC.IS_OTHER, TextFragmentType.FULL_TASKS),

    // Task verb only
    // Since all tasks are guaranteed to have a verb, checking for tasks suffices
    T_V_TITLE(CC.IS_TITLE, TextFragmentType.TASK_VERBS),
    T_V_OB(CC.HAS_OB, TextFragmentType.TASK_VERBS),
    T_V_EB(CC.HAS_EB, TextFragmentType.TASK_VERBS),
    T_V_S2R(CC.HAS_S2R, TextFragmentType.TASK_VERBS),
    T_V_OTHER(CC.IS_OTHER, TextFragmentType.TASK_VERBS),

    // Task object only
    T_O_TITLE(CC.IS_TITLE, TextFragmentType.TASK_OBJECTS),
    T_O_OB(CC.HAS_OB, TextFragmentType.TASK_OBJECTS),
    T_O_EB(CC.HAS_EB, TextFragmentType.TASK_OBJECTS),
    T_O_S2R(CC.HAS_S2R, TextFragmentType.TASK_OBJECTS),
    T_O_OTHER(CC.IS_OTHER, TextFragmentType.TASK_OBJECTS),

    // Task verb and object
    // Since all tasks are guaranteed to have a verb, checking for tasks suffices
    T_VO_TITLE(CC.IS_TITLE, TextFragmentType.TASK_VERBS_AND_OBJECTS),
    T_VO_OB(CC.HAS_OB, TextFragmentType.TASK_VERBS_AND_OBJECTS),
    T_VO_EB(CC.HAS_EB, TextFragmentType.TASK_VERBS_AND_OBJECTS),
    T_VO_S2R(CC.HAS_S2R, TextFragmentType.TASK_VERBS_AND_OBJECTS),
    T_VO_OTHER(CC.IS_OTHER, TextFragmentType.TASK_VERBS_AND_OBJECTS);

    static final Set<BugReportComponent> DISCOURSE_COMPONENTS = new HashSet<>();
    static final Set<BugReportComponent> TASK_COMPONENTS = new HashSet<>();

    private static Predicate<BugReportComponent> isTask = s -> s.toString().startsWith("T_");

    static {

        DISCOURSE_COMPONENTS.addAll(Arrays.stream(values())
                .filter(isTask.negate())
                .filter(c -> c != OTHER)
                .collect(Collectors.toList()));

        TASK_COMPONENTS.addAll(Arrays.stream(values())
                .filter(isTask)
                .collect(Collectors.toList()));
    }

    private TextFragmentType extractor;
    private Predicate<CodedBugReportWithTasks.Sentence> checker;

    BugReportComponent(Predicate<CodedBugReportWithTasks.Sentence> checker,
                       TextFragmentType extractor) {
        this.checker = checker;
        this.extractor = extractor;
    }

    public static boolean isTaskComponent(BugReportComponent component) {
        return component.toString().startsWith("T_");
    }

    /**
     * Base component refers to the original 5 types: TITLE, OB, CODE, etc. and including OTHER.
     *
     * @param components To group.
     * @return Grouped components.
     */
    static List<Set<BugReportComponent>> groupByBaseComponent(Set<BugReportComponent> components) {
        return components.stream()
                .collect(Collectors.groupingBy(c -> {
                    String cName = c.toString();
                    return extractBaseComponentName(cName);
                }))
                .values().stream()
                .map(HashSet::new)
                .collect(Collectors.toList());
    }

    private static String extractBaseComponentName(String cName) {
        String[] splitName = cName.split("_");

        return splitName[splitName.length - 1];
    }

    /**
     * The fragment will be null if the component does not apply or if it does apply but
     * there is no text to extract (e.g. the sentence has tasks but none of the tasks have
     * objects). In either of these two cases the component is not satisfied.
     *
     * @param sentence A sentence from which the component should be extracted.
     * @return A text fragment containing text or NULL_FRAGMENT.
     */
    public TextFragment extractFrom(CodedBugReportWithTasks.Sentence sentence) {
        if (checker.test(sentence)) {
            return extractor.extractFrom(sentence);
        } else {
            return TextFragment.NULL_FRAGMENT;
        }
    }

    public boolean hasSameGroup(String componentName) {
        return extractBaseComponentName(this.toString()).equals(extractBaseComponentName(componentName));
    }
}

/**
 * Component Checker class. Must be used so that the functions can be passed as constructor
 * arguments to the enum constants.
 */
class CC {
    static final Predicate<CodedBugReportWithTasks.Sentence> IS_TITLE =
            CodedBugReportWithTasks.Sentence::isTitle;

    static final Predicate<CodedBugReportWithTasks.Sentence> HAS_OB =
            CodedBugReportWithTasks.Sentence::hasOB;

    static final Predicate<CodedBugReportWithTasks.Sentence> HAS_EB =
            CodedBugReportWithTasks.Sentence::hasEB;

    static final Predicate<CodedBugReportWithTasks.Sentence> HAS_S2R =
            CodedBugReportWithTasks.Sentence::hasS2R;

    static final Predicate<CodedBugReportWithTasks.Sentence> IS_OTHER =
            CodedBugReportWithTasks.Sentence::isOther;
}

