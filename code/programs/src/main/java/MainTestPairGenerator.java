import seers.appcore.csv.CSVHelper;
import seers.appcore.utils.JavaUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class MainTestPairGenerator {


    public static void main(String[] args) throws IOException {
        final String filePath = "C:\\Users\\ojcch\\Documents\\Projects\\Task-based_query_reformulation" +
                "\\strategies_pairs\\new_strategies.csv";

        List<List<String>> data = CSVHelper.readCsv(filePath, false);
        Map<Set<String>, String> strategies = data.stream()
                .map(r -> r.get(0))
                .collect(Collectors.toMap(r -> JavaUtils.getSet(r.split("\\*")), r -> r));


       /* Set<Map.Entry<Set<String>, String>> onlyNoOther =
                strategies.entrySet().stream().filter(s -> !s.getKey().contains("T_OTHER")).collect(Collectors.toSet());
        getOtherPairs(strategies, onlyNoOther);*/

        Set<Map.Entry<Set<String>, String>> onlyNonTasks =
                strategies.entrySet().stream().filter(s -> !s.getValue().contains("T_") && !s.getKey().contains(
                        "OTHER")).collect(Collectors.toSet());

        System.out.println(onlyNonTasks);


        final List<List<String>> otherPairs = getOtherPairs(strategies, onlyNonTasks);


        //-----------------------------------------------


        final List<List<String>> taskPairs = getTaskPairs(Arrays.asList("TITLE", "OB", "EB", "S2R"), strategies);
        final List<List<String>> taskPairs2 = getTaskPairs(Collections.singletonList("OTHER"), strategies);


        String otherFile = "C:\\Users\\ojcch\\Documents\\Projects\\Task-based_query_reformulation\\t_other_strategies" +
                ".csv";
        String tasksFile = "C:\\Users\\ojcch\\Documents\\Projects\\Task-based_query_reformulation\\task_strategies" +
                ".csv";
        String tasksFile2 = "C:\\Users\\ojcch\\Documents\\Projects\\Task-based_query_reformulation" +
                "\\task_strategies_other" +
                ".csv";
        List<String> header = Arrays.asList("strat1", "strat2");
        CSVHelper.writeCsv(otherFile, header, otherPairs);
        CSVHelper.writeCsv(tasksFile, header, taskPairs);
        CSVHelper.writeCsv(tasksFile2, header, taskPairs2);

        /*System.out.println(strategies);
        System.out.println(onlyNoOther);*/
    }

    private static List<List<String>> getTaskPairs(List<String> infoTypes, Map<Set<String>, String> strategies) {

        List<List<String>> map = new ArrayList<>();

        for (String infoType : infoTypes) {

            final Set<Map.Entry<Set<String>, String>> strats =
                    strategies.entrySet().stream()
                            .filter(s -> s.getKey().contains(infoType))
                            .collect(Collectors.toSet());

            System.out.println(strats.size());

            for (Map.Entry<Set<String>, String> strategy : strats) {

                final Set<String> components = strategy.getKey();
                List<String> otherComponents = new ArrayList<>(components);
                final int i = otherComponents.indexOf(infoType);
                otherComponents.set(i, "T_" + infoType);


                final String otherStrategy = strategies.get(new HashSet<>(otherComponents));

                if (otherStrategy == null) throw new RuntimeException();


                map.add(Arrays.asList(strategy.getValue(), otherStrategy));
//                System.out.println(strategy.getValue() + ";" + otherStrategy);

            }
        }

        return map;
    }

    private static List<List<String>> getOtherPairs(Map<Set<String>, String> strategies,
                                                    Set<Map.Entry<Set<String>, String>> onlyNoOther) {

        List<List<String>> map = new ArrayList<>();

        for (Map.Entry<Set<String>, String> strategy : onlyNoOther) {

            final Set<String> components = strategy.getKey();
            Set<String> otherComponents = new HashSet<>(components);
            otherComponents.add("T_OTHER");

            final String strategyWithOther = strategies.get(otherComponents);
            if (strategyWithOther == null) throw new RuntimeException(strategy.getValue());

            map.add(Arrays.asList(strategy.getValue(), strategyWithOther));
            // System.out.println(strategy.getValue() + ";" + strategyWithOther);

        }

        return map;
    }
}
