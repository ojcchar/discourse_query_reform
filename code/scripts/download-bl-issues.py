#!/usr/bin/env python3

"""
Description
"""


import argparse
import os
import json
import sys
from bs4 import BeautifulSoup as bs
from http.client import HTTPConnection as HTTPC, HTTPSConnection as HTTPSC


def download(lines):
    url = 'bugs.eclipse.org'
    c = HTTPSC(url)

    for line in lines:
        bug = json.loads(line)
        key = bug['key']
        c.request('GET', '/bugs/show_bug.cgi?id=%s' % key)
        html = bs(c.getresponse().read())

        title = html.find(id='short_desc_nonedit_display')
        if not title:
            print('WARNING: No title found for bug', key, file=sys.stderr)
        else:
            bug['title'] = title.text

        desc = html.find(id='c0').find('pre')
        if not desc:
            print('WARNING: No description found for bug', key, file=sys.stderr)
        else:
            bug['description'] = desc.text

        yield bug


def main():
    for bug in download(sys.stdin):
        print(json.dumps(bug))


if __name__ == '__main__':
##    parser = argparse.ArgumentParser()
##    parser.add_argument('bugs_dir', help='Directory with original bug reports')
##
##    args = parser.parse_args()

    main()#args.bugs_dir)
