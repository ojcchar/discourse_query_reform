##---------
library("dplyr")
library("ggpubr")
library("effsize")
library(doParallel)

##---------------
#read the data

conf_level = 0.95


  #==========================================================
  #
  # PARAM 1: all-stats-combined file
  #
  #==========================================================

data_file = "/home/juan/Source/discourse_query_reform/results/2020-04-30-ase-eval/stat-tests/all-stats-combined.csv"

  #==========================================================
all_data_set = read.csv(data_file, sep = ",", header = TRUE)


# 'new' is combinatorial and 'old' is conjuctive
all_data_set_new = subset(all_data_set, type=="new")
all_data_set_old = subset(all_data_set, type=="old")


attach(all_data_set)
all_data_set <- all_data_set[order(type, Technique, Strategy, Granularity, N),]
detach(all_data_set)


all_data_set = all_data_set[,c(1:6, 9, 11, 13, 15, 17, 19)]
colnames(all_data_set) <- c("Technique", "Strategy", "type", "Config", "Granularity", "N", "hits_reform", "hits_improv", "map_reform", "map_improv","mrr_reform", "mrr_improv")

#----------------------

  #==========================================================
  #
  # PARAM 2: pairs file
  #
  #==========================================================

data_file_pairs = "/home/juan/Source/discourse_query_reform/results/2020-04-30-ase-eval/stat-tests/pairs/5_blizzard.csv"

  #==========================================================
  
  
pairs = read.csv(data_file_pairs, sep = ",", header = TRUE)

#----------------------

#set the cluster
cl <- makeCluster(8, outfile="/tmp/log.txt")
registerDoParallel(cl)

results = foreach (i=1:nrow(pairs), .combine = rbind, .packages = c('dplyr',  'ggpubr', 'effsize')) %dopar% {
  
  strategy1 =  as.character(pairs[i,1])
  strategy2 =  as.character(pairs[i,2])
  
  # CHANGE here only for conj vs comb
  
  # old vs new
  #data1 = subset(all_data_set_old, Strategy== strategy1)
  #data2 = subset(all_data_set_new, Strategy== strategy2)
  
  # other and tasks
  data1 = subset(all_data_set_new, Strategy== strategy1)
  data2 = subset(all_data_set_new, Strategy== strategy2)
  
  # the # of rows of the merge should be 42
  merged_d = merge(data1, data2, by =c("Technique", "Granularity", "N"))
  n_vals = nrow(merged_d)
  
  
  #==========================================================
  #
  # PARAM 3: METRIC
  #
  #==========================================================
  
  #vals1 = merged_d$Reform.HITS_PERCENTAGE.x;
  #vals2 = merged_d$Reform.HITS_PERCENTAGE.y;
  
  #vals1 = merged_d$Reform.MRR.x;
  #vals2 = merged_d$Reform.MRR.y;
  
  vals1 = merged_d$Reform.MAP.x;
  vals2 = merged_d$Reform.MAP.y;
  
  #==========================================================
  
  
  wil = wilcox.test(vals1, vals2, 
                    paired=TRUE, alternative="less", conf.level = conf_level)
  
  #wil
  
  
  w_stat = wil$statistic
  p_val = wil$p.value
  #cat(p_val, sep = "\n")
  
  p_val_intr = ""
  if(is.numeric(p_val) && !is.na(p_val)){
    p_val_intr = "NR: (Ho: C1 >= C2)"
    if(p_val < (1-conf_level)){
      p_val_intr = "R: (Ho: C1 >= C2)"
    }
  }
  
  
  d = cliff.delta(vals1, vals2)
  d_estimate = d$estimate
  d_magnitude = d$magnitude
  
  avg1 = mean(vals1)
  avg2 = mean(vals2)
  diff = avg2 - avg1
  improv = (avg2 - avg1)/avg1
  
  
  
  #==========================================================
  #
  # PARAM 4: METRIC IMPROVEMENT
  #
  #==========================================================
  
  #avg3 = mean(merged_d$Rel.Imp.HITS_PERCENTAGE.x);
  #avg4 = mean(merged_d$Rel.Imp.HITS_PERCENTAGE.y);
  
  #avg3 = mean(merged_d$Rel.Imp.MRR.x);
  #avg4 = mean(merged_d$Rel.Imp.MRR.y);
  
  avg3 = mean(merged_d$Rel.Imp.MAP.x);
  avg4 = mean(merged_d$Rel.Imp.MAP.y);
  
  #==========================================================
  
  
  
  c(strategy1, strategy2, n_vals, w_stat, p_val, p_val_intr, d_estimate, as.character(d_magnitude), avg1, avg2, diff, avg3, avg4, improv)
  
}

colnames(results) <-  c( "C1", "C2", "#_data_points", "w_stat", "p_value", "interpretation", "cliff_delta", "cliff_magnitude", "avg_metric_C1", "avg_metric_C2", "diff_avg_metric",
                         "avg_met_improv_C1", "avg_met_improv_C2", "improv_metric_str")
results


  #==========================================================
  #
  # PARAM 5: OUTPUT
  #
  #==========================================================

write.table(file = "/home/juan/Source/discourse_query_reform/results/2020-04-30-ase-eval/stat-tests/15_blizzard_map.csv", results, sep=";", row.names = FALSE)



#shutdown the cluster
stopCluster(cl)
