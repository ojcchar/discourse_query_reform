data_file = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/buglocator-all-hard-to-retrieve-queries.csv"
data  = read.csv(data_file, sep = ",", header = FALSE)  
colnames(data) <- c("system", "bug_id", "rank")

out_file = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/buglocator-sample-hard-to-retrieve-queries.csv"
file.remove(out_file)

systems = c("aspectj-1.5.3", "eclipse-3.1", "swt-3.1")
num_instances = 174

for (sys in systems) {
  
  system_data = subset(data, system == sys)
  
  sample_data =system_data
  if (nrow(sample_data) >num_instances ){
    sample_data = sample_data[sample(1:nrow(sample_data), num_instances),]
  }
  write.table(sample_data, out_file, append=TRUE, sep=",", row.names = FALSE, col.names = FALSE)
 
}