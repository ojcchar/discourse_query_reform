#!/usr/bin/env python3

"""
Description
"""


import argparse
import json
import sys


def printerr(*messages, **kwargs):
    if 'file' in kwargs:
        del kwargs['file']
    print(*messages, file=sys.stderr, **kwargs)


def main(full_bugs_path, bug_corpus_path):
    # Load product-component pairs for every bug
    pc_lookup = {}
    with open(full_bugs_path) as full_bugs:
        for bug in json.load(full_bugs):
            pc_lookup[str(bug['id'])] = (bug['product'], bug['component'])

    output_bugs = {}
    with open(bug_corpus_path) as corpus_file:
        for bug in map(json.loads, corpus_file):
            bug_pc = pc_lookup[bug['key']]
            output_bugs.setdefault(bug_pc, []).append(bug)

    for pc, bugs in output_bugs.items():
        with open('%s-%s.json' % pc, 'w') as out_file:
            for bug in bugs:
                print(json.dumps(bug), file=out_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('full_bugs_path', help='JSON file with full bugzilla '
                        'bugs')
    parser.add_argument('bug_corpus_path', help='JSON lines file with bug '
                        'corpus')

    args = parser.parse_args()

    main(args.full_bugs_path, args.bug_corpus_path)
