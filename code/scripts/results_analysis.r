
rm(list=ls())
library(plyr)


#data_file= "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/d4j_gen_auto_coding/overall-stats2.csv"
tr_approach <<- "brtracer"
data_file <<- paste("C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/buglocator-sample-hard-to-retrieve/",tr_approach, "/overall-stats-or.csv", sep ="" )
list_of_bugs <<- "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/all-hard-to-retrieve-queries-b10.csv"
data_set <<- "brt"
#baseline ="OB"
 baseline  <<- "ALL_TEXT"
 #strategies  = c("OB", "EB", "S2R", "OTHER", "OB_EB", "OB_S2R", "EB_S2R", "OB_EB_S2R", "OB_OTHER", "EB_OTHER", "S2R_OTHER", "OB_EB_OTHER", "OB_S2R_OTHER", "EB_S2R_OTHER")
strategies  <<- c("OB")
#strategies  = c("EB_S2R_OTHER")
#baseline ="ALL_TEXT_BS"
#strategies  = c("ALL_TEXT",  "OB", "EB", "S2R", "OTHER", "OB_EB", "OB_S2R", "EB_S2R", "OB_EB_S2R", "OB_OTHER", "EB_OTHER", "S2R_OTHER", "OB_EB_OTHER", "OB_S2R_OTHER", "EB_S2R_OTHER")
#strategies  = c("OB_OTHER")
out_folder <<- "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/analysis"


compute_systems <<- FALSE
remove_outliers <<- FALSE
remove_outliers2 <<- remove_outliers


#--------------------------------------------------------------

categorize_ranks <- function(data2) {
  
  if (nrow(data2) > 0) {
    
    data2[,"rank_cat"] <- NA
    if(nrow(subset(data2,rank >=1 & rank <=10)) > 0){
      data2[data2$rank >=1 & data2$rank <=10, ]$rank_cat = "1-10"
    }
    if(nrow(subset(data2,rank >=11 & rank <=20)) > 0){
      data2[data2$rank >=11 & data2$rank <=20, ]$rank_cat = "11-20"
    }
    
    if(nrow(subset(data2,rank >=21 & rank <=30)) > 0){
      data2[data2$rank >=21 & data2$rank <=30, ]$rank_cat = "21-30"
    }
    if(nrow(subset(data2,rank >= 31)) > 0){
      data2[data2$rank >=31, ]$rank_cat = "31-"
    }
  
  }else{
    data2$rank_cat = numeric(0)
  }
  
  return(data2)
}


categorize_ranks2 <- function(data2) {
  
  if (nrow(data2) > 0) {
    
    data2[,"rank_cat"] <- NA
    if(nrow(subset(data2,rank ==1)) > 0){
      data2[data2$rank ==1, ]$rank_cat = "1"
    }
    if(nrow(subset(data2,rank >=2 & rank <=5)) > 0){
      data2[data2$rank >=2 & data2$rank <=5, ]$rank_cat = "2-5"
    }
    if(nrow(subset(data2,rank >=6 & rank <=10)) > 0){
      data2[data2$rank >=6 & data2$rank <=10, ]$rank_cat = "6-10"
    }
    if(nrow(subset(data2,rank >=11 & rank <=20)) > 0){
      data2[data2$rank >=11 & data2$rank <=20, ]$rank_cat = "11-20"
    }
    
    if(nrow(subset(data2,rank >=21 & rank <=30)) > 0){
      data2[data2$rank >=21 & data2$rank <=30, ]$rank_cat = "21-30"
    }
    if(nrow(subset(data2,rank >=31 & rank <=50)) > 0){
      data2[data2$rank >=31 & data2$rank <=50, ]$rank_cat = "31-50"
    }
    if(nrow(subset(data2,rank >= 51)) > 0){
      data2[data2$rank >=51, ]$rank_cat = "51-"
    }
    
  }else{
    data2$rank_cat = numeric(0)
  }
  
  return(data2)
}

#--------------------------------------------------------------


remove_outliers_fn <- function(data4, below_rank, above_rank){
  
  
  data_no_outliers = data4
  
  if (remove_outliers) {
    sub_set = subset(data4, rank.x>below_rank & type_rd.y != 0 )
    if (!is.null(above_rank)) {
      sub_set = subset(sub_set, rank.x<=above_rank )
    }
    if(nrow(sub_set) > 0){
      box = boxplot(sub_set$rank_diff.y)
      bounds = box$stats[c(1, 5)]
      data_no_outliers = subset(data4, rank_diff.y >= bounds[1] & rank_diff.y <=bounds[2] )
    }
  }
  
  if (remove_outliers2) {
    data_no_outliers = subset(data_no_outliers, rank.y > 2 )
  }
  
  # else{
  #   #min = min(sub_set$rank_diff.y)
  #   #max = max(sub_set$rank_diff.y)
  #   bounds = c(min(sub_set$rank_diff.y), max(sub_set$rank_diff.y))
  # }
  
  return (data_no_outliers)
}


#--------------------------

improv_catgs = data.frame(from =c("11-20", "21-30", "31-",  "21-30", "31-",   "31-",   "11-20", "21-30", "31-"), 
                          to =  c("1-10",  "1-10",  "1-10", "11-20", "11-20", "21-30", "11-20", "21-30", "31-"))
deter_catgs = data.frame(from =c("11-20", "11-20", "21-30", "11-20", "21-30", "31-"), 
                         to =  c("21-30", "31-",   "31-",   "11-20", "21-30", "31-"))


improv_catgs2 = data.frame(from =c("11-20", "21-30", "31-50", "51-", "11-20","21-30", "31-50",  "51-", "11-20", "21-30", "31-50",  "51-",  "21-30", "31-50", "51-",   "31-50", "51-",   "51-" ,  "11-20", "21-30", "31-50",  "51-" ), 
                          to =  c("1",     "1"   ,  "1",     "1",   "2-5",   "2-5",   "2-5",   "2-5", "6-10",  "6-10",  "6-10",   "6-10", "11-20", "11-20", "11-20", "21-30", "21-30", "31-50", "11-20", "21-30", "31-50",  "51-"))
deter_catgs2 = data.frame(from =c("11-20", "11-20", "11-20", "21-30", "21-30", "31-50", "11-20", "21-30", "31-50",  "51-"), 
                         to =  c("21-30", "31-50", "51-",   "31-50", "51-",   "51-",   "11-20", "21-30", "31-50",  "51-"))


#--------------------------------------------------------------------------

print_results <- function(out_file){
  
  write(c("Baseline stats"), out_file, sep=";", append = TRUE)
  cat(c("system", "#_queries", "#_queries_retr", "#_top1_queries_retr"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  write(str_base_stats_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  cat(c("system", "strategy", "#_queries_str", "#_queries_retr_str", "#_queries_str_below", "#_queries_retr_str_below"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_stats_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  write(c("performance considering only the queries having the info"), out_file, sep=";", append = TRUE)
  cat(c("system", "strategy", "rank(s)", "#_queries_b_rank","#_queries_b_rank_voided", "mrr_base", "mrr_strat", "mrr_diff", "mrr_improv", "map_base", "map_strat", "map_diff", "map_improv"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_mrr_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("performance considering empty queries"), out_file, sep=";", append = TRUE)
  cat(c("system", "strategy", "rank(s)", "#_queries_b_rank", "mrr_base", "mrr_strat", "mrr_diff", "mrr_improv", "map_base", "map_strat", "map_diff", "map_improv"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_mrr_string_all, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  write(c("# of queries and improv/deter size"), out_file, sep=";", append = TRUE)
  cat(c("system", "strategy", "rank(s)", "#_improv", "#_deter", "diff", "improv_size_avg", "improv_size_med", "deter_size_avg", "deter_size_med", "improv_rd_avg", "improv_rd_med", "deter_rd_avg", "deter_rd_med", "num_same", "%_improv", "%_deter", "%_same" ), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_imp_det_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  str_imp_cat_header=""
  
  str_imp_cat_header = paste(str_imp_cat_header, paste("system",  "strategy", "rank(s)", sep = ";"), sep =  "")
  for (cat_to in unique(improv_catgs$to)) {
    
    c_from = "*"
    c_to =paste( "[",as.character(cat_to),"]",  sep = "" )
    
    str_imp_cat_header = paste(str_imp_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_imp_cat_header = paste(str_imp_cat_header, "total_to", sep = ";")
  
  for (cat_from in unique(improv_catgs$from)) {
    
    c_from = paste("[", as.character(cat_from),"]",  sep = "" )
    c_to ="*"
    
    str_imp_cat_header = paste(str_imp_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_imp_cat_header = paste(str_imp_cat_header, "total_from", sep = ";")
  str_imp_cat_header = paste(str_imp_cat_header, "above_rank", sep = ";")
  
  for (idx in 1:nrow(improv_catgs)) {
    
    c_from = paste("[", as.character(improv_catgs[idx, 1]),"]",  sep = "" )
    c_to =paste( "[",as.character(improv_catgs[idx, 2]),"]",  sep = "" )
    
    str_imp_cat_header = paste(str_imp_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_imp_cat_header = paste(str_imp_cat_header, "\n", sep =  "")
  
  write(c("# improved queries"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg rank diff (improv queries)"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_avg_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med rank diff (improv queries)"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_med_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg improvement size"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_avg_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med improvement size"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_med_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  
  str_det_cat_header=""
  
  str_det_cat_header = paste(str_det_cat_header, paste( "system", "strategy", "rank(s)", sep = ";"), sep =  "")
  
  for (cat_from in unique(deter_catgs$from)) {
    
    c_from = paste("[", as.character(cat_from),"]",  sep = "" )
    c_to ="*"
    
    str_det_cat_header = paste(str_det_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_det_cat_header = paste(str_det_cat_header, "total_from", sep = ";")
  
  for (cat_to in unique(deter_catgs$to)) {
    
    c_from = "*"
    c_to =paste( "[",as.character(cat_to),"]",  sep = "" )
    
    str_det_cat_header = paste(str_det_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_det_cat_header = paste(str_det_cat_header, "total_to", sep = ";")
  
  for (idx in 1:nrow(deter_catgs)) {
    
    c_from = paste("[", as.character(deter_catgs[idx, 1]),"]",  sep = "" )
    c_to =paste( "[",as.character(deter_catgs[idx, 2]),"]",  sep = "" )
    
    str_det_cat_header = paste(str_det_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  
  str_det_cat_header = paste(str_det_cat_header, "\n", sep =  "")
  
  
  write(c("# deteriorated queries"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg rank diff (deter queries)"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_avg_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med rank diff (deter queries)"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_med_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg deterioration size"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_avg_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  write(c("med deterioration size"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_med_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
}


#--------------------------------------------------------------

compute_stats <- function(all_data2, sys_name, input_rank, input_rank2, rank_str){ 
  
  if (sys_name == overall_sys) {
    all_data = all_data2
  }else{
    all_data = subset(all_data2, system == sys_name)
  }
  
  
  cat("system: " , sys_name, "\n" )
  
  
  cat("input_rank: " , input_rank, "\n" )
  cat("input_rank2: " , input_rank2, "\n" )
  cat("rank_str: " , rank_str, "\n" )
  
  baseline_data = subset(all_data, strategy == baseline)
  
  #--------------------------
  
  cat("Baseline stats", "\n" )
  total_num_queries = nrow(baseline_data)
  cat("# of queries: " , total_num_queries, "\n" )
  total_num_queries_res = nrow(subset(baseline_data, rank!=0 ))
  cat("# of queries with results: " ,total_num_queries_res, "\n" )
  num_queries_top1 = nrow(subset(baseline_data, rank==1 ))
  cat("# of queries in top-1 with results: " ,num_queries_top1, "\n\n" )
  
  
  str_base_stats_string <<- paste(str_base_stats_string, paste(sys_name, total_num_queries, total_num_queries_res, num_queries_top1, sep = ";"), "\n", sep =  "")
  
  
  #--------------------------
  
  baseline_data_notop1 = subset(baseline_data, rank!=1 & rank!=0)
  baseline_data_notop1$type_rd <- c(0)
  baseline_data_notop1$rec_rank <- 1 /baseline_data_notop1$rank
  baseline_data_notop1 = categorize_ranks(baseline_data_notop1)
  
  
  
  #-------------------------------------------
 
  
    
  # strat_data_res = subset(all_data, strategy %in% strategies & !is.na(rank) & rank!=0 )
  # strat_data_res$type_rd <- c(0)
  # strat_data_res[strat_data_res$rank_diff>0, ]$type_rd =1
  # strat_data_res[strat_data_res$rank_diff<0, ]$type_rd =-1
  # strat_data_res$rec_rank <- 1 /strat_data_res$rank
  # 
  # strat_data_res = categorize_ranks(strat_data_res)
  # 
  # strat_data_merged = merge(baseline_data_notop1, strat_data_res, by = c("system", "bug_id"))
  # 
  # c_bounds =determine_bounds(   strat_data_merged, input_rank)
  
  
  #-------------------------------------------
  
  for (strat in strategies) {
    
    cat("Strategy: ", strat , "-----------------------\n\n" )
    
    
    strat_data = subset(all_data, strategy == strat & !is.na(rank))
    
    #--------------------------
    
    num_queries_str = nrow(strat_data)
    cat("# of queries: " , num_queries_str, "\n" )
    
    
    strat_data_res = subset(strat_data,  rank!=0 )
    num_queries_str_res = nrow(strat_data_res)
    cat("# of queries with results: " ,num_queries_str_res, "\n\n" )
    
    #cat("str_stats_string: " ,str_stats_string, "\n\n" )
  
    
    #--------------------------
    
    
    if (nrow(strat_data_res) >0) {
      strat_data_res$type_rd <- c(0)
      
      if (nrow(strat_data_res[strat_data_res$rank_diff>0, ]) > 0) {
        strat_data_res[strat_data_res$rank_diff>0, ]$type_rd =1
      }
      if (nrow(strat_data_res[strat_data_res$rank_diff<0, ]) > 0) {
        strat_data_res[strat_data_res$rank_diff<0, ]$type_rd =-1
      }
      strat_data_res$rec_rank <- 1 /strat_data_res$rank
      
    }else{
      strat_data_res$type_rd = numeric(0)
      strat_data_res$rec_rank = numeric(0)
    }
    
    strat_data_res = categorize_ranks(strat_data_res)
    
    ## DATA MERGED -> WITH NO OUTLIERS, DEPENDING ON THE PARAMETERS
    strat_data_merged = merge(baseline_data_notop1, strat_data_res, by = c("system", "bug_id"))
    strat_data_merged = remove_outliers_fn(strat_data_merged, input_rank, input_rank2)
    
    if (sys_name == overall_sys) {
      
      outf_file_str =  paste(out_folder, "/results_", data_set,"_",tr_approach, "_", rank_str,"_", strat,".csv", sep="");
      #outf_file_str =  paste(out_folder, "/results_", strat, ".csv", sep="");
      write.table(strat_data_merged, file = outf_file_str,row.names = FALSE,  sep=";")
    }
    
    #--------------------------
    #FIXME? outliers are not removed
    
    strat_data_merged_temp = merge(baseline_data_notop1, strat_data, by = c("system", "bug_id"))
    data_below_temp =subset(strat_data_merged_temp, rank.x>input_rank )
    if (!is.null(input_rank2)) {
      data_below_temp=subset(data_below_temp, rank.x<=input_rank2  )
    }
    
    num_queries_str_below = nrow(data_below_temp)
    num_queries_str_below_res = nrow(subset(data_below_temp, rank.y !=0  ))
    num_queries_str_below_res_voided = num_queries_str_below - num_queries_str_below_res
    
    str_stats_string <<- paste(str_stats_string, paste( sys_name, strat, num_queries_str, num_queries_str_res, num_queries_str_below, num_queries_str_below_res, sep = ";"), "\n", sep =  "")
    
    #--------------------------
    
    
    strat_data_merged$improv_size.y =  strat_data_merged$rank_diff.y / strat_data_merged$rank.x
      
    data_below=subset(strat_data_merged, rank.x>input_rank )
    if (!is.null(input_rank2)) {
      data_below=subset(data_below, rank.x<=input_rank2 )
    }
    
    num_queries_str_below = nrow(data_below)
    cat("# of below top-", input_rank ," queries with results: " ,num_queries_str_below, "\n" )
    
    #--------------------------
    
    #mrr & map
    
    mrr_baseline = mean(data_below$rec_rank.x)
    mrr_str = mean(data_below$rec_rank.y)
    mrr_diff = mrr_str - mrr_baseline
    mrr_improv = mrr_diff/mrr_baseline
    cat("mrr base: " ,mrr_baseline, "\n" )
    cat("mrr str: " ,mrr_str, "\n" )
    cat("mrr diff: " ,mrr_diff, "\n" )
    
    map_baseline = mean(data_below$avg_prec.x)
    map_str = mean(data_below$avg_prec.y)
    map_diff = map_str - map_baseline
    map_improv = map_diff / map_baseline
    cat("map base: " ,map_baseline, "\n" )
    cat("map str: " ,map_str, "\n" )
    cat("map diff: " ,map_diff, "\n" )
    
    
    
    
    str_mrr_string <<- paste(str_mrr_string, paste(sys_name, strat, rank_str, num_queries_str_below, num_queries_str_below_res_voided,  mrr_baseline, mrr_str, mrr_diff, mrr_improv, map_baseline, map_str, map_diff, map_improv, sep = ";"), "\n", sep =  "")
    
    
    #--------------------------
    
    #mrr & map (including baseline rank when the strategy rank is null)
    
    strat_data_merged_all = merge(baseline_data_notop1, strat_data_res, by = c("system", "bug_id"), all.x =TRUE)
    na_rows = is.na(strat_data_merged_all$query_id.y)
    cols_base = which(colnames(strat_data_merged_all) %in% c("query_id.x" , "strategy.x" , "rank.x" , "rank_diff.x", "avg_prec.x",  "type_rd.x",  "rec_rank.x",  "rank_cat.x" ))
    cols_strat = which(colnames(strat_data_merged_all) %in% c("query_id.y" , "strategy.y" , "rank.y" , "rank_diff.y", "avg_prec.y",  "type_rd.y",  "rec_rank.y",  "rank_cat.y" ))
    strat_data_merged_all[na_rows, cols_strat ] = strat_data_merged_all[na_rows, cols_base ]
    strat_data_merged_all[na_rows,  which(colnames(strat_data_merged_all) == "strategy.y") ] = strat
    
    
    strat_data_merged_all = remove_outliers_fn(strat_data_merged_all, input_rank,  input_rank2)
    
    
    data_below_all=subset(strat_data_merged_all, rank.x>input_rank )
    if (!is.null(input_rank2)) {
      data_below_all=subset(data_below_all, rank.x<=input_rank2 )
    }
    
    
    num_queries_str_below_all = nrow(data_below_all)
    
    mrr_baseline_all = mean(data_below_all$rec_rank.x)
    mrr_str_all = mean(data_below_all$rec_rank.y)
    mrr_diff_all = mrr_str_all - mrr_baseline_all
    mrr_improv_all = mrr_diff_all / mrr_baseline_all
    
    map_baseline_all = mean(data_below_all$avg_prec.x)
    map_str_all = mean(data_below_all$avg_prec.y)
    map_diff_all = map_str_all - map_baseline_all
    map_improv_all = map_diff_all / map_baseline_all
    
    
    str_mrr_string_all <<- paste(str_mrr_string_all, paste(sys_name, strat, rank_str, num_queries_str_below_all,  mrr_baseline_all, mrr_str_all, mrr_diff_all, mrr_improv_all, map_baseline_all, map_str_all, map_diff_all, map_improv_all, sep = ";"), "\n", sep =  "")
    
    
    #--------------------------
    
    # improv and deter
    data_below_improv = subset(data_below,type_rd.y == 1)
    data_below_deter = subset(data_below,type_rd.y == -1)
    
    num_same_str = nrow( subset(data_below,type_rd.y == 0))
    num_improv_str = nrow(data_below_improv)
    num_deter_str = nrow(data_below_deter)
    cat("# queries improved: " ,num_improv_str, "\n" )
    cat("# queries deteriorated: " ,num_deter_str, "\n" )
    num_improv_diff = num_improv_str - num_deter_str
    
    # pdf(paste("improv_", strat, ".pdf", sep = ""))
    # hist(data_below_improv$improv_size.y, breaks=20, labels = TRUE, col="gray")
    # dev.off()
    # 
    # 
    # pdf(paste("box_improv_", strat, ".pdf", sep = ""))
    # boxplot(data_below_improv$improv_size.y)
    # dev.off()
    # 
    # pdf(paste("deter_", strat, ".pdf", sep = ""))
    # hist(data_below_deter$improv_size.y, breaks=100, labels = TRUE, col="gray", xlim = c(-20, 0))
    # dev.off()
    # 
    # 
    # pdf(paste("box_deter_", strat, ".pdf", sep = ""))
    # boxplot(data_below_deter$improv_size.y)
    # dev.off()
    
    improv_size = c(mean(data_below_improv$improv_size.y), median(data_below_improv$improv_size.y))
    deter_size = c(mean(data_below_deter$improv_size.y), median(data_below_deter$improv_size.y))
    improv_size2 = c(mean(data_below_improv$rank_diff.y), median(data_below_improv$rank_diff.y))
    deter_size2 = c(mean(data_below_deter$rank_diff.y), median(data_below_deter$rank_diff.y))
    
    str_imp_det_string <<- paste(str_imp_det_string, paste( sys_name, strat, rank_str, num_improv_str, num_deter_str, num_improv_diff, 
                               improv_size[1], improv_size[2], deter_size[1], deter_size[2], 
                               improv_size2[1], improv_size2[2], deter_size2[1], deter_size2[2], 
                               num_same_str,  num_improv_str/num_queries_str_below, num_deter_str/num_queries_str_below, num_same_str/num_queries_str_below , sep = ";"), "\n", sep =  "")
    
    
    #--------------------------
    
    
    
    
    # improv from below to the top
    #-----------------------------------------------------------
    str_imp_cat_string <<- paste(str_imp_cat_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_avg_string  <<- paste(str_imp_cat_avg_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_med_string  <<- paste(str_imp_cat_med_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_avg_string2  <<- paste(str_imp_cat_avg_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_med_string2  <<- paste(str_imp_cat_med_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    
   
    
    #improv "to"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_to in unique(improv_catgs$to)) {
      
      data_comb = subset(data_below_improv,  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
      str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
      str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
      str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
      
    }
    str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    improv_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    improv_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
   
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
    
    #improv "from"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_from in unique(improv_catgs$from)) {
      
      data_comb = subset(data_below_improv,  rank_cat.x == c_from )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
      str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
      str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
      str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
      
    }
    str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    improv_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    
    improv_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
    
    
    #improv "above rank"
    #-------------------
    
    data_comb = subset(data_below_improv,  rank.y <= input_rank & rank.y != 0)
    count_comb = nrow(data_comb)
    
    improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
    improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
    
    str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
    
    #-------------------
    
    prefix = "# of queries improved ";
    for (idx in 1:nrow(improv_catgs)) {
      
      c_from = as.character(improv_catgs[idx, 1])
      c_to = as.character(improv_catgs[idx, 2])
      
      data_comb = subset(data_below_improv, rank_cat.x == c_from &  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      
      improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      cat(prefix,"[", c_from," --> ",c_to," ] : ", count_comb, "\n" )
      str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
      str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
      str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
      str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
      
    }
    
    str_imp_cat_string <<- paste(str_imp_cat_string, "\n", sep =  "")
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, "\n", sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, "\n", sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, "\n", sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, "\n", sep =  "")
    
    #deterioration
    #----------------------------------------
    
    
    
    str_det_cat_string <<- paste(str_det_cat_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    
    
   
    
    #deter "from"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_from in unique(deter_catgs$from)) {
      
      data_comb = subset(data_below_deter,  rank_cat.x == c_from )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      deter_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      deter_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_det_cat_string <<- paste(str_det_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
      str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
      str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
      str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
      
    }
    str_det_cat_string <<- paste(str_det_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    
    deter_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    deter_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
    
    
    #deter "to"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_to in unique(deter_catgs$to)) {
      
      data_comb = subset(data_below_deter,  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      deter_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      deter_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_det_cat_string <<- paste(str_det_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
      str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
      str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
      str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
      
    }
    str_det_cat_string <<- paste(str_det_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    deter_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    deter_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
    
    prefix = "# of queries deteriorated ";
    for (idx in 1:nrow(deter_catgs)) {
      
      c_from = as.character(deter_catgs[idx, 1])
      c_to = as.character(deter_catgs[idx, 2])
      
      data_comb = subset(data_below_deter, rank_cat.x == c_from &  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      deter_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      deter_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      
      cat(prefix,"[", c_from," --> ",c_to," ] : ", count_comb, "\n" )
      
      str_det_cat_string <<- paste(str_det_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
      str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
      str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
      str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
    }
    
    str_det_cat_string <<- paste(str_det_cat_string, "\n", sep =  "")
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, "\n", sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, "\n", sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, "\n", sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, "\n", sep =  "")
    
    
    cat("\n" )
      
    
  }
  

}


#--------------------------------------------------------------------------

run_all_analysis <- function(input_rank, input_rank2){
  
  #--------------------------------------------------------------------------
  
  
  str_base_stats_string <<- ""
  str_stats_string <<- ""
  str_mrr_string <<- ""
  str_imp_det_string <<- ""
  str_imp_cat_string <<- ""
  str_imp_cat_avg_string <<- ""
  str_imp_cat_med_string <<- ""
  str_imp_cat_avg_string2 <<- ""
  str_imp_cat_med_string2 <<- ""
  str_det_cat_string <<- ""
  str_det_cat_avg_string <<- ""
  str_det_cat_med_string <<- ""
  str_det_cat_avg_string2 <<- ""
  str_det_cat_med_string2 <<- ""
  
  str_mrr_string_all <<- ""

  
  #--------------------------------------------------------------
  
  
  if (!is.null(input_rank2)) {
    rank_str = paste("(",input_rank, ",",input_rank2,"]", sep="")
  }else{
    rank_str = paste("(",input_rank, ", inf]", sep="")
  }

  overall_sys <<- "overall"
  
  
  bugs  = read.csv(list_of_bugs, sep = ";", header = TRUE) 
  cols= c ("dataset", "system", "bug_id")
  colnames(bugs) <- cols
  bugs_ds = subset(bugs, dataset == data_set )
  bugs_ds$system = tolower(bugs_ds$system)
  
  all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = "") 
  
  all_data_bugs = merge(all_data_set, bugs_ds, by =  c ("system", "bug_id"))
  
  compute_stats(all_data_bugs,overall_sys, input_rank, input_rank2, rank_str)
  
  if (compute_systems) {
   systems = unique(all_data_bugs$system)
   for (sys_name in systems) {
     compute_stats(all_data_bugs,sys_name, input_rank, input_rank2, rank_str)  
   }
  }
  
   
  out_file =  paste(out_folder, "/analysis_", data_set,"_",tr_approach, "_", rank_str,".csv", sep="");
  if (file.exists(out_file)) {
    file.remove(out_file)
  }
  print_results(out_file)
  
  cat("Results written in ", out_file, "\n")

}

##---------------------------------------------------

ranks = data.frame(from = c(10, 10, 20, 30), to = c(NA, 20, 30, NA))

for (idx in 1:nrow(ranks)) {
  
  from_rank = ranks[idx, 1]
  to_rank = ranks[idx, 2]
  
  if (is.na(to_rank)) {
    to_rank <- NULL
  }
  
  run_all_analysis(from_rank, to_rank)
  
}