#!/usr/bin/env python3

"""
Description
"""


import argparse
from http.client import HTTPConnection as HTTPC, HTTPSConnection as HTTPSC
import os
import json
import urllib
import xml.etree.ElementTree as ET


TRACKERS = {
    'ArgoUML-0.22': ('bz', 'argouml.tigris.org'),
    'bookkeeper-4.1.0': ('ji', 'issues.apache.org/jira'),
    'derby-10.7.1.1': ('ji', 'issues.apache.org/jira'),
    'derby-10.9.1.0': ('ji', 'issues.apache.org/jira'),
    'hibernate-3.5.0b2': ('ji', 'hibernate.atlassian.net'),
    'JabRef-2.6': ('sf', 'jabref'),
    'jEdit-4.3': ('sf', 'jedit'),
    'lucene-4.0': ('ji', 'issues.apache.org/jira'),
    'mahout-0.8': ('ji', 'issues.apache.org/jira'),
    'openjpa-2.0.1': ('ji', 'issues.apache.org/jira'),
    'openjpa-2.2.0': ('ji', 'issues.apache.org/jira'),
    'pig-0.8.0': ('ji', 'issues.apache.org/jira'),
    'pig-0.11.1': ('ji', 'issues.apache.org/jira'),
    'solr-4.4.0': ('ji', 'issues.apache.org/jira'),
    'tika-1.3': ('ji', 'issues.apache.org/jira'),
    'zookeeper-3.4.5': ('ji', 'issues.apache.org/jira'),
}

API_BATCH_SIZE = 100


def jira_download(bugs, url):
    end_index = 0
    issue_ids = list(bugs.keys())
    splits = url.split('/', 1)
    if len(splits) > 1:
        domain =splits[0]
        rest = '/' + splits[1]
    else:
        domain = url
        rest = ''
    connection = HTTPSC(domain)

    while end_index < len(issue_ids):
        start_index = end_index
        end_index = start_index + API_BATCH_SIZE

        connection.request(
            'GET', rest + '/rest/api/2/search?%s' %
            urllib.parse.urlencode(
                {'jql': 'key IN (%s)' %
                 ','.join(issue_ids[start_index:end_index]),
                 'maxResults': API_BATCH_SIZE,
                 'fields': 'id,key,summary,description,resolutiondate,created'
                 }))

        rs = connection.getresponse().read()
        response_json = json.loads(rs.decode())

        if API_BATCH_SIZE > response_json['maxResults']:
            print("ERROR: API's maxResults value is lower than amount "
                  "of requests sent", file=sys.stderr)
            exit(1)

        expected_amount = min(end_index - start_index,
                              len(issue_ids) - start_index)
        actual_amount = len(response_json['issues'])

        if expected_amount != actual_amount:
            print('ERROR: API returned %d results, expected %d' %
                  (actual_amount, expected_amount), file=sys.stderr)
            exit(1)

        for wrapper in response_json['issues']:
            issue = wrapper['fields']
            key = wrapper['key']
            if issue['resolutiondate'] is None:
                print('WARNING: No resolution date for issue %s' % key,
                      file=sys.stderr)

            bug = bugs[key]
            bug['title'] = issue['summary']
            bug['description'] = issue['description']
            bug['creation_date'] = issue['created']
            bug['resolution_date'] = issue['resolutiondate']
                
            yield bug


def get_bugs(file_path):
    file_name = os.path.basename(file_path)
    system_name = file_name[:file_name.rindex('.')]

    tracker, url = TRACKERS[system_name]

    if tracker == 'bz':
        def download(bug):
            c = HTTPC(url)
            c.request('GET', '/issues/xml.cgi?id=%s' % bug['key'])
            rs = c.getresponse().read()
            issue = ET.fromstring(rs)[0]

            activities = [a for a in issue.findall('activity')
                          if (a.find('field_name').text == 'resolution' and
                              a.find('newvalue').text == 'FIXED')]
            if not activities:
                resolution_date = None
            else:
                resolution_date = activities[-1].find('when').text

            bug['title'] = issue.find('short_desc').text
            bug['description'] = issue.find('long_desc').find('thetext').text
            bug['creation_date'] = issue.find('creation_ts').text
            bug['resolution_date'] = resolution_date

            return bug
        
        with open(file_path) as f:
            for line in f:
                bug = json.loads(line)
                yield download(bug)
        
    if tracker == 'ji':
        bugs = {}
        with open(file_path) as f:
            for line in f:
                bug = json.loads(line)
                bugs[bug['key']] = bug
                
        yield from jira_download(bugs, url)

    if tracker == 'sf':
        known_real_keys = {'1285977': '281',
                           '2105329': '690',
                           '1536064': '2599',
                           '1541372': '2635',
                           '1569735': '2674',
                           '1584436': '2702',
                           '1883809': '3019',
                           '1984000': '3107',
                           '2091052': '3156',
                           '2173091': '3175',}
        with open(file_path) as f:
            c = HTTPSC('sourceforge.net')
            for line in f:
                bug = json.loads(line)
                if bug['key'] not in known_real_keys:
                    c.request('GET', '/rest/p/'+url+'/bugs/search/?q=' + bug['key'])
                    rs=c.getresponse().read().decode()
##                    print(rs)
                    srch = json.loads(rs)

                    if srch['count'] > 1:
                        print('More than one result when searching issue ' +
                        bug['key'])
                        continue
                    elif srch['count'] == 0:
                        print('issue %s not found' % bug['key'])
                        continue

                    real_key = str(srch['tickets'][0]['ticket_num'])
                else:
                    real_key = known_real_keys[bug['key']]

                c.request('GET', '/rest/p/'+url+'/bugs/' + real_key)
                issue = json.loads(c.getresponse().read().decode())['ticket']

                bug['real_key'] = real_key
                bug['title'] = issue['summary']
                bug['description'] = issue['description']
                bug['creation_date'] = issue['created_date']
                bug['resolution_date'] = issue['mod_date']

                yield bug
            

def main(input_file_path, output_path):
    file_name = os.path.basename(input_file_path)
    with open(os.path.join(output_path, file_name), 'w') as output_file:
        for bug in get_bugs(input_file_path):
            print(json.dumps(bug), file=output_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help='The directory where '
                        'the basic info of the issues can be found')
    parser.add_argument('output_directory', help='The directory where '
                        'the downloaded bugs will be output')

    args = parser.parse_args()

    main(args.input_file, args.output_directory)
