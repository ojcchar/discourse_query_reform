#!/bin/bash

export BASE_DIR=/mnt/c/Users/ojcch/Documents/Projects/Discourse_query_reformulation
export RAW_RESULTS_DIR=$BASE_DIR/results/1_retrieval-results
export ANALYSIS_FORMAT_DIR=$BASE_DIR/results/2_analysis-format
export TECHNIQUE=brtracer

#dataset_folders=("buglocator-sample-hard-to-retrieve" "d4j-all-hard-to-retrieve" "query_quality-all-hard-to-retrieve" "lobster-all-hard-to-retrieve")
dataset_folders=("buglocator-sample-hard-to-retrieve")
#dataset_folders=("lobster-all-hard-to-retrieve")
query_strategies=("TITLE" "CODE" "OB" "EB" "S2R" "OB_EB" "OB_S2R" "EB_S2R" "OB_EB_S2R" "OB_TITLE" "EB_TITLE" "S2R_TITLE" "OB_EB_TITLE" "OB_S2R_TITLE" "EB_S2R_TITLE" "OB_EB_S2R_TITLE" "TITLE_CODE" "OB_CODE" "EB_CODE" "S2R_CODE" "OB_EB_CODE" "OB_S2R_CODE" "EB_S2R_CODE" "OB_EB_S2R_CODE" "OB_TITLE_CODE" "EB_TITLE_CODE" "S2R_TITLE_CODE" "OB_EB_TITLE_CODE" "OB_S2R_TITLE_CODE" "EB_S2R_TITLE_CODE" "OB_EB_S2R_TITLE_CODE")
thresholds=("10" "20" "30")

for dataset_folder in "${dataset_folders[@]}" 
do
   for query_strategy in "${query_strategies[@]}"
   do
		echo "Processing $dataset_folder $query_strategy"
		IN_DIR=$RAW_RESULTS_DIR/$dataset_folder/$TECHNIQUE/and/TITLE_DESCRIPTION/$query_strategy
		OUT_DIR=$ANALYSIS_FORMAT_DIR/$TECHNIQUE/$dataset_folder/$query_strategy
		mkdir -p $OUT_DIR
		python3 convert-evaluation-to-analysis-format.py -i $IN_DIR -o $OUT_DIR -b ALL_TEXT
		 
		#for each threshold
	   for threshold in "${thresholds[@]}"
	   do
		 echo "Processing $operator $oracle $threshold"
			IN_DIR=$RAW_RESULTS_DIR/$dataset_folder/$TECHNIQUE/and/TITLE_DESCRIPTION/$query_strategy/threshold-$threshold
			OUT_DIR=$ANALYSIS_FORMAT_DIR/$TECHNIQUE/$dataset_folder/$query_strategy/threshold-$threshold
			mkdir -p $OUT_DIR
			python3 convert-evaluation-to-analysis-format.py -i $IN_DIR -o $OUT_DIR -b ALL_TEXT &
	   done
   done
done





