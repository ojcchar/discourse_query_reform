##---------
library("dplyr")
library("ggpubr")
library("effsize")

rm(list=ls())

#-------------------------------------

strats = c("OB_EB_TITLE", "OB_EB", "EB_TITLE", "OB_S2R_TITLE", "OB_S2R", 
           "OB_TITLE_CODE", "OB_EB_S2R_TITLE", "OB_EB_S2R", "S2R_TITLE", 
           "OB_TITLE", "OB_CODE", "TITLE", "EB_S2R_TITLE", "OB", 
           "OB_S2R_TITLE_CODE", "OB_EB_TITLE_CODE", "OB_EB_CODE", 
           "TITLE_CODE", "OB_S2R_CODE", "S2R_TITLE_CODE", "EB", 
           "EB_TITLE_CODE", "EB_S2R", "OB_EB_S2R_TITLE_CODE", 
           "OB_EB_S2R_CODE", "S2R", "S2R_CODE", "CODE", "EB_CODE", 
           "EB_S2R_TITLE_CODE", "EB_S2R_CODE")
techniques = c("lucene", "buglocator", "brtracer","lobster", "locus")
granularities = c("FILE", "CLASS", "METHOD")



#strats = c(
#          "OB_EB_S2R_CODE")
#techniques = c("lobster")
write_stdout = FALSE

##----------------------------------------

confidence_Val = 0.95

base_folder =  'C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/4_results-summary2/'
data_file = paste(base_folder, 'all-results.csv', sep = "")
out_file=  paste(base_folder, 'st-tests.csv', sep = "")

if(file.exists(out_file)){
  file.remove(out_file)
}

#-----------------------------------------------

#load data
all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = c("","NA"))

all_data_set = subset(all_data_set, !is.na(h.n.improv))

#filter all granularities
all_ds = subset(all_data_set, DocumentGranularity=='all_granularities')

compute_write_stats2 <- function(vals1, vals2, conf_level, strategy, technique, granularity, metric){
  n_vals = length(vals1)
  
  if(n_vals > 0){
    wil = wilcox.test(vals1, vals2, 
                      paired=TRUE, alternative="less", conf.level = conf_level)
    
    w_stat = wil$statistic
    p_val = wil$p.value
    #cat(p_val, sep = "\n")
    
    p_val_intr = ""
    if(is.numeric(p_val) && !is.na(p_val)){
      p_val_intr = "NR: (Ho: B >= A)"
      if(p_val < (1-conf_level)){
        p_val_intr = "R: (Ho: B >= A)"
      }
    }
    
    d = cliff.delta(vals1, vals2)
    d_estimate = d$estimate
    d_magnitude = d$magnitude
    
    if(write_stdout){
      
      cat(paste(metric, strategy, technique, granularity, n_vals, 
                w_stat, p_val, p_val_intr, d_estimate, d_magnitude, sep=";"), 
          sep = "\n")
    }else{
      cat(file = out_file, paste(metric, strategy, technique, granularity, n_vals, 
                                 w_stat, p_val, p_val_intr, d_estimate, d_magnitude, sep=";"), 
          sep = "\n", append=TRUE)
    }
  }else{
    if(write_stdout){
      
      cat(paste(metric, strategy, technique, granularity, n_vals, 
                "", "", "", "", "", sep=";"), 
          sep = "\n")
    }else{
      cat(file = out_file, paste(metric, strategy, technique, granularity, n_vals, 
                                 "", "", "", "", "", sep=";"), 
          sep = "\n", append=TRUE)
    }
  }
}


compute_write_stats <- function(data_in, conf_level, strategy, technique, granularity){
  
  vals1 = data_in$X..h.n.baseline.1
  vals2 = data_in$X..h.n.strat.1
  
  compute_write_stats2(vals1, vals2, conf_level, strategy, technique, granularity, "hits")
  
  vals1 = data_in$mrr_base
  vals2 = data_in$mrr_strat
  
  compute_write_stats2(vals1, vals2, conf_level, strategy, technique, granularity, "mrr")
  
  vals1 = data_in$map_base
  vals2 = data_in$map_strat
  
  compute_write_stats2(vals1, vals2, conf_level, strategy, technique, granularity, "map")
  
}

for(strat in strats){
  
  strat_ds = subset(all_ds, Strategy == strat & !is.na(X..h.n.baseline.1))
  
  compute_write_stats(strat_ds, confidence_Val, strat, "all", "all")
  
  for(tech in techniques){
    strat_ds = subset(all_ds, Strategy == strat & Technique == tech 
                      & !is.na(X..h.n.baseline.1))
    compute_write_stats(strat_ds, confidence_Val, strat, tech, "all")
  }
  
  #--------------------------------
  
  
  strat_ds = subset(all_data_set,   DocumentGranularity!='all_granularities' & Strategy == strat & !is.na(X..h.n.baseline.1))
  compute_write_stats(strat_ds, confidence_Val, strat, "all2", "all2")
  
  
  for(tech in techniques){
    strat_ds = subset(all_data_set,  DocumentGranularity!='all_granularities' & Strategy == strat & Technique == tech 
                      & !is.na(X..h.n.baseline.1))
    compute_write_stats(strat_ds, confidence_Val, strat, tech, "all2")
  }
  
  
  #--------------------------------
  
  
  for(gran in granularities){
    strat_ds = subset(all_data_set, DocumentGranularity==gran &  
                        Strategy == strat & !is.na(X..h.n.baseline.1))
    compute_write_stats(strat_ds, confidence_Val, strat, "all", gran)
    strat_ds = subset(all_data_set, DocumentGranularity==gran & Technique == "lucene" &  
                        Strategy == strat & !is.na(X..h.n.baseline.1))
    compute_write_stats(strat_ds, confidence_Val, strat, "lucene", gran)
  }
  
}
