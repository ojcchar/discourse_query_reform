export GIT_REPOS_DIR=/home/ojcch/ojcch/Documents/Repositories/Git
export DATA_DIR=$GIT_REPOS_DIR/discourse_query_reform/data/existing_data_sets/bench4bl_data
export SCRIPT_PATH=$GIT_REPOS_DIR/ir4se-fwk/ir4se-fwk/src/scripts/corpus-preprocessing

cd $SCRIPT_PATH
for file in $(find $DATA_DIR -name 'original-bug-reports.json'); do
	filename=$(basename "$file")
	dirname=$(dirname  "$file")
    echo "Processing $dirname $filename"
    python3 preprocess-bug-reports.py < $file > $dirname/bug-reports.json &
done


cd $CURDIR
