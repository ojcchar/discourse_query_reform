#!/bin/bash
#IMPORTANT: use the branch 'discourse-query-reform' of 'ir4se-fwk' for running this script

export BASE_DIR=/home/ojcch/ojcch/Documents
export GIT_REPOS_DIR=$BASE_DIR/Repositories/Git
export SCRIPT_PATH=$GIT_REPOS_DIR/ir4se-fwk/ir4se-fwk/src/scripts/
export DATA_DIR=$GIT_REPOS_DIR/discourse_query_reform/data/existing_data_sets/lobster_data
export OUTPUT_DIR=$BASE_DIR/Projects/Discourse_query_reformulation/data/lobster_regenerated

cd $SCRIPT_PATH

for system_folder in $DATA_DIR/*/
do
   filename=$(basename "$system_folder")
   echo $filename
   mkdir $OUTPUT_DIR/$filename
   python3 ./preprocess-lobster-data.py $system_folder $OUTPUT_DIR/$filename &
done




