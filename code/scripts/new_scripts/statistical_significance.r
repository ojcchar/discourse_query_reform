##---------
rm(list=ls())
source("statistical_significance_base_functions.r")

#-------------------------------------

strats = c("OB_EB_TITLE", "OB_EB", "EB_TITLE", "OB_S2R_TITLE", "OB_S2R", 
           "OB_TITLE_CODE", "OB_EB_S2R_TITLE", "OB_EB_S2R", "S2R_TITLE", 
           "OB_TITLE", "OB_CODE", "TITLE", "EB_S2R_TITLE", "OB", 
           "OB_S2R_TITLE_CODE", "OB_EB_TITLE_CODE", "OB_EB_CODE", 
           "TITLE_CODE", "OB_S2R_CODE", "S2R_TITLE_CODE", "EB", 
           "EB_TITLE_CODE", "EB_S2R", "OB_EB_S2R_TITLE_CODE", 
           "OB_EB_S2R_CODE", "S2R", "S2R_CODE", "CODE", "EB_CODE", 
           "EB_S2R_TITLE_CODE", "EB_S2R_CODE")
techniques = c("lucene", "buglocator", "brtracer","lobster", "locus")
granularities = c("FILE", "CLASS", "METHOD")

base_folder =  'C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/4_results-summary2/'

#-----------------------------------------------

run_full_analysis(base_folder, strats, techniques, granularities)