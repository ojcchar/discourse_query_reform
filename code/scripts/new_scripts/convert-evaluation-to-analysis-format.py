#!/usr/bin/env python3

"""
Converts evaluation as output by MainRunner (from evaluation runner project) into a format usable by results_analysis.r
"""

import argparse
import csv
import glob
import os
import sys
import tempfile


def print_err(*messages, **kwargs):
    if 'file' in kwargs:
        del kwargs['file']
    print(*messages, file=sys.stderr, **kwargs)


def read_stats_file_paths(input_dir_path, baseline_name):
    stats_paths = glob.glob(os.path.join(input_dir_path, '*-stats.csv'))
    stats_names = [os.path.basename(p) for p in stats_paths]
    i = stats_names.index(baseline_name + '-stats.csv')
    baseline_path = stats_paths[i]

    return baseline_path, stats_paths


def read_baseline_queries(baseline_file):
    reader = csv.reader(baseline_file, delimiter=';')

    # Ignore the first 2 lines
    [next(reader) for _ in range(2)]

    queries = {}

    for line in reader:
        bug_id = line[2]
        queries[bug_id] = {'system': line[0], 'rank': float(line[3])}

    return queries


def convert_and_write_stats(stats_file, baseline_queries, writer):
    reader = csv.reader(stats_file, delimiter=';')
    # Ignore the first 2 lines
    [next(reader) for _ in range(2)]

    strategy = os.path.basename(stats_file.name).replace('-stats.csv', '')

    for line in reader:
        system, query_id, bug_id, rank, avg_prec = line[0], line[1], line[2], float(line[3]), line[10]
        if bug_id in baseline_queries:
            baseline_rank = baseline_queries[bug_id]['rank']
            rank_diff = baseline_rank - rank

            writer.writerow([system, query_id, bug_id, strategy, rank, rank_diff, avg_prec])


def write_bug_list(baseline_queries, bug_list_file):
    writer = csv.writer(bug_list_file, delimiter=';')
    writer.writerow(['dataset', 'sys', 'bug_id'])

    for bug_id, fields in baseline_queries.items():
        writer.writerow(['data', fields['system'], bug_id])


def main(input_dir_path, output_dir_path, baseline_name):
    print(input_dir_path)
    print(output_dir_path)
    print(baseline_name)

    baseline_file_path, stats_file_paths = read_stats_file_paths(input_dir_path, baseline_name)
    os.makedirs(output_dir_path, exist_ok=True)

    with open(baseline_file_path) as baseline_file:
        baseline_queries = read_baseline_queries(baseline_file)

    with open(os.path.join(output_dir_path, 'overall-stats.csv'), 'w') as overall_stats_file:
        writer = csv.writer(overall_stats_file, delimiter=';')
        writer.writerow(['system', 'query_id', 'bug_id', 'strategy', 'rank', 'rank_diff', 'avg_prec'])
        for stats_file_path in stats_file_paths:
            with open(stats_file_path) as stats_file:
                convert_and_write_stats(stats_file, baseline_queries, writer)

    with open(os.path.join(output_dir_path, 'bug-list.csv'), 'w') as bug_list_file:
        write_bug_list(baseline_queries, bug_list_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-folder', help='Folder with evaluation results',
                        default=os.path.join('..', '..', 'output', 'lucene-first-run', 'coding-1-evaluation'))
    parser.add_argument('-o', '--output-folder', help='Folder to output evaluation in analysis format',
                        default=os.path.join(tempfile.gettempdir(), 'analysis'))
    parser.add_argument('-b', '--baseline-stats', help='The evaluation that should be used as baseline',
                        default='TITLE_DESCRIPTION')

    args = parser.parse_args()

    main(args.input_folder, args.output_folder, args.baseline_stats)
    print("------------------------------------------------")
