##---------
library("dplyr")
library("ggpubr")
library("effsize")
#-----------------------------------------------

compute_write_stats2 <- function(vals1, vals2, conf_level, strategy, technique, granularity, metric, write_stdout, out_file){
  n_vals = length(vals1)
  
  if(n_vals > 0){
    wil = wilcox.test(vals1, vals2, 
                      paired=TRUE, alternative="less", conf.level = conf_level)
    
    w_stat = wil$statistic
    p_val = wil$p.value
    #cat(p_val, sep = "\n")
    
    p_val_intr = ""
    if(is.numeric(p_val) && !is.na(p_val)){
      p_val_intr = "NR: (Ho: B >= A)"
      if(p_val < (1-conf_level)){
        p_val_intr = "R: (Ho: B >= A)"
      }
    }
    
    d = cliff.delta(vals1, vals2)
    d_estimate = d$estimate
    d_magnitude = d$magnitude
    
    if(write_stdout){
      
      cat(paste(metric, strategy, technique, granularity, n_vals, 
                w_stat, p_val, p_val_intr, d_estimate, d_magnitude, sep=";"), 
          sep = "\n")
    }else{
      cat(file = out_file, paste(metric, strategy, technique, granularity, n_vals, 
                                 w_stat, p_val, p_val_intr, d_estimate, d_magnitude, sep=";"), 
          sep = "\n", append=TRUE)
    }
  }else{
    if(write_stdout){
      
      cat(paste(metric, strategy, technique, granularity, n_vals, 
                "", "", "", "", "", sep=";"), 
          sep = "\n")
    }else{
      cat(file = out_file, paste(metric, strategy, technique, granularity, n_vals, 
                                 "", "", "", "", "", sep=";"), 
          sep = "\n", append=TRUE)
    }
  }
}


compute_write_stats <- function(data_in, conf_level, strategy, technique, granularity, write_stdout, out_file){
  
  vals1 = data_in$X..h.n.baseline.1
  vals2 = data_in$X..h.n.strat.1
  
  compute_write_stats2(vals1, vals2, conf_level, strategy, technique, granularity, "hits", write_stdout, out_file)
  
  vals1 = data_in$mrr_base
  vals2 = data_in$mrr_strat
  
  compute_write_stats2(vals1, vals2, conf_level, strategy, technique, granularity, "mrr", write_stdout, out_file)
  
  vals1 = data_in$map_base
  vals2 = data_in$map_strat
  
  compute_write_stats2(vals1, vals2, conf_level, strategy, technique, granularity, "map", write_stdout, out_file)
  
}

run_full_analysis <- function(base_folder=NULL, strats=c(), techniques=c(), granularities=c(), postfix_out_file="", initial_threshold=5,
                              confidence_Val=0.95, write_stdout=FALSE){
  
  data_file = paste(base_folder, 'all-results.csv', sep = "")
  out_file=  paste(base_folder, 'st-tests', postfix_out_file,'.csv', sep = "")
  
  #remove the out file
  if(file.exists(out_file)){
    file.remove(out_file)
  }
  

  #load data
  all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = c("","NA"))
  
  all_data_set = subset(all_data_set, !is.na(h.n.improv) & N >= initial_threshold)
  
  #filter all granularities
  all_ds = subset(all_data_set, DocumentGranularity=='all_granularities'& N >= initial_threshold)
  
  for(strat in strats){
    
    #data for the strat
    strat_ds = subset(all_ds, Strategy == strat & !is.na(X..h.n.baseline.1))
    
    #results overall
    compute_write_stats(strat_ds, confidence_Val, strat, "all", "all", write_stdout, out_file)
    
    #results per technique
    for(tech in techniques){
      strat_ds = subset(all_ds, Strategy == strat & Technique == tech 
                        & !is.na(X..h.n.baseline.1))
      compute_write_stats(strat_ds, confidence_Val, strat, tech, "all", write_stdout, out_file)
    }
    
    #--------------------------------
    
    #overall results2 (for all granularities)
    strat_ds = subset(all_data_set,   DocumentGranularity!='all_granularities' & Strategy == strat & !is.na(X..h.n.baseline.1))
    compute_write_stats(strat_ds, confidence_Val, strat, "all2", "all2", write_stdout, out_file)
    
    #results per technique
    for(tech in techniques){
      strat_ds = subset(all_data_set,  DocumentGranularity!='all_granularities' & Strategy == strat & Technique == tech 
                        & !is.na(X..h.n.baseline.1))
      compute_write_stats(strat_ds, confidence_Val, strat, tech, "all2", write_stdout, out_file)
    }
    
    
    #--------------------------------
    
    #results for each granularity
    for(gran in granularities){
      strat_ds = subset(all_data_set, DocumentGranularity==gran &  
                          Strategy == strat & !is.na(X..h.n.baseline.1))
      compute_write_stats(strat_ds, confidence_Val, strat, "all", gran, write_stdout, out_file)
      strat_ds = subset(all_data_set, DocumentGranularity==gran & Technique == "lucene" &  
                          Strategy == strat & !is.na(X..h.n.baseline.1))
      compute_write_stats(strat_ds, confidence_Val, strat, "lucene", gran, write_stdout, out_file)
    }
    
  }
}


run_full_analysis_pair <- function(base_folder=NULL, strat1=NULL, strat2=NULL, techniques=c(), 
                              granularities=c(),  postfix_out_file="",   initial_threshold=5, confidence_Val=0.95, write_stdout=FALSE){
  
  data_file = paste(base_folder, 'all-results.csv', sep = "")
  out_file=  paste(base_folder, 'st-tests', postfix_out_file,'.csv', sep = "")
  
  #remove the out file
  if(file.exists(out_file)){
    file.remove(out_file)
  }
  
  
  #load data
  all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = c("","NA"))
  
  all_data_set = subset(all_data_set, !is.na(h.n.improv) & N >= initial_threshold)
  
  #filter all granularities
  all_ds = subset(all_data_set, DocumentGranularity=='all_granularities'& N >= initial_threshold)
  
  strat1_ds = subset(all_data_set,   DocumentGranularity!='all_granularities' & Strategy == strat1 & !is.na(X..h.n.baseline.1))
  strat2_ds = subset(all_data_set,   DocumentGranularity!='all_granularities' & Strategy == strat2 & !is.na(X..h.n.baseline.1))
  
  strats_merge_ds = merge(strat1_ds, strat2_ds, by=c('Technique', 'DocumentGranularity', 'N'))
  
  vals1 = strats_merge_ds$X..h.n.strat.1.x
  vals2 = strats_merge_ds$X..h.n.strat.1.y
  
  compute_write_stats2(vals1, vals2, confidence_Val, paste(strat1,strat2,sep='-'), "all2", "all2", "hits", write_stdout, out_file)
  
  vals1 = strats_merge_ds$mrr_strat.x
  vals2 = strats_merge_ds$mrr_strat.y
  
  compute_write_stats2(vals1, vals2, confidence_Val, paste(strat1,strat2,sep='-'), "all2", "all2", "mrr", write_stdout, out_file)
  
  vals1 = strats_merge_ds$map_strat.x
  vals2 = strats_merge_ds$map_strat.y
  
  compute_write_stats2(vals1, vals2, confidence_Val, paste(strat1,strat2,sep='-'), "all2", "all2", "map", write_stdout, out_file)
}