##---------
library("dplyr")
library("ggpubr")
library("effsize")

rm(list=ls())

base_folder =  'C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/4_results-summary2/'
data_file = paste(base_folder, 'all-results.csv', sep = "")

#load data
all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = c("","NA"))

#filter all granularities
all_ds = subset(all_data_set, Granularity!='all_granularities' & N >=5)

par(mfrow=c(1,1))
par(family="serif")
#par(mar=c(1,2,3,1))

#some stats (overall)
c = boxplot(all_ds$h.n.improv, col=(c("lightgray")), yaxt="n")
title(main="HITS@N improvement")
m = mean(all_ds$h.n.improv, na.rm = TRUE)
points(m, col ="red", pch=20)
vals = (-2:6)/2
axis(2, at= vals, labels=paste(vals*100,"%", sep=""))
c$stats
m
abline(h = vals, col = "lightgray", lty = "dotted", lwd = par("lwd"))

#distribution
hist(all_ds$h.n.improv, breaks=50)
plot(density(all_ds$h.n.improv, na.rm = TRUE))

#normality test
ggqqplot(all_ds$h.n.improv)
shapiro.test(all_ds$h.n.improv)

#box plot across granularity (only lucene)
all_ds2 = subset(all_data_set, Technique == "lucene")
c = boxplot(all_ds2$h.n.improv ~ all_ds2$Granularity, col=(c("lightgray")))


#box plot across techniques - ALL
c = boxplot(all_ds$h.n.improv ~ all_ds$Technique, col=(c("lightgray")))

#box plot across techniques - CLASS
all_ds3 = subset(all_data_set, Granularity=='CLASS')
c = boxplot(all_ds3$h.n.improv ~ all_ds3$Technique, col=(c("lightgray")))

#FILE
all_ds3 = subset(all_data_set, Granularity=='FILE')
c = boxplot(all_ds3$h.n.improv ~ all_ds3$Technique, col=(c("lightgray")))

# strategies
#par(mfrow=c(6,6))
c = boxplot(all_ds$h.n.improv ~ all_ds$Strategy, col=(c("lightgray")))
grid(nx=NA, ny=NULL)


#---------------------------------------

max = max(all_ds$h.n.improv, na.rm =TRUE)
min = min(all_ds$h.n.improv, na.rm =TRUE)

par(mfrow=c(6,5))
par(mar=c(1,2,1,1))

for(n in 1:30){
  
  #filter all granularities
  n_ds = subset(all_ds, N==n)
  st = boxplot(n_ds$h.n.improv,  main=n, ylim = c(min, max))
  mean = mean(n_ds$h.n.improv, na.rm =TRUE)
  sd=sd(n_ds$h.n.improv, na.rm =TRUE)
  cat(paste(n, mean, sd, paste(st$stats, collapse = " "), sep=" "), sep = "\n")
  #axis(2,)
  
}
