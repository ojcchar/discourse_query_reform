#!/usr/bin/env python3

"""
Applies tokenizing, identifier splitting, stop word and java terms removal on
queries. Modifies files in place.

Input: File paths with the queries

Input format:
{"projects": {
    "project": [
        {"text": ...},
        ...
    ]
}}
"""


import argparse
import preprocessing
import os
import json
import sys


def main(file_paths):
    pre = preprocessing.default_preprocessor()
    
    for fp in file_paths:
        print("Preprocessing", fp)
        
        with open(fp, encoding='utf_8') as file:
            c = json.load(file)

        for _, qs in c['projects'].items():
            for q in qs:
                raw_title = q.get('title', None)
                q['rawTitle'] = raw_title
                if raw_title:
                    q['title'] = pre.preprocess(raw_title).strip() or None

                raw_description = q.get('description', None)
                q['rawDescription'] = raw_description
                if raw_description:
                    q['description'] = pre.preprocess(raw_description).strip() or None

        with open(fp, 'w', encoding='utf_8') as file:
            json.dump(c, file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('files', help='Files containing the queries',
                        nargs='+')
    
    args = parser.parse_args()

    main(args.files)
