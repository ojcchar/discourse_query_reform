#!/bin/bash

export GIT_REPOS_DIR=/home/ojcch/ojcch/Documents/Repositories/Git
export DATA_DIR=/home/ojcch/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries
export SCRIPT_PATH=$GIT_REPOS_DIR/ir4se-fwk/ir4se-fwk/src/scripts/corpus-preprocessing

#dataset_folders=("buglocator-generated-sample-hard-to-retrieve_and" "lobster-generated-all-hard-to-retrieve_and" "d4j-generated-all-hard-to-retrieve-queries_and" "query_quality-generated-all-hard-to-retrieve-queries_and")
dataset_folders=("b4bl_and_split_generated")
info_sources=("TITLE_DESCRIPTION")
query_strategies=("ALL_TEXT" "TITLE" "CODE" "OB" "EB" "S2R" "OB_EB" "OB_S2R" "EB_S2R" "OB_EB_S2R" "OB_TITLE" "EB_TITLE" "S2R_TITLE" "OB_EB_TITLE" "OB_S2R_TITLE" "EB_S2R_TITLE" "OB_EB_S2R_TITLE" "TITLE_CODE" "OB_CODE" "EB_CODE" "S2R_CODE" "OB_EB_CODE" "OB_S2R_CODE" "EB_S2R_CODE" "OB_EB_S2R_CODE" "OB_TITLE_CODE" "EB_TITLE_CODE" "S2R_TITLE_CODE" "OB_EB_TITLE_CODE" "OB_S2R_TITLE_CODE" "EB_S2R_TITLE_CODE" "OB_EB_S2R_TITLE_CODE")

cd $SCRIPT_PATH

for dataset_folder in "${dataset_folders[@]}" 
do
   
	for info_source in "${info_sources[@]}" 
	do
		for query_strategy in "${query_strategies[@]}" 
		do
		   out_folder=$DATA_DIR/$dataset_folder\_prep/$info_source/$query_strategy
		   mkdir -p $out_folder
		   
		   for file in $DATA_DIR/$dataset_folder/$info_source/$query_strategy/*.json;
		   do
			   echo "Processing $file"
			   
			   filename=$(basename "$file")
			   python3 preprocess-source-texts.py < $file > $out_folder/$filename.prep &
		   done
		done
	done
done




