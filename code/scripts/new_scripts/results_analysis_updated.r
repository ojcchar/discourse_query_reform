
rm(list=ls())
library(plyr)
library(optparse)

#---------------------------------------------------------

option_list = list(
  make_option(c("-d", "--data_file"), type="character", default=NULL, 
              help="dataset file name", metavar="character"),
  make_option(c("-l", "--list_of_bugs"), type="character", default=NULL, 
              help="file with the list of bugs", metavar="character"),
  make_option(c("-o", "--out_folder"), type="character", default=NULL, 
              help="output folder", metavar="character"),
  make_option(c("-r", "--remove_outliers"), action="store_true", default=FALSE, 
              help="remove outliers? (true/false)"),
  make_option(c("-s", "--reform_strategy"), type="character", default=NULL, 
              help="query reformulation strategy", metavar="character"),
  make_option(c("-b", "--baseline"), type="character", default="ALL_TEXT", 
              help="query reformulation baseline strategy", metavar="character"),
  make_option(c("-t", "--tr_approach"), type="character", default="lucene", 
              help="TR-based approach", metavar="character"),
  make_option(c("-a", "--data_set"), type="character", default=NULL, 
              help="Data set name", metavar="character"),
  make_option(c("-y", "--compute_systems"), action="store_true", default=FALSE, 
              help="compute results per system? (true/false)"),
  make_option(c("-n", "--rank_threshold"), type="integer", default=10, 
              help="rank threshold (>1)", metavar="number"),
  make_option(c("-i", "--num_cat_bins"), type="integer", default=4, 
              help="number of bins to category the queries (>1)", metavar="number")
); 
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

#---------------------------------------------------------

data_file <<- opt$data_file
list_of_bugs <<- opt$list_of_bugs
out_folder <<-  opt$out_folder

compute_systems <<- opt$compute_systems
remove_outliers <<- opt$remove_outliers
remove_outliers2 <<- remove_outliers


qr_strategy <<- opt$reform_strategy
baseline  <<- opt$baseline
strategies <<- c(qr_strategy)

tr_approach <<- opt$tr_approach
data_set <<- opt$data_set

rank_threshold <<- opt$rank_threshold
num_cat_bins <<- opt$num_cat_bins

cat("data_file: ", data_file , "\n" )
cat("list_of_bugs: ", list_of_bugs , "\n" )
cat("out_folder: ", out_folder , "\n" )
cat("compute_systems: ", compute_systems , "\n" )
cat("remove_outliers: ", remove_outliers , "\n" )
cat("qr_strategy: ", qr_strategy , "\n" )
cat("baseline: ", baseline , "\n" )
cat("tr_approach: ", tr_approach , "\n" )
cat("data_set: ", data_set , "\n" )
cat("rank_threshold: ", rank_threshold , "\n" )
cat("num_cat_bins: ", num_cat_bins , "\n" )

cat("---------------------------------------", "\n" )

#--------------------------------------------------------------


categorize_ranks_param <- function(data2, rank_threshold2, num_bins) {
  
  if (nrow(data2) <= 0) {
    data2$rank_cat = numeric(0)
  }else{
    
      data2[,"rank_cat"] <- NA
    
      idx_ini=0
      idx_end=0
      
      for (bin in 1:num_bins) {
        
        idx_ini = idx_end + 1
        
        #last bin
        if(bin == num_bins){
          #eg., "1-"
          idx_end = NULL
          label=paste(as.character(idx_ini), "-", sep="")
        } else {
          #eg., "1-10"
          idx_end = bin * rank_threshold2
          label=paste(as.character(idx_ini), "-", as.character(idx_end), sep="")
        }
        
        #-----------------------------------
        
        #last bin
        if(bin == num_bins){
          if(nrow(subset(data2,rank >= idx_ini)) > 0){
            data2[data2$rank >=idx_ini, ]$rank_cat = label
          }
        }else{
          if(nrow(subset(data2,rank >= idx_ini & rank <= idx_end)) > 0){
            data2[data2$rank >= idx_ini & data2$rank <= idx_end, ]$rank_cat = label
          }
        }
        
        
      }
    
  }
  
  return(data2)
}

categorize_ranks <- function(data2) {
  
  if (nrow(data2) > 0) {
    
    data2[,"rank_cat"] <- NA
    if(nrow(subset(data2,rank >=1 & rank <=10)) > 0){
      data2[data2$rank >=1 & data2$rank <=10, ]$rank_cat = "1-10"
    }
    if(nrow(subset(data2,rank >=11 & rank <=20)) > 0){
      data2[data2$rank >=11 & data2$rank <=20, ]$rank_cat = "11-20"
    }
    
    if(nrow(subset(data2,rank >=21 & rank <=30)) > 0){
      data2[data2$rank >=21 & data2$rank <=30, ]$rank_cat = "21-30"
    }
    if(nrow(subset(data2,rank >= 31)) > 0){
      data2[data2$rank >=31, ]$rank_cat = "31-"
    }
        
  }else{
    data2$rank_cat = numeric(0)
  }
  
  return(data2)
}


categorize_ranks3 <- function(data2) {
  
  if (nrow(data2) > 0) {
    
    data2[,"rank_cat"] <- NA
    if(nrow(subset(data2,rank >=1 & rank <=10)) > 0){
      data2[data2$rank >=1 & data2$rank <=10, ]$rank_cat = "1-10"
    } 
    if(nrow(subset(data2,rank >=11 & rank <=15)) > 0){
      data2[data2$rank >=11 & data2$rank <=15, ]$rank_cat = "11-15"
    }
    if(nrow(subset(data2,rank >=16 & rank <=20)) > 0){
      data2[data2$rank >=16 & data2$rank <=20, ]$rank_cat = "16-20"
    }
    if(nrow(subset(data2,rank >=21 & rank <=25)) > 0){
      data2[data2$rank >=21 & data2$rank <=25, ]$rank_cat = "21-25"
    }
    if(nrow(subset(data2,rank >=26 & rank <=30)) > 0){
      data2[data2$rank >=26 & data2$rank <=30, ]$rank_cat = "26-30"
    }
    if(nrow(subset(data2,rank >=31 & rank <=40)) > 0){
      data2[data2$rank >=31 & data2$rank <=40, ]$rank_cat = "31-40"
    }
    if(nrow(subset(data2,rank >=41 & rank <=50)) > 0){
      data2[data2$rank >=41 & data2$rank <=50, ]$rank_cat = "41-50"
    }
    if(nrow(subset(data2,rank >=51 & rank <=60)) > 0){
      data2[data2$rank >=51 & data2$rank <=60, ]$rank_cat = "51-60"
    }
    if(nrow(subset(data2,rank >= 61)) > 0){
      data2[data2$rank >=61, ]$rank_cat = "61-"
    }
    
  }else{
    data2$rank_cat = numeric(0)
  }
  
  return(data2)
}


categorize_ranks2 <- function(data2) {
  
  if (nrow(data2) > 0) {
    
    data2[,"rank_cat"] <- NA
    if(nrow(subset(data2,rank ==1)) > 0){
      data2[data2$rank ==1, ]$rank_cat = "1"
    }
    if(nrow(subset(data2,rank >=2 & rank <=5)) > 0){
      data2[data2$rank >=2 & data2$rank <=5, ]$rank_cat = "2-5"
    }
    if(nrow(subset(data2,rank >=6 & rank <=10)) > 0){
      data2[data2$rank >=6 & data2$rank <=10, ]$rank_cat = "6-10"
    }
    if(nrow(subset(data2,rank >=11 & rank <=20)) > 0){
      data2[data2$rank >=11 & data2$rank <=20, ]$rank_cat = "11-20"
    }
    
    if(nrow(subset(data2,rank >=21 & rank <=35)) > 0){
      data2[data2$rank >=21 & data2$rank <=35, ]$rank_cat = "21-35"
    }
    if(nrow(subset(data2,rank >=36 & rank <=50)) > 0){
      data2[data2$rank >=36 & data2$rank <=50, ]$rank_cat = "36-50"
    }
    if(nrow(subset(data2,rank >= 51)) > 0){
      data2[data2$rank >=51, ]$rank_cat = "51-"
    }
    
  }else{
    data2$rank_cat = numeric(0)
  }
  
  return(data2)
}

#--------------------------------------------------------------


remove_outliers_fn <- function(data4, below_rank, above_rank){
  
  
  data_no_outliers = data4
  
  if (remove_outliers) {
    sub_set = subset(data4, rank.x>below_rank & type_rd.y != 0 )
    if (!is.null(above_rank)) {
      sub_set = subset(sub_set, rank.x<=above_rank )
    }
    if(nrow(sub_set) > 0){
      box = boxplot(sub_set$rank_diff.y)
      bounds = box$stats[c(1, 5)]
      data_no_outliers = subset(data4, rank_diff.y >= bounds[1] & rank_diff.y <=bounds[2] )
    }
  }
  
  if (remove_outliers2) {
    data_no_outliers = subset(data_no_outliers, rank.y > 2 )
  }
  
  # else{
  #   #min = min(sub_set$rank_diff.y)
  #   #max = max(sub_set$rank_diff.y)
  #   bounds = c(min(sub_set$rank_diff.y), max(sub_set$rank_diff.y))
  # }
  
  return (data_no_outliers)
}


#--------------------------

determine_category_transitions <- function(rank_threshold2, num_bins) {
  
  labels = get_all_labels(rank_threshold2, num_bins)
  labels_gen = get_all_labels_gen(rank_threshold2, num_bins)
  
  improv_catgs <<- data.frame(from =character(),
                            to =  character())
  deter_catgs <<- data.frame(from =character(),
                            to =  character())
  
  improv_catgs_gen <<- data.frame(from =character(),
                              to =  character())
  deter_catgs_gen <<- data.frame(from =character(),
                             to =  character())
  
  #----------------------------------------------------
  
  for (i in 1:(length(labels)-1)) { 
    
    
      for (j in (i+1):length(labels)) { 
        
        from_label= labels[j]
        to_label= labels[i]
        
        
        #cat(from_label,to_label , "\n" )
        
        catgs_temp = data.frame(from = c(from_label),
                                 to = c(to_label))
        improv_catgs <<- rbind(improv_catgs, catgs_temp)
        
      }
    
    #----------------------------------------
    
    for (j in (i+1):length(labels)) { 
      
      from_label= labels[i]
      to_label= labels[j]
      
      
      #cat(from_label,to_label , "\n" )
      
      catgs_temp = data.frame(from = c(from_label),
                                    to = c(to_label))
      deter_catgs <<- rbind(deter_catgs, catgs_temp)
      
    }
    
  }
  
  
  
  for (i in 1:length(labels)) { 
    catgs_temp = data.frame(from = c(labels[i]),
                                  to = c(labels[i]))
    improv_catgs <<- rbind(improv_catgs, catgs_temp)
    deter_catgs <<- rbind(deter_catgs, catgs_temp)
  }
  
  #----------------------------------------------------
  
  for (i in 1:(length(labels_gen)-1)) { 
    
    
    for (j in (i+1):length(labels_gen)) { 
      
      from_label= labels_gen[j]
      to_label= labels_gen[i]
      
      
      #cat(from_label,to_label , "\n" )
      
      catgs_temp = data.frame(from = c(from_label),
                              to = c(to_label))
      improv_catgs_gen <<- rbind(improv_catgs_gen, catgs_temp)
      
    }
    
    #----------------------------------------
    
    for (j in (i+1):length(labels_gen)) { 
      
      from_label= labels_gen[i]
      to_label= labels_gen[j]
      
      
      #cat(from_label,to_label , "\n" )
      
      catgs_temp = data.frame(from = c(from_label),
                              to = c(to_label))
      deter_catgs_gen <<- rbind(deter_catgs_gen, catgs_temp)
      
    }
    
  }
  
  
  
  for (i in 1:length(labels_gen)) { 
    catgs_temp = data.frame(from = c(labels_gen[i]),
                            to = c(labels_gen[i]))
    improv_catgs_gen <<- rbind(improv_catgs_gen, catgs_temp)
    deter_catgs_gen <<- rbind(deter_catgs_gen, catgs_temp)
  }
  
  
}

get_all_labels_gen <- function(rank_threshold2, num_bins) {
  
  idx_ini=''
  idx_end=''
  
  labels <- c()
  
  for (bin in 1:num_bins) {
    
    if(bin==1){
      idx_ini = "(1)"
    }else if(bin==2){
      idx_ini = "(N+1)"
    }else{
      idx_ini = paste("(", as.character(bin-1),"N+1)", sep="")
    }
    
    #last bin
    if(bin == num_bins){
      label = paste(idx_ini, "-", sep="")
    } else {
      if(bin==1){
        idx_end = "(N)"
      }else{
        idx_end = paste("(", bin, "N", ")", sep="")
      }
      label = paste(idx_ini, "-", idx_end, sep="")
    }
    
    labels <- c(labels, label)
  }
  
  return(labels)
  
}

get_all_labels <- function(rank_threshold2, num_bins) {
  
  idx_ini=0
  idx_end=0
  
  labels <- c()
  
  for (bin in 1:num_bins) {
    
    idx_ini = idx_end + 1
    
    #last bin
    if(bin == num_bins){
      #eg., "1-"
      idx_end = NULL
      label = paste(as.character(idx_ini), "-", sep="")
    } else {
      #eg., "1-10"
      idx_end = bin * rank_threshold2
      label = paste(as.character(idx_ini), "-", as.character(idx_end), sep="")
    }
    
    labels <- c(labels, label)
  }
    
  return(labels)
    
}


improv_catgs = data.frame(from =c("11-20", "21-30", "31-",  "21-30", "31-",   "31-",   "1-10", "11-20", "21-30", "31-"),
                          to =  c("1-10",  "1-10",  "1-10", "11-20", "11-20", "21-30", "1-10", "11-20", "21-30", "31-"))
deter_catgs = data.frame(from =c("1-10", "1-10", "1-10",  "11-20", "11-20", "21-30", "1-10", "11-20", "21-30", "31-"),
                         to =  c("11-20", "21-30", "31-", "21-30", "31-",   "31-",   "1-10", "11-20", "21-30", "31-"))

improv_catgs3 = data.frame(from = c("11-15", "16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-",  "16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-",   "21-25", "26-30",  "31-40", "41-50", "51-60", "61-",   "26-30",  "31-40", "41-50", "51-60", "61-",   "31-40", "41-50", "51-60",  "61-",   "41-50", "51-60", "61-",   "51-60", "61-",   "61-",   "11-15", "16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-"),
                           to =  c("1-10" , "1-10" , "1-10" , "1-10" ,  "1-10" , "1-10" ,  "1-10", "1-10", "11-15", "11-15", "11-15",  "11-15", "11-15", "11-15", "11-15", "16-20", "16-20",  "16-20", "16-20", "16-20", "16-20", "21-25",  "21-25", "21-25", "21-25", "21-25", "26-30", "26-30", "26-30",  "26-30", "31-40", "31-40", "31-40", "41-50", "41-50", "51-60", "11-15", "16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-"))
deter_catgs3 = data.frame(from =  c("11-15", "11-15", "11-15",  "11-15", "11-15", "11-15", "11-15", "16-20", "16-20",  "16-20", "16-20", "16-20",  "16-20", "21-25", "21-25", "21-25", "21-25",  "21-25", "26-30", "26-30", "26-30", "26-30", "31-40", "31-40",  "31-40", "41-50", "41-50", "51-60", "11-15", "16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-"),
                         to =    c("16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-",   "21-25", "26-30",  "31-40", "41-50", "51-60",  "61-",   "26-30", "31-40", "41-50", "51-60",  "61-",   "31-40", "41-50", "51-60",  "61-",  "41-50", "51-60",  "61-",   "51-60", "61-",   "61-",   "11-15", "16-20", "21-25", "26-30",  "31-40", "41-50", "51-60", "61-"))


improv_catgs2 = data.frame(from =c("11-20", "21-35", "36-50", "51-", "11-20","21-35", "36-50",  "51-", "11-20", "21-35", "36-50",  "51-",  "21-35", "36-50", "51-",   "36-50", "51-",   "51-" ,  "11-20", "21-35", "36-50",  "51-" ),
                          to =  c("1",     "1"   ,  "1",     "1",   "2-5",   "2-5",   "2-5",   "2-5", "6-10",  "6-10",  "6-10",   "6-10", "11-20", "11-20", "11-20", "21-35", "21-35", "36-50", "11-20", "21-35", "36-50",  "51-"))
deter_catgs2 = data.frame(from =c("11-20", "11-20", "11-20", "21-35", "21-35", "36-50", "11-20", "21-35", "36-50",  "51-"),
                         to =  c("21-35", "36-50", "51-",   "36-50", "51-",   "51-",   "11-20", "21-35", "36-50",  "51-"))


#--------------------------------------------------------------------------

print_results <- function(out_file){
  
  write(c("Baseline stats"), out_file, sep=";", append = TRUE)
  cat(c("system", "#_queries", "#_queries_retr", "#_top1_queries_retr"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  write(str_base_stats_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  cat(c("system", "strategy", "#_queries_str", "#_queries_retr_str", "#_queries_str_below", "#_queries_retr_str_below"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_stats_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  write(c("performance considering only the queries having the info"), out_file, sep=";", append = TRUE)
  cat(c("system", "strategy", "rank(s)", "#_queries_b_rank","#_queries_b_rank_not_retr", "mrr_base", "mrr_strat", "mrr_diff", "mrr_improv", "map_base", "map_strat", "map_diff", "map_improv"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_mrr_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  # write(c("performance considering all queries"), out_file, sep=";", append = TRUE)
  # cat(c("system", "strategy", "rank(s)", "#_queries_b_rank", "mrr_base", "mrr_strat", "mrr_diff", "mrr_improv", "map_base", "map_strat", "map_diff", "map_improv"), file = out_file, sep=";", append = TRUE)
  # write(c(), out_file, sep=";", append = TRUE)
  # cat(str_mrr_string_all, file = out_file, append = TRUE)
  # write(c(), out_file, sep=";", append = TRUE)
  
  
  write(c("# of queries and improv/deter size"), out_file, sep=";", append = TRUE)
  cat(c("system", "strategy", "rank(s)", "#_improv", "#_deter", "diff", "improv_size_avg", "improv_size_med", "deter_size_avg", "deter_size_med", "improv_rd_avg", "improv_rd_med", "deter_rd_avg", "deter_rd_med", "num_same", "%_improv", "%_deter", "%_same" ), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  cat(str_imp_det_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  
  #-------------------------------------
  
  str_same_cat_header=""
  str_same_cat_header = get_str_same_cat_header(str_same_cat_header, improv_catgs)
  str_same_cat_header = get_str_same_cat_header(str_same_cat_header, improv_catgs_gen)
  
  write(c("# unchanged queries"), out_file, sep=";", append = TRUE)
  cat(str_same_cat_header, file = out_file, append = TRUE)
  cat(str_same_cat_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  #-------------------------------------
  
  str_nonretr_cat_header=""
  str_nonretr_cat_header = get_str_nonretr_cat_header(str_nonretr_cat_header, improv_catgs)
  str_nonretr_cat_header = get_str_nonretr_cat_header(str_nonretr_cat_header, improv_catgs_gen)
  
  
  write(c("# queries that didn't retrieve"), out_file, sep=";", append = TRUE)
  cat(str_nonretr_cat_header, file = out_file, append = TRUE)
  cat(str_nonretr_cat_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  #-------------------------------------
  
  str_imp_cat_header=""
  str_imp_cat_header = get_str_imp_cat_header(str_imp_cat_header, improv_catgs)
  str_imp_cat_header = get_str_imp_cat_header(str_imp_cat_header, improv_catgs_gen)
  
  write(c("# improved queries"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg rank diff (improv queries)"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_avg_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med rank diff (improv queries)"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_med_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg improvement size"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_avg_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med improvement size"), out_file, sep=";", append = TRUE)
  cat(str_imp_cat_header, file = out_file, append = TRUE)
  cat(str_imp_cat_med_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  #-------------------------------------
  
  str_det_cat_header=""
  
  str_det_cat_header = get_str_det_cat_header(str_det_cat_header, deter_catgs)
  str_det_cat_header = get_str_det_cat_header(str_det_cat_header, deter_catgs_gen)
  
  write(c("# deteriorated queries"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg rank diff (deter queries)"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_avg_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med rank diff (deter queries)"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_med_string2, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("avg deterioration size"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_avg_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  write(c("med deterioration size"), out_file, sep=";", append = TRUE)
  cat(str_det_cat_header, file = out_file, append = TRUE)
  cat(str_det_cat_med_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
  #-------------------------------------
  
  write(c("HITS stats"), out_file, sep=";", append = TRUE)
  cat(c("system", "strategy", "rank(s)", "#_q", "# h@n baseline", "# h@n strat", "% h@n baseline", "% h@n strat", "h@n diff", "h@n improv", 
        "# B->G", "# G->B", "# G->G", "# B->B", "% B->G", "% G->B", "% G->G", "% B->B", "#_q2"), file = out_file, sep=";", append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  write(hits_stats_string, file = out_file, append = TRUE)
  write(c(), out_file, sep=";", append = TRUE)
  
}

get_str_nonretr_cat_header<- function(str_nonretr_cat_header, improv_catgs){
  
  str_nonretr_cat_header = paste(str_nonretr_cat_header, paste("system",  "strategy", "rank(s)", sep = ";"), sep =  "")
  for (cat_to in unique(improv_catgs$to)) {
    
    c_to =paste( "[",as.character(cat_to),"]",  sep = "" )
    
    str_nonretr_cat_header = paste(str_nonretr_cat_header, paste( "",  c_to, sep = ";"), sep =  "")
    
  }
  str_nonretr_cat_header = paste(str_nonretr_cat_header, "total", sep = ";")
  str_nonretr_cat_header = paste(str_nonretr_cat_header, "\n", sep =  "")
}

get_str_same_cat_header<- function(str_same_cat_header, improv_catgs){
  
  
  str_same_cat_header = paste(str_same_cat_header, paste("system",  "strategy", "rank(s)", sep = ";"), sep =  "")
  for (cat_to in unique(improv_catgs$to)) {
    
    c_to =paste( "[",as.character(cat_to),"]",  sep = "" )
    
    str_same_cat_header = paste(str_same_cat_header, paste( "",  c_to, sep = ";"), sep =  "")
    
  }
  str_same_cat_header = paste(str_same_cat_header, "total", sep = ";")
  str_same_cat_header = paste(str_same_cat_header, "\n", sep =  "")
  
}

get_str_imp_cat_header <- function(str_imp_cat_header, improv_catgs){
  
  
  str_imp_cat_header = paste(str_imp_cat_header, paste("system",  "strategy", "rank(s)", sep = ";"), sep =  "")
  for (cat_to in unique(improv_catgs$to)) {
    
    c_from = "*"
    c_to =paste( "[",as.character(cat_to),"]",  sep = "" )
    
    str_imp_cat_header = paste(str_imp_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_imp_cat_header = paste(str_imp_cat_header, "total_to", sep = ";")
  
  for (cat_from in unique(improv_catgs$from)) {
    
    c_from = paste("[", as.character(cat_from),"]",  sep = "" )
    c_to ="*"
    
    str_imp_cat_header = paste(str_imp_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_imp_cat_header = paste(str_imp_cat_header, "total_from", sep = ";")
  str_imp_cat_header = paste(str_imp_cat_header, "above_rank", sep = ";")
  
  for (idx in 1:nrow(improv_catgs)) {
    
    c_from = paste("[", as.character(improv_catgs[idx, 1]),"]",  sep = "" )
    c_to =paste( "[",as.character(improv_catgs[idx, 2]),"]",  sep = "" )
    
    str_imp_cat_header = paste(str_imp_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_imp_cat_header = paste(str_imp_cat_header, "\n", sep =  "")
}


get_str_det_cat_header <- function(str_det_cat_header, deter_catgs){
  
  str_det_cat_header = paste(str_det_cat_header, paste( "system", "strategy", "rank(s)", sep = ";"), sep =  "")
  
  for (cat_from in unique(deter_catgs$from)) {
    
    c_from = paste("[", as.character(cat_from),"]",  sep = "" )
    c_to ="*"
    
    str_det_cat_header = paste(str_det_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_det_cat_header = paste(str_det_cat_header, "total_from", sep = ";")
  
  for (cat_to in unique(deter_catgs$to)) {
    
    c_from = "*"
    c_to =paste( "[",as.character(cat_to),"]",  sep = "" )
    
    str_det_cat_header = paste(str_det_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  str_det_cat_header = paste(str_det_cat_header, "total_to", sep = ";")
  
  for (idx in 1:nrow(deter_catgs)) {
    
    c_from = paste("[", as.character(deter_catgs[idx, 1]),"]",  sep = "" )
    c_to =paste( "[",as.character(deter_catgs[idx, 2]),"]",  sep = "" )
    
    str_det_cat_header = paste(str_det_cat_header, paste( "", paste( c_from, c_to, sep = "->" ), sep = ";"), sep =  "")
    
  }
  
  str_det_cat_header = paste(str_det_cat_header, "\n", sep =  "")
  
  return(str_det_cat_header)
}


#--------------------------------------------------------------

compute_stats <- function(all_data2, sys_name, input_rank, input_rank2, rank_str){
  
  if (sys_name == overall_sys) {
    all_data = all_data2
  }else{
    all_data = subset(all_data2, system == sys_name)
  }
  
  
  cat("system: " , sys_name, "\n" )
  
  
  cat("input_rank: " , input_rank, "\n" )
  cat("input_rank2: " , input_rank2, "\n" )
  cat("rank_str: " , rank_str, "\n" )
  
  baseline_data = subset(all_data, strategy == baseline)
  
  #--------------------------
  
  cat("Baseline stats", "\n" )
  total_num_queries = nrow(baseline_data)
  cat("# of queries: " , total_num_queries, "\n" )
  total_num_queries_res = nrow(subset(baseline_data, rank!=0 ))
  cat("# of queries with results: " ,total_num_queries_res, "\n" )
  num_queries_top1 = nrow(subset(baseline_data, rank==1 ))
  cat("# of queries in top-1 with results: " ,num_queries_top1, "\n\n" )
  
  
  str_base_stats_string <<- paste(str_base_stats_string, paste(sys_name, total_num_queries, total_num_queries_res, num_queries_top1, sep = ";"), "\n", sep =  "")
  
  
  #--------------------------
  
  #baseline_data_retrieved = subset(baseline_data, rank!=1 & rank!=0)
  #baseline_data_retrieved = subset(baseline_data, rank!=0)
  baseline_data_retrieved = subset(baseline_data, rank!=0)
  baseline_data_retrieved$type_rd <- c(0)
  baseline_data_retrieved$rec_rank <- 1 /baseline_data_retrieved$rank
  baseline_data_retrieved = categorize_ranks_param(baseline_data_retrieved, rank_threshold, num_cat_bins)
  
  
  
  #-------------------------------------------
  
  
  
  # strat_data_res = subset(all_data, strategy %in% strategies & !is.na(rank) & rank!=0 )
  # strat_data_res$type_rd <- c(0)
  # strat_data_res[strat_data_res$rank_diff>0, ]$type_rd =1
  # strat_data_res[strat_data_res$rank_diff<0, ]$type_rd =-1
  # strat_data_res$rec_rank <- 1 /strat_data_res$rank
  #
  # strat_data_res = categorize_ranks(strat_data_res)
  #
  # strat_data_merged = merge(baseline_data_retrieved, strat_data_res, by = c("dataset", "system", "bug_id"))
  #
  # c_bounds =determine_bounds(   strat_data_merged, input_rank)
  
  
  #-------------------------------------------
  
  for (strat in strategies) {
    
    cat("Strategy: ", strat , "-----------------------\n\n" )
    
    
    strat_data = subset(all_data, strategy == strat & !is.na(rank))
    
    #--------------------------
    
    num_queries_str = nrow(strat_data)
    cat("# of queries: " , num_queries_str, "\n" )
    
    
    #strat_data_res = subset(strat_data,  rank!=0 )
    strat_data_res = strat_data
    num_queries_str_res = nrow(subset(strat_data_res,  rank!=0 ))
    cat("# of queries with results: " ,num_queries_str_res, "\n\n" )
    
    #cat("str_stats_string: " ,str_stats_string, "\n\n" )
    
    
    #--------------------------
    
    
    if (nrow(strat_data_res) >0) {
      strat_data_res$type_rd <- c(0)
      
      if (nrow(strat_data_res[strat_data_res$rank_diff>0 & strat_data_res$rank!=0, ]) > 0) {
        strat_data_res[strat_data_res$rank_diff>0 & strat_data_res$rank!=0, ]$type_rd =1
      }
      #there were not retrieved, hence they are type of deterioration
      if (nrow(strat_data_res[strat_data_res$rank_diff>0 & strat_data_res$rank==0, ]) > 0) {
        strat_data_res[strat_data_res$rank_diff>0 & strat_data_res$rank==0, ]$type_rd =-2
      }
      if (nrow(strat_data_res[strat_data_res$rank_diff<0, ]) > 0) {
        strat_data_res[strat_data_res$rank_diff<0, ]$type_rd =-1
      }
      strat_data_res$rec_rank <- 1 /strat_data_res$rank
      
    }else{
      strat_data_res$type_rd = numeric(0)
      strat_data_res$rec_rank = numeric(0)
    }
    
    strat_data_res = categorize_ranks_param(strat_data_res, rank_threshold, num_cat_bins)
    
    ## DATA MERGED -> WITH NO OUTLIERS, DEPENDING ON THE PARAMETERS
    strat_data_merged = merge(baseline_data_retrieved, strat_data_res, by = c("dataset","system", "bug_id"))
    strat_data_merged = remove_outliers_fn(strat_data_merged, input_rank, input_rank2)
    
    if (sys_name == overall_sys) {
      
      outf_file_str =  paste(out_folder, "/results_", data_set,"_",tr_approach, "_", rank_str,"_", strat,".csv", sep="");
      #outf_file_str =  paste(out_folder, "/results_", strat, ".csv", sep="");
      write.table(strat_data_merged, file = outf_file_str,row.names = FALSE,  sep=";")
    }
    
    #--------------------------
    #FIXME? outliers are not removed
    
    strat_data_merged_temp = merge(baseline_data_retrieved, strat_data, by = c("dataset", "system", "bug_id"))
    data_below_temp =subset(strat_data_merged_temp, rank.x>input_rank )
    if (!is.null(input_rank2)) {
      data_below_temp=subset(data_below_temp, rank.x<=input_rank2  )
    }
    
    num_queries_str_below = nrow(data_below_temp)
    num_queries_str_below_res = nrow(subset(data_below_temp, rank.y !=0  ))
    num_queries_str_below_not_retr = num_queries_str_below - num_queries_str_below_res
    
    str_stats_string <<- paste(str_stats_string, paste( sys_name, strat, num_queries_str, num_queries_str_res, num_queries_str_below, num_queries_str_below_res, sep = ";"), "\n", sep =  "")
    
    #--------------------------
    
    
    strat_data_merged$improv_size.y =  strat_data_merged$rank_diff.y / strat_data_merged$rank.x
    
    data_below=subset(strat_data_merged, rank.x>input_rank )
    if (!is.null(input_rank2)) {
      data_below=subset(data_below, rank.x<=input_rank2 )
    }
    
    num_queries_str_below = nrow(data_below)
    cat("# of below top-", input_rank ," queries with results: " ,num_queries_str_below, "\n" )
    
    #--------------------------
    
    #mrr & map
    
    data_below_mrr_map = subset(data_below,rank.y!=0)
    
    mrr_baseline = mean(data_below_mrr_map$rec_rank.x)
    mrr_str = mean(data_below_mrr_map$rec_rank.y)
    mrr_diff = mrr_str - mrr_baseline
    mrr_improv = mrr_diff/mrr_baseline
    cat("mrr base: " ,mrr_baseline, "\n" )
    cat("mrr str: " ,mrr_str, "\n" )
    cat("mrr diff: " ,mrr_diff, "\n" )
    
    map_baseline = mean(data_below_mrr_map$avg_prec.x)
    map_str = mean(data_below_mrr_map$avg_prec.y)
    map_diff = map_str - map_baseline
    map_improv = map_diff / map_baseline
    cat("map base: " ,map_baseline, "\n" )
    cat("map str: " ,map_str, "\n" )
    cat("map diff: " ,map_diff, "\n" )
    
    str_mrr_string <<- paste(str_mrr_string, paste(sys_name, strat, rank_str, num_queries_str_below, num_queries_str_below_not_retr,  mrr_baseline, mrr_str, mrr_diff, mrr_improv, map_baseline, map_str, map_diff, map_improv, sep = ";"), "\n", sep =  "")
    
        #--------------------------
    
    # improv and deter
    data_below_improv = subset(data_below,type_rd.y == 1)
    data_below_deter = subset(data_below,type_rd.y == -1)
    data_below_same = subset(data_below,type_rd.y == 0)
    
    num_same_str = nrow( data_below_same)
    num_improv_str = nrow(data_below_improv)
    num_deter_str = nrow(data_below_deter)
    cat("# queries improved: " ,num_improv_str, "\n" )
    cat("# queries deteriorated: " ,num_deter_str, "\n" )
    num_improv_diff = num_improv_str - num_deter_str
    
    improv_size = c(mean(data_below_improv$improv_size.y), median(data_below_improv$improv_size.y))
    deter_size = c(mean(data_below_deter$improv_size.y), median(data_below_deter$improv_size.y))
    improv_size2 = c(mean(data_below_improv$rank_diff.y), median(data_below_improv$rank_diff.y))
    deter_size2 = c(mean(data_below_deter$rank_diff.y), median(data_below_deter$rank_diff.y))
    
    str_imp_det_string <<- paste(str_imp_det_string, paste( sys_name, strat, rank_str, num_improv_str, num_deter_str, num_improv_diff,
                                                            improv_size[1], improv_size[2], deter_size[1], deter_size[2],
                                                            improv_size2[1], improv_size2[2], deter_size2[1], deter_size2[2],
                                                            num_same_str,  num_improv_str/num_queries_str_below, num_deter_str/num_queries_str_below, num_same_str/num_queries_str_below , sep = ";"), "\n", sep =  "")
    
    
    #--------------------------
    
    
    
    
    # improv from below to the top
    #-----------------------------------------------------------
    str_imp_cat_string <<- paste(str_imp_cat_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_avg_string  <<- paste(str_imp_cat_avg_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_med_string  <<- paste(str_imp_cat_med_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_avg_string2  <<- paste(str_imp_cat_avg_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_imp_cat_med_string2  <<- paste(str_imp_cat_med_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_same_cat_string  <<- paste(str_same_cat_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_nonretr_cat_string  <<- paste(str_nonretr_cat_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    
    
    #-------------------
    #unchanged queries
    
    accum_same = 0
    acumm_data_comb  <- NULL
    for (c_to in unique(improv_catgs$to)) {
      
      data_comb = subset(data_below_same,  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      accum_same = accum_same+ count_comb
      
      str_same_cat_string <<- paste(str_same_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      
    }
    str_same_cat_string <<- paste(str_same_cat_string, paste( "", accum_same, sep = ";"), sep =  "")
    
    
    #-------------------
    
    #queries that don't retrieve
    data_below_nonretr = subset(data_below,type_rd.y == -2)
    
    accum_nonretr = 0
    acumm_data_comb  <- NULL
    for (c_to in unique(improv_catgs$to)) {
      
      data_comb = subset(data_below_nonretr,  rank_cat.x == c_to )
      count_comb = nrow(data_comb)
      accum_nonretr = accum_nonretr+ count_comb
      
      str_nonretr_cat_string <<- paste(str_nonretr_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      
    }
    str_nonretr_cat_string <<- paste(str_nonretr_cat_string, paste( "", accum_nonretr, sep = ";"), sep =  "")
    
    
    #-------------------
    
    
    #improv "to"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_to in unique(improv_catgs$to)) {
      
      data_comb = subset(data_below_improv,  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      if(count_comb == 0 ){
        improv_size = c(0,0)
        improv_size2 = c(0,0)
      }
      
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
      str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
      str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
      str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
      
    }
    str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    
    n_acumm_data_comb = nrow(acumm_data_comb)
    
    improv_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    improv_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    
    if(n_acumm_data_comb == 0){
      improv_size = c(0,0)
      improv_size2 = c(0,0)
    }
    
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
    
    #improv "from"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_from in unique(improv_catgs$from)) {
      
      data_comb = subset(data_below_improv,  rank_cat.x == c_from )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      
      if(count_comb == 0 ){
        improv_size = c(0,0)
        improv_size2 = c(0,0)
      }
      
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
      str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
      str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
      str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
      
    }
    str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    
    n_acumm_data_comb = nrow(acumm_data_comb)
    
    improv_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    improv_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    
    
    if(n_acumm_data_comb == 0){
      improv_size = c(0,0)
      improv_size2 = c(0,0)
    }
    
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
    
    
    #improv "above rank"
    #-------------------
    
    data_comb = subset(data_below_improv,  rank.y <= input_rank & rank.y != 0)
    count_comb = nrow(data_comb)
    
    improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
    improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
    
    
    if(count_comb == 0 ){
      improv_size = c(0,0)
      improv_size2 = c(0,0)
    }
    
    str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
    
    #-------------------
    
    prefix = "# of queries improved ";
    for (idx in 1:nrow(improv_catgs)) {
      
      c_from = as.character(improv_catgs[idx, 1])
      c_to = as.character(improv_catgs[idx, 2])
      
      data_comb = subset(data_below_improv, rank_cat.x == c_from &  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      
      improv_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      improv_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      
      if(count_comb == 0 ){
        improv_size = c(0,0)
        improv_size2 = c(0,0)
      }
      
      cat(prefix,"[", c_from," --> ",c_to," ] : ", count_comb, "\n" )
      str_imp_cat_string <<- paste(str_imp_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, paste( "", improv_size[1], sep = ";"), sep =  "")
      str_imp_cat_med_string <<- paste(str_imp_cat_med_string, paste( "", improv_size[2], sep = ";"), sep =  "")
      str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, paste( "", improv_size2[1], sep = ";"), sep =  "")
      str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, paste( "", improv_size2[2], sep = ";"), sep =  "")
      
    }
    
    str_imp_cat_string <<- paste(str_imp_cat_string, "\n", sep =  "")
    str_same_cat_string <<- paste(str_same_cat_string, "\n", sep =  "")
    str_nonretr_cat_string <<- paste(str_nonretr_cat_string, "\n", sep =  "")
    str_imp_cat_avg_string <<- paste(str_imp_cat_avg_string, "\n", sep =  "")
    str_imp_cat_med_string <<- paste(str_imp_cat_med_string, "\n", sep =  "")
    str_imp_cat_avg_string2 <<- paste(str_imp_cat_avg_string2, "\n", sep =  "")
    str_imp_cat_med_string2 <<- paste(str_imp_cat_med_string2, "\n", sep =  "")
    
    #deterioration
    #----------------------------------------
    
    
    
    str_det_cat_string <<- paste(str_det_cat_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste(sys_name, strat, rank_str, sep = ";"), sep =  "")
    
    
    
    
    #deter "from"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_from in unique(deter_catgs$from)) {
      
      data_comb = subset(data_below_deter,  rank_cat.x == c_from )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      deter_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      deter_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      if(count_comb == 0 ){
        deter_size = c(0,0)
        deter_size2 = c(0,0)
      }
      
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_det_cat_string <<- paste(str_det_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
      str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
      str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
      str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
      
    }
    str_det_cat_string <<- paste(str_det_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    
    n_acumm_data_comb = nrow(acumm_data_comb)
    deter_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    deter_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    
    if(n_acumm_data_comb == 0 ){
      deter_size = c(0,0)
      deter_size2 = c(0,0)
    }
    
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
    
    
    #deter "to"
    #-------------------
    
    accum_improv = 0
    acumm_data_comb  <- NULL
    for (c_to in unique(deter_catgs$to)) {
      
      data_comb = subset(data_below_deter,  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      accum_improv = accum_improv+ count_comb
      
      deter_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      deter_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      if(count_comb == 0 ){
        deter_size = c(0,0)
        deter_size2 = c(0,0)
      }
      
      if (is.null(acumm_data_comb)) {
        acumm_data_comb = data_comb
      }else{
        acumm_data_comb <- rbind(acumm_data_comb,data_comb)
      }
      
      str_det_cat_string <<- paste(str_det_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
      str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
      str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
      str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
      
    }
    str_det_cat_string <<- paste(str_det_cat_string, paste( "", accum_improv, sep = ";"), sep =  "")
    n_acumm_data_comb = nrow(acumm_data_comb)
    deter_size = c(mean(acumm_data_comb$improv_size.y), median(acumm_data_comb$improv_size.y))
    deter_size2 = c(mean(acumm_data_comb$rank_diff.y), median(acumm_data_comb$rank_diff.y))
    
    
    if(n_acumm_data_comb == 0 ){
      deter_size = c(0,0)
      deter_size2 = c(0,0)
    }
    
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
    
    prefix = "# of queries deteriorated ";
    for (idx in 1:nrow(deter_catgs)) {
      
      c_from = as.character(deter_catgs[idx, 1])
      c_to = as.character(deter_catgs[idx, 2])
      
      data_comb = subset(data_below_deter, rank_cat.x == c_from &  rank_cat.y == c_to )
      count_comb = nrow(data_comb)
      deter_size = c(mean(data_comb$improv_size.y), median(data_comb$improv_size.y))
      deter_size2 = c(mean(data_comb$rank_diff.y), median(data_comb$rank_diff.y))
      
      if(count_comb == 0 ){
        deter_size = c(0,0)
        deter_size2 = c(0,0)
      }
      
      cat(prefix,"[", c_from," --> ",c_to," ] : ", count_comb, "\n" )
      
      str_det_cat_string <<- paste(str_det_cat_string, paste( "", count_comb, sep = ";"), sep =  "")
      str_det_cat_avg_string <<- paste(str_det_cat_avg_string, paste( "", deter_size[1], sep = ";"), sep =  "")
      str_det_cat_med_string <<- paste(str_det_cat_med_string, paste( "", deter_size[2], sep = ";"), sep =  "")
      str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, paste( "", deter_size2[1], sep = ";"), sep =  "")
      str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, paste( "", deter_size2[2], sep = ";"), sep =  "")
    }
    
    str_det_cat_string <<- paste(str_det_cat_string, "\n", sep =  "")
    str_det_cat_avg_string <<- paste(str_det_cat_avg_string, "\n", sep =  "")
    str_det_cat_med_string <<- paste(str_det_cat_med_string, "\n", sep =  "")
    str_det_cat_avg_string2 <<- paste(str_det_cat_avg_string2, "\n", sep =  "")
    str_det_cat_med_string2 <<- paste(str_det_cat_med_string2, "\n", sep =  "")
    
    #-----------------------------------------------
    
    
    #HITS statistics
    
    d_good_base = subset(data_below, rank.x <= rank_threshold & rank.x != 0)
    d_good_strat = subset(data_below, rank.y <= rank_threshold & rank.y != 0)
    
    n_hits_base = nrow(d_good_base)
    n_hits_strat = nrow(d_good_strat)
    p_hits_base = n_hits_base / num_queries_str_below
    p_hits_strat = n_hits_strat / num_queries_str_below
    p_hits_diff = n_hits_strat - n_hits_base
    p_hits_improv = p_hits_diff / n_hits_base
    if(n_hits_base == 0 && n_hits_strat == 0){
      p_hits_improv = 0
    } else if(n_hits_base == 0){
      p_hits_improv = NA
    }
    
    d_bad_good = subset(data_below, (rank.x > rank_threshold | rank.x ==0) & (rank.y <= rank_threshold & rank.y != 0))
    d_good_bad = subset(data_below, (rank.x <= rank_threshold & rank.x != 0) & (rank.y > rank_threshold | rank.y == 0))
    d_good_good = subset(data_below, (rank.x <= rank_threshold & rank.x != 0) & (rank.y <= rank_threshold & rank.y != 0))
    d_bad_bad = subset(data_below, (rank.x > rank_threshold | rank.x ==0) & (rank.y > rank_threshold | rank.y == 0))
    
    n_bad_good = nrow(d_bad_good)
    n_good_bad = nrow(d_good_bad)
    n_good_good = nrow(d_good_good)
    n_bad_bad = nrow(d_bad_bad)
    p_bad_good = n_bad_good / num_queries_str_below
    p_good_bad = n_good_bad / num_queries_str_below
    p_good_good = n_good_good / num_queries_str_below
    p_bad_bad = n_bad_bad / num_queries_str_below
    
    hits_stats_string <<- paste(hits_stats_string, paste(sys_name, strat, rank_str, num_queries_str_below, n_hits_base, n_hits_strat,
                                                         p_hits_base, p_hits_strat, p_hits_diff, p_hits_improv,
                                                         n_bad_good, n_good_bad, n_good_good, n_bad_bad,
                                                         p_bad_good, p_good_bad, p_good_good, p_bad_bad, 
                                                         n_bad_good + n_good_bad + n_good_good + n_bad_bad,
                                                         sep = ";"), "\n", sep =  "")
    
    #-----------------------------------------------
    
    
    cat("\n" )
    
    
  }
  
  
}


run_all_analysis <- function(bugs_ds, input_rank, input_rank2){
  #--------------------------------------------------------------------------
  
  
  str_base_stats_string <<- ""
  str_stats_string <<- ""
  str_mrr_string <<- ""
  str_imp_det_string <<- ""
  str_imp_cat_string <<- ""
  str_imp_cat_avg_string <<- ""
  str_imp_cat_med_string <<- ""
  str_imp_cat_avg_string2 <<- ""
  str_imp_cat_med_string2 <<- ""
  str_det_cat_string <<- ""
  str_det_cat_avg_string <<- ""
  str_det_cat_med_string <<- ""
  str_det_cat_avg_string2 <<- ""
  str_det_cat_med_string2 <<- ""
  
  str_mrr_string_all <<- ""
  
  str_same_cat_string <<- ""
  str_nonretr_cat_string <<- ""
  
  hits_stats_string<<- ""
  
  
  #--------------------------------------------------------------
  
  
  overall_sys <<- "overall"
  
  
  if (!is.null(input_rank2)) {
    rank_str = paste("(",input_rank, ",",input_rank2,"]", sep="")
  }else{
    rank_str = paste("(",input_rank, ", inf]", sep="")
  }
  
  
  all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = "")
  
  all_data_bugs = merge(all_data_set, bugs_ds, by =  c ("dataset", "system", "bug_id"))
  
  compute_stats(all_data_bugs,overall_sys, input_rank, input_rank2, rank_str)
  
  if (compute_systems) {
    systems = unique(all_data_bugs$system)
    for (sys_name in systems) {
      compute_stats(all_data_bugs,sys_name, input_rank, input_rank2, rank_str)
    }
  }
  
  
  out_file =  paste(out_folder, "/analysis_", data_set,"_",tr_approach, "_", rank_str,".csv", sep="");
  if (file.exists(out_file)) {
    file.remove(out_file)
  }
  print_results(out_file)
  
  cat("Results written in ", out_file, "\n")
}


##---------------------------------------------------

#ranks = data.frame(from = c(0, 10, 10, 20, 30), to = c(NA, NA, 20, 30, NA))
#ranks = data.frame(from = c( 10, 20, 30), to = c(NA, NA, NA))
ranks = data.frame(from = c(0), to = c(NA))

determine_category_transitions(rank_threshold, num_cat_bins)

for (idx in 1:nrow(ranks)) {
  
  from_rank = ranks[idx, 1]
  to_rank = ranks[idx, 2]
  
  if (is.na(to_rank)) {
    to_rank <- NULL
  }
  
  bugs  = read.csv(list_of_bugs, sep = ";", header = TRUE)
  cols= c ("dataset", "system", "bug_id")
  colnames(bugs) <- cols
  bugs_ds = bugs
  if (!is.null(data_set)){
	bugs_ds = subset(bugs, dataset == data_set )
  }
  #bugs_ds$system = tolower(bugs_ds$system)
    
  run_all_analysis(bugs_ds, from_rank, to_rank)
  
}
