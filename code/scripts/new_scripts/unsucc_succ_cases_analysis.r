##---------
library("dplyr")
library("ggpubr")
library("effsize")

rm(list=ls())

base_folder =  'C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/3_results-analysis2/all_granularities/'
data_file = paste(base_folder, 'all_results.csv', sep = "")

#load data
all_data_set = read.csv(data_file, sep = ";", header = TRUE, na.strings = c("","NA"))

ds5 = subset(all_data_set, n ==5)
c = boxplot(ds5$rank_diff.y ~ ds5$strat, col=(c("lightgray")), ylim = c(-100, 30))
c$stats