#!/usr/bin/env python3

"""
Description
"""


import argparse
import csv
import glob
import json
import os
import sys


def printerr(*messages, **kwargs):
    if 'file' in kwargs:
        del kwargs['file']
    print(*messages, file=sys.stderr, **kwargs)


ds2g = {
    'LB': 'CLASS',
    'BRT': 'FILE',
    'D4J': 'METHOD',
    'B4BL': 'FILE',
    'QQ': 'METHOD'
}
    

def main(ind, outd):
    os.makedirs(outd, exist_ok=True)
    for fn in glob.glob(os.path.join(ind, '*.json')):
        sfn = os.path.basename(fn)
        if sfn.startswith('comp'):
            continue

        print(fn)

        with open(fn) as inf:
            ps = json.load(inf)['projects']

        with open(os.path.join(outd, sfn.replace('.json', '.csv')), 'w') as of:
            wr = csv.DictWriter(of,
                                ('Dataset', 'System', 'Bug ID', 'Creation Date',
                                 'Title', 'Description', 'Ground Truth'))
            wr.writeheader()
            
            for p, qs in ps.items():
                ds, sys = p.split('__')
                gran = ds2g[ds]

                for q in qs:
                    ooo = {'Dataset': gran, 'System': sys}
                    ooo['Bug ID'] = q['key']
                    ooo['Creation Date'] = q['creationDate']
                    ooo['Title'] = q.get('rawTitle', '')
                    ooo['Description'] = q.get('rawDescription', '')
                    ooo['Ground Truth'] = '\n'.join(q['goldSet'])

                    wr.writerow(ooo)


if __name__ == '__main__':
##    this_script_dir = os.path.dirname(os.path.abspath(__file__))
    parser = argparse.ArgumentParser()
    parser.add_argument('indir')
    parser.add_argument('outdir')
##    parser.add_argument('number', help='An int value', type=int)
##    parser.add_argument('-v', '--verbose', help='Makes it verbose',
##                        action='store_true')
##    parser.add_argument('-a', '--amount', help='amount', type=int)

    args = parser.parse_args()
##    print(args.number)
##    if args.verbose:
##        pass

    main(args.indir, args.outdir)
