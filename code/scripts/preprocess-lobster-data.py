#!/usr/bin/env python3

"""
Applies tokenizing, identifier splitting, stop word and java terms removal on
previously extracted source file texts and saves them in lobster format.

Input format: {"file": ..., "text": ..., "package_fqn": ...,
               "import_fqns": [...]}
"""


from preprocessing import Preprocessor
import os
import json
import re
import sys


# Terms we want to ignore: stop words and java keywords
IGNORE_TERMS_FILE_NAMES = (
    'stop-words.txt',
    'java-keywords.txt',
)


def create_preprocessor():
    # Directory where this script is located
    dirname = os.path.dirname(__file__)

    # List of terms to be ignored by the tokenizer
    ignore_terms = []

    # Collect the terms we want to ignore
    for ignore_file_name in IGNORE_TERMS_FILE_NAMES:
        with open(os.path.join(dirname, ignore_file_name)) as file:
            ignore_terms.extend(term.strip() for term in file)
            
    # Create our custom tokenizer, it receives the terms we want to ignore
    return Preprocessor(word_chars='a-zA-Z0-9', inter_chars="'",
                                min_length=3, ignore=ignore_terms)


def main(source_dir_path, dest_dir_path):
    os.makedirs(dest_dir_path, exist_ok=True)
    nl_re = re.compile(r'\n|\f|\r')
    preprocessor = create_preprocessor()
    # For converting file paths to class fully qualified names.
    corpus_files = set()
    # For identifying classes mentioned in stack traces
    stack_class_re = re.compile(r'([\w$.]+)\.[\w$<>]+\s*'
                                '\((?:[\w\-_]+\.java:\d+|'
                                'Native Method|Unknown Source)\)')

    system_name = os.path.basename(source_dir_path.rstrip(os.sep))
    print('System name is:', system_name)
    print('Destination directory is:', dest_dir_path)
    def open_output_file(path):
        return open(os.path.join(dest_dir_path, system_name + path), mode='w')
    mapping_file, corpus_file = (
        map(open_output_file, ('_Mapping.txt', '_Corpus.txt')))
        
    for line in open(os.path.join(source_dir_path, 'source-code.json')):
        src_file = json.loads(line)

        if system_name.startswith('aspectj'):
            file_id = src_file['file_path']
        elif system_name.startswith('eclipse') or system_name.startswith('swt'):
            file_id = src_file.get('file_package_path', '')

        processed_text = ' '.join(preprocessor.preprocess(src_file['text']))

        if not processed_text:
            print('No text after preprocessing for file',
                  src_file['file_path'],
                  file=sys.stderr)
            continue

        corpus_files.add(file_id)
        print(file_id, file=mapping_file)
        print(processed_text, end='\n\n', file=corpus_file)

    mapping_file.close()
    corpus_file.close()

    queries_file, original_queries_file, stack_file = (
        map(open_output_file, ('_Queries.txt', '_Original_Queries.txt',
                               '_Stack.txt')))
    bug_id = 1

    for line in open(os.path.join(source_dir_path, 'bug-reports.json')):
        bug_report = json.loads(line)
        title = bug_report.get('title', '') or ''
        desc = bug_report.get('description', '') or ''

        processed_text = ' '.join(preprocessor.preprocess(title, desc))

        if not processed_text:
            print('No text after preprocessing for bug',
                  bug_report['key'],
                  file=sys.stderr)
            continue

        original_text = nl_re.sub(' ', ' '.join((title, desc)))

        fixed_files = []
        for ff in bug_report['fixed_files']:
            if (system_name.startswith('eclipse') or
                system_name.startswith('swt')):
                ff = ''.join(ff.rsplit('.java', 1))

            if ff not in corpus_files:
                print('WARNING: Not in corpus files:', ff, file=sys.stderr)
            else:
                fixed_files.append(ff)
            
        if not fixed_files:
            print('WARNING: No fixed files for bug', bug_report['key'],
                  file=sys.stderr)
            continue
        
        fixed_files_string = '\n'.join([str(len(fixed_files))]+fixed_files)

        print(bug_id, file=queries_file)
        print(bug_id, bug_report['key'], file=original_queries_file)
        print(bug_id, file=stack_file)

        print(processed_text, file=queries_file)
        print(original_text, file=original_queries_file)
        print(processed_text, file=stack_file)

        stack_classes = set(stack_class_re.findall(original_text))
        sc_string = '\n'.join([str(len(stack_classes))]+list(stack_classes))

        print(fixed_files_string, file=queries_file, end='\n\n')
        print(fixed_files_string, file=original_queries_file, end='\n\n')
        print(sc_string, file=stack_file, end='\n\n')

        bug_id += 1


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('ARGUMENTS: RAW_BUG_REPORTS_DIR DEST_DIRECTORY',
              file=sys.stderr)
    else:
        main(*map(os.path.abspath, sys.argv[1:]))
