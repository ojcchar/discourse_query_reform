retrieval_results_file = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/d4j/query_results.csv"
results_data  = read.csv(retrieval_results_file, sep = ";", header = TRUE)  

out_file = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/results/d4j/query_results_sample.csv"
file.remove(out_file)

systems = c("Lang", "Math", "Time")
num_instances = 10

for (sys in systems) {
  
  system_data = subset(results_data, system == sys)
  
  rank1_data = subset(system_data, rank == 1)
  rank10_data = subset(system_data, rank >=2 & rank <=10)
  rankother_data = subset(system_data, rank >10)
  
  sample_data =rank1_data
  if (nrow(sample_data) >10 ){
    sample_data = sample_data[sample(1:nrow(sample_data), num_instances),]
  }
  write.table(sample_data, out_file, append=TRUE, sep=";", row.names = FALSE, col.names = FALSE)
  
  sample_data =rank10_data
  if (nrow(sample_data) >10 ){
    sample_data = sample_data[sample(1:nrow(sample_data), num_instances),]
  }
  write.table(sample_data, out_file, append=TRUE, sep=";", row.names = FALSE, col.names = FALSE)
  
  sample_data =rankother_data
  if (nrow(sample_data) >10 ){
    sample_data = sample_data[sample(1:nrow(sample_data), num_instances),]
  }
  write.table(sample_data, out_file, append=TRUE, sep=";", row.names = FALSE, col.names = FALSE)
  
}