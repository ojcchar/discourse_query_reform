export JAVA_HOME=/home/Oscar/Documents/jdk1.8.0_91
export JAVA_EXEC=$JAVA_HOME/bin/java

export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation
export CLASSPATH=$BASE_DIR/code/discourse_query_reform/code/query-generation/target/query-generation-0.0.1.jar

export DATA_FOLDER=$BASE_DIR/code/discourse_query_reform/data/existing_data_sets/buglocator_data
#export OUTPUT_FOLDER=$BASE_DIR/output/parsed_bugs_lobser_data
export OUTPUT_FOLDER=$BASE_DIR/output/buglocator-sample-hard-to-retrieve
export SYSTEMS=aspectj-1.5.3,eclipse-3.1,swt-3.1
export AUTOTAG=n
export PATTERN_LIST_DIR=$BASE_DIR/code/discourse_query_reform/code/query-generation/pattern_list
export LIST_OF_BUGS=$DATA_FOLDER/buglocator-sample-hard-to-retrieve-queries.csv

$JAVA_EXEC seers.disqueryreform.querygen.parsing.LobsterMainParsing $DATA_FOLDER $OUTPUT_FOLDER $SYSTEMS $AUTOTAG $PATTERN_LIST_DIR $LIST_OF_BUGS
