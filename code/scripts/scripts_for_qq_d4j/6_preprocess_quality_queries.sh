export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation
export D4J_DATA_DIR=$BASE_DIR/code/discourse_query_reform/data/existing_data_sets/query_quality_data

export DATA_DIR=$D4J_DATA_DIR/queries
export SCRIPT_PATH=$BASE_DIR/code/ir4se-fwk/ir4se-fwk/src/scripts
export CURDIR=$PWD
export OUT_DIR=$D4J_DATA_DIR/queries_prep

mkdir $OUT_DIR

cd $SCRIPT_PATH
for file in $DATA_DIR/*.json; do
    echo "Processing $file"
    python3 preprocess-source-texts.py < $file > $file.prep
done

rm $OUT_DIR/*
mv $DATA_DIR/*.prep $OUT_DIR

cd $CURDIR
