export JAVA_HOME=/home/Oscar/Documents/jdk1.8.0_91
export JAVA_EXEC=$JAVA_HOME/bin/java

export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation
export CLASSPATH=$BASE_DIR/code/discourse_query_reform/code/query-generation/target/query-generation-0.0.1.jar
export DATA_FOLDER=$BASE_DIR/code/discourse_query_reform/data/existing_data_sets/lobster_data
#export OUTPUT_FOLDER=$BASE_DIR/output/parsed_bugs_lobser_data
export OUTPUT_FOLDER=$BASE_DIR/output/lobster-all-hard-to-retrieve
export SYSTEMS=argouml-0.22,jabref-2.6,bookkeeper-4.1.0,derby-10.7.1.1,derby-10.9.1.0,hibernate-3.5.0b2,jedit-4.3,lucene-4.0,mahout-0.8,openjpa-2.0.1,openjpa-2.2.0,pig-0.11.1,pig-0.8.0,solr-4.4.0,tika-1.3,zookeeper-3.4.5
export AUTOTAG=n
export PATTERN_LIST_DIR=$BASE_DIR/code/discourse_query_reform/code/query-generation/pattern_list
export LIST_OF_BUGS=$DATA_FOLDER/lobster-all-hard-to-retrieve-queries.csv

$JAVA_EXEC seers.disqueryreform.querygen.parsing.LobsterMainParsing $DATA_FOLDER $OUTPUT_FOLDER $SYSTEMS $AUTOTAG $PATTERN_LIST_DIR $LIST_OF_BUGS
