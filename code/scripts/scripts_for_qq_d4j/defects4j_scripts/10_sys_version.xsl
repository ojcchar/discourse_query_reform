<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" />
<xsl:template match="/issue">
	 <xsl:value-of select="concat(key,';')"/>
	 <xsl:for-each select="fields/versions/name">
	   <xsl:value-of select="."/>
	   <xsl:if test="position() != last()">
		  <xsl:text>,</xsl:text>
	   </xsl:if>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
