system=time

mkdir $system/short;
mkdir $system/short/$system;
for file in $system/*.xml; do
echo "Processing $file file..";
bugid=$(xmllint --xpath /issue/number $file);
title=$(xmllint --xpath /issue/title $file);
body=$(xmllint --xpath /issue/body $file);
content=$(echo -e "<bug>$bugid\n$title\n$body</bug>");
content=$(echo "${content/<number>/<id>}");
content=$(echo "${content/<\/number>/</id>}");
content=$(echo "${content/<body>/<description>}");
content=$(echo "${content/<\/body>/</description>}");
xmlname=$(echo "$system/short/$file");
echo "$content" > $xmlname;
xmllint --format "$xmlname" -o "$xmlname";
done
