system=$1

mkdir $system/short
mkdir $system/short/$system;
mkdir short;
for file in $system/*.xml; do
echo "Processing $file file..";
bugid=$(xmllint --xpath "/issue/key/text()" $file);
title=$(xmllint --xpath "/issue/fields/summary/text()" $file);
body=$(xmllint --xpath "/issue/fields/description/text()" $file);
content=$(echo -e "<bug><id>$bugid</id>\n<title>$title</title>\n<description>$body</description></bug>");
xmlname=$(echo "$system/short/$file");
echo "$content" > $xmlname;
xmllint --format "$xmlname" -o "$xmlname";
done

