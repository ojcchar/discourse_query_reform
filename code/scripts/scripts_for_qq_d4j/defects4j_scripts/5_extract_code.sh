export JAVA_HOME=/home/Oscar/Documents/jdk1.8.0_91
export JAVA_EXEC=$JAVA_HOME/bin/java

export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation
export CLASSPATH=$BASE_DIR/code/discourse_query_reform/code/corpus-processor/target/corpus-processor-0.0.1.jar

export BUGS_DIR=/home/Oscar/Documents/query_based_fault_localization/bugs_data_src
export BUGS_FILE=$BASE_DIR/code/discourse_query_reform/data/existing_data_sets/defects4j_data/bugs_with_method_goldset.csv
export OUTPUT_DIR=$BASE_DIR/output/dj4_src_code_corpus

$JAVA_EXEC seers.disqueryreform.corpus.D4JVocabExtractorMain $BUGS_DIR $BUGS_FILE $OUTPUT_DIR
