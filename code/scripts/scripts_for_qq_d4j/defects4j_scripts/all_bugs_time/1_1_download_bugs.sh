mkdir data

#echo $1".."$2" - "$3" per page"

count_bugs=0
num_bugs_per_page=100

echo "initial: "$count_bugs

# for each page
for ((pn = 1; pn <= 5; pn++)); do

 #request
 #parse the json
 #read each line of the parsing, i.e., each bug
 count=0 curl --user "ojcchar:contcar11.GIThub" https://api.github.com/repos/JodaOrg/joda-time/issues?state=all\&sort=created\&direction=asc\&page=$pn\&per_page=$((num_bugs_per_page)) \
 | json -0 -a \
 | while read -r line; \
 do (( count++ ));

  #get the id the pull_request fields
  id=$(echo $line | json number);
  is_pull=$(echo $line | json pull_request);

  line_count_bugs=0;

  #not a pull request --> it is a bug
  if [  -z "$is_pull" ]
     then
     (( line_count_bugs++ ));
     echo $id >> only_bugs.csv;
     echo $line | json > data/$id.json;
  else
    # pull request
    echo $id >> pull_requests.csv;
  fi

  count_bugs=$((count_bugs + line_count_bugs));

  #debugging msgs
  echo "done line "$count": "$line_count_bugs
  echo "total # of bugs: "$count_bugs

 done;
done;

echo "done"

