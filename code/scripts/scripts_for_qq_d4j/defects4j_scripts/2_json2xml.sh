#### Convert JSON to XML ####
system=$1

for file in ./$system/*.json; do
echo "Processing $file file..";
content=$(cat $file); content=$(echo "{ \"issue\": $content }");
xmlname=${file/json/xml};
json2xml -j "$content" -o "$xmlname";
sed -i 's/48x48/x48x48/g' "$xmlname" && sed -i 's/24x24/x24x24/g' "$xmlname" && sed -i 's/16x16/x16x16/g' "$xmlname" && sed -i 's/32x32/x32x32/g' "$xmlname"
xmllint --format "$xmlname" -o "$xmlname"
done

