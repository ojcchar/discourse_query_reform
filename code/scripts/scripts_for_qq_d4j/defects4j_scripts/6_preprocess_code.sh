export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation

export DATA_DIR=$BASE_DIR/output/dj4_src_code_corpus
export SCRIPT_PATH=$BASE_DIR/code/ir4se-fwk/ir4se-fwk/src/scripts
export CURDIR=$PWD
export OUT_DIR=$BASE_DIR/output/dj4_src_code_corpus_prep

cd $SCRIPT_PATH
for file in $DATA_DIR/*.json; do
    echo "Processing $file"
    python3 preprocess-source-texts.py < $file > $file.prep
done

rm $OUT_DIR/*
mv $DATA_DIR/*.prep $OUT_DIR

cd $CURDIR
