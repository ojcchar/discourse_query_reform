export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation

export CURDIR=$PWD
export SCRIPT_PATH=$BASE_DIR/code/ir4se-fwk/ir4se-fwk/src/scripts

#export QUERIES_FOLDER=d4j_generated_queries
#export QUERIES_FOLDER=d4j_generated_queries_no_code
#export QUERIES_FOLDER=d4j_generated_queries_auto_coding
export QUERIES_FOLDER=query_quality-generated-all-hard-to-retrieve-queries_and

cd $SCRIPT_PATH

mkdir $BASE_DIR/output/$QUERIES_FOLDER\_prep

for strat in ALL_TEXT OB EB S2R OTHER OB_EB OB_S2R EB_S2R OB_EB_S2R OB_OTHER EB_OTHER S2R_OTHER OB_EB_OTHER OB_S2R_OTHER EB_S2R_OTHER; do

	export DATA_DIR=$BASE_DIR/output/$QUERIES_FOLDER/$strat
	export OUT_DIR=$BASE_DIR/output/$QUERIES_FOLDER\_prep/$strat

	mkdir $OUT_DIR

	for file in $DATA_DIR/*.json; do
		echo "Processing $file"
		python3 preprocess-source-texts.py < $file > $file.prep
	done

	rm $OUT_DIR/*
	mv $DATA_DIR/*.prep $OUT_DIR

done

cd $CURDIR


