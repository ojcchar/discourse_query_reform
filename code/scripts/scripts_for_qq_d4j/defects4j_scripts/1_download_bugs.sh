
mkdir lang
mkdir math
mkdir time

#for i in LANG-259 LANG-281 LANG-292 LANG-295 LANG-299 LANG-300 LANG-303 LANG-304 LANG-315 LANG-328 LANG-346 LANG-363 LANG-365 LANG-368 LANG-380 LANG-393 LANG-412 LANG-419 LANG-432 LANG-457 LANG-477 LANG-480 LANG-521 LANG-538 LANG-552 LANG-586 LANG-587 LANG-59 LANG-624 LANG-636 LANG-638 LANG-645 LANG-658 LANG-662 LANG-664 LANG-677 LANG-710 LANG-719 LANG-746 LANG-747 LANG-788 LANG-805 LANG-807 LANG-822 LANG-831 LANG-832 LANG-857 LANG-879 LANG-882; do curl "https://issues.apache.org/jira/rest/api/2/issue/$i" | json -a > "lang/$i.json"; done

#for i in MATH-1005 MATH-1021 MATH-166 MATH-167 MATH-175 MATH-198 MATH-200 MATH-209 MATH-221 MATH-238 MATH-241 MATH-259 MATH-273 MATH-274 MATH-280 MATH-305 MATH-318 MATH-322 MATH-326 MATH-329 MATH-338 MATH-344 MATH-358 MATH-362 MATH-369 MATH-371 MATH-377 MATH-393 MATH-482 MATH-546 MATH-552 MATH-554 MATH-567 MATH-60 MATH-618 MATH-631 MATH-645 MATH-679 MATH-691 MATH-695 MATH-704 MATH-713 MATH-716 MATH-722 MATH-727 MATH-776 MATH-778 MATH-779 MATH-781 MATH-803 MATH-835 MATH-836 MATH-844 MATH-859 MATH-85 MATH-865 MATH-867 MATH-904 MATH-927 MATH-929 MATH-934 MATH-935 MATH-942 MATH-949 MATH-988; do curl "https://issues.apache.org/jira/rest/api/2/issue/$i" | json -a > "math/$i.json"; done

cp all_bugs_time/data/93.json ./time
cp all_bugs_time/data/77.json ./time
cp all_bugs_time/data/88.json ./time
cp all_bugs_time/data/79.json ./time
cp all_bugs_time/data/28.json ./time
cp all_bugs_time/data/21.json ./time
cp all_bugs_time/data/42.json ./time
cp all_bugs_time/data/43.json ./time
cp all_bugs_time/data/22.json ./time
cp all_bugs_time/data/18.json ./time
cp all_bugs_time/data/8.json ./time

