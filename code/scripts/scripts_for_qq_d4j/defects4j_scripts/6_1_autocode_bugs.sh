export JAVA_HOME=/home/Oscar/Documents/jdk1.8.0_91
export JAVA_EXEC=$JAVA_HOME/bin/java

export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation
export CLASSPATH=$BASE_DIR/code/discourse_query_reform/code/query-generation/target/query-generation-0.0.1.jar

export DATA_FOLDER=$BASE_DIR/code/discourse_query_reform/data/existing_data_sets/defects4j_data/bug_reports
export OUTPUT_FOLDER=$BASE_DIR/output/d4j_coding_auto
export PATTERN_LIST_DIR=$BASE_DIR/code/discourse_query_reform/code/query-generation/pattern_list

$JAVA_EXEC -Xmx60G seers.disqueryreform.querygen.d4j.autocoding.D4JAutoCoding $DATA_FOLDER $OUTPUT_FOLDER $PATTERN_LIST_DIR
