export JAVA_HOME=/home/Oscar/Documents/jdk1.8.0_91
export JAVA_EXEC=$JAVA_HOME/bin/java

export BASE_DIR=/home/Oscar/Documents/discourse_query_reformulation
export CLASSPATH=$BASE_DIR/code/discourse_query_reform/code/corpus-processor/target/corpus-processor-0.0.1.jar

export SYSTEMS_FILE=$BASE_DIR/code/discourse_query_reform/code/corpus-processor/test_data/systems/systems.csv
export SRC_BASE_FOLDER=$BASE_DIR/data/query_quality_code
export OUTPUT_FOLDER=$BASE_DIR/output/query_quality_code_corpus

$JAVA_EXEC seers.disqueryreform.corpus.querrqualdata.QQDVocabExtractorMain $SYSTEMS_FILE $SRC_BASE_FOLDER $OUTPUT_FOLDER
