#!/usr/bin/env python3

"""
Description
"""


import argparse
import os
import re
import shutil
import sys


failed_system_re = re.compile(r'.*- \[([^]]+)\] Error running system (.*)$')


def printerr(*messages, **kwargs):
    if 'file' in kwargs:
        del kwargs['file']
    print(*messages, file=sys.stderr, **kwargs)


def main(reform_queries_path, full_data_path, log_file_path, output_dir_path):
    reruns = []
    with open(log_file_path) as log_file:
        for line in log_file:
            match =failed_system_re.match(line)
            if match:
                reruns.append(match.groups())

    out_queries_path = os.path.join(output_dir_path, 'queries')
    out_corpus_path = os.path.join(output_dir_path, 'corpus')
    if reruns:
        os.makedirs(out_queries_path, exist_ok=True)
        os.makedirs(out_corpus_path, exist_ok=True)

    copied_systems = set()

    for strat, system in reruns:
        print(strat, system)
        out_strat_path = os.path.join(out_queries_path, strat)
        os.makedirs(out_strat_path, exist_ok=True)
        orig_strat_path = os.path.join(reform_queries_path, strat,
                                       system + '.json')
        
        shutil.copy2(orig_strat_path, out_strat_path)

        if system not in copied_systems:
            shutil.copytree(os.path.join(full_data_path, system),
                            os.path.join(out_corpus_path, system))
            
            copied_systems.add(system)
        


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('reform_queries', help='Queries')
    parser.add_argument('full_data', help='All systems\' source '
                        'code, git repo and bugs')
    parser.add_argument('log_file', help='To find failed executions')
    parser.add_argument('output_dir')

    args = parser.parse_args()

    main(args.reform_queries, args.full_data, args.log_file, args.output_dir)
