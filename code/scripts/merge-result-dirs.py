#!/usr/bin/env python3

"""
Description
"""


import argparse
import os
import sys


def printerr(*messages, **kwargs):
    if 'file' in kwargs:
        del kwargs['file']
    print(*messages, file=sys.stderr, **kwargs)


def merge_files(newfp, oldfp):
    print('Merging ', newfp, oldfp)
    with open(newfp) as newf, open(oldfp, 'a') as oldf:
        # Skip the first two lines
        next(newf)
        next(newf)

        for line in newf:
            print(line, file=oldf, end='')


def merge_thresh(new, old):
    file_names = list(filter(lambda f: (f.endswith('.csv') and
                                        os.path.isfile(os.path.join(new, f))),
                             os.listdir(new)))

    if len(file_names) != 2:
        print(file_names)
        raise Exception('Not exactly 2 files in '+new)

    for f in file_names:
        merge_files(os.path.join(new, f),
                    os.path.join(old, f))


def merge_strat(new_st, old_strat_path):
    # Merge the CSVs at the base of the strategy dir
    merge_thresh(new_st, old_strat_path)
    
    for thresh in sorted(
        filter(lambda n: os.path.isdir(os.path.join(new_st, n)),
               os.listdir(new_st))):
        merge_thresh(os.path.join(new_st, thresh),
                     os.path.join(old_strat_path, thresh))


def main(new_path, old_path):
    for strat in filter(lambda n:os.path.isdir(os.path.join(new_path,n)),
                        os.listdir(new_path)):
        print('Merging strategy ', strat)
        merge_strat(os.path.join(new_path, strat),
                     os.path.join(old_path, strat))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('new_results')
    parser.add_argument('old_results')

    args = parser.parse_args()

    main(args.new_results, args.old_results)
