#!/usr/bin/env python3

import csv
import os
import sys

PREFIXES = {
    'CG-CG': 'rank_lb',
    'BugLocator': 'rank_bl',
    'BRTracer': 'rank_brt',
}

HEADERS = ['project', 'bug_id', 'data_set'] + list(PREFIXES.values())

all_props = {}

def process_file(file_path):
    file_prop = {}
    file_name = os.path.basename(file_path)

    if file_name.startswith('CG-CG'):
        file_prop['project'] = (file_name.replace('CG-CG-', '', 1)
                                .replace('-0.9-3-C-D-stats.csv', '', 1))
        file_prop['data_set'] = 'lb'
    elif file_name.startswith('BugLocator'):
        file_prop['project'] = (file_name.replace('BugLocator-', '', 1)
                                .replace('-stats.csv', '', 1))
        file_prop['data_set'] = 'brt'
    else:
        file_prop['project'] = (file_name.replace('BRTracer-', '', 1)
                                .replace('-stats.csv', '', 1))
        file_prop['data_set'] = 'brt'
        
    with open(file_path) as o:
        file = csv.reader(o, delimiter=';')
        # skip first line
        next(file)
        
        file_headers = next(file)
        
        bug_id_idx, rank_idx = (file_headers.index('query key'),
                                file_headers.index('rank'))

        for line in file:
            # Duplicate file properties
            bug_prop = dict(file_prop)
            bug_prop['bug_id'] = line[bug_id_idx]
            rank = int(float(line[rank_idx]))

            if file_name.startswith('CG-CG'):
                bug_prop['rank_lb'] = rank
            elif file_name.startswith('BugLocator'):
                bug_prop['rank_bl'] = rank
            else:
                bug_prop['rank_brt'] = rank

            key = (bug_prop['bug_id'], bug_prop['bug_id'])
            all_props.setdefault(key, []).append(bug_prop)

# Files to combine are the arguments
file_paths = sys.argv[1:]

for f in file_paths:
    process_file(f)

writer = csv.writer(sys.stdout, delimiter=';')
writer.writerow(HEADERS)

for pl in all_props.values():
    pf = pl[0]

    for p in pl[1:]:
        if 'rank_lb' in p and 'rank_lb' not in pf:
            pf['rank_lb'] = p['rank_lb']
        if 'rank_bl' in p and 'rank_bl' not in pf:
            pf['rank_bl'] = p['rank_bl']
        if 'rank_brt' in p and 'rank_brt' not in pf:
            pf['rank_brt'] = p['rank_brt']
            
    writer.writerow(pf.get(h, '') for h in HEADERS)
