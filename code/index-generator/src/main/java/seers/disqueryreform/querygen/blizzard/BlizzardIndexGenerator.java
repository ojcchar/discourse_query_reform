package seers.disqueryreform.querygen.blizzard;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.nodeTypes.NodeWithIdentifier;
import edu.utdallas.seers.text.preprocessing.IdentifierSplitter;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BlizzardIndexGenerator {
    private final Logger logger = LoggerFactory.getLogger(BlizzardIndexGenerator.class);
    private final JavaParser parser = new JavaParser();
    private final IdentifierSplitter splitter = new IdentifierSplitter();
    private final WordExtractor wordExtractor = new WordExtractor();
    private final Set<String> excludeWords;
    private final Path blizzardPath;

    public BlizzardIndexGenerator(Path blizzardPath, Set<String> excludeWords) {
        this.blizzardPath = blizzardPath;
        this.excludeWords = excludeWords;
    }

    public void createLuceneIndex(String corpus) {
        logger.info("Creating Lucene index for " + corpus);
        IndexWriterConfig writerConfig = new IndexWriterConfig(new WhitespaceAnalyzer());
        writerConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        Path indexPath = blizzardPath.resolve(Paths.get("Lucene-Index", corpus));
        IndexWriter indexWriter;
        try {
            indexWriter = new IndexWriter(FSDirectory.open(indexPath), writerConfig);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        FieldType storedWithTermVectors = new FieldType();
        storedWithTermVectors.setStored(true);
        storedWithTermVectors.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        storedWithTermVectors.setStoreTermVectors(true);
        storedWithTermVectors.setTokenized(true);

        Path corpusPath = blizzardPath.resolve(Paths.get("Corpus", corpus));

        try (Stream<Document> documents = createDocuments(storedWithTermVectors, corpusPath)) {
            documents.forEach(d -> {
                try {
                    indexWriter.addDocument(d);
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });

            indexWriter.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Stream<Document> createDocuments(FieldType storedWithTermVectors, Path corpusPath) throws IOException {
        return Files.walk(corpusPath)
                .filter(p -> p.toString().endsWith(".java"))
                .map(p -> {
                    List<String> terms;

                    try {
                        terms = extractJavaFileText(p);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }

                    Document document = new Document();

                    document.add(new StringField("path", blizzardPath.relativize(p).toString(), Field.Store.YES));
                    document.add(new Field("contents", String.join(" ", terms), storedWithTermVectors));

                    return document;
                });
    }

    @SuppressWarnings("rawtypes")
    private List<String> extractJavaFileText(Path javaFile) throws IOException {
        ParseResult<CompilationUnit> parseResult = parser.parse(javaFile);

        if (!parseResult.isSuccessful()) {
            logger.warn(String.format("Parsing of Java file unsuccessful: %s", javaFile));
        }

        Optional<CompilationUnit> maybeUnit = parseResult.getResult();

        if (!maybeUnit.isPresent()) {
            return Collections.emptyList();
        }

        // Extract identifiers
        CompilationUnit unit = maybeUnit.get();

        Stream<String> identifierWords = unit.stream()
                .filter(n -> n instanceof NodeWithIdentifier)
                .map(n -> ((NodeWithIdentifier) n).getIdentifier());

        // Extract comments
        List<Comment> comments = new ArrayList<>(unit.getComments());

        // The license comment is not being returned there
        unit.getComment().ifPresent(comments::add);

        Stream<String> commentWords = comments.stream()
                .flatMap(c -> wordExtractor.extractWords(c.getContent()).stream());

        // Extract literal strings
        Stream<String> literalWords = unit.stream()
                .filter(n -> n instanceof StringLiteralExpr)
                .flatMap(s -> wordExtractor.extractWords(((StringLiteralExpr) s).asString()).stream());

        return Stream.of(identifierWords, commentWords, literalWords)
                .flatMap(s -> s)
                .flatMap(w -> {
                    List<String> components = splitter.splitIdentifier(w);

                    // Append the original word regardless of whether or not it could be split,
                    // distinct will take care of duplicates
                    return Stream.concat(Stream.of(w.toLowerCase()), components.stream());
                })
                .filter(w -> w.length() > 2 && !excludeWords.contains(w))
                .distinct()
                .collect(Collectors.toList());
    }

    private static class WordExtractor {
        private final Pattern wordsPattern = Pattern.compile("[a-zA-Z]*");

        private ArrayList<String> extractWords(String content) {
            Matcher matcher = wordsPattern.matcher(content);

            ArrayList<String> words = new ArrayList<>();

            while (matcher.find()) {
                words.add(matcher.group());
            }

            return words;
        }
    }
}
