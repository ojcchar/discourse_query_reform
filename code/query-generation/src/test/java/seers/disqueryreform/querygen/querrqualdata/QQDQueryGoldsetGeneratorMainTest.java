package seers.disqueryreform.querygen.querrqualdata;

import org.junit.Test;
import seers.disqueryreform.base.InformationSource;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;

public class QQDQueryGoldsetGeneratorMainTest {

	@Test
	public void test() throws Exception {
		QueryReformStrategy[] values = QueryReformStrategy.values();

		String baseDataFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets";
		String baseQueriesFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/query_quality_data/queries_prep";
		
		String operator = "and";
		String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries/query_quality-all-hard-to-retrieve-queries_"
				+ operator;
		String outputDir = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries/query_quality-generated-all-hard-to-retrieve-queries_"
				+ operator;

		InformationSource[] infoSources = InformationSource.values();
		for (InformationSource infoSource : infoSources) {
			for (QueryReformStrategy strat : values) {

				String queriesFolder2 = queriesFolder + File.separator + infoSource;
				String outputDir2 = outputDir + File.separator + infoSource;

				String[] args = {queriesFolder2, baseQueriesFolder, baseDataFolder, outputDir2, strat.toString()};
//				QQDQueryGoldsetGeneratorMain.main(args);
			}
		}
	
	}

}
