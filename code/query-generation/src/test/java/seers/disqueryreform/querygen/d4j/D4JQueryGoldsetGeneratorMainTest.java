package seers.disqueryreform.querygen.d4j;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import seers.disqueryreform.base.InformationSource;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;

public class D4JQueryGoldsetGeneratorMainTest {

	@Test
	public void test() throws Exception {

		QueryReformStrategy[] values = QueryReformStrategy.values();

		String goldSetsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/gold_sets";
		String baseDataFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets";
		String datesDir = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/d4j_bug_dates.csv";

		String operator = "and";
		String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries/d4j-all-hard-to-retrieve-queries_"
				+ operator;
		String outputDir = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries/d4j-generated-all-hard-to-retrieve-queries_"
				+ operator;

		InformationSource[] infoSources = InformationSource.values();
		for (InformationSource infoSource : infoSources) {

            for (QueryReformStrategy strat : values) {

               String queriesFolder2 = queriesFolder + File.separator + infoSource;
                String outputDir2 = outputDir + File.separator + infoSource;

                String[] args = {queriesFolder2, goldSetsFolder, baseDataFolder, outputDir2, datesDir, strat.toString()};
//                D4JQueryGoldsetGeneratorMain.main(args);
            }
		}

	}

}
