package seers.disqueryreform.querygen;

import org.junit.Test;

import seers.disqueryreform.querygen.parsing.LobsterMainParsing;

public class LobsterMainParsingTest {

	@Test
	public void testMain() throws Exception {

//		String systems = "argouml-0.22,jabref-2.6,bookkeeper-4.1.0,derby-10.7.1.1,derby-10.9.1.0,hibernate-3.5.0b2,jedit-4.3,lucene-4.0,mahout-0.8,openjpa-2.0.1,openjpa-2.2.0,pig-0.11.1,pig-0.8.0,solr-4.4.0,tika-1.3,zookeeper-3.4.5";
		String systems = "argouml-0.22";
		String bugsFile = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/lobster_data/lobster-all-hard-to-retrieve-queries.csv";
		String[] args = {
				"C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/lobster_data",
				"C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/lobster-all-hard-to-retrieve",
				systems, "n", "pattern_list", bugsFile  };
		LobsterMainParsing.main(args);
	}
	

	@Test
	public void testMain2() throws Exception {

		String systems = "aspectj-1.5.3,eclipse-3.1,swt-3.1";
		String bugsFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/buglocator-sample-hard-to-retrieve-queries.csv";
		String[] args = {
				"C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/buglocator",
				"C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/buglocator-all-hard-to-retrieve",
				systems, "n", "pattern_list", bugsFile  };
		LobsterMainParsing.main(args);
	}

	
}
