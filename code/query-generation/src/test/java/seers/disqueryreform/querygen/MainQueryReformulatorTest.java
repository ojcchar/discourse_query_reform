package seers.disqueryreform.querygen;

import org.junit.Test;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.InformationSource;
import seers.disqueryreform.base.QueryReformStrategy;

public class MainQueryReformulatorTest {

    @Test
    public void testMain() throws Exception {

//        QueryReformStrategy[] values = {QueryReformStrategy.ALL_TEXT};
        QueryReformStrategy[] values = QueryReformStrategy.values();
        InformationSource[] infoSources = InformationSource.values();

        String baseDataFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
                "\\existing_data_sets";
        String queriesOutBaseFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation" +
                "/reformulated_queries";
        DataSet[] dataSets = {DataSet.B4BL};
        String andOperator = "and";

        for (InformationSource infoSource : infoSources) {

            for (DataSet dataSet : dataSets) {

                for (QueryReformStrategy strat : values) {

                    String strategy = strat.toString();

                    String[] args = {strategy, baseDataFolder, queriesOutBaseFolder, dataSet.toString(), andOperator,
							infoSource
                            .toString()};
                    MainQueryReformulator.main(args);

                }
            }
        }

    }

}
