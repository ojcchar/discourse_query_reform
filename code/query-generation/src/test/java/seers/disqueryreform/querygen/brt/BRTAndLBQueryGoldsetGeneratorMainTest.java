package seers.disqueryreform.querygen.brt;

import org.junit.Test;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.InformationSource;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;

public class BRTAndLBQueryGoldsetGeneratorMainTest {

    @Test
    public void test() throws Exception {

        String operator = "and";
        String baseDataFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data" +
                "/existing_data_sets";

        //bug locator
        String bugReportsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data" +
                "/existing_data_sets/buglocator_data";
        String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries" +
                "/buglocator-sample-hard-to-retrieve_"
                + operator;
        String outputDir = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries" +
                "/buglocator" +
                "-generated-sample-hard-to-retrieve_"
                + operator;
        DataSet dataSet = DataSet.BRT;

        //lobster
        /*String bugReportsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data" +
                "/existing_data_sets/lobster_data";
        String queriesFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries" +
                "/lobster-all-hard" +
                "-to-retrieve_"
                + operator;
        String outputDir = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/reformulated_queries" +
                "/lobster-generated-all" +
                "-hard-to-retrieve_" + operator;
        DataSet dataSet = DataSet.LB;*/

        //------------------------------

        QueryReformStrategy[] values = QueryReformStrategy.values();


        InformationSource[] infoSources = InformationSource.values();
        for (InformationSource infoSource : infoSources) {

            for (QueryReformStrategy strat : values) {

                String queriesFolder2 = queriesFolder + File.separator + infoSource;
                String outputDir2 = outputDir + File.separator + infoSource;

                String[] args = {queriesFolder2, bugReportsFolder, baseDataFolder, outputDir2, strat.toString(),
                        dataSet.toString()};
//                BRTAndLBQueryGoldsetGeneratorMain.main(args);
            }
        }

    }

}
