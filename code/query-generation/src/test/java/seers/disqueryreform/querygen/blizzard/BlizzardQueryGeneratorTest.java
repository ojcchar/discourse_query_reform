package seers.disqueryreform.querygen.blizzard;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class BlizzardQueryGeneratorTest {

    private static final String ORIG_INDEX = "/home/juan/Source/BLIZZARD/Lucene-Index/ecf";

    public static void main(String[] args) throws IOException {
        DirectoryReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(ORIG_INDEX)));

        Document d = reader.document(0);
        IndexableField f = d.getField("contents");

        IndexSearcher s = new IndexSearcher(reader);

        Terms terms = MultiFields.getTerms(reader, "contents");

        Map<Integer, List<String>> docTerms = new HashMap<>();

        for (int docID = 336; docID < reader.maxDoc(); docID++) {
            TermsEnum it = terms.iterator();
            docTerms.put(docID, new ArrayList<>());

            BytesRef t;
            while ((t = it.next()) != null) {
                String termString = t.utf8ToString();
                TopDocs res = s.search(new TermQuery(new Term("contents", termString)), Integer.MAX_VALUE);
                int finalDocID = docID;
                if (Arrays.stream(res.scoreDocs).anyMatch(sd -> sd.doc == finalDocID)) {
                    docTerms.get(docID).add(termString);
                }
            }

            System.out.println(docID + ": " + String.join(" ", docTerms.get(docID)));
        }
    }

}