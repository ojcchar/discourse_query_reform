package seers.disqueryreform.querygen.d4j.autocoding;

import org.junit.Test;

public class D4JAutoCodingTest {

	@Test
	public void testMain() throws Exception {

		String dataFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/bug_reports";
		String outputFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/d4j_first_sample_coding_auto";
		String patternListFolder = "pattern_list";
		String[] args = { dataFolder, outputFolder, patternListFolder };
		D4JAutoCoding.main(args);
	}

}
