package seers.disqueryreform.querygen.d4j;

import org.junit.Test;

public class D4JBaselineQueryGeneratorMainTest {

	String bugReportsFolder = "";
	String goldSetsFolder = "";
	String bugsFile = "";
	String outputDir = "";

	@Test
	public void test() throws Exception {

		bugReportsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/bug_reports";
		bugsFile = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/bugs_with_method_goldset.csv";
		outputDir = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/queries";
		goldSetsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/gold_sets";

		String[] args = { bugReportsFolder, goldSetsFolder, bugsFile, outputDir };
		D4JBaselineQueryGeneratorMain.main(args);
	}

}
