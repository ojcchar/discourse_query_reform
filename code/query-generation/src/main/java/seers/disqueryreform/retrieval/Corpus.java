package seers.disqueryreform.retrieval;

import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a corpus. It can be a subdivision of a project, e.g. in D4J each query has its own
 * corpus and in B4BL each project has many corpora, each corresponding to a version (e.g.
 * ENTESB_1_0_0 is a version of ENTESB).
 */
public class Corpus {
    private static final Logger LOGGER = LoggerFactory.getLogger(Corpus.class);

    private final String name;
    private final List<RetrievalDoc> documents;
    // TODO: some queries might have the same text
    private final List<DiscourseQuery> queries;
    private final Map<String, List<RetrievalDoc>> docsByName;
    // TODO: consider adding this as a property to DiscourseQuery instead
    private final Project project;
    private final ConcurrentMap<String, RelJudgment> relevanceJudgmentCache = new ConcurrentHashMap<>();

    public Corpus(String name, Project project, List<RetrievalDoc> documents, List<DiscourseQuery> queries) {
        this.name = name;
        this.project = project;
        this.documents = documents;
        this.queries = queries;

        docsByName = documents.stream()
                .collect(Collectors.toMap(
                        RetrievalDoc::getDocName,
                        Collections::singletonList,
                        (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList())
                ));
    }

    public RelJudgment createRelevanceJudgment(DiscourseQuery discourseQuery) {
        return relevanceJudgmentCache.computeIfAbsent(discourseQuery.getKey(), k -> {
            List<RetrievalDoc> relevantDocs = discourseQuery.getGoldSet().stream()
                    .flatMap(s -> docsByName.getOrDefault(s, Collections.emptyList()).stream())
                    .collect(Collectors.toList());

            RelJudgment relJudgment = new RelJudgment();
            relJudgment.setRelevantDocs(relevantDocs);

            if (relevantDocs.size() != discourseQuery.getGoldSet().size()) {
                LOGGER.warn(String.format("Number of relevant docs in corpus for %s of %s is different from query gold set: %d vs %d",
                        discourseQuery.getKey(), project, relevantDocs.size(), discourseQuery.getGoldSet().size()));
            }

            return relJudgment;
        });
    }

    @Override
    public String toString() {
        return "Corpus{" +
                "name='" + name + '\'' +
                ", project=" + project +
                '}';
    }

    public String getName() {
        return name;
    }

    public List<RetrievalDoc> getDocuments() {
        return documents;
    }

    public List<DiscourseQuery> getQueries() {
        return queries;
    }

    public Project getProject() {
        return project;
    }
}
