package seers.disqueryreform.retrieval;

import java.io.*;

public class SubProcessOutputCollector extends Thread {

    private final InputStream inputStream;
    private StringBuilder outputBuilder;

    public SubProcessOutputCollector(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public void run() {
        outputBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                outputBuilder.append("\t").append(line).append("\n");
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String getOutput() {
        return outputBuilder.toString();
    }
}
