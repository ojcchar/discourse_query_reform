package seers.disqueryreform.retrieval;

import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DiscourseQuery;
import seers.disqueryreform.base.Project;
import seers.disqueryreform.base.RetrievalUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CorpusLoader {

    private final boolean loadCorpusDocs;
    private final Path existingDataSetsPath;

    public CorpusLoader(Path existingDataSetsPath, boolean loadCorpusDocs) {
        this.existingDataSetsPath = existingDataSetsPath;
        this.loadCorpusDocs = loadCorpusDocs;
    }

    /**
     * Project will have more than one corpus sometimes, e.g. D4J has a corpus for each query and
     * B4BL has multiple versions for each project. The queries are grouped and returned with their
     * respective corpus (lazily loaded).
     * <p>
     * NOTE: It must be ensured that {@link RetrievalUtils#readCorpus(Path, DataSet, String, DiscourseQuery)}
     * always loads documents with the same ID.
     *
     * @param project Whose corpora will be loaded.
     * @param queries Queries to be grouped.
     * @return The corpora.
     */
    public Stream<Corpus> loadCorpora(Project project, List<DiscourseQuery> queries) {
        DataSet dataSet = project.getDataSet();
        String projectName = project.getProjectName();

        if (dataSet.equals(DataSet.D4J)) {
            return queries.stream()
                    /* Each query gets its own corpus but there may be many versions of the same
                    query, e.g. one for each strategy/TaskNav configuration, so they must
                    be grouped */
                    .collect(Collectors.groupingBy(DiscourseQuery::getKey))
                    .entrySet().stream()
                    .map(corpusGrouper(project));
        } else if (dataSet.equals(DataSet.B4BL)) {
            // Must be grouped by system version, which is stored in some files
            Map<String, String> idToProject =
                    DataSet.loadB4BLIDToProjectMap(existingDataSetsPath);
            return queries.stream()
                    .collect(Collectors.groupingBy(q -> idToProject.get(q.getKey())))
                    .entrySet().stream()
                    .map(corpusGrouper(project));
        } else {
            // Everything uses the same corpus
            List<RetrievalDoc> documents = loadCorpusDocs(dataSet, projectName, null);
            return Stream.of(new Corpus(projectName, project, documents, queries));
        }
    }

    public List<RetrievalDoc> loadCorpusDocs(DataSet dataSet, String projectName, DiscourseQuery query) {
        if (!loadCorpusDocs) {
            return Collections.emptyList();
        }

        try {
            return RetrievalUtils.readCorpus(existingDataSetsPath, dataSet, projectName, query);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Function<Map.Entry<String, List<DiscourseQuery>>, Corpus> corpusGrouper(Project project) {
        return e -> {
            String corpusName = e.getKey();
            List<DiscourseQuery> groupedQueries = e.getValue();
            List<RetrievalDoc> documents =
                    loadCorpusDocs(project.getDataSet(),
                            project.getProjectName(), groupedQueries.get(0));

            return new Corpus(corpusName, project, documents, groupedQueries);
        };
    }
}