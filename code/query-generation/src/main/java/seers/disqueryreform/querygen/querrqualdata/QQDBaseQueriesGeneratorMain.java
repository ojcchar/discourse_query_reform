package seers.disqueryreform.querygen.querrqualdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.Gson;

import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.xml.XMLHelper;
import seers.disqueryreform.base.D4JUtils;

public class QQDBaseQueriesGeneratorMain {

	static String folderOriginalContent = "C:/Users/ojcch/Downloads/concept-location-dataset";
	static String corpusFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/query_quality_data/query_quality_code_corpus_prep";
	static String queriesFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/query_quality_data/original_queries.csv";
	static String bugsFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/query_quality_data/all_bugs";
	static String queriesOutFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/query_quality_data/queries";
	static String bugOutFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/query_quality_data/bug_reports";

	public static DateTimeFormatter dtf = DateTimeFormat.forPattern("y-M-d HH:mm:ss");
	public static DateTimeFormatter dtf2 = DateTimeFormat.forPattern("y-M-d HH:mm:ss Z");
	static List<String> systems = Arrays.asList("adempiere-3.1.0", "apache-nutch-1.8", "apache-nutch-2.1",
			"atunes-1.10.0", "bookkeeper-4.1.0", "commons-math-3-3.0", "derby-10.9.1.0", "jedit-4.2", "mahout-0.4",
			"mahout-0.8", "openjpa-2.0.1", "tika-1.3", "eclipse-2.0");
//	 static List<String> systems = Arrays.asList("bookkeeper-4.1.0");
	static Gson gson = new Gson();
	
	static AtomicInteger queryIdSequence = new AtomicInteger(1);

	public static void main(String[] args) throws Exception {

		File outfol = new File(queriesOutFolder);
		outfol.mkdirs();

		for (String sys : systems) {
			File file = new File(outfol + File.separator + sys + ".json");
			if (file.exists()) {
				FileUtils.forceDelete(file);
			}
		}

		new File(bugOutFolder).mkdirs();

		List<List<String>> lines = D4JUtils.readLines(new File(queriesFile), ';');

		List<List<String>> linesFiltered = lines.stream()
				.filter(l -> systems.contains(l.get(0)) && !l.get(3).trim().isEmpty()).collect(Collectors.toList());

		ThreadExecutor.executePaginated(linesFiltered, QueryProcessor.class, new ThreadParameters());

	}

	static ConcurrentHashMap<String, Map<Query, List<String>>> systemRelJudg = new ConcurrentHashMap<>();
	static ConcurrentHashMap<String, List<RetrievalDoc>> systemCorpora = new ConcurrentHashMap<>();
	static ConcurrentHashMap<String, List<BugReport>> systemBugs = new ConcurrentHashMap<>();

	public static class QueryProcessor extends ThreadProcessor {

		private List<List> lines;

		public QueryProcessor(ThreadParameters params) {
			super(params);
			lines = params.getListParam(List.class, ThreadExecutor.ELEMENTS_PARAM);
		}

		@Override
		public void executeJob() throws Exception {

			for (List<String> line : lines) {

				String system = line.get(0);
				String queryId = line.get(1);
				String bugId = line.get(3);

				try {
					System.out.println("query " + system + "-" + queryId);

					// read goldset
					List<String> goldset = readGoldset(system, queryId);

					// read the list of documents
					Set<RetrievalDoc> docs = getListOfDocs(system, goldset);

					// read bugReports
					readBugReports(system);

					// create the query
					BugReport bug = getBug(system, bugId);

					// write the query
					writeQuery(system, queryId, bug, docs);

					writeBugReport(system, bug);
				} catch (Exception e) {
					System.err.println("Error for query: " + system + "-" + queryId);
					e.printStackTrace();
				}

			}

		}

		private void writeBugReport(String system, BugReport bug) throws Exception {

			File sysFolder = new File(bugOutFolder + File.separator + system);
			sysFolder.mkdirs();

			seers.bugrepcompl.entity.BugReport bug2 = new seers.bugrepcompl.entity.BugReport(bug.getKey(),
					bug.getTitle(), bug.getDescription());
			File outputFile = new File(sysFolder + File.separator + bug2.getId() + ".xml");
			XMLHelper.writeXML(seers.bugrepcompl.entity.BugReport.class, bug2, outputFile);

		}

		private void writeQuery(String system, String queryId, BugReport bug, Set<RetrievalDoc> docs)
				throws IOException {

			// create json object
			Map<String, Object> json = new LinkedHashMap<>();
			json.put("id", queryIdSequence.getAndIncrement());
			json.put("bug_id", bug.getKey());
			json.put("text", getWholeText(bug));
			json.put("title", bug.getTitle());
			json.put("description", bug.getDescription().equals("NA") ? null : bug.getDescription());
			json.put("method_goldset", getGoldset(docs));
			json.put("creation_date", bug.getCreationDate() == null ? "" : bug.getCreationDate().toString(dtf));
			json.put("resolution_date", bug.getResolutionDate() == null ? "" : bug.getResolutionDate().toString(dtf));

			File outFile = new File(queriesOutFolder + File.separator + system + ".json");
			FileUtils.write(outFile, gson.toJson(json) + "\n", true);

		}

		private List<String> getGoldset(Set<RetrievalDoc> docs) {
			return docs.stream().map(d -> d.getDocName()).collect(Collectors.toList());
		}

		private String getWholeText(BugReport bug) {
			return bug.getTitle() + (bug.getDescription().equals("NA") ? "" : ". " + bug.getDescription());
		}

		private BugReport getBug(String system, String bugId) {
			List<BugReport> bugs = systemBugs.get(system);
			int i = bugs.indexOf(new BugReport(bugId, null, null));
			return bugs.get(i);
		}

		private void readBugReports(String system) throws Exception {
			synchronized (systemBugs) {
				if (systemBugs.get(system) == null) {

					System.out.println("reading bugs");

					String pathname = bugsFolder + File.separator + system + "_Queries_info.txt";
					List<List<String>> bugLines = D4JUtils.readLines(new File(pathname), ';');

					List<BugReport> bugs = new ArrayList<>();
					for (List<String> bugLine : bugLines) {
						bugs.add(new BugReport(bugLine.get(1), unescapeString(bugLine.get(13)),
								unescapeString(bugLine.get(11)),
								bugLine.get(6).equals("NA") ? null : getDate(bugLine.get(6)),
								bugLine.get(5).equals("NA") ? null : getDate(bugLine.get(5)), null));
					}

					systemBugs.put(system, bugs);
				}
			}
		}

		private DateTime getDate(String string) {
			try {
				return DateTime.parse(string);
			} catch (Exception e) {
				return DateTime.parse(string, dtf2);
			}
		}

		private String unescapeString(String string) {
			return string.replace("\\\\r\\\\n", "\n").replace("\\\\t", "\\t").replace("\\\"", "\"")
					.replace("\\\\(", "(").replace("\\\\{", "{").replace("\\\\[", "[").replace("\\\\)", ")")
					.replace("\\\\}", "}").replace("\\\\]", "]").replace("\\\\_", "_").replace("\\\\!", "!")
					.replace("\\\\.", ".").replace("\\\\", "\\");
		}

		private Set<RetrievalDoc> getListOfDocs(String system, List<String> goldset) throws Exception {

			List<RetrievalDoc> docs;
			synchronized (systemCorpora) {
				docs = systemCorpora.get(system);
				if (docs == null) {
					String corpFilePath = corpusFolder + File.separator + system + ".json.prep";
					docs = readCorpus(corpFilePath);
					systemCorpora.put(system, docs);
				}
			}

			// --------------------------------
			Set<RetrievalDoc> gsDocs = new LinkedHashSet<>();

			for (String doc : goldset) {

				Optional<RetrievalDoc> element = docs.stream().filter(d -> doc.toLowerCase().replace(" ", "")
						.endsWith(d.getDocName().toLowerCase().replace(" ", "").replace(":", "."))).findFirst();
				if (element.isPresent()) {
					gsDocs.add(element.get());
				} else {
					throw new RuntimeException("doc not found: " + doc);
				}
			}

			return gsDocs;
		}

		@SuppressWarnings("unchecked")
		public List<RetrievalDoc> readCorpus(String corpFilePath) throws Exception {
			Gson gson = new Gson();

			List<RetrievalDoc> docList = new ArrayList<RetrievalDoc>();

			try (BufferedReader inCorpus = new BufferedReader(new FileReader(corpFilePath));) {
				// add every document into the list
				String lineCorpus;
				while ((lineCorpus = inCorpus.readLine()) != null) {

					if (lineCorpus.isEmpty()) {
						continue;
					}

					Map<String, Object> jsonVals = gson.fromJson(lineCorpus, Map.class);

					RetrievalDoc doc = new RetrievalDoc(((Double) jsonVals.get("id")).intValue(),
							jsonVals.get("text").toString(), jsonVals.get("qname").toString());

					docList.add(doc);
				}
			}

			return docList;
		}

		private List<String> readGoldset(String system, String queryId) throws Exception {

			synchronized (systemRelJudg) {
				Map<Query, List<String>> relj = systemRelJudg.get(system);
				if (relj == null) {
					String releJudgmentPath = folderOriginalContent + File.separator + system + File.separator + system
							+ "_Queries.txt";
					relj = readReleJudgments(releJudgmentPath);
					systemRelJudg.put(system, relj);
				}
				return relj.get(new Query(Integer.valueOf(queryId)));
			}

		}

	}

	private static Map<Query, List<String>> readReleJudgments(String releJudgmentPath) throws Exception {
		Map<Query, List<String>> relJudgMap = new LinkedHashMap<>();
		File fileRelJudg = new File(releJudgmentPath);

		try (BufferedReader reader = new BufferedReader(new FileReader(fileRelJudg));) {

			String line;
			int lineNumber = 0;
			int numberTargetDocs = -1;
			Integer queryId = 0;

			Query query = new Query();
			List<String> targetDocs = null;
			while ((line = reader.readLine()) != null) {

				// it is not a blank line
				String lineTrimmed = line.trim();
				if (!lineTrimmed.isEmpty()) {
					lineNumber++;
					switch (lineNumber) {
					case 1:
						if (lineTrimmed.contains(" ")) {
							queryId = Integer.valueOf(lineTrimmed.split(" ")[0]);
						} else {
							queryId = Integer.valueOf(lineTrimmed);
						}
						// System.out.println(queryId);
						query.setQueryId(queryId);
						// queryId++;
						break;
					// case 2:
					// query.setTxt(line.trim().toLowerCase());
					// break;
					case 3:
						numberTargetDocs = Integer.parseInt(lineTrimmed);
						targetDocs = new ArrayList<String>(numberTargetDocs);
						break;
					default:
						if (lineNumber >= 4) {
							targetDocs.add(lineNumber - 4, line);
							// System.out.println(getDocQueryStr(line));
						}
						break;
					}
				} else {

					if (targetDocs != null) {
						relJudgMap.put(query, targetDocs);
					}

					lineNumber = 0;
					numberTargetDocs = -1;
					query = new Query();
					targetDocs = null;
				}
			}

			if (targetDocs != null) {
				relJudgMap.put(query, targetDocs);
			}

		}
		return relJudgMap;
	}

}
