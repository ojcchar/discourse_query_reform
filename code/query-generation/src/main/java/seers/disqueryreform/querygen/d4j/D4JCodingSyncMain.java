package seers.disqueryreform.querygen.d4j;

import java.io.File;
import java.util.List;
import java.util.Optional;

import org.apache.commons.text.beta.similarity.LevenshteinDistance;

import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportDescription;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionParagraph;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionSentence;
import seers.disqueryreform.base.D4JUtils;

public class D4JCodingSyncMain {

	static String querySampleFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/d4j-selected-queries.csv";
	static String codingFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/d4j-first_sample";
	static String parsedFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/d4j-selected-queries_no_code";
	static String outputFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/d4j-first_sample_no_code";

	public static void main(String[] args) throws Exception {

		// read all queries from sample
		List<List<String>> lines = D4JUtils.readLines(new File(querySampleFile), ',');

		for (List<String> line : lines) {

			String system = line.get(0);
			String bugId = line.get(1);

			System.out.println("Processing: " + line);

			// read bug report
			PatternLabeledBugReport codedBug = XMLHelper.readXML(PatternLabeledBugReport.class,
					codingFolder + File.separator + system.toLowerCase() + File.separator + bugId + ".xml");
			PatternLabeledBugReport parsedBug = XMLHelper.readXML(PatternLabeledBugReport.class,
					parsedFolder + File.separator + system.toLowerCase() + File.separator + bugId + ".xml");

			syncParsedBug(line, parsedBug, codedBug);

			File sysFolder = new File(outputFolder + File.separator + system.toLowerCase());
			sysFolder.mkdir();

			File outputFile = new File(sysFolder + File.separator + bugId + ".xml");
			XMLHelper.writeXML(parsedBug, outputFile);

			outputFile = new File(sysFolder + File.separator + bugId + ".coded.xml");
			XMLHelper.writeXML(codedBug, outputFile);

		}
	}

	private static void syncParsedBug(List<String> line, PatternLabeledBugReport parsedBug, PatternLabeledBugReport codedBug) {


		PatternLabeledBugReportTitle pTitle = parsedBug.getTitle();
		pTitle.setOb(codedBug.getTitle().getOb());
		pTitle.setEb(codedBug.getTitle().getEb());
		pTitle.setSr(codedBug.getTitle().getSr());

		PatternLabeledBugReportDescription parsedDesc = parsedBug.getDescription();
		PatternLabeledBugReportDescription codedDesc = codedBug.getDescription();

		List<PatternLabeledDescriptionParagraph> parsedPags = parsedDesc.getParagraphs();

		for (PatternLabeledDescriptionParagraph parsedParagraph : parsedPags) {
			Optional<PatternLabeledDescriptionParagraph> findFirst = codedDesc.getParagraphs().stream()
					.filter(codedParagraph -> similarSentences(codedParagraph.getSentences().get(0),
									parsedParagraph.getSentences().get(0)))
					.findFirst();
			if (findFirst.isPresent()) {
				PatternLabeledDescriptionParagraph p = findFirst.get();
				parsedParagraph.setOb(p.getOb());
				parsedParagraph.setEb(p.getEb());
				parsedParagraph.setSr(p.getSr());
			}
		}

		List<PatternLabeledDescriptionSentence> parsedSentences = parsedDesc.getAllSentences();
		List<PatternLabeledDescriptionSentence> codedSentences = codedDesc.getAllSentences();
		for (PatternLabeledDescriptionSentence ps : parsedSentences) {

			Optional<PatternLabeledDescriptionSentence> findFirst = codedSentences.stream()
					.filter(cs -> similarSentences(cs, ps)).findFirst();
			if (findFirst.isPresent()) {
				PatternLabeledDescriptionSentence s = findFirst.get();
				ps.setOb(s.getOb());
				ps.setEb(s.getEb());
				ps.setSr(s.getSr());
			}
		}
	}

	private static boolean similarSentences(PatternLabeledDescriptionSentence codedSentence, PatternLabeledDescriptionSentence parsedSentence) {
		LevenshteinDistance d = new LevenshteinDistance();
		String cSentText = codedSentence.getValue();
		String pSentText = parsedSentence.getValue();
		if (cSentText.equals(pSentText)) {
			return true;
		} else {

			Integer distance = d.apply(cSentText, pSentText);
			Integer lengthDiff = Math.abs(cSentText.length() - pSentText.length());
			if (lengthDiff != 0 && distance.equals(lengthDiff)) {
				return true;
			}
		}
		return false;
	}

}
