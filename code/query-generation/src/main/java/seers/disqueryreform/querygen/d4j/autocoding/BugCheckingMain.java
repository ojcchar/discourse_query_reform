package seers.disqueryreform.querygen.d4j.autocoding;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;

import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportDescription;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionParagraph;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionSentence;

public class BugCheckingMain {

	 static String bugFolder =
			 "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/bug_coding/quer_qual-all-hard-to-retrieve";
	// static String bugFolder =
	// "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/buglocator-sample-hard-to-retrieve_fixed";
//	static String bugFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/bug_coding/lobster-all-hard-to-retrieve";

	public static void main(String[] args) throws Exception {

		Collection<File> listFiles = FileUtils.listFiles(new File(bugFolder), new String[] { "xml" }, true);

		for (File file : listFiles) {
			String bugId = file.getName().replace(".xml", "");
			String system = file.getParentFile().getName();

			PatternLabeledBugReport bug = XMLHelper.readXML(PatternLabeledBugReport.class, file);

			boolean isNoBug = hasInfo(bug.getNoBug());
			boolean[] info = chechInfo(bug);
			boolean hasOb = info[0];
			boolean hasEb = info[1];
			boolean hasSr = info[2];

			String comments = bug.getComments();
			if (isNoBug) {
				System.out.println(system + ";" + bugId + ";nb;" + comments);
			} else {

				PatternLabeledBugReportTitle title = bug.getTitle();
				PatternLabeledBugReportDescription description = bug.getDescription();
				if (isTitleNotCoded(title) && isDescriptionNotCoded(description)) {
					System.out.println(system + ";" + bugId + ";nc;" + comments);
				} else if (hasEb && !hasOb && !hasSr) {
					System.out.println(system + ";" + bugId + ";oe;" + comments);
				} else {
					if (!comments.trim().isEmpty()) {
						System.out.println(system + ";" + bugId + ";cc;" + comments);
					}
				}
			}
		}

	}

	private static boolean hasInfo(String value) {
		return !value.trim().isEmpty();
	}

	private static boolean[] chechInfo(PatternLabeledBugReport bugCoded) {

		PatternLabeledBugReportTitle title = bugCoded.getTitle();

		boolean hasOb = false;
		boolean hasEb = false;
		boolean hasSr = false;
		boolean hasOther = false;

		if (hasInfo(title.getOb())) {
			hasOb = true;
		}
		if (hasInfo(title.getEb())) {
			hasEb = true;
		}
		if (hasInfo(title.getSr())) {
			hasSr = true;
		}
		if (!hasInfo(title.getOb()) && !hasInfo(title.getEb()) && !hasInfo(title.getSr())) {
			hasOther = true;
		}

		// ---------------------------------------------

		PatternLabeledBugReportDescription description = bugCoded.getDescription();

		if (description != null) {

			List<PatternLabeledDescriptionParagraph> paragraphs = description.getParagraphs();

			for (PatternLabeledDescriptionParagraph parag : paragraphs) {

				if (hasInfo(parag.getOb())) {
					hasOb = true;
				}
				if (hasInfo(parag.getEb())) {
					hasEb = true;
				}
				if (hasInfo(parag.getSr())) {
					hasSr = true;
				}
				if (!hasInfo(parag.getOb()) && !hasInfo(parag.getEb()) && !hasInfo(parag.getSr())) {
					hasOther = true;
				}
			}

			// ---------------------------------------------

			List<PatternLabeledDescriptionSentence> sentences = description.getAllSentences();

			for (PatternLabeledDescriptionSentence sent : sentences) {

				if (hasInfo(sent.getOb())) {
					hasOb = true;
				}
				if (hasInfo(sent.getEb())) {
					hasEb = true;
				}
				if (hasInfo(sent.getSr())) {
					hasSr = true;
				}
				if (!hasInfo(sent.getOb()) && !hasInfo(sent.getEb()) && !hasInfo(sent.getSr())) {
					hasOther = true;
				}
			}

		}

		return new boolean[] { hasOb, hasEb, hasSr, hasOther };
	}

	private static boolean isDescriptionNotCoded(PatternLabeledBugReportDescription description) {

		return description.getParagraphs().stream().noneMatch(
				p -> !p.getOb().trim().isEmpty() || !p.getEb().trim().isEmpty() || !p.getSr().trim().isEmpty())
				&& description.getAllSentences().stream().noneMatch(
						p -> !p.getOb().trim().isEmpty() || !p.getEb().trim().isEmpty() || !p.getSr().trim().isEmpty());
	}

	private static boolean isTitleNotCoded(PatternLabeledBugReportTitle title) {
		return title.getOb().trim().isEmpty() && title.getEb().trim().isEmpty() && title.getSr().trim().isEmpty();
	}

}
