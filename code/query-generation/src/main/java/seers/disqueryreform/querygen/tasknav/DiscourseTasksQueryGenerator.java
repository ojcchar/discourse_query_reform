package seers.disqueryreform.querygen.tasknav;

import com.google.gson.Gson;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import seers.appcore.utils.JavaUtils;
import seers.disqueryreform.base.*;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DiscourseTasksQueryGenerator {

    @SuppressWarnings("WeakerAccess")
    public static final String SIZE_SUMMARY_FILE_NAME = "strategy-size-summary.txt";
    private static final Logger LOGGER = LoggerFactory.getLogger(DiscourseTasksQueryGenerator.class);
    public static final Set<String> CODE_TYPES_TO_DISCARD = JavaUtils.getSet("TextFragmentNode", "BaseReferenceTypeNode",
            "QualifiedIdentifierNode", "IdentifierNode", "SeparatorNode", "ArrayTypeNode", "InterfaceRelationshipNode");
    private static final String REAL_STRATEGY_SIZE_FILE_NAME = "real-strategy-size-summary.txt";
    public static final String COMPONENT_TABLE_FILE_NAME = "component-table.json";

    private final Path preprocessingScriptPath;
    private final Path datasetsDirPath;
    private final Path reformQueriesDirPath;
    private final Path taskNavPath;

    public DiscourseTasksQueryGenerator(Path baseRepositoryPath) {
        reformQueriesDirPath = baseRepositoryPath.resolve(Paths.get("data", "tasknav_reformulated_queries"));
        taskNavPath = baseRepositoryPath.resolve(Paths.get("data", "TaskNav"));
        preprocessingScriptPath = baseRepositoryPath.resolve(Paths.get("code", "scripts", "new_scripts", "preprocess-queries-containers.py"));
        datasetsDirPath = baseRepositoryPath.resolve(Paths.get("data", "existing_data_sets"));
    }

    /**
     * Constructor for non-task generation.
     * <p>
     * TODO: move the tasknav generation to a subclass to clean this up.
     *
     * @param baseRepositoryPath   base path
     * @param reformQueriesDirPath output dir
     */
    public DiscourseTasksQueryGenerator(Path baseRepositoryPath, Path reformQueriesDirPath) {
        this.reformQueriesDirPath = reformQueriesDirPath;
        taskNavPath = null;
        preprocessingScriptPath = baseRepositoryPath.resolve(Paths.get("code", "scripts", "new_scripts", "preprocess-queries-containers.py"));
        datasetsDirPath = baseRepositoryPath.resolve(Paths.get("data", "existing_data_sets"));
    }

    /**
     * @param preprocess           Whether or not the queries should be preprocessed
     * @param simplifiedGeneration If true, uses only true-false configuration and full tasks
     * @param includeOther
     */
    public void generateQueries(boolean preprocess, boolean simplifiedGeneration, boolean includeOther) {

        // -------------------------------------------------------------------------
        // PROCESS START

        if (reformQueriesDirPath.toFile().exists()) {
            LOGGER.info("Preprocessed queries already exist, skipping creation");
            return;
        }

        try {
            reformulateAndAddGoldSets(simplifiedGeneration, includeOther);
        } catch (IOException | ParserConfigurationException | JAXBException | SAXException e) {
            throw new Error(e);
        }

        if (preprocess) {
            LOGGER.info("---------------------------------");
            LOGGER.info("Preprocessing queries");

            preprocessQueries(reformQueriesDirPath);

            // TODO: this should probably be outside if?
            writeComponentTables();
        }

        LOGGER.info("---------------------------------");
    }

    private void reformulateAndAddGoldSets(boolean simplifiedGeneration, boolean includeOther)
            throws IOException, ParserConfigurationException, JAXBException, SAXException {
        LOGGER.info("Reformulating queries");

        List<StrategyQueryContainer> containers = generateReformulatedQueries(simplifiedGeneration, includeOther);

        LOGGER.info("---------------------------------");
        LOGGER.info("Adding gold sets");

        new GoldSetAdder(datasetsDirPath).addGoldSets(containers);

        Gson gson = GsonUtils.createDefaultGson();
        reformQueriesDirPath.toFile().mkdirs();

        for (StrategyQueryContainer container : containers) {
            File outfile = reformQueriesDirPath.resolve(container.getStrategyName() + ".json").toFile();
            FileUtils.write(outfile, gson.toJson(container), "utf-8");
        }

        saveSizeSummaries(containers);
    }

    protected void saveSizeSummaries(List<StrategyQueryContainer> containers) throws IOException {
        Map<String, Integer> fileSizeSummary = new HashMap<>();
        Map<String, Integer> strategySizeSummary = new HashMap<>();

        for (StrategyQueryContainer container : containers) {
            // Add strategy file size
            fileSizeSummary.put(container.getStrategyName(), container.getTotalQueries());

            /*
            Add strategy query sizes
            Each container corresponds to a single strategy but it contains queries from many
            configurations. For the real strategy size we need to find the queries for each real
            strategy, which is the strategy name + taskNav config
            FIXME all this would be simplified if configuration was part of strategy
            */
            strategySizeSummary.putAll(
                    container.allQueries()
                            .collect(Collectors.groupingBy(
                                    q -> DiscourseQuery.generateFullStrategyName(q.getStrategy(),
                                            q.getTaskNavConfiguration()))
                            )
                            .entrySet().stream()
                            .collect(Collectors.toMap(
                                    Map.Entry::getKey,
                                    e -> e.getValue().size()
                            ))
            );
        }

        // Write the file size summary
        writeSizeSummary(fileSizeSummary, reformQueriesDirPath.resolve(SIZE_SUMMARY_FILE_NAME).toFile());

        // Write strategy real size summary
        writeSizeSummary(strategySizeSummary, reformQueriesDirPath.resolve(REAL_STRATEGY_SIZE_FILE_NAME).toFile());
    }

    @SuppressWarnings("ConstantConditions")
    protected List<StrategyQueryContainer> generateReformulatedQueries(boolean simplifiedGeneration, boolean includeOther)
            throws IOException, ParserConfigurationException, JAXBException, SAXException {

        Stream<File> allFiles = Arrays.stream(taskNavPath.toFile()
                .listFiles(f -> f.getName().endsWith(".json")));

        if (simplifiedGeneration) {
            allFiles = allFiles.filter(f -> f.getName().endsWith("true-false.json"));
        }

        List<TaskNavContainer> containers = allFiles
                .map(jsonFile -> {
                    try {
                        return TaskNavContainer.fromFile(jsonFile);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .collect(Collectors.toList());

        Set<DiscourseTaskStrategy> strategies;

        if (simplifiedGeneration) {
            strategies = DiscourseTaskStrategy.generateSimplifiedStrategies();
        } else {
            strategies = DiscourseTaskStrategy.generateDiscourseTaskStrategies();
        }

        if (includeOther) {
            strategies.addAll(DiscourseTaskStrategy.generateAllOtherStrategies());
        }

        DiscourseTasksQueryReformulator tasksReformulator = new DiscourseTasksQueryReformulator(datasetsDirPath.toString(),
                DataSet.values(),
                strategies,
                CODE_TYPES_TO_DISCARD,
                containers
        );

        List<StrategyQueryContainer> applicableStrategies = tasksReformulator.generateQueryCombinations(true);

        /* Adding the original 31 strategies
         * Must be done separately because otherwise it would generate many identical queries for
         * each original query, each for every TaskNav configuration*/
        DiscourseTasksQueryReformulator noTasksReformulator = new DiscourseTasksQueryReformulator(datasetsDirPath.toString(),
                DataSet.values(),
                DiscourseTaskStrategy.generateDiscourseStrategies(),
                CODE_TYPES_TO_DISCARD,
                null
        );

        applicableStrategies.addAll(noTasksReformulator.generateQueryCombinations(false));
        return applicableStrategies;
    }

    @SuppressWarnings("ConstantConditions")
    public void preprocessQueries(Path reformulatedQueriesPath) {
        // Command is the python script + the list of files
        List<String> commandBase = new ArrayList<>(Arrays.asList("python3", preprocessingScriptPath.toString()));

        // Dataset directories
        List<String> strategyFiles = Arrays.stream(reformulatedQueriesPath.toFile().listFiles(f -> f.getName().endsWith(".json")))
                .map(File::getPath)
                .collect(Collectors.toList());

        // Must split the list because there might be too many files
        int step = 100;

        GeneralUtils.partitionList(strategyFiles, step)
                // Do it concurrently
                .parallel()
                // Preprocess using Python script
                .forEach(l -> {
                    ProcessBuilder builder = new ProcessBuilder(
                            Stream.concat(commandBase.stream(), l.stream())
                                    .collect(Collectors.toList())
                    );

                    try {
                        Process process = builder.start();
                        new Thread(() -> {
                            try (BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                                String line;
                                while ((line = outputReader.readLine()) != null) {
                                    LOGGER.info(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }).start();

                        try (BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
                            String line;
                            while ((line = outputReader.readLine()) != null) {
                                LOGGER.error(line);
                            }
                        }

                        int status = process.waitFor();

                        if (status != 0) {
                            throw new Error("Error when preprocessing queries");
                        }
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    } catch (InterruptedException e) {
                        throw new Error(e);
                    }
                });
    }

    protected void writeComponentTables() {
        List<StrategyQueryContainer> singleComponentContainers;

        LOGGER.info("Creating table of components for bug...");

        try {
            singleComponentContainers = Files.walk(reformQueriesDirPath)
                    .filter(p -> {
                        String strategyName = p.getFileName().toString().replace(".json", "");
                        return Files.isRegularFile(p) &&
                                p.toString().endsWith(".json") &&
                                !p.endsWith(COMPONENT_TABLE_FILE_NAME) &&
                                DiscourseTaskStrategy.splitIntoComponentNames(strategyName).size() == 1;
                    })
                    .map(p -> {
                        try {
                            LOGGER.info("Loading preprocessed queries from " + p);

                            return JSON.readJSON(p, StrategyQueryContainer.class);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        int totalQueries = singleComponentContainers.stream()
                .filter(c -> c.getStrategyName().equals(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME))
                .findFirst()
                .get()
                .getTotalQueries();

        ComponentsForBugTable componentsForBugTable = new ComponentsForBugTable(singleComponentContainers, totalQueries);

        LOGGER.info("Writing table of components for bug");

        try {
            JSON.writeJSON(componentsForBugTable, reformQueriesDirPath.resolve(COMPONENT_TABLE_FILE_NAME));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public ComponentsForBugTable readComponentsForBugTable() {
        // In case they are not generated
        generateQueries(true, true, true);

        Path filePath = reformQueriesDirPath.resolve(COMPONENT_TABLE_FILE_NAME);

        if (!Files.exists(filePath)) {
            throw new Error("Component table does not exist. This step requires preprocessed queries");
        }

        return JSON.readJSONSilent(filePath, ComponentsForBugTable.class);
    }

    private void writeSizeSummary(Map<String, Integer> sizeSummary, File outputFile) throws IOException {
        FileUtils.writeLines(outputFile,
                sizeSummary.entrySet().stream()
                        .map(e -> e.getKey() + " " + e.getValue())
                        .collect(Collectors.toList()));
    }

    @SuppressWarnings("unused")
    public Stream<Pair<String, Integer>> readStrategyFileSizeSummary() throws IOException {
        return readSizeSummary(reformQueriesDirPath, SIZE_SUMMARY_FILE_NAME);
    }

    @SuppressWarnings("unused")
    public Stream<Pair<String, Integer>> readRealStrategyFileSizeSummary() throws IOException {
        return readSizeSummary(reformQueriesDirPath, REAL_STRATEGY_SIZE_FILE_NAME);
    }

    private Stream<Pair<String, Integer>> readSizeSummary(Path reformulatedQueriesPath, String fileName) throws IOException {
        // In case they are not generated
        generateQueries(false, false, false);

        File sizeSummaryFile = reformulatedQueriesPath.resolve(fileName).toFile();

        return FileUtils.readLines(sizeSummaryFile, "utf-8").stream()
                .map(l -> {
                    String[] split = l.split(" ");
                    return new Pair<>(split[0], Integer.valueOf(split[1]));
                });
    }

    public static class Main {
        public static void main(String[] args) {
            new DiscourseTasksQueryGenerator(Paths.get(args[0])).generateQueries(true, true, true);
        }
    }
}
