package seers.disqueryreform.querygen.parsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.bugrepclassifier.BugReportClassifier;
import seers.bugrepclassifier.patternbased.PatternBugReportClassifier;
import seers.disqueryreform.base.D4JUtils;

public class LobsterMainParsing {

	static String dataFolder;
	static String outputFolder;
	static List<String> systems;
	static String bugsFile;
	static ConcurrentHashMap<String, Integer> allowedBugs = new ConcurrentHashMap<>();

	public static String patternListFolder = "pattern_list";
	public static BugReportClassifier sentenceClassifier;
	public static BugReportClassifier paragraphClassifier;

	public static boolean autoTag = false;

	public static void main(String[] args) throws Exception {

		dataFolder = args[0];
		outputFolder = args[1];
		systems = Arrays.asList(args[2].split(","));
		autoTag = "y".equals(args[3]);
		patternListFolder = args[4];
		bugsFile = args[5];

		try {
			List<List<String>> listOfBugs = D4JUtils.readLines(new File(bugsFile), ',');
			for (List<String> bug : listOfBugs) {
				allowedBugs.put(bug.get(0) + "-" + bug.get(1), 1);
			}
		} catch (IndexOutOfBoundsException e) {
			List<List<String>> listOfBugs = D4JUtils.readLines(new File(bugsFile), ';');
			for (List<String> bug : listOfBugs) {
				allowedBugs.put(bug.get(0) + "-" + bug.get(1), 1);
			}
		}

		if (autoTag) {

			sentenceClassifier = new PatternBugReportClassifier(
					new File(patternListFolder + File.separator + "sentence_patterns.csv"));
			paragraphClassifier = new PatternBugReportClassifier(
					new File(patternListFolder + File.separator + "paragraph_patterns.csv"));
		}

		// --------------------------------------

		// read the folders
		File dataFold = new File(dataFolder);
		Collection<File> fileList = FileUtils.listFiles(dataFold, new String[] { "json" }, true);

		// for each JSON file in the data folder
		for (File dataFile : fileList) {

			String fileName = dataFile.getName().toLowerCase();
			if (!fileName.equals("bug-reports.json")) {
				continue;
			}

			// check if the system is in the list of systems
			String system = dataFile.getParentFile().getName().toLowerCase();
			if (!systems.contains(system)) {
				System.err.println("System not found: " + system);
				continue;
			}

			System.out.println("Processing " + system);

			File sysFolder = new File(outputFolder + File.separator + system);
			if (!sysFolder.exists()) {
				sysFolder.mkdirs();
			}

			List<String> lines = getAllBugLines(dataFile);

			System.out.println("# of bugs for " + system + ": " + lines.size());

			ThreadParameters params = new ThreadParameters();
			params.addParam("system", system);
			params.addParam("sysFolder", sysFolder);

			Class<? extends ThreadProcessor> class1 = LineExecutor.class;
			ThreadExecutor.executePaginated(lines, class1, params);

		}

	}

	private static List<String> getAllBugLines(File dataFile) throws IOException, FileNotFoundException {
		List<String> lines = new ArrayList<>();
		// read the data file
		try (BufferedReader br = new BufferedReader(new FileReader(dataFile))) {
			String line;

			// for each bug
			while ((line = br.readLine()) != null) {
				lines.add(line);

			}
		}
		return lines;
	}

}
