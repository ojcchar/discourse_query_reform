package seers.disqueryreform.querygen.agreement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.utils.FilePathUtils;
import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.Labels;
import seers.bugrepcompl.entity.codingparse.LabeledBugReport;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AgreementChecker2 extends AgreementCheckerMain {


    static final Logger LOGGER = LoggerFactory.getLogger(AgreementChecker2.class);
    static String sampleFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\output" +
            "\\sampling\\4-23\\sample-4-23.csv";

    static String codingFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data\\04-23" +
            "-bug_coding-xml";

    public static void main(String[] args) throws Exception {

        final AgreementChecker2 checker = new AgreementChecker2();

        final List<List<String>> lines = CSVHelper.readCsv(sampleFile, true);

        for (List<String> line : lines) {
            final String dataSet = line.get(0);
            final String project = line.get(1);
            final String bugId = line.get(2);
            final String coder1 = line.get(9);
            final String coder2 = line.get(10);

            final File f1 = FilePathUtils.getFile(new File(codingFolder), dataSet, coder1, project, bugId +
                    ".xml");
            final File f2 = FilePathUtils.getFile(new File(codingFolder), dataSet, coder2, project, bugId +
                    ".xml");


            try {
                // read bug 1
                LabeledBugReport bug1 = XMLHelper.readXML(LabeledBugReport.class, f1);

                // read bug 2
                LabeledBugReport bug2 = XMLHelper.readXML(LabeledBugReport.class, f2);


                // check for same # of sentences and paragraphs
                checker.checkBugs(bug1, bug2);

                // propagate coding
                checker.propagateCoding(bug1);
                checker.propagateCoding(bug2);

                // determine agreement
                checker.determineAgreement(project, bugId, bug1, bug2, coder1, coder2, false);
            } catch (Exception e) {
                LOGGER.error("error for: " + line, e);
            }

        }


    }

    @Override
    void checkLabels(String system, String bugId, Labels sentLabels1, Labels sentLabels2,
                     String sentId, String coder1, String coder2, boolean onlyOb2, String stncText) {
        String agreement = "1";

        String labelString1 = getLabelString2(sentLabels1, onlyOb2);
        String labelString2 = getLabelString2(sentLabels2, onlyOb2);
        if (!labelString1.equals(labelString2)) {
            agreement = "0";
        }

        String coder3 = getCoder3(coder1, coder2);
        String stncText2 = "";
        if ("0".equals(agreement))
            stncText2 = stncText.replace("\"", "");
        System.out.println(system + ";" + bugId + ";" + sentId + ";" + labelString1 + ";" +
                labelString2 + ";" + agreement + ";" + coder1 + ";" + coder2 + ";" + coder3+ ";\"" + stncText2 + "\"");
    }

    static List<String> allCoders = Arrays.asList("oscar", "juan", "unnati");

    private String getCoder3(String coder1, String coder2) {
        final ArrayList<String> codersCopy = new ArrayList<>(allCoders);
        codersCopy.removeAll(Arrays.asList(coder1, coder2));
        if (codersCopy.size() != 1)
            throw new RuntimeException("Error!");

        return codersCopy.get(0);
    }

    private static String getLabelString2(Labels sentLabels, boolean onlyOb2) {
        if (onlyOb2) {
            return "not supported";
        } else {
            return sentLabels.getIsOB() + ";" + sentLabels.getIsEB() + ";" + sentLabels.getIsSR();
        }
    }


}
