package seers.disqueryreform.querygen.d4j.autocoding;

import java.io.File;
import java.util.List;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.xml.XMLHelper;
import seers.bugrepclassifier.parsing.TextParser;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportDescription;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;
import seers.disqueryreform.querygen.parsing.SentenceClassifier;

public class D4jBugExecutor extends ThreadProcessor {

	private List<File> fileList;
	static TextParser txtParser = new TextParser();

	public D4jBugExecutor(ThreadParameters params) {
		super(params);
		fileList = params.getListParam(File.class, ThreadExecutor.ELEMENTS_PARAM);
	}

	@Override
	public void executeJob() throws Exception {

		SentenceClassifier sentClfier = new SentenceClassifier(D4JAutoCoding.sentenceClassifier,
				D4JAutoCoding.paragraphClassifier);

		// for each JSON file in the data folder
		for (File dataFile : fileList) {

			// check if the system is in the list of sentences
			String system = dataFile.getParentFile().getName();
			String bugId = dataFile.getName().replace(".xml", "");

			File sysFolder = new File(D4JAutoCoding.outputFolder + File.separator + system);
			if (!sysFolder.exists()) {
				sysFolder.mkdir();
			}

			LOGGER.debug("Processing bug: " + system + ";" + bugId);

			try {

				seers.bugrepcompl.entity.BugReport bugRepNotParsed = XMLHelper
						.readXML(seers.bugrepcompl.entity.BugReport.class, dataFile);

				// parse the description
				PatternLabeledBugReportDescription parsedDesc = txtParser.parseText2(bugRepNotParsed.getDescription());

				// write the XML

				PatternLabeledBugReportTitle title = new PatternLabeledBugReportTitle();
				title.setValue(bugRepNotParsed.getTitle());

				seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport codingBug = new seers.bugrepcompl.entity
						.patterncoding.PatternLabeledBugReport(
						bugId, title, parsedDesc);
				sentClfier.classifySentences(codingBug);

				File outputFile = new File(sysFolder + File.separator + bugId + ".xml");
				XMLHelper.writeXML(codingBug, outputFile);
			} catch (Exception e) {
				LOGGER.error("Error for bug: " + system + " " + bugId, e);
			}

		}
	}

}
