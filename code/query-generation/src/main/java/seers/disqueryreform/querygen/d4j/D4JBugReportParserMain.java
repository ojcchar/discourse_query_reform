package seers.disqueryreform.querygen.d4j;

import java.io.File;
import java.util.List;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.xml.XMLHelper;
import seers.bugrepclassifier.parsing.TextParser;
import seers.bugrepcompl.entity.BugReport;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportDescription;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;
import seers.disqueryreform.base.D4JUtils;
//import seers.stormedclient.StormedClient;

public class D4JBugReportParserMain {

	static boolean removeCode = false;
//	static String bugsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/bug_reports";
//	static String querySampleFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/d4j-all-hard-to-retrieve-queries.csv";
//	static String outputFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/d4j-all-hard-to-retrieve";
	

	static String bugsFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/query_quality_data/bug_reports";
	static String querySampleFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/quer_qual-all-hard-to-retrieve-queries.csv";
	static String outputFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/quer_qual-all-hard-to-retrieve";
	
	static TextParser txtParser = new TextParser();

	public static void main(String[] args) throws Exception {

		// read all queries from sample
		List<List<String>> lines = D4JUtils.readLines(new File(querySampleFile), ',');

		ThreadExecutor.executePaginated(lines, BugProcessor.class, new ThreadParameters(), 15, 10);

	}

	public static class BugProcessor extends ThreadProcessor {

		@SuppressWarnings("rawtypes")
		private List<List> lines;

		public BugProcessor(ThreadParameters params) {
			super(params);
			lines = params.getListParam(List.class, ThreadExecutor.ELEMENTS_PARAM);
		}

		@SuppressWarnings("unchecked")
		@Override
		public void executeJob() throws Exception {
			for (List<String> line : lines) {

				String system = line.get(0);
				String bugId = line.get(1);

				System.out.println("Processing: " + line);

				// read bug report
				BugReport bug = XMLHelper.readXML(BugReport.class,
						bugsFolder + File.separator + system.toLowerCase() + File.separator + bugId + ".xml");

				// parse bug report
				String description = bug.getDescription();
				String processedDesc = description;
				if (removeCode) {
					//processedDesc = StormedClient.isolateText(description);
				}

				PatternLabeledBugReportDescription desc = txtParser.parseText2
						(processedDesc);

				PatternLabeledBugReportTitle title = new PatternLabeledBugReportTitle();
				String titleText = bug.getTitle();
				String processedTitle = titleText;
				if (removeCode) {
					//processedTitle = StormedClient.isolateText(titleText);
				}
				title.setValue(processedTitle);
				
				seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport bugParsed = new seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport(
						bug.getId(), title, desc);

				File sysFolder = new File(outputFolder + File.separator + system.toLowerCase());
				sysFolder.mkdirs();

				File outputFile = new File(sysFolder + File.separator + bugId + ".xml");
				XMLHelper.writeXML(bugParsed, outputFile);

			}
		}

	}

}
