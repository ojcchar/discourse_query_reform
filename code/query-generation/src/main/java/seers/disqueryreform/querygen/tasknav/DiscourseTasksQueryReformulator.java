package seers.disqueryreform.querygen.tasknav;

import edu.utdallas.seers.entity.BugReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport;
import seers.disqueryreform.base.*;
import seers.disqueryreform.querygen.utils.OriginalBugsReader;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Generates queries for all combinations of tasks and bug report components.
 */
public class DiscourseTasksQueryReformulator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiscourseTasksQueryReformulator.class);
    private final OriginalBugsReader originalBugsReader;
    private String baseDataFolder;
    private DataSet[] dataSets;
    private Set<DiscourseTaskStrategy> strategies;
    private final Set<String> codeTypesToDiscard;
    @Nullable
    private List<TaskNavContainer> taskNavContainers;
    private Map<DataSet, Integer> totalBugs = new HashMap<>();

    public DiscourseTasksQueryReformulator(String baseDataFolder, DataSet[] dataSets,
                                           Set<DiscourseTaskStrategy> strategies, Set<String> codeTypesToDiscard,
                                           @Nullable List<TaskNavContainer> taskNavContainers) {
        this.baseDataFolder = baseDataFolder;
        this.dataSets = dataSets;
        this.strategies = strategies;
        this.codeTypesToDiscard = codeTypesToDiscard;
        this.taskNavContainers = taskNavContainers;
        this.originalBugsReader = new OriginalBugsReader(Paths.get(baseDataFolder));
    }

    List<StrategyQueryContainer> generateQueryCombinations(boolean includeAllText)
            throws IOException, ParserConfigurationException, JAXBException, SAXException {

        Map<String, StrategyQueryContainer> strategyCache = new HashMap<>();

        for (DataSet dataSet : dataSets) {
            LOGGER.info(String.format("Generating query combinations for %s", dataSet));

            Map<String, List<CodedBugReportWithTasks>> bugReports = loadBugReports(dataSet);

            for (DiscourseTaskStrategy strategy : strategies) {
                StrategyQueryContainer container =
                        strategyCache.computeIfAbsent(strategy.toString(), StrategyQueryContainer::new);

                for (String system : bugReports.keySet()) {
                    List<DiscourseQuery> queries = bugReports.get(system).parallelStream()
                            .map(b -> b.extractTextForStrategy(strategy).orElse(null))
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList());

                    container.addProject(dataSet, system, queries);
                }
            }
        }

        if (includeAllText) {
        /* We add ALL_TEXT strategy separately because doing it using the coded bug reports might
         result in code terms being duplicated */
            strategyCache.put("", generateAllTextStrategy());
        }

        return new ArrayList<>(strategyCache.values());
    }

    public StrategyQueryContainer generateAllTextStrategy() throws SAXException, JAXBException, ParserConfigurationException, IOException {
        StrategyQueryContainer container = new StrategyQueryContainer(DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME);

        for (DataSet dataSet : dataSets) {
            int allTextQueries = 0;

            for (Map.Entry<String, List<BugReport>> projectEntry : originalBugsReader.readOriginalBugs(dataSet).entrySet()) {
                allTextQueries += projectEntry.getValue().size();

                List<DiscourseQuery> queries = projectEntry.getValue().stream()
                        .map(b -> new DiscourseQuery(
                                b.getKey(), DiscourseTaskStrategy.ALL_TEXT_STRATEGY_NAME, null,
                                b.getTitle(), b.getDescription()
                        ))
                        .collect(Collectors.toList());

                container.addProject(dataSet, projectEntry.getKey(), queries);
            }

            // Total bugs will be empty when this method is used directly without generating all combinations
            if (!totalBugs.isEmpty() && allTextQueries != totalBugs.get(dataSet)) {
                throw new IllegalStateException("Total number of queries and all text bugs should " +
                        "be the same for dataset " + dataSet);
            }
        }

        return container;
    }

    private Map<String, List<CodedBugReportWithTasks>> loadBugReports(DataSet dataSet) throws IOException, JAXBException, SAXException, ParserConfigurationException {
        Map<String, List<CodedBugReportWithTasks>> bugReports = new HashMap<>();

        File codedBugsFolder = DataSetUtils.getCodedBugReportsFolder(baseDataFolder, dataSet);
        List<DataSetUtils.BugKey> bugKeys = DataSetUtils.readSampledBugReports(baseDataFolder, dataSet);

        totalBugs.put(dataSet, bugKeys.size());

        for (DataSetUtils.BugKey bugKey : bugKeys) {
            String system = bugKey.system;
            String bugId = bugKey.bugId;
            String codedFilePath = codedBugsFolder + File.separator + system + File.separator
                    + bugId + ".xml";
            File codeTaggedFile = DataSetUtils.getBugReportCodeFile(baseDataFolder, dataSet, system, bugId, false);
            PatternLabeledBugReport labeledBugReport = PatternLabeledBugReport.buildFromComponents(codedFilePath, codeTaggedFile);

            if (taskNavContainers != null) {
                for (TaskNavContainer taskNavContainer : taskNavContainers) {
                    bugReports.computeIfAbsent(system, s -> new ArrayList<>())
                            .add(new CodedBugReportWithTasks(dataSet, system, labeledBugReport, taskNavContainer, codeTypesToDiscard));
                }
            } else {
                bugReports.computeIfAbsent(system, s -> new ArrayList<>())
                        .add(new CodedBugReportWithTasks(dataSet, system, labeledBugReport, null, codeTypesToDiscard));
            }
        }

        return bugReports;
    }
}
