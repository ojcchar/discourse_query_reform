package seers.disqueryreform.querygen.sample;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.DataSetUtils.BugKey;

public class SampleMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(SampleMain.class);

	static String sampleFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/queries/selected-top-1s.csv";
	static String dataFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/parsed_bugs_lobser_data";
	static String outputFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/coding/lobster_second_sample";

	public static void main(String[] args) throws Exception {

		// read all the bugs
		List<BugKey> bugs = DataSetUtils.readSampledBugReports(sampleFile);

		for (BugKey bugKey : bugs) {

			try {
				String system = bugKey.system.toLowerCase();
				String bugId = bugKey.bugId;

				File srcFile = new File(dataFolder + File.separator + system + File.separator + bugId + ".xml");

				File sysFolder = new File(outputFolder + File.separator + system);
				if (!sysFolder.exists()) {
					sysFolder.mkdir();
				}
				File destFile = new File(outputFolder + File.separator + system + File.separator + bugId + ".xml");

				FileUtils.copyFile(srcFile, destFile);
			} catch (Exception e) {
				LOGGER.error("Error for bug: " + bugKey, e);
			}
		}

	}

}
