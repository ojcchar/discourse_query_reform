package seers.disqueryreform.querygen.querrqualdata;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.text.beta.similarity.LevenshteinDistance;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.disqueryreform.base.D4JUtils;

public class BugIdFinderMain {

	static String queriesFile = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/query_quality_data/original_queries.csv";
	static List<String> systems = Arrays.asList("adempiere-3.1.0");
	static String bugsFolder = "C:/Users/ojcch/Documents/Projects/Discourse_query_reformulation/data/query_quality_data/bugs2";

	static ConcurrentHashMap<String, List<List<String>>> sysBugs = new ConcurrentHashMap<>();

	public static void main(String[] args) throws Exception {

		List<List<String>> readLines = D4JUtils.readLines(new File(queriesFile), ';');

		List<List<String>> lines = readLines.stream().filter(l -> systems.contains(l.get(0).toLowerCase()))
				.collect(Collectors.toList());

		ThreadExecutor.executePaginated(lines, LineBugProcessor.class, new ThreadParameters());

	}

	public static class LineBugProcessor extends ThreadProcessor {

		private List<List> lines;

		public LineBugProcessor(ThreadParameters params) {
			super(params);
			lines = params.getListParam(List.class, ThreadExecutor.ELEMENTS_PARAM);
		}

		@Override
		public void executeJob() throws Exception {

			LevenshteinDistance d = new LevenshteinDistance();
			for (List<String> line : lines) {
				String system = line.get(0).toLowerCase();

				List<List<String>> bugs = getBugs(system);

				// --------------------------------

				String queryId = line.get(1);
				String text = line.get(2);
				String titleText = getSubString(text);

//				HashMap<BugText, Integer> distances = new HashMap<>();

				BugText minBug = null;

				for (List<String> bug : bugs) {
					String bugId = bug.get(1);
					String bugText = bug.get(14) + " " + bug.get(12);
					String bugText2 = getSubString(bugText);

					Integer dis = d.apply(titleText, bugText2);

					if (minBug == null) {
						minBug = new BugText(system, bugId, bugText2, dis);
					}else{
						if (minBug.d > dis) {
							minBug = new BugText(system, bugId, bugText2, dis);
						}
					}

//					distances.put(new BugText(system, bugId, bugText2), dis);
				}

				// ------------------

//				ArrayList<Entry<BugText, Integer>> distList = new ArrayList<>(distances.entrySet());
				// Collections.sort(distList, (i1, i2) ->
				// i1.getValue().compareTo(i2.getValue()));

//				Entry<BugText, Integer> entry = distList.stream()
//						.max((i1, i2) -> i1.getValue().compareTo(i2.getValue())).get();

				System.out.println(system + ";" + queryId + ";" + text + ";" + minBug.d + ";"
						+ minBug.bugId + ";" + minBug.text);

				// System.out.println(system + ";" + queryId + ";" + text + ";"
				// + distList.get(0).getValue() + ";"
				// + distList.get(0).getKey().bugId + ";" +
				// distList.get(0).getKey().text + ";"
				// + distList.get(1).getValue() + ";" +
				// distList.get(1).getKey().bugId + ";"
				// + distList.get(1).getKey().text);

			}
		}

		private List<List<String>> getBugs(String system) throws IOException, FileNotFoundException {
			synchronized (sysBugs) {
				List<List<String>> bugs = sysBugs.get(system);
				if (bugs == null) {
					String bugsFile = bugsFolder + File.separator + system + "_Queries_info.txt";
					bugs = D4JUtils.readLines(new File(bugsFile), ';');
					sysBugs.put(system, bugs);
				}
				return bugs;
			}
		}

		private String getSubString(String text) {
			int LIMIT = 70;
			return text.length() <= LIMIT ? text : text.substring(0, LIMIT);
		}
	}

	public static class BugText {

		public String system;
		public String bugId;
		public String text;
		public Integer d;

		public BugText(String system, String bugId, String text) {
			super();
			this.system = system;
			this.bugId = bugId;
			this.text = text;
		}

		public BugText(String system, String bugId, String text, Integer d) {
			super();
			this.system = system;
			this.bugId = bugId;
			this.text = text;
			this.d = d;
		}

	}
}
