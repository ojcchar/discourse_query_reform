package seers.disqueryreform.querygen.d4j.autocoding;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.bugrepclassifier.BugReportClassifier;
import seers.bugrepclassifier.patternbased.PatternBugReportClassifier;

public class D4JAutoCoding {

	static String dataFolder;
	static String outputFolder;
	public static String patternListFolder;

	public static BugReportClassifier sentenceClassifier;
	public static BugReportClassifier paragraphClassifier;

	public static void main(String[] args) throws Exception {

		dataFolder = args[0];
		outputFolder = args[1];
		patternListFolder = args[2];

		sentenceClassifier = new PatternBugReportClassifier(
				new File(patternListFolder + File.separator + "sentence_patterns.csv"));
		paragraphClassifier = new PatternBugReportClassifier(
				new File(patternListFolder + File.separator + "paragraph_patterns.csv"));

		// --------------------------------------

		// read the folders
		File dataFold = new File(dataFolder);

		String[] extensions = { "xml" };
		Collection<File> fileList = FileUtils.listFiles(dataFold, extensions, true);

		Class<? extends ThreadProcessor> class1 = D4jBugExecutor.class;
		ThreadExecutor.executePaginated(new ArrayList<>(fileList), class1, new ThreadParameters());

	}

}
