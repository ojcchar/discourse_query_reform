package seers.disqueryreform.querygen.goldset;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seers.disqueryreform.querygen.utils.Utils;
import seers.disqueryreform.querygen.utils.Utils.D4JBugMapping;

public class GoldSetCopyMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(GoldSetCopyMain.class);

	static String mappingFile = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/bug_id_mapping.csv";
	static String originalGoldSetDir = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/original_gold_sets";
	static String outputFolder = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/defects4j_data/gold_sets";

	public static void main(String[] args) throws Exception {

		// read all the bugs
		List<D4JBugMapping> bugs = Utils.readD4JBugsMapping(mappingFile);

		for (D4JBugMapping bug : bugs) {

			try {
				String system = bug.system;
				String d4jId = bug.d4jId;
				String bugId = bug.bugId;

				File srcFile1 = new File(
						originalGoldSetDir + File.separator + system + "_" + d4jId + File.separator + "goldset.txt");
				File srcFile2 = new File(originalGoldSetDir + File.separator + system + "_" + d4jId + File.separator
						+ "method_goldset.txt");

				if (!srcFile2.exists()) {
					LOGGER.debug("No method goldset for " + bug);
					continue;
				}

				File destDir = new File(outputFolder + File.separator + system.toLowerCase() + File.separator + bugId);
				if (!destDir.exists()) {
					destDir.mkdirs();
				}

				FileUtils.copyFileToDirectory(srcFile1, destDir);
				FileUtils.copyFileToDirectory(srcFile2, destDir);

				FileUtils.moveFile(new File(destDir + File.separator + "goldset.txt"),
						new File(destDir + File.separator + "class_goldset.txt"));

			} catch (Exception e) {
				LOGGER.error("Error for bug: " + bug, e);
			}
		}

	}
}
