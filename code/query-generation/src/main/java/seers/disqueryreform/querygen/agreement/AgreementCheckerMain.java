package seers.disqueryreform.querygen.agreement;

import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.Labels;
import seers.bugrepcompl.entity.codingparse.*;
import seers.bugrepcompl.entity.patterncoding.*;
import seers.bugrepcompl.xmlcoding.AgreementMain;
import seers.disqueryreform.base.D4JUtils;
import seers.disqueryreform.base.DataSet;

import java.io.File;
import java.util.List;

public class AgreementCheckerMain {

    static String codingFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data\\04-23" +
            "-bug_coding-xml";
    static String dataset = DataSet.BRT.toString().toLowerCase();
    static String bugsFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\output" +
            "\\sampling\\4-23\\sample-4-23.csv";
    static String coder1 = "juan";
    static String coder2 = "oscar";
    static boolean onlyOb = false;

    public static void main(String[] args) throws Exception {


        final AgreementCheckerMain checker = new AgreementCheckerMain();

        List<List<String>> listOfBugs = D4JUtils.readLines(new File(bugsFile), ';');

        for (List<String> line : listOfBugs) {

            try {
                final String system = line.get(1);
                final String bugId = line.get(2);

                File f1 = new File(codingFolder + File.separator + dataset + File.separator
                        + coder1 + File.separator + system + File.separator + bugId + ".xml");
                File f2 = new File(codingFolder + File.separator + dataset + File.separator
                        + coder2 + File.separator + system + File.separator + bugId + ".xml");

                if (!f1.exists() || !f2.exists())
                    continue;

                // read bug 1
                PatternLabeledBugReport bug1 = XMLHelper.readXML(PatternLabeledBugReport.class, f1);

                // read bug 2
                PatternLabeledBugReport bug2 = XMLHelper.readXML(PatternLabeledBugReport.class, f2);

                // check for same # of sentences and paragraphs
                checker.checkBugs(bug1, bug2);

                // propagate coding
                checker.propagateCoding(bug1);
                checker.propagateCoding(bug2);

                // determine agreement
                checker.determineAgreement(system, bugId, bug1, bug2, coder1, coder2, onlyOb);
            } catch (Exception e) {
                System.err.println("Error for bug: " + line);
                e.printStackTrace();
            }
        }

    }

    void determineAgreement(String system, String bugId, PatternLabeledBugReport bug1, PatternLabeledBugReport
            bug2, String coder1, String coder2, boolean onlyOb2) {

        determineTitleAgreement(system, bugId, bug1, bug2, coder1, coder2, onlyOb2);

        // ---------------------------------------------

        determineDescriptionAgreement(system, bugId, bug1, bug2, coder1, coder2, onlyOb2);
    }

    private void determineDescriptionAgreement(String system, String bugId, PatternLabeledBugReport bug1,
                                               PatternLabeledBugReport bug2, String coder1, String coder2, boolean
                                                       onlyOb2) {

        if (bug1.getDescription() == null) {
            return;
        }

        List<PatternLabeledDescriptionSentence> sents1 = bug1.getDescription().getAllSentences();
        List<PatternLabeledDescriptionSentence> sents2 = bug2.getDescription().getAllSentences();

        for (int i = 0; i < sents1.size(); i++) {

            PatternLabeledDescriptionSentence sent1 = sents1.get(i);
            PatternLabeledDescriptionSentence sent2 = sents2.get(i);

            String sentId = sent1.getId();
            if (!sentId.equals(sent2.getId())) {
                throw new RuntimeException("Sentences do not correspond: " + sentId + " vs. " + sent2.getId());
            }

            Labels sentLabels1 = new Labels(sent1.getOb().trim(), sent1.getEb().trim(), sent1.getSr().trim());
            Labels sentLabels2 = new Labels(sent2.getOb().trim(), sent2.getEb().trim(), sent2.getSr().trim());

            checkLabels(system, bugId, sentLabels1, sentLabels2, sentId, coder1, coder2, onlyOb2, sent1.getValue());
        }
    }

    private void determineTitleAgreement(String system, String bugId, PatternLabeledBugReport bug1,
                                         PatternLabeledBugReport bug2, String coder1, String coder2, boolean onlyOb2) {
        PatternLabeledBugReportTitle title1 = bug1.getTitle();
        PatternLabeledBugReportTitle title2 = bug2.getTitle();

        Labels sentLabels1 = new Labels(title1.getOb().trim(), title1.getEb().trim(), title1.getSr().trim());
        Labels sentLabels2 = new Labels(title2.getOb().trim(), title2.getEb().trim(), title2.getSr().trim());
        String sentId = "0";

        checkLabels(system, bugId, sentLabels1, sentLabels2, sentId, coder1, coder2, onlyOb2, title1.getValue());
    }

    void checkLabels(String system, String bugId, Labels sentLabels1, Labels sentLabels2,
                     String sentId, String coder1, String coder2, boolean onlyOb2, String stncText) {
        String agreement = "1";

        String labelString1 = getLabelString(sentLabels1, onlyOb2);
        String labelString2 = getLabelString(sentLabels2, onlyOb2);
        if (!labelString1.equals(labelString2)) {
            agreement = "0";
        }

        System.out.println(system + ";" + bugId + ";" + sentId + ";" + labelString1 + ";" + labelString2 + ";"
                + agreement + ";" + coder1 + ";" + coder2);
    }

    private String getLabelString(Labels sentLabels, boolean onlyOb2) {
        if (onlyOb2) {
            return "[ob=" + sentLabels.getIsOB() + ", eb=, s2r=]";
        } else {
            return "[ob=" + sentLabels.getIsOB() + ", eb=" + sentLabels.getIsEB() + ", s2r=" + sentLabels.getIsSR()
                    + "]";
        }
    }

    void propagateCoding(PatternLabeledBugReport bug) {

        PatternLabeledBugReportDescription cDescription = bug.getDescription();
        if (cDescription == null) {
            return;
        }

        List<PatternLabeledDescriptionParagraph> paragraphs = cDescription.getParagraphs();
        for (PatternLabeledDescriptionParagraph paragraph : paragraphs) {

            Labels paragraphLabels = new Labels(paragraph.getOb().trim(), paragraph.getEb().trim(),
                    paragraph.getSr().trim());

            List<PatternLabeledDescriptionSentence> sentences = paragraph.getSentences();
            for (PatternLabeledDescriptionSentence sent : sentences) {
                Labels sentLabels = new Labels(sent.getOb().trim(), sent.getEb().trim(), sent.getSr().trim());
                Labels mergedLabels = AgreementMain.mergeLabels(sentLabels, paragraphLabels);

                sent.setOb(mergedLabels.getIsOB());
                sent.setEb(mergedLabels.getIsEB());
                sent.setSr(mergedLabels.getIsSR());

            }
        }
    }

    void checkBugs(PatternLabeledBugReport bug1, PatternLabeledBugReport bug2) {
        PatternLabeledBugReportDescription desc1 = bug1.getDescription();
        PatternLabeledBugReportDescription desc2 = bug2.getDescription();

        if (desc1 == null || desc2 == null) {

            if (!(desc1 == null && desc2 == null)) {
                throw new RuntimeException("Descriptions do not match");
            }
        } else {

            List<PatternLabeledDescriptionParagraph> paragraphs1 = desc1.getParagraphs();
            List<PatternLabeledDescriptionParagraph> paragraphs2 = desc2.getParagraphs();

            if (paragraphs1.size() != paragraphs2.size()) {
                throw new RuntimeException("Paragraphs do not match");
            }

            List<PatternLabeledDescriptionSentence> sent1 = desc1.getAllSentences();
            List<PatternLabeledDescriptionSentence> sent2 = desc2.getAllSentences();

            if (sent1.size() != sent2.size()) {
                throw new RuntimeException("Sentences do not match");
            }
        }

    }

    public void checkBugs(LabeledBugReport bug1, LabeledBugReport bug2) {
        LabeledBugReportDescription desc1 = bug1.getDescription();
        LabeledBugReportDescription desc2 = bug2.getDescription();

        if (desc1 == null || desc2 == null) {

            if (!(desc1 == null && desc2 == null)) {
                throw new RuntimeException("Descriptions do not match");
            }
        } else {

            List<LabeledDescriptionParagraph> paragraphs1 = desc1.getParagraphs();
            List<LabeledDescriptionParagraph> paragraphs2 = desc2.getParagraphs();

            if (paragraphs1 == null || paragraphs2 == null) {

                if (!(paragraphs1 == null && paragraphs2 == null)) {
                    throw new RuntimeException("Paragraphs do not match");
                }
            } else {

                if (paragraphs1.size() != paragraphs2.size()) {
                    throw new RuntimeException("Paragraphs do not match");
                }

                List<LabeledDescriptionSentence> sent1 = desc1.getAllSentences();
                List<LabeledDescriptionSentence> sent2 = desc2.getAllSentences();

                if (sent1.size() != sent2.size()) {
                    throw new RuntimeException("Sentences do not match");
                }
            }
        }

    }

    public void propagateCoding(LabeledBugReport bug) {
        LabeledBugReportDescription cDescription = bug.getDescription();
        if (cDescription == null) {
            return;
        }

        List<LabeledDescriptionParagraph> paragraphs = cDescription.getParagraphs();
        if (paragraphs == null)
            return;
        for (LabeledDescriptionParagraph paragraph : paragraphs) {

            Labels paragraphLabels = new Labels(paragraph.getOb().trim(), paragraph.getEb().trim(),
                    paragraph.getSr().trim());

            List<LabeledDescriptionSentence> sentences = paragraph.getSentences();
            for (LabeledDescriptionSentence sent : sentences) {
                Labels sentLabels = new Labels(sent.getOb().trim(), sent.getEb().trim(), sent.getSr().trim());
                Labels mergedLabels = AgreementMain.mergeLabels(sentLabels, paragraphLabels);

                sent.setOb(mergedLabels.getIsOB());
                sent.setEb(mergedLabels.getIsEB());
                sent.setSr(mergedLabels.getIsSR());

            }
        }
    }

    public void determineAgreement(String system, String bugId, LabeledBugReport bug1, LabeledBugReport bug2,
                                   String coder1, String coder2, boolean onlyOb2) {
        determineTitleAgreement(system, bugId, bug1, bug2, coder1, coder2, onlyOb2);

        // ---------------------------------------------

        determineDescriptionAgreement(system, bugId, bug1, bug2, coder1, coder2, onlyOb2);
    }

    private void determineDescriptionAgreement(String system, String bugId, LabeledBugReport bug1, LabeledBugReport
            bug2, String coder1, String coder2, boolean onlyOb2) {

        if (bug1.getDescription() == null) {
            return;
        }

        if (bug1.getDescription().getParagraphs() == null)
            return;

        List<LabeledDescriptionSentence> sents1 = bug1.getDescription().getAllSentences();
        List<LabeledDescriptionSentence> sents2 = bug2.getDescription().getAllSentences();

        for (int i = 0; i < sents1.size(); i++) {

            LabeledDescriptionSentence sent1 = sents1.get(i);
            LabeledDescriptionSentence sent2 = sents2.get(i);

            String sentId = sent1.getId();
            if (!sentId.equals(sent2.getId())) {
                throw new RuntimeException("Sentences do not correspond: " + sentId + " vs. " + sent2.getId());
            }

            Labels sentLabels1 = new Labels(sent1.getOb().trim(), sent1.getEb().trim(), sent1.getSr().trim());
            Labels sentLabels2 = new Labels(sent2.getOb().trim(), sent2.getEb().trim(), sent2.getSr().trim());

            checkLabels(system, bugId, sentLabels1, sentLabels2, sentId, coder1, coder2, onlyOb2, sent1.getValue());
        }
    }

    private void determineTitleAgreement(String system, String bugId, LabeledBugReport bug1, LabeledBugReport bug2, String coder1, String coder2, boolean onlyOb2) {
        LabeledBugReportTitle title1 = bug1.getTitle();
        LabeledBugReportTitle title2 = bug2.getTitle();

        Labels sentLabels1 = new Labels(title1.getOb().trim(), title1.getEb().trim(), title1.getSr().trim());
        Labels sentLabels2 = new Labels(title2.getOb().trim(), title2.getEb().trim(), title2.getSr().trim());
        String sentId = "0";

        checkLabels(system, bugId, sentLabels1, sentLabels2, sentId, coder1, coder2, onlyOb2, title1.getValue());
    }
}
