package seers.disqueryreform.querygen;

import org.apache.commons.io.FileUtils;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.utils.JavaUtils;
import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.Labels;
import seers.bugrepcompl.entity.patterncoding.*;
import seers.bugrepcompl.xmlcoding.AgreementMain;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.DataSetUtils.BugKey;
import seers.disqueryreform.base.codeident.CodeTaggedBugReport;
import seers.disqueryreform.base.codeident.TaggedText;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BugExecutor extends ThreadProcessor {

    private List<BugKey> bugs;
    private Boolean andOperator;


    final Set<String> TEXT_TYPES_TO_DISCARD = JavaUtils.getSet("TextFragmentNode", "BaseReferenceTypeNode",
            "QualifiedIdentifierNode", "IdentifierNode", "SeparatorNode", "ArrayTypeNode", "InterfaceRelationshipNode");


    public BugExecutor(ThreadParameters params) {
        super(params);
        bugs = params.getListParam(BugKey.class, ThreadExecutor.ELEMENTS_PARAM);
        andOperator = MainQueryReformulator.andOperator;
    }

    @Override
    public void executeJob() throws Exception {

        // for each bug
        for (BugKey bug : bugs) {

            /*if (!"84558".equals(bug.bugId)){
                continue;
            }*/

            try {
                String system = bug.system;
                String bugId = bug.bugId;

                // read the coded bug report
                String filepath = MainQueryReformulator.codedBugsFolder + File.separator + system + File.separator
                        + bugId + ".xml";
                PatternLabeledBugReport bugCoded = XMLHelper.readXML(PatternLabeledBugReport.class, filepath);

                if (bugCoded.getDescription() == null) {
                    LOGGER.warn("Description is null: " + system + ";" + bugId);
                }

                //------------------------------

                //read the xml with the identified code snippets
                final File bugReportCodeFile = DataSetUtils.getBugReportCodeFile(MainQueryReformulator
                        .baseDataFolder, MainQueryReformulator.dataset, system, bugId, false);
                final CodeTaggedBugReport codeTaggedBR = XMLHelper.readXML(CodeTaggedBugReport.class,
                        bugReportCodeFile);

                //-------------------------------
                //filter only the code snippets from the description

                List<TaggedText> descTaggedTexts = new ArrayList<>();
                if (codeTaggedBR.getDescription() != null) {
                    descTaggedTexts = codeTaggedBR.getDescription().getTaggedTexts();
                    if (descTaggedTexts == null)
                        descTaggedTexts = new ArrayList<>();
                }

                final List<TaggedText> codeTaggedTexts = descTaggedTexts.stream().filter(taggedText -> {
                    final String type = taggedText.getType();
                    return !TEXT_TYPES_TO_DISCARD.contains(type);
                }).collect(Collectors.toList());

                //-------------------------------------------

                // get the title
                String title = getTitle(bugCoded.getTitle());
                // get the description
                String description = getDescription(bugCoded.getDescription(), codeTaggedTexts);

                // if ((title + description).isEmpty()) {
                // LOGGER.warn("Bug is empty: " + bug);
                // }

                // -----------------------------------------

                if (discardBug(bugCoded, codeTaggedTexts)) {
                    continue;
                }

                // -----------------------------------------

                File file = new File(MainQueryReformulator.queriesOutFolder + File.separator
                        + MainQueryReformulator.strategy.toString() + ".csv");
                CharSequence data = system + ";" + bugId + ";" + hasOB(bugCoded) + ";" + hasEB(bugCoded) + ";"
                        + hasSR(bugCoded) + ";" + hasOther(bugCoded) + ";" + (!codeTaggedTexts.isEmpty() ? "x" : "")
                        + "\n";
                FileUtils.write(file, data, true);

                // -----------------------------------------

                edu.utdallas.seers.entity.BugReport jsonBug = new edu.utdallas.seers.entity.BugReport(bugId, title,
                        description);

                // add to the json file
                File outputFile = new File(MainQueryReformulator.strategyOutputDir + File.separator + system + ".json");
                FileUtils.write(outputFile, MainQueryReformulator.gson.toJson(jsonBug) + "\n", true);

            } catch (Exception e) {
                LOGGER.error("Error for bug: " + bug, e);
            }
        }
    }

    private boolean discardBug(PatternLabeledBugReport bugCoded, List<TaggedText> codeTaggedTexts) {

        if (!andOperator) {
            return false;
        }

        // ---------------------------------------------

        PatternLabeledBugReportTitle title = bugCoded.getTitle();

        boolean hasOb = false;
        boolean hasEb = false;
        boolean hasSr = false;
        boolean hasCode = !codeTaggedTexts.isEmpty();

        if (hasInfo(title.getOb())) {
            hasOb = true;
        }
        if (hasInfo(title.getEb())) {
            hasEb = true;
        }
        if (hasInfo(title.getSr())) {
            hasSr = true;
        }

        // ---------------------------------------------

        PatternLabeledBugReportDescription description = bugCoded.getDescription();

        if (description != null) {

            List<PatternLabeledDescriptionParagraph> paragraphs = description.getParagraphs();
            if (paragraphs != null) {
                for (PatternLabeledDescriptionParagraph parag : paragraphs) {

                    if (hasInfo(parag.getOb())) {
                        hasOb = true;
                    }
                    if (hasInfo(parag.getEb())) {
                        hasEb = true;
                    }
                    if (hasInfo(parag.getSr())) {
                        hasSr = true;
                    }
                }
                List<PatternLabeledDescriptionSentence> sentences = description.getAllSentences();
                for (PatternLabeledDescriptionSentence sent : sentences) {

                    if (hasInfo(sent.getOb())) {
                        hasOb = true;
                    }
                    if (hasInfo(sent.getEb())) {
                        hasEb = true;
                    }
                    if (hasInfo(sent.getSr())) {
                        hasSr = true;
                    }
                }
            }
        }

        // ---------------------------------------------
        //all the bugs have title

        boolean discard = true;
        switch (MainQueryReformulator.strategy) {
            case ALL_TEXT:
            case TITLE:
                discard = false;
                break;
            case OB:
            case OB_TITLE:
                if (hasOb) {
                    discard = false;
                }
                break;
            case EB:
            case EB_TITLE:
                if (hasEb) {
                    discard = false;
                }
                break;
            case S2R:
            case S2R_TITLE:
                if (hasSr) {
                    discard = false;
                }
                break;
            case OB_EB:
            case OB_EB_TITLE:
                if (hasOb && hasEb) {
                    discard = false;
                }
                break;
            case OB_S2R:
            case OB_S2R_TITLE:
                if (hasOb && hasSr) {
                    discard = false;
                }
                break;
            case EB_S2R:
            case EB_S2R_TITLE:
                if (hasEb && hasSr) {
                    discard = false;
                }
                break;
            case OB_EB_S2R:
            case OB_EB_S2R_TITLE:
                if (hasOb && hasEb && hasSr) {
                    discard = false;
                }
                break;

            case TITLE_CODE:
            case CODE:
                if (hasCode) {
                    discard = false;
                }
                break;
            case OB_CODE:
            case OB_TITLE_CODE:
                if (hasOb && hasCode) {
                    discard = false;
                }
                break;
            case EB_CODE:
            case EB_TITLE_CODE:
                if (hasEb && hasCode) {
                    discard = false;
                }
                break;
            case S2R_CODE:
            case S2R_TITLE_CODE:
                if (hasSr && hasCode) {
                    discard = false;
                }
                break;
            case OB_EB_CODE:
            case OB_EB_TITLE_CODE:
                if (hasOb && hasEb && hasCode) {
                    discard = false;
                }
                break;
            case OB_S2R_CODE:
            case OB_S2R_TITLE_CODE:
                if (hasOb && hasSr && hasCode) {
                    discard = false;
                }
                break;
            case EB_S2R_CODE:
            case EB_S2R_TITLE_CODE:
                if (hasEb && hasSr && hasCode) {
                    discard = false;
                }
                break;
            case OB_EB_S2R_CODE:
            case OB_EB_S2R_TITLE_CODE:
                if (hasOb && hasEb && hasSr && hasCode) {
                    discard = false;
                }
                break;
            default:
                break;
        }

        return discard;
    }

    private boolean hasInfo(String value) {
        return !value.trim().isEmpty();
    }

    private String hasOther(PatternLabeledBugReport bugCoded) {

        boolean isTitle = bugCoded.getTitle().getOb().trim().isEmpty() && bugCoded.getTitle().getEb().trim().isEmpty()
                && bugCoded.getTitle().getSr().trim().isEmpty();
        boolean isSentence = false;

        PatternLabeledBugReportDescription description = bugCoded.getDescription();
        if (description != null) {

            isSentence = description.getAllSentences().stream().anyMatch(
                    s -> s.getOb().trim().isEmpty() && s.getEb().trim().isEmpty() && s.getSr().trim().isEmpty());
        }

        return (isTitle || isSentence) ? "x" : "";
    }

    private String hasSR(PatternLabeledBugReport bugCoded) {

        boolean isTitle = !bugCoded.getTitle().getSr().trim().isEmpty();
        boolean isSentence = false;

        PatternLabeledBugReportDescription description = bugCoded.getDescription();
        if (description != null) {
            isSentence = description.getAllSentences().stream().anyMatch(s -> !s.getSr().trim().isEmpty());
        }

        return (isTitle || isSentence) ? "x" : "";
    }

    private String hasEB(PatternLabeledBugReport bugCoded) {

        boolean isTitle = !bugCoded.getTitle().getEb().trim().isEmpty();
        boolean isSentence = false;

        PatternLabeledBugReportDescription description = bugCoded.getDescription();
        if (description != null) {
            isSentence = description.getAllSentences().stream().anyMatch(s -> !s.getEb().trim().isEmpty());
        }

        return (isTitle || isSentence) ? "x" : "";
    }

    private String hasOB(PatternLabeledBugReport bugCoded) {

        boolean isTitle = !bugCoded.getTitle().getOb().trim().isEmpty();
        boolean isSentence = false;

        PatternLabeledBugReportDescription description = bugCoded.getDescription();
        if (description != null) {
            isSentence = description.getAllSentences().stream().anyMatch(s -> !s.getOb().trim().isEmpty());
        }

        return (isTitle || isSentence) ? "x" : "";
    }

    private String getDescription(PatternLabeledBugReportDescription cDescription, List<TaggedText> codeTaggedTexts) {

        if (cDescription == null)
            return "";

        // -----------------------
        // propagate the paragraph labels to the sentences

        List<PatternLabeledDescriptionParagraph> paragraphs = cDescription.getParagraphs();

        if (paragraphs == null)
            return "";

        for (PatternLabeledDescriptionParagraph paragraph : paragraphs) {

            Labels paragraphLabels = new Labels(paragraph.getOb().trim(), paragraph.getEb().trim(),
                    paragraph.getSr().trim());

            List<PatternLabeledDescriptionSentence> sentences = paragraph.getSentences();
            for (PatternLabeledDescriptionSentence sent : sentences) {
                Labels sentLabels = new Labels(sent.getOb().trim(), sent.getEb().trim(), sent.getSr().trim());
                Labels mergedLabels = AgreementMain.mergeLabels(sentLabels, paragraphLabels);

                sent.setOb(mergedLabels.getIsOB());
                sent.setEb(mergedLabels.getIsEB());
                sent.setSr(mergedLabels.getIsSR());

            }
        }

        // ----------------------------------------

        StringBuffer description = new StringBuffer();

        List<PatternLabeledDescriptionSentence> sentences = cDescription.getAllSentences();
        for (PatternLabeledDescriptionSentence sentence : sentences) {
            String sentTxt = getSententence(sentence);
            description.append(sentTxt);
            description.append(" ");
        }

        //-----------------------------------

        appendCodeSnippets(description, codeTaggedTexts);

        return description.toString().trim();
    }

    private void appendCodeSnippets(StringBuffer buffer, List<TaggedText> codeTaggedTexts) {


        //any strategy with CODE?
        if (MainQueryReformulator.strategy.toString().contains("CODE")) {
            codeTaggedTexts.forEach(taggedText -> {
                buffer.append(taggedText.getValue());
                buffer.append(" ");
            });
        }

    }

    private String getSententence(PatternLabeledDescriptionSentence sentence) {

        String sent = "";

        switch (MainQueryReformulator.strategy) {
            case ALL_TEXT:
                sent = sentence.getValue();
                break;
            case OB:
            case OB_TITLE:
            case OB_CODE:
            case OB_TITLE_CODE:
                if (!sentence.getOb().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            case EB:
            case EB_TITLE:
            case EB_CODE:
            case EB_TITLE_CODE:
                if (!sentence.getEb().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            case S2R:
            case S2R_TITLE:
            case S2R_CODE:
            case S2R_TITLE_CODE:
                if (!sentence.getSr().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            case OB_EB:
            case OB_EB_TITLE:
            case OB_EB_CODE:
            case OB_EB_TITLE_CODE:
                if (!sentence.getOb().trim().isEmpty() || !sentence.getEb().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            case OB_S2R:
            case OB_S2R_TITLE:
            case OB_S2R_CODE:
            case OB_S2R_TITLE_CODE:
                if (!sentence.getOb().trim().isEmpty() || !sentence.getSr().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            case EB_S2R:
            case EB_S2R_TITLE:
            case EB_S2R_CODE:
            case EB_S2R_TITLE_CODE:
                if (!sentence.getEb().trim().isEmpty() || !sentence.getSr().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            case OB_EB_S2R:
            case OB_EB_S2R_TITLE:
            case OB_EB_S2R_CODE:
            case OB_EB_S2R_TITLE_CODE:
                if (!sentence.getOb().trim().isEmpty() || !sentence.getEb().trim().isEmpty()
                        || !sentence.getSr().trim().isEmpty()) {
                    sent = sentence.getValue();
                }
                break;
            default:
                break;
        }
        return sent;
    }

    private String getTitle(PatternLabeledBugReportTitle cTitle) {

        String title = "";

        switch (MainQueryReformulator.strategy) {
            case ALL_TEXT:
            case TITLE:
            case OB_TITLE:
            case EB_TITLE:
            case S2R_TITLE:
            case OB_EB_TITLE:
            case OB_S2R_TITLE:
            case EB_S2R_TITLE:
            case OB_EB_S2R_TITLE:
            case TITLE_CODE:
            case OB_TITLE_CODE:
            case EB_TITLE_CODE:
            case S2R_TITLE_CODE:
            case OB_EB_TITLE_CODE:
            case OB_S2R_TITLE_CODE:
            case EB_S2R_TITLE_CODE:
            case OB_EB_S2R_TITLE_CODE:
                title = cTitle.getValue();
                break;
            case OB:
            case OB_CODE:
                if (!cTitle.getOb().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case EB:
            case EB_CODE:
                if (!cTitle.getEb().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case S2R:
            case S2R_CODE:
                if (!cTitle.getSr().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case OB_EB:
            case OB_EB_CODE:
                if (!cTitle.getOb().trim().isEmpty() || !cTitle.getEb().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case OB_S2R:
            case OB_S2R_CODE:
                if (!cTitle.getOb().trim().isEmpty() || !cTitle.getSr().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case EB_S2R:
            case EB_S2R_CODE:
                if (!cTitle.getEb().trim().isEmpty() || !cTitle.getSr().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case OB_EB_S2R:
            case OB_EB_S2R_CODE:
                if (!cTitle.getOb().trim().isEmpty() || !cTitle.getEb().trim().isEmpty()
                        || !cTitle.getSr().trim().isEmpty()) {
                    title = cTitle.getValue();
                }
                break;
            case CODE:
                //the title does not have code snippets!
            default:
                break;
        }
        return title;
    }

}
