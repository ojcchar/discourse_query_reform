package seers.disqueryreform.querygen.coding;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.patterncoding.*;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BratToXmlTaggingConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(BratToXmlTaggingConverter.class);
    private static String sampleFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform" +
            "\\output" +
            "\\sampling\\8-30\\sample-8-30.csv";
    private static String bratFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
            "\\08-30" +
            "-bug_coding";
    private static String outFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
            "\\08-30" +
            "-bug_coding-xml";
    private static String nonBugsFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\data" +
            "\\08-30" +
            "-bug_coding\\non_bugs.csv";

    public static void main(String[] args) throws Exception {

        final List<List<String>> lines = CSVHelper.readCsv(sampleFile, true);
        final List<List<String>> nonBugs = CSVHelper.readCsv(nonBugsFile, true);
        Map<String, List<List<String>>> nonBugsPerCoder = nonBugs.stream()
                .collect(Collectors.groupingBy(l -> l.get(0)));

        for (List<String> line : lines) {
            final String dataSet = line.get(0);
            final String project = line.get(1);
            final String bugId = line.get(2);
            //the following index could 9 or 10, depending on the sample file format
            final String coder1 = line.get(10);
            final String coder2 = line.get(11);

            final List<List<String>> coderNonBugs1 = nonBugsPerCoder.get(coder1);
            processBugForCoder(dataSet, project, bugId, coder1, coderNonBugs1);

/*            final List<List<String>> coderNonBugs2 = nonBugsPerCoder.get(coder2);
            processBugForCoder(dataSet, project, bugId, coder2, coderNonBugs2);*/

        }


    }

    private static void processBugForCoder(String dataSet, String project, String bugId, String coder1,
                                           List<List<String>> coderNonBugs) throws Exception {
        List<TagIndexes> indexes = getTagIndexes(coder1, project, bugId, dataSet);
        List<List<BratSentence>> bratParagraphs = getBratSentences(coder1, project, bugId, dataSet);

        if (bratParagraphs == null)
            return;

        Stream<List<String>> projectNonBugs = null;
        if (coderNonBugs != null) {
            projectNonBugs =
                    coderNonBugs.stream().filter(nb -> dataSet.equals(nb.get(1)) && project.equals(nb.get(2)));
        }
        PatternLabeledBugReport bugReport = getLabeledBugReport(bugId, bratParagraphs, indexes, projectNonBugs, coder1);
        writeBugReport(dataSet, coder1, project, bugReport);
    }

    private static PatternLabeledBugReport getLabeledBugReport(String bugId, List<List<BratSentence>> bratParagraphs,
                                                               List<TagIndexes> indexes,
                                                               Stream<List<String>> coderNonBugs,
                                                               String coder) {

        final List<BratSentence> titleSentences = bratParagraphs.get(0);

        String titleString = titleSentences.stream().map(s -> s.text).collect(Collectors.joining(" "));
        PatternLabeledBugReportTitle title = new PatternLabeledBugReportTitle();
        title.setValue(titleString);
        ImmutableTriple<String, String, String> coding1 = getTitleCoding(titleSentences, indexes);
        title.setOb(coding1.left);
        title.setEb(coding1.middle);
        title.setSr(coding1.right);

        List<PatternLabeledDescriptionParagraph> paragraphs = new ArrayList<>();
        int paragraphId = 0;
        if (bratParagraphs.size() > 1) {
            final List<List<BratSentence>> descriptionParagraphs = bratParagraphs.subList(1, bratParagraphs.size());
            for (List<BratSentence> descriptionParagraph : descriptionParagraphs) {

                if (descriptionParagraph.isEmpty())
                    continue;

                paragraphId++;
                PatternLabeledDescriptionParagraph paragraph = new PatternLabeledDescriptionParagraph();
                paragraph.setId(String.valueOf(paragraphId));
                int sentenceId = 0;

                List<PatternLabeledDescriptionSentence> paragSentences = new ArrayList<>();
                for (BratSentence bratSentence : descriptionParagraph) {

                    sentenceId++;

                    PatternLabeledDescriptionSentence sentence = new PatternLabeledDescriptionSentence();

                    ImmutableTriple<String, String, String> coding2 = getSentenceCoding(bratSentence, indexes);
                    sentence.setOb(coding2.left);
                    sentence.setEb(coding2.middle);
                    sentence.setSr(coding2.right);
                    sentence.setValue(bratSentence.text);
                    sentence.setId(String.valueOf(paragraphId) + "." + String.valueOf(sentenceId));

                    paragSentences.add(sentence);
                }

                paragraph.setSentences(paragSentences);

                paragraphs.add(paragraph);
            }
        }


        PatternLabeledBugReportDescription description = new PatternLabeledBugReportDescription();
        description.setParagraphs(paragraphs);
        final PatternLabeledBugReport bugReport = new PatternLabeledBugReport(bugId, title, description);
        if (coderNonBugs != null)
            bugReport.setNoBug(coderNonBugs.anyMatch(l -> bugId.equals(l.get(3))) ? "x" : "");
        return bugReport;
    }

    private static ImmutableTriple<String, String, String> getSentenceCoding(BratSentence sentence,
                                                                             List<TagIndexes> indexes) {
        final int iniIdx = sentence.iniIdx;
        final int endIdx = sentence.endIdx;
        final List<TagIndexes> sentenceCoding = indexes.stream().filter(i -> (i.iniIdx >= iniIdx && i.iniIdx <=
                endIdx) || (i.endIdx >= iniIdx && i.endIdx <= endIdx)).collect(Collectors.toList());
        String ob = sentenceCoding.stream().anyMatch(t -> "Observed-Behavior".equals(t.tag)) ? "x" : "";
        String eb = sentenceCoding.stream().anyMatch(t -> "Expected-Behavior".equals(t.tag)) ? "x" : "";
        String s2r = sentenceCoding.stream().anyMatch(t -> "Steps-to-Reproduce".equals(t.tag)) ? "x" : "";
        return new ImmutableTriple<>(ob, eb, s2r);
    }

    private static ImmutableTriple<String, String, String> getTitleCoding(List<BratSentence> titleSentences,
                                                                          List<TagIndexes> indexes) {
        final List<ImmutableTriple<String, String, String>> coding = titleSentences.stream().map(s ->
                getSentenceCoding(s, indexes)).collect(Collectors.toList());
        String ob = coding.stream().anyMatch(t -> "x".equals(t.left)) ? "x" : "";
        String eb = coding.stream().anyMatch(t -> "x".equals(t.middle)) ? "x" : "";
        String s2r = coding.stream().anyMatch(t -> "x".equals(t.right)) ? "x" : "";
        return new ImmutableTriple<>(ob, eb, s2r);
    }

    private static List<List<BratSentence>> getBratSentences(String coder, String project, String bugId,
                                                             String dataSet)
            throws IOException {

        File bugTextFile = Paths.get(bratFolder, coder, dataSet, project, bugId + ".txt").toFile();

        if (!bugTextFile.exists()) {
            LOGGER.debug("File does not exist: " + bugTextFile);
            return null;
        }

        List<List<BratSentence>> sentences = new ArrayList<>();
        List<BratSentence> currentParagraph = new ArrayList<>();

        String bugText = FileUtils.readFileToString(bugTextFile, Charset.defaultCharset());

        String descriptionLabel = "BUG REPORT DESCRIPTION:";
        int descIniIdx = bugText.indexOf(descriptionLabel);
        int descEndIdx = descIniIdx + descriptionLabel.length();

        int fromIndex = 0;
        while (fromIndex < bugText.length()) {
            int toIndex = bugText.indexOf("\n", fromIndex);
            if (toIndex == -1)
                toIndex = bugText.length();

            final String text = bugText.substring(fromIndex, toIndex);
            if (!"BUG REPORT TITLE:".equals(text) && !descriptionLabel.equals(text)) {

                if (text.isEmpty()) {
                    if (!currentParagraph.isEmpty())
                        sentences.add(currentParagraph);
                    currentParagraph = new ArrayList<>();
                } else {
                    boolean isTitle = fromIndex > 17 && fromIndex < descIniIdx && toIndex > 17 && toIndex < descIniIdx;
                    BratSentence sentence = new BratSentence(isTitle, text, fromIndex, toIndex);
                    currentParagraph.add(sentence);
                }
            }

            fromIndex = toIndex + 1;
        }
        if (!currentParagraph.isEmpty())
            sentences.add(currentParagraph);
        return sentences;
    }

    private static List<TagIndexes> getTagIndexes(String coder, String project, String bugId, String dataSet) throws IOException {

        File bugAnnFile = Paths.get(bratFolder, coder, dataSet, project, bugId + ".ann").toFile();

        if (!bugAnnFile.exists()) {
            LOGGER.debug("File does not exist: " + bugAnnFile);
            return null;
        }

        List<String> annLines = FileUtils.readLines(bugAnnFile, Charset.defaultCharset());

        List<String> codedLines = annLines.stream().filter(l -> !l.trim().isEmpty() && l.startsWith("T")).collect
                (Collectors.toList());

        List<TagIndexes> indexes = new ArrayList<>();

        codedLines.forEach(codedLine -> {

            if (!codedLine.matches("^T\\d+\t.+"))
                return;

            String[] lineSections = codedLine.split("\t");
            int i = lineSections[1].indexOf(" ");
            String tag = lineSections[1].substring(0, i);
            String[] indicesPairs = lineSections[1].substring(i + 1).split(";");

            for (String indices : indicesPairs) {

                String[] split = indices.split(" ");
                int iniIdx = Integer.valueOf(split[0]);
                int endIdx = Integer.valueOf(split[1]);

                indexes.add(new TagIndexes(tag, iniIdx, endIdx));
            }

        });

        indexes.sort(Comparator.comparingInt(i -> i.iniIdx));
        return indexes;
    }

    private static void writeBugReport(String dataSet, String coder, String project, PatternLabeledBugReport bugReport)
            throws JAXBException, FileNotFoundException {

        final File baseFolder = Paths.get(outFolder, coder, dataSet, project).toFile();
        baseFolder.mkdirs();
        File outFile = Paths.get(baseFolder.getAbsolutePath(), bugReport.getId() + ".xml").toFile();
        XMLHelper.writeXML(bugReport, outFile);
    }

    public static class BratSentence {
        boolean isTitle;
        String text;
        int iniIdx;
        int endIdx;

        BratSentence(boolean isTitle, String text, int iniIdx, int endIdx) {
            this.isTitle = isTitle;
            this.text = text;
            this.iniIdx = iniIdx;
            this.endIdx = endIdx;
        }

        @Override
        public String toString() {
            return "s{" +
                    " tl=" + isTitle +
                    ", i=" + iniIdx +
                    ", e=" + endIdx +
                    ", t='" + text + '\'' +
                    '}';
        }
    }


    public static class TagIndexes {
        String tag;
        int iniIdx;
        int endIdx;

        TagIndexes(String tag, int iniIdx, int endIdx) {
            this.tag = tag;
            this.iniIdx = iniIdx;
            this.endIdx = endIdx;
        }


    }


    private static boolean invalidIndices(int iniIdx, int endIdx, int descIniIdx, int descEndIdx) {
        boolean iniCheck = (iniIdx >= 0 && iniIdx <= 17) || ((iniIdx >= descIniIdx && iniIdx <= descEndIdx));
        boolean endCheck = (endIdx >= 0 && endIdx <= 17) || ((endIdx >= descIniIdx && endIdx <= descEndIdx));
        return iniCheck || endCheck;
    }
}
