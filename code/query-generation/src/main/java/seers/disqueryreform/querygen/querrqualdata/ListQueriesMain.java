package seers.disqueryreform.querygen.querrqualdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import edu.wayne.cs.severe.ir4se.processor.controllers.RetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.entity.Query;
import edu.wayne.cs.severe.ir4se.processor.entity.RelJudgment;
import edu.wayne.cs.severe.ir4se.processor.entity.RetrievalDoc;
import edu.wayne.cs.severe.ir4se.processor.exception.CorpusException;
import edu.wayne.cs.severe.ir4se.processor.exception.QueryException;
import edu.wayne.cs.severe.ir4se.processor.exception.RelJudgException;
import edu.wayne.cs.severe.ir4se.processor.utils.ExceptionUtils;

public class ListQueriesMain {

	public static void main(String[] args) throws Exception {

		String pathname = "C:/Users/ojcch/Downloads/lobster_data";
//		String pathname = "C:/Users/ojcch/Documents/Repositories/Git/discourse_query_reform/data/existing_data_sets/query_reduction_data";
//		String pathname = "C:/Users/ojcch/Downloads/concept-location-dataset";
		File directory = new File(pathname);
		Collection<File> listFiles = FileUtils.listFiles(directory, new String[] { "txt" }, true);

		System.out.println(listFiles.size());

		RetrievalParser parser = getParser();

//		listQueryReductionQueries(listFiles, parser);
//		listSoniasTOSEMQueries(listFiles, parser);
		listLosterQueries(listFiles, new DefaultRetrievalParser());

	}

	private static void listLosterQueries(Collection<File> listFiles, RetrievalParser parser) 
			throws QueryException{
		for (File file : listFiles) {
			String name = file.getName();
			String suffix = "_Queries.txt";
			String suffix2 = "_Original_Queries.txt";
			if (!name.trim().endsWith(suffix) || name.trim().endsWith(suffix2)) {
				continue;
			}
			String system = name.trim().split(suffix)[0];

			String orginal =file.getParentFile().getAbsolutePath() +File.separator +system+suffix2 ;
			List<Query> queries = parser.readQueriesWithKey(file.getAbsolutePath(), orginal);
			for (int i = 0; i < queries.size() ; i++) {
				Query query = queries.get(i);
				System.out.println(system + ";" + query.getKey() + ";" + query.getTxt());
			}
		}
	}

	private static void listSoniasTOSEMQueries(Collection<File> listFiles, RetrievalParser parser)
			throws QueryException {
		for (File file : listFiles) {
			String name = file.getName();
			String suffix = "_Queries.txt";
			if (!name.trim().endsWith(suffix)) {
				continue;
			}
			String system = name.trim().split(suffix)[0];

			List<Query> queries = parser.readQueriesWithKey(file.getAbsolutePath(), null);
			for (int i = 0; i < queries.size() / 3; i++) {
				int j = 3 * i + 2;
				Query query = queries.get(j);
				System.out.println(system + ";" + query.getQueryId() + ";" + query.getTxt());
			}
		}
	}

	private static void listQueryReductionQueries(Collection<File> listFiles, RetrievalParser parser)
			throws QueryException {
		for (File file : listFiles) {
			String name = file.getName();
			String suffix = "_Queries.txt";
			if (!name.trim().endsWith(suffix)) {
				continue;
			}
			String system = name.trim().split(suffix)[0];
			List<Query> queries = parser.readQueriesWithKey(file.getAbsolutePath(), null);
			for (Query query : queries) {
				System.out.println(system+ ";" + query.getKey() + ";" + query.getTxt());
			}
		}
	}

	private static RetrievalParser getParser() {
		return new RetrievalParser() {

			@Override
			public Map<Query, RelJudgment> readRelevantJudgments(String releJudgmentPath, List<RetrievalDoc> corpusDocs)
					throws RelJudgException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Map<Query, RelJudgment> readReleJudgments(String releJudgmentPath, String mapDocsPath)
					throws RelJudgException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Query> readQueriesWithKey(String queriesPath, String originalQueriesFilePath)
					throws QueryException {

				List<Query> queryList = new ArrayList<Query>();
				File fileQuery = new File(queriesPath);

				if (!fileQuery.isFile() || !fileQuery.exists()) {
					throw new QueryException("Query file (" + queriesPath + ") is not valid!");
				}

				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(fileQuery));

					String line;
					int lineNumber = 0;
					Integer queryId = 0;
					String key = null;

					Query query = new Query();
					while ((line = reader.readLine()) != null) {

						// it is not a blank line
						String lineTrimmed = line.trim();
						if (!lineTrimmed.isEmpty()) {
							lineNumber++;
							switch (lineNumber) {
							case 1:
								if (lineTrimmed.contains(" ")) {
									queryId = Integer.valueOf(lineTrimmed.split(" ")[0]);
									key = lineTrimmed.split(" ")[1];
								} else {
									queryId = Integer.valueOf(lineTrimmed);
								}

								query.setQueryId(queryId);
								query.setKey(key);
								// queryId++;
								break;
							case 2:
								query.setTxt(lineTrimmed);
								queryList.add(query);
								break;
							}
						} else {
							lineNumber = 0;
							query = new Query();
						}
					}

				} catch (Exception e) {
					QueryException e2 = new QueryException(e.getMessage());
					ExceptionUtils.addStackTrace(e, e2);
					throw e2;
				} finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (IOException e) {
							throw new QueryException(e.getMessage());
						}
					}
				}
				return queryList;
			}

			@Override
			public List<Query> readQueries(String queriesFilePath, String queriesExcludedFilePath)
					throws QueryException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Query> readQueries(String queriesPath) throws QueryException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<RetrievalDoc> readCorpus(String corpFilePath, String mapDocsPath) throws CorpusException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<RetrievalDoc> readCorpus(String corpFilePath) throws CorpusException {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
