package seers.disqueryreform.querygen.parsing;

import java.util.List;

import seers.bugrepclassifier.BugReportClassifier;
import seers.bugrepcompl.entity.Labels;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportDescription;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionParagraph;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledDescriptionSentence;
import seers.bugreppatterns.pattern.predictor.PredictionOutput;
import seers.bugreppatterns.processor.PatternFeature;

public class SentenceClassifier {

	BugReportClassifier sentenceClassifier;
	BugReportClassifier paragraphClassifier;

	public SentenceClassifier(BugReportClassifier sentenceClassifier, BugReportClassifier paragraphClassifier) {
		super();
		this.sentenceClassifier = sentenceClassifier;
		this.paragraphClassifier = paragraphClassifier;
	}

	public void classifySentences(seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport codingBug) throws
			Exception {

		PredictionOutput output = sentenceClassifier.detectInformation(codingBug.getId(),
				codingBug.getTitle().getValue());
		updateTitleLabels(codingBug.getTitle(), output);

		PatternLabeledBugReportDescription description = codingBug.getDescription();
		if (description == null) {
			return;
		}

		List<PatternLabeledDescriptionParagraph> paragraphs = description.getParagraphs();
		for (PatternLabeledDescriptionParagraph paragraph : paragraphs) {
			classifyParagraph(codingBug.getId(), paragraph);
		}
	}

	private void classifyParagraph(String bugId, PatternLabeledDescriptionParagraph paragraph) throws Exception {
		StringBuffer paragraphText = new StringBuffer();
		for (PatternLabeledDescriptionSentence sentence : paragraph.getSentences()) {
			PredictionOutput output = sentenceClassifier.detectInformation(bugId, sentence.getValue());
			updateSentenceLabels(sentence, output);

			paragraphText.append(sentence.getValue());
			paragraphText.append(".\n");
		}
		if (paragraphText.length() > 0) {
			PredictionOutput output = paragraphClassifier.detectInformation(bugId, paragraphText.toString());
			updateParagraphLabels(paragraph, output);
		}
	}

	private void updateParagraphLabels(PatternLabeledDescriptionParagraph paragraph, PredictionOutput output) {
		Labels labels = output.getLabels();

		paragraph.setOb(labels.getIsOB());
		paragraph.setEb(labels.getIsEB());
		paragraph.setSr(labels.getIsSR());

		final String patterns = getPatterns(output);

		paragraph.setPatterns(patterns);
	}

	private void updateSentenceLabels(PatternLabeledDescriptionSentence sentence, PredictionOutput output) {
		Labels labels = output.getLabels();

		sentence.setOb(labels.getIsOB());
		sentence.setEb(labels.getIsEB());
		sentence.setSr(labels.getIsSR());

		final String patterns = getPatterns(output);

		sentence.setPatterns(patterns);
	}

	private void updateTitleLabels(PatternLabeledBugReportTitle title, PredictionOutput output) {
		Labels labels = output.getLabels();

		title.setOb(labels.getIsOB());
		title.setEb(labels.getIsEB());
		title.setSr(labels.getIsSR());

		final String patterns = getPatterns(output);

		title.setPatterns(patterns);

	}

	private String getPatterns(PredictionOutput output) {
		final StringBuffer patterns = new StringBuffer();
		for (PatternFeature feature : output.getFeatures()) {

			patterns.append(feature.getName());
			patterns.append(",");
		}
		if (patterns.length() > 0) {
			patterns.delete(patterns.length() - 1, patterns.length());
		} else {
			return null;
		}

		return patterns.toString();
	}
}
