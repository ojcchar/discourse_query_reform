package seers.disqueryreform.querygen.blizzard;

import edu.utdallas.seers.functions.Pair;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.disqueryreform.base.*;
import seers.disqueryreform.querygen.tasknav.DiscourseTasksQueryGenerator;
import seers.disqueryreform.retrieval.Corpus;
import seers.disqueryreform.retrieval.CorpusLoader;
import seers.disqueryreform.retrieval.SubProcessOutputCollector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static edu.utdallas.seers.functions.Functions.*;

/**
 * Uses the BLIZZARD tool to reformulate queries.
 */
@SuppressWarnings("deprecation")
public class BlizzardQueryGenerator {
    private final Logger logger = LoggerFactory.getLogger(BlizzardQueryGenerator.class);

    private final Path blizzardPath;
    private final Path dataSetsDir;
    private final Path discourseQueriesPath;
    private final DiscourseTasksQueryGenerator discourseTasksGenerator;
    private final Path baseRepoPath;
    private final Path outputPath;
    private final Set<String> blizzardExcludeWords;

    private BlizzardQueryGenerator(Path baseRepoPath, Path dataSetsDir, Path blizzardPath,
                                   Path outputPath, Set<String> excludeWords) {
        this.baseRepoPath = baseRepoPath;
        this.dataSetsDir = dataSetsDir;
        this.blizzardPath = blizzardPath;
        blizzardExcludeWords = excludeWords;
        discourseQueriesPath = baseRepoPath.resolve(Paths.get("data", "tasknav_reformulated_queries"));
        discourseTasksGenerator = new DiscourseTasksQueryGenerator(baseRepoPath);
        this.outputPath = outputPath;
    }

    public static void main(String[] args) throws IOException {
        createBlizzardQueryGenerator(Paths.get(args[0]))
                .generateQueries();
    }

    /**
     * Loads stop words, java keywords and creates a generator.
     *
     * @param baseRepoPath Base path
     * @return a new {@link BlizzardQueryGenerator}
     */
    public static BlizzardQueryGenerator createBlizzardQueryGenerator(Path baseRepoPath) throws IOException {
        Path outputPath = baseRepoPath.resolve(Paths.get("data", "blizzard-queries"));

        Path dataSetsDir = baseRepoPath.resolve(Paths.get("data", "existing_data_sets"));
        Path blizzardPath = baseRepoPath.resolve(Paths.get("code", "other-tools", "BLIZZARD"));

        Set<String> excludeWords = loadExcludeWords(blizzardPath);

        return new BlizzardQueryGenerator(baseRepoPath, dataSetsDir, blizzardPath, outputPath, excludeWords);
    }

    public static Set<String> loadExcludeWords(Path blizzardPath) throws IOException {
        Path stopWordsPath = blizzardPath.resolve(Paths.get("data", "stop-words-english-total.txt"));
        Path keywordsPath = blizzardPath.resolve(Paths.get("data", "keyword.txt"));

        return Stream.concat(Files.lines(stopWordsPath), Files.lines(keywordsPath))
                .collect(Collectors.toSet());
    }

    public void generateQueries() throws IOException {
        if (Files.exists(outputPath)) {
            logger.info("Blizzard queries already exist");
            return;
        }

        discourseTasksGenerator
                .generateQueries(true, true, true);

        Stream<Path> walk;
        try {
            walk = Files.walk(discourseQueriesPath, 1);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        ComponentsForBugTable table = discourseTasksGenerator.readComponentsForBugTable();

        Map<Project, List<DiscourseQuery>> allQueries =
                walk.filter(p -> Files.isRegularFile(p) && p.getFileName().toString().endsWith(".json") &&
                        // Only read strategy files and ignore component table
                        Character.isUpperCase(p.getFileName().toString().charAt(0))
                )
                        .map(p -> JSON.readJSONSilent(p, StrategyQueryContainer.class))
                        // Map each container to pairs (project, query)
                        .flatMap(c -> filterOriginalQueries(c, table))
                        .collect(groupPairs());

        List<StrategyQueryContainer> containers = reformulateProjects(allQueries);

        FileUtils.forceMkdir(outputPath.toFile());

        AtomicInteger ids = new AtomicInteger(1);
        for (StrategyQueryContainer container : containers) {
            // Set IDs, otherwise some runners will break
            container.allQueries().forEach(q -> q.setId(ids.getAndIncrement()));

            // Write
            JSON.writeJSON(container, outputPath.resolve(container.getStrategyName() + ".json"));
        }

        discourseTasksGenerator.preprocessQueries(outputPath);

        FileUtils.copyFileToDirectory(
                discourseQueriesPath.resolve(DiscourseTasksQueryGenerator.COMPONENT_TABLE_FILE_NAME).toFile(),
                outputPath.toFile()
        );

        logger.info("Finished generating blizzard queries");
        // TODO size summary is required for running on multiple machines
    }

    private List<StrategyQueryContainer> reformulateProjects(Map<Project, List<DiscourseQuery>> queries) {
        CorpusLoader grouper =
                new CorpusLoader(baseRepoPath.resolve(Paths.get("data", "existing_data_sets")), false);

        List<Corpus> corpora = queries.entrySet().stream()
                .flatMap(e -> grouper.loadCorpora(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        cleanBugReports(corpora);

        cleanUpBlizzardDirectory();

        Map<Project, List<DiscourseQuery>> blizzardQueries = corpora.parallelStream()
                .collect(Collectors.toConcurrentMap(
                        Corpus::getProject,
                        this::reformulateCorpus,
                        (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList())
                ));

        return Stream.of(queries, blizzardQueries)
                // For every (project, [queries])
                .flatMap(m -> m.entrySet().stream())
                // Unravel into pairs (project, query)
                .flatMap(e -> e.getValue().stream().map(pair(q -> e.getKey(), q -> q)))
                // Group pairs by strategy (strategy, [(project, query)...]
                .collect(Collectors.groupingBy(extract((p, q) -> q.getStrategy())))
                .entrySet().stream()
                .map(e -> {
                    StrategyQueryContainer container = new StrategyQueryContainer(e.getKey());

                    e.getValue().stream()
                            // Group pairs (project, query) into a map (project, [queries])
                            .collect(groupPairs())
                            .forEach((p, qs) -> container.addProject(p.getDataSet(), p.getProjectName(), qs));

                    return container;
                })
                .collect(Collectors.toList());
    }

    private List<DiscourseQuery> reformulateCorpus(Corpus corpus) {
        String corpusID = corpus.getProject() + "_" + corpus.getName();

        Map<DiscourseQuery, Integer> idMapping = createIDMapping(corpus.getQueries());

        logger.info("Writing BLIZZARD data for " + corpusID);
        writeRawBugReports(corpusID, corpus.getQueries(), idMapping);
        writeCorpus(corpusID, corpus);
        new BlizzardIndexGenerator(blizzardPath, blizzardExcludeWords)
                .createLuceneIndex(corpusID);

        Map<Integer, String> blizzardTexts;

        try {
            runBlizzard(corpusID, idMapping);

            blizzardTexts = Files.lines(blizzardPath.resolve(corpusID + "-queries.txt"))
                    .collect(Collectors.toMap(
                            l -> Integer.parseInt(l.split("\t")[0]),
                            l -> l.split("\t")[1]
                    ));
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        return corpus.getQueries().stream()
                .map(q -> {
                    String key = q.getKey();

                    String blizzardText = Optional.of(blizzardTexts.get(idMapping.get(q)))
                            .get();

                    String newStrategy = "BLIZZARD--" + q.getStrategy();

                    DiscourseQuery newQuery = new DiscourseQuery(key, newStrategy, q.getTaskNavConfiguration(),
                            // Blizzard does not provide separate title field and our tools don't care anyway
                            null, blizzardText);
                    newQuery.update(0, q.getCreationDate(), q.getResolutionDate(), q.getGoldSet());

                    return newQuery;
                })
                .collect(Collectors.toList());
    }

    private void runBlizzard(String corpusID, Map<DiscourseQuery, Integer> idMapping) throws IOException, InterruptedException {
        Path idsPath = blizzardPath.resolve(corpusID + "-ids.txt");
        //Write IDs
        Files.write(idsPath, idMapping.values().stream().map(Object::toString).collect(Collectors.toList()));
        JSON.writeJSON(idMapping, blizzardPath.resolve(corpusID + "-mapping.json"));

        Process blizzard = new ProcessBuilder("java", "-Xmx1g",
                "-jar", "blizzard-runner.jar",
                "-task", "reformulateQuery", "-repo", corpusID, "-bugIDFile", idsPath.toAbsolutePath().toString(),
                "-queryFile", corpusID + "-queries.txt")
                .directory(blizzardPath.toAbsolutePath().toFile())
                .start();

        Thread stdOut = new Thread(() -> {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(blizzard.getInputStream()))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    logger.info(String.format("[BLIZZARD-%s] - %s", corpusID, line));
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });

        SubProcessOutputCollector stdErr = new SubProcessOutputCollector(blizzard.getErrorStream());

        stdOut.start();
        stdErr.start();

        int result = blizzard.waitFor();
        stdOut.join();
        stdErr.join();

        if (result != 0) {
            logger.error(String.format("BLIZZARD error for %s:\n%s", corpusID, stdErr.getOutput()));
            throw new RuntimeException("BLIZZARD execution failed");
        }
    }

    private void writeCorpus(String corpusID, Corpus corpus) {
        DataSet dataSet = corpus.getProject().getDataSet();
        String projectName = corpus.getProject().getProjectName();

        Path sourcesPath = dataSet.resolveProjectSourcesPath(dataSetsDir, projectName, corpus.getName());
        Path corpusPath = blizzardPath.resolve(Paths.get("Corpus", corpusID));

        AtomicInteger ids = new AtomicInteger(1);

        logger.info("Writing corpus for " + corpus);
        try {
            Files.createDirectories(corpusPath);
            Files.walk(sourcesPath)
                    .filter(p -> p.toString().endsWith(".java"))
                    .sorted(Comparator.comparing(sourcesPath::relativize))
                    .forEach(p -> {
                        int id = ids.getAndIncrement();

                        try {
                            Files.copy(p, corpusPath.resolve(id + ".java"));
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
        } catch (IOException e) {
            throw new UncheckedIOException("Error loading project sources. Don't forget to extract " +
                    "BLIZZARD data supplement into the base repo dir", e);
        }
    }

    private void writeRawBugReports(String corpusID, List<DiscourseQuery> queries, Map<DiscourseQuery, Integer> idMapping) {
        Path brRawPath = blizzardPath.resolve(Paths.get("BR-Raw", corpusID));
        try {
            Files.createDirectories(brRawPath);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        logger.info(String.format("Writing raw %s bug reports", corpusID));

        for (DiscourseQuery query : queries) {
            Integer id = Optional.of(idMapping.get(query)).get();

            Path path = brRawPath.resolve(id + ".txt");

            String content =
                    String.format("Bug %d - %s\n%s", id,
                            query.getRawTitle().orElse(""),
                            query.getRawDescription().orElse(""));

            try {
                Files.write(path, Collections.singletonList(content));
            } catch (IOException e) {
                throw new RuntimeException("Error when writing bug report " + query.getKey(), e);
            }
        }
    }

    /**
     * Blizzard may only work if bug IDs are integers so we make this conversion just in case.
     *
     * @param queries To run with BLIZZARD.
     * @return Map of bug IDs to integer.
     */
    private Map<DiscourseQuery, Integer> createIDMapping(List<DiscourseQuery> queries) {
        return IntStream.rangeClosed(1, queries.size())
                .boxed()
                .collect(Collectors.toMap(
                        i -> queries.get(i - 1),
                        i -> i
                ));
    }

    /**
     * LUCENE-4182 has a weird text that makes BLIZZARD hang. We get rid of it here. Will still take
     * a while to process but it will finish.
     *
     * @param corpora Containing bug reports.
     */
    private void cleanBugReports(List<Corpus> corpora) {
        corpora.stream()
                .filter(c -> c.getName().startsWith("lucene"))
                .flatMap(c -> c.getQueries().stream())
                .forEach(q -> {
                    if (!q.getKey().equals("LUCENE-4182")) {
                        return;
                    }

                    logger.warn("Cleaning text of LUCENE-4182");

                    // Eliminate unicode escape sequences
                    Pattern pattern = Pattern.compile("\\\\u[A-Za-z0-9]{4}");
                    Matcher matcher = pattern.matcher(q.getRawDescription().orElse(""));

                    String cleanBody = matcher.replaceAll("");

                    // Query objects should be immutable, but we must modify this here because of this blizzard limitation
                    q.setRawText(q.getRawTitle().orElse(null), cleanBody);
                });
    }

    private void cleanUpBlizzardDirectory() {
        logger.info("Deleting data in BLIZZARD path");

        List<Path> dirs = Arrays.asList(
                blizzardPath.resolve("BR-Raw"),
                blizzardPath.resolve("Corpus"),
                blizzardPath.resolve("Lucene-Index")
        );

        for (Path dir : dirs) {
            logger.info("Deleting " + dir);
            FileUtils.deleteQuietly(dir.toFile());
        }
    }

    private Stream<Pair<Project, DiscourseQuery>> filterOriginalQueries(StrategyQueryContainer container, ComponentsForBugTable table) {
        return container.getItems().stream()
                .flatMap(e -> {
                    Project project = e.getKey();
                    return e.getValue().stream()
                            // If a query is partially voided its results will never be used
                            .filter(q -> !table.isPartiallyVoided(
                                    project,
                                    q.getKey(),
                                    q.getStrategy(),
                                    q.getTaskNavConfiguration()
                            ))
                            .map(pair(q -> project, q -> {
                                // So that preprocessing of original queries doesn't break
                                q.setText(q.getRawTitle().orElse(null), q.getRawDescription().orElse(null));
                                return q;
                            }));
                });
    }

}
