package seers.disqueryreform.querygen.tasknav;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import seers.disqueryreform.base.*;
import seers.disqueryreform.querygen.d4j.D4JBaselineQueryGeneratorMain;
import seers.disqueryreform.querygen.querrqualdata.QQDBaseQueriesGeneratorMain;
import seers.disqueryreform.querygen.utils.OriginalBugsReader;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GoldSetAdder {
    private int idSequence = 1;
    private Map<String, Map<String, Object>> uglyMap = new HashMap<>();
    private Path existingDataSetsDir;

    public GoldSetAdder(Path existingDataSetsDir) throws IOException {
        this.existingDataSetsDir = existingDataSetsDir;

        loadBRTAndLBGoldSets();
        loadQQGoldSets();
        loadB4BLGoldSets();
        loadD4JGoldSets();
    }

    void addGoldSets(List<StrategyQueryContainer> containers) {
        for (StrategyQueryContainer container : containers) {
            for (Map.Entry<Project, List<DiscourseQuery>> item : container.getItems()) {
                for (DiscourseQuery query : item.getValue()) {
                    addGoldSet(query, item.getKey());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void addGoldSet(DiscourseQuery query, Project projectTuple) {
        Map<String, Object> bugReport =
                uglyMap.get(projectTuple.getDataSet() + ";" + projectTuple.getProjectName() + ";" + query.getKey());

        Optional<Object> creationDateString = Optional.ofNullable(bugReport.get("creation_date"));
        DateTime creationDate = creationDateString
                .map(s -> DateTime.parse(((String) s).replace(" ", "T")))
                .orElse(null);

        Optional<Object> resolutionDateString = Optional.ofNullable(bugReport.get("resolution_date"));
        DateTime resolutionDate = resolutionDateString
                .map(s -> DateTime.parse(((String) s).replace(" ", "T")))
                .orElse(null);

        List<String> methodGoldset = (List<String>) bugReport.getOrDefault("method_goldset", Collections.emptyList());
        List<String> fixedClasses = (List<String>) bugReport.getOrDefault("fixed_classes", Collections.emptyList());
        List<String> fixedFiles = (List<String>) bugReport.getOrDefault("fixed_files", Collections.emptyList());

        query.update(idSequence++,
                creationDate,
                resolutionDate,
                Stream.of(methodGoldset, fixedClasses, fixedFiles)
                        .flatMap(Collection::stream)
                        .map(s -> s.replace('$', '.'))
                        .collect(Collectors.toSet()));
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    private void loadBRTAndLBGoldSets() throws IOException {
        Gson gson = GsonUtils.createDefaultGson();

        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();

        for (DataSet dataSet : Arrays.asList(DataSet.BRT, DataSet.LB)) {
            String bugReportFolder = DataSetUtils.getCorpusBaseFolder(existingDataSetsDir.toString(), null, dataSet);

            File[] systemSubFolders = new File(bugReportFolder).listFiles(File::isDirectory);
            for (File systemSubFolder : systemSubFolders) {
                // Eclipse subsystems TODO: centralize these (see LocusRetrieverWrapper in runner)
                if (Stream.of("jdt.core-3.1", "jdt.debug-3.1", "jdt.ui-3.1", "pde.build-3.1",
                        "pde.ui-3.1", "eclipse.platform.debug-3.1", "eclipse.platform.ui-3.1")
                        .anyMatch(s -> s.equals(systemSubFolder.getName()))) {
                    continue;
                }
                List<String> lines;
                String fname = "bug-reports";
                lines = FileUtils.readLines(new File(systemSubFolder + File.separator + fname +
                        ".json"), Charset.defaultCharset());

                lines.stream()
                        .map(line -> (Map<String, Object>) gson.fromJson(line, type))
                        .forEach(br -> {
                            String key = dataSet + ";" + systemSubFolder.getName() + ";" + br.get("key");
                            uglyMap.put(key, br);
                        });
            }
        }
    }

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    private void loadQQGoldSets() throws IOException {
        Gson gson = new Gson();

        String bugReportsFolder =
                existingDataSetsDir.resolve(Paths.get("query_quality_data", "queries_prep")).toString();

        for (File queriesFile : new File(bugReportsFolder).listFiles(f -> f.getName().endsWith(".json.prep"))) {
            List<String> lines = FileUtils.readLines(queriesFile);
            String system = queriesFile.getName().replace(".json.prep", "");

            for (String line : lines) {

                if (line.isEmpty()) {
                    continue;
                }

                Map<String, Object> jsonVals = gson.fromJson(line, Map.class);
                String key = DataSet.QQ + ";" + system + ";" + jsonVals.get("bug_id").toString();

                uglyMap.put(key, jsonVals);
            }
        }
    }

    private void loadB4BLGoldSets() {
        String corpusBaseFolder = DataSetUtils.getCorpusBaseFolder(existingDataSetsDir.toString(), null, DataSet.B4BL);
        Map<String, Map<String, Object>> map = OriginalBugsReader.readWithoutVersion(corpusBaseFolder);

        for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
            uglyMap.put(DataSet.B4BL + ";" + entry.getKey(), entry.getValue());
        }
    }

    private void loadD4JGoldSets() throws IOException {
        class Tuple<X, Y> {
            private final X x;
            private final Y y;

            private Tuple(X x, Y y) {
                this.x = x;
                this.y = y;
            }
        }

        Map<String, Tuple<DateTime, DateTime>> queryDates = new HashMap<>();

        String datesFile = existingDataSetsDir.resolve(Paths.get("defects4j_data", "d4j_bug_dates.csv")).toString();
        List<List<String>> lines = D4JUtils.readLines(new File(datesFile), ';');

        List<List<String>> subList = lines.subList(1, lines.size());

        for (List<String> list : subList) {
            String key = list.get(0) + "-" + list.get(1);
            Tuple<DateTime, DateTime> value = new Tuple<>(DateTime.parse(list.get(2)),
                    DateTime.parse(list.get(3)));
            queryDates.put(key, value);
        }

        List<DataSetUtils.BugKey> bugs = DataSetUtils.readSampledBugReports(existingDataSetsDir.toString(), DataSet.D4J);
        String goldSetsPath = existingDataSetsDir.resolve(Paths.get("defects4j_data", "gold_sets")).toString();

        for (DataSetUtils.BugKey bugKey : bugs) {
            Map<String, Object> goldsetInfo = new HashMap<>();
            String system = bugKey.system;
            String bugId = bugKey.bugId;
            goldsetInfo.put("class_goldset",
                    D4JBaselineQueryGeneratorMain.getGoldset(goldSetsPath, system, bugId, "class_goldset.txt"));
            goldsetInfo.put("method_goldset",
                    D4JBaselineQueryGeneratorMain.getGoldset(goldSetsPath, system, bugId, "method_goldset.txt"));

            Tuple<DateTime, DateTime> tuple = queryDates.get(system + "-" + bugId);

            Tuple<String, String> dates = new Tuple<>(tuple.x.toString(QQDBaseQueriesGeneratorMain.dtf),
                    tuple.y.toString(QQDBaseQueriesGeneratorMain.dtf));

            goldsetInfo.put("creation_date", dates.x);
            goldsetInfo.put("resolution_date", dates.y);

            uglyMap.put(DataSet.D4J + ";" + system + ";" + bugId, goldsetInfo);
        }
    }

    public List<String> getGoldSet(Project project, String queryKey) {
        Map<String, Object> bug = uglyMap.get(project.getDataSet() + ";" + project.getProjectName() + ";" + queryKey);

        List<String> methodGoldset = (List<String>) bug.getOrDefault("method_goldset", Collections.emptyList());
        List<String> fixedClasses = (List<String>) bug.getOrDefault("fixed_classes", Collections.emptyList());
        List<String> fixedFiles = (List<String>) bug.getOrDefault("fixed_files", Collections.emptyList());

        return Stream.of(methodGoldset, fixedClasses, fixedFiles)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }
}
