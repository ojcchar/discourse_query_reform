package seers.disqueryreform.querygen.parsing;

import java.io.File;
import java.util.List;

import org.joda.time.DateTime;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.utdallas.seers.entity.BugReport;
import edu.utdallas.seers.ir4se.index.utils.DateTimeJsonAdapter;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.threads.processor.ThreadProcessor;
import seers.appcore.xml.XMLHelper;
import seers.bugrepclassifier.parsing.TextParser;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportDescription;
import seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReportTitle;

public class LineExecutor extends ThreadProcessor {

	private static Gson gson;
	private List<String> lines;
	private String system;
	private File sysFolder;
	static TextParser txtParser = new TextParser();

	static {

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
		gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeJsonAdapter());
		gson = gsonBuilder.create();
	}

	public LineExecutor(ThreadParameters params) {
		super(params);

		lines = params.getListParam(String.class, ThreadExecutor.ELEMENTS_PARAM);
		system = params.getStringParam("system");
		sysFolder = params.getParam(File.class, "sysFolder");

	}

	@Override
	public void executeJob() throws Exception {

		SentenceClassifier sentClfier = new SentenceClassifier(LobsterMainParsing.sentenceClassifier,
				LobsterMainParsing.paragraphClassifier);

		for (String line : lines) {

			BugReport jsonBug = gson.fromJson(line, BugReport.class);
			String bugId = jsonBug.getKey();

			if (LobsterMainParsing.allowedBugs.get(system + "-" + bugId) == null) {
				continue;
			}

			String description = jsonBug.getDescription();
			if (description == null) {
				System.err.println("Bug with no description: " + system + " " + bugId);
				continue;
			}

			System.out.println("Processing bug: " + system + ";" + bugId);

			description = description.replace("\r\n", "\n");

			try {
				// parse the description
				PatternLabeledBugReportDescription parsedDesc = txtParser
						.parseText2(description);

				// write the XML

				PatternLabeledBugReportTitle title = new PatternLabeledBugReportTitle();
				title.setValue(jsonBug.getTitle());

				seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport codingBug = new seers.bugrepcompl.entity.patterncoding.PatternLabeledBugReport(
						bugId, title, parsedDesc);
				if (LobsterMainParsing.autoTag) {
					sentClfier.classifySentences(codingBug);
				}

				File outputFile = new File(sysFolder + File.separator + bugId + ".xml");
				XMLHelper.writeXML(codingBug, outputFile);
			} catch (Exception e) {
				System.err.println("Error for bug: " + system + " " + bugId);
				e.printStackTrace();
			}
		}
	}

}
