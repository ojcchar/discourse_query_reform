package seers.disqueryreform.querygen.coding;

import com.google.gson.Gson;
import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.csv.CSVHelper;
import seers.appcore.utils.FilePathUtils;
import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.regularparse.ParsedBugReport;
import seers.bugrepcompl.entity.regularparse.ParsedBugReportDescription;
import seers.bugrepcompl.entity.regularparse.ParsedDescriptionParagraph;
import seers.bugrepcompl.entity.regularparse.ParsedDescriptionSentence;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.nimbus.bugparser.sentence.BugReportSentenceParser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MainBRATBugCodingGenerator {

    /*
        public static Set<String> projects =
                JavaUtils.getSet("eclipse-3.1", "swt-3.1", "lang", "math", "time", "argouml-0.22", "bookkeeper-4.1.0",
                        "derby-10.9.1.0", "derby-10.7.1.1", "hibernate-3.5.0b2", "jabref-2.6", "jedit-4.3", "lucene-4
                        .0",
                        "mahout-0.8", "openjpa-2.0.1", "pig-0.11.1", "pig-0.8.0", "solr-4.4.0", "tika-1.3",
                        "zookeeper-3" +
                                ".4.5", "adempiere-3.1.0", "apache-nutch-1.8", "apache-nutch-2.1", "atunes-1.10.0",
                        "commons-math-3-3.0", "eclipse-2.0", "mahout-0.4", "openjpa-2.2.0");
    */
    public static Set<String> projects = Collections.emptySet();
    private static String sampleFile = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\output" +
            "\\sampling\\8-30\\sample-8-30.csv";
    private static File outFolder = new File("C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform\\output" +
            "\\08-30-bug_coding");

    private static final Logger LOGGER = LoggerFactory.getLogger(MainBRATBugCodingGenerator.class);
    private static String baseDataFolder = "C:\\Users\\ojcch\\Documents\\Repositories\\Git\\discourse_query_reform" +
            "\\data\\existing_data_sets";

    public static void main(String[] args) throws Exception {

        FileUtils.forceMkdir(outFolder);

        //read sample
        List<List<String>> lines = CSVHelper.readCsv(sampleFile, true, CSVHelper.DEFAULT_SEPARATOR);

        if (projects == null || projects.isEmpty()) {
            //pick all the projects from the sample
            projects = lines.stream().map(l -> l.get(1)).collect(Collectors.toSet());
        }

        Gson gson = GsonUtils.createDefaultGson();

        //for each system
        for (String project : projects) {

            //get the sample of the system
            List<List<String>> projLines = lines.stream()
                    .filter(l -> l.get(1).equalsIgnoreCase(project))
                    .collect(Collectors.toList());

            //for each bug of the sample
            for (List<String> bugLine : projLines) {

                String bugKey = bugLine.get(2);
                String dataset = bugLine.get(0);

                LOGGER.debug("Processing " + bugKey);

                Map<String, BugReport> bugReports = readBugReports(gson, project, dataset, bugKey);

                //get the title and description of the bug
                BugReport bugReport = bugReports.get(bugLine.get(2));

                //get the coders
                String coder1 = bugLine.get(bugLine.size() - 2);
                String coder2 = bugLine.get(bugLine.size() - 1);

                //generate the BRAT files for coder 1
                Boolean subFoldersNeeded = true;
                generateBratFiles(coder1, bugReport, project, dataset, subFoldersNeeded);

                //generate the BRAT files for coder 2
                generateBratFiles(coder2, bugReport, project, dataset, subFoldersNeeded);

            }

        }


    }

    private static Map<String, BugReport> readBugReports(Gson gson, String project, String dataset, String bugKey) throws Exception {
        final DataSet dataSet = DataSet.valueOf(dataset.toUpperCase());
        File baseFolder = DataSetUtils.getBaseDataSetFolder(baseDataFolder, dataSet);

        switch (dataSet) {
            case BRT:
            case LB:
                final File bugReportsFile = Paths.get(baseFolder.getAbsolutePath(),
                        project, "original-bug-reports.json").toFile();
                return readBugReportsFromJson(gson, bugReportsFile);
            case B4BL:
                final Map<String, BugReport> allBugReports = new LinkedHashMap<>();

                final File[] projectVersionFolders = Paths.get(baseFolder.getAbsolutePath(), project).toFile()
                        .listFiles(File::isDirectory);

                if (projectVersionFolders == null)
                    return allBugReports;

                for (File projectVersion : projectVersionFolders) {
                    final File bugReportsFile2 = Paths.get(projectVersion.getAbsolutePath(),
                            "original-bug-reports.json").toFile();
                    if (!bugReportsFile2.exists()) continue;
                    allBugReports.putAll(readBugReportsFromJson(gson, bugReportsFile2));
                }
                return allBugReports;
            case D4J:
            case QQ:
                final seers.bugrepcompl.entity.BugReport bugReport =
                        XMLHelper.readXML(seers.bugrepcompl.entity.BugReport.class,
                                Paths.get(baseFolder.getAbsolutePath(),
                                        "bug_reports", project, bugKey + ".xml").toFile());

                Map<String, BugReport> map = new HashMap<>();
                BugReport br = new BugReport(bugReport.getId(), bugReport.getTitle(), bugReport.getDescription());
                map.put(bugReport.getId(), br);
                return map;

        }

        return null;
    }

    private static Map<String, BugReport> readBugReportsFromJson(Gson gson, File bugReportsFile) throws IOException {
        List<String> bugRepLines = FileUtils.readLines(bugReportsFile, Charset.defaultCharset());
        return bugRepLines.stream().map(d -> gson.fromJson(d, BugReport.class))
                .collect(Collectors.toMap(BugReport::getKey, Function.identity()));
    }

    private static void generateBratFiles(String coder, BugReport bugReport, String system,
                                          String dataset, Boolean subFolderNeeded) throws IOException {
        //parse the lines of the bug
        seers.bugrepcompl.entity.BugReport bugReport2 = new seers.bugrepcompl.entity.BugReport();
        bugReport2.setId(bugReport.getKey());
        bugReport2.setTitle(bugReport.getTitle());
        bugReport2.setDescription(bugReport.getDescription());

        ParsedBugReport parsedBugReport = BugReportSentenceParser.parseBugReport(bugReport2);

        //save the lines into the text file
        String bugFileName = bugReport.getKey();
        File bugGroupFolder;
        if (!subFolderNeeded) {
            bugGroupFolder = FilePathUtils.getFile(outFolder, coder);
        } else {
            bugGroupFolder = FilePathUtils.getFile(outFolder, coder, dataset, system);
        }

        File txtFile = FilePathUtils.getFile(bugGroupFolder, bugFileName + ".txt");
        ArrayList<String> lines = getContentForTagging(parsedBugReport);
        FileUtils.writeLines(txtFile, lines, IOUtils.LINE_SEPARATOR_UNIX);

        //create the annotations file
        File annFile = FilePathUtils.getFile(bugGroupFolder, bugFileName + ".ann");
        annFile.createNewFile();

//        System.out.println("file: "+lines.size()+" - "+annFile);
    }

    private static ArrayList<String> getContentForTagging(ParsedBugReport parsedBugReport) {
        ArrayList<String> lines = new ArrayList<>();
        lines.add("BUG REPORT TITLE:");
        lines.add(parsedBugReport.getTitle());
        lines.add("");
        lines.add("BUG REPORT DESCRIPTION:");
        lines.addAll(getBugReportSentences(parsedBugReport));
        return lines;
    }

    private static List<String> getBugReportSentences(ParsedBugReport parsedBugReport) {

        List<String> lines = new ArrayList<>();

        ParsedBugReportDescription description = parsedBugReport.getDescription();
        if (description == null)
            return lines;

        List<ParsedDescriptionParagraph> paragraphs = description.getParagraphs();
        if (paragraphs == null)
            return lines;

        for (ParsedDescriptionParagraph paragraph : paragraphs) {
            if (paragraph == null) {
                continue;
            }

            List<ParsedDescriptionSentence> sentences = paragraph.getSentences();
            if (sentences == null) {
                continue;
            }

            for (ParsedDescriptionSentence sentence : sentences) {
                lines.add(sentence.getValue());
            }

            lines.add("");
        }

        if (!lines.isEmpty()) {
            lines.remove(lines.size() - 1);
        }

        return lines;
    }

}
