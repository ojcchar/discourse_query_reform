package seers.disqueryreform.querygen.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.utdallas.seers.entity.BugReport;
import edu.wayne.cs.severe.ir4se.processor.utils.GsonUtils;
import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;
import seers.appcore.xml.XMLHelper;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OriginalBugsReader {
    private final Map<DataSet, Path> corpusPaths = new HashMap<>();

    public OriginalBugsReader(Path datasetsDirPath) {
        corpusPaths.put(DataSet.D4J, datasetsDirPath.resolve("defects4j_data/bug_reports"));
        corpusPaths.put(DataSet.QQ, datasetsDirPath.resolve(Paths.get("query_quality_data", "queries")));
        corpusPaths.put(DataSet.B4BL, datasetsDirPath.resolve("bench4bl_data"));
        corpusPaths.put(DataSet.BRT, datasetsDirPath.resolve("buglocator_data"));
        corpusPaths.put(DataSet.LB, datasetsDirPath.resolve("lobster_data"));
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    private static Map<String, Map<String, Object>> readBRTOrLBBugReports(String bugReportFolder) {
        Gson gson = GsonUtils.createDefaultGson();
        Map<String, Map<String, Object>> bugReports = new LinkedHashMap<>();

        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();

        File[] systemSubFolders = new File(bugReportFolder).listFiles(File::isDirectory);
        for (File systemSubFolder : systemSubFolders) {
            if (Stream.of("jdt.core-3.1", "jdt.debug-3.1", "jdt.ui-3.1", "pde.build-3.1", "pde.ui-3.1")
                    .anyMatch(s -> s.equals(systemSubFolder.getName()))) {
                continue;
            }
            List<String> lines;
            try {
                String fname = "original-bug-reports";
                lines = FileUtils.readLines(new File(systemSubFolder + File.separator + fname +
                        ".json"), Charset.defaultCharset());
            } catch (IOException e) {
                continue;
            }
            lines.stream().map(line -> (Map<String, Object>) gson.fromJson(line, type)).forEach(br -> {
                String key = systemSubFolder.getName() + ";" + br.get("key");
                bugReports.put(key, br);
            });
        }

        return bugReports;
    }

    public static Map<String, Map<String, Object>> readWithoutVersion(String corpusPath) {
        Map<String, Map<String, Object>> corpus = readBRTOrLBBugReports(corpusPath);

        return corpus.entrySet().stream()
                .collect(Collectors.toMap(e -> {
                    String systemNoVersion = e.getKey().split(";")[0].split("_")[0];
                    String id = e.getKey().split(";")[1];
                    return systemNoVersion + ";" + id;
                }, Map.Entry::getValue));
    }

    public Map<String, List<BugReport>> readOriginalBugs(DataSet dataSet) throws IOException, JAXBException, SAXException, ParserConfigurationException {
        Map<String, Map<String, Object>> originalBugReports;
        String corpusPath = corpusPaths.get(dataSet).toString();
        switch (dataSet) {
            case QQ:
                originalBugReports = readSampledBugReports(
                        corpusPath,
                        dataSet,
                        readBugReportsFlat(corpusPath));
                break;
            case D4J:
                originalBugReports = readBugReportsXML(corpusPath);
                break;
            case B4BL:
            case LB:
                originalBugReports = readSampledBugReports(
                        corpusPath,
                        dataSet,
                        readWithoutVersion(corpusPath));
                break;
            case BRT:
                originalBugReports = readSampledBugReports(
                        corpusPath,
                        dataSet,
                        readBRTOrLBBugReports(corpusPath));
                break;
            default:
                throw new IOException("Unknown dataset");
        }

        Map<String, List<BugReport>> outputBugs = new HashMap<>();

        for (Map.Entry<String, Map<String, Object>> bugEntry : originalBugReports.entrySet()) {
            String bugSysKey = bugEntry.getKey();
            String project = bugSysKey.split(";")[0];
            String key = bugSysKey.split(";")[1];

            Map<String, Object> originalMap = bugEntry.getValue();

            outputBugs.computeIfAbsent(project, s -> new ArrayList<>()).add(
                    new BugReport(key, (String) originalMap.get("title"), ((String) originalMap.get("description")))
            );
        }

        return outputBugs;
    }

    private Map<String, Map<String, Object>> readSampledBugReports(String corpusPath,
                                                                   DataSet dataSet,
                                                                   Map<String, Map<String, Object>> allBugs)
            throws IOException {
        File dataDir = Paths.get(corpusPath).toAbsolutePath().normalize().toFile();
        while (!dataDir.getName().equals("existing_data_sets")) {
            dataDir = dataDir.getParentFile();
        }

        Set<DataSetUtils.BugKey> sampledKeys = new HashSet<>(
                DataSetUtils.readSampledBugReports(dataDir.getAbsolutePath(), dataSet));

        return allBugs.entrySet().stream()
                .filter(e -> {
                    String system = e.getKey().split(";")[0];
                    String id = e.getKey().split(";")[1];

                    return sampledKeys.contains(new DataSetUtils.BugKey(system, id));
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @SuppressWarnings("ConstantConditions")
    private Map<String, Map<String, Object>> readBugReportsXML(String corpusPath) throws IOException, ParserConfigurationException, SAXException, JAXBException {
        Map<String, Map<String, Object>> result = new HashMap<>();

        Set<String> sampled = DataSetUtils
                .readSampledBugReports(Paths.get(corpusPath).resolve("../../../..").toString(), DataSet.D4J).stream()
                .map(k -> k.system + ";" + k.bugId)
                .collect(Collectors.toSet());

        for (File projectDir : new File(corpusPath).listFiles(File::isDirectory)) {
            for (File bugFile : projectDir.listFiles()) {
                seers.bugrepcompl.entity.BugReport bug =
                        XMLHelper.readXML(seers.bugrepcompl.entity.BugReport.class, bugFile.getAbsolutePath());

                Map<String, Object> outBug = new HashMap<>();
                String key = projectDir.getName() + ";" + bug.getId();
                if (!sampled.contains(key)) {
                    continue;
                }

                outBug.put("key", key);
                outBug.put("title", bug.getTitle());
                outBug.put("description", bug.getDescription());

                result.put(key, outBug);
            }
        }

        return result;
    }

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    private Map<String, Map<String, Object>> readBugReportsFlat(String bugReportFolder) throws IOException {
        Gson gson = GsonUtils.createDefaultGson();
        Map<String, Map<String, Object>> bugReports = new LinkedHashMap<>();

        File[] systemFiles = new File(bugReportFolder).listFiles(f -> f.getName().endsWith(".json"));
        for (File systemFile : systemFiles) {
            List<String> lines = FileUtils.readLines(systemFile, Charset.defaultCharset());

            for (String line : lines) {
                Map<String, Object> br = gson.fromJson(line, Map.class);
                String key = systemFile.getName().replace(".json", "") + ";" + br.get("bug_id");
                bugReports.put(key, br);
            }
        }

        return bugReports;
    }
}
