package seers.disqueryreform.querygen.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utils {


	public static class D4JBugMapping {

		public String system;
		public String d4jId;
		public String bugId;

		public D4JBugMapping(String system, String d4jId, String bugId) {
			super();
			this.system = system;
			this.d4jId = d4jId;
			this.bugId = bugId;
		}

		@Override
		public String toString() {
			return "[sys=" + system + ", d4jId=" + d4jId + ", bugId=" + bugId + "]";
		}

	}

	public static List<D4JBugMapping> readD4JBugsMapping(String mappingFile) throws IOException {

		List<D4JBugMapping> bugs = new ArrayList<>();

		// read the data file
		try (BufferedReader br = new BufferedReader(new FileReader(mappingFile))) {

			String line = br.readLine();

			// for each bug
			while ((line = br.readLine()) != null) {

				if (line.trim().isEmpty()) {
					continue;
				}

				String[] split = line.split(";");
				bugs.add(new D4JBugMapping(split[0], split[1], split[2]));

			}
		}

		return bugs;
	}
}
