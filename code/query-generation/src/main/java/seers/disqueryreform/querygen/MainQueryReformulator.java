package seers.disqueryreform.querygen;

import com.google.gson.Gson;
import edu.stanford.nlp.parser.nndep.Dataset;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seers.appcore.threads.ThreadExecutor;
import seers.appcore.threads.processor.ThreadParameters;
import seers.appcore.utils.EnumUtils;
import seers.disqueryreform.base.DataSet;
import seers.disqueryreform.base.DataSetUtils;
import seers.disqueryreform.base.DataSetUtils.BugKey;
import seers.disqueryreform.base.InformationSource;
import seers.disqueryreform.base.QueryReformStrategy;

import java.io.File;
import java.util.List;

public class MainQueryReformulator {

	private static final Logger LOGGER = LoggerFactory.getLogger(MainQueryReformulator.class);

	static String baseDataFolder = "";
	static String queriesOutBaseFolder = "";
	static DataSet dataset;
	static boolean andOperator;

	static Gson gson = new Gson();
	static QueryReformStrategy strategy;
	static File strategyOutputDir;
	static InformationSource infoSource;

	static File codedBugsFolder;
	static File queriesOutFolder;

	public static void main(String[] args) throws Exception {

		strategy = EnumUtils.getEnumParam(args[0], QueryReformStrategy.class);
		baseDataFolder = args[1];
		queriesOutBaseFolder = args[2];
		dataset = DataSet.valueOf(args[3]);
		andOperator = "and".equals(args[4]);
		infoSource = EnumUtils.getEnumParam(args[5], InformationSource.class);

		queriesOutFolder = DataSetUtils.getReformulatedQueriesOutFolder(queriesOutBaseFolder, dataset,
				andOperator);
		codedBugsFolder = DataSetUtils.getCodedBugReportsFolder(baseDataFolder, dataset);

		// read all the bugs
		List<BugKey> bugs = DataSetUtils.readSampledBugReports(baseDataFolder, dataset);

		strategyOutputDir = new File(queriesOutFolder + File.separator + infoSource.toString() + File.separator +
				strategy.toString());
		FileUtils.deleteDirectory(strategyOutputDir);
		strategyOutputDir.mkdirs();

		File file = new File(queriesOutFolder + File.separator
				+ strategy.toString() + ".csv");
		if (file.exists()) {
			FileUtils.forceDelete(file);
		}
		CharSequence data = "system;bugId;has_ob;has_eb;has_sr;has_other;has_code\n";
		FileUtils.write(file, data, true);

		LOGGER.debug("strategy: " + strategy.toString());
		LOGGER.debug("# of bugs: " + bugs.size());

		ThreadExecutor.executePaginated(bugs, BugExecutor.class, new ThreadParameters());

		LOGGER.debug("done!");

	}

}
