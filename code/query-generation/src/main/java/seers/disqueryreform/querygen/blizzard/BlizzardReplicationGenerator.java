package seers.disqueryreform.querygen.blizzard;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Generates the Lucene indexes for BLIZZARD replication.
 */
public class BlizzardReplicationGenerator {
    private final BlizzardIndexGenerator indexGenerator;
    private final Path blizzardPath;

    public BlizzardReplicationGenerator(Path blizzardPath) throws IOException {
        this.blizzardPath = blizzardPath;
        indexGenerator = new BlizzardIndexGenerator(
                blizzardPath,
                BlizzardQueryGenerator.loadExcludeWords(blizzardPath)
        );
    }

    public static void main(String[] args) throws IOException {
        Path blizzardPath = Paths.get(args[0]);

        new BlizzardReplicationGenerator(blizzardPath).generateIndexes();
    }

    private void generateIndexes() throws IOException {
        Path corpusPath = blizzardPath.resolve("Corpus");

        Files.walk(corpusPath, 1)
                .filter(p -> !p.equals(corpusPath) && Files.isDirectory(p))
                .forEach(p -> indexGenerator.createLuceneIndex(p.getFileName().toString()));
    }

}
