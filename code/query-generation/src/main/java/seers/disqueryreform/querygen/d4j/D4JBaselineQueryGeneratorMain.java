package seers.disqueryreform.querygen.d4j;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import seers.appcore.xml.XMLHelper;
import seers.bugrepcompl.entity.BugReport;
import seers.disqueryreform.base.D4JUtils;

public class D4JBaselineQueryGeneratorMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(D4JBaselineQueryGeneratorMain.class);

	static String bugReportsFolder = "";
	static String goldSetsFolder = "";
	static String bugsFile = "";
	static String outputDir = "";

	public static void main(String[] args) throws Exception {

		bugReportsFolder = FilenameUtils.separatorsToSystem(args[0]);
		goldSetsFolder = args[1];
		bugsFile = args[2];
		outputDir = args[3];

		// read all the bugs
		List<List<String>> allBugs = D4JUtils.readLines(new File(bugsFile), ';');
		List<List<String>> subList = allBugs.subList(1, allBugs.size());

		LOGGER.debug("# of bugs: " + subList.size());

		// --------------------

		// out file
		File outFolder = new File(outputDir);
		FileUtils.deleteDirectory(outFolder);
		outFolder.mkdir();

		// --------------------

		Gson gson = new Gson();
		AtomicInteger sequence = new AtomicInteger(1);

		for (List<String> bug : subList) {

			try {
				String system = bug.get(0);
				String d4jId = bug.get(1);
				String bugId = bug.get(2);

				File outFile = new File(outFolder + File.separator + system + ".json");

				// create json object
				Map<String, Object> json = new LinkedHashMap<>();
				json.put("id", sequence.getAndIncrement());
				json.put("d4j_id", d4jId);
				json.put("bug_id", bugId);
				json.put("text", getWholeText(system, bugId));
				json.put("class_goldset", getGoldset(goldSetsFolder, system, bugId, "class_goldset.txt"));
				json.put("method_goldset", getGoldset(goldSetsFolder, system, bugId, "method_goldset.txt"));

				FileUtils.write(outFile, gson.toJson(json) + "\n", true);
			} catch (Exception e) {
				LOGGER.error("Error for bug: " + bug, e);
			}

		}

		LOGGER.debug("Done!");
	}

	public static List<String> getGoldset(String goldSetsFol, String system, String bugId, String fileName) throws IOException {
		List<String> lines = FileUtils.readLines(new File(goldSetsFol + File.separator + system.toLowerCase()
				+ File.separator + bugId + File.separator + fileName));
		return lines.stream().filter(l -> !l.trim().isEmpty()).collect(Collectors.toList());
	}

	private static String getWholeText(String system, String bugId) throws Exception {
		BugReport report = XMLHelper.readXML(BugReport.class,
				bugReportsFolder + File.separator + system.toLowerCase() + File.separator + bugId + ".xml");
		return report.getTitle() + ". " + report.getDescription();
	}

}
